using System;
using System.Linq;
using System.Xml.Serialization;
using BankALM.Infrastructure.Data;
using NHibernate;
using NHibernate.Linq;

namespace BondManager.Domain
{
    // [ActiveRecord(Lazy = false)]
    [Serializable]
    [XmlRoot]
    public class FXRate : EntityBase<FXRate, int>
    {
        private Currency from;
        private Currency to;
        private string value;

        // [Property]
        [XmlElement]
        public virtual string Value
        {
            get { return this.value; }
            set { this.value = value; }
        }

        // [Property]
        [XmlElement]
        public virtual Currency From
        {
            get { return this.from; }
            set { this.from = value; }
        }

        // [Property]
        [XmlElement]
        public virtual Currency To
        {
            get { return this.to; }
            set { this.to = value; }
        }

        public static FXRate GetRate(ISession session, Currency from, Currency to)
        {
            return session.Query<FXRate>().FirstOrDefault(fxr => fxr.From == from && fxr.To == to);
            //return FindFirst(Expression.And(Expression.Eq("From", from), Expression.Eq("To", to)));
        }


    }


}