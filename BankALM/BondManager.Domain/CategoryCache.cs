﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BondManager.Domain
{
    public static class CategoryCache
    {
        public static List<Category> categories = new List<Category>();

        public static void set(Category[] cats)
        {
            categories = cats.ToList();
        }

        public static Category FindSpecialCategory( BalanceType type)
        {
            return categories.Find(c => c.BalanceType == type);//session.Query<Category>().FirstOrDefault(c => c.BalanceType == type);
        }

        public static Category FindBalanceSheet()
        {
            return FindSpecialCategory(BalanceType.BalanceSheet);
        }

        public static Category FindOffBalanceSheet()
        {
            return FindSpecialCategory(BalanceType.OffBalanceSheet);
        }

        public static Category FindPortfolio()
        {
            return FindSpecialCategory( BalanceType.Portfolio);
        }

        public static Category FindNodeByName( string lbl)
        {
            return categories.Find(c => c.Label == lbl);//session.Query<Category>().FirstOrDefault(c => c.Label == lbl);
            //return FindOne(nullableSession, Expression.Eq("Label", lbl));
        }

        public static Category[] FindByType( CategoryType cattype)
        {
            return categories.FindAll(c => c.Type == cattype).ToArray();//session.Query<Category>().Where(c => c.Type == cattype).ToArray();
            //return FindAllByProperty(nullableSession, "Type", cattype);
        }

        public static Category[] FindByBalanceType( BalanceType baltype)
        {
            //Category[] findByBalanceType = FindAllByProperty(nullableSession, "BalanceType", baltype);
           // Category[] categories = session.Query<Category>().Where(c => c.BalanceType == baltype).ToArray();
            //return categories;
            return categories.FindAll(c => c.BalanceType == baltype).ToArray();
        }

        public static Category[] FindByBalanceTypeAndCatType( BalanceType baltype, CategoryType cattype)
        {
            return categories.FindAll(c => c.BalanceType == baltype && c.Type == cattype).ToArray();//session.Query<Category>().Where(c => c.BalanceType == baltype && c.Type == cattype).ToArray();
            //return FindAll(nullableSession, Expression.Eq("BalanceType", baltype), Expression.Eq("Type", cattype));
        }

        public static IEnumerable<Product> LoadProducts( Category category, int currentPage, int categoryRowsPerPage, out int count)
        {
            //ICriteria criteria = session.CreateCriteria<Product>().Add(Expression.Eq("Category.Id", category.Id)).AddOrder(Order.Asc("Position"))
            //    .SetFirstResult(currentPage * categoryRowsPerPage).SetMaxResults(categoryRowsPerPage);

            //count = session.Query<Product>().Count(p => p.Category.Id == category.Id);

            //return criteria.List<Product>();
            Product[] prods = ProductCache.FindByCategory(category);//.OrderBy(p => p.Position).ElementAt(currentPage * categoryRowsPerPage);
            prods.OrderBy(p => p.Position);
            count = ProductCache.FindByCategory(category).Length;
            return count > 0 ? prods.ToList().GetRange(currentPage * categoryRowsPerPage, categoryRowsPerPage): new List<Product>(); 

        }

        public static Category ParrentCategory(BalanceType baltype)
        {
            Category[] categories = FindByBalanceType(baltype);
            if (categories.Length == 0) return null;

            Category auxCat = categories[0];
            while (auxCat.Parent.Parent != null)
            {
                auxCat = auxCat.Parent;
            }

            return auxCat;
        }

        public static Category CashAdjCategory( BalanceType baltype)
        {
            Category parrentCategory = ParrentCategory( baltype);
            return parrentCategory == null ? null :
                parrentCategory.Children.FirstOrDefault(cat => cat.Type == CategoryType.CashAdj);
        }

  

        public static Category[] FindAllWithChildren()
        {
            //return session.Query<Category>().Fetch(c => c.Children).ToArray();
            return categories.FindAll(c=> c.Children.Count>0).ToArray();
        }

        public static string[] GetAllAnalysisCategories()
        {
            //if (nullableSession == null)
            //{
            //    using (ISession sessionInternal = SessionManager.OpenSession())
            //    {
            //        return sessionInternal.Query<Category>().
            //            Select(p => p.AnalysisCategory).
            //            Distinct().
            //            ToArray();
            //    }
            //}
            //else
            //{
            //    return nullableSession.Query<Category>().
            //            Select(p => p.AnalysisCategory).
            //            Distinct().
            //            ToArray();
            //}
            return categories.Select(c => c.AnalysisCategory).ToArray();
        }

        public static Category Find(int id)
        {
            return categories.Find(c=> c.Id==id);
        }
    }
}
