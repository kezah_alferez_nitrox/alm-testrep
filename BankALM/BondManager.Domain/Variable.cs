using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using BankALM.Infrastructure.Data;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Linq;

namespace BondManager.Domain
{
    // [ActiveRecord(Lazy = false)]
    [Serializable]
    [XmlRoot]
    public class Variable : EntityBase<Variable, int>
    {
        private string name;
        private DateTime? creationDate;
        private bool selected;
        private VariableType variableType;
        private Variable self;
        private IList<VariableValue> variableValues = new List<VariableValue>();


        // [Property(Unique = true)]
        [XmlElement]
        public virtual string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }

        //[Property]
        [XmlElement]
        public virtual DateTime? CreationDate
        {
            get { return this.creationDate; }
            set { this.creationDate = value; }
        }

        //[Property]
        [XmlElement]
        public virtual bool Selected
        {
            get { return this.selected; }
            set { this.selected = value; }
        }

        //[Property]
        [XmlElement]
        public virtual VariableType VariableType
        {
            get { return this.variableType; }
            set { this.variableType = value; }
        }

        [XmlIgnore]
        public virtual Variable Self
        {
            get { return this; }
        }


        //[HasMany(Lazy = true, Inverse = true, Cascade = ManyRelationCascadeEnum.AllDeleteOrphan, BatchSize = 10)]
        [XmlIgnore]
        public virtual IList<VariableValue> VariableValues
        {
            get { return this.variableValues; }
            set { this.variableValues = value; }
        }

        public override string ToString()
        {
            return this.Name;
        }

        public static Variable[] FindAvailable(ISession session)
        {
            return session.Query<Variable>().Where(s => !s.Selected).ToArray();
            //return FindAll(session, Expression.Eq("Selected", false));
        }

        public static Variable[] FindSelected(ISession session)
        {
            return session.Query<Variable>().Where(s => s.Selected).ToArray();
            //return FindAll(session, Expression.Eq("Selected", true));
        }

        public static Variable FindByName(ISession session, string _name)
        {
            return session.Query<Variable>().FirstOrDefault(s => s.Name == _name);
            //return FindFirst(session, Expression.Eq("Name", _name));
        }

        public static Variable[] FindNames(ISession session, string[] names)
        {
            return FindAll(session, Expression.In("Name", names));
        }

        public static bool ExistsByName(ISession session, string variableName)
        {
            return session.Query<Variable>().Where(v => v.Name == variableName).Count() > 0;
            //return Exists(session, "Name = ?", variableName);
        }

        public static bool ExistById(ISession session, int id)
        {
            return session.Query<Variable>().Where(v => v.Id == id).Count() > 0;
            //return Exists(session, "Id = ?", id);
        }
        
    }


    public enum VariableType
    {
        Number,
        Vector,
        Formula,
        Array
    }

}