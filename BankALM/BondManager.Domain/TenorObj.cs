﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ALMSolutions
{
    public class TenorObj
    {
        public Tenor? tenor { get; set; }

        public int frequency { get; set; }

        public TenorObj(Tenor? tenor, int frequency)
        {
            this.tenor = tenor;
            this.frequency = frequency;
        }
    }

    public enum Tenor
    {
        D,
        BD,
        W,
        M,
        Q,
        S,
        Y,
        A
    }
}
