﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ALMS.Products;

namespace BondManager.Domain
{
    public static class ProductCache
    {
        public static List<Product> products = new List<Product>();

        // optimisation: copy BalanceType and IsRollAccount properties to Product
        public static int GetMaxAlmIdForCategory(Category category)
        {
            if (category == null || category.Type == CategoryType.Roll)
            {
                List<Product> a = products.FindAll(p => p.Attrib == ProductAttrib.Roll);
                if (a.Count() == 0)
                {
                    return 0;
                }
                return a.Max(p => (int?)p.ALMID).GetValueOrDefault(0);
            }
            else
            {
                return products.FindAll(p => p.Category.BalanceType == category.BalanceType).
                    Max(p => (int?)p.ALMID).GetValueOrDefault(0);
            }
        }

        public static Product FindFirstCashAdjByBalanceTypeAndAttrib(BalanceType balanceType, ProductAttrib attrib)
        {
            try
            {
                return products.Find(p => p.Category.BalanceType.Equals(balanceType) && p.Attrib.Equals(attrib));
            }
            catch (Exception e)
            {
                return null;
            }

        }

        public static Product[] GetBondsByTypeAndAtrib(BalanceType balanceType, ProductAttrib productAtrib)
        {
            return products.FindAll(p => p.Category.BalanceType == balanceType && p.Attrib == productAtrib).
                    Distinct().ToArray();
        }

        public static Product[] FindByCategory(Category category)
        {
            return products.FindAll(p => p.Category == category).ToArray();
        }

        public static Product FindById(int bondId)
        {
            return products.Find(p => p.Id == bondId);
        }

        public static Product[] FindIsModel()
        {
            return products.FindAll(p => p.IsModel).ToArray();
        }

        public static Product[] FindAllNotExcluded()
        {
            return products.FindAll(p => !p.Exclude).ToArray();
        }

        public static Product[] FindAllNotExcludedNotNotionalNotInFuture()
        {
            return products.FindAll(p => !p.Exclude && p.Attrib != ProductAttrib.Notional && p.Attrib != ProductAttrib.InFuture).ToArray();
        }


        public static Product[] FindByAttrib(ProductAttrib attrib)
        {
            return products.FindAll(p => p.Attrib == attrib).ToArray();
        }
        //public static Product[] FindNonSwaptionsWithLinkedProducts()
        //{
        //    return session.Query<Product>().Where(p => p.LinkedProducts.Count>0 && p.ProductType!=ProductType.Swaption).ToArray();
        //}


        //public static Product TryFindByAlmId( string almId)
        //{
        //    return FindFirst(session, Restrictions.Eq("ALMID", almId));
        //}



        public static Product[] FindHavingInvestmentParameter()
        {
            return products.FindAll(p => !p.InvestmentParameter.Equals(null) && !p.InvestmentParameter.Equals("")).ToArray();
            //return FindAll(session, Restrictions.And(
            //    Restrictions.IsNotNull("InvestmentParameter"),
            //    Restrictions.Not(Restrictions.Eq("InvestmentParameter", "")
            //    )));
        }

        public static int GetMaxPosition(Category category)
        {
            int? max = products.FindAll(p => p.Category.Id == category.Id).Max(p => (int?)p.Position);

            return max.GetValueOrDefault(0);
        }

        public static System.Tuple<int, int, double>[] GetBookValueByCategoryAndCurrency(Currency valuationCurrency)
        {
            IList<System.Tuple<int, int, double>> result = new List<System.Tuple<int, int, double>>();


            //            const string sql =
            //                 @"SELECT
            //                    Category_id, 
            //                    Currency_id,    
            //                    SUM(BookValue) AS sum
            //                FROM Product
            //                WHERE (Attrib <> 'Notional') AND (Attrib <> 'InFuture') AND (Exclude = 0)
            //                GROUP BY Category_id, Currency_id";

            //            IQuery sqlQuery = session.CreateSQLQuery(sql);
            foreach (Product prod in products.FindAll(p => !p.Attrib.Equals("Notional") && !p.Attrib.Equals("InFuture") && !p.Exclude))  //    foreach (object[] row in sqlQuery.List())
            {
                int categoryId = prod.Category.Id;
                int currencyId = prod.Currency == null ? valuationCurrency.Id : prod.Currency.Id;
                double bookValue = prod.BookValue == null ? 0 : (double)prod.BookValue;

                result.Add(new System.Tuple<int, int, double>(categoryId, currencyId, bookValue));
            }
            //IList<Product> products = Product.FindAllNotExcludedNotNotionalNotInFuture(session);
            //foreach (Product product in products)
            //{
            //    if (result.Any(t => t.Item1==product.Category.Id && )
            //}

            return result.ToArray();
        }

        public static Product FindLikeALMID(int id)
        {
            //            string sql =
            //                @"SELECT
            //                            id
            //                        FROM Product
            //                        WHERE CONVERT(nvarchar(10), ALMID) like '%" + id + "%'";

            //            IQuery sqlQuery = session.CreateSQLQuery(sql);
            //            IList<int> list = sqlQuery.List<int>();

            //            if (list.Count > 0) return Product.Find(session, list[0]);

            //            return null;
            return products.Find(p => p.ALMID.ToString().Contains(id.ToString()));
        }

        public static void set(Product[] prods)
        {
            products = prods.ToList();
        }
    }
}
