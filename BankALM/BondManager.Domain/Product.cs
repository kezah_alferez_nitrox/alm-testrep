using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Xml.Serialization;
using ALMS.Products;
using ALMSCommon;
using BankALM.Infrastructure.Data;
using Iesi.Collections.Generic;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Linq;

namespace BondManager.Domain
{
    [Serializable]
    [XmlRoot("Bond")]
    public class Product : EntityBase<Product, int>, IProduct
    {

        public enum BondStatus
        {
            None = 0,
            OK,
            Warning,
            Error
        }

        public const string NoRoll = "No Roll";
        private BondSwapLeg? swapLeg;
        private int almId;

        private ProductAccounting accounting;
        private ProductAttrib attrib;
        private string bookPrice = "100";
        private double? bookValue;
        private Category category;
        private string cdr;
        private string coupon;
        private string cpr;
        //private bool originBasedOnStartDate;
        private string currFace;
        private Currency currency;
        private string description;
        private double? duration;
        private DateTime? expirationDate;
        private string investmentParameter;
        private int position;
        private bool isModel;
        private bool isTotal;
        private bool? isVisibleChecked;
        private string lgd;
        private ProductFrequency frequency;
        private string rollSpec;
        private double? rwa;
        private string secId;
        private BondStatus status;
        private ProductTax tax;
        private string _irc;
        private string theoreticalCurrFace;
        private ProductAmortizationType amortizationType;
        private string applicationFee;
        private string prePayementFee;
        private string applicationCharge;
        private int lagNoAmortisationPeriodInMonths;

        public string ApplicationCharge
        {
            get { return applicationCharge; }
            set { applicationCharge = value; }
        }
        private string recoveryLag;
        public Product()
        {
            this.isTotal = false;
            this.accounting = ProductAccounting.LAndR;
            this.frequency = ProductFrequency.Quarterly;
            this.tax = ProductTax.Standard;
            this.attrib = ProductAttrib.InBalance;
            this.RollSpec = NoRoll;
            this.RollRatio = "1";
            this.Amortization = "0";
            this.LinkedProducts = new HashedSet<Product>();
        }

        public virtual double GetFace(DateTime date)
        {
            if (this.GetFaceFunction != null)
            {
                double face = this.GetFaceFunction(date);
                return face;
            }

            return 1.0;
        }

        [XmlIgnore]
        public virtual Func<DateTime, double> GetFaceFunction { get; set; }

        [XmlIgnore]
        public virtual string portfolio { get; set; }

        [XmlElement]
        public virtual string[] portfolios { get; set; }

        [XmlElement]
        public virtual int Position
        {
            get { return this.position; }
            set { this.position = value; }
        }


        [XmlElement]
        public virtual Category Category
        {
            get { return this.category; }
            set
            {
                this.category = value;
                this.cachedALMIDDisplayed = null;
            }
        }

        [XmlElement]
        public virtual bool Exclude { get; set; }


        [XmlElement]
        public virtual BondStatus Status
        {
            get { return this.status; }
            set { this.status = value; }
        }


        [XmlElement]
        public virtual string SecId
        {
            get { return this.secId; }
            set { this.secId = value; }
        }


        [XmlElement]
        public virtual string Description
        {
            get { return this.description; }
            set { this.description = value; }
        }

        [XmlElement]
        public virtual ProductAmortizationType AmortizationType
        {
            get { return this.amortizationType; }
            set { this.amortizationType = value; }
        }


        [XmlElement]
        public virtual string Amortization { get; set; }


        [XmlElement]
        public virtual double Complement { get; set; }
        //    get
        //    {
        //        double result=0;
        //        if (double.TryParse(ComplementString,out result))
        //        {
        //            return result;
        //        }
        //        return 0;
        //    }
        //}

        [XmlElement]
        public virtual string ComplementString { get; set; }


        [XmlElement("Growth")]
        public virtual string InvestmentParameter
        {
            get { return this.investmentParameter; }
            set { this.investmentParameter = value; }
        }


        [XmlElement]
        public virtual ProductAccounting Accounting
        {
            get { return this.accounting; }
            set
            {
                this.accounting = value;

                if (this.OtherSwapLeg != null && this.OtherSwapLeg.Accounting != this.accounting) this.OtherSwapLeg.Accounting = this.accounting;

            }
        }


        [XmlElement("CurrFace")]
        public virtual string OrigFace
        {
            get { return this.currFace; }
            set
            {
                this.currFace = value;
                if (this.OtherSwapLeg != null && this.OtherSwapLeg.OrigFace != this.OrigFace) this.OtherSwapLeg.OrigFace = this.OrigFace;
            }
        }


        [XmlElement]
        public virtual string BookPrice
        {
            get { return this.bookPrice; }
            set
            {
                this.bookPrice = value;
            }
        }


        [XmlElement]
        public virtual string ActuarialPrice { get; set; }

        [XmlElement("MarketPrice")]
        public virtual string OldMarketPrice
        {
            get { return null; }
            set
            {
                if (!String.IsNullOrEmpty(value))
                {
                    this.bookPrice = value;
                }
            }
        }

        public virtual double BookPriceValue { get; set; }


        [XmlElement]
        public virtual double? BookValue
        {
            get { return this.bookValue; }
            set { this.bookValue = value; }
        }


        [XmlElement]
        public virtual int ALMID
        {
            get { return this.almId; }
            set
            {
                this.almId = value;
                this.cachedALMIDDisplayed = null;
            }
        }

        private string cachedALMIDDisplayed;
        public virtual string ALMIDDisplayed
        {
            get
            {
                this.cachedALMIDDisplayed = this.cachedALMIDDisplayed ?? GetAlmidDisplayed(this.category == null ? "" : this.category.Code, this.almId);
                return this.cachedALMIDDisplayed;
            }
            set { this.cachedALMIDDisplayed = value; }
        }

        public static string GetAlmidDisplayed(string categoryCode, int almId)
        {
            if (String.IsNullOrEmpty(categoryCode)) return "";
            return String.Format("{0}{1:0000#}", categoryCode, almId);
        }


        [XmlElement]
        public virtual string RollSpec
        {
            get { return this.rollSpec; }
            set { this.rollSpec = value; }
        }


        [XmlElement]
        public virtual string RollRatio { get; set; }

        private DateTime? startDate;


        [XmlElement]
        public virtual DateTime? StartDate
        {
            get { return this.startDate; }
            set
            {
                DateTime minDate = SqlDateTime.MinValue.Value;
                if (value != null && value < minDate) return;
                this.startDate = value;
            }
        }


        [XmlElement]
        public virtual string Coupon
        {
            get { return this.coupon; }
            set { this.coupon = value; }
        }


        [XmlElement]
        public virtual CouponType CouponType { get; set; }


        [XmlElement]
        public virtual CouponCalcType CouponCalcType { get; set; }


        [XmlElement]
        public virtual ProductAttrib Attrib
        {
            get { return this.attrib; }
            set { this.attrib = value; }
        }


        [XmlElement]
        public virtual bool IsModel
        {
            get { return this.isModel; }
            set { this.isModel = value; }
        }

        public bool IsModelAndNotDerivative
        {
            get { return this.isModel && !this.IsDerivative; }
        }

        public virtual bool IsMargin
        {
            get { return IsModel; }
        }


        [XmlElement]
        public virtual string CDR
        {
            get { return this.cdr; }
            set { this.cdr = value; }
        }


        [XmlElement]
        public virtual string CPR
        {
            get { return this.cpr; }
            set { this.cpr = value; }
        }

        //[XmlElement]
        //public virtual bool OriginBasedOnStartDate
        //{
        //    get { return this.originBasedOnStartDate; }
        //    set { this.originBasedOnStartDate = value; }
        //}

        [XmlElement]
        public virtual string TheoreticalCurrFace
        {
            get { return this.theoreticalCurrFace; }
            set { this.theoreticalCurrFace = value; }
        }


        [XmlElement]
        public virtual string LGD
        {
            get { return this.lgd; }
            set { this.lgd = value; }
        }


        [XmlElement("NewBasel2")]
        public virtual string Basel2 { get; set; }


        [XmlElement("Period")]
        public virtual ProductFrequency Frequency
        {
            get { return this.frequency; }
            set { this.frequency = value; }
        }


        [XmlElement]
        public virtual double? Duration
        {
            get
            {
                return this.duration;
            }
            set
            {
                this.duration = value;

                if (this.OtherSwapLeg != null && this.OtherSwapLeg.Duration != this.duration) this.OtherSwapLeg.Duration = this.duration;
            }
        }

        public DateTime GetExpiryDate(DateTime valuationDate)
        {
            if (this.expirationDate != null)
                return expirationDate.GetValueOrDefault();

            DateTime realStart = this.StartDate.GetValueOrDefault(valuationDate);

            realStart = realStart.AddMonths(IntDuration);
            if (this.startDate == null)
                realStart = realStart.AddDays(-realStart.Day);

            return realStart;

        }

        public int IntDuration
        {
            get
            {
                int intDuration = (int)this.Duration.GetValueOrDefault();
                //if (this.Attrib == ProductAttrib.InFuture) intDuration++;
                //        //MTU : the integer duration for INFuture is +1 as the first month there are Run (like in opening balance)
                return intDuration;
            }
            set { Duration = value; }
        }


        [XmlElement]
        public virtual ProductTax Tax
        {
            get { return this.tax; }
            set { this.tax = value; }
        }


        [XmlElement]
        public virtual string IRC
        {
            get { return this._irc; }
            set { this._irc = value; }
        }

        [XmlElement]
        public virtual bool? IsVisibleChecked
        {
            get { return this.isVisibleChecked; }
            set { this.isVisibleChecked = value; }
        }

        ICurrency IProduct.Currency
        {
            get { return this.currency; }
        }


        [XmlElement]
        public virtual Currency Currency
        {
            get { return this.currency; }
            set { this.currency = value; }
        }


        [XmlElement]
        public virtual CouponBasis CouponBasis { get; set; }


        [XmlElement]
        public virtual ProductInvestmentRule InvestmentRule { get; set; }


        [XmlElement("BondFinancialType")]
        public virtual ProductType ProductType { get; set; }

        [XmlIgnore]
        public virtual Product OtherSwapLeg { get; set; }

        [XmlElement]
        public virtual int OtherSwapLegId { get; set; }

        [XmlElement]
        public virtual string OtherSwapLegAlmId { get; set; }


        [XmlElement]
        public virtual BondSwapLeg? SwapLeg
        {
            get { return this.swapLeg; }
            set
            {
                if (this.swapLeg != value)
                {
                    this.swapLeg = value;
                    this.SetBondSwapLegToLink();
                }
            }
        }

        public virtual bool IsCashflowProduct
        {
            get
            {
                return this.ProductType != ProductType.Swaption;
            }
        }

        public virtual bool IsDerivative
        {
            get
            {
                return this.ProductType == ProductType.Swap || this.ProductType == ProductType.Cap || this.ProductType == ProductType.Floor;
            }
        }

        private string spread;

        [XmlElement]
        public virtual string Spread
        {
            get
            {
                if (this.ProductType != ProductType.BondFr)
                    return spread;
                else
                    return "0";
            }
            set { spread = value; }
        }


        [XmlElement]
        public virtual string MarketBasis { get; set; }

        public virtual double? RWA
        {
            get
            {
                if (this.IsTotal)
                {
                    return this.rwa;
                }

                if (this.ProductType == ProductType.StockEquity)
                {
                    return this.BookValue * this.Basel2Value;
                }

                return this.OrigFaceValue * this.Basel2Value;
            }
            set { this.rwa = value; }
        }


        [XmlElement]
        public virtual double? FirstRate { get; set; }

        [XmlElement]
        //[System.Obsolete("use FirstRate")]
        public virtual double? FirstCoupon { get; set; }


        [XmlElement]
        public virtual LiquidityEligibility LiquidityEligibility { get; set; }

        private double liquidityHaircut;


        [XmlElement]
        public virtual double LiquidityHaircut
        {
            get { return this.liquidityHaircut; }
            set
            {
                if (value < 0 || value > 1) throw new ArgumentOutOfRangeException("LiquidityHaircut");
                this.liquidityHaircut = value;
            }
        }

        private double liquidityRunOff;


        [XmlElement]
        public virtual double LiquidityRunOff
        {
            get { return this.liquidityRunOff; }
            set
            {
                if (value < 0 || value > 1) throw new ArgumentOutOfRangeException("LiquidityRunOff");
                this.liquidityRunOff = value;
            }
        }

        public virtual bool IsTotal
        {
            get { return this.isTotal; }
            set { this.isTotal = value; }
        }

        [XmlIgnore]
        public virtual Iesi.Collections.Generic.ISet<Product> LinkedProducts { get; set; }

        public virtual int[] LinkedProductsIds { get; set; }

        [XmlIgnore]
        public virtual string LinkedProductsAlmIds { get; set; }

        [XmlElement]
        public virtual string[] LinkedProductsAlm { get; set; }

        double? IProduct.FirstRate
        {
            get { return this.FirstRate; }
        }

        DateTime? IProduct.IssueDate
        {
            get { return null; }
        }

        public override string ToString()
        {
            //return this.Attrib + "<" + this.ALMIDDisplayed + "> " + this.Description;
            return this.ALMIDDisplayed;
        }

        public virtual bool HasRoll
        {
            get { return this.RollSpec != NoRoll; }
        }

        public virtual double OrigFaceValue
        {
            get
            {
                double? value = ParsingTools.ClipboardStringToDouble(this.OrigFace);
                if (value.HasValue) return value.Value;
                if (this.OrigFace == null) return 0;

                Vector vector;
                vector = VectorCache.FindByName(this.OrigFace);

                if (vector == null || vector.VectorPoints.Count == 0) return 0;

                return vector.VectorPoints[0].Value;
            }
        }

        public virtual double Basel2Value
        {
            get
            {
                if (String.IsNullOrEmpty(this.Basel2)) return 0;

                bool percent = false;
                string toParse = this.Basel2;
                if (this.Basel2.Contains("%"))
                {
                    percent = true;
                    toParse = toParse.Replace("%", "").Trim();
                }

                double? basel2 = ParsingTools.ClipboardStringToDouble(toParse);
                if (!basel2.HasValue) basel2 = 0;
                return basel2.Value * (percent ? 0.01 : 1);
            }
        }



        private void SetBondSwapLegToLink()
        {
            if (this.OtherSwapLeg == null) return;
            switch (this.SwapLeg)
            {
                case BondSwapLeg.Paid:
                    this.OtherSwapLeg.SwapLeg = BondSwapLeg.Received;
                    break;
                case BondSwapLeg.Received:
                    this.OtherSwapLeg.SwapLeg = BondSwapLeg.Paid;
                    break;
            }
        }

        private double? ComputeBookValueInternal(double bookPriceValue)
        {
            double? ret;
            try
            {
                double bp = bookPriceValue;
                if (this.ProductType != ProductType.StockEquity) bp /= 100;

                ret = this.OrigFaceValue * bp;
            }
            catch (OverflowException)
            {
                ret = null;
            }
            return ret;
        }


        public virtual void UpdateStatusProperty()
        {
            Status = BondStatus.OK;
            if (String.IsNullOrEmpty(this.OrigFace) || String.IsNullOrEmpty(this.BookPrice))
            {
                this.Status = this.BookValue.HasValue ? BondStatus.Warning : BondStatus.Error;
            }
            else
            {
                if (this.BookValue.HasValue)
                {
                    this.Status = (this.BookValue.Value == this.ComputeBookValueInternal(this.BookPriceValue))
                                      ? BondStatus.OK
                                      : BondStatus.Warning;
                }
                else
                {
                    this.Status = BondStatus.Error;
                }
            }
            if ((this.ProductType == ProductType.Swap || ProductType == ProductType.Swaption) && this.OtherSwapLeg == null)
                Status = BondStatus.Error;
        }

        public virtual bool IsReceived
        {
            get
            {
                return this.SwapLeg == null || this.SwapLeg == BondSwapLeg.Received;
            }
        }

        public virtual bool UsesPricer
        {
            get
            {
                double dummy;
                bool isVarBookPrice = !String.IsNullOrEmpty(this.BookPrice) && !Double.TryParse(this.BookPrice, out dummy);

                return !isVarBookPrice && this.Accounting == ProductAccounting.AFS || this.Accounting == ProductAccounting.HFT;
            }
        }

        public virtual bool IsVariableRate
        {
            get
            {
                return this.GetIsVariableRate(this.ProductType, this.CouponType);
            }
        }

        public virtual int BalanceSign
        {
            get
            {
                return ((this.Category.BalanceType == BalanceType.Asset || this.Category.BalanceType == BalanceType.Derivatives) ? 1 : -1);
            }
        }

        public virtual int SwapLegSign
        {
            get
            {
                int sign = 1;
                switch (this.ProductType)
                {
                    case ProductType.Swap:
                    case ProductType.Cap:
                    case ProductType.Floor:
                        sign = this.SwapLeg == BondSwapLeg.Paid ? -1 : 1;
                        break;
                }
                return sign;
            }
        }

        public virtual int Sign
        {
            get { return this.BalanceSign; }
        }

        public virtual bool GetIsVariableRate(ProductType productType, CouponType couponType)
        {
            switch (productType)
            {
                case ProductType.Bond:
                case ProductType.BondFr:
                case ProductType.StockEquity:
                case ProductType.Custom:
                case ProductType.Swaption: // todo CDU : not sure
                    return false;
                case ProductType.BondVr:
                case ProductType.Cap:
                case ProductType.Floor:
                case ProductType.Amortizing:
                    return true;
                case ProductType.Swap:
                    return couponType != CouponType.Fixed;
                default:
                    throw new ArgumentOutOfRangeException("productType");
            }
        }

        public virtual bool HasBookValue()
        {
            return !this.Exclude && !this.IsTotal && this.Attrib != ProductAttrib.Notional;
        }

        public virtual void ComputeBookValue()
        {
            this.ComputeBookValue(this.BookPriceValue);
        }

        public virtual void ComputeBookValue(double bp)
        {
            this.BookValue = this.ComputeBookValueInternal(bp);
        }

        public virtual void GenerateALMID()
        {
            this.almId = ProductCache.GetMaxAlmIdForCategory(this.Category) + 1;
        }

        public virtual bool IsCouponFormulaAllowed
        {
            get
            {
                bool isBookPriceFormula = false;
                if (!String.IsNullOrEmpty(this.BookPrice))
                {
                    string bp = this.BookPrice.Replace("%", "");
                    double temp;
                    isBookPriceFormula = !Double.TryParse(bp, out temp);
                }

                return (this.Accounting == ProductAccounting.HTM ||
                this.Accounting == ProductAccounting.LAndR ||
                isBookPriceFormula) &&
                this.AmortizationType != ProductAmortizationType.CST_PAY;
            }
        }


        [XmlElement]
        public string RecoveryLag
        {
            get { return this.recoveryLag; }
            set { this.recoveryLag = value; }
        }

        [XmlElement]
        public string ApplicationFee
        {
            get { return this.applicationFee; }
            set { this.applicationFee = value; }
        }
        [XmlElement]
        public string PrePayementFee
        {
            get { return this.prePayementFee; }
            set { this.prePayementFee = value; }
        }

        public int LagNoAmortisationPeriodInMonths
        {
            get { return lagNoAmortisationPeriodInMonths; }
            set
            {
                lagNoAmortisationPeriodInMonths = value;
                //if (this.Attrib == ProductAttrib.InFuture)
                //    lagNoAmortisationPeriodInMonths++;
            }
        }


        public DateTime? ExpirationDate
        {
            get { return expirationDate; }
            set
            {
                DateTime minDate = SqlDateTime.MinValue.Value;
                if (value != null && value < minDate) return;
                this.expirationDate = value;
            }
        }

        #region Nested type: Basel2Types

        public static class Basel2Types
        {
            public static string CoreEquity = "core equity";
            public static string OtherTiers1 = "other tiers 1";
            public static string Tiers2 = "tiers 2";
        }

        #endregion

        public static bool IsRollOf(string rollSpec, int rollAlmId)
        {
            if (String.IsNullOrEmpty(rollSpec)) return false;
            if (rollSpec == NoRoll) return false;
            if (rollSpec.Length <= 1) return false;

            string rollSpecString = rollSpec.Substring(1);
            int rollSpecInt;
            if (Int32.TryParse(rollSpecString, out rollSpecInt) && rollSpecInt == rollAlmId)
            {
                return true;
            }

            return false;
        }





        public void IntegrityCheck()
        {
            //MTU20120508
            if (this.Duration == null ||
                this.Duration < 0)
            {
                this.Duration = 0;
            }
        }

        public void LoadLinkedProducts()
        {
            NHibernateUtil.Initialize(this.LinkedProducts);
        }



        public int ComputeStartStep(DateTime valuationDate, bool isDaily)
        {
            int startStep;
            /*if ((product.Attrib == ProductAttrib.InFuture) ||
                    (product.Attrib == ProductAttrib.InBalance && (int)Math.Round((startDate - this.valuationDate).TotalDays / 30.0) > 0))*/
            if (Attrib == ProductAttrib.InFuture && startDate != null)
            {
                //startStep = (int) Math.Round((startDate.GetValueOrDefault().AddDays(-1) - valuationDate).TotalDays/30.0);// +1;
                DateTime realStartDate = startDate.GetValueOrDefault();//.AddDays(-1);
                startStep = PricerUtil.DateDiffInSteps(realStartDate, valuationDate, isDaily) + 1;
            }
            else
            {
                startStep = 0;
            }
            return startStep;
        }

        public bool isRollOrInFutureOrTreasury()
        {
            return this.Category.Type == CategoryType.Treasury || isRollOrInFuture();
        }
        public bool isRollOrInFuture()
        {
            return isRoll() || this.Attrib == ProductAttrib.InFuture;
        }
        public bool isRoll()
        {
            return this.Category.Type == CategoryType.Roll;
        }

        public void FixBookValue()
        {
            BookPriceValue = ParsingTools.StringToDouble(BookPrice).GetValueOrDefault();
            ComputeBookValue();
        }

        public bool hasStartDateInPast(DateTime valuationDate)
        {
            return this.startDate != null && this.startDate < valuationDate;
        }

        public int SignIfDerivative
        {
            get { return (this.IsDerivative ? this.SwapLegSign : 1); }
        }

    }
}