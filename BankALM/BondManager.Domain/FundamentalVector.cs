using System;
using System.Linq;
using System.Xml.Serialization;
using ALMS.Products;
using BankALM.Infrastructure.Data;
using NHibernate;
using NHibernate.Linq;

namespace BondManager.Domain
{
    // [ActiveRecord(Lazy = false)]
    [Serializable]
    [XmlRoot]
    public class FundamentalVector : EntityBase<FundamentalVector, int>
    {
        //[Property]
        public virtual string Name { get; set; }

        //[Property]
        public virtual int Position { get; set; }

        //[Property]
        public virtual int Delta { get; set; }

        //[Property]
        public virtual TimeUnit Unit { get; set; }

        //[Property]
        public virtual FundamentalVectorType Type { get; set; }

        //[Property]
        public virtual CouponBasis DateConvention { get; set; }

        //[Property]
        public virtual ProductFrequency Frequency { get; set; }

        //[Property]
        public virtual string Volatility { get; set; }

        //[BelongsTo]
        public virtual Variable Variable { get; set; }

        //[BelongsTo]
        public virtual Currency Currency { get; set; }

        public virtual string VariableName
        {
            get { return this.Variable.Name; }
        }

        public new static FundamentalVector[] FindAll(ISession session)
        {
            //return FindAll(session, Order.Asc("Position"));
            return session.Query<FundamentalVector>().OrderBy(fv => fv.Position).ToArray();
        }

        public override string ToString()
        {
            return this.Name;
        }
        
    }




}