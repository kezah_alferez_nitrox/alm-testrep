using System;
using System.Linq;
using System.Xml.Serialization;
using BankALM.Infrastructure.Data;
using Eu.AlterSystems.ASNetLib.Core;
using NHibernate;
using NHibernate.Linq;

namespace BondManager.Domain
{
    // [ActiveRecord(Lazy = false)]
    [Serializable]
    [XmlRoot]
    public class VolatilityConfig : EntityBase<VolatilityConfig, int>
    {
        private bool selected;
        private double volatility;
        private string mean;
        private SimulationModes simulationMode;
        private bool isLog;
        private double halfLife;
        private Variable variable;
        private Scenario scenario;
        private string variableName;


        public VolatilityConfig()
        {
        }

        public VolatilityConfig(bool selected, double volatility, string mean, Variable variable, Scenario scenario)
        {
            this.Selected = selected;
            this.Volatility = volatility;
            this.Mean = mean;
            this.Variable = variable;
            this.Scenario = scenario;
        }

        // [Property]
        [XmlElement]
        public virtual bool Selected
        {
            get { return this.selected; }
            set { this.selected = value; }
        }

        //[Property]
        [XmlElement]
        public virtual double Volatility
        {
            get { return this.volatility; }
            set { this.volatility = value; }
        }

        //[Property]
        [XmlElement]
        public virtual string Mean
        {
            get { return this.mean; }
            set { this.mean = value; }
        }

        //[Property]
        [XmlElement]
        public virtual SimulationModes SimulationMode
        {
            get { return this.simulationMode; }
            set { this.simulationMode = value; }
        }

        //[Property]
        [XmlElement]
        public virtual bool IsLog
        {
            get { return this.isLog; }
            set { this.isLog = value; }
        }

        //[Property]
        [XmlElement]
        public virtual double HalfLife
        {
            get { return this.halfLife; }
            set { this.halfLife = value; }
        }

        //[BelongsTo]
        [XmlElement]
        public virtual Variable Variable
        {
            get { return this.variable; }
            set { this.variable = value; }
        }

        //[BelongsTo]
        [XmlElement]
        public virtual Scenario Scenario
        {
            get { return this.scenario; }
            set { this.scenario = value; }
        }


        [XmlIgnore]
        public virtual string VariableName
        {
            get { return variable.Name; }
        }


        public new static VolatilityConfig[] FindAll(ISession session)
        {
            return session.Query<VolatilityConfig>().OrderBy(s => s.Id).ToArray();
            //return FindAll(session, Order.Asc("Id"));
        }

        public enum SimulationModes
        {
            [EnumDescription("Mean")]
            Mean,
            [EnumDescription("Trend")]
            Trend,
            [EnumDescription("Mean-reversion")]
            MeanReversion
        }        
    }
}