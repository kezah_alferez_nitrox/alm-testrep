﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ALMSolutions;

namespace ObjectModelValidation.BaseObjects
{
    class Schedule
    {

        public static DateTime nextStep(DateTime start, DateTime end, int step, DateTime fy)
        {
            DateTime prevFy = fy.AddYears(-1);
            DateTime temp = prevFy.AddMonths(step); //set temp as previous fy + step

            while (temp <= start) //loop until temp is greater than start
            {
                temp = temp.AddMonths(step);
            }

            if (temp == end) //if temp is equal to end
                return temp;
            else if (fy > end) //if start of fiscal year is after end date
            {
                if (temp > end) //if temp+ step is after end return end date
                    return end;
            }
            else if (temp > fy) // if start of fiscal year is before end date. if temp+step is after fy return fy
                return fy;

            return new DateTime(temp.Year, temp.Month, DateTime.DaysInMonth(temp.Year, temp.Month));
        }

        public static DateTime prevStep(DateTime stepDate, DateTime end, DateTime fy, int step)
        {
            if (stepDate == end) //if stepdate is equal to the end date
            {
                if (end < fy) //if end date is before fy
                {
                    DateTime temp = fy.AddMonths(step * -1); //set temp as fy - step
                    while (temp > end) //loop temp until it is before end
                    {
                        temp = temp.AddMonths(step * -1);
                    }
                    return new DateTime(temp.Year, temp.Month, DateTime.DaysInMonth(temp.Year, temp.Month));
                }

            }
            //else just do normal stepdate - step
            DateTime temp2 = stepDate.AddMonths(step * -1);
            return new DateTime(temp2.Year, temp2.Month, DateTime.DaysInMonth(temp2.Year, temp2.Month));

        }

        public static DateTime addDate(DateTime date, TenorObj tenor, DateTime nextStep, Rule rule)
        {
            DateTime nextDate = getNextDate(date, tenor); //get next date

            if (rule.Equals(Rule.EOM)) //end of month
                nextDate = new DateTime(nextDate.Year, nextDate.Month, DateTime.DaysInMonth(nextDate.Year, nextDate.Month));
            else if (rule.Equals(Rule.Preceding))//go backward one business dayday 
            {
                while (nextDate.DayOfWeek == DayOfWeek.Saturday || nextDate.DayOfWeek == DayOfWeek.Sunday)
                {
                    nextDate = nextDate.AddDays(-1);
                }
            }
            else if (rule.Equals(Rule.Following)) //go forward one business day
            {
                while (nextDate.DayOfWeek == DayOfWeek.Saturday || nextDate.DayOfWeek == DayOfWeek.Sunday)
                {
                    nextDate = nextDate.AddDays(1);
                }
            }
            else if (rule.Equals(Rule.ModifiedFollowing))
            {
                if (nextDate.AddDays(1).Month == nextDate.Month) //if next day is still same month, following
                    nextDate = nextDate.AddDays(1);
                else //if next month, preceding
                    nextDate = nextDate.AddDays(-1);
            }

            if (nextDate > nextStep) //if more than step date, return step date
                return nextStep;

            return nextDate;
        }

        private static DateTime getNextDate(DateTime date, TenorObj tenor)
        {
            if (tenor.tenor != null)
            {
                if (tenor.tenor == Tenor.D) //day
                    return dailyFrequency(date, tenor.frequency);
                else if (tenor.tenor == Tenor.BD) //business day
                    return businessDayFrequency(date, tenor.frequency);
                else if (tenor.tenor == Tenor.W) //week
                    return dailyFrequency(date, 7 * tenor.frequency);
                else if (tenor.tenor == Tenor.Q) //quarter
                    return monthlyFrequency(date, 3 * tenor.frequency);
                else if (tenor.tenor == Tenor.S) //semi annual
                    return monthlyFrequency(date, 6 * tenor.frequency);
                else if (tenor.tenor == Tenor.Y || tenor.tenor == Tenor.A) //year or annual
                    return yearFrequency(date, tenor.frequency);
                else //month
                    return monthlyFrequency(date, tenor.frequency);
            }
            else
            { //if enum tenor is empty, set as monthly
                return monthlyFrequency(date, tenor.frequency);
            }
        }

        private static DateTime monthlyFrequency(DateTime date, int frequency)
        {
            return date.AddMonths(frequency);
        }

        private static DateTime dailyFrequency(DateTime date, int frequency)
        {
            return date.AddDays(frequency);
        }

        private static DateTime yearFrequency(DateTime date, int frequency)
        {
            return date.AddYears(frequency);
        }

        private static DateTime businessDayFrequency(DateTime startDate, int days) //skip weekend and holidays
        {
            var sign = Math.Sign(days);
            var unsignedDays = Math.Abs(days);
            DateTime nextDate = startDate;
            for (var i = 0; i < unsignedDays; i++)
            {
                do
                {
                    nextDate = nextDate.AddDays(sign);
                }
                while (nextDate.DayOfWeek == DayOfWeek.Saturday ||
                    nextDate.DayOfWeek == DayOfWeek.Sunday);
            }

            return nextDate;

        }


    }

    public enum Rule
    {
        Preceding,
        Following,
        ModifiedFollowing,
        None,
        EOM
    }

}
