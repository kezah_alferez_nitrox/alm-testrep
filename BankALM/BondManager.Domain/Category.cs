using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using ALMS.Products;
using BankALM.Infrastructure.Data;
using Eu.AlterSystems.ASNetLib.Core;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Linq;

namespace BondManager.Domain
{
    // [ActiveRecord(Lazy = false)]
    [Serializable]
    [XmlRoot]
    public class Category : EntityBase<Category, int>
    {
        public static object locker = new object();

        private string label;
        private Category parent;
        private IList<Product> bonds = new List<Product>();
        private IList<Category> children = new List<Category>();
        private BalanceType balanceType;
        private CategoryType type = CategoryType.Normal;
        private bool readOnly;
        private bool? visible;
        private bool? isVisibleChecked;
        private bool? isVisibleCheckedModel;
        private int position;
        private int almid;
        private string analysisCategory;
        public const string DEFAULT_ANALYSIS_CATEGORY = "[Default]";
        public const string TREASURIES_ANALYSIS_CATEGORY = "Treasuries";
        public const string DEFAULT_ROLL_LABEL = "Default";

        public bool CanRename()
        {
            return Type != CategoryType.Roll || Label != DEFAULT_ROLL_LABEL;
        }

        public int Sign {
            get { return this.BalanceType == BalanceType.Asset ? 1 : -1; } 
        }
        // [Property]
        [XmlElement]
        public virtual string Label
        {
            get { return this.label; }
            set { this.label = value; }
        }
        // [Property]
        [XmlElement]
        public virtual int ALMID
        {
            get { return this.almid; }
            set { this.almid = value; }
        }

        // [Property]
        [XmlElement]
        public virtual int Position
        {
            get { return this.position; }
            set { this.position = value; }
        }

        // [BelongsTo]
        [XmlElement]
        public virtual Category Parent
        {
            get { return this.parent; }
            set { this.parent = value; }
        }

        // [HasMany(Lazy = true, Inverse = true, Cascade = ManyRelationCascadeEnum.AllDeleteOrphan, BatchSize = 10, OrderBy = "Position, Attrib")]
        [XmlIgnore]
        public virtual IList<Product> Bonds
        {
            get { return this.bonds; }
            set { this.bonds = value; }
        }

        // [HasMany(Lazy = true, Inverse = true, Cascade = ManyRelationCascadeEnum.AllDeleteOrphan, BatchSize = 10, OrderBy = "Position, Type")]
        [XmlIgnore]
        public virtual IList<Category> Children
        {
            get { return this.children; }
            set { this.children = value; }
        }

        public virtual IEnumerable<Category> ChildrenOrdered
        {
            get
            {
                return this.children == null ? null :
                    this.children.OrderBy(c => c.Position).ThenBy(c => (int)c.Type);
            }
        }

        // [Property(Index = "IDX_BALANCE_TYPE")]
        [XmlElement]
        public virtual BalanceType BalanceType
        {
            get { return this.balanceType; }
            set { this.balanceType = value; }
        }

        // [Property]
        [XmlElement]
        public virtual CategoryType Type
        {
            get { return this.type; }
            set { this.type = value; }
        }

        // [Property]
        [XmlElement]
        public virtual bool ReadOnly
        {
            get { return this.readOnly; }
            set { this.readOnly = value; }
        }

        // [Property]
        [XmlElement]
        public virtual bool? isVisible
        {
            get { return this.visible; }
            set { this.visible = value; }
        }

        // [Property]
        [XmlElement]
        public virtual bool? IsVisibleCheckedModel
        {
            get { return this.isVisibleCheckedModel; }
            set { this.isVisibleCheckedModel = value; }
        }

        // [Property]
        [XmlElement]
        public virtual bool? IsVisibleChecked
        {
            get { return this.isVisibleChecked; }
            set { this.isVisibleChecked = value; }
        }

        [XmlIgnore]
        public virtual bool IsNew
        {
            get { return this.Id == default(int); }
        }

        public virtual string FullPath()
        {
            string path = this.Label;
            Category category = this;
            while (category.Parent != null && category.Parent.Parent != null)
            {
                category = category.Parent;
                path = category.Label + "/" + path;
            }
            return path;
        }

        public virtual bool Equals(Category other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return this.Id != 0 && other.Id == this.Id;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof(Category)) return false;
            return this.Equals((Category)obj);
        }

        public override int GetHashCode()
        {
            return this.Id;
        }

        public static bool operator ==(Category left, Category right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Category left, Category right)
        {
            return !Equals(left, right);
        }

        public static string BalanceTypeCode(BalanceType balanceType)
        {
            switch (balanceType)
            {
                case BalanceType.Portfolio:
                    return "";
                case BalanceType.Asset:
                    return "A";
                case BalanceType.Liability:
                    return "L";
                case BalanceType.Equity:
                    return "E";
                case BalanceType.Derivatives:
                case BalanceType.OffBalanceSheet:
                    return "D";
                default:
                    throw new ArgumentOutOfRangeException("balanceType");
            }
        }

        public virtual string Code
        {
            get
            {
                return (this.Type == CategoryType.Roll) ? "R" : BalanceTypeCode(this.BalanceType);
            }
        }

        public string AnalysisCategory
        {
            get { return analysisCategory??DEFAULT_ANALYSIS_CATEGORY; }
            set { analysisCategory = value; }
        }


        private static Category FindSpecialCategory(ISession session, BalanceType type)
        {
            return session.Query<Category>().FirstOrDefault(c => c.BalanceType == type);
        }
        public static Category FindBalanceSheet(ISession session)
        {
            return FindSpecialCategory(session, BalanceType.BalanceSheet);
        }
        public static Category FindOffBalanceSheet(ISession session)
        {
            return FindSpecialCategory(session, BalanceType.OffBalanceSheet);
        }
        public static Category FindPortfolio(ISession session)
        {
            return FindSpecialCategory(session, BalanceType.Portfolio);
        }


        public static Category FindNodeByName(ISession session, string lbl)
        {
            return session.Query<Category>().FirstOrDefault(c => c.Label == lbl);
            //return FindOne(nullableSession, Expression.Eq("Label", lbl));
        }

        public static Category[] FindByType(ISession session, CategoryType cattype)
        {
            return session.Query<Category>().Where(c => c.Type == cattype).ToArray();
            //return FindAllByProperty(nullableSession, "Type", cattype);
        }

        public static Category[] FindByBalanceType(ISession session, BalanceType baltype)
        {
            //Category[] findByBalanceType = FindAllByProperty(nullableSession, "BalanceType", baltype);
            Category[] categories = session.Query<Category>().Where(c => c.BalanceType == baltype).ToArray();
            return categories;
        }

        public static Category[] FindByBalanceTypeAndCatType(ISession session, BalanceType baltype, CategoryType cattype)
        {
            return session.Query<Category>().Where(c => c.BalanceType == baltype && c.Type == cattype).ToArray();
            //return FindAll(nullableSession, Expression.Eq("BalanceType", baltype), Expression.Eq("Type", cattype));
        }

        public static IEnumerable<Product> LoadProducts(ISession session, Category category, int currentPage, int categoryRowsPerPage, out int count)
        {
            //ICriteria criteria = session.CreateCriteria<Product>().Add(Expression.Eq("Category.Id", category.Id)).AddOrder(Order.Asc("Position"))
            //    .SetFirstResult(currentPage * categoryRowsPerPage).SetMaxResults(categoryRowsPerPage);

            //count = session.Query<Product>().Count(p => p.Category.Id == category.Id);

            //return criteria.List<Product>();
            Product[] prods = ProductCache.FindByCategory(category);//.OrderBy(p => p.Position).ElementAt(currentPage * categoryRowsPerPage);
            prods.OrderBy(p => p.Position);
            count = ProductCache.FindByCategory(category).Length;

            if(count > 0){
                int end = count - (currentPage * categoryRowsPerPage);
                if (end > categoryRowsPerPage)
                {
                    end = categoryRowsPerPage;
                }
                return prods.ToList().GetRange(currentPage * categoryRowsPerPage, end);
            }

            return new List<Product>(); 
        }

        public static Category ParrentCategory(ISession session, BalanceType baltype)
        {
            Category[] categories = FindByBalanceType(session, baltype);
            if (categories.Length == 0) return null;

            Category auxCat = categories[0];
            while (auxCat.parent.parent != null)
            {
                auxCat = auxCat.parent;
            }

            return auxCat;
        }

        public static Category CashAdjCategory(ISession session, BalanceType baltype)
        {
            Category parrentCategory = ParrentCategory(session, baltype);
            return parrentCategory == null ? null :
                parrentCategory.Children.FirstOrDefault(cat => cat.Type == CategoryType.CashAdj);
        }

        public static ProductAttrib MapCategoryTypeToBondAttrib(CategoryType type)
        {
            switch (type)
            {
                case CategoryType.Normal:
                    return ProductAttrib.InBalance;
                case CategoryType.Roll:
                    return ProductAttrib.Roll;
                case CategoryType.CashAdj:
                    return ProductAttrib.CashAdj;
                case CategoryType.Treasury:
                    return ProductAttrib.Treasury;
                case CategoryType.Impairments:
                    return ProductAttrib.Impairments;
                case CategoryType.ModelImpairments:
                    return ProductAttrib.ModelImpairments;
                case CategoryType.Receivable:
                    return ProductAttrib.Receivable;
                case CategoryType.Reserves:
                    return ProductAttrib.Reserves;
                case CategoryType.OtherComprehensiveIncome:
                    return ProductAttrib.OtherComprehensiveIncome;
                case CategoryType.PositiveDerivativesValuation:
                    return ProductAttrib.PositiveDerivativesValuation;
                case CategoryType.NegativeDerivativesValuation:
                    return ProductAttrib.NegativeDerivativesValuation;
                case CategoryType.Payable:
                    return ProductAttrib.Payable;
                case CategoryType.IBNR:
                    return ProductAttrib.IBNR;
                case CategoryType.MarginCallReceived:
                    return ProductAttrib.MarginCallReceived;
                case CategoryType.MarginCallPaid:
                    return ProductAttrib.MarginCallPaid;

                default:
                    throw new ArgumentOutOfRangeException("type");
            }
        }

        public int ProductCount(ISession session)
        {
            int bondsCount = session.Query<Product>().Count(p => p.Category == this);
            return bondsCount;
        }

        public static Category[] FindAllWithChildren(ISession session)
        {
            return session.Query<Category>().Fetch(c => c.Children).ToArray();
        }

        public override string ToString()
        {
            return string.Format("{0}, BalanceType: {1}, Type: {2}", this.label, this.balanceType, this.type);
        }

        public static int GetNewALMID(ISession session)
        {

            return session.Query<Category>().Max(c => (int?)c.ALMID).GetValueOrDefault() + 1;
        }

        public string GetDisplayedALMID()
        {
            return "C" + this.ALMID;
        }

        public static string[] GetAllAnalysisCategories(ISession nullableSession)
        {
            if (nullableSession == null)
            {
                using (ISession sessionInternal = SessionManager.OpenSession())
                {
                    return sessionInternal.Query<Category>().
                        Select(p => p.AnalysisCategory).
                        Distinct().
                        ToArray();
                }
            }
            else
            {
                return nullableSession.Query<Category>().
                        Select(p => p.AnalysisCategory).
                        Distinct().
                        ToArray();
            }
        }
    }


    public enum BalanceType
    {
        Portfolio = -1,
        BalanceSheet = 0,
        None = 0,
        Asset = 1,
        Liability = 2,
        Equity = 3,
        OffBalanceSheet = 4,
        Derivatives = 5
    }

    public enum CategoryType
    {
        Normal = 1,
        Roll = 2,
        CashAdj = 3,
        Treasury = 4,
        Impairments = 5,
        Receivable = 6,
        Reserves = 7,
        [EnumDescription("Other Comprehensive Income")]
        OtherComprehensiveIncome = 8,
        [EnumDescription("Positive Derivatives Valuation")]
        PositiveDerivativesValuation = 9,//MTU
        [EnumDescription("Negative Derivatives Valuation")]
        NegativeDerivativesValuation = 10,//MTU
        Payable = 11,
        [EnumDescription("Interest Received On Derivatives")]
        InterestReceivedOnDerivatives = 12,
        [EnumDescription("Interest Payed On Derivatives")]
        InterestPayedOnDerivatives = 13,
        [EnumDescription("IBNR")]
        IBNR = 14,
        RollParent = 15,
        [EnumDescription("Margin call received")]
        MarginCallReceived  = 16,
        [EnumDescription("Margin call paid")]
        MarginCallPaid = 17,
        [EnumDescription("Model impairments")]
        ModelImpairments = 18,
    }
}