using System;
using System.Xml.Serialization;
using BankALM.Infrastructure.Data;
using NHibernate;
using NHibernate.Criterion;

namespace BondManager.Domain
{
    // [ActiveRecord(Lazy = false)]
    [Serializable]
    [XmlRoot]
    public class VariableValue : EntityBase<VariableValue, int>
    {
        public const string ArrayPrefix = "array:";

        private Variable variable;
        private Scenario scenario;
        private string value;
        private bool generated;

        //[BelongsTo(UniqueKey = "UniqueVariableScenario")]
        [XmlElement]
        public virtual Variable Variable
        {
            get { return this.variable; }
            set { this.variable = value; }
        }

        //[BelongsTo(UniqueKey = "UniqueVariableScenario")]
        [XmlElement]
        public virtual Scenario Scenario
        {
            get { return this.scenario; }
            set { this.scenario = value; }
        }

        //[Property(Length = 32000)]
        [XmlElement]
        public virtual string Value
        {
            get { return this.value; }
            set { this.value = value; }
        }

        //[Property]
        [XmlElement]
        public virtual bool Generated
        {
            get { return this.generated; }
            set { this.generated = value; }
        }

        public virtual bool ValueIsArray()
        {
            return this.Value != null && this.Value.StartsWith(ArrayPrefix);
        }

        public virtual double[] GetArrayValue()
        {
            string[] arrayStrings = this.Value.Substring(7).Split(';');
            double[] result = new double[arrayStrings.Length];
            for (int i = 0; i < arrayStrings.Length; i++)
            {
                double decimalValue;
                double.TryParse(arrayStrings[i], out decimalValue);

                result[i] = decimalValue;
            }
            return result;
        }

        public static VariableValue Find(ISession session, Variable variable, Scenario scenario)
        {
            return FindFirst(session, Expression.And(Expression.And(
                                 Expression.Eq("Variable", variable),
                                 Expression.Eq("Scenario", scenario)),
                                 Expression.Eq("Generated", false)));
        }

        public static VariableValue[] FindByVariable(ISession session, Variable variable)
        {
            return FindAll(session, Expression.And(
                Expression.Eq("Variable", variable),
                Expression.Eq("Generated", false)));
        }

        public static VariableValue[] FindByScenario(ISession session, Scenario scenario)
        {
            return FindAll(session, Expression.And(
                Expression.Eq("Scenario", scenario),
                Expression.Eq("Generated", false)));
        }

        public static VariableValue[] FindGenerated(ISession session)
        {
            return FindAll(session, Expression.Eq("Generated", true));
        }

        public static void DeleteGenerated(ISession session)
        {
            ExecuteDelete(session, "from VariableValue vv where vv.Generated=1");
        }
    }


}


