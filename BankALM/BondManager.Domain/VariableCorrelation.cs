using System;
using System.Linq;
using System.Xml.Serialization;
using BankALM.Infrastructure.Data;
using NHibernate;
using NHibernate.Linq;

namespace BondManager.Domain
{
    // [ActiveRecord(Lazy = false)]
    [Serializable]
    [XmlRoot]
    public class VariableCorrelation : EntityBase<VariableCorrelation, int>
    {

        public VariableCorrelation()
        {
        }

        public VariableCorrelation(Variable variable1, Variable variable2, double correlationValue, Scenario scenario)
        {
            this.Variable1 = variable1;
            this.Variable2 = variable2;
            this.CorrelationValue = correlationValue;
            this.Scenario = scenario;
        }

        //[BelongsTo]
        public virtual Variable Variable1 { get; set; }

        //[BelongsTo]
        public virtual Variable Variable2 { get; set; }

        //[BelongsTo]
        public virtual Scenario Scenario { get; set; }

        //[Property]
        public virtual double CorrelationValue { get; set; }

        public new static VariableCorrelation[] FindAll(ISession session)
        {
            //return FindAll(session, Order.Asc("Id"));
            return session.Query<VariableCorrelation>().OrderBy(vc => vc.Id).ToArray();
        }
        
    }


    

}