﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BondManager.Domain
{
    public static class VectorCache
    {
        public static List<Vector> vectors = new List<Vector>();

        public static void save(Vector vector)
        {
            vectors.Add(vector);
            vectors.OrderBy(o => o.CreationDate);
        }

        public static void set(Vector[] vectors1)
        {
            vectors = vectors1.ToList();
        }

        public static Vector[] FindByGroup(VectorGroup group)
        {
            return vectors.FindAll(v => v.Group == group).OrderBy(o => o.CreationDate).ToArray();
        }

        public static Vector[] FindByGroupAndName(VectorGroup group, String name)
        {
            return vectors.FindAll(v => v.Group == group && v.Name.ToLower().Contains(name)).OrderBy(o => o.CreationDate).ToArray();
        }

        public static Vector[] FindByImportantMarketData()
        {
            return vectors.FindAll(v => v.ImportantMarketData.Equals(true)).OrderBy(o => o.CreationDate).ToArray();
        }

        public static Vector FindByName(string name)
        {
            return vectors.Find(v => v.Name == name);
        }

        public static Vector[] FindNames(string[] names)
        {
            return vectors.FindAll(v => names.Contains(v.Name)).OrderBy(o => o.CreationDate).ToArray();
        }

        public static Vector[] FindLikeName(string name)
        {
            return vectors.FindAll(v => v.Name.ToLower().Contains(name)).OrderBy(o => o.CreationDate).ToArray();
        }

        public static Vector FindById(int id)
        {
            return vectors.Find(v => v.Id == id);
        }

        public static VectorPoint[] FindByVectorGroup(VectorGroup group)
        {
            Vector[] vectorsArr = vectors.FindAll(vp => vp.Group == group).OrderBy(o => o.CreationDate).ToArray();
            List<VectorPoint> points = new List<VectorPoint>();

            for (int i = 0; i < vectorsArr.Length; i++)
            {
                points.AddRange(vectorsArr[i].VectorPoints);
            }

            return points.ToArray();
        }

        public static void DeleteByGroup(VectorGroup group)
        {
            vectors.RemoveAll(v => v.Group.Equals(group));
        }

        public static void DeleteAll()
        {
            vectors = new List<Vector>();
        }

        public static void remove(Vector vector)
        {
            vectors.Remove(vector);
        }

        public static void addVectorPoint(Vector vector, VectorPoint point)
        {
            vectors.Find(v => v.Equals(vector)).VectorPoints.Add(point);
        }

        public static void clearVectorPoints(Vector vector)
        {
            vectors.Find(v => v.Equals(vector)).VectorPoints.Clear();
        }

        public static VectorPoint[] getVectorPoints()
        {
            List<VectorPoint> points = new List<VectorPoint>();
            for (int i = 0; i < vectors.Count; i++)
            {
                points.AddRange(vectors[i].VectorPoints);
            }

            return points.ToArray();
        }

        public static void deleteVectorPoints()
        {
            for (int i = 0; i < vectors.Count; i++)
            {
                vectors[i].VectorPoints.Clear();
            }
        }
    }
}
