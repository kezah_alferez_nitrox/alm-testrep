using System;
using System.Linq;
using System.ComponentModel;
using System.Xml.Serialization;
using BankALM.Infrastructure.Data;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Linq;

namespace BondManager.Domain
{
    // [ActiveRecord(Lazy = false)]
    [Serializable]
    [XmlRoot]
    public class DynamicVariable : EntityBase<DynamicVariable, int>
    {
        private int position;
        private string name;
        private bool formula;

        //[Property(Unique = true)]
        [XmlElement]
        public virtual string Name
        {
            get { return this.name; }
            set
            {
                this.name = value;
                this.FirePropertyChanged("Name");
            }
        }

        //[Property]
        [XmlElement]
        public virtual int Position { get; set; }

        //[Property]
        [XmlElement]
        public virtual string Formula { get; set; }

        //[XmlIgnore]
        public virtual DynamicVariable Self
        {
            get { return this; }
        }

        public virtual event PropertyChangedEventHandler PropertyChanged;

        private void FirePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public override string ToString()
        {
            return this.Name;
        }

        public static DynamicVariable FindByName(ISession session, string name)
        {
            return session.Query<DynamicVariable>().FirstOrDefault(dv => dv.Name == name);
            //return FindFirst(session, Expression.Eq("Name", name));
        }

        public static bool ExistsByName(ISession session, string variableName)
        {
            return session.Query<DynamicVariable>().FirstOrDefault(dv => dv.Name == variableName) != null;
            //return Exists(session, "Name = ?", variableName);
        }

        public static new DynamicVariable[] FindAll(ISession session)
        {
            return session.Query<DynamicVariable>().OrderBy(dv => dv.Position).ToArray();
        }

        public static bool ExistById(ISession session, int id)
        {
            return session.Query<DynamicVariable>().FirstOrDefault(dv => dv.Id == id) != null;
            //return Exists(session, "Id = ?", id);
        }
        
    }




}