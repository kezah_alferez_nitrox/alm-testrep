﻿using BankALM.Infrastructure.Data;

namespace BondManager.Domain.Mapping
{
    // [Property] -> Map
    // [BelongsTo] -> References
    // [HasMany] -> HasMany
    public class OtherItemAssumtMap : EntityMap<OtherItemAssum, int>
    {
        public OtherItemAssumtMap()
        {
            this.Id(e => e.Id);
            this.Map(e => e.Descrip);
            this.Map(e => e.Vect);
            //this.Map(e => e.Values);
            this.Map(e => e.IsVisibleChecked);
            this.Map(e => e.SubtotalGroup);
            this.Map(e => e.Status);
            this.Map(e => e.Position);
            this.Map(e => e.CannotDelete);
        }
    }
}