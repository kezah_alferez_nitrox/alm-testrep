﻿using BankALM.Infrastructure.Data;

namespace BondManager.Domain.Mapping
{
    // [Property] -> Map
    // [BelongsTo] -> References
    // [HasMany] -> HasMany
    public class DividendMap : EntityMap<Dividend, int>
    {
        public DividendMap()
        {
            this.Id(e => e.Id);
            this.Map(e => e.Name);
            this.Map(e => e.Minimum);
            this.Map(e => e.Amount);
        }
    }
}