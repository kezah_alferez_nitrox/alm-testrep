﻿using BankALM.Infrastructure.Data;

namespace BondManager.Domain.Mapping
{
    // [Property] -> Map
    // [BelongsTo] -> References
    // [HasMany] -> HasMany
    public class FXRateMap : EntityMap<FXRate, int>
    {
        public FXRateMap()
        {
            this.Id(e => e.Id);
            this.Map(e => e.Value);

            this.References(e => e.From).UniqueKey("UniqueCurrencyPairFXRate");
            this.References(e => e.To).UniqueKey("UniqueCurrencyPairFXRate");
        }
    }
}