﻿using BankALM.Infrastructure.Data;

namespace BondManager.Domain.Mapping
{
    public class ParamMap : EntityMap<Param, string>
    {
        public ParamMap()
        {
            this.Id(e => e.Id).GeneratedBy.Assigned();
            this.Map(e => e.Val).CustomType("StringClob");
        }
    }
}