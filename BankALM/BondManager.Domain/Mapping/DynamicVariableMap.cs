﻿using BankALM.Infrastructure.Data;

namespace BondManager.Domain.Mapping
{
    // [Property] -> Map
    // [BelongsTo] -> References
    // [HasMany] -> HasMany
    public class DynamicVariableMap : EntityMap<DynamicVariable, int>
    {
        public DynamicVariableMap()
        {
            this.Not.LazyLoad();

            this.Id(e => e.Id);

            this.Map(e => e.Position);
            this.Map(e => e.Name).Unique();
            this.Map(e => e.Formula);
        }
    }
}