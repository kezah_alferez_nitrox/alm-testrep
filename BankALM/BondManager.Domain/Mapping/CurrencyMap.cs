﻿using BankALM.Infrastructure.Data;

namespace BondManager.Domain.Mapping
{
    public class CurrencyMap : EntityMap<Currency, int>
    {
        public CurrencyMap()
        {
            this.Id(e => e.Id);
            this.Map(e => e.ShortName);
            this.Map(e => e.Symbol);
        }
    }
}