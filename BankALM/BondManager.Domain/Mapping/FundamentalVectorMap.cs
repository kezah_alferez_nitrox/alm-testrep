﻿using BankALM.Infrastructure.Data;

namespace BondManager.Domain.Mapping
{
    // [Property] -> Map
    // [BelongsTo] -> References
    // [HasMany] -> HasMany
    public class FundamentalVectorMap : EntityMap<FundamentalVector, int>
    {
        public FundamentalVectorMap()
        {
            this.Id(e => e.Id);

            this.Map(e => e.Position);
            this.Map(e => e.Name);
            this.Map(e => e.Delta);
            this.Map(e => e.Unit);
            this.Map(e => e.Type);
            this.Map(e => e.DateConvention);
            this.Map(e => e.Frequency);
            this.Map(e => e.Volatility);

            this.References(e=>e.Variable);
            this.References(e=>e.Currency);

        }
    }
}