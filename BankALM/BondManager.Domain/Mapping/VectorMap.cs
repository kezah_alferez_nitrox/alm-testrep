﻿using BankALM.Infrastructure.Data;

namespace BondManager.Domain.Mapping
{
    public class VectorMap : EntityMap<Vector, int>
    {
        public VectorMap()
        {
            this.Map(e => e.Name).Unique();

            this.Map(e => e.Group).Index("IDX_GROUP");
            this.Map(e => e.VectorType);
            this.Map(e => e.CreationDate);
            this.Map(e => e.Frequency);
            this.Map(e => e.Position);

            this.Map(e => e.ImportantMarketData);

            this.HasMany(e => e.VectorPoints).OrderBy("Date");
        }
    }
}