﻿using BankALM.Infrastructure.Data;

namespace BondManager.Domain.Mapping
{
    // [Property] -> Map
    // [BelongsTo] -> References
    // [HasMany] -> HasMany
    public class VolatilityConfigMap : EntityMap<VolatilityConfig, int>
    {
        public VolatilityConfigMap()
        {
            this.Id(e => e.Id);

            this.Map(e => e.Selected);
            this.Map(e => e.Volatility);
            this.Map(e => e.Mean);
            this.Map(e => e.SimulationMode);
            this.Map(e => e.IsLog);
            this.Map(e => e.HalfLife);

            this.References(e => e.Variable);
            this.References(e => e.Scenario);
        }
    }
}