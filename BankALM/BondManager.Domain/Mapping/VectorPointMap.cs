﻿using BankALM.Infrastructure.Data;

namespace BondManager.Domain.Mapping
{
    // [Property] -> Map
    // [BelongsTo] -> References
    // [HasMany] -> HasMany
    public class VectorPointMap : EntityMap<VectorPoint, int>
    {
        public VectorPointMap()
        {
            this.Map(e => e.Date);
            this.Map(e => e.Value);

            this.References(e=>e.Vector);
        }
    }
}