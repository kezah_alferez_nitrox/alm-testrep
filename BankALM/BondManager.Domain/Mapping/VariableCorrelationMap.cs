﻿using BankALM.Infrastructure.Data;

namespace BondManager.Domain.Mapping
{
    // [Property] -> Map
    // [BelongsTo] -> References
    // [HasMany] -> HasMany
    public class VariableCorrelationMap : EntityMap<VariableCorrelation, int>
    {
        public VariableCorrelationMap()
        {
            this.Id(e => e.Id);

            this.Map(e => e.CorrelationValue);

            this.References(e => e.Variable1);
            this.References(e => e.Variable2);
            this.References(e => e.Scenario);

        }
    }
}