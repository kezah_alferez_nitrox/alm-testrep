﻿using BankALM.Infrastructure.Data;

namespace BondManager.Domain.Mapping
{
    // [Property] -> Map
    // [BelongsTo] -> References
    // [HasMany] -> HasMany
    public class CategoryMap : EntityMap<Category, int>
    {
        public CategoryMap()
        {
            this.Not.LazyLoad();

            this.Id(e => e.Id);
            this.Map(e => e.Label);
            this.Map(e => e.BalanceType).Index("IDX_BALANCE_TYPE");
            this.Map(e => e.Position);
            this.Map(e => e.Type);
            this.Map(e => e.ReadOnly);
            this.Map(e => e.isVisible);
            this.Map(e => e.IsVisibleCheckedModel);
            this.Map(e => e.IsVisibleChecked);
            this.Map(e => e.ALMID);
            this.Map(e => e.AnalysisCategory);
            this.References(e => e.Parent);

            this.HasMany(e => e.Bonds).OrderBy("Position, Attrib");
            this.HasMany(e => e.Children).KeyColumn("Parent_id").OrderBy("Position, Type");
        }
    }
}