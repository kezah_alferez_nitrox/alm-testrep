using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using BankALM.Infrastructure.Data;
using NHibernate;

namespace BondManager.Domain
{
    public class Param : EntityBase<Param, string>
    {
        public const string VALUATION_CURRENCY = "Valuation Currency";
        public const string VALUATION_DATE = "Valuation Date";
        
        public const string STANDARD_TAX = "Standard Rate";
        public const string MAX_DURATION = "Max Duration";
        public const string RECOVERY_LAG_IN_MONTHS = "Recovery Lag In Months";
        private const string DIVIDEND_PAYMENT_MONTH = "DIVIDEND_PAYMENT_MONTH";
        private const string AUTO_CALCULATE_BOOK_PRICE = "AUTO_CALCULATE_BOOK_PRICE";
        private const string SHOW_DYNAMIC_VARIABLES_IN_RESULT = "SHOW_DYNAMIC_VARIABLES_IN_RESULT";
        public const string LONG_TERM_TAX = "Long Term";
        public const string SPECIAL_TAX = "Special";
        public const string FILE_NAME = "FileName";
        public const string DATE_FORMAT = "o";
        private const string ROLLS_ENABLED = "RollEnabled";
        private const string Compute_Optimisation_Type = "Compute_Optimisation_Type";
        private const string Per_Product_Result_Details = "Per_Product_Result_Details";
        private const string OCI_Management_Selected_Index = "OCI_Management_Selected_Index";
        private const string LOGO = "LOGO";
        private const string CONFIRM_BEFORE_DELETE_LINE = "CONFIRM_BEFORE_DELETE_LINE";
        private const string IMPORT_DELETES_MISSING_PRODUCTS = "IMPORT_DELETES_MISSING_PRODUCTS";
        private const string LINK_SWAPS_2_BY_2_IN_ORDER_DURING_IMPORT = "LINK_SWAPS_2_BY_2_IN_ORDER_DURING_IMPORT";
        private const string COMPUTE_ANALYSIS_CATEGORIES = "COMPUTE_ANALYSIS_CATEGORIES";
        private const string COMPUTE_GAP = "COMPUTE_GAP";
        private const string GAP_FIXED_RATE = "GAP_FIXED_RATE";
        private const string GAP_TYPE = "GAP_TYPE";
        private const string GAP_CASHFLOW_PRESENTATION = "GAP_CASHFLOW_PRESENTATION";
        private const string DEBUG_LOG_LEVEL = "DEBUG_LOG_LEVEL";
        private const string ORIGIN_BASED_ON_STARTDATE = "ORIGIN_BASED_ON_STARTDATE";
        private const string DO_NOT_SHOW_DATE_VALIDATION_POPUP = "DO_NOT_SHOW_DATE_VALIDATION_POPUP";
        private const string SHOW_DECIMAL_VALUE_IN_RESULT = "SHOW_DECIMAL_VALUE_IN_RESULT";
        private const string FOLLOW_CONFIG_SETTING_IN_RESULT = "FOLLOW_CONFIG_SETTING_IN_RESULT";

        public const int OCI_MANAGEMENT_AS_EQUITY = 0;
        public const int OCI_MANAGEMENT_AS_LIABILITY = 1;

        public const int GAP_LIQUIDITY = 0;
        public const int GAP_TAUX = 1;
        public const int GAP_STOCK = 0;


        public virtual string Val { get; set; }

        public virtual Param Copy()
        {
            Param copy = new Param();
            copy.Id = this.Id;
            copy.Val = this.Val;
            return copy;
        }


        private static Currency valuationCurrency;
        public static Currency GetValuationCurrency(ISession session)
        {
            if (valuationCurrency == null)
            {
                Param param = Find(session, VALUATION_CURRENCY);
                if (param == null || param.Val == null) return null;

                int currencyId = int.Parse(param.Val);
                try
                {
                    valuationCurrency = Currency.Find(session, currencyId);
                }
                catch (Exception)
                {
                    // todo
                }
            }
            return valuationCurrency;
        }

        public static void SetValuationCurrency(ISession session, Currency value)
        {
            valuationCurrency = value;

            Param valuationCurrencyParam = Find(session, VALUATION_CURRENCY);
            if (valuationCurrencyParam == null)
            {
                valuationCurrencyParam = new Param();
                valuationCurrencyParam.Id = VALUATION_CURRENCY;
                valuationCurrencyParam.Create(session);
                session.Flush();
            }
            valuationCurrencyParam.Val = value.Id.ToString();
            valuationCurrencyParam.Update(session);
        }

        public static string GetFileName(ISession session)
        {
            Param param = Find(session, FILE_NAME);
            if (param == null || param.Val == null) return null;

            string filename = param.Val;
            return filename;
        }

        public static void SetFileName(ISession session, string value)
        {
            Param param = Find(session, FILE_NAME);
            if (param == null)
            {
                param = new Param();
                param.Id = FILE_NAME;
                param.Create(session);
            }
            param.Val = value;
            param.Update(session);
        }

        public static DateTime? GetValuationDate(ISession session)
        {
            Param param = Find(session, VALUATION_DATE);
            if (param == null || param.Val == null) return null;

            DateTime valuationDate;
            if (!DateTime.TryParseExact(param.Val, DATE_FORMAT, null, DateTimeStyles.AdjustToUniversal, out valuationDate))
            {
                return null;
            }

                return new DateTime(valuationDate.Year, valuationDate.Month, 1);
        }

        public static void SetValuationDate(ISession session, DateTime? value)
        {
            Param param = Find(session, VALUATION_DATE);
            if (param == null)
            {
                param = new Param();
                param.Id = VALUATION_DATE;
                param.Create(session);
            }
            param.Val = value == null ? null : value.Value.ToString(DATE_FORMAT);
            param.Update(session);
        }

        


        private static bool? rollsEnabledCache = null;

        public static bool GetRollEnabled(ISession session)
        {
            if (rollsEnabledCache != null) return rollsEnabledCache.Value;
            rollsEnabledCache = GetBoolParamByName(session, ROLLS_ENABLED).GetValueOrDefault(true);
            return rollsEnabledCache.Value;
        }

        public static void SetRollEnabled(ISession session, bool value)
        {
            rollsEnabledCache = value;
            SetBoolParamByName(session, ROLLS_ENABLED, value);
        }

        private static bool? GetBoolParamByName(ISession session, string name)
        {
            Param param = Find(session, name);
            if (param == null || string.IsNullOrEmpty(param.Val)) return null;

            bool value;
            return bool.TryParse(param.Val, out value) ? value : (bool?)null;
        }

        private static void SetBoolParamByName(ISession session, string name, bool value)
        {
            Param param = Find(session, name);
            if (param == null)
            {
                param = new Param();
                param.Id = name;
                param.Create(session);
            }
            param.Val = value.ToString();
            param.Update(session);
        }

        private static double GetDecimalParamByName(ISession session, string name)
        {
            Param param = Find(session, name);
            if (param == null || string.IsNullOrEmpty(param.Val)) return 0;

            double value;
            return double.TryParse(param.Val, out value) ? value : 0;
        }

        private static void SetDecimalParamByName(ISession session, string name, double value)
        {
            Param param = Find(session, name);
            if (param == null)
            {
                param = new Param();
                param.Id = name;
                param.Create(session);
            }
            param.Val = value.ToString();
            param.Update(session);
        }

        private static int GetIntParamByName(ISession session, string name)
        {
            Param param = Find(session, name);
            if (param == null || string.IsNullOrEmpty(param.Val)) return 0;

            int value;
            return int.TryParse(param.Val, out value) ? value : 0;
        }

        private static void SetIntParamByName(ISession session, string name, int value)
        {
            Param param = Find(session, name);
            if (param == null)
            {
                param = new Param();
                param.Id = name;
                param.Create(session);
            }
            param.Val = value.ToString();
            param.Update(session);
        }

        private static string GetStringParamByName(ISession session, string name)
        {
            Param param = Find(session, name);
            if (param == null || string.IsNullOrEmpty(param.Val)) return "";

            return param.Val;
        }

        private static void SetStringParamByName(ISession session, string name, string value)
        {
            Param param = Find(session, name);
            if (param == null)
            {
                param = new Param();
                param.Id = name;
                param.Create(session);
            }
            param.Val = value;
            param.Update(session);
        }

        public static double GetStandardTax(ISession session)
        {
            return GetDecimalParamByName(session, STANDARD_TAX);
        }
        public static void SetStandardTax(ISession session, double value)
        {
            SetDecimalParamByName(session, STANDARD_TAX, value);
        }

        public static double GetLongTermTax(ISession session)
        {
            return GetDecimalParamByName(session, LONG_TERM_TAX);
        }
        public static void SetLongTermTax(ISession session, double value)
        {
            SetDecimalParamByName(session, LONG_TERM_TAX, value);
        }

        public static double SpecialTax(ISession session)
        {
            return GetDecimalParamByName(session, SPECIAL_TAX);
        }
        public static void SpecialTax(ISession session, double value)
        {
            SetDecimalParamByName(session, SPECIAL_TAX, value);
        }

        public static int GetComputeOptimisationType(ISession session)
        {
            return GetIntParamByName(session, Compute_Optimisation_Type);
        }
        public static void SetComputeOptimisationType(ISession session, int value)
        {
            SetIntParamByName(session, Compute_Optimisation_Type, value);
        }

        public static string GetGapFixedRate(ISession session)
        {
            return GetStringParamByName(session, GAP_FIXED_RATE);
        }
        public static void SetGapFixedRate(ISession session, string value)
        {
            SetStringParamByName(session, GAP_FIXED_RATE, value);
        }  

        public static bool GetPerProductResultDetails(ISession session)
        {
            return GetBoolParamByName(session, Per_Product_Result_Details).GetValueOrDefault(true);
        }

        public static int GetGapType(ISession session)
        {
            return GetIntParamByName(session, GAP_TYPE);
        }
        public static void SetGapType(ISession session, int value)
        {
            SetIntParamByName(session, GAP_TYPE, value);
        }
        public static int GetGapCashflowPresentation(ISession session)
        {
            return GetIntParamByName(session, GAP_CASHFLOW_PRESENTATION);
        }
        public static void SetGapCashflowPresentation(ISession session, int value)
        {
            SetIntParamByName(session, GAP_CASHFLOW_PRESENTATION, value);
        }
        

        public static int GetOCIManagementSelectedIndex(ISession session)
        {
            return GetIntParamByName(session, OCI_Management_Selected_Index);
        }
        public static void SetPerProductResultDetails(ISession session, bool value)
        {
            SetBoolParamByName(session, Per_Product_Result_Details, value);
        }
        public static void SetOCIManagementSelectedIndex(ISession session, int value)
        {
            SetIntParamByName(session, OCI_Management_Selected_Index, value);
        }
        public static bool GetConfirmBeforeDelete(ISession session)
        {
             return GetBoolParamByName(session, CONFIRM_BEFORE_DELETE_LINE)??false;
            
        }
        public static void SetConfirmBeforeDelete(ISession session, bool value)
        {
            SetBoolParamByName(session, CONFIRM_BEFORE_DELETE_LINE, value);
        }

        public static bool GetImportDeletesMissingProducts(ISession session)
        {
            return GetBoolParamByName(session, IMPORT_DELETES_MISSING_PRODUCTS) ?? false;

        }
        public static void SetImportDeletesMissingProducts(ISession session, bool value)
        {
            SetBoolParamByName(session, IMPORT_DELETES_MISSING_PRODUCTS, value);
        }

        public static bool GetLinkSwaps2by2(ISession session)
        {
            return GetBoolParamByName(session, LINK_SWAPS_2_BY_2_IN_ORDER_DURING_IMPORT) ?? false;
        }
        public static void SetLinkSwaps2by2(ISession session, bool value)
        {
            SetBoolParamByName(session, LINK_SWAPS_2_BY_2_IN_ORDER_DURING_IMPORT, value);
        }

        public static bool GetComputeAnalysisCategories(ISession session)
        {
            return GetBoolParamByName(session, COMPUTE_ANALYSIS_CATEGORIES) ?? true;
        }
        public static void SetComputeAnalysisCategories(ISession session, bool value)
        {
            SetBoolParamByName(session, COMPUTE_ANALYSIS_CATEGORIES, value);
        }
        public static bool GetDebugLogLevel(ISession session)
        {
            return GetBoolParamByName(session, DEBUG_LOG_LEVEL) ?? false;
        }
        public static void SetDebugLogLevel(ISession session, bool value)
        {
            SetBoolParamByName(session, DEBUG_LOG_LEVEL, value);
        }
        public static bool GetOriginBasedOnStartdate(ISession session)
        {
            return GetBoolParamByName(session, ORIGIN_BASED_ON_STARTDATE) ?? false;
        }
        public static void SetOriginBasedOnStartdate(ISession session, bool value)
        {
            SetBoolParamByName(session, ORIGIN_BASED_ON_STARTDATE, value);
        }


        public static bool GetComputeGap(ISession session)
        {
            return GetBoolParamByName(session, COMPUTE_GAP) ?? false;
        }
        public static void SetComputeGap(ISession session, bool value)
        {
            SetBoolParamByName(session, COMPUTE_GAP, value);
        }

        public static int GetRecoveryLagInMonths(ISession session)
        {
            return GetIntParamByName(session, RECOVERY_LAG_IN_MONTHS) ;

        }
        public static void SetRecoveryLagInMonths(ISession session, int value)
        {
            SetIntParamByName(session, RECOVERY_LAG_IN_MONTHS, value);
        }

        private static int? cachedMaxDuration = null;
        public static int GetMaxDuration(ISession session)
        {
            if (cachedMaxDuration != null) return cachedMaxDuration.GetValueOrDefault();

            if (Find(session, MAX_DURATION) == null)
            {
                cachedMaxDuration = 121;
                SetIntParamByName(session, MAX_DURATION, cachedMaxDuration.GetValueOrDefault());
            }
            else
            {
                cachedMaxDuration = GetIntParamByName(session, MAX_DURATION)+1;
            }
            return cachedMaxDuration.GetValueOrDefault();
        }
        public static void SetMaxDuration(ISession session, int value)
        {
            cachedMaxDuration = value;
            SetIntParamByName(session, MAX_DURATION, value);
        }

        private static int? cachedDividendPaymentMonth;
        public static int GetDividendPaymentMonth(ISession session)
        {
            if (cachedDividendPaymentMonth != null) return cachedDividendPaymentMonth.GetValueOrDefault();

            if (Find(session, DIVIDEND_PAYMENT_MONTH) == null)
            {
                cachedDividendPaymentMonth = 12;
                SetIntParamByName(session, DIVIDEND_PAYMENT_MONTH, cachedDividendPaymentMonth.GetValueOrDefault());
            }
            else
            {
                cachedDividendPaymentMonth = GetIntParamByName(session, DIVIDEND_PAYMENT_MONTH);
            }
            return cachedDividendPaymentMonth.GetValueOrDefault();
        }

        public static void SetDividendPaymentMonth(ISession session, int value)
        {
            cachedDividendPaymentMonth = value;
            SetIntParamByName(session, DIVIDEND_PAYMENT_MONTH, value);
        }



        private static bool? cachedAutoCalculateBookPrice;

        public static bool GetAutoCalculateBookPrice(ISession session)
        {
            if (cachedAutoCalculateBookPrice != null) return cachedAutoCalculateBookPrice.GetValueOrDefault();

            cachedAutoCalculateBookPrice = GetBoolParamByName(session, AUTO_CALCULATE_BOOK_PRICE);

            if (cachedAutoCalculateBookPrice == null)
            {
                cachedAutoCalculateBookPrice = false;
                SetBoolParamByName(session, AUTO_CALCULATE_BOOK_PRICE, cachedAutoCalculateBookPrice.GetValueOrDefault());
            }

            return cachedAutoCalculateBookPrice.GetValueOrDefault();
        }
        public static void SetAutoCalculateBookPrice(ISession session, bool value)
        {
            cachedAutoCalculateBookPrice = value;
            SetBoolParamByName(session, AUTO_CALCULATE_BOOK_PRICE, value);
        }


        public static bool GetShowDynamicVariablesInResult(ISession session)
        {
            bool? value = GetBoolParamByName(session, SHOW_DYNAMIC_VARIABLES_IN_RESULT);

            return value.GetValueOrDefault(true);
        }
        public static void SetShowDynamicVariablesInResult(ISession session, bool value)
        {
            SetBoolParamByName(session, SHOW_DYNAMIC_VARIABLES_IN_RESULT, value);
        }

        public static int GetVectorYears(ISession session)
        {
            return GetMaxDuration(session) % 12 == 0 ? GetMaxDuration(session) / 12 : GetMaxDuration(session) / 12 + 1;
        }

        public static int GetDefaultBondDuration(ISession session)
        {
            return GetMaxDuration(session)+1;
        }

        public static int ResultMonths(ISession session)
        {
            return GetMaxDuration(session) + 1;
        }

        public static bool GetDateValidateMsgPopUp(ISession session)
        {
            return GetBoolParamByName(session, DO_NOT_SHOW_DATE_VALIDATION_POPUP) ?? false;
        }
        public static void SetDateValidateMsgPopUp(ISession session, bool value)
        {
            SetBoolParamByName(session, DO_NOT_SHOW_DATE_VALIDATION_POPUP, value);
        }

        public static bool GetShowDecimalValueInResult(ISession session)
        {
            bool? value = GetBoolParamByName(session, SHOW_DECIMAL_VALUE_IN_RESULT);

            return value.GetValueOrDefault(false);
        }
        public static void SetShowDecimalValueInResult(ISession session, bool value)
        {
            SetBoolParamByName(session, SHOW_DECIMAL_VALUE_IN_RESULT, value);
        }

        public static bool GetFollowConfigSettingInResult(ISession session)
        {
            bool? value = GetBoolParamByName(session, FOLLOW_CONFIG_SETTING_IN_RESULT);

            return value.GetValueOrDefault(false);
        }
        public static void SetFollowConfigSettingInResult(ISession session, bool value)
        {
            SetBoolParamByName(session, FOLLOW_CONFIG_SETTING_IN_RESULT, value);
        }

        public static Param FindByID(ISession session, string ParamId)
        {
            return Find(session, ParamId);
        }

        public static void ClearCaches()
        {
            cachedAutoCalculateBookPrice = null;
            cachedDividendPaymentMonth = null;
            cachedMaxDuration = null;
        }

        public static Image GetLogo(ISession session)
        {
            Param param = Find(session, LOGO);
            if (param == null || param.Val == null) return null;

            return Base64ToImage(param.Val);
        }

        public static void SetLogo(ISession session, Image value)
        {
            Param param = Find(session, LOGO);
            if (param == null)
            {
                param = new Param();
                param.Id = LOGO;
                param.Create(session);
            }
            param.Val = (value == null ? null : ImageToBase64(value, ImageFormat.Png));
            param.Update(session);
        }

        public static string ImageToBase64(Image image, ImageFormat format)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                // Convert Image to byte[]
                image.Save(ms, format);
                byte[] imageBytes = ms.ToArray();

                // Convert byte[] to Base64 String
                string base64String = Convert.ToBase64String(imageBytes);
                return base64String;
            }
        }

        public static Image Base64ToImage(string base64String)
        {
            // Convert Base64 String to byte[]
            byte[] imageBytes = Convert.FromBase64String(base64String);
            MemoryStream ms = new MemoryStream(imageBytes, 0,
                                               imageBytes.Length);

            // Convert byte[] to Image
            ms.Write(imageBytes, 0, imageBytes.Length);
            Image image = Image.FromStream(ms, true);
            return image;
        }

        public static void SaveLogoTo(Image image, string fileName)
        {
            if (image == null) return;
            using (FileStream fileStream = File.Open(fileName, FileMode.Create))
            {
                image.Save(fileStream, ImageFormat.Jpeg);
            }
        }
    }
}
