using System;
using System.Linq;
using System.Xml.Serialization;
using BankALM.Infrastructure.Data;
using NHibernate;
using NHibernate.Linq;

namespace BondManager.Domain
{
    // [ActiveRecord(Lazy = false)]
    [Serializable]
    [XmlRoot]

    public class VectorPoint : EntityBase<VectorPoint, int>
    {
        private Vector vector;
        private DateTime date = DateTime.Now;
        private double value;


        //[BelongsTo]
        [XmlElement] // TODO ajouter index
        public virtual Vector Vector
        {
            get
            {
                return this.vector;
            }
            set
            {
                this.vector = value;
            }
        }

        //[Property]
        [XmlElement]
        public virtual DateTime Date
        {
            get
            {
                return this.date;
            }
            set
            {
                this.date = value;
            }
        }

        //[Property]
        [XmlElement]
        public virtual double Value
        {
            get
            {
                return this.value;
            }
            set
            {
                this.value = value;
            }
        }

        public virtual VectorPoint Copy()
        {
            VectorPoint newVectorPoint = new VectorPoint();
            newVectorPoint.Date = this.Date;
            newVectorPoint.Value = this.Value;

            return newVectorPoint;
        }



    }



}