using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using BankALM.Infrastructure.Data;
using Eu.AlterSystems.ASNetLib.Core;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Linq;

namespace BondManager.Domain
{
    [Serializable]
    [XmlRoot]
    public class Vector : EntityBase<Vector, int>
    {
        public Vector()
        {
            this.VectorPoints = new List<VectorPoint>();
        }

        [XmlElement]
        public virtual string Name { get; set; }

        [XmlElement]
        public virtual VectorGroup Group { get; set; }

        [XmlElement]
        public virtual DateTime? CreationDate { get; set; }

        [XmlIgnore]
        public virtual string extrapolationleft { get; set; }

        [XmlIgnore]
        public virtual string extrapolationright { get; set; }

        [XmlIgnore]
        public virtual string interpolation { get; set; }


        [XmlElement]
        public virtual VectorFrequency Frequency { get; set; }

        //[XmlElement]
        //public virtual VectorOriginReference OriginReference { get; set; }

        [XmlElement]
        public VectorType VectorType { get; set; }

        [XmlElement]
        public virtual bool ImportantMarketData { get; set; }

        [XmlElement]
        public int Position { get; set; }

        [XmlElement]
        public virtual List<VectorPoint> VectorPoints { get; set; }

        public virtual Vector Self
        {
            get { return this; }
        }

        public virtual bool Equals(Vector other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return other.Name.Equals(this.Name);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof(Vector)) return false;
            return this.Equals((Vector)obj);
        }

        public override int GetHashCode()
        {
            return this.Id;
        }

        public override string ToString()
        {
            return this.Name;
        }

        public static bool operator ==(Vector left, Vector right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Vector left, Vector right)
        {
            return !Equals(left, right);
        }

        public virtual Vector Copy()
        {
            Vector vectorCopy = new Vector();
            vectorCopy.Name = Name;
            vectorCopy.Group = Group;
            vectorCopy.Frequency = Frequency;
            vectorCopy.VectorType = VectorType;
            foreach (VectorPoint point in this.VectorPoints)
            {
                VectorPoint vectorPointCopy = point.Copy();
                vectorPointCopy.Vector = vectorCopy;
                vectorCopy.VectorPoints.Add(vectorPointCopy);
            }

            return vectorCopy;
        }



        public static Func<DateTime, int, DateTime> GetIncrementFunc(VectorFrequency frequency)
        {
            Func<DateTime, int, DateTime> func;
            switch (frequency)
            {
                case VectorFrequency.None:
                    func = (d, c) => d.AddMonths(c * 1); // todo ?!
                    break;
                case VectorFrequency.Daily:
                    func = (d, c) => d.AddDays(c);
                    break;
                case VectorFrequency.Monthly:
                    func = (d, c) => d.AddMonths(c * 1);
                    break;
                case VectorFrequency.Quaterly:
                    func = (d, c) => d.AddMonths(c * 3);
                    break;
                case VectorFrequency.SemiAnnual:
                    func = (d, c) => d.AddMonths(c * 6);
                    break;
                case VectorFrequency.Annual:
                    func = (d, c) => d.AddMonths(c * 12);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            return func;
        }

        public virtual Func<DateTime, int, DateTime> GetIncrementFunc()
        {
            return GetIncrementFunc(this.Frequency);
        }

        public virtual VectorPoint GetCurrentValue(DateTime date)
        {
            date = date.Date;
            Func<DateTime, int, DateTime> func = this.GetIncrementFunc();
            DateTime dateMin = func(date, -1);
            DateTime dateMax = date.AddDays(1);

            VectorPoint point = this.VectorPoints.Find(vp => vp.Vector == this && vp.Date >= dateMin && vp.Date < dateMax);// session.Query<VectorPoint>().
            //  FirstOrDefault(vp => vp.Vector == this && vp.Date >= dateMin && vp.Date < dateMax);
            return point;
        }

        public virtual VectorPoint[] FindByVector()
        {
            return this.VectorPoints.OrderBy(o => o.Date).ToArray();
        }

        public virtual VectorPoint[] FindByDate(DateTime date)
        {
            return this.VectorPoints.FindAll(x => x.Date.Equals(date)).OrderBy(o => o.Date).ToArray();
        }

        public virtual VectorPoint FindByMonthYear(int year, int month)
        {
            foreach (VectorPoint _vp in this.VectorPoints)
            {
                if (_vp.Date.Year == year && _vp.Date.Month == month)
                    return _vp;
            }
            return null;
        }

    }

    public enum VectorType
    {
        Percentage,
        Value
    }


    public enum VectorOriginReference
    {
        [EnumDescription("Valuation Date (absolue value)")]
        ValuationDate,
        [EnumDescription("Start Date (per product relative value)")]
        StartDate
    }

    public enum VectorGroup
    {
        All,
        Workspace,
        MarketData,
        MarketVol,
        [EnumDescription("Customer Data Vectors")]
        CustomerData
    }

    public enum VectorFrequency
    {
        None,
        Daily,
        Monthly,
        Quaterly,
        SemiAnnual,
        Annual,
    }

}