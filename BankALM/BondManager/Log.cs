using System;
using System.Diagnostics;
using log4net;

namespace BondManager
{
    public class Log
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(Log));

        public static void Exception(string message, Exception ex)
        {
            Debug.WriteLine(message);
            log.Error(message, ex);
        }

        public static void Error(string message)
        {
            Debug.WriteLine(message);
            log.Error(message);
        }

        public static void Warn(string message)
        {
            Debug.WriteLine(message);
            log.Warn(message);
        }

        public static void Warn(Exception ex)
        {
            Debug.WriteLine(ex.ToString());
            log.Warn(ex);
        }
    }


}