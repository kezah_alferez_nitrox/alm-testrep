﻿using System;
using System.Globalization;
using System.Threading;
using System.Windows.Forms;
using BondManager.Views;
using BondManager.Views.BalanceSheet.Balance;

namespace BondManager
{
    internal static class Program
    {
        /// <summary>
        /// Point d'entrée principal de l'application.
        /// </summary>
        [STAThread]
        private static void Main(string[] args)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");
#if !DEBUG
            return;
            Application.ThreadException += Application_ThreadException;
#endif

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            MainForm balanceSheet = new MainForm(args);
            balanceSheet.WindowState = FormWindowState.Maximized;
            Application.Run(balanceSheet);
        }

#if !DEBUG
        private static void Application_ThreadException(object sender, ThreadExceptionEventArgs e)
        {
            Cursor.Current = Cursors.Default;
            Log.Exception("ThreadException", e.Exception);      
        }
#endif
    }
}