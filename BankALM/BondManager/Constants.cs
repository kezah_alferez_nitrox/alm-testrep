using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Drawing;
using System.Text.RegularExpressions;
using BondManager.Resources.Language;
using BondManager.Views;

namespace BondManager
{
    public class Constants
    {
        public static string APPLICATION_NAME = Labels.Constants_APPLICATION_NAME_ALM_Solutions;

        public const string MONEY_CELL_FORMAT = "N2";
        public const string DURATION_CELL_FORMAT = "N0";
        public const string VECTOR_POINT_CELL_FORMAT = "N5";
        public const string PERCENTAGE_CELL_FORMAT = "P3";

        public const string ASSET = "Asset";
        public const string LIABILITIES = "Liabilities";

        public static string TEMPLATE_FILE_DIRECTORY_PATH = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\WorkspaceTemplates\";

        public static readonly string[] LINE_SEPARATOR = new string[] { Environment.NewLine };
        public const char VALUE_SEPARATOR = '\t';

        public const int GRID_ROW_HEIGHT = 18;

        //public const int VECTOR_YEARS = 30;
        //public const int DEFAULT_BOND_DURATION = VECTOR_YEARS * 12;
        //public const int RESULT_MONTHS = VECTOR_YEARS * 12 + 1;        

        public static string CASH_ADJ_BOND_DESCRIPTION = Labels.Constants_CASH_ADJ_BOND_DESCRIPTION_Cash_Adj;

        public static MainForm BalanceSheetForm;

        public class Colors
        {
            public static readonly Color TableHeader1 = Color.FromArgb(239, 255, 224);
            public static readonly Color TableHeader2 = Color.FromArgb(198, 215, 255);
            public static readonly Color TableFooter1 = Color.FromArgb(198, 215, 255);
            public static readonly Color TableFooter2 = Color.FromArgb(198, 215, 255);
            public static readonly Color SwapLeg = Color.FromArgb(250, 220, 235);
            public static readonly Color Cap = Color.FromArgb(235, 250, 220);
            public static readonly Color Floor = Color.FromArgb(220, 235, 250);
        }

        public class Fonts
        {
            public static readonly Font MainFont = new Font("Microsoft Sans Serif", 6.5f);
        }

        public static class Status
        {
            public const string NEW = "new";
            public const string SAVED = "saved";
            public const string DELETED = "deleted";
        }

        public const string CULTURE_NAME = "en-US";

        public static readonly List<string> SupportedFunctions = new List<string>(new string[] { "Abs", "Acos", "Asin", "Atan", "Ceiling", "Cos", "Exp", "Floor", "IEEERemainder", "Log", "Log10", "Pow", "Round", "Sign", "Sin", "Sqrt", "Tan", "Max", "Min", "if", "in" });
        private static readonly List<string> _loweredFunctions = new List<string>();
        public static List<string> LoweredFunctions
        {
            get
            {
                if (_loweredFunctions.Count == 0) foreach (string function in SupportedFunctions) _loweredFunctions.Add(function.ToLowerInvariant());
                return _loweredFunctions;
            }
        }

        public static Regex VectorOrVariableRegex = new Regex("^[a-zA-Z_]{1}[a-zA-Z0-9_]*$");
        public static string ResultNumericFormat = "###,###,###,###.##";
        public static string ResultNumericFormatNoDec = "###,###,###,###";
        public static string DateFormat = "dd/MM/yyyy";
        public static string[] DateFormat_8Digits = {"dd/MM/yyyy",
                                                     "MM/dd/yyyy",
                                                     "ddMMyyyy",
                                                     "MMddyyyy",
                                                     "d/M/yyyy",
                                                     "M/d/yyyy"};
        public static DateTime MinSqlDate = SqlDateTime.MinValue.Value;
    }
}