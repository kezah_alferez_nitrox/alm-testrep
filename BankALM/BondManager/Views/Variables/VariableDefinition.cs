using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using ALMSCommon;
using BondManager.Core;
using BondManager.Domain;
using BondManager.Properties;
using BondManager.Resources.Language;

namespace BondManager.Views.Variables
{
    public partial class VariableDefinition : SessionForm
    {
        private string cellValue = string.Empty;

        private TrueSortableBindingList<Variable> variableList;

        private Scenario[] scenarios;

        public VariableDefinition()
        {
            this.InitializeComponent();

            this.valuesDataGridView.AutoGenerateColumns = false;
        }

        private void VariableDefinitionLoad(object sender, EventArgs e)
        {
            scenarios = Scenario.FindNonGenerated(this.Session);

            this.variablesGrid.AutoGenerateColumns = false;
            this.columnVectorName.ValueType = typeof(string);
            this.columnVectorDate.ValueType = typeof(DateTime);
            this.InitializeVariables();

            this.RefreshVector(string.Empty);

            if (this.vectorListBox.SelectedItem == null || this.valuesDataGridView.CurrentCell == null)
                this.asociateButton.Enabled = false;
            else
                this.asociateButton.Enabled = true;

            this.ReorderVariables();
        }

        private void InitializeVariables()
        {
            this.variablesGrid.DataSource =
                this.variableList = new TrueSortableBindingList<Variable>(new List<Variable>(Variable.FindAll(this.Session)));
        }

        private void ReorderVariables()
        {
            if (Settings.Default.VariableOrderingColumn >= 0 && Settings.Default.VariableOrderingColumn < this.variablesGrid.Columns.Count)
                this.variablesGrid.Sort(this.variablesGrid.Columns[Settings.Default.VariableOrderingColumn],
                    Settings.Default.VariableOrderingAscending ? ListSortDirection.Ascending : ListSortDirection.Descending);
        }

        private void RefreshVector(string val)
        {
            this.vectorListBox.DisplayMember = "Name";
            this.vectorListBox.ValueMember = "Self";
            this.vectorListBox.DataSource = new BindingList<Vector>(new List<Vector>(VectorCache.FindLikeName(val)));
        }

        private void AddButtonClick(object sender, EventArgs e)
        {
            string variableName = this.newVariableTextBox.Text;
            variableName = Tools.EscapeIdentifier(variableName);

            string message;
            if (!Tools.IsValidIdentifier(variableName, out message))
            {
                MessageBox.Show(message, Labels.VariableDefinition_addButton_Click_Error, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            if (Variable.ExistsByName(this.Session, variableName))
            {
                MessageBox.Show(string.Format(Properties.Resources.VariableAlreadyExists, variableName),
                                Labels.VariableDefinition_addButton_Click_Error, MessageBoxButtons.OK,
                                MessageBoxIcon.Exclamation);
                return;
            }

            Variable variable = this.variableList.AddNew();
            if (variable == null) return;

            variable.CreationDate = DateTime.Now;
            variable.Name = variableName;
            variable.Create(this.Session);
            this.variableList.ResetBindings();

            this.newVariableTextBox.Text = "";
            this.ValuesDataGridResetBindings();

            foreach (DataGridViewRow row in variablesGrid.Rows)
                row.Selected = row.DataBoundItem is Variable && ((Variable)row.DataBoundItem).Id == variable.Id;

            this.ReorderVariables();

            MainForm.ModifiedScenarioTemplate = true;
            MainForm.ModifiedValues = true;
        }

        protected Variable GetSelectedVariable()
        {
            if (this.variablesGrid.SelectedRows.Count != 1) return null;
            Variable variable = this.variablesGrid.SelectedRows[0].DataBoundItem as Variable;
            return variable;
        }
        
        private void RemoveButtonClick(object sender, EventArgs e)
        {
            Variable variable = this.GetSelectedVariable();
            if (variable == null) return;

            if (variable.Selected)
            {
                MessageBox.Show(string.Format(Properties.Resources.ImpossibleToRemoveVariable, variable.Name),
                    "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            if (FundamentalVector.FindAllByProperty(this.Session, "Variable", variable).Length > 0)
            {
                MessageBox.Show(
                    string.Format(Properties.Resources.ImpossibleToRemoveVariableUsedByFundamentalVector, variable.Name),
                    "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            this.variableList.Remove(variable);
            this.variableList.ResetBindings();
            DeleteVariable(variable);

            this.ValuesDataGridResetBindings();

            MainForm.ModifiedValues = true;
        }

        private void DeleteVariable(Variable variable)
        {
            List<VolatilityConfig> volatilityConfigs =
                VolatilityConfig.FindAll(this.Session).Where(vc => vc.Variable == variable).ToList();
            List<VariableCorrelation> variableCorrelations =
                VariableCorrelation.FindAll(this.Session).Where(vc => (vc.Variable1 == variable || vc.Variable2 == variable)).ToList();

            foreach (VariableValue variableValue in variable.VariableValues)
            {
                variableValue.Scenario.VariableValues.Remove(variableValue);
            }

            foreach (VolatilityConfig volatilityConfig in volatilityConfigs)
            {
                volatilityConfig.Delete(this.Session);
            }

            foreach (VariableCorrelation variableCorrelation in variableCorrelations)
            {
                variableCorrelation.Delete(this.Session);
            }

            variable.Delete(this.Session);
            MainForm.ModifiedScenarioTemplate = true;
        }

        private void NewVariableTextBoxTextChanged(object sender, EventArgs e)
        {
            this.addButton.Enabled = !string.IsNullOrEmpty(this.newVariableTextBox.Text);
        }

        private void VariablesListBoxSelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.GetSelectedVariable() == null)
            {
                this.asociateButton.Enabled = false;
                this.valuesDataGridView.Rows.Clear();
            }
            else
                this.asociateButton.Enabled = true;

            this.removeBbutton.Enabled = (this.variablesGrid.SelectedRows.Count >= 0);

            this.ValuesDataGridResetBindings();
        }

        private void ValuesDataGridResetBindings()
        {
            this.valuesDataGridView.DataBindings.Clear();
            Variable variable = this.GetSelectedVariable();
            if (variable == null) return;

            if (variable.VariableValues.Count != scenarios.Length)
            {
                AddValuesForAllSelectedScenarios(variable);
            }

            VariableValue[] values = variable.VariableValues.Where(vv=>!vv.Scenario.Generated).OrderBy(v=>v.Scenario.Position).ToArray();
            this.valuesDataGridView.DataSource = new BindingList<VariableValue>(new List<VariableValue>(values));
        }

        private void AddValuesForAllSelectedScenarios(Variable variable)
        {
            foreach (Scenario scenario in scenarios)
            {
                if (!HasValueForScenario(variable, scenario))
                {
                    VariableValue value = new VariableValue();
                    value.Scenario = scenario;
                    value.Variable = variable;
                    variable.VariableValues.Add(value);
                    scenario.VariableValues.Add(value);
                    value.Save(this.Session);
                }
            }
        }

        private static bool HasValueForScenario(Variable variable, Scenario scenario)
        {
            foreach (VariableValue value in variable.VariableValues)
            {
                if (value.Scenario == scenario)
                {
                    return true;
                }
            }

            return false;
        }

        private void VariableDefinitionActivated(object sender, EventArgs e)
        {
            this.newVariableTextBox.Focus();
        }

        private void OkButtonClick(object sender, EventArgs e)
        {
            //PersistenceSession.Flush();
            this.SaveChangesInSession();
            MainForm.ModifiedValues = true;
        }

        private void VectorFilterTextBoxKeyUp(object sender, KeyEventArgs e)
        {
            this.RefreshVector(((TextBox)sender).Text);
        }

        private void AsociateButtonClick(object sender, EventArgs e)
        {
            DataGridViewCell currentcell =
                this.valuesDataGridView[this.valueColumn.Index, this.valuesDataGridView.CurrentCell.RowIndex];
            if (this.vectorListBox.SelectedItem != null)
            {
                currentcell.Value = currentcell.Value + ((Vector) this.vectorListBox.SelectedItem).Name;
            }
        }

        private void ValuesDataGridViewSelectionChanged(object sender, EventArgs e)
        {
            if (this.valuesDataGridView.CurrentCell == null)
                this.asociateButton.Enabled = false;
            else
                this.asociateButton.Enabled = true;
        }

        private void ValuesDataGridViewCellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                //Scenario scenario = scenarios[e.RowIndex];

                //// todo e.ColumnIndex n'est-il pas toujours �gal � 0 ?
                //Variable variable = variableList[e.ColumnIndex - 1];

                //VariableValue value = VariableValue.Find(variable, scenario);

                //if (this.cellValue != value.Value)
                //{
                //  MainForm.ModifiedScenarioTemplate = true;
                //}
            }
        }

        private void ValuesDataGridViewCellEnter(object sender, DataGridViewCellEventArgs e)
        {
            this.cellValue = Convert.ToString(this.valuesDataGridView[e.ColumnIndex, e.RowIndex].Value);
        }

        private void CopyVariable(object sender, EventArgs e)
        {
            DataGridViewRow selectedRow = valuesDataGridView.SelectedCells.OfType<DataGridViewCell>().Select(c => c.OwningRow).Distinct().FirstOrDefault();

            if (selectedRow != null)
            {
                bool above = sender == copyAboveToolStripMenuItem;
                object value = selectedRow.Cells[valueColumn.Index].Value;

                int start, end;
                if (above)
                {
                    start = 0;
                    end = selectedRow.Index;
                }
                else
                {
                    start = selectedRow.Index + 1;
                    end = valuesDataGridView.Rows.Count;
                }

                for (int i = start; i < end; i++)
                    valuesDataGridView.Rows[i].Cells[valueColumn.Index].Value = value;
            }
        }

        private void ScenarioClick(object sender, MouseEventArgs e)
        {
            DataGridView.HitTestInfo hit = this.valuesDataGridView.HitTest(e.X, e.Y);

            if (hit.ColumnIndex < 0 || hit.RowIndex < 0) return;
            this.valuesDataGridView.ClearSelection();
            DataGridViewCell cell = this.valuesDataGridView[hit.ColumnIndex, hit.RowIndex];
            cell.Selected = true;
        }

        private void ChangeColumnSort(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.ColumnIndex < 0) return;

            DataGridViewColumn column = variablesGrid.Columns[e.ColumnIndex];
            Settings.Default.VariableOrderingColumn = column.Index;
            Settings.Default.VariableOrderingAscending = variablesGrid.SortOrder != SortOrder.Descending;
        }

        private void CloneVariable(object sender, EventArgs e)
        {
            CopyVariable form = new CopyVariable();
            if (form.ShowDialog() == DialogResult.OK)
            {
                this.InitializeVariables();
                this.ReorderVariables();
            }
        }
    }
}