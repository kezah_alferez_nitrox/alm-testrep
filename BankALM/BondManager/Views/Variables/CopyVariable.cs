﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ALMSCommon.Forms;
using BondManager.Core;
using BondManager.Domain;
using BondManager.Resources.Language;

namespace BondManager.Views.Variables
{
    public partial class CopyVariable : SessionForm // todo faire heriter session for; de dpi from
    {
        private bool valid;

        public string VariableName
        {
            get
            {
                return txtVariableName.Text;
            }
        }

        public Variable[] Variables { get; set; }
        public Variable SelectedVariable
        {
            get 
            {
                if (!(comboBaseVariable.SelectedItem is KeyValuePair<Variable, string>)) return null;
                KeyValuePair<Variable, string> pair = (KeyValuePair<Variable, string>)comboBaseVariable.SelectedItem;
                return pair.Key;
            }
        }
        public string OffsetFormula { get { return this.txtOffsetFormula.Text ?? ""; } }

        public CopyVariable()
        {
            InitializeComponent();

            this.Variables = Variable.FindAll(this.Session);
            if (Variables != null)
            {
                comboBaseVariable.Items.Clear();
                foreach (Variable variable in Variables.OrderBy(v => v.Name))
                {
                    KeyValuePair<Variable, string> pair = new KeyValuePair<Variable, string>(variable, variable.Name);
                    comboBaseVariable.Items.Add(pair);
                }
                comboBaseVariable.DisplayMember = "Value";
                comboBaseVariable.ValueMember = "Key";
            }
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            if (Variable.ExistsByName(this.Session, txtVariableName.Text))
            {
                MessageBox.Show(string.Format(Properties.Resources.VariableAlreadyExists, txtVariableName.Text),
                    Labels.VariableDefinition_addButton_Click_Error, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                valid = false;
                return;
            }

            Variable variable = SelectedVariable;
            if (variable != null)
            {
                Variable variableCopy = new Variable();
                variableCopy.Save(this.Session);
                //if (variableCopy == null) return;

                variableCopy.Name = VariableName;
                variableCopy.CreationDate = DateTime.Now;
                variableCopy.Selected = false;
                variableCopy.VariableType = variable.VariableType;

                foreach (VariableValue value in VariableValue.FindByVariable(this.Session, variable))
                {
                    VariableValue variableValueCopy = new VariableValue();
                    variableValueCopy.Value = value.Value + OffsetFormula;
                    variableValueCopy.Variable = value.Variable;
                    variableValueCopy.Variable = variableCopy;
                    variableValueCopy.Scenario = value.Scenario;

                    variableCopy.VariableValues.Add(variableValueCopy);
                }

                MainForm.ModifiedScenarioTemplate = true;
                MainForm.ModifiedValues = true;

                this.Session.Flush();
            }

            valid = true;
        }

        private void CreateVariable_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = !valid;
        }

        private void cancelButton_Click(object sender, System.EventArgs e)
        {
            valid = true;
        }

        private void variableNameTextBox_TextChanged(object sender, System.EventArgs e)
        {
            okButton.Enabled = !string.IsNullOrEmpty(txtVariableName.Text);
        }
    }
}
