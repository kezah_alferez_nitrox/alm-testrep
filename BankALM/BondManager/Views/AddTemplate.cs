﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace BondManager
{
    public partial class AddTemplate : Form
    {
        private bool accept;

        public string Name
        {
            get { return this.txtName.Text; }
            set { this.txtName.Text = value; }
        }

        public bool SendTemplate
        {
            get { return this.checkSendTemplate.Checked; }
            set { this.checkSendTemplate.Checked = value; }
        }

        public AddTemplate()
        {
            InitializeComponent();
            this.Name = "";
            this.accept = false;
        }

        private void txtName_TextChanged(object sender, EventArgs e)
        {
            buttonAdd.Enabled = !string.IsNullOrEmpty(txtName.Text);
        }

        private void Closing(object sender, FormClosingEventArgs e)
        {
            if (this.accept)
            {
                char[] invalidChar = System.IO.Path.GetInvalidFileNameChars();
                e.Cancel = true;
                try
                {
                    new System.IO.FileInfo(this.Name);
                    if (Name != null && Name.IndexOfAny(invalidChar) == -1) e.Cancel = false;
                }
                catch (ArgumentException)
                {
                }
                catch (System.IO.PathTooLongException)
                {
                }
                catch (NotSupportedException)
                {
                }

                if (e.Cancel)
                {
                    this.accept = false;
                    MessageBox.Show("Invalid file name", "Add template", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
        }

        private void Add(object sender, EventArgs e)
        {
            this.accept = true;
        }
    }
}
