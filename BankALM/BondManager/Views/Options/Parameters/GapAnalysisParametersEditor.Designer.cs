﻿namespace BondManager.Views.Options.Parameters
{
    partial class GapAnalysisParametersEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.computeGapCheckBox = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.gapType = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.fixedGapRate = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.cashflowPresentation = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // computeGapCheckBox
            // 
            this.computeGapCheckBox.AutoSize = true;
            this.computeGapCheckBox.Location = new System.Drawing.Point(121, 12);
            this.computeGapCheckBox.Name = "computeGapCheckBox";
            this.computeGapCheckBox.Size = new System.Drawing.Size(91, 17);
            this.computeGapCheckBox.TabIndex = 0;
            this.computeGapCheckBox.Text = "Compute Gap";
            this.computeGapCheckBox.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Gap type";
            // 
            // gapType
            // 
            this.gapType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.gapType.FormattingEnabled = true;
            this.gapType.Items.AddRange(new object[] {
            "Liquidity",
            "Fixed Interest Rate"});
            this.gapType.Location = new System.Drawing.Point(121, 39);
            this.gapType.Name = "gapType";
            this.gapType.Size = new System.Drawing.Size(119, 21);
            this.gapType.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 72);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(97, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Actualisation factor";
            this.label2.Visible = false;
            // 
            // comboBox2
            // 
            this.comboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Items.AddRange(new object[] {
            "Fixed",
            "Forward"});
            this.comboBox2.Location = new System.Drawing.Point(121, 69);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(119, 21);
            this.comboBox2.TabIndex = 4;
            this.comboBox2.Visible = false;
            // 
            // fixedGapRate
            // 
            this.fixedGapRate.Location = new System.Drawing.Point(121, 96);
            this.fixedGapRate.Name = "fixedGapRate";
            this.fixedGapRate.Size = new System.Drawing.Size(119, 20);
            this.fixedGapRate.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(85, 103);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(30, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Rate";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(158, 176);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(82, 28);
            this.button1.TabIndex = 9;
            this.button1.Text = "Ok";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(4, 133);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(111, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Cashflow presentation";
            // 
            // cashflowPresentation
            // 
            this.cashflowPresentation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cashflowPresentation.FormattingEnabled = true;
            this.cashflowPresentation.Items.AddRange(new object[] {
            "Stock",
            "Treasury"});
            this.cashflowPresentation.Location = new System.Drawing.Point(121, 130);
            this.cashflowPresentation.Name = "cashflowPresentation";
            this.cashflowPresentation.Size = new System.Drawing.Size(119, 21);
            this.cashflowPresentation.TabIndex = 11;
            // 
            // GapAnalysisParametersEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(254, 227);
            this.Controls.Add(this.cashflowPresentation);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.fixedGapRate);
            this.Controls.Add(this.comboBox2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.gapType);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.computeGapCheckBox);
            this.Name = "GapAnalysisParametersEditor";
            this.Text = "Gap Analysis Parameters Editor";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.CheckBox computeGapCheckBox;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.ComboBox gapType;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboBox2;
        public System.Windows.Forms.TextBox fixedGapRate;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.ComboBox cashflowPresentation;
    }
}