﻿using ALMSCommon;
using BondManager.Resources.Language;

namespace BondManager.Views.Options.Parameters
{
    partial class ParametersEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonOk = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCancel = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label16 = new System.Windows.Forms.Label();
            this.dateValidationMsgCheckbox = new System.Windows.Forms.CheckBox();
            this.label15 = new System.Windows.Forms.Label();
            this.originModelCheckbox = new System.Windows.Forms.CheckBox();
            this.debugLogLevelCheckBox = new System.Windows.Forms.CheckBox();
            this.label14 = new System.Windows.Forms.Label();
            this.computeAnalysisCategoriesCheckBox = new System.Windows.Forms.CheckBox();
            this.label13 = new System.Windows.Forms.Label();
            this.linkSwaps2by2DuringImport = new System.Windows.Forms.CheckBox();
            this.label12 = new System.Windows.Forms.Label();
            this.importDeletesMissingProducts = new System.Windows.Forms.CheckBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.RecoveryLagInMonths = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.confirmBeforeDelete = new System.Windows.Forms.CheckBox();
            this.label8 = new System.Windows.Forms.Label();
            this.OCIManagementComboBox = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.btnBrowseLogo = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.perProductResultDetailsCheckBox = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.optimiseForComboBox = new System.Windows.Forms.ComboBox();
            this.autoBookPriceCheckBox = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dividendComboBox = new System.Windows.Forms.ComboBox();
            this.maxDurationNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RecoveryLagInMonths)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.maxDurationNumericUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonOk
            // 
            this.buttonOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonOk.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonOk.Location = new System.Drawing.Point(99, 13);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(116, 32);
            this.buttonOk.TabIndex = 1;
            this.buttonOk.Text = global::BondManager.Resources.Language.Labels.ParametersEditor_InitializeComponent_OK;
            this.buttonOk.UseVisualStyleBackColor = true;
            this.buttonOk.Click += new System.EventHandler(this.ButtonOkClick);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnCancel);
            this.panel1.Controls.Add(this.buttonOk);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 437);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(356, 57);
            this.panel1.TabIndex = 2;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancel.Location = new System.Drawing.Point(230, 13);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(116, 32);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = global::BondManager.Resources.Language.Labels.ParametersEditor_InitializeComponent_Cancel;
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.BtnCancelClick);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.dateValidationMsgCheckbox);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.originModelCheckbox);
            this.groupBox1.Controls.Add(this.debugLogLevelCheckBox);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.computeAnalysisCategoriesCheckBox);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.linkSwaps2by2DuringImport);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.importDeletesMissingProducts);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.RecoveryLagInMonths);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.confirmBeforeDelete);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.OCIManagementComboBox);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.btnBrowseLogo);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.perProductResultDetailsCheckBox);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.optimiseForComboBox);
            this.groupBox1.Controls.Add(this.autoBookPriceCheckBox);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.dividendComboBox);
            this.groupBox1.Controls.Add(this.maxDurationNumericUpDown);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(356, 437);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Parameters";
            // 
            // label16
            // 
            this.label16.Location = new System.Drawing.Point(12, 388);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(157, 38);
            this.label16.TabIndex = 31;
            this.label16.Text = "Do not show Date Validation pop-up message";
            // 
            // dateValidationMsgCheckbox
            // 
            this.dateValidationMsgCheckbox.AutoSize = true;
            this.dateValidationMsgCheckbox.Location = new System.Drawing.Point(176, 388);
            this.dateValidationMsgCheckbox.Name = "dateValidationMsgCheckbox";
            this.dateValidationMsgCheckbox.Size = new System.Drawing.Size(15, 14);
            this.dateValidationMsgCheckbox.TabIndex = 30;
            this.dateValidationMsgCheckbox.UseVisualStyleBackColor = true;
            // 
            // label15
            // 
            this.label15.Location = new System.Drawing.Point(12, 353);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(157, 38);
            this.label15.TabIndex = 29;
            this.label15.Text = "CDR, CPR && LGD formulas origin based on Start Date";
            // 
            // originModelCheckbox
            // 
            this.originModelCheckbox.AutoSize = true;
            this.originModelCheckbox.Location = new System.Drawing.Point(176, 352);
            this.originModelCheckbox.Name = "originModelCheckbox";
            this.originModelCheckbox.Size = new System.Drawing.Size(15, 14);
            this.originModelCheckbox.TabIndex = 28;
            this.originModelCheckbox.UseVisualStyleBackColor = true;
            // 
            // debugLogLevelCheckBox
            // 
            this.debugLogLevelCheckBox.AutoSize = true;
            this.debugLogLevelCheckBox.Location = new System.Drawing.Point(176, 332);
            this.debugLogLevelCheckBox.Name = "debugLogLevelCheckBox";
            this.debugLogLevelCheckBox.Size = new System.Drawing.Size(15, 14);
            this.debugLogLevelCheckBox.TabIndex = 27;
            this.debugLogLevelCheckBox.UseVisualStyleBackColor = true;
            this.debugLogLevelCheckBox.CheckedChanged += new System.EventHandler(this.DebugLogLevelCheckBox_CheckedChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(12, 332);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(89, 13);
            this.label14.TabIndex = 26;
            this.label14.Text = "Debug Log Level";
            // 
            // computeAnalysisCategoriesCheckBox
            // 
            this.computeAnalysisCategoriesCheckBox.AutoSize = true;
            this.computeAnalysisCategoriesCheckBox.Location = new System.Drawing.Point(176, 312);
            this.computeAnalysisCategoriesCheckBox.Name = "computeAnalysisCategoriesCheckBox";
            this.computeAnalysisCategoriesCheckBox.Size = new System.Drawing.Size(15, 14);
            this.computeAnalysisCategoriesCheckBox.TabIndex = 25;
            this.computeAnalysisCategoriesCheckBox.UseVisualStyleBackColor = true;
            // 
            // label13
            // 
            this.label13.Location = new System.Drawing.Point(12, 312);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(168, 33);
            this.label13.TabIndex = 24;
            this.label13.Text = "Compute Analysis Categories";
            // 
            // linkSwaps2by2DuringImport
            // 
            this.linkSwaps2by2DuringImport.AutoSize = true;
            this.linkSwaps2by2DuringImport.Location = new System.Drawing.Point(176, 279);
            this.linkSwaps2by2DuringImport.Name = "linkSwaps2by2DuringImport";
            this.linkSwaps2by2DuringImport.Size = new System.Drawing.Size(15, 14);
            this.linkSwaps2by2DuringImport.TabIndex = 23;
            this.linkSwaps2by2DuringImport.UseVisualStyleBackColor = true;
            // 
            // label12
            // 
            this.label12.Location = new System.Drawing.Point(12, 279);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(168, 33);
            this.label12.TabIndex = 22;
            this.label12.Text = "Link swaps 2 by 2 following order during import";
            // 
            // importDeletesMissingProducts
            // 
            this.importDeletesMissingProducts.AutoSize = true;
            this.importDeletesMissingProducts.Location = new System.Drawing.Point(176, 255);
            this.importDeletesMissingProducts.Name = "importDeletesMissingProducts";
            this.importDeletesMissingProducts.Size = new System.Drawing.Size(15, 14);
            this.importDeletesMissingProducts.TabIndex = 21;
            this.importDeletesMissingProducts.UseVisualStyleBackColor = true;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(12, 255);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(157, 13);
            this.label11.TabIndex = 20;
            this.label11.Text = "Import deletes missing products ";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(303, 254);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(41, 13);
            this.label10.TabIndex = 19;
            this.label10.Text = "months";
            this.label10.Visible = false;
            // 
            // RecoveryLagInMonths
            // 
            this.RecoveryLagInMonths.Location = new System.Drawing.Point(176, 252);
            this.RecoveryLagInMonths.Maximum = new decimal(new int[] {
            12000,
            0,
            0,
            0});
            this.RecoveryLagInMonths.Name = "RecoveryLagInMonths";
            this.RecoveryLagInMonths.Size = new System.Drawing.Size(121, 20);
            this.RecoveryLagInMonths.TabIndex = 18;
            this.RecoveryLagInMonths.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.RecoveryLagInMonths.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.RecoveryLagInMonths.Visible = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(12, 254);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(107, 13);
            this.label9.TabIndex = 17;
            this.label9.Text = "Default Recovery lag";
            this.label9.Visible = false;
            // 
            // confirmBeforeDelete
            // 
            this.confirmBeforeDelete.AutoSize = true;
            this.confirmBeforeDelete.Location = new System.Drawing.Point(176, 229);
            this.confirmBeforeDelete.Name = "confirmBeforeDelete";
            this.confirmBeforeDelete.Size = new System.Drawing.Size(15, 14);
            this.confirmBeforeDelete.TabIndex = 16;
            this.confirmBeforeDelete.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 229);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(143, 13);
            this.label8.TabIndex = 15;
            this.label8.Text = "Confirm before deleting a line";
            // 
            // OCIManagementComboBox
            // 
            this.OCIManagementComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.OCIManagementComboBox.FormattingEnabled = true;
            this.OCIManagementComboBox.Items.AddRange(new object[] {
            "As Equity",
            "As Liability"});
            this.OCIManagementComboBox.Location = new System.Drawing.Point(176, 199);
            this.OCIManagementComboBox.Name = "OCIManagementComboBox";
            this.OCIManagementComboBox.Size = new System.Drawing.Size(121, 21);
            this.OCIManagementComboBox.TabIndex = 14;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 199);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(90, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "OCI Management";
            // 
            // btnBrowseLogo
            // 
            this.btnBrowseLogo.Location = new System.Drawing.Point(176, 164);
            this.btnBrowseLogo.Name = "btnBrowseLogo";
            this.btnBrowseLogo.Size = new System.Drawing.Size(168, 23);
            this.btnBrowseLogo.TabIndex = 12;
            this.btnBrowseLogo.Text = "Browse image ...";
            this.btnBrowseLogo.UseVisualStyleBackColor = true;
            this.btnBrowseLogo.Click += new System.EventHandler(this.BrowseLogo);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 169);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(85, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Workspace logo";
            // 
            // perProductResultDetailsCheckBox
            // 
            this.perProductResultDetailsCheckBox.AutoSize = true;
            this.perProductResultDetailsCheckBox.Location = new System.Drawing.Point(176, 128);
            this.perProductResultDetailsCheckBox.Name = "perProductResultDetailsCheckBox";
            this.perProductResultDetailsCheckBox.Size = new System.Drawing.Size(15, 14);
            this.perProductResultDetailsCheckBox.TabIndex = 9;
            this.perProductResultDetailsCheckBox.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 129);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(123, 26);
            this.label5.TabIndex = 8;
            this.label5.Text = "Per product result details\n(uses more memory)";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 104);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(62, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Optimise for";
            // 
            // optimiseForComboBox
            // 
            this.optimiseForComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.optimiseForComboBox.FormattingEnabled = true;
            this.optimiseForComboBox.Items.AddRange(new object[] {
            "Speed",
            "Memory"});
            this.optimiseForComboBox.Location = new System.Drawing.Point(176, 101);
            this.optimiseForComboBox.Name = "optimiseForComboBox";
            this.optimiseForComboBox.Size = new System.Drawing.Size(168, 21);
            this.optimiseForComboBox.TabIndex = 6;
            // 
            // autoBookPriceCheckBox
            // 
            this.autoBookPriceCheckBox.AutoSize = true;
            this.autoBookPriceCheckBox.Location = new System.Drawing.Point(176, 81);
            this.autoBookPriceCheckBox.Name = "autoBookPriceCheckBox";
            this.autoBookPriceCheckBox.Size = new System.Drawing.Size(15, 14);
            this.autoBookPriceCheckBox.TabIndex = 5;
            this.autoBookPriceCheckBox.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 69);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(143, 26);
            this.label3.TabIndex = 4;
            this.label3.Text = "Automatically calculate\nBook Price in Balance Sheet";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(126, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Dividend Payment Month";
            // 
            // dividendComboBox
            // 
            this.dividendComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.dividendComboBox.FormattingEnabled = true;
            this.dividendComboBox.Items.AddRange(new object[] {
            global::BondManager.Resources.Language.Labels.ParametersEditor_InitializeComponent_January,
            global::BondManager.Resources.Language.Labels.ParametersEditor_InitializeComponent_February,
            global::BondManager.Resources.Language.Labels.ParametersEditor_InitializeComponent_March,
            global::BondManager.Resources.Language.Labels.ParametersEditor_InitializeComponent_April,
            global::BondManager.Resources.Language.Labels.ParametersEditor_InitializeComponent_May,
            global::BondManager.Resources.Language.Labels.ParametersEditor_InitializeComponent_June,
            global::BondManager.Resources.Language.Labels.ParametersEditor_InitializeComponent_July,
            global::BondManager.Resources.Language.Labels.ParametersEditor_InitializeComponent_August,
            global::BondManager.Resources.Language.Labels.ParametersEditor_InitializeComponent_September,
            global::BondManager.Resources.Language.Labels.ParametersEditor_InitializeComponent_October,
            global::BondManager.Resources.Language.Labels.ParametersEditor_InitializeComponent_November,
            global::BondManager.Resources.Language.Labels.ParametersEditor_InitializeComponent_December});
            this.dividendComboBox.Location = new System.Drawing.Point(176, 44);
            this.dividendComboBox.Name = "dividendComboBox";
            this.dividendComboBox.Size = new System.Drawing.Size(168, 21);
            this.dividendComboBox.TabIndex = 2;
            // 
            // maxDurationNumericUpDown
            // 
            this.maxDurationNumericUpDown.Location = new System.Drawing.Point(176, 18);
            this.maxDurationNumericUpDown.Maximum = new decimal(new int[] {
            12000,
            0,
            0,
            0});
            this.maxDurationNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.maxDurationNumericUpDown.Name = "maxDurationNumericUpDown";
            this.maxDurationNumericUpDown.Size = new System.Drawing.Size(168, 20);
            this.maxDurationNumericUpDown.TabIndex = 1;
            this.maxDurationNumericUpDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.maxDurationNumericUpDown.Value = new decimal(new int[] {
            120,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Max Duration";
            // 
            // ParametersEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(356, 494);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "ParametersEditor";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Parameters";
            this.panel1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RecoveryLagInMonths)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.maxDurationNumericUpDown)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonOk;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox dividendComboBox;
        private System.Windows.Forms.NumericUpDown maxDurationNumericUpDown;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox autoBookPriceCheckBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox optimiseForComboBox;
        private System.Windows.Forms.CheckBox perProductResultDetailsCheckBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnBrowseLogo;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox OCIManagementComboBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox confirmBeforeDelete;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.NumericUpDown RecoveryLagInMonths;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.CheckBox importDeletesMissingProducts;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.CheckBox linkSwaps2by2DuringImport;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.CheckBox computeAnalysisCategoriesCheckBox;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.CheckBox debugLogLevelCheckBox;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.CheckBox originModelCheckbox;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.CheckBox dateValidationMsgCheckbox;
    }
}