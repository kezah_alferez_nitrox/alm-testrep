using System;
using System.Linq;
using System.Collections.Generic;
using System.Windows.Forms;
using ALMSCommon;
using ALMSCommon.Controls;
using BondManager.BusinessLogic;
using BondManager.Core;
using BondManager.Resources.Language;

using BondManager.Domain;
using NHibernate;
using NHibernate.Linq;

namespace BondManager.Views.Options.FxRates
{
    public partial class FxRatesEditor : SessionForm
    {
        private const string NameColumnName = "Name";
        private const string SymbolColumnName = "Symbol";
        private const string ValueColumnName = "Value";

        private Currency valuationCurrency;

        public FxRatesEditor()
        {
            InitializeComponent();

            currenciesGrid.AutoGenerateColumns = false;
            currenciesGrid.EditMode = DataGridViewEditMode.EditOnEnter;
            currenciesGrid.ContextMenuStrip = gridContextMenuStrip;
        }

        private void InitColumns()
        {
            string[] allVariablesAndVectorNames = DomainTools.GetAllVariablesAndVectorNames(this.Session);

            DataGridViewTextBoxColumn nameColumn = new DataGridViewTextBoxColumn();
            nameColumn.Name = NameColumnName;
            nameColumn.HeaderText = Labels.FxRatesEditor_InitColumns_Name__used_for_import_;
            nameColumn.Width = 140;

            DataGridViewTextBoxColumn symbolColumn = new DataGridViewTextBoxColumn();
            symbolColumn.Name = SymbolColumnName;
            symbolColumn.HeaderText = SymbolColumnName;
            symbolColumn.Width = 70;

            AutoCompleteColumn valueColumn = new AutoCompleteColumn();
            valueColumn.Name = ValueColumnName;
            valueColumn.HeaderText = Labels.FxRatesEditor_InitColumns_Rate_ + valuationCurrency.Symbol;
            valueColumn.Width = 180;
            valueColumn.ValueList = allVariablesAndVectorNames;

            currenciesGrid.Columns.Clear();
            currenciesGrid.Columns.AddRange(new DataGridViewColumn[] { nameColumn, symbolColumn, valueColumn });
        }

        private void CurrenciesLoad(object sender, EventArgs e)
        {
            Reload();
            currenciesGrid.CellValueChanged += currenciesGrid_CellValueChanged;
        }

        private void currenciesGrid_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            MainForm.ModifiedValues = true;
        }

        private void Reload()
        {
            valuationCurrency = Param.GetValuationCurrency(this.Session);

            Currency[] allCurrencies = Currency.FindAllNotEmpty(this.Session);

            InitColumns();
            if (allCurrencies.Length <= 1) return;

            currenciesGrid.Rows.Add(allCurrencies.Length - 1);

            int i = 0;
            foreach (Currency currency in allCurrencies)
            {
                if (this.valuationCurrency != currency)
                {
                    FillNewRow(currency, i);
                    i++;
                }
            }
        }

        private void FillNewRow(Currency currency, int index)
        {
            FXRate fxRate = null;
            if (currency == null)
            {
                currency = new Currency();
            }
            else
            {
                fxRate = FXRate.GetRate(this.Session, currency, this.valuationCurrency);
            }
            if (fxRate == null)
            {
                fxRate = new FXRate();
                fxRate.From = currency;
                fxRate.To = this.valuationCurrency;
            }

            currenciesGrid[NameColumnName, index].Value = currency.ShortName;
            currenciesGrid[SymbolColumnName, index].Value = currency.Symbol;
            currenciesGrid[ValueColumnName, index].Value = fxRate.Value;
            currenciesGrid.Rows[index].Tag = fxRate;
        }

        private void CloseButtonClick(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            foreach (DataGridViewRow row in currenciesGrid.Rows)
            {
                FXRate rate = row.Tag as FXRate;
                if (rate == null) continue;
                string name = (string)row.Cells[NameColumnName].Value;
                string symbol = (string)row.Cells[SymbolColumnName].Value;

                if (string.IsNullOrEmpty(symbol) && !string.IsNullOrEmpty(name)) symbol = name;
                if (string.IsNullOrEmpty(name) && !string.IsNullOrEmpty(symbol)) name = symbol;

                if (!string.IsNullOrEmpty(symbol))
                {
                    rate.From.ShortName = name;
                    rate.From.Symbol = symbol;
                    rate.From.Save(this.Session);
                    rate.Value = (string)row.Cells[ValueColumnName].Value;
                    rate.Save(this.Session);
                }
            }

            this.SaveChangesInSession();

            BalanceBL.EnsureSpecialCategoriesAndBondsExist();
            Cursor = Cursors.Arrow;
        }

        private void AddButtonClick(object sender, EventArgs e)
        {
            AddNewRowsPrompt addNewRowsPrompt = new AddNewRowsPrompt();
            addNewRowsPrompt.Text = Labels.FxRatesEditor_addButton_Click_Add_currencies;
            if (addNewRowsPrompt.ShowDialog() == DialogResult.OK)
            {
                AddRows(addNewRowsPrompt.RowCount);
                MainForm.ModifiedValues = true;
            }
        }

        private void AddRows(int count)
        {
            for (int i = 0; i < count; i++)
            {
                int index = currenciesGrid.Rows.Add();
                FillNewRow(null, index);
            }
        }

        private void DeleteToolStripMenuItemClick(object sender, EventArgs e)
        {
            IEnumerable<FXRate> fxRates = currenciesGrid.SelectedRows.
                Cast<DataGridViewRow>().
                Select(row => row.Tag as FXRate).
                Where(fx => fx != null);

            if (!fxRates.Any()) return;

            IEnumerable<FXRate> undeletable = fxRates.Where(fx => !this.CanDeleteCurrency(fx.From));
            if (undeletable.Any())
            {
                string names = String.Join(",", undeletable.Select(fx => fx.From.ToString()));
                Dialogs.Error("Delete Curencies", string.Format("Can't delete {0} because it's used by a product or a fundamental vector", names));
                return;
            }

            string namesToDelete = String.Join(",", fxRates.Select(fx => fx.From.ToString()));
            if (!Dialogs.Confirm("Delete Currency", "Delete " + namesToDelete + " ?"))
            {
                return;
            }

            foreach (FXRate fxRate in fxRates)
            {
                Currency currency = fxRate.From;

                CurrencyBL.DeleteCurrency(this.Session, currency);

                DataGridViewRow rowToDelete = currenciesGrid.Rows.
                    Cast<DataGridViewRow>().
                    First(row => row.Tag as FXRate == fxRate);
                currenciesGrid.Rows.Remove(rowToDelete);
            }
        }

        private bool CanDeleteCurrency(Currency currency)
        {
            return !ProductCache.products.//this.Session.Query<Product>().
                Any(p => !(p.Category.Type == CategoryType.Treasury ||
                            p.Category.Type == CategoryType.Receivable ||
                            p.Category.Type == CategoryType.Payable) &&
                        p.Currency == currency) &&
                !this.Session.Query<FundamentalVector>().Any(fv => fv.Currency == currency);
        }

        private void CurrenciesGridRowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            currenciesGrid.EndEdit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DeleteToolStripMenuItemClick(sender, e);
        }
    }
}