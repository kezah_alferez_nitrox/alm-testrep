﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using ALMSCommon;
using ALMSCommon.Controls;
using ALMSCommon.Forms;
using BankALM.Infrastructure.Data;
using BondManager.BusinessLogic;
using BondManager.Calculations;
using BondManager.Core;
using BondManager.Domain;
using BondManager.Generator;
using BondManager.Properties;
using BondManager.Resources.Language;

using Eu.AlterSystems.ASNetLib.Core;
using NHibernate;

namespace BondManager.Views.Generator
{
    public partial class MultiScenarioParameters : SessionUserControl
    {
        private readonly IList<int> ignoredVariableindexes = new List<int>();
        private double[,] correlationMatrix;
        private ScenarioVariablesEvaluator evaluator;
        private bool fillingGrid;
        private DataGridViewCheckBoxColumn generatedColumn;
        private DataGridViewTextBoxColumn halfLifeColumn;
        private bool loaded;
        private DataGridViewCheckBoxColumn logColumn;
        private AutoCompleteColumn meanColumn;

        private Scenario selectedScenario;
        private Simulation simulation;
        private DataGridViewComboBoxColumn simulationModeColumn;
        private DataGridViewTextBoxColumn variableColumn;
        private double[,] variableValues;
        private Variable[] variables;
        private BindingList<VolatilityConfig> volatilities;
        private DataGridViewTextBoxColumn volatitlityFactorColumn;

        

        public MultiScenarioParameters()
        {
            this.InitializeComponent();

            if (this.DesignMode || Tools.InDesignMode) return;

            this.correlationsDataGridView.ColumnHeadersHeightSizeMode =
                this.volatilitiesDataGridView.ColumnHeadersHeightSizeMode =
                DataGridViewColumnHeadersHeightSizeMode.DisableResizing;

            this.correlationsDataGridView.RowHeightChanged += this.CorrelationsDataGridViewRowHeightChanged;
            this.volatilitiesDataGridView.RowHeightChanged += this.VolatilitiesDataGridViewRowHeightChanged;

            this.volatilitiesDataGridView.ColumnHeadersHeight += this.correlationsLabel.Height;

            this.correlationsDataGridView.CellValidating += CorrelationsDataGridViewCellValidating;
            this.volatilitiesDataGridView.CellValidating += VolatilitiesDataGridViewCellValidating;

            this.volatilitiesDataGridView.CellMouseUp += this.VolatilitiesDataGridViewCellMouseUp;
            this.volatilitiesDataGridView.CellValueChanged += this.VolatilitiesDataGridViewCellValueChanged;

            this.correlationsDataGridView.CellValueChanged += this.CorrelationsDataGridViewCellValueChanged;
        }

        public Simulation Simulation
        {
            get { return this.simulation; }
        }

        public event EventHandler DisposeOldScenario;

        private void VolatilitiesDataGridViewCellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex == 0)
            {
                this.volatilitiesDataGridView.EndEdit();
            }
        }

        private static void VolatilitiesDataGridViewCellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (e.ColumnIndex == 1)
            {
                double tmp;
                if (!double.TryParse((string)e.FormattedValue, out tmp))
                {
                    e.Cancel = true;
                }
            }
        }

        private static void CorrelationsDataGridViewCellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            double tmp;
            if (!double.TryParse((string)e.FormattedValue, out tmp))
            {
                e.Cancel = true;
            }
        }

        private void VolatilitiesDataGridViewRowHeightChanged(object sender, DataGridViewRowEventArgs e)
        {
            this.correlationsDataGridView.Rows[e.Row.Index].Height = e.Row.Height;
        }

        private void CorrelationsDataGridViewRowHeightChanged(object sender, DataGridViewRowEventArgs e)
        {
            this.volatilitiesDataGridView.Rows[e.Row.Index].Height = e.Row.Height;
        }

        private void VolatilitiesDataGridViewCellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0) return;

            if (e.ColumnIndex == this.generatedColumn.Index)
            {
                this.OnVolatilitiesCheckChanged();
            }
            else if (e.ColumnIndex == this.simulationModeColumn.Index)
            {
                this.CheckHalfLife(e.RowIndex);
            }

            MainForm.ModifiedValues = true;
            this.CheckPositiveDefinite();
        }

        private void CheckHalfLife(int rowIndex)
        {
            DataGridViewCell cell = this.volatilitiesDataGridView[this.halfLifeColumn.Index, rowIndex];
            VolatilityConfig.SimulationModes mode = (VolatilityConfig.SimulationModes)this.volatilitiesDataGridView[this.simulationModeColumn.Index, rowIndex].Value;
            EnableCell(cell, mode == VolatilityConfig.SimulationModes.MeanReversion && (bool)this.volatilitiesDataGridView[this.generatedColumn.Index, rowIndex].Value);
        }

        private void OnVolatilitiesCheckChanged()
        {
            this.ignoredVariableindexes.Clear();
            for (int i = 0; i < this.volatilitiesDataGridView.Rows.Count; i++)
            {
                bool selected = (bool)this.volatilitiesDataGridView[0, i].Value;
                VolatilityConfig.SimulationModes mode = (VolatilityConfig.SimulationModes)this.volatilitiesDataGridView[this.simulationModeColumn.Index, i].Value;

                if (!selected) this.ignoredVariableindexes.Add(i);
                EnableCell(this.volatilitiesDataGridView[1, i], selected);
                EnableCell(this.volatilitiesDataGridView[2, i], selected);
                EnableCell(this.volatilitiesDataGridView[this.halfLifeColumn.Index, i], selected && mode == VolatilityConfig.SimulationModes.MeanReversion);
            }

            for (int i = 0; i < this.variables.Length - 1; i++)
            {
                for (int j = i + 1; j < this.variables.Length; j++)
                {
                    EnableCell(this.correlationsDataGridView[j, i], true);
                }
            }

            foreach (int index in this.ignoredVariableindexes)
            {
                for (int i = 0; i < this.variables.Length; i++)
                {
                    EnableCell(this.correlationsDataGridView[index, i], false);
                    EnableCell(this.correlationsDataGridView[i, index], false);
                }
            }
        }

        private void MultiScenarioGeneratorLoad(object sender, EventArgs e)
        {
            if (this.DesignMode || Tools.InDesignMode) return;

            if (this.loaded) return;
            this.loaded = true;
            this.CreateVolatilityColumns();
            this.scenariosComboBox.DisplayMember = "Name";
            this.scenariosComboBox.ValueMember = "Self";
            this.scenariosComboBox.SelectedIndexChanged += this.ScenariosComboBoxSelectedIndexChanged;
            Reload();
        }

        public void Reload()
        {

            List<Scenario> scenarios = new List<Scenario>(Scenario.FindNonGenerated(this.Session));

            this.scenariosComboBox.DataSource = scenarios;

            this.Save();

            this.ComputeGrids();

            this.CheckPositiveDefinite();




            
        }

        private void CreateVolatilityColumns()
        {
            this.volatilitiesDataGridView.AutoGenerateColumns = false;

            this.generatedColumn = new DataGridViewCheckBoxColumn();
            this.volatitlityFactorColumn = new DataGridViewTextBoxColumn();
            this.meanColumn = new AutoCompleteColumn();
            this.halfLifeColumn = new DataGridViewTextBoxColumn();
            this.variableColumn = new DataGridViewTextBoxColumn();
            this.simulationModeColumn = new DataGridViewComboBoxColumn();
            this.logColumn = new DataGridViewCheckBoxColumn();

            // 
            // generatedColumn
            // 
            this.generatedColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            this.generatedColumn.DataPropertyName = "Selected";
            this.generatedColumn.FillWeight = 70F;
            this.generatedColumn.HeaderText = Labels.MultiScenarioParameters_CreateVolatilityColumns_Generated_within_the_simulation;
            this.generatedColumn.Name = "generatedColumn";
            // 
            // volatitlityFactorColumn
            // 
            this.volatitlityFactorColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            this.volatitlityFactorColumn.DataPropertyName = "Volatility";
            DataGridViewCellStyle dataGridViewCellStyle1 = new DataGridViewCellStyle();
            dataGridViewCellStyle1.Alignment = DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle1.Format = "N3";
            this.volatitlityFactorColumn.DefaultCellStyle = dataGridViewCellStyle1;
            this.volatitlityFactorColumn.FillWeight = 70F;
            this.volatitlityFactorColumn.HeaderText = Labels.MultiScenarioParameters_CreateVolatilityColumns_Volatility_factor;
            this.volatitlityFactorColumn.Name = "volatitlityFactorColumn";
            this.volatitlityFactorColumn.SortMode = DataGridViewColumnSortMode.NotSortable;
            // 
            // meanColumn
            // 
            this.meanColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            this.meanColumn.DataPropertyName = "Mean";
            DataGridViewCellStyle dataGridViewCellStyle2 = new DataGridViewCellStyle();
            dataGridViewCellStyle2.Alignment = DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle2.Format = "N3";
            this.meanColumn.DefaultCellStyle = dataGridViewCellStyle2;
            this.meanColumn.FillWeight = 70F;
            this.meanColumn.HeaderText = Labels.MultiScenarioParameters_CreateVolatilityColumns_Mean_or_Trend;
            this.meanColumn.Name = "meanColumn";
            this.meanColumn.SortMode = DataGridViewColumnSortMode.NotSortable;
            this.meanColumn.ValueList = DomainTools.GetAllVectorNames();
            // 
            // halfLifeColumn
            // 
            this.halfLifeColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            this.halfLifeColumn.DataPropertyName = "HalfLife";
            this.halfLifeColumn.DefaultCellStyle = dataGridViewCellStyle2;
            this.halfLifeColumn.FillWeight = 70F;
            this.halfLifeColumn.HeaderText = Labels.MultiScenarioParameters_CreateVolatilityColumns_Half_life;
            this.halfLifeColumn.Name = "halfLifeColumn";
            this.halfLifeColumn.SortMode = DataGridViewColumnSortMode.NotSortable;
            // 
            // variableColumn
            // 
            this.variableColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            this.variableColumn.DataPropertyName = "VariableName";
            DataGridViewCellStyle dataGridViewCellStyle3 = new DataGridViewCellStyle();
            dataGridViewCellStyle3.BackColor = SystemColors.ControlLight;
            dataGridViewCellStyle3.Font = new Font("Microsoft Sans Serif", 8.25F, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.variableColumn.DefaultCellStyle = dataGridViewCellStyle3;
            this.variableColumn.HeaderText = Labels.MultiScenarioParameters_CreateVolatilityColumns_Variable;
            this.variableColumn.Name = "variableColumn";
            this.variableColumn.ReadOnly = true;
            this.variableColumn.SortMode = DataGridViewColumnSortMode.NotSortable;
            // 
            // simulationModeColumn
            // 
            this.simulationModeColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            this.simulationModeColumn.DataPropertyName = "SimulationMode";
            this.simulationModeColumn.HeaderText = Labels.MultiScenarioParameters_CreateVolatilityColumns_Mode_de_simulation;
            this.simulationModeColumn.Name = "simulationModeColumn";
            this.simulationModeColumn.DisplayMember = "Value";
            this.simulationModeColumn.ValueMember = "Key";
            this.simulationModeColumn.DataSource = EnumHelper.ToList(typeof(VolatilityConfig.SimulationModes));
            this.simulationModeColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.simulationModeColumn.Width = 65;
            this.simulationModeColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;
            //
            // logColumn
            //
            this.logColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            this.logColumn.DataPropertyName = "IsLog";
            this.logColumn.FillWeight = 70F;
            this.logColumn.HeaderText = Labels.MultiScenarioParameters_CreateVolatilityColumns_Log;
            this.logColumn.Name = "logColumn";
            this.logColumn.DefaultCellStyle.NullValue = false;

            this.volatilitiesDataGridView.Columns.AddRange(new DataGridViewColumn[] {
            this.generatedColumn,
            this.variableColumn,
            this.volatitlityFactorColumn,
            this.meanColumn,
            this.halfLifeColumn,
            this.simulationModeColumn,
            this.logColumn});
        }

        private void ScenariosComboBoxSelectedIndexChanged(object sender, EventArgs e)
        {
            this.Save();
            this.ComputeGrids();
        }

        private void ComputeGrids()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.volatilitiesDataGridView.Rows.Clear();
                this.correlationsDataGridView.Rows.Clear();

                this.selectedScenario = this.scenariosComboBox.SelectedItem as Scenario;
                if (this.selectedScenario == null) return;


                this.selectedScenario = Scenario.Find(this.Session, this.selectedScenario.Id);
                this.evaluator = new ScenarioVariablesEvaluator(this.selectedScenario, this.Session, Util.GetSimulationParams(this.Session));
                this.evaluator.Run();

                if (this.evaluator.ValidationErrors.Count > 0)
                {
                    MessageBox.Show(Labels.MultiScenarioParameters_ComputeGrids_There_are_errors_in_the_variable_formulas);
                    return;
                }

                IDictionary<Variable, double[]> variableDictionary = this.evaluator.Values;

                this.variables = new Variable[variableDictionary.Count];
                variableDictionary.Keys.CopyTo(this.variables, 0);

                this.variableValues = new double[variableDictionary.Count, Param.ResultMonths(this.Session)];
                for (int i = 0; i < variableDictionary.Count; i++)
                {
                    double[] values = variableDictionary[this.variables[i]];
                    for (int j = 0; j < values.Length; j++)
                    {
                        this.variableValues[i, j] = values[j];
                    }
                }

                this.FillVolatilitiesGrid();
                this.FillCorrelationGrid();
                this.OnVolatilitiesCheckChanged();
                this.CheckPositiveDefinite();

                this.Cursor = Cursors.Default;
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void FillVolatilitiesGrid()
        {
            Stopwatch stopwatch = Stopwatch.StartNew();
            this.volatilities = new BindingList<VolatilityConfig>();
            VolatilityConfig[] configs = VolatilityConfig.FindAllByProperty(this.Session, "Scenario", this.selectedScenario).ToArray();
            VariableValue[] scenarioVariableValues = VariableValue.FindByScenario(this.Session, this.selectedScenario);
            for (int i = 0; i < this.variables.Length; i++)
            {
                double[] v = MatrixHelper.GetRow(this.variableValues, i);
                double volatility = Statistics.StandardDeviation(v);
                if (double.IsNaN(volatility)) volatility = 0;
                VolatilityConfig config = configs.Where(vc => vc.VariableName == this.variables[i].Name).FirstOrDefault();

                if (config == null)
                {
                    VariableValue variableValue = scenarioVariableValues.FirstOrDefault(vv => vv.Variable == this.variables[i]);
                    string mean = variableValue == null || variableValue.ValueIsArray() ? "0" : variableValue.Value;
                    config = new VolatilityConfig(false, volatility,
                                                  mean,
                                                  this.variables[i], this.selectedScenario);
                    config.Save(this.Session);
                }
                this.volatilities.Add(config);
            }

            this.volatilitiesDataGridView.DataSource = this.volatilities;
            for (int i = 0; i < this.variables.Length; i++) this.CheckHalfLife(i);

            // Dialogs.Info("", "FillVolatilitiesGrid " + stopwatch.ElapsedMilliseconds);
        }

        private void FillCorrelationGrid()
        {
            this.correlationsDataGridView.Columns.Clear();
            this.correlationsDataGridView.ColumnHeadersDefaultCellStyle.Font =
                new Font(this.correlationsDataGridView.Font, FontStyle.Bold);

            foreach (Variable variable in this.variables)
            {
                DataGridViewTextBoxColumn column = new DataGridViewTextBoxColumn();
                column.HeaderText = variable.Name;
                column.DefaultCellStyle.Format = "N3";
                column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                column.Width = 60;
                column.SortMode = DataGridViewColumnSortMode.NotSortable;
                this.correlationsDataGridView.Columns.Add(column);
            }

            this.correlationMatrix = MatrixHelper.CorrelationMatrix(this.variableValues);

            for (int i = 0; i < this.correlationMatrix.GetLength(0); i++)
            {
                this.correlationMatrix[i, i] = 1;
            }

            this.CorrelationMatrixToCorrelationsDataGridView();
            this.SetVariableCorrelationToGrid();
        }

        private void CorrelationMatrixToCorrelationsDataGridView()
        {
            this.fillingGrid = true;
            try
            {
                this.correlationsDataGridView.Rows.Clear();
                int length = this.correlationMatrix.GetLength(0);
                if (length <= 0) return;

                this.correlationsDataGridView.Rows.Add(length);
                for (int i = 0; i < length; i++)
                {
                    for (int j = 0; j < this.correlationMatrix.GetLength(1); j++)
                    {
                        this.correlationsDataGridView[i, j].Value = this.correlationMatrix[i, j];

                        if (i <= j)
                        {
                            this.correlationsDataGridView[i, j].ReadOnly = true;
                            this.correlationsDataGridView[i, j].Style.BackColor = SystemColors.ControlLight;
                        }
                    }
                }
            }
            finally
            {
                this.fillingGrid = false;
            }
        }

        private void SetVariableCorrelationToGrid()
        {
            MainForm.ModifiedValuesSuspended = true;
            VariableCorrelation[] correlations = VariableCorrelation.FindAllByProperty(this.Session, "Scenario", this.selectedScenario).ToArray();

            for (int i = 0; i < this.correlationsDataGridView.Columns.Count; i++)
            {
                for (int j = i; j < this.correlationsDataGridView.Rows.Count; j++)
                {
                    VariableCorrelation variableCorrelation = this.GetVariableCorrelation(correlations, this.variables[i],
                                                                                     this.variables[j]);

                    if (variableCorrelation != null)
                    {
                        this.correlationsDataGridView[j, i].Value = variableCorrelation.CorrelationValue;
                        this.correlationsDataGridView[i, j].Value = variableCorrelation.CorrelationValue;
                    }
                }
            }
            MainForm.ModifiedValuesSuspended = false;
        }

        private void CorrelationsDataGridViewCellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            this.correlationsDataGridView.CellValueChanged -= this.CorrelationsDataGridViewCellValueChanged;

            if (!this.fillingGrid && e.ColumnIndex != e.RowIndex)
            {
                this.correlationsDataGridView[e.RowIndex, e.ColumnIndex].Value =
                    this.correlationsDataGridView[e.ColumnIndex, e.RowIndex].Value;

                MainForm.ModifiedValues = true;

                this.CheckPositiveDefinite();
            }

            this.correlationsDataGridView.CellValueChanged += this.CorrelationsDataGridViewCellValueChanged;
        }

        private void CheckPositiveDefinite()
        {
            if (this.variables == null) return;

            this.Cursor = Cursors.WaitCursor;

            this.CorrelationsDataGridViewToCorrelationMatrix();

            double[,] correlations = this.GetCorrelationMatrix();

            double[,] cholesky = MatrixHelper.Cholesky(correlations);

            bool containsNaN = MatrixHelper.ContainsNaN(cholesky);
            // bool positiveDefinite = MatrixHelper.IsPositiveDefinite(this.correlationMatrix);

            // Debug.Assert((!containsNaN) == positiveDefinite);

            if (containsNaN)
            {
                this.correlationsLabel.Text = Labels.MultiScenarioParameters_CheckPositiveDefinite_The_current_configuration_cannot_produce_a_valid_simulation;
                this.correlationsLabel.ForeColor = Color.Red;
                this.generateButton.Enabled = false;
            }
            //else if (!positiveDefinite)
            //{
            //    this.correlationsLabel.Text = Labels.MultiScenarioParameters_CheckPositiveDefinite_Invalid_Correlations__not_a_positive_definite_matrix;
            //    this.correlationsLabel.ForeColor = Color.Orange;
            //}
            else
            {
                this.correlationsLabel.Text = "";// Labels.MultiScenarioParameters_CheckPositiveDefinite_Correlations;
                this.correlationsLabel.ForeColor = Color.Black;
                this.generateButton.Enabled = true;
            }

            for (int i = 0; i < this.correlationsDataGridView.Columns.Count; i++)
            {
                for (int j = 0; j < this.correlationsDataGridView.Rows.Count; j++)
                {
                    DataGridViewCell cell = this.correlationsDataGridView[i, j];

                    if (cell.ReadOnly)
                    {
                        cell.Style.BackColor = SystemColors.ControlLight;
                        cell.Style.Font = this.correlationsDataGridView.Font;
                    }
                    else if (Convert.ToString(cell.Value) == "NaN")
                    {
                        cell.Style.BackColor = Color.FromArgb(255, 63, 63);
                        cell.Style.Font = new Font(this.correlationsDataGridView.Font, FontStyle.Bold);
                    }
                    else
                    {
                        cell.Style.BackColor = Color.White;
                        cell.Style.Font = this.correlationsDataGridView.Font;
                    }
                }
            }

            this.Cursor = Cursors.Default;
        }

        private void CorrelationsDataGridViewToCorrelationMatrix()
        {
            for (int i = 0; i < this.correlationsDataGridView.Rows.Count; i++)
            {
                for (int j = 0; j < this.correlationsDataGridView.Columns.Count; j++)
                {
                    this.correlationMatrix[i, j] = Convert.ToDouble(this.correlationsDataGridView[j, i].Value);
                }
            }
        }

        private void GenerateButtonClick(object sender, EventArgs e)
        {
            if (this.variables == null || this.variables.Length == 0) return;
            this.Save();
            this.SaveChangesInSession();

            if (this.DisposeOldScenario != null) this.DisposeOldScenario.Invoke(this, EventArgs.Empty);

            LoadForm.Instance.DoWork(new WorkParameters(this.GenerateScenariosAndRunSimulation,
                                                        this.AfterGenerateScenarios, true), null);
        }

        private void AfterGenerateScenarios(Exception exception)
        {
            if (this.Simulation.IsSuccesfull)
           {
                //this.DialogResult = DialogResult.OK;
                //this.Close();
                //todo : MTU : basculer vers TAB resultats multiscenarios
                //Dialogs.Info("","todo : MTU : basculer vers TAB resultats multiscenarios");
               ((MainForm) Parent.Parent.Parent).RefreshScenarioGenerator(this.Simulation);
            }
            else
            {
                if (this.Simulation.ValidationErrors.Count > 0)
                {
                    MessageBox.Show(this.Simulation.ValidationErrors[0].Message);
                }
                else
                {
                    MessageBox.Show(Labels.MultiScenarioParameters_AfterGenerateScenarios_Some_errors_where_encountered);
                }
            }
        }

        private void GenerateScenariosAndRunSimulation(BackgroundWorker backgroundWorker)
        {
            this.simulation = new Simulation(true);

            Thread.CurrentThread.CurrentCulture = new CultureInfo(Constants.CULTURE_NAME);
            LoadForm.Instance.SetTitle(Labels.MultiScenarioParameters_GenerateScenariosAndRunSimulation_Generating_Scenarios____);

            using (ISession session = SessionManager.OpenSession())
            {
                VariableValue.DeleteGenerated(session);
                Scenario.DeleteGenerated(session);
                session.Flush();

                try
                {
                    double[][] means = this.GetMeans();
                    if (means == null) return;

                    double[] stdevs = this.GetStandardDeviations();
                    VolatilityConfig.SimulationModes[] modes = this.GetModes();
                    bool[] isLog = this.GetLogs();
                    double[] halfLives = this.GetHalfLives();
                    double[,] corrMatrix = this.GetCorrelationMatrix();
                    int scenarioCount = (int)this.scenariosToGenerateNumericUpDown.Value;
                    MultivariateRandomGenerator generator = new MultivariateRandomGenerator();
                    for (int scenarioNo = 0; scenarioNo < scenarioCount; scenarioNo++)
                    {
                        Scenario scenario = new Scenario();
                        scenario.Name = string.Format("G{0}", scenarioNo);
                        scenario.Generated = true;

                        generator.GenerateMultivariateNormals(Param.ResultMonths(session), 1.0 / 12.0, means, stdevs,
                                                              corrMatrix, modes, isLog, halfLives);

                        int j2 = 0;
                        for (int variableNo = 0; variableNo < this.variables.Length; variableNo++)
                        {
                            Variable variable = this.variables[variableNo];

                            VariableValue value = new VariableValue();
                            value.Scenario = scenario;
                            value.Variable = variable;
                            value.Generated = true;

                            if (this.ignoredVariableindexes.Contains(variableNo))
                            {
                                VariableValue baseVariableValue =
                                    this.selectedScenario.VariableValues.Single(vv => vv.Variable.Id == variable.Id);
                                value.Value = baseVariableValue.Value;
                            }
                            else
                            {
                                double[] row = MatrixHelper.GetRow(generator.Result, j2++);
                                value.Value = ArrayToString(row);
                            }

                            scenario.VariableValues.Add(value);
                        }
                        scenario.Save(session);

                        backgroundWorker.ReportProgress(5 * (scenarioNo + 1) / scenarioCount);
                    }
                }
                catch (Exception ex)
                {
                    this.simulation.ValidationErrors.Add(new ValidationMessage(ex.Message));

                    return;
                }

                session.Flush();
            }

            LoadForm.Instance.SetTitle(Labels.MultiScenarioParameters_GenerateScenariosAndRunSimulation_Running_Simulation____);

            try
            {
                this.simulation.Run(backgroundWorker);
            }
            catch (OverflowException)
            {
                MessageBox.Show(
                    Labels.MultiScenarioParameters_GenerateScenariosAndRunSimulation_, Labels.MultiScenarioParameters_GenerateScenariosAndRunSimulation_Error,
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private static string ArrayToString(double[] doubles)
        {
            return VariableValue.ArrayPrefix + doubles.Aggregate("", (a, b) => a + ";" + b);
        }

        private double[][] GetMeans()
        {
            int count = this.variables.Length - this.ignoredVariableindexes.Count;
            double[][] result = new double[count][];
            int j = 0;
            for (int i = 0; i < this.volatilities.Count; i++)
            {
                if (this.ignoredVariableindexes.Contains(i)) continue;
                double[] mean = this.GetMean(this.volatilities[i].Mean);
                if (mean == null)
                {
                    return null;
                }
                result[j++] = mean;
            }
            return result;
        }

        private double[] GetMean(string stringMean)
        {
            double[] result = new double[Param.ResultMonths(this.Session)];
            double mean;
            if (double.TryParse(stringMean, out mean))
            {
                for (int i = 0; i < result.Length; i++)
                {
                    result[i] = mean;
                }
            }
            else
            {
                for (int i = 0; i < result.Length; i++)
                {
                    result[i] = this.evaluator.Evaluate(stringMean, i);
                }
            }

            return result;
        }

        private VolatilityConfig.SimulationModes[] GetModes()
        {
            int count = this.variables.Length - this.ignoredVariableindexes.Count;
            VolatilityConfig.SimulationModes[] result = new VolatilityConfig.SimulationModes[count];
            int j = 0;
            for (int i = 0; i < this.volatilities.Count; i++)
            {
                if (this.ignoredVariableindexes.Contains(i)) continue;
                result[j++] = this.volatilities[i].SimulationMode;
            }
            return result;
        }

        private bool[] GetLogs()
        {
            int count = this.variables.Length - this.ignoredVariableindexes.Count;
            bool[] result = new bool[count];
            int j = 0;
            for (int i = 0; i < this.volatilities.Count; i++)
            {
                if (this.ignoredVariableindexes.Contains(i)) continue;
                result[j++] = this.volatilities[i].IsLog;
            }
            return result;
        }

        private double[] GetHalfLives()
        {
            int count = this.variables.Length - this.ignoredVariableindexes.Count;
            double[] result = new double[count];
            int j = 0;
            for (int i = 0; i < this.volatilities.Count; i++)
            {
                if (this.ignoredVariableindexes.Contains(i)) continue;
                result[j++] = this.volatilities[i].HalfLife;
            }
            return result;
        }

        private double[] GetStandardDeviations()
        {
            int count = this.variables.Length - this.ignoredVariableindexes.Count;
            double[] result = new double[count];
            int j = 0;
            for (int i = 0; i < this.volatilities.Count; i++)
            {
                if (this.ignoredVariableindexes.Contains(i)) continue;
                result[j++] = this.volatilities[i].Volatility;
            }
            return result;
        }

        private double[,] GetCorrelationMatrix()
        {
            int count = this.variables.Length - this.ignoredVariableindexes.Count;
            double[,] result = new double[count, count];

            int i2 = 0;
            for (int i = 0; i < this.correlationMatrix.GetLength(0); i++)
            {
                if (this.ignoredVariableindexes.Contains(i)) continue;
                int j2 = 0;
                for (int j = 0; j < this.correlationMatrix.GetLength(1); j++)
                {
                    if (this.ignoredVariableindexes.Contains(j)) continue;
                    result[i2, j2] = this.correlationMatrix[i, j];
                    j2++;
                }
                i2++;
            }

            return result;
        }

        private void Save()
        {
            this.SaveVolatilityConfig();
            this.SaveVariableCorrelation();
        }

        private void SaveVolatilityConfig()
        {
            if (this.volatilities == null) return;

            foreach (VolatilityConfig volatilityConfig in this.volatilities)
            {
                volatilityConfig.Save(this.Session);
            }
        }

        private void SaveVariableCorrelation()
        {
            VariableCorrelation[] corelations = VariableCorrelation.FindAllByProperty(this.Session, "Scenario", this.selectedScenario).ToArray();

            for (int i = 0; i < this.correlationsDataGridView.Rows.Count; i++)
            {
                for (int j = i; j < this.correlationsDataGridView.Columns.Count; j++)
                {
                    VariableCorrelation variableCorrelation = this.GetVariableCorrelation(corelations, this.variables[i], this.variables[j]);

                    if (variableCorrelation == null)
                    {
                        variableCorrelation = new VariableCorrelation(this.variables[i], this.variables[j],
                                                                      Convert.ToDouble(
                                                                          this.correlationsDataGridView[j, i].Value),
                                                                      this.selectedScenario);
                    }
                    else
                    {
                        variableCorrelation.CorrelationValue = Convert.ToDouble(this.correlationsDataGridView[j, i].Value);
                    }

                    variableCorrelation.Save(this.Session);
                }
            }
        }

        private VariableCorrelation GetVariableCorrelation(IEnumerable<VariableCorrelation> correlations, Variable variable1, Variable variable2)
        {
            return correlations.Where(
                vc =>
                (vc.Variable1 == variable1 && vc.Variable2 == variable2 && vc.Scenario == this.selectedScenario) ||
                (vc.Variable1 == variable2 && vc.Variable2 == variable1 && vc.Scenario == this.selectedScenario)).FirstOrDefault();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            //this.Close();
        }

        private static void EnableCell(DataGridViewCell cell, bool enabled)
        {
            cell.ReadOnly = !enabled;
            cell.Style.BackColor = enabled ? Color.White : SystemColors.ControlLight;
        }

        private void ReloadButtonClick(object sender, EventArgs e)
        {
            foreach (VolatilityConfig volatilityConfig in VolatilityConfig.FindAllByProperty(this.Session, "Scenario", this.selectedScenario))
            {
                volatilityConfig.Delete(this.Session);
            }

            foreach (VariableCorrelation variableCorrelation in VariableCorrelation.FindAllByProperty(this.Session, "Scenario", this.selectedScenario))
            {
                variableCorrelation.Delete(this.Session);
            }

            this.ComputeGrids();
        }

        private void SaveAndCloseButtonClick(object sender, EventArgs e)
        {
            if (this.variables == null || this.variables.Length == 0) return;
            this.Save();
            this.SaveChangesInSession();
        }
    }
}