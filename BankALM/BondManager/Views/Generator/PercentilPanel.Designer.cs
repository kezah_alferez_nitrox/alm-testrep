using System.Windows.Forms;
using ALMSCommon.Controls;
using ALMSCommon.Controls.TreeGridView;
using BondManager.Resources.Language;

namespace BondManager.Views.Generator
{
    partial class PercentilPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.simulationPanel1 = new System.Windows.Forms.Panel();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.zedGraphControl = new ZedGraph.ZedGraphControl();
            this.percentileGrid = new DataGridViewEx();
            this.labelColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.yearColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.year1Column = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.year2Column = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.year3Column = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.year4Column = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.year5Column = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel5 = new System.Windows.Forms.Panel();
            this.bClose = new System.Windows.Forms.Button();
            this.titleLabel = new System.Windows.Forms.Label();
            this.simulationPanel1.SuspendLayout();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.percentileGrid)).BeginInit();
            this.panel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // simulationPanel1
            // 
            this.simulationPanel1.Controls.Add(this.splitContainer2);
            this.simulationPanel1.Controls.Add(this.panel5);
            this.simulationPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.simulationPanel1.Location = new System.Drawing.Point(1, 1);
            this.simulationPanel1.Name = "simulationPanel1";
            this.simulationPanel1.Size = new System.Drawing.Size(883, 299);
            this.simulationPanel1.TabIndex = 1;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 30);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.zedGraphControl);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.percentileGrid);
            this.splitContainer2.Size = new System.Drawing.Size(883, 269);
            this.splitContainer2.SplitterDistance = 646;
            this.splitContainer2.TabIndex = 4;
            // 
            // zedGraphControl
            // 
            this.zedGraphControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.zedGraphControl.Location = new System.Drawing.Point(0, 0);
            this.zedGraphControl.Name = "zedGraphControl";
            this.zedGraphControl.ScrollGrace = 0D;
            this.zedGraphControl.ScrollMaxX = 0D;
            this.zedGraphControl.ScrollMaxY = 0D;
            this.zedGraphControl.ScrollMaxY2 = 0D;
            this.zedGraphControl.ScrollMinX = 0D;
            this.zedGraphControl.ScrollMinY = 0D;
            this.zedGraphControl.ScrollMinY2 = 0D;
            this.zedGraphControl.Size = new System.Drawing.Size(646, 269);
            this.zedGraphControl.TabIndex = 0;
            this.zedGraphControl.ZoomEvent += new ZedGraph.ZedGraphControl.ZoomEventHandler(this.ZedGraphControlZoomEvent);
            this.zedGraphControl.MouseDownEvent += new ZedGraph.ZedGraphControl.ZedMouseEventHandler(this.ZedGraphControlMouseDownEvent);
            // 
            // percentileGrid
            // 
            this.percentileGrid.AllowUserToAddRows = false;
            this.percentileGrid.AllowUserToDeleteRows = false;
            this.percentileGrid.BackgroundColor = ALMSCommon.AlmsColors.Control;
            this.percentileGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.percentileGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.labelColumn,
            this.yearColumn,
            this.year1Column,
            this.year2Column,
            this.year3Column,
            this.year4Column,
            this.year5Column});
            this.percentileGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.percentileGrid.Location = new System.Drawing.Point(0, 0);
            this.percentileGrid.Name = "percentileGrid";
            this.percentileGrid.ReadOnly = true;
            this.percentileGrid.Size = new System.Drawing.Size(233, 269);
            this.percentileGrid.TabIndex = 4;
            // 
            // labelColumn
            // 
            this.labelColumn.Name = "labelColumn";
            this.labelColumn.ReadOnly = true;
            // 
            // yearColumn
            // 
            this.yearColumn.Name = "yearColumn";
            this.yearColumn.ReadOnly = true;
            // 
            // year1Column
            // 
            this.year1Column.Name = "year1Column";
            this.year1Column.ReadOnly = true;
            // 
            // year2Column
            // 
            this.year2Column.Name = "year2Column";
            this.year2Column.ReadOnly = true;
            // 
            // year3Column
            // 
            this.year3Column.Name = "year3Column";
            this.year3Column.ReadOnly = true;
            // 
            // year4Column
            // 
            this.year4Column.Name = "year4Column";
            this.year4Column.ReadOnly = true;
            // 
            // year5Column
            // 
            this.year5Column.Name = "year5Column";
            this.year5Column.ReadOnly = true;
            // 
            // panel5
            // 
            this.panel5.BackColor = ALMSCommon.AlmsColors.ControlDark;
            this.panel5.Controls.Add(this.bClose);
            this.panel5.Controls.Add(this.titleLabel);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(883, 30);
            this.panel5.TabIndex = 3;
            // 
            // bClose
            // 
            this.bClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bClose.BackColor = ALMSCommon.AlmsColors.Control;
            this.bClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bClose.ForeColor = ALMSCommon.AlmsColors.ControlText;
            this.bClose.Location = new System.Drawing.Point(857, 3);
            this.bClose.Name = "bClose";
            this.bClose.Size = new System.Drawing.Size(23, 23);
            this.bClose.TabIndex = 2;
            this.bClose.Text = "X";
            this.bClose.UseVisualStyleBackColor = false;
            this.bClose.Click += new System.EventHandler(this.BCloseClick);
            // 
            // titleLabel
            // 
            this.titleLabel.AutoSize = true;
            this.titleLabel.BackColor = System.Drawing.Color.Transparent;
            this.titleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold);
            this.titleLabel.ForeColor = System.Drawing.Color.White;
            this.titleLabel.Location = new System.Drawing.Point(6, 4);
            this.titleLabel.Name = "titleLabel";
            this.titleLabel.Size = new System.Drawing.Size(55, 24);
            this.titleLabel.TabIndex = 1;
            this.titleLabel.Text = Labels.PercentilPanel_InitializeComponent_EBIT;
            // 
            // PercentilPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.simulationPanel1);
            this.Name = "PercentilPanel";
            this.Padding = new System.Windows.Forms.Padding(1, 1, 1, 5);
            this.Size = new System.Drawing.Size(885, 305);
            this.simulationPanel1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.percentileGrid)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel simulationPanel1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label titleLabel;
        private DataGridViewEx percentileGrid;
        private System.Windows.Forms.Button bClose;
        private ZedGraph.ZedGraphControl zedGraphControl;
        private DataGridViewTextBoxColumn labelColumn;
        private DataGridViewTextBoxColumn yearColumn;
        private DataGridViewTextBoxColumn year1Column;
        private DataGridViewTextBoxColumn year2Column;
        private DataGridViewTextBoxColumn year3Column;
        private DataGridViewTextBoxColumn year4Column;
        private DataGridViewTextBoxColumn year5Column;
    }
}