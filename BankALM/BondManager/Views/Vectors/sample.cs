﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace BondManager.Views.Vectors
{
    public partial class sample : Component
    {
        public sample()
        {
            InitializeComponent();
        }

        public sample(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }
    }
}
