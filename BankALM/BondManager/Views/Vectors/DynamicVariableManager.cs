﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using ALMS.Products;
using ALMSCommon;
using BankALM.Infrastructure.Data;
using BondManager.BusinessLogic;
using BondManager.Calculations;
using BondManager.Core;
using BondManager.Domain;
using BondManager.Resources.Language;
using NHibernate;
using NHibernate.Linq;

namespace BondManager.Views.Vectors
{
    public partial class DynamicVariableManager : SessionForm
    {
        private Font boldFont;

        private readonly List<DynamicVariable> addedVariables = new List<DynamicVariable>();
        private readonly List<DynamicVariable> deletedVariables = new List<DynamicVariable>();
        private DynamicVariable currentVariable;

        private bool hasChanges;
        private BindingList<DynamicVariable> variables;
        private string[] staticVariableNames;
        private Product[] productsWithInvestmentParameter;
        private string[] allVectorNames;

        public DynamicVariableManager()
        {
            this.InitializeComponent();

            boldFont = new Font(this.Font, FontStyle.Bold);

            this.dynamicVarListBox.DisplayMember = "Name";
            this.dynamicVarListBox.ValueMember = "Self";

            this.Load += this.DynamicVectorManagerLoad;
        }

        private void DynamicVectorManagerLoad(object sender, EventArgs e)
        {
            try
            {
                this.productsWithInvestmentParameter = ProductCache.FindHavingInvestmentParameter();

                this.FillDynamicVariablesList();

                this.FillProductsTreeView();

                this.FillResultTreeView();

                this.FillStaticVectorsList();

                this.FillVariableList();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void FillVariableList()
        {
            this.staticVariableNames = this.staticVariableNames ?? DomainTools.GetAllVariableNames(this.Session);

            this.staticVariableList.DataSource = this.staticVariableNames.Concat(this.variables.Select(v => v.Name)).ToArray();
        }

        private void FillDynamicVariablesList()
        {
            this.variables = new BindingList<DynamicVariable>(new List<DynamicVariable>(DynamicVariable.FindAll(this.Session)));

            this.dynamicVarListBox.DataSource = this.variables;

            this.UpdateControls();
        }

        private void FillStaticVectorsList()
        {
            this.allVectorNames = DomainTools.GetAllVectorNames();
            this.staticVectorsListBox.DataSource = this.allVectorNames;
        }

        private void FillProductsTreeView()
        {
            Category root = Category.FindPortfolio(this.Session);
            if (root == null) return;

            this.AddCategoriesToTree(this.productsTreeView.Nodes, root.ChildrenOrdered);
        }

        private void AddCategoriesToTree(TreeNodeCollection nodes, IEnumerable<Category> categories)
        {
            foreach (Category category in categories)
            {
                TreeNode node = new TreeNode(category.Label + "(C" + category.ALMID+")      ");
                node.Tag = category;
                node.NodeFont = boldFont;
                this.AddPropertiesToCategory(node.Nodes);
                this.AddCategoriesToTree(node.Nodes, category.ChildrenOrdered);

                this.AddProductsToNode(node.Nodes, ProductCache.products.FindAll(p => p.Category.ALMID == category.ALMID));//category.Bonds);

                if (node.Nodes.Count > 0)
                {
                    nodes.Add(node);
                    node.Expand();
                }
            }
        }

        private void AddProductsToNode(TreeNodeCollection nodes, IList<Product> products)
        {
            foreach (Product product in products)
            {
                if (product.Exclude) continue;

                TreeNode node = nodes.Add(product.ALMIDDisplayed + " - " + product.Description);
                node.Tag = product.ALMIDDisplayed;
                node.NodeFont = boldFont;
                this.AddPropertiesToProduct(node.Nodes);
            }
        }

        private void AddPropertiesToProduct(TreeNodeCollection nodes)
        {
            foreach (string pp in DynamicVariableEvaluator.ProductDataProperties.Keys)
            {
                nodes.Add(pp);
            }
        }
        private void AddPropertiesToCategory(TreeNodeCollection nodes)
        {
            foreach (string pp in DynamicVariableEvaluator.CategoryDataProperties.Keys)
            {
                nodes.Add(pp);
            }
        }

        private void FillResultTreeView()
        {
            ResultData resultData = new ResultData();
            resultData.Initialize(Category.FindAll(this.Session), OtherItemAssum.FindAll(this.Session), Dividend.FindAll(this.Session));

            resultTreeView.Nodes.Clear();
            FillResultNodes(resultTreeView.Nodes, resultData.Lines);
        }

        private void FillResultNodes(TreeNodeCollection nodes, IList<ResultLine> lines)
        {
            foreach (ResultLine line in lines)
            {
                TreeNode node = nodes.Add(line.Label);
                node.Tag = line;
                this.FillResultNodes(node.Nodes, line.Children);
            }
        }

        private void DeleteDynamicVariable()
        {
            if (this.dynamicVarListBox.SelectedIndices.Count <= 0) return;

            List<DynamicVariable> toRemove = new List<DynamicVariable>();
            foreach (int index in dynamicVarListBox.SelectedIndices)
            {
                DynamicVariable variable = this.dynamicVarListBox.Items[index] as DynamicVariable;
                if (variable == null) continue;

                if (this.addedVariables.Contains(variable))
                {
                    this.addedVariables.Remove(variable);
                }
                else
                {
                    this.deletedVariables.Add(variable);
                }

                toRemove.Add(variable);
            }

            foreach (DynamicVariable variable in toRemove)
            {
                this.variables.Remove(variable);
            }

            this.hasChanges = true;
            this.UpdateControls();
        }

        private void CopyDynamicVariable()
        {
            if (this.currentVariable == null) return;

            DynamicVariable copy = new DynamicVariable
                                       {
                                           Name = this.currentVariable + "Copy",
                                           Formula = this.currentVariable.Formula,
                                           Position = this.variables.Count
                                       };

            this.addedVariables.Add(copy);
            this.variables.Add(copy);

            this.dynamicVarListBox.SelectedItem = null;
            this.dynamicVarListBox.SelectedItem = copy;
            this.hasChanges = true;
            this.UpdateControls();
        }

        private void DynamicVarListBoxSelectedIndexChanged(object sender, EventArgs e)
        {
            this.FillVariableList();

            DynamicVariable variable = this.dynamicVarListBox.SelectedItem as DynamicVariable;
            if (variable == this.currentVariable)
            {
                this.UpdateControls();
                return;
            }

            if (variable != null || this.variables.Count == 0)
            {
                this.currentVariable = variable;
                this.UpdateControls();
            }
        }

        private void StaticVectorsListBoxMouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (this.staticVectorsListBox.SelectedItem == null) return;

            this.InsertTextIntoFormula(this.staticVectorsListBox.SelectedItem + "[t]");
        }

        private void StaticVariableListViewDoubleClick(object sender, EventArgs e)
        {
            if (this.staticVariableList.SelectedItem == null) return;
            this.InsertTextIntoFormula(this.staticVariableList.SelectedItem + "[t]");
        }

        private void ProductsTreeViewDoubleClick(object sender, EventArgs e)
        {
            TreeNode selectedNode = this.productsTreeView.SelectedNode;
            if (selectedNode == null) return;

            if (selectedNode.Parent != null && selectedNode.Parent.Tag is Category)
            {
                this.InsertTextIntoFormula(((Category)selectedNode.Parent.Tag).GetDisplayedALMID() + "." + selectedNode.Text + "[t-1]");
            }
            else if (selectedNode.Parent != null && selectedNode.FirstNode == null)
            {
                string almId = selectedNode.Parent.Tag as string;
                string text = almId + "." + selectedNode.Text + "[t-1]";

                this.InsertTextIntoFormula(text);
            }


        }



        private void ResultTreeViewDoubleClick(object sender, EventArgs e)
        {
            TreeNode selectedNode = this.resultTreeView.SelectedNode;
            if (selectedNode == null) return;

            ResultLine line = selectedNode.Tag as ResultLine;
            if (line == null) return;

            this.InsertTextIntoFormula(string.Format("Result[\"{0}\"][t-1]", line.Id));
        }

        private void InsertTextIntoFormula(string text)
        {
            int position = this.formulaTextBox.SelectionStart != 0
                               ? this.formulaTextBox.SelectionStart
                               : this.formulaTextBox.Text.Length;

            this.formulaTextBox.Text = this.formulaTextBox.Text.Insert(position, text);

            this.hasChanges = true;
            this.UpdateControls();

            formulaTextBox.Focus();
            formulaTextBox.Select(position + text.Length, 0);
        }

        private void OkButtonClick(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            this.SaveChanges();
            Util.UpdateBookValueForDynamicVariables(variables,this.Session);
            MainForm.ModifiedValues = true;
            this.Cursor = Cursors.Arrow;
            this.Close();
        }

        private void SaveChanges()
        {
            this.Cursor = Cursors.WaitCursor;

            try
            {
                foreach (DynamicVariable variable in this.deletedVariables)
                {
                    variable.Delete(this.Session);
                }

                foreach (DynamicVariable variable in this.addedVariables)
                {
                    variable.Create(this.Session);
                }

                //PersistenceSession.Flush();
                this.SaveChangesInSession();

                MainForm.ModifiedScenarioTemplate = true;
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void CancelButtonClick(object sender, EventArgs e)
        {
            this.Close();
        }

        private void NewButtonClick(object sender, EventArgs e)
        {
            this.CreateVariable();
        }

        private void CreateVariable()
        {
            DynamicVariable variable = new DynamicVariable
                                           {
                                               Name = this.GetUniqueName()
                                           };

            this.addedVariables.Add(variable);
            this.variables.Add(variable);

            this.dynamicVarListBox.SelectedItem = null;
            this.dynamicVarListBox.SelectedItem = variable;
            this.hasChanges = true;
            this.UpdateControls();
        }

        private string GetUniqueName()
        {
            int i = 0;
            string name = "DynamicVariable" + i;
            while (this.variables.Any(v => v.Name == name))
            {
                i++;
                name = "DynamicVariable" + i;
            }
            return name;
        }

        private void UpdateControls()
        {
            this.okButton.Enabled = this.hasChanges;

            if (this.currentVariable == null)
            {
                this.detailsGroupBox.Visible = false;
                this.varNameTextBox.Text = "";
                this.formulaTextBox.Text = "";
                this.copyButton.Enabled = false;
                this.deleteButton.Enabled = false;
                this.upButton.Enabled = false;
                this.downButton.Enabled = false;
            }
            else
            {
                this.detailsGroupBox.Visible = true;
                this.varNameTextBox.Text = this.currentVariable.Name;
                this.formulaTextBox.Text = this.currentVariable.Formula;
                this.deleteButton.Enabled = true;

                bool onlyOneVariableSelected = this.dynamicVarListBox.SelectedIndices.Count == 1;

                this.copyButton.Enabled = onlyOneVariableSelected;

                this.upButton.Enabled = this.dynamicVarListBox.SelectedIndex > 0 && onlyOneVariableSelected;
                this.downButton.Enabled = this.dynamicVarListBox.SelectedIndex < this.dynamicVarListBox.Items.Count - 1 && onlyOneVariableSelected;
            }
        }

        private void DeleteButtonClick(object sender, EventArgs e)
        {
            this.DeleteDynamicVariable();
        }

        private void CopyButtonClick(object sender, EventArgs e)
        {
            this.CopyDynamicVariable();
        }

        private void FormulaTextBoxTextChanged(object sender, EventArgs e)
        {
            if (this.currentVariable == null ||
                this.currentVariable.Formula == this.formulaTextBox.Text) return;

            this.currentVariable.Formula = this.formulaTextBox.Text;

            this.hasChanges = true;
            this.UpdateControls();
        }

        private void VarNameTextBoxValidated(object sender, EventArgs e)
        {
            if (this.currentVariable == null ||
                this.currentVariable.Name == this.varNameTextBox.Text) return;

            RenameVariableInFormulas(this.currentVariable.Name, this.varNameTextBox.Text);

            this.currentVariable.Name = this.varNameTextBox.Text;

            this.hasChanges = true;
            this.UpdateControls();
        }

        private void RenameVariableInFormulas(string oldName, string newName)
        {
            foreach (Product product in productsWithInvestmentParameter)
            {
                string newValue = RenameVariableInFormula(product.InvestmentParameter, oldName, newName);
                if (newValue != product.InvestmentParameter)
                {
                    product.InvestmentParameter = newValue;
                   // product.Save(this.Session);
                    if (ProductCache.products.Any(p => p.ALMIDDisplayed == product.ALMIDDisplayed))
                    {
                        int pos = ProductCache.products.FindIndex(p => p.ALMIDDisplayed == product.ALMIDDisplayed);
                        ProductCache.products[pos] = product;
                    }
                    else
                        ProductCache.products.Add(product);
                }
            }
        }

        private string RenameVariableInFormula(string investmentParameter, string oldName, string newName)
        {
            if (string.IsNullOrEmpty(investmentParameter)) return investmentParameter;

            bool finished;
            int startIndex = 0;
            do
            {
                finished = false;
                int index = investmentParameter.IndexOf(oldName, startIndex);
                if (index >= 0)
                {
                    int endIndex = index + oldName.Length;
                    if (
                        (index == 0 || !IsIdentifierChar(investmentParameter[index - 1]) &&
                        (endIndex == investmentParameter.Length || !IsIdentifierChar(investmentParameter[endIndex])))
                    )
                    {
                        string newValue = investmentParameter.Substring(0, index);
                        newValue += newName;
                        newValue += endIndex == investmentParameter.Length - 1
                                       ? ""
                                       : investmentParameter.Substring(endIndex, investmentParameter.Length - endIndex);
                        investmentParameter = newValue;
                    }
                    startIndex = index + 1;
                }
                else
                {
                    finished = true;
                }
            } while (!finished);

            return investmentParameter;
        }

        private bool IsIdentifierChar(char c)
        {
            return Char.IsLetterOrDigit(c) || c == '_';
        }

        private void VarNameTextBoxValidating(object sender, CancelEventArgs e)
        {
            e.Cancel = !this.IsDynamicVariableNameValid(this.varNameTextBox.Text);
        }

        private bool IsDynamicVariableNameValid(string name)
        {
            if (!this.variables.All(v => v == this.currentVariable || v.Name != name))
            {
                Dialogs.Warning(Constants.APPLICATION_NAME, Labels.DynamicVectorManager_IsDynamicVariableNameValid_The_dynamic_variable_name_is_not_unique);
                return false;
            }

            if (!this.allVectorNames.All(v => v != name))
            {
                Dialogs.Warning(Constants.APPLICATION_NAME, Labels.DynamicVariableManager_IsDynamicVariableNameValid_There_is_already_a_vector_having_this_name);
                return false;
            }

            if (!this.staticVariableNames.All(v => v != name))
            {
                Dialogs.Warning(Constants.APPLICATION_NAME, "There is already a variable having this name");
                return false;
            }

            string message;
            if (!Tools.IsValidIdentifier(name, out message))
            {
                Dialogs.Warning(Constants.APPLICATION_NAME, message);
                return false;
            }

            return true;
        }

        private void UpButtonClick(object sender, EventArgs e)
        {
            MoveSelectedVariabl(-1);
        }

        private void DownButtonClick(object sender, EventArgs e)
        {
            MoveSelectedVariabl(+1);
        }

        private void MoveSelectedVariabl(int delta)
        {
            int startIndex = this.dynamicVarListBox.SelectedIndex;
            int endIndex = startIndex + delta;

            DynamicVariable variable = this.variables[startIndex];
            this.variables.RemoveAt(startIndex);
            this.variables.Insert(endIndex, variable);

            for (int i = 0; i < variables.Count; i++)
            {
                variables[i].Position = i;
            }

            this.dynamicVarListBox.SelectedItem = null;
            this.dynamicVarListBox.SelectedItem = variable;

            hasChanges = true;
            this.UpdateControls();
        }


    }
}