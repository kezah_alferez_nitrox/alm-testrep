﻿using ALMSCommon.Controls;
using ALMSCommon.Controls.TreeGridView;
using BondManager.Resources.Language;

namespace BondManager.Views.Vectors
{
    partial class VectorManager
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VectorManager));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.bottomPanel = new System.Windows.Forms.Panel();
            this.closeButton = new System.Windows.Forms.Button();
            this.groupComboBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.newButton = new System.Windows.Forms.Button();
            this.modifyButton = new System.Windows.Forms.Button();
            this.copyButton = new System.Windows.Forms.Button();
            this.deleteButton = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.vectorFilter = new System.Windows.Forms.TextBox();
            this.vectorGrid = new ALMSCommon.Controls.DataGridViewEx();
            this.columnVectorName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnVectorDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CMS_VectorListBox = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.TMI_MoveToWorkSpace = new System.Windows.Forms.ToolStripMenuItem();
            this.TMI_MarketDataVectors = new System.Windows.Forms.ToolStripMenuItem();
            this.TMI_MarketVolVectors = new System.Windows.Forms.ToolStripMenuItem();
            this.valuesDataGridView = new ALMSCommon.Controls.DataGridViewEx();
            this.periodColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.valueColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bottomPanel.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vectorGrid)).BeginInit();
            this.CMS_VectorListBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.valuesDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // bottomPanel
            // 
            this.bottomPanel.Controls.Add(this.closeButton);
            this.bottomPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bottomPanel.Location = new System.Drawing.Point(0, 526);
            this.bottomPanel.Name = "bottomPanel";
            this.bottomPanel.Size = new System.Drawing.Size(571, 44);
            this.bottomPanel.TabIndex = 0;
            // 
            // closeButton
            // 
            this.closeButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.closeButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.closeButton.Location = new System.Drawing.Point(445, 6);
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size(114, 32);
            this.closeButton.TabIndex = 0;
            this.closeButton.Text = global::BondManager.Resources.Language.Labels.VectorManager_InitializeComponent_Close;
            this.closeButton.UseVisualStyleBackColor = true;
            this.closeButton.Click += new System.EventHandler(this.CloseButtonClick);
            // 
            // groupComboBox
            // 
            this.groupComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.groupComboBox.FormattingEnabled = true;
            this.groupComboBox.Location = new System.Drawing.Point(87, 13);
            this.groupComboBox.Name = "groupComboBox";
            this.groupComboBox.Size = new System.Drawing.Size(236, 21);
            this.groupComboBox.TabIndex = 8;
            this.groupComboBox.SelectedIndexChanged += new System.EventHandler(this.GroupComboBoxSelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Vector groups :";
            // 
            // newButton
            // 
            this.newButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.newButton.Image = global::BondManager.Properties.Resources.NewDocumentHS;
            this.newButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.newButton.Location = new System.Drawing.Point(109, 457);
            this.newButton.Name = "newButton";
            this.newButton.Padding = new System.Windows.Forms.Padding(5);
            this.newButton.Size = new System.Drawing.Size(95, 32);
            this.newButton.TabIndex = 11;
            this.newButton.Text = global::BondManager.Resources.Language.Labels.VectorManager_InitializeComponent_New___;
            this.newButton.UseVisualStyleBackColor = true;
            this.newButton.Click += new System.EventHandler(this.NewButtonClick);
            // 
            // modifyButton
            // 
            this.modifyButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.modifyButton.Image = ((System.Drawing.Image)(resources.GetObject("modifyButton.Image")));
            this.modifyButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.modifyButton.Location = new System.Drawing.Point(12, 457);
            this.modifyButton.Name = "modifyButton";
            this.modifyButton.Padding = new System.Windows.Forms.Padding(5);
            this.modifyButton.Size = new System.Drawing.Size(95, 32);
            this.modifyButton.TabIndex = 10;
            this.modifyButton.Text = global::BondManager.Resources.Language.Labels.VectorManager_InitializeComponent_Modify___;
            this.modifyButton.UseVisualStyleBackColor = true;
            this.modifyButton.Click += new System.EventHandler(this.ModifyButtonClick);
            // 
            // copyButton
            // 
            this.copyButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.copyButton.Image = global::BondManager.Properties.Resources.CopyHS;
            this.copyButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.copyButton.Location = new System.Drawing.Point(109, 495);
            this.copyButton.Name = "copyButton";
            this.copyButton.Padding = new System.Windows.Forms.Padding(5);
            this.copyButton.Size = new System.Drawing.Size(95, 32);
            this.copyButton.TabIndex = 7;
            this.copyButton.Text = global::BondManager.Resources.Language.Labels.VectorManager_InitializeComponent_Copy___;
            this.copyButton.UseVisualStyleBackColor = true;
            this.copyButton.Click += new System.EventHandler(this.CopyButtonClick);
            // 
            // deleteButton
            // 
            this.deleteButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.deleteButton.Image = global::BondManager.Properties.Resources.DeleteHS;
            this.deleteButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.deleteButton.Location = new System.Drawing.Point(12, 495);
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Padding = new System.Windows.Forms.Padding(5);
            this.deleteButton.Size = new System.Drawing.Size(95, 32);
            this.deleteButton.TabIndex = 6;
            this.deleteButton.Text = global::BondManager.Resources.Language.Labels.VectorManager_InitializeComponent_Delete;
            this.deleteButton.UseVisualStyleBackColor = true;
            this.deleteButton.Click += new System.EventHandler(this.DeleteButtonClick);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.vectorFilter);
            this.panel1.Controls.Add(this.vectorGrid);
            this.panel1.Controls.Add(this.groupComboBox);
            this.panel1.Controls.Add(this.deleteButton);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.copyButton);
            this.panel1.Controls.Add(this.newButton);
            this.panel1.Controls.Add(this.modifyButton);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.MinimumSize = new System.Drawing.Size(258, 414);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(329, 526);
            this.panel1.TabIndex = 12;
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button2.Image = global::BondManager.Properties.Resources.CopyHS;
            this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button2.Location = new System.Drawing.Point(205, 495);
            this.button2.Name = "button2";
            this.button2.Padding = new System.Windows.Forms.Padding(5);
            this.button2.Size = new System.Drawing.Size(118, 32);
            this.button2.TabIndex = 18;
            this.button2.Text = "Create a serie";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Location = new System.Drawing.Point(300, 47);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(23, 20);
            this.button1.TabIndex = 17;
            this.button1.Text = "X";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 13);
            this.label2.TabIndex = 16;
            this.label2.Text = "Filter vectors :";
            // 
            // vectorFilter
            // 
            this.vectorFilter.Location = new System.Drawing.Point(87, 47);
            this.vectorFilter.Name = "vectorFilter";
            this.vectorFilter.Size = new System.Drawing.Size(216, 20);
            this.vectorFilter.TabIndex = 15;
            this.vectorFilter.KeyUp += new System.Windows.Forms.KeyEventHandler(this.vectorFilter_KeyUp);
            // 
            // vectorGrid
            // 
            this.vectorGrid.AllowUserToAddRows = false;
            this.vectorGrid.AllowUserToDeleteRows = false;
            this.vectorGrid.AllowUserToOrderColumns = true;
            this.vectorGrid.AllowUserToResizeRows = false;
            this.vectorGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.vectorGrid.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.vectorGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.vectorGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.columnVectorName,
            this.columnVectorDate});
            this.vectorGrid.Location = new System.Drawing.Point(12, 73);
            this.vectorGrid.Name = "vectorGrid";
            this.vectorGrid.ReadOnly = true;
            this.vectorGrid.RowHeadersVisible = false;
            this.vectorGrid.RowHeadersWidth = 23;
            this.vectorGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.vectorGrid.Size = new System.Drawing.Size(311, 378);
            this.vectorGrid.TabIndex = 14;
            this.vectorGrid.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.ChangeColumnSort);
            this.vectorGrid.SelectionChanged += new System.EventHandler(this.VectorsListSelectedIndexChanged);
            this.vectorGrid.DoubleClick += new System.EventHandler(this.VectorListBoxDoubleClick);
            // 
            // columnVectorName
            // 
            this.columnVectorName.DataPropertyName = "Name";
            dataGridViewCellStyle1.NullValue = null;
            this.columnVectorName.DefaultCellStyle = dataGridViewCellStyle1;
            this.columnVectorName.HeaderText = "Name";
            this.columnVectorName.Name = "columnVectorName";
            this.columnVectorName.ReadOnly = true;
            this.columnVectorName.Width = 160;
            // 
            // columnVectorDate
            // 
            this.columnVectorDate.DataPropertyName = "CreationDate";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle2.Format = "dd/MM/yyyy";
            this.columnVectorDate.DefaultCellStyle = dataGridViewCellStyle2;
            this.columnVectorDate.HeaderText = "Date";
            this.columnVectorDate.Name = "columnVectorDate";
            this.columnVectorDate.ReadOnly = true;
            // 
            // CMS_VectorListBox
            // 
            this.CMS_VectorListBox.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TMI_MoveToWorkSpace,
            this.TMI_MarketDataVectors,
            this.TMI_MarketVolVectors});
            this.CMS_VectorListBox.Name = "CMS_VectorListBox";
            this.CMS_VectorListBox.Size = new System.Drawing.Size(263, 70);
            // 
            // TMI_MoveToWorkSpace
            // 
            this.TMI_MoveToWorkSpace.Name = "TMI_MoveToWorkSpace";
            this.TMI_MoveToWorkSpace.Size = new System.Drawing.Size(262, 22);
            this.TMI_MoveToWorkSpace.Text = global::BondManager.Resources.Language.Labels.VectorManager_InitializeComponent_Move_Vector_to_Workspace;
            this.TMI_MoveToWorkSpace.Click += new System.EventHandler(this.TmiMoveToWorkSpaceClick);
            // 
            // TMI_MarketDataVectors
            // 
            this.TMI_MarketDataVectors.Name = "TMI_MarketDataVectors";
            this.TMI_MarketDataVectors.Size = new System.Drawing.Size(262, 22);
            this.TMI_MarketDataVectors.Text = global::BondManager.Resources.Language.Labels.VectorManager_InitializeComponent_Move_Vector_to_Market_Data_Vectors;
            this.TMI_MarketDataVectors.Click += new System.EventHandler(this.TmiMarketDataVectorsClick);
            // 
            // TMI_MarketVolVectors
            // 
            this.TMI_MarketVolVectors.Name = "TMI_MarketVolVectors";
            this.TMI_MarketVolVectors.Size = new System.Drawing.Size(262, 22);
            this.TMI_MarketVolVectors.Text = global::BondManager.Resources.Language.Labels.VectorManager_InitializeComponent_Move_Vector_to_Market_Vol_Vectors_;
            this.TMI_MarketVolVectors.Click += new System.EventHandler(this.TmiMarketVolVectorsClick);
            // 
            // valuesDataGridView
            // 
            this.valuesDataGridView.AllowUserToAddRows = false;
            this.valuesDataGridView.AllowUserToDeleteRows = false;
            this.valuesDataGridView.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.valuesDataGridView.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
            this.valuesDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.valuesDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.periodColumn,
            this.valueColumn});
            this.valuesDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.valuesDataGridView.Location = new System.Drawing.Point(329, 0);
            this.valuesDataGridView.Name = "valuesDataGridView";
            this.valuesDataGridView.RowHeadersWidth = 23;
            this.valuesDataGridView.Size = new System.Drawing.Size(242, 526);
            this.valuesDataGridView.TabIndex = 13;
            // 
            // periodColumn
            // 
            this.periodColumn.DataPropertyName = "Date";
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle3.Format = "MM/yyyy";
            dataGridViewCellStyle3.NullValue = null;
            this.periodColumn.DefaultCellStyle = dataGridViewCellStyle3;
            this.periodColumn.HeaderText = global::BondManager.Resources.Language.Labels.VectorManager_InitializeComponent_Period;
            this.periodColumn.Name = "periodColumn";
            this.periodColumn.ReadOnly = true;
            this.periodColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.periodColumn.Width = 80;
            // 
            // valueColumn
            // 
            this.valueColumn.DataPropertyName = "Value";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.Format = "N5";
            this.valueColumn.DefaultCellStyle = dataGridViewCellStyle4;
            this.valueColumn.HeaderText = global::BondManager.Resources.Language.Labels.VectorManager_InitializeComponent_Value;
            this.valueColumn.Name = "valueColumn";
            this.valueColumn.ReadOnly = true;
            // 
            // VectorManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.CancelButton = this.closeButton;
            this.ClientSize = new System.Drawing.Size(571, 570);
            this.ControlBox = false;
            this.Controls.Add(this.valuesDataGridView);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.bottomPanel);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
            this.MinimumSize = new System.Drawing.Size(16, 492);
            this.Name = "VectorManager";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Vector Manager";
            this.Load += new System.EventHandler(this.VectorManagerLoad);
            this.bottomPanel.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vectorGrid)).EndInit();
            this.CMS_VectorListBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.valuesDataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel bottomPanel;
        private System.Windows.Forms.Button closeButton;
        private System.Windows.Forms.ComboBox groupComboBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button newButton;
        private System.Windows.Forms.Button modifyButton;
        private System.Windows.Forms.Button copyButton;
        private System.Windows.Forms.Button deleteButton;
        private System.Windows.Forms.Panel panel1;
        private DataGridViewEx valuesDataGridView;
        private System.Windows.Forms.ContextMenuStrip CMS_VectorListBox;
        private System.Windows.Forms.ToolStripMenuItem TMI_MarketDataVectors;
        private System.Windows.Forms.ToolStripMenuItem TMI_MarketVolVectors;
        private System.Windows.Forms.ToolStripMenuItem TMI_MoveToWorkSpace;
        private System.Windows.Forms.DataGridViewTextBoxColumn periodColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn valueColumn;
        private DataGridViewEx vectorGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnVectorName;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnVectorDate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox vectorFilter;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
    }
}