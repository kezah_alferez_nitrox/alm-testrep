using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using ALMSCommon;
using BondManager.Core;
using BondManager.Domain;
using BondManager.Resources.Language;
using Eu.AlterSystems.ASNetLib.Core;
using ZedGraph;

namespace BondManager.Views.Vectors
{


    public partial class VectorEditor : SessionForm
    {
        private readonly int vectorId;
        private LineItem graphCurve;
        private bool isNewVector;
        private bool valid = true;
        private Vector vector;
        private BindingList<VectorPoint> vectorPointList = new BindingList<VectorPoint>();

        public VectorEditor()
        {
            this.InitializeComponent();

            this.valuesDataGridView.AutoGenerateColumns = false;

            this.zedGraphControl.GraphPane.Title.IsVisible = false;
            this.zedGraphControl.GraphPane.Legend.IsVisible = false;
            this.zedGraphControl.GraphPane.XAxis.Type = AxisType.Date;
            this.zedGraphControl.GraphPane.XAxis.Title.IsVisible = false;
            this.zedGraphControl.GraphPane.YAxis.Title.IsVisible = false;
            this.zedGraphControl.GraphPane.YAxis.MajorGrid.IsVisible = true;

            this.typeComboBox.DisplayMember = "Value";
            this.typeComboBox.ValueMember = "Key";

            this.frequencyComboBox.DisplayMember = "Value";
            this.frequencyComboBox.ValueMember = "Key";

            this.originComboBox.DisplayMember = "Value";
            this.originComboBox.ValueMember = "Key";

        }

        public VectorEditor(int vectorId)
            : this()
        {
            this.vectorId = vectorId;
        }

        public Vector GetVector()
        {
            return this.vector;
        }

        private void VectorEditorLoad(object sender, EventArgs e)
        {
            this.typeComboBox.DataSource = EnumHelper.ToList(typeof(VectorType));

            this.frequencyComboBox.DataSource = EnumHelper.ToList(typeof(VectorFrequency));

            this.originComboBox.DataSource = EnumHelper.ToList(typeof(VectorOriginReference));

            if (this.vectorId == 0)
            {
                this.isNewVector = true;
                this.vector = new Vector();
                this.vector.CreationDate = DateTime.Now;
                this.vector.Frequency = VectorFrequency.Monthly;
                this.vector.Group = VectorGroup.Workspace;
                this.vectorPointList = new BindingList<VectorPoint>();
            }
            else
            {
                this.vector = VectorCache.FindById(this.vectorId);// Vector.Find(this.Session, this.vectorId);

                this.isNewVector = false;
                this.nameTextBox.Text = this.vector.Name;
                this.vectorPointList = this.GetAdjustedPointList();
            }

            this.importantCheckBox.Checked = this.vector.ImportantMarketData;
            this.frequencyComboBox.SelectedValue = this.vector.Frequency;
            this.typeComboBox.SelectedValue = this.vector.VectorType;
            //this.originComboBox.SelectedValue = this.vector.OriginReference;

            RefreshBinding();
        }

        private void RefreshBinding()
        {
            DateTime valuationDate = Param.GetValuationDate(this.Session) ?? new DateTime();

            VectorFrequency vectorFrequency = (VectorFrequency)this.frequencyComboBox.SelectedValue;

            valuesDataGridView.Rows.Clear();

            this.valuesDataGridView.DataSource =
                new BindingList<VectorPointView>(
                    this.vectorPointList.Select(d => new VectorPointView(d, valuationDate, vectorFrequency)).ToList());
        }

        private BindingList<VectorPoint> GetAdjustedPointList()
        {
            List<VectorPoint> pointList = new List<VectorPoint>(this.vector.VectorPoints);
            return new BindingList<VectorPoint>(pointList);
        }

        private void FrequencyComboBoxSelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.vector == null) return;

            VectorFrequency frequency = (VectorFrequency)this.frequencyComboBox.SelectedIndex;

            DateTime valuationDate = Param.GetValuationDate(this.Session) ?? DateTime.Today;

            DateTime startDate = frequency == VectorFrequency.Daily ?
                valuationDate:
                new DateTime(valuationDate.Year, valuationDate.Month, DateTime.DaysInMonth(valuationDate.Year, valuationDate.Month));
            DateTime endDate = startDate.AddYears(Param.GetVectorYears(this.Session));

            this.vectorPointList.Clear();

            BindingList<VectorPoint> originalvectorPointList = null;

            if (this.vector != null)
            {
                originalvectorPointList = new BindingList<VectorPoint>(new List<VectorPoint>(this.vector.VectorPoints));
                if (this.vector.Id != 0 && this.vector.VectorPoints.Count > 0)
                {
                    DateTime lastPointDate = this.vector.VectorPoints[this.vector.VectorPoints.Count - 1].Date;
                    endDate = lastPointDate > endDate
                                  ? lastPointDate
                                  : endDate;
                }
            }

            Func<DateTime, int, DateTime> frequencyFunc = Vector.GetIncrementFunc(frequency);

            DateTime date = startDate.Date;
            int j = 0;
            VectorPoint newPoint;
            if (originalvectorPointList != null && originalvectorPointList.Count != 0)
            {
                VectorPoint vectorPoint = originalvectorPointList[0];

                while (DateIsBefore(date, endDate, frequency))
                //&& vectorPointList.Count < vector.VectorPoints.Count)
                {
                    newPoint = this.vectorPointList.AddNew();
                    newPoint.Date = date;
                    foreach (VectorPoint vp in originalvectorPointList)
                    {
                        if (DatesAreEqual(newPoint.Date, vp.Date, frequency))
                        {
                            newPoint = vp;
                            vectorPoint = vp;
                        }
                        else
                        {
                            if (newPoint.Date > vectorPoint.Date)
                                newPoint.Value = vectorPoint.Value;
                        }
                    }

                    this.vectorPointList[j] = newPoint;
                    j++;
                    date = frequencyFunc(date, 1);
                    if (frequency != VectorFrequency.Daily)
                    {
                        date = new DateTime(date.Year, date.Month, DateTime.DaysInMonth(date.Year, date.Month));   
                    }                    
                }
                //this.vectorPointList.ResetBindings();
                RefreshBinding();
            }
            else
            {
                while (DateIsBefore(date, endDate, frequency) &&
                       this.vectorPointList.Count < this.vector.VectorPoints.Count)
                {
                    newPoint = this.vectorPointList.AddNew();
                    newPoint.Date = date;
                    this.vectorPointList[j] = newPoint;
                    j++;
                    date = frequencyFunc(date, 1);
                    if (frequency != VectorFrequency.Daily)
                    {
                        date = new DateTime(date.Year, date.Month, DateTime.DaysInMonth(date.Year, date.Month));
                    }
                }
                if (this.vector.VectorPoints.Count == 0)
                {
                    while (DateIsBefore(date, endDate, frequency))
                    {
                        newPoint = this.vectorPointList.AddNew();
                        newPoint.Date = date;
                        this.vectorPointList[j] = newPoint;
                        j++;
                        date = frequencyFunc(date, 1);
                        if (frequency != VectorFrequency.Daily)
                        {
                            date = new DateTime(date.Year, date.Month, DateTime.DaysInMonth(date.Year, date.Month));
                        }
                    }
                }
                //this.vectorPointList.ResetBindings();
                RefreshBinding();
            }

            this.InitializeGraph();
        }

        private static bool DatesAreEqual(DateTime date1, DateTime date2, VectorFrequency frequency)
        {
            return frequency == VectorFrequency.Daily ?
                date1.Date == date2.Date :
                date1.Year  == date2.Year && date1.Month == date2.Month;
        }

        private static bool DateIsBefore(DateTime date1, DateTime date2, VectorFrequency frequency)
        {
            return frequency == VectorFrequency.Daily ?
                date1.Date <= date2.Date :
                (date1.Year * 12 + date1.Month) <= (date2.Year * 12 + date2.Month);
        }

        private void InitializeGraph()
        {
            GraphPane zedPane = this.zedGraphControl.GraphPane;

            zedPane.CurveList.Clear();

            PointPairList list = new PointPairList();
            foreach (VectorPoint vectorPoint in this.vectorPointList)
            {
                double x = new XDate(vectorPoint.Date);
                double y = vectorPoint.Value;
                list.Add(x, y);
            }

            this.graphCurve = zedPane.AddCurve(Labels.VectorEditor_InitializeGraph_Values, list, Color.DarkGreen);
            this.graphCurve.Symbol.Type = SymbolType.Circle;
            this.graphCurve.Symbol.Fill = new Fill(Color.White);
            this.graphCurve.Symbol.Size = 8;

            this.zedGraphControl.AxisChange();
            this.zedGraphControl.Invalidate();
        }

        private string ZedGraphControlPointEditEvent(ZedGraphControl sender, GraphPane pane, CurveItem curve, int iPt)
        {
            PointPair pt = curve[iPt];
            foreach (VectorPoint point in this.vectorPointList)
            {
                if (XDate.XLDateToDateTime(pt.X) == point.Date)
                {
                    point.Value = Math.Round(pt.Y, 5);
                    break;
                }
            }
            //this.vectorPointList.ResetBindings();
            RefreshBinding();
            return null;
        }

        private void ValuesDataGridViewCellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0) return;

            double value = (double)this.valuesDataGridView[e.ColumnIndex, e.RowIndex].Value;
            this.vectorPointList[e.RowIndex].Value = value;

            this.graphCurve.Points[e.RowIndex].Y = value;

            this.zedGraphControl.AxisChange();
            this.zedGraphControl.Invalidate();
        }

        private void NameTextBoxTextChanged(object sender, EventArgs e)
        {
            this.okButton.Enabled = !string.IsNullOrEmpty(this.nameTextBox.Text);
        }

        private void OkButtonClick(object sender, EventArgs e)
        {
            this.valid = this.IsValidVectorName();
            if (!this.valid) return;

            this.vector.Name = this.nameTextBox.Text;
            this.vector.Frequency = (VectorFrequency)this.frequencyComboBox.SelectedIndex;
            this.vector.ImportantMarketData = this.importantCheckBox.Checked;
            this.vector.VectorType = (VectorType)this.typeComboBox.SelectedValue;
            //this.vector.OriginReference = (VectorOriginReference)this.originComboBox.SelectedValue;

            if (!this.isNewVector)
            {
                this.vector.VectorPoints.Clear();
            }

            foreach (VectorPoint point in this.vectorPointList)
            {
                point.Vector = this.vector;
                this.vector.VectorPoints.Add(point);
            }

            if (this.isNewVector)
            {
              //  this.vector.Create(this.Session);
                VectorCache.vectors.Add(vector);
            }

            this.SaveChangesInSession();

            MainForm.ModifiedScenarioTemplate = true;
            MainForm.ModifiedValues = true;
        }

        private void VectorEditorActivated(object sender, EventArgs e)
        {
            this.nameTextBox.Focus();
        }

        private bool IsValidVectorName()
        {
            string name = this.nameTextBox.Text;
            string message;
            if (!Tools.IsValidIdentifier(name, out message))
            {
                MessageBox.Show(message, Labels.VectorEditor_IsValidVectorName_Error, MessageBoxButtons.OK,
                                MessageBoxIcon.Exclamation);
                return false;
            }

            Vector existingVector = VectorCache.FindByName( name);
            if (existingVector != null && existingVector != this.vector)
            {
                MessageBox.Show(
                    Labels.
                        VectorEditor_IsValidVectorName_Can_not_create_or_modify_the_vector_because_another_vector_with_the_same_name_exists_,
                    Labels.VectorEditor_IsValidVectorName_Error, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            return true;
        }

        private void VectorEditorFormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = !this.valid;
        }

        private void CancelButtonClick(object sender, EventArgs e)
        {
            this.valid = true;
        }

        private void ValuesDataGridViewDataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            e.Cancel = false;
            e.ThrowException = false;
        }

        private void CopyValuetoAllCellsBelowtoolStripMenuItemClick(object sender, EventArgs e)
        {
            int _sel = this.valuesDataGridView.SelectedCells[0].RowIndex;
            string _val = this.valuesDataGridView.SelectedCells[0].Value.ToString();
            for (int i = _sel; i < this.valuesDataGridView.Rows.Count; i++)
            {
                this.valuesDataGridView.Rows[i].Cells[1].Value = _val;
            }
        }

        private void CopyValuetoAllCellsBelowwithIncrementtoolStripMenuItemClick(object sender, EventArgs e)
        {
            IncrementValuePrompt _incv = new IncrementValuePrompt();
            DialogResult dialogResult = _incv.ShowDialog();
            if (dialogResult == DialogResult.Cancel) return;

            if (dialogResult == DialogResult.OK)
            {
                double _incremetValue = double.Parse(_incv.IncrementValue); //TODO: if the entered values is not double
                int _sel = this.valuesDataGridView.SelectedCells[0].RowIndex;
                string _val = this.valuesDataGridView[this.valueColumn.Index, _sel].Value.ToString();
                //.SelectedCells[0].Value.ToString();
                for (int i = _sel + 1; i < this.valuesDataGridView.Rows.Count; i++)
                {
                    this.valuesDataGridView.Rows[i].Cells[1].Value = double.Parse(_val) + _incremetValue;
                    _val = this.valuesDataGridView.Rows[i].Cells[1].Value.ToString();
                }
            }
        }

        private void PasteToolStripMenuItemClick(object sender, EventArgs e)
        {
            string clipboardtext = Clipboard.GetText();
            if (string.IsNullOrEmpty(clipboardtext)) return;
            this.valuesDataGridView.EndEdit();
            if (this.valuesDataGridView.CurrentCell == null || this.valuesDataGridView.CurrentCell.RowIndex < 0) return;
            string[] clipboardrows = clipboardtext.Split(new[] { Environment.NewLine, "\t" }, StringSplitOptions.None);

            if (clipboardrows.Length == 0) return;

            int startIndex = this.valuesDataGridView.CurrentCell.RowIndex;

            for (int i = 0; i < clipboardrows.Length; i++)
            {
                if (startIndex >= this.valuesDataGridView.Rows.Count) break;
                double? decimalValue = ParsingTools.StringToDouble(clipboardrows[i]);
                if (decimalValue != null) this.valuesDataGridView.Rows[startIndex].Cells[1].Value = decimalValue.Value;
                startIndex++;
            }
        }

        private void ValuesDataGridViewMouseDown(object sender, MouseEventArgs e)
        {
            DataGridView.HitTestInfo hti = this.valuesDataGridView.HitTest(e.X, e.Y);
            if (hti.Type == DataGridViewHitTestType.Cell)
            {
                this.valuesDataGridView.ClearSelection();

                this.valuesDataGridView.Rows[hti.RowIndex].Cells[hti.ColumnIndex].Selected =
                    true;
            }
        }

        private void NameTextBoxLeave(object sender, EventArgs e)
        {
            this.nameTextBox.Text = Tools.EscapeIdentifier(this.nameTextBox.Text);
        }
    }

    public class VectorPointView
    {
        private readonly VectorPoint vectorPoint;
        private DateTime valuationDate;
        private readonly VectorFrequency frequency;
        private string format;

        public VectorPointView(VectorPoint vectorPoint, DateTime valuationDate, VectorFrequency frequency)
        {
            this.vectorPoint = vectorPoint;
            this.valuationDate = valuationDate;
            this.frequency = frequency;
            this.format = frequency == VectorFrequency.Daily ? "dd/MM/yyyy" : "MM/yyyy";
        }

        public string Date
        {
            get
            {

                return this.IsOpeningBalance()
                           ? Labels.ResultModel_InitColumns_Opening_Balance
                           : vectorPoint.Date.ToString(this.format);
            }
        }

        private bool IsOpeningBalance()
        {
            return this.frequency == VectorFrequency.Daily
                       ? this.valuationDate.Date == this.vectorPoint.Date.Date
                       : this.valuationDate.Month == this.vectorPoint.Date.Month && this.valuationDate.Year == this.vectorPoint.Date.Year;
        }

        public double Value { get { return vectorPoint.Value; } set { vectorPoint.Value = value; } }
    }

}