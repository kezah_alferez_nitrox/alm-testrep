using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace BondManager.Views.Vectors
{
    public partial class IncrementValuePrompt : Form
    {
        public IncrementValuePrompt()
        {
            InitializeComponent();
        }

        public string IncrementValue
        {
            get
            {
                return incrementTextBox.Text;
            }
        }

        private void IncrementValues_Load(object sender, EventArgs e)
        {
            incrementTextBox.Focus();
            incrementTextBox.Select(0, incrementTextBox.Text.Length);
        }
    }
}