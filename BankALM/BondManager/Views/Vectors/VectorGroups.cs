﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using ALMSCommon;
using BondManager.Calculations;
using BondManager.Core;
using BondManager.Domain;
using BondManager.Properties;
using BondManager.Resources.Language;
using Eu.AlterSystems.ASNetLib.Core;
using NHibernate.Linq;
using System.IO;

namespace BondManager.Views.Vectors
{
    public partial class VectorGroups : SessionForm
    {
        private String[] groupOn = new String[2] {"Column", "Row"};
        public VectorGroups()
        {
            InitializeComponent();
            this.comboBox1.DisplayMember = "Value";
            this.comboBox1.ValueMember = "Key";
            this.comboBox2.DisplayMember = "Value";
            this.comboBox2.ValueMember = "Key";
        }

        private void VectorGroups_Load(object sender, EventArgs e)
        {
            this.comboBox1.DataSource = EnumHelper.ToList(typeof(VectorGroup));
            this.comboBox2.DataSource = groupOn;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Vector[] vectors;
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();

            saveFileDialog1.Filter = "Excel Files (*.xls;*.xlsx;*.xlsm)|*.xls;*.xlsx;*.xlsm|All files (*.*)|*.*";
            saveFileDialog1.FilterIndex = 1;
            saveFileDialog1.RestoreDirectory = true;

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {

                if (this.comboBox1.SelectedIndex == this.comboBox1.FindStringExact("All"))
                {
                    vectors = VectorCache.vectors.ToArray();//this.Session.Query<Vector>().ToArray();
                }
                else
                {
                    vectors = VectorCache.vectors.FindAll(v => v.Group == (VectorGroup)this.comboBox1.SelectedValue).ToArray();// this.Session.Query<Vector>().Where(v => v.Group == (VectorGroup)this.comboBox1.SelectedValue).ToArray();
                }

                if (vectors.Length > 0 && this.comboBox2.SelectedIndex == this.comboBox2.FindStringExact("Column"))
                {
                    exportVectorGroupsToExcelColumn(vectors, saveFileDialog1);
                }
                else if (vectors.Length > 0 && this.comboBox2.SelectedIndex == this.comboBox2.FindStringExact("Row"))
                {
                    exportVectorGroupsToExcelRow(vectors, saveFileDialog1);
                }
                else
                {
                    MessageBox.Show("Empty group.");
                }
            }
        }

        private void exportVectorGroupsToExcelColumn(Vector[] vectors, SaveFileDialog saveFileDialog1)
        {
            FileStream stream = new FileStream(saveFileDialog1.FileName, FileMode.CreateNew);
            ExcelWriter writer = new ExcelWriter(stream);

            writer.BeginWrite();
            writer.WriteCell(0, 0, "Name");
            for (var j = 0; j < vectors[0].VectorPoints.Count; j++)
            {
                DateTime dt = DateTime.ParseExact(vectors[0].VectorPoints[j].Date.ToString(), "d/M/yyyy hh:mm:ss tt", CultureInfo.InvariantCulture);
                string s = dt.ToString("d/M/yyyy");
                writer.WriteCell(0, j + 2, s);
            }
            Console.WriteLine("asdf");
            for (var i = 0; i < vectors.Length; i++)
            {
                writer.WriteCell(i + 1, 0, vectors[i].Name);
                writer.WriteCell(i + 1, 1, vectors[i].CreationDate.ToString());

                for (var k = 0; k < vectors[i].VectorPoints.Count; k++)
                {
                    writer.WriteCell(i + 1, k + 2, vectors[i].VectorPoints[k].Value);
                }
            }
            writer.EndWrite();
            stream.Close();
            MessageBox.Show("File exported.");
        }

        private void exportVectorGroupsToExcelRow(Vector[] vectors, SaveFileDialog saveFileDialog1)
        {
            FileStream stream = new FileStream(saveFileDialog1.FileName, FileMode.CreateNew);
            ExcelWriter writer = new ExcelWriter(stream);

            writer.BeginWrite();
            writer.WriteCell(0, 0, "Name");
            for (var j = 0; j < vectors[0].VectorPoints.Count; j++)
            {
                DateTime dt = DateTime.ParseExact(vectors[0].VectorPoints[j].Date.ToString(), "d/M/yyyy hh:mm:ss tt", CultureInfo.InvariantCulture);
                string s = dt.ToString("dd/M/yyyy");
                writer.WriteCell(j + 2, 0, s);
            }
            for (var i = 0; i < vectors.Length; i++)
            {
                writer.WriteCell(0, i + 1, vectors[i].Name);
                writer.WriteCell(1, i + 1, vectors[i].CreationDate.ToString());

                for (var k = 0; k < vectors[i].VectorPoints.Count; k++)
                {
                    writer.WriteCell(k + 2, i + 1, vectors[i].VectorPoints[k].Value);
                }
            }
            writer.EndWrite();
            stream.Close();
            MessageBox.Show("File exported.");
        }
    }
}
