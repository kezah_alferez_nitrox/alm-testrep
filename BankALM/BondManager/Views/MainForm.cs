using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Text;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using ALMS.Products;
using ALMSCommon;
using ALMSCommon.Forms;
using BankALM.Infrastructure.Data;
using BondManager.BusinessLogic;
using BondManager.Calculations;
using BondManager.Core;
using BondManager.Core.BetaTools;
using BondManager.Core.IO;
using BondManager.Core.UndoRedo;
using BondManager.Domain;
using BondManager.ExternalData;
using BondManager.Properties;
using BondManager.Resources.Language;
using BondManager.Views.BalanceSheet.Result;
using BondManager.Views.Generator;
using BondManager.Views.Options.FxRates;
using BondManager.Views.Options.Parameters;
using BondManager.Views.Variables;
using BondManager.Views.Vectors;
using NHibernate;
using NHibernate.Linq;
using NHibernate.Mapping;
using log4net.Core;


namespace BondManager.Views
{
    public partial class MainForm : Form, IModuleMainForm
    {
        public const string NEW_WORKSPACE = "New ALM Workspace";

        public static DateTime ValuationDate = DateTime.Now;

        public static Dictionary<int, double> OpeningBalance = new Dictionary<int, double>();

        public static bool ModifiedScenarioTemplate;

        private static bool modifiedValuesSuspended;

        private static MainForm instance;

        private readonly IBalanceSerializer balanceSerializer;
        private readonly FileHistory fileHistory = new FileHistory(AlmConfiguration.Module.BankALM);
        private readonly IScenarioSerializer scenarioTemplate;
        private string _TemplateFileName = string.Empty;
        private bool _modifiedValues;
        private string[] args;
        private string currentFilePath;
        private string currentScenarioFilePath;
        private string currentWorkspacePath;
        private GeneratorResultNode[] generatorResultNodes;
        private MultiScenarioResult multiScenarioResult;
        private bool restart;
        private int[] setSetupColumnsOrder;

        private List<System.Windows.Forms.ToolStripMenuItem> externalDataInterfaces;

        public static bool IsBeta { get; set; }

        public MainForm()
        {
            this.Cursor = Cursors.WaitCursor;
            this.SuspendLayout();
            this.Font = Constants.Fonts.MainFont;
            Initialiser.Initialise();





            this.InitializeComponent();
            instance = this;

            this.balanceSerializer = new BalanceXmlSerializer();
            this.scenarioTemplate = new ScenarioXmlSerializer();
            InstalledFontCollection fontCollection = new InstalledFontCollection();
            foreach (FontFamily fontFamily in fontCollection.Families)
            {
                this.fontNameToolStripComboBox.Items.Add(fontFamily.Name);
            }

            this.parameters.ValuationCurrencyChanged += this.balance.Parameters_ValuationCurrencyChanged;
            this.Shown += this.BalanceSheetShown;
            this.balance.WorkspaceTemplateSaved += this.RefreshMenus;
            this.balance.SetModifiedValueHandler(this.SetModified);

            this.Activated += this.FormActivated;
            this.OnModifiedValue += this.ModifiedByUser;

            Constants.BalanceSheetForm = this;

            this.Closed += this.MainFormClosed;

            this.parameters.RefreshParameters();

            //this.mainTabControl.SelectedIndex = 1;
            //this.mainTabControl.SelectedIndex = 0;
            this.recalculateALMIDsToolStripMenuItem.Visible = true;



            if (AlmConfiguration.IsPensionFundModule())
            {
                if (!mainTabControl.TabPages.Contains(multiScenarioParameters))
                    mainTabControl.TabPages.Add(multiScenarioParameters);
                multiScenarioToolStripButton.Visible = false;
            }
            else
            {
                mainTabControl.TabPages.Remove(multiScenarioParameters);
                multiScenarioToolStripButton.Visible = true;
            }
            exitToolStripMenuItem.Text += " " + AlmConfiguration.GetModuleName(AlmConfiguration.CurrentModule);

            this.ResumeLayout(true);
            this.Cursor = Cursors.Arrow;
            using (ISession session=SessionManager.OpenSession())
            {
                ValuationDate = Param.GetValuationDate(session)??DateTime.Now;
            }
        }

        void MainFormClosed(object sender, EventArgs e)
        {
            this.result.ClearData();
        }

        public MainForm(string[] args)
            : this()
        {
            this.args = args;
        }

        public ILicenceInfo LicenceInfo { set; get; }

        public bool Restart
        {
            get { return this.restart; }
        }

        public static bool ModifiedValues
        {
            get
            {
                if (Instance == null) return false;
                return Instance._modifiedValues;
            }
            set
            {
                if (Instance == null || modifiedValuesSuspended) return;
                Instance._modifiedValues = value;
                EventHandler handler = Instance.OnModifiedValue;
                if (handler != null)
                {
                    handler(Instance, EventArgs.Empty);
                }
            }
        }

        public static bool ModifiedValuesSuspended
        {
            get { return modifiedValuesSuspended; }
            set { modifiedValuesSuspended = value; }
        }

        public string CurrentFilePath
        {
            get { return this.currentFilePath; }
            set
            {
                this.currentFilePath = value;
                this.Text = this.currentFilePath;
            }
        }

        public string CurrentWorkspacePath
        {
            get { return this.currentWorkspacePath; }
            set
            {
                this.currentWorkspacePath = value;
                string currentFile = Path.GetFileNameWithoutExtension(value);

                if (string.IsNullOrEmpty(currentFile)) currentFile = "New";

                this.Text = currentFile;
                using (ISession session = SessionManager.OpenSession())
                {
                    Param.SetFileName(session, currentFile);
                    session.Flush();
                }

            }
        }

        public string CurrentScenarioFilePath
        {
            get { return this.currentScenarioFilePath; }
            set
            {
                this.currentScenarioFilePath = value;
            }
        }

        #region IModuleMainForm Members

        public bool IsForcedClose { get; set; }

        public void OpenFile(string fileName)
        {
            if (File.Exists(fileName))
            {
                string extension = Path.GetExtension(fileName);
                if (extension == ".alw" || extension == ".plw")
                {
                    this.OpenWorkspace(fileName);
                }
                else if (extension == ".alm")
                {
                    this.ImportPortfolio(fileName);
                }
                else if (extension == ".sct")
                {
                    this.LoadScenario(fileName);
                }
            }
        }

        public string[] Args
        {
            get { return this.args; }
            set { this.args = value; }
        }

        public static MainForm Instance
        {
            get { return instance; }
        }

        #endregion

        public event EventHandler OnModifiedValue;

        private void AddOpenLast(string fileName)
        {
            this.fileHistory.PutFile(fileName);
            this.RefreshOpenLast();
        }

        private void ModifiedByUser(object sender, EventArgs e)
        {
            this.RefreshIfModified();
        }

        private void FormActivated(object sender, EventArgs e)
        {
            this.RefreshIfModified();
        }

        public void AddFileHistory(string path)
        {
            if (this.fileHistory != null) this.fileHistory.PutFile(path);
        }

        void BalanceSheetShown(object sender, EventArgs e)
        {
            this.OpenPassedWorkSpace();
            this.LoadUserConfiguration();
        }

        public void InitializeOpeningBalance()
        {
            OpeningBalance = new Dictionary<int, double>();
        }

        private void OpenPassedWorkSpace()
        {
            if (this.args != null && this.args.Length > 0)
            {
                foreach (string arg in this.args)
                {
                    if (!arg.StartsWith("/"))
                        this.OpenFile(arg);
                    else
                    {
                        switch (arg)
                        {
                            case "/CALC":
                                this.computeResultsStripButton_Click(this, null);
                                break;
                            default:
                                //MessageBox.Show("Unknown command line argument, ignored : " + arg);
                                break;
                        }
                    }
                }
            }
        }

        public void SetModified(object sender, EventArgs e)
        {
            this.RefreshIfModified();
        }

        public void SetModified(bool modified)
        {
            ModifiedValues = modified;
            this.RefreshIfModified();
        }

        public void RefreshIfModified()
        {
            if (ModifiedValues && !this.Text.EndsWith("*")) this.Text = this.Text + "*";
            else if (!ModifiedValues && this.Text.EndsWith("*")) this.Text = this.Text.TrimEnd('*');

        }

        private void mainTabControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.SaveChangesInSession();

            BalanceBL.AjustBalance();

            int selectedIndex = ((TabControl)sender).SelectedIndex;

            this.Cursor = Cursors.WaitCursor;
            try
            {
                if (selectedIndex == 0)
                {
                    this.balance.RefreshSelectedCategory();
                    this.balance.SetScenarios();
                }
                else if (selectedIndex == 4)//MTU:todo : refaire, avant c'etait 3
                {
                    //this.multiScenarioResult.Reload();
                }
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void New()
        {
            if (ModifiedValues && MessageBox.Show(Properties.Resources.NewFilePrompt, "", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) !=
                DialogResult.OK)
                return;

            this.CurrentFilePath = NEW_WORKSPACE;
            this.SaveChangesInSession();
            if (this._TemplateFileName != string.Empty)
            {
                new CategoryXmlSerializer().Load(this._TemplateFileName);
            }

            BalanceBL.EnsureSpecialCategoriesAndBondsExist();
            SolvencyAdjustment.EnsureExist();

            using (ISession session = SessionManager.OpenSession())
            {
                if (Param.GetValuationDate(session) == null)
                {
                    Param.SetValuationDate(session, DateTime.Today);
                }
                session.Flush();
            }

            this.result.InvalidateResult();
        }

        private void Save()
        {
            if (string.IsNullOrEmpty(this.CurrentFilePath))
            {
                if (this.saveFileDialog.ShowDialog() == DialogResult.OK)
                {
                    this.CurrentFilePath = this.saveFileDialog.FileName;
                }
            }

            this.SaveInternal();
        }

        private void SaveAs()
        {
            if (this.saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                this.CurrentFilePath = this.saveFileDialog.FileName;
                this.SaveInternal();
            }
        }

        protected void SaveInternal()
        {
            if (string.IsNullOrEmpty(this.CurrentFilePath) || this.CurrentFilePath == NEW_WORKSPACE) return;

            SaveChangesInSession();

            this.SaveFile(this.currentFilePath);
            this.SetModified(false);
            ModifiedValuesSuspended = false;
        }

        public void SaveFile(string path)
        {
            try
            {
                this.SaveChangesInSession();
                this.balanceSerializer.Save(path);
            }
            catch (Exception ex)
            {
                Log.Exception("balanceSerializer.Save(CurrentFilePath);", ex);
                MessageBox.Show(Labels.MainForm_SaveInternal_Unable_to_save_file_ + this.CurrentFilePath);
            }
        }

        private void SaveChangesInSession()
        {
            parameters.SaveChangesInSession();
            balance.SaveChangesInSession();
        }

        private void Open()
        {
            if (ModifiedValues && MessageBox.Show(Properties.Resources.NewFilePrompt, "", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) !=
                DialogResult.OK)
                return;

            if (this.openFileDialog.ShowDialog() != DialogResult.OK) return;

            this.ImportPortfolio(this.openFileDialog.FileName);
            MainForm.Instance.InitializeOpeningBalance();
        }

        private void ImportPortfolio(string fileName)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.CurrentFilePath = fileName;

                try
                {
                    this.SaveChangesInSession();
                    this.balanceSerializer.Load(this.CurrentFilePath);
                    BalanceBL.EnsureSpecialCategoriesAndBondsExist();
                    SolvencyAdjustment.EnsureExist();
                }
                catch (Exception ex)
                {
                    Log.Exception("balanceSerializer.Load(CurrentFilePath);", ex);
                    MessageBox.Show(Labels.MainForm_ImportPortfolio_Unable_to_open_file_ + this.CurrentFilePath);
                    return;
                }


                this.balance.Reload();
                this.SetModified(false);
                ModifiedValuesSuspended = false;

                this.result.InvalidateResult();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void ClipboardCopy(bool cut)
        {
            if (this.mainTabControl.SelectedTab == this.balanceSheetTabPage)
            {
                this.balance.ClipboardCopy(cut);
            }
            else if (this.mainTabControl.SelectedTab == this.resultTabPage)
            {
                this.result.ClipboardCopy();
            }
        }

        private void ClipboardPaste()
        {
            if (this.mainTabControl.SelectedTab == this.balanceSheetTabPage)
            {
                this.balance.ClipboardPaste();
            }
        }

        private void vectorManagerToolStripButton_Click(object sender, EventArgs e)
        {
            this.ShowVectorManager();
        }

        private void ShowVectorManager()
        {
            Tools.ShowDialog(new VectorManager());

            BalanceBL.AjustBalance();

            this.balance.RefreshSelectedCategory();

            this.RefreshParameters();
        }

        private void copyToolStripButton_Click(object sender, EventArgs e)
        {
            this.ClipboardCopy(false);
        }

        private void pasteToolStripButton_Click(object sender, EventArgs e)
        {
            this.ClipboardPaste();
        }

        private void openToolStripButton_Click(object sender, EventArgs e)
        {
            //Open();
            this.openWorkspacetoolStripMenuItem_Click(sender, e);
        }

        private void saveToolStripButton_Click(object sender, EventArgs e)
        {
            //Save();
            this.saveWorkspaceToolStripMenuItem_Click(sender, e);
        }

        public void RefreshResult()
        {
            this.result.RefreshResultGrid();
        }

        public void RefreshParameters()
        {
            this.parameters.RefreshParameters();
        }

        private void fontNameToolStripComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.UpdateFormFont();
        }

        private void fontSizeToolStripComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.UpdateFormFont();
        }

        private void UpdateFormFont()
        {
            string fontName = this.fontNameToolStripComboBox.SelectedItem as string;
            string fontSizeString = this.fontSizeToolStripComboBox.SelectedItem as string ?? "";
            float fontSize;
            if (!string.IsNullOrEmpty(fontName) &&
                (float.TryParse(fontSizeString, out fontSize) ||
                 float.TryParse(fontSizeString.Replace('.', ','), out fontSize)))
            {
                try
                {
                    Font f = new Font(fontName, fontSize);
                    this.Font = f;
                    this.Refresh();
                }
                catch (Exception ex)
                {
                    Log.Warn(ex);
                }
            }
        }

        public void OnValuationCurrencyChange()
        {
            CurrencyBL.ResetValuationDateFxRates();
            this.balance.OnFxRatesChange();
            this.parameters.InitFxRates();
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Open();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Save();
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.SaveAs();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.restart = false;
            AlmConfiguration.CurrentModule = 0;

            this.Close();
        }

        private void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.ClipboardCopy(false);
        }

        private void pasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.ClipboardPaste();
        }

        private void vectorManagerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.ShowVectorManager();
        }

        private void variableDefinitionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Tools.ShowDialog(new VariableDefinition()) == DialogResult.OK)
            {
                this.RefreshParameters();
                this.balance.RefreshSelectedCategory();
            }
        }

        private void otherItemsAssumptionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Tools.ShowDialog(new OtherItemsAssumptionsManager());
        }

        private void taxAssumptionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Tools.ShowDialog(new TaxAssumptions());
        }

        private void dividendToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Tools.ShowDialog(new DividendForm());
        }

        private void currenciesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Tools.ShowDialog(new FxRatesEditor());

            this.OnValuationCurrencyChange();
        }

        private void parametersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ParametersEditor form = new ParametersEditor();
            form.ShowDialog();
            Cursor = Cursors.WaitCursor;
            using (ISession session = SessionManager.OpenSession())
            {
                this.picLogo.Image = Param.GetLogo(session);
            }
            balance.RefreshTreeView();
            balance.RefreshSelectedCategory();
            Cursor = Cursors.Arrow;
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ((new About())).ShowDialog();
        }

        private void saveScenarioTemplateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.saveScenarioFileDialog.ShowDialog() == DialogResult.OK)
            {
                this.currentScenarioFilePath = this.saveScenarioFileDialog.FileName;
                this.SaveChangesInSession();

                this.Cursor = Cursors.WaitCursor;
                try
                {
                    this.scenarioTemplate.Save(this.currentScenarioFilePath);
                    ModifiedScenarioTemplate = false;
                }
                catch (Exception ex)
                {
                    Log.Exception("scenarioTemplate.Save(currentScenarioFilePath);", ex);
                    MessageBox.Show(Labels.MainForm_saveScenarioTemplateToolStripMenuItem_Click_Unable_to_save_scenario_ + this.currentScenarioFilePath);
                }
                finally
                {
                    this.Cursor = Cursors.Default;
                }
            }
        }

        private void applyScenarioTemplate_Click(object sender, EventArgs e)
        {
            if (ModifiedScenarioTemplate)
            {
                if (MessageBox.Show(Properties.Resources.NewFilePrompt, "", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) !=
                    DialogResult.OK)
                    return;
            }

            if (this.openScenarioFileDialog.ShowDialog() != DialogResult.OK) return;

            this.LoadScenario(this.openScenarioFileDialog.FileName);
        }

        private void LoadScenario(string fileName)
        {
            this.Cursor = Cursors.WaitCursor;

            try
            {
                this.SaveChangesInSession();
                this.currentScenarioFilePath = fileName;
                this.scenarioTemplate.Load(this.currentScenarioFilePath);
                ModifiedScenarioTemplate = false;
            }
            catch (Exception ex)
            {
                Log.Exception("scenarioTemplate.Load(currentScenarioFilePath);", ex);
                MessageBox.Show(Labels.MainForm_LoadScenario_Unable_to_apply_scenario_ + this.currentScenarioFilePath);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

            this.parameters.RefreshParameters();
            this.result.InvalidateResult();
        }

        private void BalanceSheet_Load(object sender, EventArgs e)
        {
            IsBeta = args != null &&
                          args.Length > 0 &&
                          args[0] == "beta";



            this.RefreshMenus();

            using (ISession session = SessionManager.OpenSession())
            {
                this.picLogo.Image = Param.GetLogo(session);

                Param param = Param.FindByID(session, "FileName");
                this.CurrentFilePath = NEW_WORKSPACE;
                if (param != null)
                    this.CurrentFilePath = Param.GetFileName(session);
                if (Param.GetDebugLogLevel(session))
                    Logger.SetLoggerLevel(Level.Info);
            }


            this.RefreshOpenLast();

            this.Size = Properties.Settings.Default.MySize;

            this.Location = Properties.Settings.Default.MyLoc;

            this.WindowState = FormWindowState.Maximized;
        }

       

        private void LoadUserConfiguration()
        {
            if (this.fontNameToolStripComboBox.SelectedItem as string != Settings.Default.FontFamily) this.fontNameToolStripComboBox.SelectedItem = Settings.Default.FontFamily;
            if (this.fontSizeToolStripComboBox.SelectedItem as string != Settings.Default.FontSize) this.fontSizeToolStripComboBox.SelectedItem = Settings.Default.FontSize;
        }

        private void SaveUserConfiguration()
        {
            Settings.Default.FontFamily = this.fontNameToolStripComboBox.SelectedItem as string;
            Settings.Default.FontSize = this.fontSizeToolStripComboBox.SelectedItem as string;

            Settings.Default.Save();
        }

        public void RefreshMenus(object sender, EventArgs e)
        {
            this.RefreshMenus();
        }

        public void RefreshMenus()
        {
            string[] files = { };

            string path1 = Constants.TEMPLATE_FILE_DIRECTORY_PATH;
            if (Directory.Exists(path1))
            {
                files = files.Concat(Directory.GetFiles(path1, "*.xml")).ToArray();
            }

            string[] fileNames = files.Select(Path.GetFileNameWithoutExtension).ToArray();

            string path2 = Path.Combine(Path.GetDirectoryName(typeof(MainForm).Assembly.Location), "WorkspaceTemplates");
            if (Directory.Exists(path2))
            {
                string[] files2 = Directory.GetFiles(path2, "*.xml").
                    Where(f2 => fileNames.All(f1 => string.Compare(f1, Path.GetFileNameWithoutExtension(f2), true) != 0)).
                    ToArray();
                files = files.Concat(files2).ToArray();
            }

            this.newToolStripMenuItem.DropDownItems.Clear();
            this.contextMenuStripNewPortfolio.Items.Clear();

            foreach (string filepath in files)
            {
                string filename = filepath.Replace(path2, string.Empty);
                if (filename.EndsWith(".xml"))
                {
                    string name = Path.GetFileNameWithoutExtension(filename);
                    ToolStripMenuItem _newToolStrip = new ToolStripMenuItem(Labels.MainForm_BalanceSheet_Load_With_ + name + Labels.MainForm_BalanceSheet_Load__Template);
                    this.newToolStripMenuItem.DropDownItems.Add(_newToolStrip);
                    _newToolStrip.Tag = filepath;
                    _newToolStrip.Click += this._newToolStrip_Click;
                    ToolStripMenuItem _newContextToolStrip = new ToolStripMenuItem(Labels.MainForm_BalanceSheet_Load_With_ + name + Labels.MainForm_BalanceSheet_Load__Template);
                    this.contextMenuStripNewPortfolio.Items.Add(_newContextToolStrip);
                    _newContextToolStrip.Tag = filepath;
                    _newContextToolStrip.Click += this._newToolStrip_Click;
                }
            }
            PopulateExternalDataInterfaces();

            if (IsBeta)
                this.betaToolStripMenuItem.Visible = true;
        }

        private void PopulateExternalDataInterfaces()
        {
            externalDataInterfaces = new List<ToolStripMenuItem>();
            ToolStripMenuItem item;
#if !DEBUG       
            if (LicenceInfo.Interfaces.Length > 0)
            {
               
                for (int i = 0; i < LicenceInfo.Interfaces.Length; i++)
                {
                    item = new System.Windows.Forms.ToolStripMenuItem();

                    item.Text = ExternalDataConfiguration.InterfaceClasses[LicenceInfo.Interfaces[i]].Name;
                    item.Click +=new System.EventHandler(ExternalDataConfiguration.InterfaceClasses[LicenceInfo.Interfaces[i]].Import);
                    externalDataInterfaces.Add(item);
                }
                this.dataInterfacesToolStripMenuItem.DropDownItems.AddRange(externalDataInterfaces.ToArray());
            }
            else
            {
                this.dataInterfacesToolStripMenuItem.Visible = false;
            }
#endif
#if DEBUG
            foreach (KeyValuePair<int, IExternalDataInterface> externalDataInterface in ExternalDataConfiguration.InterfaceClasses)
            {
                item = new System.Windows.Forms.ToolStripMenuItem();
                item.Text = externalDataInterface.Value.Name;
                item.Click += new System.EventHandler(externalDataInterface.Value.Import);
                externalDataInterfaces.Add(item);
            }
            this.dataInterfacesToolStripMenuItem.DropDownItems.AddRange(externalDataInterfaces.ToArray());

#endif
        }

        private void NewPortfolioMenuClick(object sender, EventArgs e)
        {
            this.contextMenuStripNewPortfolio.Show(Cursor.Position);
        }

        private void _newToolStrip_Click(object sender, EventArgs e)
        {
            this._TemplateFileName = (string)((ToolStripMenuItem)sender).Tag;
            this.New();
            this.balance.Reload();
        }

        private void computeResultsStripButton_Click(object sender, EventArgs e)
        {

            this.balance.BeforeRunSimulation();
            this.result.BeforeRunSimulation();

            this.SaveChangesInSession();

            BalanceBL.EnsureSpecialCategoriesAndBondsExist();
            BalanceBL.AjustBalance();

            LoadForm.Instance.DoWork(new WorkParameters(this.result.RunSimulation, this.SimulationEnd, true), null);
        }

        public void SimulationEnd(Exception exception)
        {
            if (this.result.SimulationEnd())
            {
                for (int i = 0; i < mainTabControl.TabCount; i++)
                {
                    if (mainTabControl.TabPages[i].Name == "resultTabPage")
                    {
                        this.mainTabControl.SelectedIndex = i;
                    }
                }

            }
        }

        public void FindAndSelectProduct(Product product, string property)
        {
            if (product == null) return;

            this.mainTabControl.SelectedIndex = 0;
            this.balance.FindAndSelectBond(product, property);
        }

        public void FindAndSelectVariableValue(VariableValue value)
        {
            this.mainTabControl.SelectedIndex = 1;
            this.parameters.FindAndSelectVariableValue(value);
        }

        private void BalanceSheetFormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.IsForcedClose) return;
            Properties.Settings.Default.MyState = this.WindowState;

            if (this.WindowState == FormWindowState.Normal)
            {

                Properties.Settings.Default.MySize = this.Size;

                Properties.Settings.Default.MyLoc = this.Location;

            }

            else
            {

                Properties.Settings.Default.MySize = this.RestoreBounds.Size;

                Properties.Settings.Default.MyLoc = this.RestoreBounds.Location;

            }


            Properties.Settings.Default.Save();

            if (!this.restart)
            {
                this.SaveChangesInSession();
            }

            if (ModifiedValues || ModifiedScenarioTemplate)
            {
                if (MessageBox.Show("Are you sure you want to quit ?\nUnsaved changes will be lost.", Constants.APPLICATION_NAME,
                                    MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                {
                    e.Cancel = true;
                    return;
                }
            }

            this.SaveUserConfiguration();
        }

        private void saveWorkspaceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.currentWorkspacePath) && this.currentWorkspacePath != "New Workspace")
                this.SaveWorkSpace(this.currentWorkspacePath);
            else this.saveAsToolStripButton_Click(sender, e);
        }


        private void saveAsToolStripButton_Click(object sender, EventArgs e)
        {

            this.workspaceSaveFileDialog.DefaultExt = AlmConfiguration.GetWorkspaceExtensionForCUrrentModule();
            this.workspaceSaveFileDialog.Filter = AlmConfiguration.GetModuleName(AlmConfiguration.CurrentModule) + " Workspace Files|*." + AlmConfiguration.GetWorkspaceExtensionForCUrrentModule() + "|All files|*.*";

            if (this.workspaceSaveFileDialog.ShowDialog() != DialogResult.OK) return;

            this.SaveWorkSpace(this.workspaceSaveFileDialog.FileName);
        }

        public void SaveWorkSpace(string workSpaceFileName)
        {
            this.Validate();
            this.SaveChangesInSession();

            if (this.multiScenarioResult != null)
            {
                this.generatorResultNodes = this.multiScenarioResult.GetGeneratorResult();
            }

            int[] scenarioSetupColumnsOrder = this.parameters.GetScenarioSetupColumnsOrder();

            WorkspaceXmlSerializer workspaceSerializer = new WorkspaceXmlSerializer(this.generatorResultNodes, scenarioSetupColumnsOrder);
            this.Cursor = Cursors.WaitCursor;
            try
            {
                workspaceSerializer.Filename = workSpaceFileName;
                TimeSpan ts = new TimeSpan(0, 0, int.Parse(ConfigurationManager.AppSettings["LAST_SAVE_DURATION"]));
                LoadForm.Instance.DoWork(new WorkParameters(workspaceSerializer.Save, null), ts);
                ConfigurationManager.AppSettings["LAST_SAVE_DURATION"] =
                    LoadForm.Instance.WorkDurationInSeconds.ToString();
                this.SetModified(false);
                ModifiedValuesSuspended = false;

                ModifiedScenarioTemplate = false;

                this.CurrentWorkspacePath = workSpaceFileName;
                this.AddOpenLast(this.CurrentWorkspacePath);
            }
            catch (Exception ex)
            {
                Log.Exception("workspaceSerializer.Save(workspaceSaveFileDialog.FileName);", ex);
                MessageBox.Show(Labels.MainForm_SaveWorkSpace_Unable_to_save_workspace_file_ + this.CurrentFilePath);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void RefreshOpenLast()
        {
            IEnumerable<string> files = this.fileHistory.GetFiles().Where(f => string.Compare(f, this.currentWorkspacePath) != 0);

            this.openLastToolStripMenuItem.DropDownItems.Clear();
            foreach (string file in files)
            {
                ToolStripMenuItem toolStripMenuItem = new ToolStripMenuItem();
                toolStripMenuItem.Text = file;
                toolStripMenuItem.Click += this._toolStripMenuItem_Click;
                this.openLastToolStripMenuItem.DropDownItems.Add(toolStripMenuItem);
            }
        }

        private void _toolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Validate();

            if (ModifiedValues && this.AskSaveChanges() != DialogResult.OK)
                return;

            this.OpenWorkspace(((ToolStripMenuItem)sender).Text);
            MainForm.Instance.InitializeOpeningBalance();
        }


        private void openWorkspacetoolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Validate();

            if (ModifiedValues && this.AskSaveChanges() != DialogResult.OK)
                return;

            if (ModifiedScenarioTemplate)
            {
                if (MessageBox.Show(Properties.Resources.NewFilePrompt, "", MessageBoxButtons.OKCancel,
                                    MessageBoxIcon.Question) != DialogResult.OK)
                    return;
            }
            this.workspaceOpenFileDialog.DefaultExt = AlmConfiguration.GetWorkspaceExtensionForCUrrentModule();
            this.workspaceOpenFileDialog.Filter = AlmConfiguration.GetModuleName(AlmConfiguration.CurrentModule) + " Workspace Files|*." + AlmConfiguration.GetWorkspaceExtensionForCUrrentModule() + "|All files|*.*";

            if (this.workspaceOpenFileDialog.ShowDialog() != DialogResult.OK) return;

            this.Cursor = Cursors.WaitCursor;

            this.SaveChangesInSession();

            this.OpenWorkspace(this.workspaceOpenFileDialog.FileName);
        }

        private DialogResult AskSaveChanges()
        {
            return MessageBox.Show(Properties.Resources.NewFilePrompt, "",
                MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
        }

        private void OpenWorkspace(string fileName)
        {
            WorkspaceXmlSerializer workspaceSerializer = new WorkspaceXmlSerializer();
            workspaceSerializer.Filename = fileName;
            Settings.Default.Upgrade();
            TimeSpan ts = new TimeSpan(0, 0, Settings.Default.LAST_LOAD_DURATION);
            this.CurrentWorkspacePath = fileName;
            LoadForm.Instance.DoWork(
                new WorkParameters(bw => this.LoadWorkspace(workspaceSerializer, bw), this.OpenWorkspaceEnd),
                ts);

        }

        private void LoadWorkspace(WorkspaceXmlSerializer serializer, BackgroundWorker worker)
        {
            serializer.Load(worker);
            this.generatorResultNodes = serializer.GeneratorResultNodes;
            this.setSetupColumnsOrder = serializer.ScenarioSetupColumnsOrder;

            BalanceBL.EnsureSpecialCategoriesAndBondsExist();
            SolvencyAdjustment.EnsureExist();
            CurrencyBL.RemoveUnusedCurrencies();

            using (ISession session = SessionManager.OpenSession())
            {
                Util.UpdateBookValueForDynamicVariables(new BindingList<DynamicVariable>(new List<DynamicVariable>(DynamicVariable.FindAll(session))),
                                                 session);
            }

        }

        public void OpenWorkspaceEnd(Exception exception)
        {
            Settings.Default.LAST_LOAD_DURATION = LoadForm.Instance.WorkDurationInSeconds;
            Settings.Default.Save();
            MainForm.Instance.InitializeOpeningBalance();
            this.AddOpenLast(this.CurrentWorkspacePath);
            if (this.balance != null) this.balance.SetScenarios();

            if (exception != null)
            {
                Dialogs.Warning(Constants.APPLICATION_NAME, exception.Message);
            }

            try
            {
                ConfigurationManager.AppSettings["LAST_LOAD_DURATION"] =
                    LoadForm.Instance.WorkDurationInSeconds.ToString();

                ModifiedScenarioTemplate = false;
                this.SetModified(false);
                ModifiedValuesSuspended = false;
            }
            catch (Exception ex)
            {
                Log.Exception("workspaceSerializer.Load(workspaceOpenFileDialog.FileName);", ex);
                MessageBox.Show(Labels.MainForm_SimulationOpenWorkspaceEnd_Unable_to_load_workspace_ + this.currentScenarioFilePath);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            this.parameters.RefreshParameters();
            this.balance.Reload();

            this.result.InvalidateResult();

            this.parameters.SetSetupColumnsOrder(this.setSetupColumnsOrder);

            if (this.generatorResultNodes != null && this.generatorResultNodes.Length > 0)
            {
                this.ShowMultiScenarioResult();
                this.multiScenarioResult.SetGeneratorResult(this.generatorResultNodes);
            }
            else
            {
                //if (this.mainTabControl.TabPages.Count > 3)
                //{
                //    this.mainTabControl.TabPages.RemoveAt(3);
                //    this.multiScenarioResult = null;
                //}
            }
            ReloadMultiScenarioParameters();
            hideMultiScenarioResult();
            mainTabControl.SelectedIndex = 0;

            using (ISession session = SessionManager.OpenSession())
            {
                this.picLogo.Image = Param.GetLogo(session);
            }

        }

        private void ReloadMultiScenarioParameters()
        {
            this.multiScenarioParameters.Controls.Remove(this.multiScenarioParameters1);
            this.multiScenarioParameters1 = null;
            this.multiScenarioParameters1 = new BondManager.Views.Generator.MultiScenarioParameters();
            this.multiScenarioParameters.Controls.Add(this.multiScenarioParameters1);
            this.multiScenarioParameters1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.multiScenarioParameters1.Location = new System.Drawing.Point(3, 3);
            this.multiScenarioParameters1.Name = "multiScenarioParameters1";
            this.multiScenarioParameters1.Size = new System.Drawing.Size(1245, 367);
            this.multiScenarioParameters1.TabIndex = 0;
            this.multiScenarioParameters1.Load += new System.EventHandler(this.multiScenarioParameters1_Load);


        }

        private void clearALLStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ModifiedValues && MessageBox.Show(Properties.Resources.ClearAllPrompt, "", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) !=
                DialogResult.OK)
                return;
            ModifiedValues = false;

            Initialiser.NewDatabase();

            this.CurrentWorkspacePath = "New Workspace";
            MainForm.Instance.InitializeOpeningBalance();
            //Application.Restart();
            this.restart = true;
            this.Close();
        }

        private void BalanceSheet_HelpRequested(object sender, HelpEventArgs hlpevent)
        {
            string helpFile = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location),
                                           "help.chm");
            try
            {
                Process.Start(helpFile);
            }
            catch (Exception ex)
            {
                Logger.Exception(helpFile, ex);
                Dialogs.Error(Constants.APPLICATION_NAME, Labels.MainForm_BalanceSheet_HelpRequested_Help_file_not_found_or_invalid);
            }

            //System.Diagnostics.Process.Start("https://www.dropbox.com/s/0c2pw18cjfd14gj/Aide%20en%20ligne%20de%20ALM-Solutions.pdf");
        }

        private void showBankALMHelpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.BalanceSheet_HelpRequested(sender, null);
        }

        private void dynamicVectorsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.ShowDynamicVectorManager();
        }

        private void ShowDynamicVectorManager()
        {
            Tools.ShowDialog(new DynamicVariableManager());
            this.balance.RefreshSelectedCategory();
        }

        private void MultiScenarioToolStripButtonClick(object sender, EventArgs e)
        {
            if (AlmConfiguration.IsBankALMModule() && !mainTabControl.TabPages.Contains(multiScenarioParameters))
            {
                mainTabControl.TabPages.Add(multiScenarioParameters);
                mainTabControl.SelectedIndex = multiScenarioParameters.TabIndex;
                //multiScenarioParameters. DisposeOldScenario += this.DisposeOldScenario;
            }
            else if (AlmConfiguration.IsBankALMModule() && mainTabControl.TabPages.Contains(multiScenarioParameters))
            {
                mainTabControl.TabPages.Remove(multiScenarioParameters);
                hideMultiScenarioResult();
                mainTabControl.SelectedIndex = 0;
                //multiScenarioParameters. DisposeOldScenario += this.DisposeOldScenario;
            }


            //if (Tools.ShowDialog(multiScenarioParameters) != DialogResult.Cancel)
            //{
            //    this.RefreshScenarioGenerator(multiScenarioParameters.Simulation);
            //}


        }

        /// <summary>
        /// Delete the previous generated scenario
        /// </summary>
        private void DisposeOldScenario(object sender, EventArgs eventArgs)
        {

            //if (this.mainTabControl.Controls.Count > 3)
            //{
            foreach (Control control in this.mainTabControl.Controls)
            {
                if (control.Controls.Count == 1 && control.Controls[0] is MultiScenarioResult)
                    ((MultiScenarioResult)control.Controls[0]).DisposeSimultation();
            }
            //}
        }

        public void RefreshScenarioGenerator(Simulation simulation)
        {
            this.ShowMultiScenarioResult();
            this.multiScenarioResult.Reload(simulation);
        }

        private void ShowMultiScenarioResult()
        {
            if (this.multiScenarioResult == null)
            {
                this.multiScenarioResult = new MultiScenarioResult();
                this.multiScenarioResult.Dock = DockStyle.Fill;
                TabPage page = new TabPage();
                page.Text = Labels.MainForm_ShowMultiScenarioResult_Multi_scenario_Simulation_Result;
                page.Controls.Add(this.multiScenarioResult);
                this.mainTabControl.TabPages.Add(page);
            }

            this.mainTabControl.SelectedIndex = this.mainTabControl.TabCount - 1;
        }

        private void hideMultiScenarioResult()
        {
            if (multiScenarioResult != null)
            {
                foreach (TabPage page in mainTabControl.TabPages)
                {
                    if (page.Controls.Contains(multiScenarioResult))
                    {
                        mainTabControl.TabPages.Remove(page);
                        break;
                    }
                }
            }
        }

        private void solvencyAdjustmentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Tools.ShowDialog(new SolvencyAdjustmentConfiguration());
        }

        private void recalculateALMIDsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LoadForm.Instance.DoWork(new WorkParameters(this.RecalculateALMIds, this.OnRecalculateALMIdsFinished));
        }

        private void RecalculateALMIds(BackgroundWorker backgroundworker)
        {
            using (ISession session = SessionManager.OpenSession())
            {
                int counterAssets = 1;
                int counterLiabilities = 1;
                int counterEquity = 1;
                int counterRoll = 1;
                int counterDerivatives = 1;
                int categoriesIndex = 1;
                List<DynamicVariable> variables = new List<DynamicVariable>(DynamicVariable.FindAll(session));

                Product[] products = ProductCache.products.OrderBy(p => p.Category.FullPath().Length).ToArray();//Product.FindAll(session).OrderBy(p => p.Category.FullPath().Length).ToArray();
                Category[] categories = Category.FindAll(session).OrderBy(p => p.FullPath().Length).ToArray();

                for (int index = 0; index < products.Length; index++)
                {
                    Product product = products[index];

                    backgroundworker.ReportProgress(50 * index / products.Length);

                    int almid = 0;
                    if (product.Attrib == ProductAttrib.Roll)
                    {
                        almid = counterRoll++;
                    }
                    else
                    {
                        switch (product.Category.BalanceType)
                        {
                            case BalanceType.Asset:
                                almid = counterAssets++;
                                break;
                            case BalanceType.Liability:
                                almid = counterLiabilities++;
                                break;
                            case BalanceType.Equity:
                                almid = counterEquity++;
                                break;
                            case BalanceType.Derivatives:
                                almid = counterDerivatives++;
                                break;
                        }
                    }

                    int oldALMId = product.ALMID;
                    string oldALMIDDisplayed = Product.GetAlmidDisplayed(product.Category.Code, oldALMId);
                    string newALMIDDisplayed = Product.GetAlmidDisplayed(product.Category.Code, almid);
                    //change in all dynamic variables containting the old ALMId
                    foreach (DynamicVariable varr in variables)
                    {
                        if (varr.Formula.Contains(oldALMIDDisplayed))
                            varr.Formula = varr.Formula.Replace(oldALMIDDisplayed, newALMIDDisplayed);
                    }

                    product.ALMID = almid;

                    if (product.Attrib == ProductAttrib.Roll)
                    {
                        this.UpdateRollSpecs(products, oldALMId, almid);
                    }
                }

                for (int index = 0; index < categories.Length; index++)
                {
                    Category category = categories[index];

                    backgroundworker.ReportProgress(50 + 50 * index / categories.Length);

                    int almid = 0;
                    almid = categoriesIndex++;

                    int oldALMId = category.ALMID;
                    string oldALMIDDisplayed = category.GetDisplayedALMID();
                    category.ALMID = almid;
                    string newALMIDDisplayed = category.GetDisplayedALMID();
                    //change in all dynamic variables containting the old ALMId
                    foreach (DynamicVariable varr in variables)
                    {
                        if (varr.Formula.Contains(oldALMIDDisplayed))
                            varr.Formula = varr.Formula.Replace(oldALMIDDisplayed, newALMIDDisplayed);
                    }

                }


                session.Flush();
            }
        }

        private void UpdateRollSpecs(IEnumerable<Product> products, int oldRollAlmId, int rollAlmid)
        {
            foreach (Product product in products)
            {
                if (Product.IsRollOf(product.RollSpec, oldRollAlmId))
                {
                    product.RollSpec = Product.GetAlmidDisplayed("R", rollAlmid);
                }
            }
        }

        private void OnRecalculateALMIdsFinished(Exception exception)
        {
            ModifiedValues = true;
        }

        private void recalculateBookValuesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LoadForm.Instance.DoWork(new WorkParameters(this.RecalculateBookValues, this.OnRecalculateBookValuesFinished));
        }

        private void RecalculateBookValues(BackgroundWorker backgroundworker)
        {
            using (ISession session = SessionManager.OpenSession())
            {
                Product[] products = ProductCache.products.ToArray();//Product.FindAll(session);

                for (int index = 0; index < products.Length; index++)
                {
                    backgroundworker.ReportProgress(80 * index / products.Length);
                    Product product = products[index];
                    if (product.Attrib == ProductAttrib.CashAdj) continue;

                    try
                    {
                        double bookPrice = Util.GetValueWithFormula(product.BookPrice, session);
                        product.ComputeBookValue(bookPrice);
                    }
                    catch (Exception ex)
                    {
                        Logger.Warn(ex);
                    }
                }

                session.Flush();
            }
        }

        private void OnRecalculateBookValuesFinished(Exception exception)
        {
            this.mainTabControl_SelectedIndexChanged(this.mainTabControl, null);

            this.parameters.LoadCurrenciesComboBox();
        }

        private void SaveAsTemplate(object sender, EventArgs e)
        {
            this.balance.SaveAsTemplate();
        }

        private void MenuVisibleChanged(object sender, EventArgs e)
        {
            if (menuStrip1.Visible == false)
            {
                mainToolStrip.Top = 0;
                mainTabControl.Top = 27;
                picLogo.Height = 48;
            }
            else
            {
                mainToolStrip.Top = 24;
                mainTabControl.Top = 52;
                picLogo.Height = 72;
            }
            mainTabControl.Height = this.Height - mainTabControl.Top - 39;
        }

        private void PicLogoClick(object sender, EventArgs e)
        {
            Image image = Tools.browseForLogo();
            this.picLogo.Image = image;
            using (ISession session = SessionManager.OpenSession())
            {
                Param.SetLogo(session, image);
                session.Flush();
            }

            ModifiedValues = true;
        }

        private void SearchToolStripButtonClick(object sender, EventArgs e)
        {
            this.FindAndSelectProductByText();
        }

        private void FindAndSelectProductByText()
        {
            string text = this.searchToolStripTextBox.Text;
            if (string.IsNullOrWhiteSpace(text)) return;

            this.Cursor = Cursors.WaitCursor;

            Product p = this.FindProductByText(text);
            if (p != null) this.FindAndSelectProduct(p, null);

            this.Cursor = Cursors.Default;
        }

        private Product FindProductByText(string text)
        {
            using (ISession session = SessionManager.OpenSession())
            {
                Product product = FindProductByALMID(session, text);
                if (product == null) product = FindProductByDescription(text, session);
                return product;
            }
        }

        private Product FindProductByALMID(ISession session, string text)
        {
            int id;
            if (int.TryParse(text, out id))
            {
                Product product = ProductCache.FindLikeALMID(id);
                if (product != null) return product;
            }

            if (text.StartsWith("A"))
            {
                return FindProductByALMIDAndBalanceType(session, text.Substring(1), BalanceType.Asset);
            }
            else if (text.StartsWith("L"))
            {
                return FindProductByALMIDAndBalanceType(session, text.Substring(1), BalanceType.Liability);
            }
            else if (text.StartsWith("E"))
            {
                return FindProductByALMIDAndBalanceType(session, text.Substring(1), BalanceType.Equity);
            }
            else if (text.StartsWith("R"))
            {
                if (int.TryParse(text.Substring(1), out id))
                {
                    Product product = ProductCache.products.FirstOrDefault(p => p.Attrib == ProductAttrib.Roll && p.ALMID == id);//session.Query<Product>().FirstOrDefault(p => p.Attrib == ProductAttrib.Roll && p.ALMID == id);
                    return product;
                }
            }

            return null;
        }

        private Product FindProductByALMIDAndBalanceType(ISession session, string text, BalanceType balanceType)
        {
            int id;
            if (!int.TryParse(text, out id))
            {
                return null;
            }

            Product product = ProductCache.products.//session.Query<Product>().
                FirstOrDefault(p => p.Category.BalanceType == balanceType && p.Attrib != ProductAttrib.Roll && p.ALMID == id);

            return product;
        }

        private static Product FindProductByDescription(string text, ISession session)
        {
            Product product = ProductCache.products.//session.Query<Product>().
                FirstOrDefault(p => p.Description.Contains(text));
            return product;
        }

        private void SearchToolStripTextBoxTextChanged(object sender, EventArgs e)
        {
            searchToolStripButton.Enabled = !string.IsNullOrEmpty(searchToolStripTextBox.Text);
        }

        private void SearchToolStripTextBoxKeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                e.Handled = true;
                this.FindAndSelectProductByText();
            }
        }

        private void mainToolStrip_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void redatevectorsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            Tools.RedateVectorsAfterValuationDateChanged();
            this.Cursor = Cursors.Arrow;
        }

        private void recalculateAllBookValuesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            Tools.RecalculateBookValues();
            this._modifiedValues = true;
            this.Refresh();
            this.Cursor = Cursors.Arrow;
        }

        private void multiScenarioParameters1_Load(object sender, EventArgs e)
        {

        }

        private void gapAnalysisToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (ISession session = SessionManager.OpenSession())
            {
                GapAnalysisParametersEditor gapAnalysisParametersEditor = new GapAnalysisParametersEditor();
                gapAnalysisParametersEditor.computeGapCheckBox.Checked = Param.GetComputeGap(session);
                gapAnalysisParametersEditor.fixedGapRate.Text = Param.GetGapFixedRate(session);
                gapAnalysisParametersEditor.gapType.SelectedIndex = Param.GetGapType(session);
                gapAnalysisParametersEditor.cashflowPresentation.SelectedIndex = Param.GetGapCashflowPresentation(session);
                gapAnalysisParametersEditor.ShowDialog();
                Param.SetComputeGap(session, gapAnalysisParametersEditor.computeGapCheckBox.Checked);
                Param.SetGapFixedRate(session, gapAnalysisParametersEditor.fixedGapRate.Text);
                Param.SetGapType(session, gapAnalysisParametersEditor.gapType.SelectedIndex);
                Param.SetGapCashflowPresentation(session,gapAnalysisParametersEditor.cashflowPresentation.SelectedIndex);
                session.Flush();
            }
        }

        private void sendLogsToMaintenanceTeamToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string zippedLogs = Path.GetTempFileName() + ".zip";
            string logsFolder = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) +
                                "\\ALM Solutions\\";
            
            CompressionTools.CompressFolderToZip(logsFolder, zippedLogs);
            Util.SendMail("info@alm-vision.com", "ALM Solution Logs Files", "Here are the log files.", zippedLogs);
        }

        private void undoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ActionStack.GetInstance().CanUndo())
            {
                Cursor.Current = Cursors.WaitCursor;
                ActionStack.GetInstance().Undo();
                balance.RefreshSelectedCategory();
                Cursor.Current = Cursors.Default;
            }
        }

        private void redoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ActionStack.GetInstance().CanRedo())
            {
                Cursor.Current = Cursors.WaitCursor;
                ActionStack.GetInstance().Redo();
                balance.RefreshSelectedCategory();
                Cursor.Current = Cursors.Default;
            }
        }

        private void editToolStripMenuItem_DropDownOpening(object sender, EventArgs e)
        {
            undoToolStripMenuItem.Enabled = ActionStack.GetInstance().CanUndo();
            redoToolStripMenuItem.Enabled = ActionStack.GetInstance().CanRedo();
        }

        private void parameters_Load(object sender, EventArgs e)
        {

        }

        private void dataInterfacesToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        void betaSaveResultAsCSVToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            this.betaSaveResultFileDialog.FileName = "";

            if (this.betaSaveResultFileDialog.ShowDialog() == DialogResult.OK)
            {
                BetaResultCSV.SaveBetaResultAsCSV = true;
                BetaResultCSV.BetaResultFileNameCSV = this.betaSaveResultFileDialog.FileName;
                this.computeResultsStripButton.PerformClick();
            }
        }

        private void toolsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.ShowVectorGroups();
        }

        private void ShowVectorGroups()
        {

        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            this.ShowVectorGroupManager();
        }

        private void ShowVectorGroupManager()
        {
            Tools.ShowDialog(new VectorGroups());

            BalanceBL.AjustBalance();

            this.balance.RefreshSelectedCategory();

            this.RefreshParameters();
        }
    }
}