﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using BondManager.Calculations;
using BondManager.Core;
using BondManager.Domain;
using Iesi.Collections.Generic;
using NHibernate.Linq;
using ZedGraph;

namespace BondManager.Views.BalanceSheet.Parameters
{
    public partial class ForwardCurvesAnalyser : SessionForm
    {
        private readonly IDictionary<string, Vector> allVectors = new SortedDictionary<string, Vector>(StringComparer.CurrentCultureIgnoreCase);
        private readonly IDictionary<string, Variable> allVariables = new SortedDictionary<string, Variable>(StringComparer.CurrentCultureIgnoreCase);
        private readonly IDictionary<string, object>[] variableAndVectorValues;
        private readonly IDictionary<string, object>[] vectorValues;
        private readonly IDictionary<string, double>[] variableValues;
        private FundamentalVector[] fundamentalVectors;
        private DateTime valuationDate;
        private List<Scenario> selectedScenarios = new List<Scenario>();
        private readonly Iesi.Collections.Generic.ISet<Variable> usedVariables = new HashedSet<Variable>();
        private readonly Iesi.Collections.Generic.ISet<Vector> usedVectors = new HashedSet<Vector>();
        private readonly ToolTip toolTip = new ToolTip();
        private readonly int currencyId;

        public ForwardCurvesAnalyser(int currencyId)
        {
            this.currencyId = currencyId;
            InitializeComponent();
            this.zedGraphControl.MouseMove += this.ZedGraphControlMouseMove;
            toolTip = new ToolTip();

            int resultMonths = Param.ResultMonths(this.Session);
            variableAndVectorValues = new IDictionary<string, object>[resultMonths];
            vectorValues = new IDictionary<string, object>[resultMonths];
            variableValues = new IDictionary<string, double>[resultMonths];

            InitVectorsAndVariables();
            ValidateFundamentalVectors();
            InitGraph();

            comboBoxScenarios.DisplayMember = "Name";
            comboBoxScenarios.ValueMember = "Self";
            comboBoxScenarios.DataSource = Scenario.FindNonGenerated(this.Session);
        }

        void ZedGraphControlMouseMove(object sender, MouseEventArgs e)
        {
            CurveItem curveItem;
            int dragIndex;

            GraphPane myPane = zedGraphControl.GraphPane;
            PointF mousePt = new PointF(e.X, e.Y);

            if (myPane.FindNearestPoint(mousePt, out curveItem, out dragIndex) &&
                     curveItem.Points is PointPairList &&
                     curveItem.YAxisIndex == 0)
            {
                toolTip.Show(curveItem.Label.Text, this.zedGraphControl, new Point(e.X, e.Y));
            }
            else
                toolTip.Hide(this);
        }

        private void InitGraph()
        {
            this.zedGraphControl.IsAntiAlias = true;
            this.zedGraphControl.PointDateFormat = "dd/MM/yyyy";

            GraphPane graphPane = this.zedGraphControl.GraphPane;

            graphPane.Title.IsVisible = false;
            graphPane.Legend.IsVisible = true;

            graphPane.XAxis.Title.IsVisible = false;
            graphPane.XAxis.Type = AxisType.Date;
            graphPane.XAxis.Scale.MajorStep = 1;
            graphPane.XAxis.Scale.MajorUnit = DateUnit.Month;
            graphPane.XAxis.Scale.FontSpec.Angle = 90;
            graphPane.XAxis.Scale.Min = new XDate(valuationDate);
            graphPane.XAxis.Scale.Max = new XDate(valuationDate.AddMonths(Param.ResultMonths(this.Session))); // todo daily
            graphPane.XAxis.Scale.Format = "dd/MM/yyyy";

            graphPane.YAxis.Title.Text = "";
            graphPane.YAxis.Title.IsVisible = true;
            graphPane.LineType = LineType.Normal;

            CreateGraphCurves();
        }

        private void ComboBoxScenariosSelectedIndexChanged(object sender, EventArgs e)
        {
            Scenario scenario = comboBoxScenarios.SelectedValue as Scenario;
            if (scenario != null)
            {
                SimulationParams simulationParams = Util.GetSimulationParams(this.Session);
                List<ValidationMessage> errors = ComputeVectorAndVariablesValues(scenario, simulationParams, false);
                if (errors.Count == 0)
                {
                    InitVariableAndVectorValues();
                    DrawCurves();
                }
                else
                {
                    DrawCurves0();
                }
            }
        }

        private void CreateGraphCurves()
        {
            GraphPane graphPane = this.zedGraphControl.GraphPane;
            graphPane.CurveList.Clear();

            List<Color> colorsUsed = new List<Color>();
            foreach (FundamentalVector fundamentalVector in fundamentalVectors)
            {
                Color color = CreateRandomColor();
                while (colorsUsed.Contains(color))
                {
                    color = CreateRandomColor();
                }
                colorsUsed.Add(color);

                LineItem lineItem = new LineItem(fundamentalVector.Name, new double[Param.ResultMonths(this.Session)], new double[fundamentalVectors.Count()], color, SymbolType.None);
                lineItem.Line.Width = 1.5f;
                graphPane.CurveList.Add(lineItem);
            }
        }

        private Color CreateRandomColor()
        {
            Random randonGen = new Random();
            Color randomColor = Color.FromArgb(randonGen.Next(255), randonGen.Next(255), randonGen.Next(255));

            return randomColor;
        }

        private void DrawCurves()
        {
            for (int i = 0; i < Param.ResultMonths(this.Session); i++)
            {
                double[] zc = new double[fundamentalVectors.Length];
                for (int fIndex = 0; fIndex < fundamentalVectors.Length; fIndex++) zc[fIndex] = (double)variableAndVectorValues[i][fundamentalVectors[fIndex].Variable.Name];

                for (int j = 0; j < zedGraphControl.GraphPane.CurveList.Count; j++)
                {
                    zedGraphControl.GraphPane.CurveList[j][i].X = new XDate(valuationDate.AddMonths(i));
                    zedGraphControl.GraphPane.CurveList[j][i].Y = zc[j];
                }
            }

            zedGraphControl.AxisChange();
            zedGraphControl.Invalidate();
        }

        private void DrawCurves0()
        {
            for (int i = 0; i < Param.ResultMonths(this.Session); i++)
            {
                foreach (CurveItem curveItem in this.zedGraphControl.GraphPane.CurveList)
                {
                    curveItem[i].X = new XDate(this.valuationDate.AddMonths(i));
                    curveItem[i].Y = 0;
                }
            }

            zedGraphControl.AxisChange();
            zedGraphControl.Invalidate();
        }


        private void InitVectorsAndVariables()
        {
            Vector[] vectors = Vector.FindAll(this.Session);
            foreach (Vector vector in vectors)
            {
                allVectors.Add(vector.Name, vector);
            }

            Variable[] variables = Variable.FindAll(this.Session);
            foreach (Variable variable in variables)
            {
                allVariables.Add(variable.Name, variable);
            }

            fundamentalVectors = this.Session.Query<FundamentalVector>().Where(v =>v.Variable != null &&  v.Currency.Id == currencyId).ToArray();
            selectedScenarios = new List<Scenario>(Scenario.FindSelected(this.Session));
            valuationDate = Param.GetValuationDate(this.Session) ?? DateTime.Today;
        }

        private List<ValidationMessage> ComputeVectorAndVariablesValues(Scenario scenario, SimulationParams parameters, bool isDaily)
        {
            ExpressionParser parser = new ExpressionParser();
            List<ValidationMessage> validationErrors = new List<ValidationMessage>();

            Util.ValidateVariables(selectedScenarios, usedVariables, allVectors, usedVectors, validationErrors);
            if (validationErrors.Count > 0) return validationErrors;

            Util.ComputeVectorValues(valuationDate, vectorValues, usedVectors, parameters.ResultSteps, isDaily);
            Util.ComputeVariableValues(scenario, parser, vectorValues, variableValues, usedVariables, validationErrors, parameters);
            if (validationErrors.Count > 0) return validationErrors;
            return validationErrors;
        }

        private void ValidateFundamentalVectors()
        {
            foreach (FundamentalVector vector in fundamentalVectors)
            {
                if (vector.Variable == null) continue;

                usedVariables.Add(vector.Variable);

                foreach (VariableValue variableValue in vector.Variable.VariableValues)
                {
                    if (!string.IsNullOrEmpty(variableValue.Value))
                    {
                        string[] operands = Tools.GetOperandsFromExpression(variableValue.Value);
                        foreach (string operand in operands)
                        {
                            if (Tools.IsDecimalString(operand))
                            {
                                continue;
                            }
                            if (allVectors.ContainsKey(operand))
                            {
                                usedVectors.Add(allVectors[operand]);
                                continue;
                            }
                            if (allVariables.ContainsKey(operand))
                            {
                                usedVariables.Add(allVariables[operand]);
                            }
                        }
                    }
                }
            }
        }

        private void InitVariableAndVectorValues()
        {
            for (int i = 0; i < Param.ResultMonths(this.Session); i++)
            {
                variableAndVectorValues[i] = new SortedDictionary<string, object>(StringComparer.CurrentCultureIgnoreCase);

                foreach (KeyValuePair<string, object> vectorValue in vectorValues[i])
                {
                    variableAndVectorValues[i][vectorValue.Key] = vectorValue.Value;
                }

                foreach (KeyValuePair<string, double> variableValue in variableValues[i])
                {
                    variableAndVectorValues[i][variableValue.Key] = variableValue.Value;
                }
            }
        }
    }
}
