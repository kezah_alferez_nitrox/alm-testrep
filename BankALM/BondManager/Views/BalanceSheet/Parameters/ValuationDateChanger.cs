﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BondManager.Views.BalanceSheet.Parameters
{
    public partial class ValuationDateChanger : Form
    {
        public ValuationDateChanger()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MainForm.ValuationDate = valuationDateTimePicker.Value.Date;
            this.Close();
        }
    }
}
