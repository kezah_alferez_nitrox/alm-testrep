using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Forms;
using ALMS.Products;
using ALMSCommon;
using ALMSCommon.Controls;
using BankALM.Infrastructure.Data;
using BondManager.BusinessLogic;
using BondManager.Domain;
using System.Linq;
using BondManager.Resources.Language;
using Eu.AlterSystems.ASNetLib.Core;
using NHibernate;
using NHibernate.Linq;

namespace BondManager.Views.BalanceSheet.Parameters
{
    public class FundamentalVectorsGridControler
    {
        private ISession session;

        private readonly DataGridView grid;
        private DataGridViewTextBoxColumn nameColumn;
        private DataGridViewTextBoxColumn deltaColumn;
        private DataGridViewComboBoxColumn unitColumn;
        private DataGridViewComboBoxColumn variableColumn;
        private DataGridViewComboBoxColumn typeColumn;
        private DataGridViewComboBoxColumn dateConventionColumn;
        private DataGridViewComboBoxColumn frequencyColumn;
        private AutoCompleteColumn volatilityColumn;
        private BindingList<FundamentalVector> gridData;
        private Currency currency;
        private Variable[] variables;

        public FundamentalVectorsGridControler(DataGridView grid)
        {
            this.grid = grid;
            this.grid.EditMode = DataGridViewEditMode.EditOnEnter;
            grid.AutoGenerateColumns = false;
            grid.CellValueChanged += this.GridCellValueChanged;

            this.RefreshSession();

            this.InitColumns();
        }

        void GridCellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewRow row = grid.CurrentRow;
            if (row == null) return;

            FundamentalVector fundamentalVector = row.DataBoundItem as FundamentalVector;
            if (fundamentalVector == null) return;

            if (fundamentalVector.Currency != this.currency && this.currency != null)
            {
                fundamentalVector.Currency = this.currency;
            }

            if (e.ColumnIndex == this.typeColumn.Index)
            {
                this.SetFrequencyCell(row.Cells[this.frequencyColumn.Index], fundamentalVector.Type == FundamentalVectorType.Libor);
            }

            fundamentalVector.Save(this.session);
            this.session.Flush();
        }

        private void InitColumns()
        {
            this.variables = new[] { new Variable() }.Concat(Variable.FindAll(this.session)).ToArray();

            grid.Columns.Clear();
            grid.Rows.Clear();

            this.nameColumn = new DataGridViewTextBoxColumn();
            this.deltaColumn = new DataGridViewTextBoxColumn();
            this.unitColumn = new DataGridViewComboBoxColumn();
            this.variableColumn = new DataGridViewComboBoxColumn();
            this.typeColumn = new DataGridViewComboBoxColumn();
            this.dateConventionColumn = new DataGridViewComboBoxColumn();
            this.frequencyColumn = new DataGridViewComboBoxColumn();
            this.volatilityColumn = new AutoCompleteColumn();

            // 
            // nameColumn
            // 
            this.nameColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
            this.nameColumn.HeaderText = Labels.FundamentalVectorsGridControler_InitColumns_Name;
            this.nameColumn.Name = "nameColumn";
            this.nameColumn.Width = 100;
            this.nameColumn.DataPropertyName = "Name";
            this.nameColumn.Visible = false;
            // 
            // deltaColumn
            // 
            this.deltaColumn.HeaderText = Labels.FundamentalVectorsGridControler_InitColumns_Date_Offset;
            this.deltaColumn.Name = "deltaColumn";
            this.deltaColumn.DataPropertyName = "Delta";
            this.deltaColumn.DefaultCellStyle.Format = "N0";
            // 
            // unitColumn
            // 
            this.unitColumn.FlatStyle = FlatStyle.Flat;
            this.unitColumn.HeaderText = Labels.FundamentalVectorsGridControler_InitColumns_Unit;
            this.unitColumn.Name = "unitColumn";
            this.unitColumn.DataPropertyName = "Unit";
            this.unitColumn.DisplayMember = "Value";
            this.unitColumn.ValueMember = "Key";
            this.unitColumn.DataSource = EnumHelper.ToList(typeof(TimeUnit));
            // 
            // variableColumn
            // 
            this.variableColumn.FlatStyle = FlatStyle.Flat;
            this.variableColumn.HeaderText = Labels.FundamentalVectorsGridControler_InitColumns_Variable;
            this.variableColumn.Name = "variableColumn";
            this.variableColumn.DataPropertyName = "Variable";
            this.variableColumn.DisplayMember = "Name";
            this.variableColumn.ValueMember = "Self";
            this.variableColumn.DataSource = this.variables;
            // 
            // typeColumn
            // 
            this.typeColumn.FlatStyle = FlatStyle.Flat;
            this.typeColumn.HeaderText = Labels.FundamentalVectorsGridControler_InitColumns_Type;
            this.typeColumn.Name = "typeColumn";
            this.typeColumn.DataPropertyName = "Type";
            this.typeColumn.DisplayMember = "Value";
            this.typeColumn.ValueMember = "Key";
            this.typeColumn.DataSource = EnumHelper.ToList(typeof(FundamentalVectorType));
            // 
            // dateConventionColumn
            // 
            this.dateConventionColumn.FlatStyle = FlatStyle.Flat;
            this.dateConventionColumn.HeaderText = Labels.FundamentalVectorsGridControler_InitColumns_Convention_de_date;
            this.dateConventionColumn.Name = "dateConventionColumn";
            this.dateConventionColumn.DataPropertyName = "DateConvention";
            this.dateConventionColumn.DisplayMember = "Value";
            this.dateConventionColumn.ValueMember = "Key";
            this.dateConventionColumn.DataSource = EnumHelper.ToList(typeof(CouponBasis));
            // 
            // frequencyColumn
            // 
            this.frequencyColumn.FlatStyle = FlatStyle.Flat;
            this.frequencyColumn.HeaderText = Labels.FundamentalVectorsGridControler_InitColumns_Frequency;
            this.frequencyColumn.Name = "frequencyColumn";
            this.frequencyColumn.DataPropertyName = "Frequency";
            this.frequencyColumn.DisplayMember = "Value";
            this.frequencyColumn.ValueMember = "Key";
            this.frequencyColumn.DataSource = EnumHelper.ToList(typeof(ProductFrequency));
            // 
            // volatilityColumn
            // 
            this.volatilityColumn.HeaderText = Labels.FundamentalVectorsGridControler_InitColumns_Volatility;
            this.volatilityColumn.Name = "volatilityColumn";
            this.volatilityColumn.DataPropertyName = "Volatility";
            this.volatilityColumn.ValueList = DomainTools.GetAllVariablesAndVectorNames(this.session); 


            this.grid.Columns.AddRange(new DataGridViewColumn[]
                {
                    this.variableColumn,
                    this.nameColumn,
                    this.deltaColumn,
                    this.unitColumn,
                    //this.variableColumn,
                    this.typeColumn,
                    this.dateConventionColumn,
                    this.frequencyColumn,
                    this.volatilityColumn
                });
        }

        private void RefreshSession()
        {
            if (this.session != null)
            {
                this.session.Flush();
                this.session.Dispose();
            }
            this.session = SessionManager.OpenSession(); // todo: dispose
        }

        public void Refresh(Currency newCurrency)
        {
            if (newCurrency == null)
            {
                this.grid.DataSource = null;
                return;
            }

            this.RefreshSession();
            this.currency = Currency.Find(this.session, newCurrency.Id);

            if (currency == null)
            {
                this.grid.DataSource = null;
                return;
            }

            this.InitColumns();

            FundamentalVector[] vectors = this.session.Query<FundamentalVector>().Where(f => f.Currency.Id == currency.Id).ToArray();
            this.gridData = new BindingList<FundamentalVector>(new List<FundamentalVector>(vectors));
            this.grid.DataSource = this.gridData;

            foreach (DataGridViewRow row in this.grid.Rows)
            {
                FundamentalVector fundamentalVector = (FundamentalVector)row.DataBoundItem;
                if (fundamentalVector == null) continue;
                this.SetFrequencyCell(row.Cells[frequencyColumn.Index], false/*fundamentalVector.Type == FundamentalVectorType.Libor*/);
            }
        }

        private void SetFrequencyCell(DataGridViewCell cell, bool enabled)
        {
            if (enabled)
            {
                cell.Value = ProductFrequency.Annual;
                cell.ReadOnly = true;
                cell.Style.BackColor = AlmsColors.ControlLight;
                cell.Style.ForeColor = AlmsColors.ControlLight;
            }
            else
            {
                cell.ReadOnly = false;
                cell.Style.BackColor = grid.DefaultCellStyle.BackColor;
                cell.Style.ForeColor = grid.DefaultCellStyle.ForeColor;
            }
        }

        public void DeleteRows()
        {
            foreach (DataGridViewRow row in grid.SelectedRows)
            {
                DeleteRow(row);
            }
            session.Flush();
        }

        private void DeleteRow(DataGridViewRow row)
        {
            FundamentalVector fundamentalVector = row.DataBoundItem as FundamentalVector;
            if (fundamentalVector == null) return;

            fundamentalVector.Delete(session);

            grid.Rows.Remove(row);
        }
    }
}