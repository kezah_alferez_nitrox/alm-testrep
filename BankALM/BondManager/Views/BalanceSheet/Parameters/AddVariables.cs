using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using BondManager.Core;
using BondManager.Domain;
using BondManager.Views.Variables;

namespace BondManager.Views.BalanceSheet.Parameters
{
    public partial class AddVariables : SessionForm
    {
        private BindingList<Variable> availableList;
        private BindingList<Variable> selectedList;

        public AddVariables()
        {
            InitializeComponent();
        }

        private void AddVariablesLoad(object sender, System.EventArgs e)
        {
            this.LoadVariables();
        }

        private void LoadVariables()
        {
            this.availableListBox.DisplayMember = "Name";
            this.availableListBox.ValueMember = "Self";
            this.availableListBox.DataSource =
                this.availableList = new BindingList<Variable>(new List<Variable>(Variable.FindAvailable(this.Session)));

            this.selectedListBox.DisplayMember = "Name";
            this.selectedListBox.ValueMember = "Self";
            this.selectedListBox.DataSource =
                this.selectedList = new BindingList<Variable>(new List<Variable>(Variable.FindSelected(this.Session)));
        }

        private void AddButtonClick(object sender, System.EventArgs e)
        {
            Variable[] toAdd = this.availableListBox.SelectedIndices.Cast<int>().Select(i => this.availableList[i]).ToArray();

            foreach (Variable variable in toAdd)
            {
            availableList.Remove(variable);
            selectedList.Add(variable);
            }

            availableListBox.SelectedItem = null;
            selectedListBox.SelectedItem = null;
            for (int i = 0; i < toAdd.Length; i++)
            {
                selectedListBox.SetSelected(selectedListBox.Items.Count - i - 1, true);
            }

            MainForm.ModifiedScenarioTemplate = true;
            MainForm.ModifiedValues = true;
        }

        private void RemoveButtonClick(object sender, System.EventArgs e)
        {
            Variable[] toRemove = this.selectedListBox.SelectedIndices.Cast<int>().Select(i => this.selectedList[i]).ToArray();

            foreach (Variable variable in toRemove)
            {
            selectedList.Remove(variable);
            availableList.Add(variable);
            }

            selectedListBox.SelectedItem = null;
            availableListBox.SelectedItem = null;
            for (int i = 0; i < toRemove.Length; i++)
            {
                availableListBox.SetSelected(availableListBox.Items.Count - i - 1, true);
            }

            MainForm.ModifiedScenarioTemplate = true;
            MainForm.ModifiedValues = true;
        }

        private void OkButtonClick(object sender, System.EventArgs e)
        {
            foreach (Variable variable in selectedList)
            {
                variable.Selected = true;
            }
            foreach (Variable variable in availableList)
            {
                variable.Selected = false;
            }

            //PersistenceSession.Flush();
            this.SaveChangesInSession();
        }

        private void AvailableListBoxSelectedIndexChanged(object sender, System.EventArgs e)
        {
            addButton.Enabled = this.availableListBox.SelectedIndices.Count > 0;
    }

        private void SelectedListBoxSelectedIndexChanged(object sender, System.EventArgs e)
        {
            removeButton.Enabled = this.selectedListBox.SelectedIndices.Count > 0;
        }

        private void CloneVariable(object sender, System.EventArgs e)
        {
            CopyVariable form = new CopyVariable();
            if (form.ShowDialog() == DialogResult.OK)
            {
                this.LoadVariables();
            }
        }
    }
}