using System.Windows.Forms;
using BondManager.Core;
using BondManager.Domain;
using BondManager.Resources.Language;

namespace BondManager.Views.BalanceSheet.Parameters
{
    public partial class CreateScenario : SessionForm
    {
        private bool valid;

        public CreateScenario()
        {
            InitializeComponent();
        }

        public string ScenarioName
        {
            get
            {
                return scenarioNameTextBox.Text;
            }
        }

        private void okButton_Click(object sender, System.EventArgs e)
        {
            if (Scenario.ExistsByName(this.Session, scenarioNameTextBox.Text))
            {
                MessageBox.Show(string.Format(Properties.Resources.ScenarioAlreadyExists, scenarioNameTextBox.Text),
                    Labels.CreateScenario_okButton_Click_Error, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                valid = false;
                return;
            }

            valid = true;
        }

        private void CreateScenario_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = !valid;
        }

        private void cancelButton_Click(object sender, System.EventArgs e)
        {
            valid = true;
        }

        private void scenarioNameTextBox_TextChanged(object sender, System.EventArgs e)
        {
            okButton.Enabled = !string.IsNullOrEmpty(scenarioNameTextBox.Text);
        }
    }
}