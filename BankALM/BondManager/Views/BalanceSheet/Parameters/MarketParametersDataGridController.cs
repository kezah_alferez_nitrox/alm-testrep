using System;
using System.Linq;
using System.Windows.Forms;
using ALMSCommon;
using BankALM.Infrastructure.Data;
using BondManager.Domain;
using BondManager.Resources.Language;
using NHibernate;

namespace BondManager.Views.BalanceSheet.Parameters
{
    public class MarketParametersDataGridController
    {
        private readonly DataGridView grid;
        private DataGridViewColumn[] columns;

        private DataGridViewTextBoxColumn curvDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn dayDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn fiveyDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn onemDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn oneyDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn sixDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn tenyDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn threemDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn twoyDataGridViewTextBoxColumn;

        public MarketParametersDataGridController(DataGridView grid)
        {
            this.grid = grid;
            grid.AutoGenerateColumns = false;
            grid.AllowUserToAddRows = false;

            grid.RowTemplate.Height = Constants.GRID_ROW_HEIGHT;
            this.InitColumns();
        }

        private SortableBindingList<MarketParameters> VectorList
        {
            get { return this.grid.DataSource as SortableBindingList<MarketParameters>; }
            set { this.grid.DataSource = value; }
        }

        private void InitColumns()
        {
            this.curvDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            this.dayDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            this.onemDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            this.threemDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            this.sixDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            this.oneyDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            this.twoyDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            this.fiveyDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            this.tenyDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();

            //
            //curvDataGridViewTextBoxColumn
            //

            this.curvDataGridViewTextBoxColumn.DataPropertyName = "Curve";
            this.curvDataGridViewTextBoxColumn.HeaderText = Labels.MarketParametersDataGridController_InitColumns_Vector;
            this.curvDataGridViewTextBoxColumn.Name = "Name";
            this.curvDataGridViewTextBoxColumn.Width = 100;

            //
            //dayDataGridViewTextBoxColumn
            //
            this.dayDataGridViewTextBoxColumn.HeaderText = Labels.MarketParametersDataGridController_InitColumns_Day;
            this.dayDataGridViewTextBoxColumn.Name = "Day";
            this.dayDataGridViewTextBoxColumn.DataPropertyName = "Day";
            this.dayDataGridViewTextBoxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dayDataGridViewTextBoxColumn.AutoSizeMode=DataGridViewAutoSizeColumnMode.AllCells;
            //
            //onemDataGridViewTextBoxColumn
            //
            this.onemDataGridViewTextBoxColumn.HeaderText = Labels.MarketParametersDataGridController_InitColumns__1M;
            this.onemDataGridViewTextBoxColumn.Name = "1M";
            this.onemDataGridViewTextBoxColumn.DataPropertyName = "Onem";
            this.onemDataGridViewTextBoxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.onemDataGridViewTextBoxColumn.AutoSizeMode=DataGridViewAutoSizeColumnMode.AllCells;

            //
            //threemDataGridViewTextBoxColumn
            //
            this.threemDataGridViewTextBoxColumn.HeaderText = Labels.MarketParametersDataGridController_InitColumns__3M;
            this.threemDataGridViewTextBoxColumn.Name = "3M";
            this.threemDataGridViewTextBoxColumn.DataPropertyName = "Threem";
            this.threemDataGridViewTextBoxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.threemDataGridViewTextBoxColumn.AutoSizeMode=DataGridViewAutoSizeColumnMode.AllCells;

            //
            //sixDataGridViewTextBoxColumn
            //
            this.sixDataGridViewTextBoxColumn.HeaderText = Labels.MarketParametersDataGridController_InitColumns__6M;
            this.sixDataGridViewTextBoxColumn.Name = "6M";
            this.sixDataGridViewTextBoxColumn.DataPropertyName = "Sixm";
            this.sixDataGridViewTextBoxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.sixDataGridViewTextBoxColumn.AutoSizeMode=DataGridViewAutoSizeColumnMode.AllCells;

            //
            //oneyDataGridViewTextBoxColumn
            //
            this.oneyDataGridViewTextBoxColumn.HeaderText = Labels.MarketParametersDataGridController_InitColumns__1Y;
            this.oneyDataGridViewTextBoxColumn.Name = "1Y";
            this.oneyDataGridViewTextBoxColumn.DataPropertyName = "Oney";
            this.oneyDataGridViewTextBoxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.oneyDataGridViewTextBoxColumn.AutoSizeMode=DataGridViewAutoSizeColumnMode.AllCells;

            //
            //twoyDataGridViewTextBoxColumn
            //
            this.twoyDataGridViewTextBoxColumn.HeaderText = Labels.MarketParametersDataGridController_InitColumns__2Y;
            this.twoyDataGridViewTextBoxColumn.Name = "2Y";
            this.twoyDataGridViewTextBoxColumn.DataPropertyName = "Twoy";
            this.twoyDataGridViewTextBoxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.twoyDataGridViewTextBoxColumn.AutoSizeMode=DataGridViewAutoSizeColumnMode.AllCells;

            //
            //fiveyDataGridViewTextBoxColumn
            //
            this.fiveyDataGridViewTextBoxColumn.HeaderText = Labels.MarketParametersDataGridController_InitColumns__5Y;
            this.fiveyDataGridViewTextBoxColumn.Name = "5Y";
            this.fiveyDataGridViewTextBoxColumn.DataPropertyName = "Fivey";
            this.fiveyDataGridViewTextBoxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.fiveyDataGridViewTextBoxColumn.AutoSizeMode=DataGridViewAutoSizeColumnMode.AllCells;

            //
            //tenyDataGridViewTextBoxColumn
            //
            this.tenyDataGridViewTextBoxColumn.HeaderText = Labels.MarketParametersDataGridController_InitColumns__10Y;
            this.tenyDataGridViewTextBoxColumn.Name = "10Y";
            this.tenyDataGridViewTextBoxColumn.DataPropertyName = "Teny";
            this.tenyDataGridViewTextBoxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.tenyDataGridViewTextBoxColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;

            this.columns = new DataGridViewColumn[]
                               {
                                   this.curvDataGridViewTextBoxColumn,
                                   this.dayDataGridViewTextBoxColumn,
                                   this.onemDataGridViewTextBoxColumn,
                                   this.threemDataGridViewTextBoxColumn,
                                   this.sixDataGridViewTextBoxColumn,
                                   this.oneyDataGridViewTextBoxColumn,
                                   this.twoyDataGridViewTextBoxColumn,
                                   this.fiveyDataGridViewTextBoxColumn,
                                   this.tenyDataGridViewTextBoxColumn
                               };
            this.grid.Columns.Clear();
            this.grid.Columns.AddRange(this.columns);
        }

        public void Refresh(bool resetColumns, DateTime valuationDate)
        {
            if (this.VectorList != null)
                this.VectorList.Clear();

            SortableBindingList<MarketParameters> gridData = new SortableBindingList<MarketParameters>();
            using (ISession session = SessionManager.OpenSession())
            {
                foreach (
                    Vector vect in VectorCache.FindByImportantMarketData().OrderBy(v => v.Position).ThenBy(v => v.Id))
                {
                    MarketParameters mp = new MarketParameters();
                    mp.Curve = vect.Name;
                    mp.Vector = vect;

                    foreach (VectorPoint vp in vect.VectorPoints)
                    {
                        if (vp.Date >= valuationDate && mp.Day == null)
                        {
                            mp.Day = FormatVectorValue(vp.Value, vect.VectorType);
                        }

                        int monthsApart = 12*(valuationDate.Year - vp.Date.Year) + valuationDate.Month - vp.Date.Month;
                        monthsApart = Math.Abs(monthsApart);

                        if (monthsApart >= 1 && mp.Onem == null)
                        {
                            mp.Onem = FormatVectorValue(vp.Value, vect.VectorType);
                        }

                        if (monthsApart >= 3 && mp.Threem == null)
                        {
                            mp.Threem = FormatVectorValue(vp.Value, vect.VectorType);
                        }

                        if (monthsApart >= 6 && mp.Sixm == null)
                        {
                            mp.Sixm = FormatVectorValue(vp.Value, vect.VectorType);
                        }

                        if (monthsApart >= 12 && mp.Oney == null)
                        {
                            mp.Oney = FormatVectorValue(vp.Value, vect.VectorType);
                        }
                        if (monthsApart >= 24 && mp.Twoy == null)
                        {
                            mp.Twoy = FormatVectorValue(vp.Value, vect.VectorType);
                        }
                        if (monthsApart >= 60 && mp.Fivey == null)
                        {
                            mp.Fivey = FormatVectorValue(vp.Value, vect.VectorType);
                        }
                        if (monthsApart >= 120 && mp.Teny == null)
                        {
                            mp.Teny = FormatVectorValue(vp.Value, vect.VectorType);
                        }
                    }
                    gridData.Add(mp);
                }
                session.Flush();
            }

            if (resetColumns)
            {
                this.InitColumns();
            }
            this.VectorList = gridData;
        }

        private static string FormatVectorValue(double value, VectorType vectorType)
        {
            return vectorType == VectorType.Percentage ? string.Format("{0:P}", value) : string.Format("{0:N}", value);
        }
    }
}