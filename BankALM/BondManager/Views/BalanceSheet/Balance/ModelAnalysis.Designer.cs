using System.Windows.Forms;
using ALMSCommon.Controls;
using ALMSCommon.Controls.TreeGridView;
using BondManager.Resources.Language;

namespace BondManager.Views.BalanceSheet.Balance
{
    partial class ModelAnalysis
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bottomPanel = new System.Windows.Forms.Panel();
            this.closeButton = new System.Windows.Forms.Button();
            this.topPanel = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.scenarioComboBox = new System.Windows.Forms.ComboBox();
            this.cenetrSplitContainer = new System.Windows.Forms.SplitContainer();
            this.grid = new ALMSCommon.Controls.DataGridViewEx();
            this.graphSplitContainer = new System.Windows.Forms.SplitContainer();
            this.cfZedGraphControl = new ZedGraph.ZedGraphControl();
            this.cumulativeZedGraphControl = new ZedGraph.ZedGraphControl();
            this.bottomPanel.SuspendLayout();
            this.topPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cenetrSplitContainer)).BeginInit();
            this.cenetrSplitContainer.Panel1.SuspendLayout();
            this.cenetrSplitContainer.Panel2.SuspendLayout();
            this.cenetrSplitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.graphSplitContainer)).BeginInit();
            this.graphSplitContainer.Panel1.SuspendLayout();
            this.graphSplitContainer.Panel2.SuspendLayout();
            this.graphSplitContainer.SuspendLayout();
            this.SuspendLayout();
            // 
            // bottomPanel
            // 
            this.bottomPanel.Controls.Add(this.closeButton);
            this.bottomPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bottomPanel.Location = new System.Drawing.Point(0, 566);
            this.bottomPanel.Name = "bottomPanel";
            this.bottomPanel.Size = new System.Drawing.Size(909, 55);
            this.bottomPanel.TabIndex = 0;
            // 
            // closeButton
            // 
            this.closeButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.closeButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.closeButton.Location = new System.Drawing.Point(783, 11);
            this.closeButton.Name = "closeButton";
            this.closeButton.Padding = new System.Windows.Forms.Padding(5);
            this.closeButton.Size = new System.Drawing.Size(114, 32);
            this.closeButton.TabIndex = 2;
            this.closeButton.Text = global::BondManager.Resources.Language.Labels.ModelAnalysis_InitializeComponent_Close;
            this.closeButton.UseVisualStyleBackColor = true;
            this.closeButton.Click += new System.EventHandler(this.closeButton_Click);
            // 
            // topPanel
            // 
            this.topPanel.Controls.Add(this.label1);
            this.topPanel.Controls.Add(this.scenarioComboBox);
            this.topPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.topPanel.Location = new System.Drawing.Point(0, 0);
            this.topPanel.Name = "topPanel";
            this.topPanel.Size = new System.Drawing.Size(909, 33);
            this.topPanel.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(721, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Scenario";
            // 
            // scenarioComboBox
            // 
            this.scenarioComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.scenarioComboBox.FormattingEnabled = true;
            this.scenarioComboBox.Location = new System.Drawing.Point(776, 6);
            this.scenarioComboBox.Name = "scenarioComboBox";
            this.scenarioComboBox.Size = new System.Drawing.Size(121, 21);
            this.scenarioComboBox.TabIndex = 0;
            // 
            // cenetrSplitContainer
            // 
            this.cenetrSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cenetrSplitContainer.Location = new System.Drawing.Point(0, 33);
            this.cenetrSplitContainer.Name = "cenetrSplitContainer";
            this.cenetrSplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // cenetrSplitContainer.Panel1
            // 
            this.cenetrSplitContainer.Panel1.Controls.Add(this.grid);
            // 
            // cenetrSplitContainer.Panel2
            // 
            this.cenetrSplitContainer.Panel2.Controls.Add(this.graphSplitContainer);
            this.cenetrSplitContainer.Size = new System.Drawing.Size(909, 533);
            this.cenetrSplitContainer.SplitterDistance = 281;
            this.cenetrSplitContainer.TabIndex = 2;
            // 
            // grid
            // 
            this.grid.AllowUserToAddRows = false;
            this.grid.AllowUserToDeleteRows = false;
            this.grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grid.Location = new System.Drawing.Point(0, 0);
            this.grid.Name = "grid";
            this.grid.ReadOnly = true;
            this.grid.Size = new System.Drawing.Size(Screen.PrimaryScreen.Bounds.Width, 281);
            this.grid.TabIndex = 0;
            // 
            // graphSplitContainer
            // 
            this.graphSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.graphSplitContainer.Location = new System.Drawing.Point(0, 0);
            this.graphSplitContainer.Name = "graphSplitContainer";
            // 
            // graphSplitContainer.Panel1
            // 
            this.graphSplitContainer.Panel1.Controls.Add(this.cfZedGraphControl);
            // 
            // graphSplitContainer.Panel2
            // 
            this.graphSplitContainer.Panel2.Controls.Add(this.cumulativeZedGraphControl);
            this.graphSplitContainer.Size = new System.Drawing.Size(909, 248);
            this.graphSplitContainer.SplitterDistance = 452;
            this.graphSplitContainer.TabIndex = 0;
            // 
            // cfZedGraphControl
            // 
            this.cfZedGraphControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cfZedGraphControl.EditButtons = System.Windows.Forms.MouseButtons.Left;
            this.cfZedGraphControl.EditModifierKeys = System.Windows.Forms.Keys.None;
            this.cfZedGraphControl.IsEnableVEdit = true;
            this.cfZedGraphControl.Location = new System.Drawing.Point(0, 0);
            this.cfZedGraphControl.Name = "cfZedGraphControl";
            this.cfZedGraphControl.ScrollGrace = 0D;
            this.cfZedGraphControl.ScrollMaxX = 0D;
            this.cfZedGraphControl.ScrollMaxY = 0D;
            this.cfZedGraphControl.ScrollMaxY2 = 0D;
            this.cfZedGraphControl.ScrollMinX = 0D;
            this.cfZedGraphControl.ScrollMinY = 0D;
            this.cfZedGraphControl.ScrollMinY2 = 0D;
            this.cfZedGraphControl.Size = new System.Drawing.Size(452, 248);
            this.cfZedGraphControl.TabIndex = 2;
            this.cfZedGraphControl.ZoomButtons = System.Windows.Forms.MouseButtons.None;
            // 
            // cumulativeZedGraphControl
            // 
            this.cumulativeZedGraphControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cumulativeZedGraphControl.EditButtons = System.Windows.Forms.MouseButtons.Left;
            this.cumulativeZedGraphControl.EditModifierKeys = System.Windows.Forms.Keys.None;
            this.cumulativeZedGraphControl.IsEnableVEdit = true;
            this.cumulativeZedGraphControl.Location = new System.Drawing.Point(0, 0);
            this.cumulativeZedGraphControl.Name = "cumulativeZedGraphControl";
            this.cumulativeZedGraphControl.ScrollGrace = 0D;
            this.cumulativeZedGraphControl.ScrollMaxX = 0D;
            this.cumulativeZedGraphControl.ScrollMaxY = 0D;
            this.cumulativeZedGraphControl.ScrollMaxY2 = 0D;
            this.cumulativeZedGraphControl.ScrollMinX = 0D;
            this.cumulativeZedGraphControl.ScrollMinY = 0D;
            this.cumulativeZedGraphControl.ScrollMinY2 = 0D;
            this.cumulativeZedGraphControl.Size = new System.Drawing.Size(453, 248);
            this.cumulativeZedGraphControl.TabIndex = 2;
            this.cumulativeZedGraphControl.ZoomButtons = System.Windows.Forms.MouseButtons.None;
            // 
            // ModelAnalysis
            // 
            this.AcceptButton = this.closeButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.closeButton;
            this.ClientSize = new System.Drawing.Size(909, 621);
            this.Controls.Add(this.cenetrSplitContainer);
            this.Controls.Add(this.topPanel);
            this.Controls.Add(this.bottomPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
            this.MinimizeBox = false;
            this.Name = "ModelAnalysis";
            this.Text = "Model Analysis";
            this.Shown += new System.EventHandler(this.ModelAnalysis_Shown);
            this.bottomPanel.ResumeLayout(false);
            this.topPanel.ResumeLayout(false);
            this.topPanel.PerformLayout();
            this.cenetrSplitContainer.Panel1.ResumeLayout(false);
            this.cenetrSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cenetrSplitContainer)).EndInit();
            this.cenetrSplitContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grid)).EndInit();
            this.graphSplitContainer.Panel1.ResumeLayout(false);
            this.graphSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.graphSplitContainer)).EndInit();
            this.graphSplitContainer.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel bottomPanel;
        private System.Windows.Forms.Panel topPanel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox scenarioComboBox;
        private System.Windows.Forms.SplitContainer cenetrSplitContainer;
        private DataGridViewEx grid;
        private System.Windows.Forms.SplitContainer graphSplitContainer;
        private ZedGraph.ZedGraphControl cfZedGraphControl;
        private ZedGraph.ZedGraphControl cumulativeZedGraphControl;
        private System.Windows.Forms.Button closeButton;
    }
}