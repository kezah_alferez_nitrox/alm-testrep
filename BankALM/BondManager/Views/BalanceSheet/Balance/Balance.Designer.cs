﻿using ALMSCommon.Controls;
using ALMSCommon.Controls.TreeGridView;
using BondManager.Resources.Language;

namespace BondManager.Views.BalanceSheet.Balance
{
    partial class Balance
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.treeViewContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.addASubcategoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.renameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsTemplateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importCategoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportCategoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.balanceSheetSplitContainer = new System.Windows.Forms.SplitContainer();
            this.treeView = new BondManager.Controls.CategoryTreeView();
            this._pageNavigatorUp = new BondManager.Views.BalanceSheet.Balance.PageNavigator();
            this._pageNavigatorDown = new BondManager.Views.BalanceSheet.Balance.PageNavigator();
            this.detailsGrid = new ALMSCommon.Controls.DataGridViewEx();
            this.summaryGrid = new ALMSCommon.Controls.DataGridViewEx();
            this.summaryContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toogleBookValueColumnShowHideToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toggleInputColumnShowHideToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toogleAnalysisCategoryColumnShowHideToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.balanceHeaderPanel = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.comboPages = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.categoryExcelImport = new System.Windows.Forms.Button();
            this.scenarioComboBox = new System.Windows.Forms.ComboBox();
            this.categoryExcelExport = new System.Windows.Forms.Button();
            this.excelImportButton = new System.Windows.Forms.Button();
            this.excelPasteButton = new System.Windows.Forms.Button();
            this.excelExportButton = new System.Windows.Forms.Button();
            this.upButton = new System.Windows.Forms.Button();
            this.addNewButton = new System.Windows.Forms.Button();
            this.selectedNodeLabel = new System.Windows.Forms.Label();
            this.importBttn = new System.Windows.Forms.Button();
            this.detailsGridContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.modelAnalysisToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripSeparator();
            this.recalculateBookValueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.sortAscendingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sortDescendingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.applyValueToEntireColumnToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.applyValueToRowsAboveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.applyValueToRowsBelowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.applyValueToXRowsBelowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.applyValueToSelectedRowsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.moveUptoolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.moveDowntoolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.excludeSelectedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.excludeAllBelowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.includeSelectedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.includeAllBelowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
            this.copyColumnToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pasteColumnToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.detailsGridRowHeaderContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.modelAnalysisLineStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteRowsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copyDerivativeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pasteLinesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.linkProductsAsSwapToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.treeViewContextMenuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.balanceSheetSplitContainer)).BeginInit();
            this.balanceSheetSplitContainer.Panel1.SuspendLayout();
            this.balanceSheetSplitContainer.Panel2.SuspendLayout();
            this.balanceSheetSplitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.detailsGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.summaryGrid)).BeginInit();
            this.summaryContextMenuStrip.SuspendLayout();
            this.balanceHeaderPanel.SuspendLayout();
            this.detailsGridContextMenuStrip.SuspendLayout();
            this.detailsGridRowHeaderContextMenuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // treeViewContextMenuStrip
            // 
            this.treeViewContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addASubcategoryToolStripMenuItem,
            this.renameToolStripMenuItem,
            this.deleteToolStripMenuItem,
            this.saveAsTemplateToolStripMenuItem,
            this.importCategoryToolStripMenuItem,
            this.exportCategoryToolStripMenuItem});
            this.treeViewContextMenuStrip.Name = "treeViewContextMenuStrip";
            this.treeViewContextMenuStrip.Size = new System.Drawing.Size(174, 136);
            this.treeViewContextMenuStrip.Opening += new System.ComponentModel.CancelEventHandler(this.treeViewContextMenuStrip_Opening);
            // 
            // addASubcategoryToolStripMenuItem
            // 
            this.addASubcategoryToolStripMenuItem.Name = "addASubcategoryToolStripMenuItem";
            this.addASubcategoryToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.addASubcategoryToolStripMenuItem.Text = global::BondManager.Resources.Language.Labels.Balance_InitializeComponent_Add_a_subcategory;
            this.addASubcategoryToolStripMenuItem.Click += new System.EventHandler(this.addASubcategoryToolStripMenuItem_Click);
            // 
            // renameToolStripMenuItem
            // 
            this.renameToolStripMenuItem.Name = "renameToolStripMenuItem";
            this.renameToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F2;
            this.renameToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.renameToolStripMenuItem.Text = global::BondManager.Resources.Language.Labels.Balance_InitializeComponent_Rename;
            this.renameToolStripMenuItem.Click += new System.EventHandler(this.renameToolStripMenuItem_Click);
            // 
            // deleteToolStripMenuItem
            // 
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Delete)));
            this.deleteToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.deleteToolStripMenuItem.Text = global::BondManager.Resources.Language.Labels.Balance_InitializeComponent_Delete;
            this.deleteToolStripMenuItem.Click += new System.EventHandler(this.deleteToolStripMenuItem_Click);
            // 
            // saveAsTemplateToolStripMenuItem
            // 
            this.saveAsTemplateToolStripMenuItem.Name = "saveAsTemplateToolStripMenuItem";
            this.saveAsTemplateToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.saveAsTemplateToolStripMenuItem.Text = global::BondManager.Resources.Language.Labels.Balance_InitializeComponent_Save_as_template;
            this.saveAsTemplateToolStripMenuItem.Click += new System.EventHandler(this.SaveAsTemplate);
            // 
            // importCategoryToolStripMenuItem
            // 
            this.importCategoryToolStripMenuItem.Name = "importCategoryToolStripMenuItem";
            this.importCategoryToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.importCategoryToolStripMenuItem.Text = "Import category";
            this.importCategoryToolStripMenuItem.Click += new System.EventHandler(this.ImportCategory);
            // 
            // exportCategoryToolStripMenuItem
            // 
            this.exportCategoryToolStripMenuItem.Name = "exportCategoryToolStripMenuItem";
            this.exportCategoryToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.exportCategoryToolStripMenuItem.Text = "Export category";
            this.exportCategoryToolStripMenuItem.Click += new System.EventHandler(this.ExportCategory);
            // 
            // balanceSheetSplitContainer
            // 
            this.balanceSheetSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.balanceSheetSplitContainer.Location = new System.Drawing.Point(0, 0);
            this.balanceSheetSplitContainer.Name = "balanceSheetSplitContainer";
            // 
            // balanceSheetSplitContainer.Panel1
            // 
            this.balanceSheetSplitContainer.Panel1.Controls.Add(this.treeView);
            this.balanceSheetSplitContainer.Panel1MinSize = 120;
            // 
            // balanceSheetSplitContainer.Panel2
            // 
            this.balanceSheetSplitContainer.Panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.balanceSheetSplitContainer.Panel2.Controls.Add(this.dataGridView1);
            this.balanceSheetSplitContainer.Panel2.Controls.Add(this._pageNavigatorUp);
            this.balanceSheetSplitContainer.Panel2.Controls.Add(this._pageNavigatorDown);
            this.balanceSheetSplitContainer.Panel2.Controls.Add(this.detailsGrid);
            this.balanceSheetSplitContainer.Panel2.Controls.Add(this.summaryGrid);
            this.balanceSheetSplitContainer.Panel2.Controls.Add(this.balanceHeaderPanel);
            this.balanceSheetSplitContainer.Size = new System.Drawing.Size(1236, 497);
            this.balanceSheetSplitContainer.SplitterDistance = 200;
            this.balanceSheetSplitContainer.TabIndex = 3;
            // 
            // treeView
            // 
            this.treeView.AllowDrop = true;
            this.treeView.CanOrganize = true;
            this.treeView.ContextMenuStrip = this.treeViewContextMenuStrip;
            this.treeView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeView.HideSelection = false;
            this.treeView.Location = new System.Drawing.Point(0, 0);
            this.treeView.Name = "treeView";
            this.treeView.Size = new System.Drawing.Size(200, 497);
            this.treeView.TabIndex = 0;
            this.treeView.DrawNode += new System.Windows.Forms.DrawTreeNodeEventHandler(this.OnDrawNode);
            this.treeView.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView_AfterSelect);
            // 
            // _pageNavigatorUp
            // 
            this._pageNavigatorUp.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this._pageNavigatorUp.Location = new System.Drawing.Point(0, 34);
            this._pageNavigatorUp.MaxPage = 0;
            this._pageNavigatorUp.Name = "_pageNavigatorUp";
            this._pageNavigatorUp.PageIndex = 0;
            this._pageNavigatorUp.Size = new System.Drawing.Size(1032, 29);
            this._pageNavigatorUp.TabIndex = 3;
            // 
            // _pageNavigatorDown
            // 
            this._pageNavigatorDown.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this._pageNavigatorDown.Location = new System.Drawing.Point(0, 468);
            this._pageNavigatorDown.MaxPage = 0;
            this._pageNavigatorDown.Name = "_pageNavigatorDown";
            this._pageNavigatorDown.PageIndex = 0;
            this._pageNavigatorDown.Size = new System.Drawing.Size(1032, 29);
            this._pageNavigatorDown.TabIndex = 2;
            // 
            // detailsGrid
            // 
            this.detailsGrid.AllowUserToAddRows = false;
            this.detailsGrid.AllowUserToDeleteRows = false;
            this.detailsGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.detailsGrid.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.detailsGrid.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.detailsGrid.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
            this.detailsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.detailsGrid.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.detailsGrid.Location = new System.Drawing.Point(8, 57);
            this.detailsGrid.Name = "detailsGrid";
            this.detailsGrid.RowTemplate.Height = 24;
            this.detailsGrid.Size = new System.Drawing.Size(1032, 405);
            this.detailsGrid.TabIndex = 1;
            this.detailsGrid.Visible = false;
            this.detailsGrid.CellContextMenuStripNeeded += new System.Windows.Forms.DataGridViewCellContextMenuStripNeededEventHandler(this.detailsGrid_CellContextMenuStripNeeded);
            // 
            // summaryGrid
            // 
            this.summaryGrid.AllowUserToAddRows = false;
            this.summaryGrid.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.summaryGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.summaryGrid.ContextMenuStrip = this.summaryContextMenuStrip;
            this.summaryGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.summaryGrid.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.summaryGrid.Location = new System.Drawing.Point(0, 34);
            this.summaryGrid.Name = "summaryGrid";
            this.summaryGrid.Size = new System.Drawing.Size(1032, 463);
            this.summaryGrid.TabIndex = 1;
            this.summaryGrid.Visible = false;
            // 
            // summaryContextMenuStrip
            // 
            this.summaryContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toogleBookValueColumnShowHideToolStripMenuItem,
            this.toggleInputColumnShowHideToolStripMenuItem,
            this.toogleAnalysisCategoryColumnShowHideToolStripMenuItem});
            this.summaryContextMenuStrip.Name = "summaryContextMenuStrip";
            this.summaryContextMenuStrip.Size = new System.Drawing.Size(325, 70);
            // 
            // toogleBookValueColumnShowHideToolStripMenuItem
            // 
            this.toogleBookValueColumnShowHideToolStripMenuItem.Name = "toogleBookValueColumnShowHideToolStripMenuItem";
            this.toogleBookValueColumnShowHideToolStripMenuItem.Size = new System.Drawing.Size(324, 22);
            this.toogleBookValueColumnShowHideToolStripMenuItem.Text = "Toggle Book Value Column (Show/Hide)";
            this.toogleBookValueColumnShowHideToolStripMenuItem.Click += new System.EventHandler(this.toogleBookValueColumnShowHideToolStripMenuItem_Click);
            // 
            // toggleInputColumnShowHideToolStripMenuItem
            // 
            this.toggleInputColumnShowHideToolStripMenuItem.Name = "toggleInputColumnShowHideToolStripMenuItem";
            this.toggleInputColumnShowHideToolStripMenuItem.Size = new System.Drawing.Size(324, 22);
            this.toggleInputColumnShowHideToolStripMenuItem.Text = "Toggle Input Column (Show/Hide)";
            this.toggleInputColumnShowHideToolStripMenuItem.Click += new System.EventHandler(this.toggleInputColumnShowHideToolStripMenuItem_Click);
            // 
            // toogleAnalysisCategoryColumnShowHideToolStripMenuItem
            // 
            this.toogleAnalysisCategoryColumnShowHideToolStripMenuItem.Name = "toogleAnalysisCategoryColumnShowHideToolStripMenuItem";
            this.toogleAnalysisCategoryColumnShowHideToolStripMenuItem.Size = new System.Drawing.Size(324, 22);
            this.toogleAnalysisCategoryColumnShowHideToolStripMenuItem.Text = "Toogle Analysis Category Column (Show/Hide)";
            this.toogleAnalysisCategoryColumnShowHideToolStripMenuItem.Click += new System.EventHandler(this.toogleAnalysisCategoryColumnShowHideToolStripMenuItem_Click);
            // 
            // balanceHeaderPanel
            // 
            this.balanceHeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(160)))), ((int)(((byte)(160)))), ((int)(((byte)(160)))));
            this.balanceHeaderPanel.Controls.Add(this.label1);
            this.balanceHeaderPanel.Controls.Add(this.comboPages);
            this.balanceHeaderPanel.Controls.Add(this.label8);
            this.balanceHeaderPanel.Controls.Add(this.categoryExcelImport);
            this.balanceHeaderPanel.Controls.Add(this.scenarioComboBox);
            this.balanceHeaderPanel.Controls.Add(this.categoryExcelExport);
            this.balanceHeaderPanel.Controls.Add(this.excelImportButton);
            this.balanceHeaderPanel.Controls.Add(this.excelPasteButton);
            this.balanceHeaderPanel.Controls.Add(this.excelExportButton);
            this.balanceHeaderPanel.Controls.Add(this.upButton);
            this.balanceHeaderPanel.Controls.Add(this.addNewButton);
            this.balanceHeaderPanel.Controls.Add(this.selectedNodeLabel);
            this.balanceHeaderPanel.Controls.Add(this.importBttn);
            this.balanceHeaderPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.balanceHeaderPanel.Location = new System.Drawing.Point(0, 0);
            this.balanceHeaderPanel.Name = "balanceHeaderPanel";
            this.balanceHeaderPanel.Size = new System.Drawing.Size(1032, 34);
            this.balanceHeaderPanel.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(50, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "Show";
            // 
            // comboPages
            // 
            this.comboPages.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.comboPages.DisplayMember = "Name";
            this.comboPages.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboPages.FormattingEnabled = true;
            this.comboPages.Location = new System.Drawing.Point(0, 0);
            this.comboPages.Name = "comboPages";
            this.comboPages.Size = new System.Drawing.Size(45, 21);
            this.comboPages.TabIndex = 10;
            this.comboPages.ValueMember = "Id";
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(0, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(49, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "Scenario";
            // 
            // categoryExcelImport
            // 
            this.categoryExcelImport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.categoryExcelImport.BackColor = System.Drawing.SystemColors.Control;
            this.categoryExcelImport.Location = new System.Drawing.Point(0, 0);
            this.categoryExcelImport.Name = "categoryExcelImport";
            this.categoryExcelImport.Size = new System.Drawing.Size(100, 23);
            this.categoryExcelImport.TabIndex = 9;
            this.categoryExcelImport.Text = global::BondManager.Resources.Language.Labels.Balance_InitializeComponent_Category_Excel_Import;
            this.categoryExcelImport.UseVisualStyleBackColor = true;
            this.categoryExcelImport.Click += new System.EventHandler(this.ImportCategory);
            // 
            // scenarioComboBox
            // 
            this.scenarioComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.scenarioComboBox.DisplayMember = "Name";
            this.scenarioComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.scenarioComboBox.FormattingEnabled = true;
            this.scenarioComboBox.Location = new System.Drawing.Point(0, 1);
            this.scenarioComboBox.Name = "scenarioComboBox";
            this.scenarioComboBox.Size = new System.Drawing.Size(89, 21);
            this.scenarioComboBox.TabIndex = 6;
            this.scenarioComboBox.ValueMember = "Id";
            this.scenarioComboBox.SelectedIndexChanged += new System.EventHandler(this.SelectScenario);
            // 
            // categoryExcelExport
            // 
            this.categoryExcelExport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.categoryExcelExport.BackColor = System.Drawing.SystemColors.Control;
            this.categoryExcelExport.Location = new System.Drawing.Point(0, 0);
            this.categoryExcelExport.Name = "categoryExcelExport";
            this.categoryExcelExport.Size = new System.Drawing.Size(100, 23);
            this.categoryExcelExport.TabIndex = 8;
            this.categoryExcelExport.Text = global::BondManager.Resources.Language.Labels.Balance_InitializeComponent_Category_Excel_Export;
            this.categoryExcelExport.UseVisualStyleBackColor = true;
            this.categoryExcelExport.Click += new System.EventHandler(this.ExportCategory);
            // 
            // excelImportButton
            // 
            this.excelImportButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.excelImportButton.BackColor = System.Drawing.SystemColors.Control;
            this.excelImportButton.Location = new System.Drawing.Point(0, 1);
            this.excelImportButton.Name = "excelImportButton";
            this.excelImportButton.Size = new System.Drawing.Size(100, 23);
            this.excelImportButton.TabIndex = 7;
            this.excelImportButton.Text = global::BondManager.Resources.Language.Labels.Balance_InitializeComponent_Global_Excel_Import;
            this.excelImportButton.UseVisualStyleBackColor = true;
            this.excelImportButton.Click += new System.EventHandler(this.excelImportButton_Click);
            // 
            // excelPasteButton
            // 
            this.excelPasteButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.excelPasteButton.BackColor = System.Drawing.SystemColors.Control;
            this.excelPasteButton.Location = new System.Drawing.Point(0, 0);
            this.excelPasteButton.Name = "excelPasteButton";
            this.excelPasteButton.Size = new System.Drawing.Size(100, 23);
            this.excelPasteButton.TabIndex = 7;
            this.excelPasteButton.Text = "Global Paste";
            this.excelPasteButton.UseVisualStyleBackColor = true;
            this.excelPasteButton.Click += new System.EventHandler(this.excelPasteButton_Click);
            // 
            // excelExportButton
            // 
            this.excelExportButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.excelExportButton.BackColor = System.Drawing.SystemColors.Control;
            this.excelExportButton.Location = new System.Drawing.Point(0, 0);
            this.excelExportButton.Name = "excelExportButton";
            this.excelExportButton.Size = new System.Drawing.Size(100, 23);
            this.excelExportButton.TabIndex = 6;
            this.excelExportButton.Text = global::BondManager.Resources.Language.Labels.Balance_InitializeComponent_Global_Excel_Export;
            this.excelExportButton.UseVisualStyleBackColor = true;
            this.excelExportButton.Click += new System.EventHandler(this.excelExportButton_Click);
            // 
            // upButton
            // 
            this.upButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.upButton.BackColor = System.Drawing.SystemColors.Control;
            this.upButton.Location = new System.Drawing.Point(0, 1);
            this.upButton.Name = "upButton";
            this.upButton.Size = new System.Drawing.Size(75, 23);
            this.upButton.TabIndex = 5;
            this.upButton.Text = global::BondManager.Resources.Language.Labels.Balance_InitializeComponent_Up;
            this.upButton.UseVisualStyleBackColor = true;
            this.upButton.Click += new System.EventHandler(this.upButton_Click);
            // 
            // addNewButton
            // 
            this.addNewButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.addNewButton.BackColor = System.Drawing.SystemColors.Control;
            this.addNewButton.Enabled = false;
            this.addNewButton.Location = new System.Drawing.Point(0, 1);
            this.addNewButton.Name = "addNewButton";
            this.addNewButton.Size = new System.Drawing.Size(75, 23);
            this.addNewButton.TabIndex = 4;
            this.addNewButton.Text = global::BondManager.Resources.Language.Labels.Balance_InitializeComponent_Add_New;
            this.addNewButton.UseVisualStyleBackColor = true;
            this.addNewButton.Click += new System.EventHandler(this.addNewButton_Click);
            // 
            // selectedNodeLabel
            // 
            this.selectedNodeLabel.AutoSize = true;
            this.selectedNodeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.selectedNodeLabel.ForeColor = System.Drawing.Color.White;
            this.selectedNodeLabel.Location = new System.Drawing.Point(4, 4);
            this.selectedNodeLabel.Name = "selectedNodeLabel";
            this.selectedNodeLabel.Size = new System.Drawing.Size(0, 24);
            this.selectedNodeLabel.TabIndex = 0;
            // 
            // importBttn
            // 
            this.importBttn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.importBttn.BackColor = System.Drawing.SystemColors.Control;
            this.importBttn.Location = new System.Drawing.Point(0, 1);
            this.importBttn.Name = "importBttn";
            this.importBttn.Size = new System.Drawing.Size(75, 23);
            this.importBttn.TabIndex = 4;
            this.importBttn.Text = "Import";
            this.importBttn.UseVisualStyleBackColor = true;
            this.importBttn.Click += new System.EventHandler(this.importBttn_Click);
            // 
            // detailsGridContextMenuStrip
            // 
            this.detailsGridContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.modelAnalysisToolStripMenuItem,
            this.toolStripMenuItem4,
            this.recalculateBookValueToolStripMenuItem,
            this.toolStripSeparator1,
            this.sortAscendingToolStripMenuItem,
            this.sortDescendingToolStripMenuItem,
            this.toolStripSeparator2,
            this.applyValueToEntireColumnToolStripMenuItem,
            this.applyValueToRowsAboveToolStripMenuItem,
            this.applyValueToRowsBelowToolStripMenuItem,
            this.applyValueToXRowsBelowToolStripMenuItem,
            this.applyValueToSelectedRowsToolStripMenuItem,
            this.toolStripSeparator3,
            this.moveUptoolStripMenuItem,
            this.moveDowntoolStripMenuItem,
            this.toolStripMenuItem1,
            this.excludeSelectedToolStripMenuItem,
            this.excludeAllBelowToolStripMenuItem,
            this.toolStripMenuItem2,
            this.includeSelectedToolStripMenuItem,
            this.includeAllBelowToolStripMenuItem,
            this.toolStripMenuItem3,
            this.copyColumnToolStripMenuItem,
            this.pasteColumnToolStripMenuItem});
            this.detailsGridContextMenuStrip.Name = "detailsGridContextMenuStrip";
            this.detailsGridContextMenuStrip.Size = new System.Drawing.Size(322, 420);
            this.detailsGridContextMenuStrip.Opening += new System.ComponentModel.CancelEventHandler(this.detailsGridContextMenuStrip_Opening);
            // 
            // modelAnalysisToolStripMenuItem
            // 
            this.modelAnalysisToolStripMenuItem.Name = "modelAnalysisToolStripMenuItem";
            this.modelAnalysisToolStripMenuItem.Size = new System.Drawing.Size(321, 22);
            this.modelAnalysisToolStripMenuItem.Text = global::BondManager.Resources.Language.Labels.Balance_InitializeComponent_Model_Analysis;
            this.modelAnalysisToolStripMenuItem.Click += new System.EventHandler(this.modelAnalysisToolStripMenuItem_Click);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(318, 6);
            // 
            // recalculateBookValueToolStripMenuItem
            // 
            this.recalculateBookValueToolStripMenuItem.Name = "recalculateBookValueToolStripMenuItem";
            this.recalculateBookValueToolStripMenuItem.Size = new System.Drawing.Size(321, 22);
            this.recalculateBookValueToolStripMenuItem.Text = "Recalculate Book Value";
            this.recalculateBookValueToolStripMenuItem.Click += new System.EventHandler(this.recalculateBookValueToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(318, 6);
            // 
            // sortAscendingToolStripMenuItem
            // 
            this.sortAscendingToolStripMenuItem.Name = "sortAscendingToolStripMenuItem";
            this.sortAscendingToolStripMenuItem.Size = new System.Drawing.Size(321, 22);
            this.sortAscendingToolStripMenuItem.Text = global::BondManager.Resources.Language.Labels.Balance_InitializeComponent_Sort_Ascending;
            this.sortAscendingToolStripMenuItem.Click += new System.EventHandler(this.sortAscendingToolStripMenuItem_Click);
            // 
            // sortDescendingToolStripMenuItem
            // 
            this.sortDescendingToolStripMenuItem.Name = "sortDescendingToolStripMenuItem";
            this.sortDescendingToolStripMenuItem.Size = new System.Drawing.Size(321, 22);
            this.sortDescendingToolStripMenuItem.Text = global::BondManager.Resources.Language.Labels.Balance_InitializeComponent_Sort_Descending;
            this.sortDescendingToolStripMenuItem.Click += new System.EventHandler(this.sortDescendingToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(318, 6);
            // 
            // applyValueToEntireColumnToolStripMenuItem
            // 
            this.applyValueToEntireColumnToolStripMenuItem.Name = "applyValueToEntireColumnToolStripMenuItem";
            this.applyValueToEntireColumnToolStripMenuItem.Size = new System.Drawing.Size(321, 22);
            this.applyValueToEntireColumnToolStripMenuItem.Text = global::BondManager.Resources.Language.Labels.Balance_InitializeComponent_Apply_Value_to_Entire_Column;
            this.applyValueToEntireColumnToolStripMenuItem.Click += new System.EventHandler(this.applyValueToEntireColumnToolStripMenuItem_Click);
            // 
            // applyValueToRowsAboveToolStripMenuItem
            // 
            this.applyValueToRowsAboveToolStripMenuItem.Name = "applyValueToRowsAboveToolStripMenuItem";
            this.applyValueToRowsAboveToolStripMenuItem.Size = new System.Drawing.Size(321, 22);
            this.applyValueToRowsAboveToolStripMenuItem.Text = global::BondManager.Resources.Language.Labels.Balance_InitializeComponent_Apply_Value_to_Rows_Above;
            this.applyValueToRowsAboveToolStripMenuItem.Click += new System.EventHandler(this.applyValueToRowsAboveToolStripMenuItem_Click);
            // 
            // applyValueToRowsBelowToolStripMenuItem
            // 
            this.applyValueToRowsBelowToolStripMenuItem.Name = "applyValueToRowsBelowToolStripMenuItem";
            this.applyValueToRowsBelowToolStripMenuItem.Size = new System.Drawing.Size(321, 22);
            this.applyValueToRowsBelowToolStripMenuItem.Text = global::BondManager.Resources.Language.Labels.Balance_InitializeComponent_Apply_Value_to_Rows_Below;
            this.applyValueToRowsBelowToolStripMenuItem.Click += new System.EventHandler(this.applyValueToRowsBelowToolStripMenuItem_Click);
            // 
            // applyValueToXRowsBelowToolStripMenuItem
            // 
            this.applyValueToXRowsBelowToolStripMenuItem.Name = "applyValueToXRowsBelowToolStripMenuItem";
            this.applyValueToXRowsBelowToolStripMenuItem.Size = new System.Drawing.Size(321, 22);
            this.applyValueToXRowsBelowToolStripMenuItem.Text = "Apply Value to Limited Number of Rows Below";
            this.applyValueToXRowsBelowToolStripMenuItem.Click += new System.EventHandler(this.applyValueToXRowsBelowToolStripMenuItem_Click);
            // 
            // applyValueToSelectedRowsToolStripMenuItem
            // 
            this.applyValueToSelectedRowsToolStripMenuItem.Name = "applyValueToSelectedRowsToolStripMenuItem";
            this.applyValueToSelectedRowsToolStripMenuItem.Size = new System.Drawing.Size(321, 22);
            this.applyValueToSelectedRowsToolStripMenuItem.Text = global::BondManager.Resources.Language.Labels.Balance_InitializeComponent_Apply_Value_to_Selected_Rows;
            this.applyValueToSelectedRowsToolStripMenuItem.Click += new System.EventHandler(this.applyValueToSelectedRowsToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(318, 6);
            // 
            // moveUptoolStripMenuItem
            // 
            this.moveUptoolStripMenuItem.Name = "moveUptoolStripMenuItem";
            this.moveUptoolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.Up)));
            this.moveUptoolStripMenuItem.Size = new System.Drawing.Size(321, 22);
            this.moveUptoolStripMenuItem.Text = global::BondManager.Resources.Language.Labels.Balance_InitializeComponent_Move_up;
            this.moveUptoolStripMenuItem.Click += new System.EventHandler(this.MoveItem);
            // 
            // moveDowntoolStripMenuItem
            // 
            this.moveDowntoolStripMenuItem.Name = "moveDowntoolStripMenuItem";
            this.moveDowntoolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.Down)));
            this.moveDowntoolStripMenuItem.Size = new System.Drawing.Size(321, 22);
            this.moveDowntoolStripMenuItem.Text = global::BondManager.Resources.Language.Labels.Balance_InitializeComponent_Move_down;
            this.moveDowntoolStripMenuItem.Click += new System.EventHandler(this.MoveItem);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(318, 6);
            // 
            // excludeSelectedToolStripMenuItem
            // 
            this.excludeSelectedToolStripMenuItem.Name = "excludeSelectedToolStripMenuItem";
            this.excludeSelectedToolStripMenuItem.Size = new System.Drawing.Size(321, 22);
            this.excludeSelectedToolStripMenuItem.Text = global::BondManager.Resources.Language.Labels.Balance_InitializeComponent_E_xclude_All_Above;
            this.excludeSelectedToolStripMenuItem.Click += new System.EventHandler(this.excludeSelectedToolStripMenuItem_Click);
            // 
            // excludeAllBelowToolStripMenuItem
            // 
            this.excludeAllBelowToolStripMenuItem.Name = "excludeAllBelowToolStripMenuItem";
            this.excludeAllBelowToolStripMenuItem.Size = new System.Drawing.Size(321, 22);
            this.excludeAllBelowToolStripMenuItem.Text = global::BondManager.Resources.Language.Labels.Balance_InitializeComponent_Exclude_All_Below;
            this.excludeAllBelowToolStripMenuItem.Click += new System.EventHandler(this.excludeAllBelowToolStripMenuItem_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(318, 6);
            // 
            // includeSelectedToolStripMenuItem
            // 
            this.includeSelectedToolStripMenuItem.Name = "includeSelectedToolStripMenuItem";
            this.includeSelectedToolStripMenuItem.Size = new System.Drawing.Size(321, 22);
            this.includeSelectedToolStripMenuItem.Text = global::BondManager.Resources.Language.Labels.Balance_InitializeComponent__Include_All_Above;
            this.includeSelectedToolStripMenuItem.Click += new System.EventHandler(this.includeSelectedToolStripMenuItem_Click);
            // 
            // includeAllBelowToolStripMenuItem
            // 
            this.includeAllBelowToolStripMenuItem.Name = "includeAllBelowToolStripMenuItem";
            this.includeAllBelowToolStripMenuItem.Size = new System.Drawing.Size(321, 22);
            this.includeAllBelowToolStripMenuItem.Text = global::BondManager.Resources.Language.Labels.Balance_InitializeComponent_Include_All_Below;
            this.includeAllBelowToolStripMenuItem.Click += new System.EventHandler(this.includeAllBelowToolStripMenuItem_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(318, 6);
            // 
            // copyColumnToolStripMenuItem
            // 
            this.copyColumnToolStripMenuItem.Name = "copyColumnToolStripMenuItem";
            this.copyColumnToolStripMenuItem.Size = new System.Drawing.Size(321, 22);
            this.copyColumnToolStripMenuItem.Text = global::BondManager.Resources.Language.Labels.Balance_InitializeComponent_Copy_Column;
            this.copyColumnToolStripMenuItem.Click += new System.EventHandler(this.copyColumnToolStripMenuItem_Click);
            // 
            // pasteColumnToolStripMenuItem
            // 
            this.pasteColumnToolStripMenuItem.Name = "pasteColumnToolStripMenuItem";
            this.pasteColumnToolStripMenuItem.Size = new System.Drawing.Size(321, 22);
            this.pasteColumnToolStripMenuItem.Text = global::BondManager.Resources.Language.Labels.Balance_InitializeComponent_Paste__Column;
            this.pasteColumnToolStripMenuItem.Click += new System.EventHandler(this.pasteColumnToolStripMenuItem_Click);
            // 
            // detailsGridRowHeaderContextMenuStrip
            // 
            this.detailsGridRowHeaderContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.modelAnalysisLineStripMenuItem1,
            this.deleteRowsToolStripMenuItem,
            this.copyDerivativeToolStripMenuItem,
            this.cutToolStripMenuItem,
            this.pasteLinesToolStripMenuItem,
            this.linkProductsAsSwapToolStripMenuItem});
            this.detailsGridRowHeaderContextMenuStrip.Name = "detailsGridRowHeaderContextMenuStrip";
            this.detailsGridRowHeaderContextMenuStrip.Size = new System.Drawing.Size(317, 136);
            this.detailsGridRowHeaderContextMenuStrip.Opening += new System.ComponentModel.CancelEventHandler(this.detailsGridRowHeaderContextMenuStrip_Opening);
            // 
            // modelAnalysisLineStripMenuItem1
            // 
            this.modelAnalysisLineStripMenuItem1.Name = "modelAnalysisLineStripMenuItem1";
            this.modelAnalysisLineStripMenuItem1.Size = new System.Drawing.Size(316, 22);
            this.modelAnalysisLineStripMenuItem1.Text = global::BondManager.Resources.Language.Labels.Balance_InitializeComponent_Model_Analysis;
            this.modelAnalysisLineStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // deleteRowsToolStripMenuItem
            // 
            this.deleteRowsToolStripMenuItem.Name = "deleteRowsToolStripMenuItem";
            this.deleteRowsToolStripMenuItem.Size = new System.Drawing.Size(316, 22);
            this.deleteRowsToolStripMenuItem.Text = global::BondManager.Resources.Language.Labels.Balance_InitializeComponent_Delete_Row_s_;
            this.deleteRowsToolStripMenuItem.Click += new System.EventHandler(this.deleteRowsToolStripMenuItem_Click);
            // 
            // copyDerivativeToolStripMenuItem
            // 
            this.copyDerivativeToolStripMenuItem.Name = "copyDerivativeToolStripMenuItem";
            this.copyDerivativeToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this.copyDerivativeToolStripMenuItem.Size = new System.Drawing.Size(316, 22);
            this.copyDerivativeToolStripMenuItem.Text = global::BondManager.Resources.Language.Labels.Balance_InitializeComponent__Copy_Line;
            this.copyDerivativeToolStripMenuItem.Click += new System.EventHandler(this.copyDerivativeToolStripMenuItem_Click);
            // 
            // cutToolStripMenuItem
            // 
            this.cutToolStripMenuItem.Name = "cutToolStripMenuItem";
            this.cutToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X)));
            this.cutToolStripMenuItem.Size = new System.Drawing.Size(316, 22);
            this.cutToolStripMenuItem.Text = "C&ut Line";
            this.cutToolStripMenuItem.Click += new System.EventHandler(this.cutToolStripMenuItem_Click);
            // 
            // pasteLinesToolStripMenuItem
            // 
            this.pasteLinesToolStripMenuItem.Name = "pasteLinesToolStripMenuItem";
            this.pasteLinesToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V)));
            this.pasteLinesToolStripMenuItem.Size = new System.Drawing.Size(316, 22);
            this.pasteLinesToolStripMenuItem.Text = global::BondManager.Resources.Language.Labels.Balance_InitializeComponent__Paste_Line_s_;
            this.pasteLinesToolStripMenuItem.Click += new System.EventHandler(this.pasteLinesToolStripMenuItem_Click);
            // 
            // linkProductsAsSwapToolStripMenuItem
            // 
            this.linkProductsAsSwapToolStripMenuItem.Name = "linkProductsAsSwapToolStripMenuItem";
            this.linkProductsAsSwapToolStripMenuItem.Size = new System.Drawing.Size(316, 22);
            this.linkProductsAsSwapToolStripMenuItem.Text = "&Link Products As Derivative (Swap, Cap, Floor)";
            this.linkProductsAsSwapToolStripMenuItem.Click += new System.EventHandler(this.linkProductsAsSwapToolStripMenuItem_Click);
            // 
            // openFileDialog
            // 
            this.openFileDialog.DefaultExt = "xlsx";
            this.openFileDialog.Filter = "XLSX-Excel files 2007+|*.xlsx|XLS-Excel files 2003|*.xls|All files|*.*";
            this.openFileDialog.Title = global::BondManager.Resources.Language.Labels.Balance_InitializeComponent_Import_Excel_file;
            // 
            // saveFileDialog
            // 
            this.saveFileDialog.DefaultExt = "xlsx";
            this.saveFileDialog.Filter = "XLSX-Excel files 2007+|*.xlsx|XLS-Excel files 2003|*.xls|All files|*.*";
            this.saveFileDialog.Title = global::BondManager.Resources.Language.Labels.Balance_InitializeComponent_Save_Excel_file;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(8, 264);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(1021, 198);
            this.dataGridView1.TabIndex = 4;
            // 
            // Balance
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.balanceSheetSplitContainer);
            this.DoubleBuffered = true;
            this.Name = "Balance";
            this.Size = new System.Drawing.Size(1236, 497);
            this.Load += new System.EventHandler(this.Balance_Load);
            this.treeViewContextMenuStrip.ResumeLayout(false);
            this.balanceSheetSplitContainer.Panel1.ResumeLayout(false);
            this.balanceSheetSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.balanceSheetSplitContainer)).EndInit();
            this.balanceSheetSplitContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.detailsGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.summaryGrid)).EndInit();
            this.summaryContextMenuStrip.ResumeLayout(false);
            this.balanceHeaderPanel.ResumeLayout(false);
            this.balanceHeaderPanel.PerformLayout();
            this.detailsGridContextMenuStrip.ResumeLayout(false);
            this.detailsGridRowHeaderContextMenuStrip.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip treeViewContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem addASubcategoryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem renameToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
        private System.Windows.Forms.SplitContainer balanceSheetSplitContainer;
        private BondManager.Controls.CategoryTreeView treeView;
        private DataGridViewEx detailsGrid;
        private DataGridViewEx summaryGrid;
        private System.Windows.Forms.Panel balanceHeaderPanel;
        private System.Windows.Forms.Button addNewButton;
        private System.Windows.Forms.Label selectedNodeLabel;
        private System.Windows.Forms.ContextMenuStrip detailsGridContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem sortAscendingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sortDescendingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem applyValueToEntireColumnToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem applyValueToRowsAboveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem applyValueToRowsBelowToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem applyValueToXRowsBelowToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem applyValueToSelectedRowsToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip detailsGridRowHeaderContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem deleteRowsToolStripMenuItem;
        private System.Windows.Forms.Button upButton;
        private System.Windows.Forms.ToolStripMenuItem modelAnalysisToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem modelAnalysisLineStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem copyDerivativeToolStripMenuItem;
        private System.Windows.Forms.Button excelImportButton;
        private System.Windows.Forms.Button excelPasteButton;
        private System.Windows.Forms.Button excelExportButton;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem moveUptoolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem moveDowntoolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem excludeSelectedToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem includeSelectedToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem excludeAllBelowToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem includeAllBelowToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pasteLinesToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem pasteColumnToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem recalculateBookValueToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportCategoryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importCategoryToolStripMenuItem;
        private System.Windows.Forms.Button categoryExcelImport;
        private System.Windows.Forms.Button categoryExcelExport;
        private System.Windows.Forms.ToolStripMenuItem copyColumnToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsTemplateToolStripMenuItem;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox scenarioComboBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboPages;
        private PageNavigator _pageNavigatorDown;
        private PageNavigator _pageNavigatorUp;
        private System.Windows.Forms.ToolStripMenuItem linkProductsAsSwapToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip summaryContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem toogleBookValueColumnShowHideToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toggleInputColumnShowHideToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toogleAnalysisCategoryColumnShowHideToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cutToolStripMenuItem;
        private System.Windows.Forms.Button importBttn;
        private System.Windows.Forms.DataGridView dataGridView1; 
    }
}
