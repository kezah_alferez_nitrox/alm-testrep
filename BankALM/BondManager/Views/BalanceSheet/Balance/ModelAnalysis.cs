using System;
using System.Collections.Generic;
using System.Windows.Forms;
using ALMS.Products;
using BondManager.Calculations;
using BondManager.Core;
using BondManager.Domain;
using BondManager.Resources.Language;

using System.Drawing;
using ZedGraph;

namespace BondManager.Views.BalanceSheet.Balance
{
    public partial class ModelAnalysis : SessionForm
    {
        private readonly int count;

        private ProductAnalysisData productAnalysisData;
        private readonly Product product;

        public static void Display(Font font, int productId)
        {
            ModelAnalysis form = new ModelAnalysis(productId);
            form.Font = font;
            form.ShowDialog();
        }

        private ModelAnalysis(int productId)
        {
            InitializeComponent();

            scenarioComboBox.ValueMember = "Self";

            this.product = ProductCache.FindById(productId);//Product.Find(this.Session, productId);

            //this.count = (int)Math.Min(CommonRules.GetDuration(this.product), Param.ResultSteps(this.Session));
            int recoveryLag=0;
            try
            {
                recoveryLag=  int.Parse(this.product.RecoveryLag);
            }
            catch (Exception)
            {
                recoveryLag = 0;
            }
            int CstPayLag =0;
            try
            {
                if(product.AmortizationType==ProductAmortizationType.CST_PAY && product.Amortization!=null) CstPayLag = int.Parse(this.product.Amortization);
            }
            catch (Exception)
            {
                CstPayLag = 0;
            }

            this.count = (int)Math.Min(this.product.IntDuration + CstPayLag + recoveryLag, Param.ResultMonths(this.Session));

            this.Text = Labels.ModelAnalysis_ModelAnalysis_Model_Analysis__ + this.product;
        }

        private void ModelAnalysis_Shown(object sender, EventArgs e)
        {
            ComputeAndDisplayValues();
        }

        private void ComputeAndDisplayValues()
        {
            this.grid.Font = this.Font;
            grid.RowTemplate.Height = Constants.GRID_ROW_HEIGHT;

            scenarioComboBox.DataSource = Scenario.FindSelected(this.Session);

            scenarioComboBox.SelectedIndexChanged += scenarioComboBox_SelectedIndexChanged;

            try
            {
                Cursor = Cursors.WaitCursor;

                InitialiseGrid();

                Reload();
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void InitialiseGrid()
        {
            grid.AutoGenerateColumns = false;

            List<DataGridViewColumn> columns = new List<DataGridViewColumn>();

            DataGridViewTextBoxColumn descriptionColumn = new DataGridViewTextBoxColumn();
            descriptionColumn.HeaderText = "";
            descriptionColumn.Width = 150;
            descriptionColumn.Frozen = true;
            columns.Add(descriptionColumn);

            for (int i = 0; i < count; i++)
            {
                DataGridViewTextBoxColumn valueColumn = new DataGridViewTextBoxColumn();
                valueColumn.HeaderText = string.Format("{0}", i);
                valueColumn.Width = 75;
                valueColumn.DefaultCellStyle.Format = "N2";
                valueColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                columns.Add(valueColumn);
            }

            grid.Columns.AddRange(columns.ToArray());
        }

        void scenarioComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            Reload();
        }

        private void Reload()
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                Scenario scenario = scenarioComboBox.SelectedItem as Scenario;
                if (scenario == null) return;

                this.productAnalysisData = new ProductAnalysisData(this.Session, this.GetSimulationParameters(), product);
                Scenario scn = Scenario.Find(this.Session, scenario.Id);
                List<ValidationMessage> validationMessages = this.productAnalysisData.RunSimulation(scn);

                if (validationMessages.Count > 0)
                {
                    string messages = "";
                    foreach (ValidationMessage message in validationMessages)
                    {
                        messages += "\n" + message.Message;
                    }
                    MessageBox.Show(messages);
                    this.Close();
                    return;
                }

                grid.Rows.Clear();

                ModelData modelData = this.productAnalysisData.ModelData;
                if (modelData == null) return;

                grid.SuspendLayout();

                Color inputColor = Color.DodgerBlue;
                AddRow(Labels.ModelAnalysis_Reload_Interest_Rate, modelData.InterestRate, "P", inputColor, false);
                AddRow("Spread", this.productAnalysisData.ProductData.Spread, "P", inputColor, false);
                AddRow(Labels.ModelAnalysis_Reload_Current_Nominal, modelData.CurrentNominal, "N2", Color.Black, true);
                AddRow(Labels.ModelAnalysis_Reload_Theorical_Face, modelData.TheoricalFace, "N2", Color.Black, true);
                AddRow(Labels.ModelAnalysis_Reload_Default, modelData.Default, "N2", Color.Black, false);
                //todo : refaire le caclul corectement pour pouvoir afficher ici le coupn paid
                //AddRow(Labels.ModelAnalysis_Reload_Interest_Incomes, , "N2", Color.Black, true);
                AddRow(Labels.ModelAnalysis_Reload_Normal_Reimbursement, modelData.NormalReimbursement, "N2", Color.Black, false);
                AddRow(Labels.ModelAnalysis_Reload_Prepayments, modelData.Prepayments, "N2", Color.Black, false);
                AddRow(Labels.ModelAnalysis_Reload_Recovery, modelData.LaggedRecovery, "N2", Color.Black, false);
                AddRow(Labels.ModelAnalysis_Reload_Losses___Impairments, modelData.LossesImpairment, "N2", Color.Black, true);
                // AddRow("Total Reimbursements & Recovery", modelData.TotalReimbursementsAndRecovery, "N2", Color.Black, true);
                AddRow(null, null, null, Color.Empty, false);
                AddRow(Labels.ModelAnalysis_Reload_CPR, modelData.CPR, "P", inputColor, false);
                AddRow(Labels.ModelAnalysis_Reload_CDR, modelData.CDR, "P", inputColor, false);
                AddRow(Labels.ModelAnalysis_Reload_LGD, modelData.LGD, "P", inputColor, false);
                //AddRow(Labels.ModelAnalysis_Reload_NRR, modelData.NRR, "P", Color.Black, false);
                AddRow(null, null, null, Color.Empty, false);
                AddRow(Labels.ModelAnalysis_Reload_Cumulative_Delinquencies__on_initial_nominal_,
                       modelData.CumulativeDelinquenciesOnInitialNominal, "P", Color.Black, false);
                AddRow(Labels.ModelAnalysis_Reload_Cumulative_Losses__on_initial_nominal_, modelData.CumulativeLossesOnInitialNominal, "P",
                       Color.Black, false);
                AddRow(Labels.ModelAnalysis_Reload_Cumulative_CPR__on_initial_nominal_, modelData.CumulativeCprOnInitialNominal, "P", Color.Black,
                       false);

                grid.ResumeLayout();

                UpdateGraphs(modelData);
            }
            catch(OverflowException ex)
            {
                Log.Exception("", ex);
                MessageBox.Show(Labels.ModelAnalysis_Reload_);
                this.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void UpdateGraphs(ModelData modelData)
        {
            cfZedGraphControl.GraphPane.Title.IsVisible = false;
            cfZedGraphControl.GraphPane.CurveList.Clear();
            AddCurve(cfZedGraphControl, Labels.ModelAnalysis_Reload_Current_Nominal, modelData.CurrentNominal, Color.Olive);
            AddCurve(cfZedGraphControl, Labels.ModelAnalysis_Reload_Theorical_Face, modelData.TheoricalFace, Color.Tomato);
            cfZedGraphControl.AxisChange();
            cfZedGraphControl.Invalidate();

            cumulativeZedGraphControl.GraphPane.Title.IsVisible = false;
            cumulativeZedGraphControl.GraphPane.CurveList.Clear();
            AddCurve(cumulativeZedGraphControl, Labels.ModelAnalysis_Reload_Cumulative_Delinquencies__on_initial_nominal_, modelData.CumulativeDelinquenciesOnInitialNominal, Color.Blue);
            AddCurve(cumulativeZedGraphControl, Labels.ModelAnalysis_Reload_Cumulative_Losses__on_initial_nominal_, modelData.CumulativeLossesOnInitialNominal, Color.Red);
            AddCurve(cumulativeZedGraphControl, Labels.ModelAnalysis_Reload_Cumulative_CPR__on_initial_nominal_, modelData.CumulativeCprOnInitialNominal, Color.Green);
            cumulativeZedGraphControl.AxisChange();
            cumulativeZedGraphControl.Invalidate();
        }

        private void AddCurve(ZedGraphControl zedGraphControl, string label, double[] data, Color color)
        {
            GraphPane zedPane = zedGraphControl.GraphPane;

            PointPairList list = new PointPairList();
            for (int i = 0; i < count; i++)
            {
                double x = i;
                double y = (double)data[i];
                list.Add(x, y);
            }

            LineItem graphCurve = zedPane.AddCurve(label, list, color);
            graphCurve.Line.Width = 2;
            graphCurve.Symbol.IsVisible = false;
        }

        private void AddRow(string description, double[] values, string format, Color foreColor, bool bold)
        {
            int rowIndex = grid.Rows.Add(1);

            if (string.IsNullOrEmpty(description)) return;

            grid[0, rowIndex].Value = description;

            Font boldFont = new Font(grid.Font, FontStyle.Bold);

            for (int i = 0; i < count; i++)
            {
                grid[1 + i, rowIndex].Value = values[i];            
            }

            grid.Rows[rowIndex].DefaultCellStyle.Format = format;
            grid.Rows[rowIndex].DefaultCellStyle.ForeColor = foreColor;
            if (bold) grid.Rows[rowIndex].DefaultCellStyle.Font = boldFont;
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}