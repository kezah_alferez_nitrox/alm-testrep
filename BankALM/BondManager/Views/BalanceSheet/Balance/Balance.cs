using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ALMSCommon;
using ALMSCommon.Forms;
using BondManager.BusinessLogic;
using BondManager.Calculations;
using BondManager.Core;
using BondManager.Core.IO;
using BondManager.Domain;
using BondManager.Properties;
using BondManager.Resources.Language;
using NHibernate;
using BankALM.Infrastructure.Data;
using OfficeOpenXml;
using System.Data;

namespace BondManager.Views.BalanceSheet.Balance
{
    public partial class Balance : SessionUserControl
    {
        private readonly DetailsGridController _detailsController;
        private readonly SummaryGridController _summaryController;

        private GridMode _gridMode = GridMode.None;

        private int _menuColumnIndex;
        private int _menuRowIndex;
        private Category _selectedCategory;
        private GlobalExportImport globalExportImport;

        public event EventHandler WorkspaceTemplateSaved;
        public event EventHandler OnModifiedValues;

        private int _bondsCount;
        private string filePath;
        static Product financialProd = new Product();

        public Balance()
        {
            this.InitializeComponent();
            //order the buttons
            this.label1.Location = Tools.GetNextDrawingPoint(this.importBttn, 6);
            this.comboPages.Location = Tools.GetNextDrawingPoint(this.label1, 7);
            this.label8.Location = Tools.GetNextDrawingPoint(this.comboPages, 11);
            this.scenarioComboBox.Location = Tools.GetNextDrawingPoint(this.label8, 6);
            this.categoryExcelImport.Location = Tools.GetNextDrawingPoint(this.scenarioComboBox, 6);
            this.categoryExcelExport.Location = Tools.GetNextDrawingPoint(this.categoryExcelImport, 6);
            this.excelImportButton.Location = Tools.GetNextDrawingPoint(this.categoryExcelExport, 6);
            this.excelPasteButton.Location = Tools.GetNextDrawingPoint(this.excelImportButton, 6);
            this.excelExportButton.Location = Tools.GetNextDrawingPoint(this.excelPasteButton, 6);
            this.upButton.Location = Tools.GetNextDrawingPoint(this.excelExportButton, 6);
            this.addNewButton.Location = Tools.GetNextDrawingPoint(this.upButton, 6);
            //done
            this.SetRowsPerPageCombo();
            this.comboPages.SelectedIndexChanged += ChangeRowsPerPages;

            this.detailsGrid.BackgroundImage = Properties.Resources.logo400x400;
            this.summaryGrid.BackgroundImage = Properties.Resources.logo400x400;

            if (!this.DesignMode && !Tools.InDesignMode)
            {
                this._detailsController = new DetailsGridController(this.detailsGrid);
                this._summaryController = new SummaryGridController(this.summaryGrid);
            }

            this.detailsGrid.EditingControlShowing += this.detailsGrid_EditingControlShowing;

            this.detailsGrid.RowTemplate.HeaderCell.ContextMenuStrip = this.detailsGridRowHeaderContextMenuStrip;
            this.detailsGrid.KeyDown += this.GridKeyDown;

            this.detailsGrid.AllowDrop = true;

            this._pageNavigatorDown.PageBeforeChange += PageBeforeChanged;
            this._pageNavigatorDown.PageChanged += PageChanged;
            this._pageNavigatorDown.MaxPageChanged += MaxPageChanged;
            this._pageNavigatorUp.PageChanged += PageChangedFake;
        }

        private void PageBeforeChanged(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
        }

        private void PageChangedFake(object sender, EventArgs e)
        {
            this._pageNavigatorDown.PageIndex = this._pageNavigatorUp.PageIndex;
        }

        private void MaxPageChanged(object sender, EventArgs e)
        {
            this._pageNavigatorUp.MaxPage = this._pageNavigatorDown.MaxPage;
        }

        private void PageChanged(object sender, EventArgs e)
        {
            this._bondsCount = this._detailsController.Refresh(this._selectedCategory, _pageNavigatorDown.PageIndex, true);
            this.SetTitle(Settings.Default.CategoryRowsPerPages, this._selectedCategory.Label, this._bondsCount);

            this._pageNavigatorUp.PageChanged -= PageChangedFake;
            this._pageNavigatorUp.PageIndex = this._pageNavigatorDown.PageIndex;
            this._pageNavigatorUp.PageChanged += PageChangedFake;

            Cursor = Cursors.Default;
        }

        private void SetRowsPerPageCombo()
        {
            List<object> pages = new List<object> { 5, 10, 100, 200, 500, 1000, 2000, 5000, "All" };
            if (Settings.Default.CategoryRowsPerPages != int.MaxValue && !pages.Contains(Settings.Default.CategoryRowsPerPages))
                pages.Insert(0, Settings.Default.CategoryRowsPerPages);
            comboPages.DataSource = pages;
            if (Settings.Default.CategoryRowsPerPages != int.MaxValue) comboPages.SelectedItem = Settings.Default.CategoryRowsPerPages;
            else comboPages.SelectedItem = "All";
        }

        private void ChangeRowsPerPages(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            if (comboPages.SelectedItem is int) Settings.Default.CategoryRowsPerPages = (int)comboPages.SelectedItem;
            else Settings.Default.CategoryRowsPerPages = int.MaxValue;

            _pageNavigatorDown.RefreshPage(0);
            _pageNavigatorDown.MaxPage = (int)Math.Ceiling((float)this._bondsCount / Settings.Default.CategoryRowsPerPages) - 1;
            this.SetTitle(Settings.Default.CategoryRowsPerPages, this._selectedCategory.Label, this._bondsCount);

            Cursor.Current = Cursors.Default;
        }

        public void SetModifiedValueHandler(EventHandler handler)
        {
            this._detailsController.OnModifiedValues += handler;
            this.OnModifiedValues += handler;
        }

        private void SetModifiedValues(bool modified)
        {
            MainForm.ModifiedValues = modified;
            if (this.OnModifiedValues != null) this.OnModifiedValues.Invoke(this, EventArgs.Empty);
        }

        private void Refresh(object sender, EventArgs e)
        {
            this.RefreshSelectedCategory();
        }

        private void GridKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == (Keys.Down | Keys.Alt))
            {
                this.MoveItem(this.moveDowntoolStripMenuItem, EventArgs.Empty);
                e.SuppressKeyPress = true;
            }
            else if (e.KeyData == (Keys.Up | Keys.Alt))
            {
                this.MoveItem(this.moveUptoolStripMenuItem, EventArgs.Empty);
                e.SuppressKeyPress = true;
            }
        }

        private void Balance_Load(object sender, EventArgs e)
        {
            if (!this.DesignMode && !Tools.InDesignMode)
            {
                this.treeView.LoadNodes();
                this.treeView.DrawMode = TreeViewDrawMode.OwnerDrawText;
                if (this.treeView.Nodes.Count > 0) this.treeView.SelectedNode = this.treeView.Nodes[0];

                this.SetScenarios();
            }
        }

        public void SetScenarios()
        {
            Scenario[] scenarios = Scenario.FindSelected(this.Session);
            this.scenarioComboBox.SelectedIndexChanged -= SelectScenario;
            this.scenarioComboBox.DataSource = new BindingList<Scenario>(new List<Scenario>(scenarios));
            this.scenarioComboBox.SelectedItem = Scenario.FindDefault(this.Session);
            this.scenarioComboBox.SelectedIndexChanged += SelectScenario;
        }

        private void detailsGrid_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            //todo : move to grid controller
            if (e.Control is TextBox)
            {
                this._menuColumnIndex = this.detailsGrid.CurrentCell.ColumnIndex;
                this._menuRowIndex = this.detailsGrid.CurrentCell.RowIndex;
                e.Control.ContextMenuStrip = this.detailsGridContextMenuStrip;
            }
        }

        private void addNewButton_Click(object sender, EventArgs e)
        {
            AddProductsDialog addProductsDialog = new AddProductsDialog();
            if (addProductsDialog.ShowDialog() == DialogResult.OK)
            {
                this._detailsController.AddNewBonds(addProductsDialog.RowCount,
                    addProductsDialog.ProductType,
                    addProductsDialog.GetLeftProduct, addProductsDialog.GetRightProduct, addProductsDialog.GetSwaption);
                this._bondsCount = this._detailsController.Refresh(this._selectedCategory, _pageNavigatorDown.PageIndex, true);
                _pageNavigatorDown.MaxPage = (int)Math.Ceiling((float)this._bondsCount / Settings.Default.CategoryRowsPerPages) - 1;

                if (this._pageNavigatorDown.PageIndex < 0) this._pageNavigatorDown.PageIndex = 0;
                this.SetTitle(Settings.Default.CategoryRowsPerPages, this._selectedCategory.Label, this._bondsCount);
            }
            //AddBondsPrompt addNewRowsPrompt = new AddBondsPrompt(_selectedCategory.Type);
            //if (addNewRowsPrompt.ShowDialog() == DialogResult.OK)
            //{
            //    _detailsController.AddNewBonds(addNewRowsPrompt.RowCount,
            //        addNewRowsPrompt.ProductType,
            //        addNewRowsPrompt.InitLeftBond, addNewRowsPrompt.InitRightBond);
            //    PersistenceSession.Flush();
            //}
        }

        private void treeViewContextMenuStrip_Opening(object sender, CancelEventArgs e)
        {
            if (null == this.treeView.SelectedNode)
            {
                e.Cancel = true;
                return;
            }
            this.treeView.Enabled = true;

            this.deleteToolStripMenuItem.Enabled = CanDeleteNode(this.treeView.SelectedNode);

            this.renameToolStripMenuItem.Enabled = CanRenameNode(this.treeView.SelectedNode);

            this.addASubcategoryToolStripMenuItem.Enabled = CanAddChildNode(this.treeView.SelectedNode);

            this.importCategoryToolStripMenuItem.Enabled = !this.addASubcategoryToolStripMenuItem.Enabled;
            this.exportCategoryToolStripMenuItem.Enabled = !this.addASubcategoryToolStripMenuItem.Enabled;

            this.saveAsTemplateToolStripMenuItem.Visible = this.treeView.SelectedNode != null &&
                                                           this.treeView.SelectedNode.Level == 0;
        }

        private bool CanDeleteNode(TreeNode node)
        {
            Category category = node.Tag as Category;
            if (category == null || category.Id == 0) return false;

            category = Category.Find(this.Session, category.Id);

            return (0 == node.Nodes.Count && node.Level > 1 &&
                category != null && !category.ReadOnly && category.ProductCount(Session) == 0);
        }

        private static bool CanRenameNode(TreeNode node)
        {
            if (node == null) return false;
            Category category = node.Tag as Category;

            return (category != null) && category.CanRename();
        }

        private bool CanAddChildNode(TreeNode node)
        {
            Category category = node.Tag as Category;
            if (category == null || category.Id == 0) return false;

            category = Category.Find(this.Session, category.Id);

            return category != null && (category.Type == CategoryType.Normal || category.Type == CategoryType.RollParent) &&
                node.Level > 0 && (category.Bonds == null || category.ProductCount(Session) == 0);
        }

        private void treeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            Category category = e.Node.Tag as Category;
            if (null == category) return;

            Cursor.Current = Cursors.WaitCursor;

            this._selectedCategory = category;

            if (category.Type == CategoryType.CashAdj || category.Type == CategoryType.Treasury ||
                e.Node.Nodes.Count != 0 || category.Type == CategoryType.PositiveDerivativesValuation ||
                category.Type == CategoryType.NegativeDerivativesValuation)
                this.addNewButton.Enabled = false;
            else
                this.addNewButton.Enabled = true;

            GridMode newGridmode = (0 == this._selectedCategory.Children.Count) ? GridMode.Detail : GridMode.Summary;

            if (this._gridMode != newGridmode)
            {
                this.detailsGrid.Visible = (newGridmode == GridMode.Detail);
                this.summaryGrid.Visible = (newGridmode == GridMode.Summary);
                this._gridMode = newGridmode;
            }

            this._pageNavigatorDown.Visible = this._gridMode == GridMode.Detail;
            this._pageNavigatorUp.Visible = this._gridMode == GridMode.Detail;
            int pageRows = Settings.Default.CategoryRowsPerPages;
            //int count = 0;
            switch (this._gridMode)
            {
                case GridMode.Detail:
                    this._pageNavigatorDown.RefreshPage(0);
                    this._pageNavigatorDown.MaxPage = (int)Math.Ceiling((float)this._bondsCount / pageRows) - 1;
                    break;

                default: //case GridMode.Summary:
                    this._summaryController.Refresh(this._selectedCategory);
                    break;
            }

            this.SetTitle(pageRows, e.Node.Text, this._bondsCount);

            this.categoryExcelExport.Enabled = true;// !CanAddChildNode(this.treeView.SelectedNode);
            this.categoryExcelImport.Enabled = this.categoryExcelExport.Enabled;

            Cursor.Current = Cursors.Default;
        }

        private void SetTitle(int pageRows, string title, int count)
        {
            if (count != 0 && this._gridMode == GridMode.Detail) this.selectedNodeLabel.Text = title + " lines " +
                (this._pageNavigatorDown.PageIndex * pageRows + 1) + " to " +
                Math.Min((this._pageNavigatorDown.PageIndex + 1) * pageRows, count) + " of " + count;
            else this.selectedNodeLabel.Text = title;
        }

        private void detailsGrid_CellContextMenuStripNeeded(object sender,
                                                            DataGridViewCellContextMenuStripNeededEventArgs e)
        {
            this._menuColumnIndex = e.ColumnIndex;
            this._menuRowIndex = e.RowIndex;
            if (this._menuColumnIndex != -1)
                e.ContextMenuStrip = this.detailsGridContextMenuStrip;

            if (this._menuRowIndex >= 0 && this._menuColumnIndex >= 0)
            {
                this.detailsGrid.CurrentCell = this.detailsGrid[this._menuColumnIndex, this._menuRowIndex];
            }
        }

        private void sortAscendingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.detailsGrid.ClearSelection();
            this.detailsGrid.Sort(this.detailsGrid.Columns[this._menuColumnIndex], ListSortDirection.Ascending);
        }

        private void sortDescendingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.detailsGrid.ClearSelection();
            this.detailsGrid.Sort(this.detailsGrid.Columns[this._menuColumnIndex], ListSortDirection.Descending);
        }

        private void applyValueToEntireColumnToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.applyValueToRowsAboveToolStripMenuItem_Click(null, null);
            this.applyValueToRowsBelowToolStripMenuItem_Click(null, null);
        }

        private void applyValueToRowsAboveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            object value = this.detailsGrid[this._menuColumnIndex, this._menuRowIndex].Value;
            for (int i = 0; i < this._menuRowIndex; i++)
            {
                this.detailsGrid[this._menuColumnIndex, i].Value = value;
                this.detailsGrid.Refresh();
            }
        }

        private void excludeSelectedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this._detailsController.ExcludeAbove(true);
        }

        private void includeSelectedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this._detailsController.ExcludeAbove(false);
        }


        private void excludeAllBelowToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this._detailsController.ExcludeBelow(true);
        }

        private void includeAllBelowToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this._detailsController.ExcludeBelow(false);
        }

        private void applyValueToRowsBelowToolStripMenuItem_Click(object sender, EventArgs e)
        {
            object value = this.detailsGrid[this._menuColumnIndex, this._menuRowIndex].Value;
            for (int i = this._menuRowIndex + 1; i < this.detailsGrid.Rows.Count - 1; i++)
            {
                this.detailsGrid[this._menuColumnIndex, i].Value = value;
                this.detailsGrid.Refresh();
            }
        }

        private void applyValueToXRowsBelowToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string stringRows = "5";
            if (Util.InputBox("Copy to how many rows below ?", "Rows:", ref stringRows) == DialogResult.OK)
            {
                int rows=0;
                int.TryParse(stringRows, out rows);
                
                object value = this.detailsGrid[this._menuColumnIndex, this._menuRowIndex].Value;
                for (int i = this._menuRowIndex + 1; i < this.detailsGrid.Rows.Count - 1 && i < this._menuRowIndex + 1 + rows; i++)
                {
                    this.detailsGrid[this._menuColumnIndex, i].Value = value;
                }
            }
        }

        private void applyValueToSelectedRowsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            object value = this.detailsGrid[this._menuColumnIndex, this._menuRowIndex].Value;
            for (int i = 0; i < this.detailsGrid.Rows.Count - 1; i++)
            {
                if (i != this._menuRowIndex && this.detailsGrid[this._menuColumnIndex, i].Selected)
                {
                    this.detailsGrid[this._menuColumnIndex, i].Value = value;
                }
            }
        }

        private void detailsGridContextMenuStrip_Opening(object sender, CancelEventArgs e)
        {
            this.applyValueToSelectedRowsToolStripMenuItem.Visible =
                this.applyValueToRowsAboveToolStripMenuItem.Visible =
                this.applyValueToRowsBelowToolStripMenuItem.Visible =
                this.applyValueToEntireColumnToolStripMenuItem.Visible =
                (this._menuRowIndex != -1 && this._detailsController.IsColumnCopyAble(this._menuColumnIndex));

            this.modelAnalysisToolStripMenuItem.Enabled = this._detailsController.IsModelRow(this._menuRowIndex);

            this.detailsGrid.EndEdit();
            if (null != this.detailsGrid.EditingControl) this.detailsGrid.EditingControl.Visible = false;
        }

        public void Reload()
        {
            this.ClearSession();

            this.treeView.LoadNodes();
            this._bondsCount = this._detailsController.Refresh(null, _pageNavigatorDown.PageIndex, true);
            this._summaryController.Refresh(null);

            if (this.treeView.Nodes.Count > 0)
            {
                this.treeView.SelectedNode = this.treeView.Nodes[0];
            }
            else
            {
                this._selectedCategory = null;
                this.addNewButton.Enabled = false;
                this.upButton.Enabled = false;
            }
        }

        public void RefreshSelectedCategory()
        {
            if (this._selectedCategory != null)
            {
                if (this._gridMode == GridMode.Detail)
                {
                    this._bondsCount = this._detailsController.Refresh(this._selectedCategory /*todo verifier not null*/, this._pageNavigatorDown.PageIndex, true);
                }
                else
                {
                    this._summaryController.Refresh(this._selectedCategory);
                }
            }
        }

        public void RefreshTreeView()
        {
            treeView.LoadNodes();

        }

        public void ClipboardCopy(bool cut)
        {
            if (this._gridMode == GridMode.Detail)
            {
                this._detailsController.ClipboardCopy(cut);
            }
            else
            {
                this._summaryController.ClipboardCopy();
            }


        }

        public void ClipboardPaste()
        {
            if (this._gridMode == GridMode.Detail)
            {
                this._detailsController.ClipboadPaste();
            }
        }

        private void deleteRowsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Param.GetConfirmBeforeDelete(SessionManager.OpenSession()) &&
                (MessageBox.Show("Do you really want to delete this line(s) ?", "", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) != DialogResult.OK))
            {
                return;
            }
            Cursor = Cursors.WaitCursor;
            this._detailsController.DeleteRows(this._menuRowIndex);
            this._bondsCount = this._detailsController.Refresh(this._selectedCategory, _pageNavigatorDown.PageIndex, true);
            Cursor = Cursors.Arrow;

        }

        private void upButton_Click(object sender, EventArgs e)
        {
            if (this.treeView.SelectedNode != null && this.treeView.SelectedNode.Parent != null)
            {
                this.treeView.SelectedNode = this.treeView.SelectedNode.Parent;
            }
        }

        public void Parameters_ValuationCurrencyChanged(object sender, EventArgs e)
        {
            this._detailsController.CurrenciesChanged();
            this._summaryController.Refresh(this._selectedCategory);
        }

        public void OnFxRatesChange()
        {
            this._detailsController.CurrenciesChanged();
            this._summaryController.Refresh(this._selectedCategory);
        }

        public void FindAndSelectBond(Product product, string property)
        {
            this.FindAndSelectBategory(product.Category);
            this._detailsController.SelectCellFor(product, property);
        }

        private void FindAndSelectBategory(Category category)
        {
            this.FindCategoryInTreeView(this.treeView.Nodes[0], category);
        }

        private void FindCategoryInTreeView(TreeNode node, Category category)
        {
            Category nodeCategory = (Category)node.Tag;
            if (nodeCategory == category)
            {
                node.EnsureVisible();
                this.treeView.SelectedNode = node;
                return;
            }

            foreach (TreeNode childNode in node.Nodes)
            {
                this.FindCategoryInTreeView(childNode, category);
            }
        }

        private void modelAnalysisToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Product product = this._detailsController.GetProductAtIndex(this._menuRowIndex);
            if (product == null || product.Id == 0) return;

            _detailsController.FlushChanges(null);

            ModelAnalysis.Display(this.Font, product.Id);
        }

        public void BeforeRunSimulation()
        {
            this._detailsController.EndEdit();
            this._summaryController.EndEdit();
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            this.modelAnalysisToolStripMenuItem_Click(sender, e);
        }

        private void detailsGridRowHeaderContextMenuStrip_Opening(object sender, CancelEventArgs e)
        {


            ContextMenuStrip contextMenuStrip = sender as ContextMenuStrip;
            if (contextMenuStrip == null) return;

            ToolStripItem toolStripItem = contextMenuStrip.Items[linkProductsAsSwapToolStripMenuItem.Name];
            if (toolStripItem == null) return;

            toolStripItem.Visible = this._detailsController.CanLinkSelectedLinesAsSwap();

            ToolStripItem modelAnalysisItem = contextMenuStrip.Items[modelAnalysisLineStripMenuItem1.Name];
            if (modelAnalysisItem == null) return;

            modelAnalysisItem.Enabled = this._detailsController.IsModelRow(this._menuRowIndex);


        }

        private void copyDerivativeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.ClipboardCopy(false);
        }

        private void excelExportButton_Click(object sender, EventArgs e)
        {
            this.Session.Flush();

            LoadForm.Instance.SetTitle(Labels.Balance_excelExportButton_Click_Global_Excel_Export);
            LoadForm.Instance.DoWork(new WorkParameters(bg =>
            {
                this.globalExportImport = new GlobalExportImport(bg);
                this.globalExportImport.Export();
                bg.ReportProgress(100);
            }, this.EndGlobalExcelExport));
        }

        private void EndGlobalExcelExport(Exception exception)
        {
            if (this.globalExportImport.Errors.Count > 0)
            {
                DisplayErrors(this.globalExportImport.Errors,
                              Labels.Balance_EndGlobalExcelExport_Global_Excel_Export_Errors);
            }
        }

        //private static string GetExportGlobalNewFileName()
        //{
        //    return string.Format("{0}\\GlobalExport.{1}.xls",
        //        Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),
        //        DateTime.Now.ToString("yyyyMMdd-HHmmss"));
        //}

        private static void DisplayErrors(IEnumerable<string> errors, string title)
        {
            StringBuilder sb = new StringBuilder();

            foreach (string error in errors)
            {
                sb.AppendLine(error);
            }
            string message = sb.ToString();

            //if (message.Length > 100) message = message.Substring(0, 100) + "...";

            //MessageBox.Show(message, title, MessageBoxButtons.OK, MessageBoxIcon.Error);
            ErrorList er=new ErrorList();
            er.SetText(message);
            er.ShowDialog();
        }

        private void excelImportButton_Click(object sender, EventArgs e)
        {
            if (this.openFileDialog.ShowDialog() == DialogResult.OK)
            {
                LoadForm.Instance.SetTitle(Labels.Balance_excelImportButton_Click_Global_Excel_Import);
                LoadForm.Instance.DoWork(new WorkParameters(bg =>
                {
                    this.globalExportImport = new GlobalExportImport(bg);
                    this.globalExportImport.Import(
                        this.openFileDialog.FileName);
                    bg.ReportProgress(100);
                }, this.EndGlobalExcelImport));
            }
        }

        private void EndGlobalExcelImport(Exception exception)
        {
            if (this.globalExportImport.Errors.Count > 0)
            {
                DisplayErrors(this.globalExportImport.Errors,
                              Labels.Balance_EndGlobalExcelImport_Global_Excel_Import_Errors);
            }
            else
            {
                this.Reload();

                string message = string.Format(Labels.Balance_EndGlobalExcelImport_Import_Succeded,
                                               this.globalExportImport.AddedProductsCount,
                                               this.globalExportImport.DeletedProductsCount,
                                               this.globalExportImport.ChangedProductsCount);

                MessageBox.Show(message, Labels.Balance_excelImportButton_Click_Global_Excel_Import,
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Information);
            }
        }

        private void MoveItem(object sender, EventArgs e)
        {
            DataGridView grid = this.detailsGrid;

            List<DataGridViewRow> selectedRows = new List<DataGridViewRow>();

            foreach (DataGridViewRow row in grid.Rows)
            {
                bool selected = false;
                foreach (DataGridViewCell cell in row.Cells) if (cell.Selected) selected = true;
                if (row.Selected) selected = true;
                if (selected) selectedRows.Add(row);
            }
            List<Product> movedProducts = new List<Product>();

            grid.ClearSelection();
            grid.CurrentCell = null;

            int move;
            if (sender == this.moveUptoolStripMenuItem) move = -1;
            else if (sender == this.moveDowntoolStripMenuItem) move = 1;
            else throw new ArgumentException();

            int i;
            if (move > 0) i = this._selectedCategory.ProductCount(Session) - 1;
            else i = 0;

            BindingList<Product> products = (SortableBindingList<Product>)grid.DataSource;
            while (i >= 0 && i < grid.Rows.Count)
            {
                DataGridViewRow row = grid.Rows[i];
                Product product = row.DataBoundItem as Product;
                if (!selectedRows.Contains(row) || product == null || product.IsTotal)
                {
                    i += move * -1;
                    continue;
                }
                if (i + move < 0 || i + move >= this._selectedCategory.ProductCount(Session)) return;

                Product placeProduct = products[i + move];
                movedProducts.Add(product);

                if (product.OtherSwapLeg != null) product = product.OtherSwapLeg;
                products.Remove(product);

                if (placeProduct.SwapLeg == null || placeProduct.ProductType == product.ProductType)
                    products.Insert(i + move, product);
                else products.Insert(i + move * 2, product);

                if (product.OtherSwapLeg != null)
                {
                    products.Remove(product.OtherSwapLeg);
                    products.Insert(
                        i +
                        (placeProduct.SwapLeg == null || placeProduct.ProductType == product.ProductType ? move : move * 2),
                        product.OtherSwapLeg);
                }

                i += move * -1;
            }

            foreach (Product product in ProductCache.products.FindAll(p=>p.Category.ALMID==this._selectedCategory.ALMID))//this._selectedCategory.Bonds)
                product.Position = products.IndexOf(product);

            grid.ClearSelection();
            foreach (Product product in movedProducts)
                foreach (DataGridViewRow row in grid.Rows) if (row.DataBoundItem == product) row.Selected = true;
        }

        private void pasteLinesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this._detailsController.ClipboadPaste();
        }

        private void pasteColumnToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this._detailsController.PasteColumn();
        }

        private void recalculateBookValueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this._detailsController.RecalculateBookValue(this._menuRowIndex);
        }

        private void ExportCategory(object sender, EventArgs e)
        {
            TreeNode node = this.treeView.SelectedNode;
            if (node == null) return;
            Category category = node.Tag as Category;

            if (category == null || category.Id == 0) return;
            category = Category.Find(this.Session, category.Id);

            this.Session.Flush();

            LoadForm.Instance.SetTitle(Labels.Balance_excelExportButton_Click_Global_Excel_Export);
            LoadForm.Instance.DoWork(new WorkParameters(bg =>
            {
                this.globalExportImport = new GlobalExportImport(bg, category);
                this.globalExportImport.Export();
                bg.ReportProgress(100);
            }, this.EndGlobalExcelExport));
        }

        private void ImportCategory(object sender, EventArgs e)
        {
            TreeNode node = this.treeView.SelectedNode;
            if (node == null) return;
            Category category = node.Tag as Category;

            if (category == null || category.Id == 0) return;
            category = Category.Find(this.Session, category.Id);

            if (this.openFileDialog.ShowDialog() == DialogResult.OK)
            {
                LoadForm.Instance.SetTitle(Labels.Balance_excelImportButton_Click_Global_Excel_Import);
                LoadForm.Instance.DoWork(new WorkParameters(bg =>
                {
                    this.globalExportImport = new GlobalExportImport(
                        bg, category);
                    this.globalExportImport.Import(
                        this.openFileDialog.FileName);
                    bg.ReportProgress(100);
                }, this.EndGlobalExcelImport));

                this.SetModifiedValues(true);
            }
        }

        private void copyColumnToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.detailsGrid.ClearSelection();
            for (int i = 0; i < detailsGrid.RowCount - 1; i++)
            {
                this.detailsGrid[_menuColumnIndex, i].Selected = true;
            }

            this.ClipboardCopy(false);
        }

        #region "TreeView modification"

        private void addASubcategoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!CanAddChildNode(this.treeView.SelectedNode)) return;

            this.treeView.AddNode();
        }

        private void renameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!CanRenameNode(this.treeView.SelectedNode)) return;
            this.treeView.RenameNode();
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!CanDeleteNode(this.treeView.SelectedNode)) return;

            this.treeView.DeleteNode();
        }

        #endregion

        #region Nested type: GridMode

        protected enum GridMode
        {
            None,
            Detail,
            Summary
        }

        #endregion

        private void SaveAsTemplate(object sender, EventArgs e)
        {
            this.SaveAsTemplate();
        }

        public void SaveAsTemplate()
        {
            AddTemplate form = new AddTemplate();
            DialogResult response = form.ShowDialog();
            if (response == DialogResult.Cancel) return;

            this.Session.Flush();
            CategoryXmlSerializer serializer = new CategoryXmlSerializer();
            string path = serializer.Save(form.Name);

            if (form.SendTemplate)
            {
                Mapi mapi = new Mapi();
                mapi.AddRecipientTo("support@alm-vision.com");
                mapi.AddRecipientTo("info@alm-vision.com");
                mapi.AddAttachment(path);
                mapi.SendMailPopup("[ALM] Add template", "");
            }

            if (WorkspaceTemplateSaved != null) WorkspaceTemplateSaved.Invoke(this, EventArgs.Empty);
        }

        private void SelectScenario(object sender, EventArgs e)
        {
            Scenario oldDefault = Scenario.FindDefault(this.Session);
            Scenario newDefault = scenarioComboBox.SelectedItem as Scenario;
            if (newDefault != null && oldDefault != newDefault)
            {
                MainForm.ModifiedValues = true;
                if (oldDefault != null) oldDefault.IsDefault = false;
                newDefault.IsDefault = true;
                newDefault.Save(this.Session); // TODO CDU
                this.SaveChangesInSession();

                if (this._gridMode == GridMode.Detail) this._bondsCount = this._detailsController.Refresh(this._selectedCategory, _pageNavigatorDown.PageIndex, true);

            }
        }

        private void linkProductsAsSwapToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this._detailsController.LinkSelectedProductsAsSwap();
        }


        private void OnDrawNode(object sender, DrawTreeNodeEventArgs e)
        {
            Rectangle cadre = e.Bounds;
            cadre.Width = e.Node.TreeView.Width - cadre.Left;

            TreeNodeStates state = e.State;
            Font font = e.Node.NodeFont ?? e.Node.TreeView.Font;
            Color fore = e.Node.ForeColor;
            if (fore == Color.Empty) fore = e.Node.TreeView.ForeColor;
            if (e.Node == e.Node.TreeView.SelectedNode)
            {
                fore = Color.White;
                e.Graphics.FillRectangle(SystemBrushes.ControlDarkDark, cadre);
                ControlPaint.DrawFocusRectangle(e.Graphics, cadre, fore, SystemColors.Highlight);
                TextRenderer.DrawText(e.Graphics, e.Node.Text, font, cadre, fore, TextFormatFlags.GlyphOverhangPadding);
            }
            else
            {
                e.Graphics.FillRectangle(SystemBrushes.Window, cadre);
                TextRenderer.DrawText(e.Graphics, e.Node.Text, font, cadre, fore, TextFormatFlags.GlyphOverhangPadding);
            }
        }

        private void excelPasteButton_Click(object sender, EventArgs e)
        {
            LoadForm.Instance.SetTitle("Global Excel Paste");
            LoadForm.Instance.DoWork(new WorkParameters(bg =>
            {
                this.globalExportImport = new GlobalExportImport(bg);
                this.globalExportImport.ImportFromClipboard();
                bg.ReportProgress(100);
            }, this.EndGlobalExcelImport));


        }

        private void toogleBookValueColumnShowHideToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _summaryController.ToggleBookValueColumn();
        }

        private void toggleInputColumnShowHideToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _summaryController.ToggleInputColumn();
        }

        private void toogleAnalysisCategoryColumnShowHideToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _summaryController.ToggleAnalysisCategoryColumn();
        }

        private void cutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.ClipboardCopy(true);
        }

        private void importBttn_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Excel File (*.xlsx)|*.xlsx";
            openFileDialog.FilterIndex = 1;
            DialogResult result = openFileDialog.ShowDialog();

            if (result == DialogResult.OK)
            {
                filePath = openFileDialog.FileName;
                MessageBox.Show(filePath);

                try
                {
                    using (ExcelPackage pck = new ExcelPackage())
                    {
                        using (var stream = File.OpenRead(openFileDialog.FileName))
                        {
                            pck.Load(stream);
                        }

                        ExcelWorksheet ws = pck.Workbook.Worksheets.First();
                        dataGridView1.DataSource = WorksheetToDataTable(ws, true);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Import failed. Original error: " + ex.Message);
                }
            }         
        }

        private DataTable WorksheetToDataTable(ExcelWorksheet worksheet, bool hasHeader)
        {
            DataTable dataTable = new DataTable(worksheet.Name);
            int totalCols = worksheet.Dimension.End.Column;
            int totalRows = worksheet.Dimension.End.Row;
            int startRow = hasHeader ? 2 : 1;
            ExcelRange worksheetRow;
            DataRow dataRow;

            string headerName;
            bool boolParsed;
            double doubleParsed;
            DateTime dateTimeParsed;
            int intParsed;

            Dictionary<int, string> headerDictionary = new Dictionary<int, string>();

            foreach (var firstRowCell in worksheet.Cells[1, 1, 1, totalCols])
            {
                dataTable.Columns.Add(hasHeader ? firstRowCell.Text : string.Format("Column {0}", firstRowCell.Start.Column));
                headerDictionary.Add(dataTable.Columns.Count, firstRowCell.Text);
            }

            for (int rowNum = startRow; rowNum <= totalRows; rowNum++)
            {
                worksheetRow = worksheet.Cells[rowNum, 1, rowNum, totalCols];
                dataRow = dataTable.NewRow();

                foreach (var cell in worksheetRow)
                {
                    if (headerDictionary.TryGetValue(cell.Start.Column, out headerName))
                    {
                        switch (headerName.ToUpper())
                        {/*
                            case "BONDINTERNALID":
                                int.TryParse(cell.Text, out intParsed);
                                financialProd.ALMID = intParsed;
                                Logger.Info("Importing product " + financialProd.ALMID);
                                break;
                            case "PRODUCTTYPE":
                                financialProd.ProductType = cell.Text;
                                break;*/
                            case "DESCRIPTION":
                                financialProd.Description = cell.Text;
                                //Logger.Info("desc: " + financialProd.Description);
                                break;
                            case "STARTDATE":
                                DateTime.TryParse(cell.Text, out dateTimeParsed);
                                financialProd.StartDate = dateTimeParsed;
                                break;
                            case "EXPIRATIONDATE":
                                DateTime.TryParse(cell.Text, out dateTimeParsed);
                                financialProd.ExpirationDate = dateTimeParsed;
                                //Logger.Info("desc: " + financialProd.Description);
                                break;
                            case "DURATION":
                                double.TryParse(cell.Text, out doubleParsed);
                                financialProd.Duration = doubleParsed;
                                break;
                        /*  case "COUPONBASIS":
                                DateTime.TryParse(cell.Text, out dateTimeParsed);
                                financialProd.ExpirationDate = dateTimeParsed;
                                //Logger.Info("desc: " + financialProd.Description);
                                break;
                            case "FREQUENCY":
                                double.TryParse(cell.Text, out doubleParsed);
                                financialProd.Duration = doubleParsed;
                                break;
                            case "EXCLUDE":
                                bool.TryParse(cell.Text, out boolParsed);
                                financialProd.exclude = boolParsed;
                                break;
                            case "SECID":
                                financialProd.clientId = cell.Text;
                                break;
                            case "ACCOUNTING":
                                financialProd.accounting = cell.Text;
                                break;
                            case "ORIGFACE":
                                double.TryParse(cell.Text, out doubleParsed);
                                financialProd.origFace = doubleParsed;
                                break;
                            case "BOOKPRICE":
                                double.TryParse(cell.Text, out doubleParsed);
                                financialProd.bookPrice = doubleParsed;
                                break;
                            case "ACTUARIALPRICE":
                                double.TryParse(cell.Text, out doubleParsed);
                                financialProd.actuarialPrice = doubleParsed;
                                break;
                            case "BOOKVALUE":
                                //double.TryParse(cell.Text, out doubleParsed);
                                //financialProd.bookValue = doubleParsed;
                                break;
                            case "TAX":
                                financialProd.tax = cell.Text;
                                break;
                            case "CURRENCY":
                                financialProd.currency = cell.Text;
                                Console.WriteLine("curr" + financialProd.currency);
                                break;
                            case "RWA":
                                double.TryParse(cell.Text, out doubleParsed);
                                financialProd.RWA = doubleParsed;
                                break;
                            case "MARKETBASIS":
                                double.TryParse(cell.Text, out doubleParsed);
                                financialProd.marketBasis = doubleParsed;
                                break;
                            case "PORTFOLIO":
                                financialProd.portfolio = cell.Text;
                                break;*/
                            default:
                                break;
                        }
                    }
                    dataRow[cell.Start.Column - 1] = cell.Text;
                }
                dataTable.Rows.Add(dataRow);
            }
            return dataTable;
        }

        private string parseValues(string value, Type targetType, out object converted)
        {
            converted = null;

            //TODO

            throw new Exception("Unexpected type");
        }

    }
}