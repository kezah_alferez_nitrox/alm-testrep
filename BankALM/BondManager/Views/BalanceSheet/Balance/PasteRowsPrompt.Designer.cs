﻿using BondManager.Resources.Language;

namespace BondManager.Views.BalanceSheet.Balance
{
    partial class PasteRowsPrompt
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.cancelButton = new System.Windows.Forms.Button();
            this.overwriteButton = new System.Windows.Forms.Button();
            this.insertButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(83, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(245, 30);
            this.label1.TabIndex = 0;
            this.label1.Text = Labels.PasteRowsPrompt_InitializeComponent_Do_you_want_to_insert_copied_rows_as_new_rows_or_overwrite_existing_ones__;
            // 
            // cancelButton
            // 
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point(252, 51);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Padding = new System.Windows.Forms.Padding(5);
            this.cancelButton.Size = new System.Drawing.Size(114, 32);
            this.cancelButton.TabIndex = 1;
            this.cancelButton.Text = Labels.PasteRowsPrompt_InitializeComponent_Cancel;
            this.cancelButton.UseVisualStyleBackColor = true;
            // 
            // overwriteButton
            // 
            this.overwriteButton.DialogResult = System.Windows.Forms.DialogResult.No;
            this.overwriteButton.Location = new System.Drawing.Point(132, 51);
            this.overwriteButton.Name = "overwriteButton";
            this.overwriteButton.Padding = new System.Windows.Forms.Padding(5);
            this.overwriteButton.Size = new System.Drawing.Size(114, 32);
            this.overwriteButton.TabIndex = 2;
            this.overwriteButton.Text = Labels.PasteRowsPrompt_InitializeComponent_Overwrite;
            this.overwriteButton.UseVisualStyleBackColor = true;
            // 
            // insertButton
            // 
            this.insertButton.DialogResult = System.Windows.Forms.DialogResult.Yes;
            this.insertButton.Location = new System.Drawing.Point(12, 51);
            this.insertButton.Name = "insertButton";
            this.insertButton.Padding = new System.Windows.Forms.Padding(5);
            this.insertButton.Size = new System.Drawing.Size(114, 32);
            this.insertButton.TabIndex = 3;
            this.insertButton.Text = Labels.PasteRowsPrompt_InitializeComponent_Insert;
            this.insertButton.UseVisualStyleBackColor = true;
            // 
            // PasteRowsPrompt
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(377, 96);
            this.ControlBox = false;
            this.Controls.Add(this.insertButton);
            this.Controls.Add(this.overwriteButton);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "PasteRowsPrompt";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = Labels.PasteRowsPrompt_InitializeComponent_Paste_Rows;
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button overwriteButton;
        private System.Windows.Forms.Button insertButton;
    }
}