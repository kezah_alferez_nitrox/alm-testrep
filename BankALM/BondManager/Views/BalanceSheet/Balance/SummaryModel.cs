using BondManager.Domain;

namespace BondManager.Views.BalanceSheet.Balance
{
    public enum SummaryType
    {
        None,
        Normal,
        Header,
        Footer,
        MasterFooter
    }

    public class SummaryModel
    {
        private string description;
        private double value;
        private string openingBalance;
        private string analysisCategory;

        private SummaryType type;
        private Category category;

        public SummaryModel(string description, double value, string openingBalance, string analysisCategory, Category category)
        {
            this.description = description;
            this.value = value;
            this.openingBalance = openingBalance;
            this.analysisCategory = analysisCategory;
            this.Category = category;
        }

        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        public double Value
        {
            get { return value; }
            set { this.value = value; }
        }

        public SummaryType Type
        {
            get { return type; }
            set { type = value; }
        }

        public string OpeningBalance
        {
            get { return openingBalance; }
            set { openingBalance = value; }
        }

        public string AnalysisCategory
        {
            get { return Category == null ? null : Category.AnalysisCategory; }
            set { if(Category != null) Category.AnalysisCategory = value; }
        }

        public Category Category
        {
            get { return category; }
            set { category = value; }
        }
    }
}