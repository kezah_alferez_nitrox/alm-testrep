﻿using BondManager.Resources.Language;

namespace BondManager.Views.BalanceSheet.Balance
{
    partial class AddProductsDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.rowCountNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.comboBoxBondFinancialType = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cancelButton = new System.Windows.Forms.Button();
            this.okButton = new System.Windows.Forms.Button();
            this.panel = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.rowCountNumericUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(179, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "How many rows do you want to add ";
            // 
            // rowCountNumericUpDown
            // 
            this.rowCountNumericUpDown.Location = new System.Drawing.Point(195, 12);
            this.rowCountNumericUpDown.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.rowCountNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.rowCountNumericUpDown.Name = "rowCountNumericUpDown";
            this.rowCountNumericUpDown.Size = new System.Drawing.Size(79, 20);
            this.rowCountNumericUpDown.TabIndex = 7;
            this.rowCountNumericUpDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.rowCountNumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // comboBoxBondFinancialType
            // 
            this.comboBoxBondFinancialType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxBondFinancialType.FormattingEnabled = true;
            this.comboBoxBondFinancialType.Location = new System.Drawing.Point(107, 38);
            this.comboBoxBondFinancialType.Name = "comboBoxBondFinancialType";
            this.comboBoxBondFinancialType.Size = new System.Drawing.Size(167, 21);
            this.comboBoxBondFinancialType.TabIndex = 8;
            this.comboBoxBondFinancialType.SelectedIndexChanged += new System.EventHandler(this.FinancialTypeChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Financial Type";
            // 
            // cancelButton
            // 
            this.cancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point(280, 116);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 103;
            this.cancelButton.Text = global::BondManager.Resources.Language.Labels.AddBondsDialog_InitializeComponent_Cancel;
            this.cancelButton.UseVisualStyleBackColor = true;
            // 
            // okButton
            // 
            this.okButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okButton.Location = new System.Drawing.Point(199, 116);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(75, 23);
            this.okButton.TabIndex = 102;
            this.okButton.Text = global::BondManager.Resources.Language.Labels.AddBondsDialog_InitializeComponent_OK;
            this.okButton.UseVisualStyleBackColor = true;
            
            // 
            // panel
            // 
            this.panel.Location = new System.Drawing.Point(13, 65);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(342, 45);
            this.panel.TabIndex = 104;
            // 
            // AddProductsDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(367, 151);
            this.Controls.Add(this.panel);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.okButton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.rowCountNumericUpDown);
            this.Controls.Add(this.comboBoxBondFinancialType);
            this.Controls.Add(this.label2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AddProductsDialog";
            this.Text = "Add products";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Validate);
            this.Load += new System.EventHandler(this.FormLoad);
            ((System.ComponentModel.ISupportInitialize)(this.rowCountNumericUpDown)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown rowCountNumericUpDown;
        private System.Windows.Forms.ComboBox comboBoxBondFinancialType;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Panel panel;
    }
}