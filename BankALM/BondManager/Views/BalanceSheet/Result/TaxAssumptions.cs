using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using ALMSCommon.Forms;
using BondManager.Core;
using BondManager.Domain;

namespace BondManager.Views.BalanceSheet.Result
{
    public partial class TaxAssumptions : SessionForm
    {
        public TaxAssumptions()
        {
            InitializeComponent();
        }

        private void TaxAssumptions_Load(object sender, EventArgs e)
        {
            StandardRatetextBox.Value = (decimal)Param.GetStandardTax(this.Session);
            LongTermtextBox.Value = (decimal)Param.GetLongTermTax(this.Session);
            SpecialtextBox.Value = (decimal)Param.SpecialTax(this.Session);
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            Param.SetStandardTax(this.Session, (double)this.StandardRatetextBox.Value);
            Param.SetLongTermTax(this.Session, (double)this.LongTermtextBox.Value);
            Param.SpecialTax(this.Session, (double)this.SpecialtextBox.Value);

            this.SaveChangesInSession();
            //PersistenceSession.Flush();
        }
    }
}