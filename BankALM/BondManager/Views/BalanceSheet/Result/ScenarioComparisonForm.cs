﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ALMSCommon;
using BondManager.Calculations;
using BondManager.Core;
using BondManager.Domain;
using BondManager.Resources.Language;
using Eu.AlterSystems.ASNetLib.Core;
using ReportType = BondManager.Calculations.ReportType;

namespace BondManager.Views.BalanceSheet.Result
{
    public partial class ScenarioComparisonForm : Form
    {
        private readonly Form parent;
        private readonly Dictionary<ReportType, Dictionary<ReportAggregationType, Dictionary<Scenario, ResultModel>>> allResultModels;
        private readonly int[] rowsId;
        private IList<object[]> allData;

        public ScenarioComparisonForm(Form parent, Dictionary<ReportType, Dictionary<ReportAggregationType, Dictionary<Scenario, ResultModel>>> allResultModels, int[] rowsId)
        {
            this.parent = parent;
            this.allResultModels = allResultModels;
            this.rowsId = rowsId;
            this.InitializeComponent();

            this.grid.ReadOnly = true;
            this.grid.AllowUserToAddRows = false;
            this.grid.RowHeadersVisible = false;
            this.grid.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;

            this.Load += this.ScenarioComparisonFormLoad;

            this.KeyPreview = true;
            this.KeyDown += this.ScenarioComparisonFormKeyDown;
        }

        private void ScenarioComparisonFormKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        private void ScenarioComparisonFormLoad(object sender, EventArgs e)
        {
            IList reportTypes = EnumHelper.ToList(typeof(ReportType));
            reportTypeComboBox.ValueMember = "Key";
            reportTypeComboBox.DisplayMember = "Value";

            if (!AlmConfiguration.IsTreasuryManagementModule())
            {
                reportTypes.RemoveAt(reportTypes.Count - 1);
            }

            reportTypeComboBox.DataSource = reportTypes;
            reportTypeComboBox.SelectedIndex = 0;

            IList aggregationType = EnumHelper.ToList(typeof(ReportAggregationType));
            aggregationTypeComboBox.ValueMember = "Key";
            aggregationTypeComboBox.DisplayMember = "Value";
            aggregationTypeComboBox.DataSource = aggregationType;
            aggregationTypeComboBox.SelectedIndex = 0;

            //this.RefreshScenarioComparaison();
        }

        private void RefreshScenarioComparaison()
        {
            if (this.reportTypeComboBox.SelectedValue == null || this.aggregationTypeComboBox.SelectedValue == null)
            {
                return;
            }

            try
            {
                ReportType reportType = (ReportType)this.reportTypeComboBox.SelectedValue;
                ReportAggregationType aggregationType = (ReportAggregationType)this.aggregationTypeComboBox.SelectedValue;

                Dictionary<Scenario, ResultModel> resultModels = this.allResultModels[reportType][aggregationType]
                    .OrderBy(r => r.Key.Position)
                    .ToDictionary(r => r.Key, r => r.Value);

                ResultModel resultModel = resultModels.Values.FirstOrDefault();
                if (resultModel == null) return;

                resultModel.GetRows(ReportAnalysisType.Standard);
                ResultRowModel[] resultRowsModel =
                    this.AllRecursive(resultModel.Rows).Where(r => this.rowsId.Contains(r.Id)).ToArray();
                if (resultRowsModel.Length == 0) return;

                bool hasAsset = resultRowsModel.Any(r => r.Category != null && r.Category.BalanceType == BalanceType.Asset);
                StringBuilder builder = new StringBuilder(Labels.ScenarioComparisonForm_RefreshScenarioComparaison_Scenario_Comparison___);
                for (int index = 0; index < resultRowsModel.Length; index++)
                {
                    if (resultRowsModel[index].Category == null || resultRowsModel[index].Category.BalanceType == BalanceType.Portfolio) continue;
                    if (index > 0)
                    {
                        string sign = resultRowsModel[index].Category.BalanceType == BalanceType.Asset ? 
                            "+" : 
                            (hasAsset ? "-" : "+");
                        builder.Append(sign);
                    }

                    if (resultRowsModel[index].Category.Parent != null)
                    {
                        builder.Append(resultRowsModel[index].Category.Parent.Label);
                        builder.Append(".");
                    }
                    builder.Append(resultRowsModel[index].Description);
                }

                this.Text = builder.ToString();
                this.grid.SuspendLayout();
                this.CreateColumns(resultModel.DateIntervals, resultRowsModel.First().Type);
                this.grid.ResumeLayout();

                this.StartFillingData(resultModels);
                this.FillDataComplete();
            }
            finally
            {
                if (parent != null) parent.Cursor = Cursors.Default;
            }
        }

        private void StartFillingData(Dictionary<Scenario, ResultModel> resultModels)
        {
            this.allData = new List<object[]>();

            foreach (Scenario scenario in resultModels.Keys)
            {
                ResultModel resultModel = resultModels[scenario];
                IList<ResultRowModel> resultRowModels = resultModel.GetRows(ReportAnalysisType.Standard);

                ResultRowModel[] resultRowsModel =
                    this.AllRecursive(resultRowModels).Where(r => this.rowsId.Contains(r.Id)).ToArray();
                if (resultRowsModel.Length == 0) continue;

                object[] data = new object[resultModel.ValueColumnsCount + 1];
                data[0] = scenario.Name;

                bool hasAsset = resultRowsModel.Any(r => r.Category != null && r.Category.BalanceType == BalanceType.Asset);

                for (int i = 0; i < resultModel.ValueColumnsCount; i++)
                {
                    double total;
                    if (hasAsset)
                    {
                        double? totalAssets = resultRowsModel.Where(r => r.Category != null && r.Category.BalanceType == BalanceType.Asset).Select(
                            r => (double?)r[i]).Sum();
                        double? totalLiabilities = resultRowsModel.Where(r => r.Category != null && r.Category.BalanceType == BalanceType.Liability).Select(
                            r => (double?)r[i]).Sum();
                        double? totalEquity = resultRowsModel.Where(r => r.Category != null && r.Category.BalanceType == BalanceType.Equity).Select(
                            r => (double?)r[i]).Sum();

                        total = totalAssets.GetValueOrDefault() - totalLiabilities.GetValueOrDefault() - totalEquity.GetValueOrDefault();
                    }
                    else
                    {
                        total = resultRowsModel.Select(r => (double?)r[i]).Sum().GetValueOrDefault();
                    }
                    data[i + 1] = total;
                }

                this.allData.Add(data);
            }
        }

        private void FillDataComplete()
        {
            this.grid.Rows.Clear();
            foreach (object[] data in this.allData)
            {
                this.grid.Rows.Add(data);
            }
        }

        private IEnumerable<ResultRowModel> AllRecursive(IEnumerable<ResultRowModel> resultRowModels)
        {
            foreach (ResultRowModel row in resultRowModels)
            {
                foreach (ResultRowModel childRow in this.AllRecursive(row.Children))
                    yield return childRow;

                yield return row;
            }
        }

        private void CreateColumns(IList<DateInterval> dateIntervals, FormattingType type)
        {
            if (dateIntervals == null) return;

            this.grid.Columns.Clear();

            this.grid.AutoSize = false;

            List<DataGridViewColumn> columns = new List<DataGridViewColumn>();
            DataGridViewTextBoxColumn column = new DataGridViewTextBoxColumn();
            column.HeaderText = "Scenario";
            column.DefaultCellStyle.BackColor = Color.LemonChiffon;
            column.Width = 150;
            column.Frozen = true;
            column.SortMode = DataGridViewColumnSortMode.NotSortable;
            columns.Add(column);

            for (int index = 0; index < dateIntervals.Count; index++)
            {
                DateInterval interval = dateIntervals[index];
                var headerText = interval.ToString(index==0);

                column = new DataGridViewTextBoxColumn();
                column.Name = "Value" + interval.Start + "-" + interval.End;
                column.HeaderText = headerText;
                column.AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                column.Width = 100;
                column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                column.DefaultCellStyle.Format = type == FormattingType.Percentage
                                                     ? Constants.PERCENTAGE_CELL_FORMAT
                                                     : Constants.ResultNumericFormat;
                column.FillWeight = 10;
                column.SortMode = DataGridViewColumnSortMode.NotSortable;
                columns.Add(column);
            }

            this.grid.Columns.AddRange(columns.ToArray());

            this.grid.AutoResizeColumns();
        }

        private void ScenarioComparisonFormKeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.C && e.Control)
            {
                CopyGridToClipboard();
            }
        }

        private void CopyGridToClipboard()
        {
            if (this.grid.GetCellCount(DataGridViewElementStates.Selected) > 0)
            {
                this.grid.CopyToClipboardAsExcel();
            }
        }

        private void SelectReportType(object sender, EventArgs e)
        {
            this.RefreshScenarioComparaison();
        }

        private void SelectShow(object sender, EventArgs e)
        {
            this.RefreshScenarioComparaison();
        }
    }
}