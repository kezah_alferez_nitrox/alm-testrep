﻿using ALMSCommon.Controls;
using ALMSCommon.Controls.TreeGridView;
using BondManager.Resources.Language;

namespace BondManager.Views.BalanceSheet.Result
{
    partial class ScenarioComparisonForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.reportTypeComboBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.aggregationTypeComboBox = new System.Windows.Forms.ComboBox();
            this.headerPanel = new System.Windows.Forms.Panel();
            this.grid = new DataGridViewEx();
            this.headerPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grid)).BeginInit();
            this.SuspendLayout();
            // 
            // reportTypeComboBox
            // 
            this.reportTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.reportTypeComboBox.FormattingEnabled = true;
            this.reportTypeComboBox.Location = new System.Drawing.Point(82, 5);
            this.reportTypeComboBox.Name = "reportTypeComboBox";
            this.reportTypeComboBox.Size = new System.Drawing.Size(205, 21);
            this.reportTypeComboBox.TabIndex = 1;
            this.reportTypeComboBox.SelectedIndexChanged += new System.EventHandler(this.SelectReportType);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Report Type :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(309, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Show :";
            // 
            // aggregationTypeComboBox
            // 
            this.aggregationTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.aggregationTypeComboBox.FormattingEnabled = true;
            this.aggregationTypeComboBox.Location = new System.Drawing.Point(355, 5);
            this.aggregationTypeComboBox.Name = "aggregationTypeComboBox";
            this.aggregationTypeComboBox.Size = new System.Drawing.Size(205, 21);
            this.aggregationTypeComboBox.TabIndex = 3;
            this.aggregationTypeComboBox.SelectedIndexChanged += new System.EventHandler(this.SelectShow);
            // 
            // headerPanel
            // 
            this.headerPanel.Controls.Add(this.reportTypeComboBox);
            this.headerPanel.Controls.Add(this.label2);
            this.headerPanel.Controls.Add(this.label1);
            this.headerPanel.Controls.Add(this.aggregationTypeComboBox);
            this.headerPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.headerPanel.Location = new System.Drawing.Point(0, 0);
            this.headerPanel.Name = "headerPanel";
            this.headerPanel.Size = new System.Drawing.Size(1053, 31);
            this.headerPanel.TabIndex = 5;
            // 
            // grid
            // 
            this.grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grid.Location = new System.Drawing.Point(0, 31);
            this.grid.Name = "grid";
            this.grid.Size = new System.Drawing.Size(1053, 412);
            this.grid.TabIndex = 6;
            // 
            // ScenarioComparisonForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1053, 443);
            this.Controls.Add(this.grid);
            this.Controls.Add(this.headerPanel);
            this.Name = "ScenarioComparisonForm";
            this.Text = Labels.ScenarioComparisonForm_InitializeComponent_Scenario_Comparison;
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ScenarioComparisonFormKeyUp);
            this.headerPanel.ResumeLayout(false);
            this.headerPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox reportTypeComboBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox aggregationTypeComboBox;
        private System.Windows.Forms.Panel headerPanel;
        private DataGridViewEx grid;
    }
}