using System;
using System.Linq;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using ALMSCommon.Controls.TreeGridView;
using BankALM.Infrastructure.Data;
using BondManager.Calculations;
using BondManager.Core;
using BondManager.Domain;
using BondManager.Resources.Language;
using NHibernate;
using ReportType = BondManager.Calculations.ReportType;

namespace BondManager.Views.BalanceSheet.Result
{
    public class ResultGridController
    {
        private readonly List<DataGridViewColumn> columns = new List<DataGridViewColumn>();
        private readonly TreeGridView grid;

        public ResultGridController(TreeGridView grid)
        {
            this.grid = grid;
            grid.ReadOnly = true;
            grid.AutoGenerateColumns = false;
            grid.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.None; // .AllCells;
            grid.AllowUserToAddRows = false;
            grid.AllowUserToDeleteRows = false;
            grid.AllowUserToResizeRows = false;
            grid.RowTemplate.Height = Constants.GRID_ROW_HEIGHT;
            grid.ColumnHeadersVisible = false;
            this.grid.MouseDown += this.GridClick;
        }

        private void GridClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                DataGridView.HitTestInfo hit = this.grid.HitTest(e.X, e.Y);
                if (hit.ColumnIndex < 0 || hit.RowIndex < 0) return;

                // grid.ClearSelection();
                DataGridViewCell cell = this.grid[hit.ColumnIndex, hit.RowIndex];
                if (cell.Selected) return;

                cell.Selected = true;

                grid.CurrentCell = cell;
            }
        }

        public bool CanShowScenarioComparison(Simulation simulation)
        {
            if (this.grid.CurrentCell == null) return false;

            DataGridViewRow row = this.grid.CurrentCell.OwningRow;
            if (row.Index <= 0) return false;

            ResultRowModel resultRowModel = row.Tag as ResultRowModel;
            if (resultRowModel == null) return false;

            return true;
        }

        public void ShowScenarioComparison(Dictionary<ReportType, Dictionary<ReportAggregationType, Dictionary<Scenario, ResultModel>>> resultModels)
        {
            DataGridViewRow[] rows = this.grid.SelectedCells.Cast<DataGridViewCell>().Select(c => c.OwningRow).Distinct().ToArray();

            if (rows.Length <= 0) return;

            ResultRowModel[] resultRowsModel = rows.Select(r => r.Tag as ResultRowModel).ToArray();
            if (resultRowsModel.Length == 0) return;

            Form form = this.grid.FindForm();
            if (form != null) form.Cursor = Cursors.WaitCursor;
            ScenarioComparisonForm comparisonForm = new ScenarioComparisonForm(form, resultModels, resultRowsModel.Select(r => r.Id).ToArray());
            comparisonForm.Show(form);
        }

        private void CreateColumns(IList<DateInterval> dateIntervals)
        {
            this.columns.Clear();
            bool ShowDecimal = false;

            using (ISession session = SessionManager.OpenSession())
            {
                if (Param.GetShowDecimalValueInResult(session))
                    ShowDecimal = true;
            }

            DataGridViewTextBoxColumn modelTypeColumn = new DataGridViewTextBoxColumn();
            modelTypeColumn.Visible = false;
            modelTypeColumn.Frozen = true;
            this.columns.Add(modelTypeColumn);

            // 
            // summaryDescriptionDataGridViewTextBoxColumn
            // 
            TreeGridColumn descriptionDataGridViewTextBoxColumn = new TreeGridColumn();
            descriptionDataGridViewTextBoxColumn.HeaderText = Labels.ResultGridController_CreateColumns_Description;
            descriptionDataGridViewTextBoxColumn.Name = "Description";
            descriptionDataGridViewTextBoxColumn.Width = 200;
            descriptionDataGridViewTextBoxColumn.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            descriptionDataGridViewTextBoxColumn.Frozen = true;

            this.columns.Add(descriptionDataGridViewTextBoxColumn);

            for (int i = 0; i < dateIntervals.Count; i++)
            {
                DateInterval interval = dateIntervals[i];

                // 
                // summaryValueDataGridViewTextBoxColumn
                // 
                DataGridViewTextBoxColumn valueDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
                valueDataGridViewTextBoxColumn.Name = "Value" + interval.Start + "-" + interval.End;
                valueDataGridViewTextBoxColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.None; //.AllCells;
                valueDataGridViewTextBoxColumn.FillWeight = 1;
                valueDataGridViewTextBoxColumn.Width = 100;
                valueDataGridViewTextBoxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                if (ShowDecimal)
                {
                    valueDataGridViewTextBoxColumn.DefaultCellStyle.Format = Constants.ResultNumericFormat;
                }
                else
                {
                    valueDataGridViewTextBoxColumn.DefaultCellStyle.Format = Constants.ResultNumericFormatNoDec;
                }
                valueDataGridViewTextBoxColumn.DefaultCellStyle.NullValue = "";
                if (i == 0) valueDataGridViewTextBoxColumn.Frozen = true;
                this.columns.Add(valueDataGridViewTextBoxColumn);
            }

            this.grid.Columns.Clear();
            this.grid.Columns.AddRange(this.columns.ToArray());
       }

        private void FormatRows()
        {
            Font boldFont = new Font(this.grid.Font, FontStyle.Bold);

            for (int rowIndex = 0; rowIndex < this.grid.Rows.Count; rowIndex++)
            {
                FormattingType type = (FormattingType)this.grid[0, rowIndex].Value;
                DataGridViewRow row = this.grid.Rows[rowIndex];

                switch (type)
                {
                    case FormattingType.MasterHeader:
                        row.DefaultCellStyle.BackColor = Constants.Colors.TableHeader2;
                        row.DefaultCellStyle.Font = boldFont;
                        break;
                    case FormattingType.PeriodHeader:
                        row.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                        row.DefaultCellStyle.BackColor = Constants.Colors.TableHeader2;
                        row.DefaultCellStyle.Font = boldFont;
                        break;
                    case FormattingType.Header:
                        row.DefaultCellStyle.BackColor = Constants.Colors.TableHeader1;
                        row.DefaultCellStyle.Font = boldFont;
                        break;
                    case FormattingType.WhiteHeader:
                        row.DefaultCellStyle.Font = boldFont;
                        break;
                    case FormattingType.Footer:
                        row.DefaultCellStyle.BackColor = Constants.Colors.TableFooter1;
                        row.DefaultCellStyle.Font = boldFont;
                        break;
                    case FormattingType.MasterFooter:
                        row.DefaultCellStyle.BackColor = Constants.Colors.TableFooter2;
                        row.DefaultCellStyle.Font = boldFont;
                        break;
                    case FormattingType.Percentage:

                        row.DefaultCellStyle.BackColor = Constants.Colors.TableHeader1;
                        row.DefaultCellStyle.Font = boldFont;
                        row.DefaultCellStyle.Format = "P";
                        break;
                    // default: // SummaryType.Standard, SummaryType.None                       
                }
            }
        }

        public void Refresh(ResultModel resultModel, ReportAnalysisType analysisType)
        {
            this.CreateColumns(resultModel.DateIntervals);

            this.grid.Nodes.Clear();

            IList<ResultRowModel> gridData = resultModel.GetRows(analysisType);

            this.grid.SuspendLayout();

            this.AddNodes(gridData, this.grid.Nodes);

            this.ExpandVisibleNodes(this.grid.Nodes);

            if (this.grid.Nodes.Count >= 1)
            {
                this.grid.Nodes[0].Frozen = true;
                this.grid.Nodes[0].Height *= 2;
            }

            this.FormatRows();
            
            //this.grid.ResumeLayout(true);
            
            for (int i = 0; i < this.grid.Columns.Count; i++)
            {
                int colWidth = this.grid.Columns[i].Width;
                this.grid.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                this.grid.Columns[i].Width = colWidth;
            }
            grid.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.grid.ResumeLayout(true);
        }

        private void ExpandVisibleNodes(IEnumerable<TreeGridNode> nodes)
        {
            foreach (TreeGridNode node in nodes)
            {
                ResultRowModel resultRowModel = node.Tag as ResultRowModel;
                if (resultRowModel == null) continue;
                Category category = resultRowModel.Category;

                if (true)
                {
                    node.Expand();

                    this.ExpandVisibleNodes(node.Nodes);
                }
            }
        }

        private void AddNodes(IEnumerable<ResultRowModel> rows, TreeGridNodeCollection nodes)
        {
            int i = 0;

            /*
            bool followConfigSettings = false;
            
            using (ISession session = SessionManager.OpenSession())
            {
                followConfigSettings = Param.GetFollowConfigSettingInResult(session);
            }
            */
            foreach (ResultRowModel model in rows)
            {
                bool hasRowValues = false;

                if (model.RowValues != null)// && !followConfigSettings)
                {
                    for (int y = 2; y < model.RowValues.Count(); y++)
                    {
                        if (model.RowValues[y] == null || model.RowValues[y].ToString() == "0") continue;
                        hasRowValues = true;
                        break;
                    }
                }

                bool hide = model.Category != null && (
                            (model.ResultSection == ResultSection.BookValue && !model.Category.isVisible.GetValueOrDefault(false)) ||
                            (model.ResultSection == ResultSection.Income && !model.Category.IsVisibleChecked.GetValueOrDefault(false)) ||
                            (model.ResultSection == ResultSection.Yield && !model.Category.IsVisibleChecked.GetValueOrDefault(false)));

                if (!hide || hasRowValues)
                {
                    TreeGridNode node = nodes.Add(model.RowValues);
                    if (i != 0 || nodes != this.grid.Nodes)
                    {
                        node.ImageIndex = model.Children.Count > 0 ? 0 : 1;
                    }
                    node.Tag = model;

                    ValidateCashAdj(node, model);

                    this.AddNodes(model.Children, node.Nodes);
                }

                i++;
            }
        }

        private void ValidateCashAdj(TreeGridNode node, ResultRowModel model)
        {
            bool isBeta = MainForm.IsBeta;
#if DEBUG
            isBeta = true;
#endif
            if (isBeta && model.Category != null && model.Category.Type == CategoryType.CashAdj)
            {
                for (int i = 3; i < node.Cells.Count-1; i++)
                {
                    double value1 = (double)node.Cells[i - 1].Value;
                    double value2 = (double)node.Cells[i].Value;                    
                    if (Math.Abs(value1 - value2)>=0.01)
                    {
                        node.Cells[i].Style.BackColor = Color.Tomato;
                        node.Cells[i].Style.Font = new Font(grid.Font, FontStyle.Bold);
                    }
                }
            }
        }

        public void ClipboardCopy()
        {
            if (this.grid.GetCellCount(DataGridViewElementStates.Selected) > 0)
            {
                this.grid.CopyToClipboardAsExcel();
            }
        }

        public void InvalidateResults()
        {
            this.grid.Nodes.Clear();
        }
    }
}
