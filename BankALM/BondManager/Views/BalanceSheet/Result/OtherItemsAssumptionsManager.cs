using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using ALMSCommon;
using ALMSCommon.Controls;
using BondManager.BusinessLogic;
using BondManager.Core;
using BondManager.Domain;
using BondManager.Resources.Language;
using NHibernate.Linq;

namespace BondManager.Views.BalanceSheet.Result
{
    public partial class OtherItemsAssumptionsManager : SessionForm
    {
        private DataGridViewColumn[] columns;
        private DataGridViewTextBoxColumn descriptionDataGridViewTextBoxColumn;
        private AutoCompleteColumn subTotalGroupDataGridViewTextBoxColumn;
        private string[] variableAndVectorNames;
        private string[] variableVectorAndDynamicVariableNames ;
        private AutoCompleteColumn vectorAutoCompleteColumn;

        public OtherItemsAssumptionsManager()
        {
            this.InitializeComponent();

            this.grid.AutoGenerateColumns = false;
            this.grid.AllowUserToAddRows = false;
            this.grid.AllowUserToDeleteRows = false;
            this.grid.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
            this.grid.RowTemplate.Height = Constants.GRID_ROW_HEIGHT;
        }

        private SortableBindingList<OtherItemAssum> OiaList
        {
            get { return this.grid.DataSource as SortableBindingList<OtherItemAssum>; }
            set { this.grid.DataSource = value; }
        }

        private void InitColumns()
        {
            this.variableAndVectorNames = DomainTools.GetAllVariablesAndVectorNames(this.Session);
            this.variableVectorAndDynamicVariableNames = this.variableAndVectorNames.Concat(this.Session.Query<DynamicVariable>().Select(v => v.Name)).ToArray();

            this.descriptionDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            this.vectorAutoCompleteColumn = new AutoCompleteColumn();
            this.subTotalGroupDataGridViewTextBoxColumn = new AutoCompleteColumn();

            //descriptionDataGridViewTextBoxColumn 
            this.descriptionDataGridViewTextBoxColumn.DataPropertyName = "Descrip";
            this.descriptionDataGridViewTextBoxColumn.HeaderText = Labels.OtherItemsAssumptions_InitColumns_Description;
            this.descriptionDataGridViewTextBoxColumn.Name = "Description";
            this.descriptionDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.descriptionDataGridViewTextBoxColumn.Width = 150;

            //vectorAutoCompleteColumn
            this.vectorAutoCompleteColumn.DataPropertyName = "Vect";
            this.vectorAutoCompleteColumn.HeaderText = Labels.OtherItemsAssumptions_InitColumns_Vector;
            this.vectorAutoCompleteColumn.Name = "Vector";
            this.vectorAutoCompleteColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.vectorAutoCompleteColumn.Width = 100;
            this.vectorAutoCompleteColumn.ValueList = this.variableVectorAndDynamicVariableNames;

            //subTotalGroupDataGridViewTextBoxColumn
            this.subTotalGroupDataGridViewTextBoxColumn.DataPropertyName = "SubtotalGroup";
            this.subTotalGroupDataGridViewTextBoxColumn.HeaderText =
                Labels.OtherItemsAssumptions_InitColumns_SUBTOTAL_on_Group;
            this.subTotalGroupDataGridViewTextBoxColumn.Name = "SUBTOTAL on Group";
            this.subTotalGroupDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.subTotalGroupDataGridViewTextBoxColumn.Width = 150;
            this.subTotalGroupDataGridViewTextBoxColumn.ValueList = new[]
                                                                        {
                                                                            OtherItemAssum.CostGroup,
                                                                            OtherItemAssum.CommisionsAndOthersGroup,
                                                                            OtherItemAssum.ImpairmentsRisk
                                                                        };

            this.columns = new DataGridViewColumn[]
                               {
                                   this.descriptionDataGridViewTextBoxColumn,
                                   this.vectorAutoCompleteColumn,
                                   this.subTotalGroupDataGridViewTextBoxColumn
                               };

            this.grid.Columns.Clear();
            this.grid.Columns.AddRange(this.columns);
        }

        private void OtherItemsAssumptionsLoad(object sender, EventArgs e)
        {
            if (!this.DesignMode && !Tools.InDesignMode)
            {
                this.InitColumns();
                this.LoadData();
            }
        }

        private void AddNewItems(int itemsCount)
        {
            for (int i = 0; i < itemsCount; i++)
            {
                OtherItemAssum oia = new OtherItemAssum();
                oia.Descrip = string.Empty;
                oia.Position = this.OiaList.Count + i;
                oia.IsVisibleChecked = true;
                oia.Create(this.Session);
                this.OiaList.Add(oia);
            }
        }

        private void LoadData()
        {
            SortableBindingList<OtherItemAssum> gridData = new SortableBindingList<OtherItemAssum>(true);

            OtherItemAssum[] all = OtherItemAssum.FindAll(this.Session);
            foreach (OtherItemAssum oia in all.OrderBy(oia => oia.Position))
            {
                gridData.Add(oia);
            }
            this.OiaList = gridData;


            for (int i = 0; i < this.OiaList.Count; i++)
            {
                this.OiaList[i].Position = i;
            }

            this.UpdateUpDownButtons();

            
        }

        private void AddButtonClick(object sender, EventArgs e)
        {
            AddNewRowsPrompt addNewRowsPrompt = new AddNewRowsPrompt();
            addNewRowsPrompt.Text = Labels.OtherItemsAssumptions_addButton_Click_Other_Items_Assumption;
            if (addNewRowsPrompt.ShowDialog() == DialogResult.OK)
            {
                this.AddNewItems(addNewRowsPrompt.RowCount);
            }
        }

        private void OkButtonClick(object sender, EventArgs e)
        {
            this.Session.Flush();
        }

        private void DeleteToolStripMenuItemClick(object sender, EventArgs e)
        {
            IEnumerable<OtherItemAssum> toDelete = this.grid.SelectedRows.
                OfType<DataGridViewRow>().
                Select(r => r.DataBoundItem).
                OfType<OtherItemAssum>();
            string cannotDeleteMessage = "";
            foreach (OtherItemAssum oia in toDelete)
            {
                if (oia.CannotDelete)
                {
                    cannotDeleteMessage += oia.Descrip + "\n";
                }
                else
                {
                    this.OiaList.Remove(oia);
                    oia.Delete(this.Session);
                }
            }
            if (cannotDeleteMessage.Length>0) Dialogs.Warning("Cannot delete all OIA","You cannont delete the folowing item assumptions :\n"+cannotDeleteMessage);
            for (int i = 0; i < this.OiaList.Count; i++)
            {
                this.OiaList[i].Position = i;
            }
        }

        private void GridDataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            e.ThrowException = false;
            e.Cancel = true;
        }

        private void UpButtonClick(object sender, EventArgs e)
        {
            int index = this.GetSelectedIndex();
            if (index <= 0) return;
            this.SwitchRows(index, index - 1);
        }

        private void SwitchRows(int src, int dest)
        {
            this.grid.EndEdit();

            OtherItemAssum oia1 = this.OiaList[src];
            OtherItemAssum oia2 = this.OiaList[dest];
            oia1.Position = dest;
            oia2.Position = src;

            this.OiaList = new SortableBindingList<OtherItemAssum>(this.OiaList.OrderBy(oia => oia.Position).ToList());

            if (this.grid.CurrentCell != null)
            {
                this.grid.CurrentCell = this.grid[this.grid.CurrentCell.ColumnIndex, dest];
            }

            this.UpdateUpDownButtons();
        }

        private void DownButtonClick(object sender, EventArgs e)
        {
            int index = this.GetSelectedIndex();
            if (index >= this.grid.Rows.Count - 1) return;
            this.SwitchRows(index, index + 1);
        }

        private void UpdateUpDownButtons()
        {
            if (this.grid.Rows.Count == 0)
            {
                this.upButton.Enabled = this.downButton.Enabled = false;
                return;
            }

            int selectedIndex = this.GetSelectedIndex();
            this.upButton.Enabled = selectedIndex > 0;
            this.downButton.Enabled = selectedIndex < this.grid.Rows.Count - 1;
        }

        private int GetSelectedIndex()
        {
            return this.grid.CurrentRow != null ? this.grid.CurrentRow.Index : -1;
        }

        private void GridSelectionChanged(object sender, EventArgs e)
        {
            this.UpdateUpDownButtons();
        }

        private void ContextMenuStripOpening(object sender, CancelEventArgs e)
        {
            this.deleteToolStripMenuItem.Enabled = this.grid.SelectedRows.Count > 0;
        }

        private void grid_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            OtherItemAssum oia = grid.Rows[e.RowIndex].DataBoundItem as OtherItemAssum;

            if (oia != null && grid.CurrentCell.ColumnIndex == 0 && oia.CannotDelete)
            {
                e.Cancel = true;
            }

        }

        








    }
}