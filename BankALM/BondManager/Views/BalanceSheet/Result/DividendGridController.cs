using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using ALMSCommon;
using ALMSCommon.Controls;
using BondManager.BusinessLogic;
using BondManager.Domain;
using BondManager.Resources.Language;
using NHibernate;

namespace BondManager.Views.BalanceSheet.Result
{
    public class DividendGridController
    {
        private ISession session;

        private readonly DataGridView grid;
        private AutoCompleteColumn amountDataGridViewTextBoxColumn;

        private DataGridViewColumn[] columns;
        private DataGridViewTextBoxColumn minimumDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn;
        private string[] vectorNames;

        public DividendGridController(DataGridView Grid, ISession session)
        {
            this.grid = Grid;
            this.session = session;
            this.grid.AutoGenerateColumns = false;
            this.grid.AllowUserToAddRows = false;
            this.grid.AllowUserToDeleteRows = false;

            this.grid.RowTemplate.Height = Constants.GRID_ROW_HEIGHT;

            this.InitColumns();

        }

        public SortableBindingList<Dividend> DividendList
        {
            get { return this.grid.DataSource as SortableBindingList<Dividend>; }
            set { this.grid.DataSource = value; }
        }

        private void InitColumns()
        {
            this.vectorNames = DomainTools.GetAllVectorNames();

            this.nameDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            this.amountDataGridViewTextBoxColumn = new AutoCompleteColumn();
            this.minimumDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();

            //
            //nameDataGridViewTextBoxColumn
            //
            this.nameDataGridViewTextBoxColumn.DataPropertyName =  "Name";
            this.nameDataGridViewTextBoxColumn.Name = "Name";
            this.nameDataGridViewTextBoxColumn.HeaderText = Labels.DividendGridController_InitColumns_Name;
            this.nameDataGridViewTextBoxColumn.Width = 100;

            //
            //amountDataGridViewTextBoxColumn
            //
            this.amountDataGridViewTextBoxColumn.DataPropertyName = "Amount";
            this.amountDataGridViewTextBoxColumn.Name = "Amount";
            this.amountDataGridViewTextBoxColumn.HeaderText = Labels.DividendGridController_InitColumns_Value__vector_or_percentage_;
            this.amountDataGridViewTextBoxColumn.Width = 200;
            this.amountDataGridViewTextBoxColumn.ValueList = this.vectorNames;

            //
            //minimumDataGridViewTextBoxColumn
            //
            this.minimumDataGridViewTextBoxColumn.DataPropertyName = "Minimum";
            this.minimumDataGridViewTextBoxColumn.Name = "Minimum";
            this.minimumDataGridViewTextBoxColumn.DefaultCellStyle.Format = Constants.DURATION_CELL_FORMAT;
            this.minimumDataGridViewTextBoxColumn.HeaderText = Labels.DividendGridController_InitColumns_Minimum;
            this.minimumDataGridViewTextBoxColumn.Width = 100;
            this.minimumDataGridViewTextBoxColumn.Visible = false;

            this.columns = new DataGridViewColumn[]
            {
                this.nameDataGridViewTextBoxColumn,
                this.amountDataGridViewTextBoxColumn,
                this.minimumDataGridViewTextBoxColumn
            };
            this.grid.Columns.Clear();
            this.grid.Columns.AddRange(this.columns);

        }

        public void Refresh()
        {
            this.InitColumns();

            Dividend[] dividends = Dividend.FindAll(this.session);

            this.DividendList = new SortableBindingList<Dividend>(new List<Dividend>(dividends));

            this.grid.DataSource = this.DividendList;

            //for (int i = 0; i < this.DividendList.Count; i++)
            //{
            //    this.grid.Rows[i].Cells[this.nameDataGridViewTextBoxColumn.Index].Value = gridData[i].Name;
            //    string _amount = gridData[i].Amount;
            //    this.grid.Rows[i].Cells[this.amountDataGridViewTextBoxColumn.Index].Value = _amount;

            //    double _amountdec;
            //    if (double.TryParse(_amount, out _amountdec))
            //    {
            //        this.grid.Rows[i].Cells[this.minimumDataGridViewTextBoxColumn.Index].ReadOnly = false;
            //    }
            //    else
            //    {
            //        this.grid.Rows[i].Cells[this.minimumDataGridViewTextBoxColumn.Index].ReadOnly = true;
            //    }
            //    this.grid.Rows[i].Cells[this.minimumDataGridViewTextBoxColumn.Index].Value = gridData[i].Minimum;

            //}
        }

        public void SaveDividend()
        {
            //if (this.grid.Rows.Count > 0)
            //{
            //    Dividend[] dividends = Dividend.FindAll();
            //    foreach (DataGridViewRow _row in this.grid.Rows)
            //    {
            //        if (_row.Visible)
            //        {
            //            if (_row.Cells[this.nameDataGridViewTextBoxColumn.Index].Value == null) _row.Cells[this.nameDataGridViewTextBoxColumn.Index].Value = string.Empty;
            //            dividends[_row.Index].Name = _row.Cells[this.nameDataGridViewTextBoxColumn.Index].Value.ToString();
            //            if (_row.Cells[this.amountDataGridViewTextBoxColumn.Index].Value == null) _row.Cells[this.amountDataGridViewTextBoxColumn.Index].Value = string.Empty;
            //            string _amount = _row.Cells[this.amountDataGridViewTextBoxColumn.Index].Value.ToString().TrimEnd('%');
            //            dividends[_row.Index].Amount = _amount;
            //            if (_row.Cells[this.minimumDataGridViewTextBoxColumn.Index].Value != null)
            //                dividends[_row.Index].Minimum = Convert.ToDouble(_row.Cells[this.minimumDataGridViewTextBoxColumn.Index].Value);
            //            else
            //                dividends[_row.Index].Minimum = null;
            //        }
            //    }
            //}
            this.session.Flush();
        }

        public void AddNewItems(int itemsCount)
        {
            for (int i = 0; i < itemsCount; i++)
            {
                Dividend dividend = new Dividend();
                dividend.Create(this.session);
                this.DividendList.Add(dividend);
            }
        }

        public void DeleteSelected()
        {
            Dividend[] dividends = this.grid.SelectedRows.Cast<DataGridViewRow>().Select(r => r.DataBoundItem as Dividend).Where(d => d != null).ToArray();

            foreach (Dividend dividend in dividends)
            {
                this.DividendList.Remove(dividend);
                dividend.Delete(this.session);
            }
        }
    }
}
