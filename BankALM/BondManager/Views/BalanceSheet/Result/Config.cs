using System;
using System.Linq;
using System.Windows.Forms;
using BondManager.Calculations;
using BondManager.Core;
using BondManager.Domain;
using System.Collections.Generic;
using BondManager.Resources.Language;

namespace BondManager.Views.BalanceSheet.Result
{
    public partial class Config : SessionForm
    {
        public Config(ResultModel resultModel, ReportAnalysisType analysisType)
        {
            InitializeComponent();

            categoryTreeView.CanOrganize = false;

            if (resultModel != null)
                this.gridData = resultModel.GetRows(analysisType);
        }

        List<int> _ids = new List<int>();
        Dictionary<int, int> _oiaIDs = new Dictionary<int, int>();
        Dictionary<int, int> _bondIDs = new Dictionary<int, int>();
        Dictionary<int, int> _imparmentID = new Dictionary<int, int>();
        Dictionary<int, bool> _modified = new Dictionary<int, bool>();
        Dictionary<int, bool> _modifiedOIA = new Dictionary<int, bool>();
        Dictionary<int, bool> _modifiedBond = new Dictionary<int, bool>();
        Dictionary<int, bool> _modifiedImparment = new Dictionary<int, bool>();

        Dictionary<int, int> categoryParentIds = new Dictionary<int, int>();

        private bool _refreshed = false;
        private IList<ResultRowModel> gridData = null;

        private void ConfigLoad(object sender, EventArgs e)
        {            
            showDynamicVariablesCheckBox.Checked = Param.GetShowDynamicVariablesInResult(this.Session);
            showDecimalValueCheckBox.Checked = Param.GetShowDecimalValueInResult(this.Session);
            followConfigSettingInResultCheckBox.Checked = Param.GetFollowConfigSettingInResult(this.Session);
            if (categoryTreeView.LoadVisibleNodesForConfig())
            {
                RefreshCBL();
                if (!followConfigSettingInResultCheckBox.Checked)
                    UpdateBalanceCheckBox(categoryTreeView.Nodes[0]);
            }
        }

        private void UpdateBalanceCheckBox(TreeNode nodes)
        {
            if (this.gridData == null) return;

            foreach (TreeNode _node in nodes.Nodes)
            {
                CheckModelID(_node, this.gridData);
                UpdateBalanceCheckBox(_node);
            }
        }

        private void CheckModelID(TreeNode _node, IList<ResultRowModel> iList)
        {
            foreach (ResultRowModel model in iList)
            {
                bool hasRowValues = false;
                if (model.RowValues != null)
                {
                    for (int y = 2; y < model.RowValues.Count(); y++)
                    {
                        if (model.RowValues[y] == null || model.RowValues[y].ToString() == "0") continue;
                        hasRowValues = true;
                        break;
                    }
                }

                if (model.Type == FormattingType.PeriodHeader || !hasRowValues) continue;
                if (model.Category == null && model.Children.Count > 0)
                    CheckModelID(_node, model.Children);

                if (model.Category == null) continue;
                
                if (_node.Tag is Category && (_node.Tag as Category).Id == model.Category.Id && hasRowValues)
                {
                    _node.Parent.Expand();
                    _node.Checked = true;
                    return;
                }

                CheckModelID(_node, model.Children);
            }
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            Param.SetShowDynamicVariablesInResult(this.Session, showDynamicVariablesCheckBox.Checked);
            Param.SetShowDecimalValueInResult(this.Session, showDecimalValueCheckBox.Checked);

            followConfigSettingInResultCheckBox.Checked = true;
            Param.SetFollowConfigSettingInResult(this.Session, followConfigSettingInResultCheckBox.Checked);
            
            //update tree
            categoryTreeView.UpdateVisibles(categoryTreeView.Nodes[0]);

            //update profits & losses

            UpdateProfitsLosses();

            this.SaveChangesInSession();
        }

        private void UpdateProfitsLosses()
        {
            foreach (KeyValuePair<int, bool> _kvp in _modified)
            {
                Category cat = Category.Find(this.Session, _kvp.Key);
                cat.IsVisibleChecked = _kvp.Value;
                cat.Save(this.Session);
            }
            foreach (KeyValuePair<int, bool> _kvp in _modifiedOIA)
            {
                OtherItemAssum oia = OtherItemAssum.FindByID(this.Session, _kvp.Key);
                oia.IsVisibleChecked = _kvp.Value;
                // TODO
                oia.Save(this.Session);
            }
            foreach (KeyValuePair<int, bool> _kvp in _modifiedBond)
            {
                Product product = ProductCache.FindById(_kvp.Key);
                int pos = ProductCache.products.IndexOf(product);
                product.IsVisibleChecked = _kvp.Value;
                // TODO
                //product.Save(this.Session);
                ProductCache.products[pos] = product;
            }
            foreach (KeyValuePair<int, bool> _kvp in _modifiedImparment)
            {
                Category cat = Category.Find(this.Session, _kvp.Key);
                cat.IsVisibleCheckedModel = _kvp.Value;
                // TODO
                cat.Save(this.Session);
            }

            this.SaveChangesInSession();
        }

        private void RefreshCBL()
        {
            ProfitsLossesCheckBoxList.Items.Clear();
            _ids = new List<int>();
            _oiaIDs = new Dictionary<int, int>();
            _modified = new Dictionary<int, bool>();
            _modifiedOIA = new Dictionary<int, bool>();

            foreach (Category cat in Category.FindByBalanceType(this.Session, BalanceType.Asset))
            {
                if (cat.Parent != null)  categoryParentIds[cat.Id] = cat.Parent.Id;

                if (cat.IsVisibleChecked == true)
                {
                    ProfitsLossesCheckBoxList.Items.Add(cat.FullPath(), CheckState.Checked);
                    _ids.Add(cat.Id);
                }
                else
                {
                    ProfitsLossesCheckBoxList.Items.Add(cat.FullPath());
                    _ids.Add(cat.Id);
                }
            }
            ProfitsLossesCheckBoxList.Items.Add(Labels.Config_RefreshCBL_Total_Interest_Income, CheckState.Indeterminate);
            _ids.Add(0);

            foreach (Category cat in Category.FindByBalanceType(this.Session, BalanceType.Liability))
            {
                if (cat.Parent != null) categoryParentIds[cat.Id] = cat.Parent.Id;

                if (cat.IsVisibleChecked == true)
                {
                    ProfitsLossesCheckBoxList.Items.Add(cat.FullPath(), CheckState.Checked);
                    _ids.Add(cat.Id);
                }
                else
                {
                    ProfitsLossesCheckBoxList.Items.Add(cat.FullPath());
                    _ids.Add(cat.Id);
                }
            }
            ProfitsLossesCheckBoxList.Items.Add(Labels.Config_RefreshCBL_Equity, Category.FindByBalanceType(this.Session, BalanceType.Equity)[0].IsVisibleChecked == true ? CheckState.Checked : CheckState.Unchecked);
            _ids.Add(Category.FindByBalanceType(this.Session, BalanceType.Equity)[0].Id);


            ProfitsLossesCheckBoxList.Items.Add(Labels.Config_RefreshCBL_Total_Interest_Expense, CheckState.Indeterminate);
            _ids.Add(0);

            ProfitsLossesCheckBoxList.Items.Add(Labels.Config_RefreshCBL_Net_interest_income, CheckState.Indeterminate);
            _ids.Add(0);

            ComputeOIA();

            ProfitsLossesCheckBoxList.Items.Add(Labels.Config_RefreshCBL_Income_before_impairment_charge, CheckState.Indeterminate);
            _ids.Add(0);

            ImpartmentData();

            ProfitsLossesCheckBoxList.Items.Add(Labels.Config_RefreshCBL_Impairment_charge, CheckState.Indeterminate);
            _ids.Add(0);

            ProfitsLossesCheckBoxList.Items.Add(Labels.Config_RefreshCBL_Income_tax_benefit____expense_, CheckState.Indeterminate);
            _ids.Add(0);

            _refreshed = true;
        }

        private void ImpartmentData()
        {
            List<string> _auxlist = new List<string>();

            foreach (Product _bond in ProductCache.FindIsModel())
            {
                if (!_auxlist.Contains(_bond.Category.Label))
                {
                    if (_bond.Category.IsVisibleCheckedModel == true)
                        ProfitsLossesCheckBoxList.Items.Add(_bond.Category.Label, CheckState.Checked);
                    else
                        ProfitsLossesCheckBoxList.Items.Add(_bond.Category.Label);
                    _auxlist.Add(_bond.Category.Label);
                    _ids.Add(0);
                    _imparmentID.Add(_ids.Count - 1, _bond.Category.Id);
                }
            }
        }

        private void ProfitsLossesCheckBoxList_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            if (_refreshed)
            {
                if (e.CurrentValue == CheckState.Indeterminate || e.NewValue == CheckState.Indeterminate)
                    e.NewValue = CheckState.Indeterminate;
                else
                {
                    bool isVisible = e.NewValue == CheckState.Checked;

                    if (_ids[e.Index] != 0)
                    {
                        if (_modified.ContainsKey(_ids[e.Index]))
                            _modified[_ids[e.Index]] = isVisible;
                        else
                            _modified.Add(_ids[e.Index], isVisible);

                        if (isVisible)
                        {
                            this.CheckParentCategory(_ids[e.Index]);
                        }else
                        {
                            this.UncheckChildrenCategories(_ids[e.Index]);
                        }
                    }
                    else
                    {
                        if (_oiaIDs.ContainsKey(e.Index))
                        {
                            if (_modifiedOIA.ContainsKey(_oiaIDs[e.Index]))
                                _modifiedOIA[_oiaIDs[e.Index]] = isVisible;
                            else
                                _modifiedOIA.Add(_oiaIDs[e.Index], isVisible);
                        }
                        if (_bondIDs.ContainsKey(e.Index))
                        {
                            if (_modifiedBond.ContainsKey(_bondIDs[e.Index]))
                                _modifiedBond[_bondIDs[e.Index]] = isVisible;
                            else
                                _modifiedBond.Add(_bondIDs[e.Index], isVisible);

                        }
                        if (_imparmentID.ContainsKey(e.Index))
                        {
                            if (_modifiedImparment.ContainsKey(_imparmentID[e.Index]))
                                _modifiedImparment[_imparmentID[e.Index]] = isVisible;
                            else
                                _modifiedImparment.Add(_imparmentID[e.Index], isVisible);
                        }
                    }
                }
            }
        }

        private void UncheckChildrenCategories(int categoryId)
        {
            IEnumerable<int> childrenIds = this.categoryParentIds.Where(p => p.Value == categoryId).Select(p => p.Key);

            foreach (int childId in childrenIds)
            {
                int index = this._ids.IndexOf(childId);
                if (index < 0) continue;

                this.ProfitsLossesCheckBoxList.SetItemCheckState(index, CheckState.Unchecked);
            }
        }

        private void CheckParentCategory(int categoryId)
        {
            if (!categoryParentIds.ContainsKey(categoryId)) return;

            int parentId = categoryParentIds[categoryId];

            int parentIndex = this._ids.IndexOf(parentId);
            if (parentIndex < 0) return;

            this.ProfitsLossesCheckBoxList.SetItemCheckState(parentIndex, CheckState.Checked);

            this.CheckParentCategory(parentId);
        }

        private void ComputeOIA()
        {
            string[] subtotalgroups = OtherItemAssum.GetAllSubgroups(this.Session);

            bool swapped;
            do
            {
                swapped = false;
                for (int i = 0; i < subtotalgroups.Length - 1; i++)
                {
                    if ((subtotalgroups[i] == null ? 0 : OtherItemAssum.FindBySubtotalGroup(this.Session, subtotalgroups[i]).Length) < (subtotalgroups[i + 1] == null ? 0 : OtherItemAssum.FindBySubtotalGroup(this.Session, subtotalgroups[i + 1]).Length))
                    {
                        string aux;
                        aux = subtotalgroups[i];
                        subtotalgroups[i] = subtotalgroups[i + 1];
                        subtotalgroups[i + 1] = aux;
                        swapped = true;
                    }
                }
            }
            while (swapped);


            foreach (string subTotalGroup in subtotalgroups)
            {
                OtherItemAssum[] oias = OtherItemAssum.FindBySubtotalGroup(this.Session, subTotalGroup);
                if (oias.Length > 1 && subTotalGroup != null)
                {
                    ProfitsLossesCheckBoxList.Items.Add(subTotalGroup, CheckState.Indeterminate);
                    _ids.Add(0);
                    //_oiaIDs.Add(_ids.Count - 1, oia.Id);
                }

                foreach (OtherItemAssum oia in oias)
                {
                    string name = subTotalGroup+"/"+oia.Descrip ?? "<description missing>";
                    if (oia.IsVisibleChecked == true)
                        ProfitsLossesCheckBoxList.Items.Add(name, CheckState.Checked);
                    else
                        ProfitsLossesCheckBoxList.Items.Add(name);
                    _ids.Add(0);
                    _oiaIDs.Add(_ids.Count - 1, oia.Id);
                }


            }
        }
    }
}