using BondManager.Resources.Language;

namespace BondManager.Views.BalanceSheet.Result
{
    partial class ScenarioValidationResult
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bottomPanel = new System.Windows.Forms.Panel();
            this.closeButton = new System.Windows.Forms.Button();
            this.topPanel = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cenetrPanel = new System.Windows.Forms.Panel();
            this.messagesListBox = new System.Windows.Forms.ListBox();
            this.bottomPanel.SuspendLayout();
            this.topPanel.SuspendLayout();
            this.cenetrPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // bottomPanel
            // 
            this.bottomPanel.Controls.Add(this.closeButton);
            this.bottomPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bottomPanel.Location = new System.Drawing.Point(0, 234);
            this.bottomPanel.Name = "bottomPanel";
            this.bottomPanel.Size = new System.Drawing.Size(506, 57);
            this.bottomPanel.TabIndex = 0;
            // 
            // closeButton
            // 
            this.closeButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.closeButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.closeButton.Location = new System.Drawing.Point(407, 13);
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size(87, 32);
            this.closeButton.TabIndex = 0;
            this.closeButton.Text = Labels.ScenarioValidationResult_InitializeComponent_Close;
            this.closeButton.UseVisualStyleBackColor = true;
            this.closeButton.Click += new System.EventHandler(this.closeButton_Click);
            // 
            // topPanel
            // 
            this.topPanel.Controls.Add(this.label2);
            this.topPanel.Controls.Add(this.label1);
            this.topPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.topPanel.Location = new System.Drawing.Point(0, 0);
            this.topPanel.Name = "topPanel";
            this.topPanel.Size = new System.Drawing.Size(506, 56);
            this.topPanel.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 33);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(258, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = Labels.ScenarioValidationResult_InitializeComponent_Double_click_on_the_error_message_to_go_to_its_source_;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(349, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = Labels.ScenarioValidationResult_InitializeComponent_There_have_been_some_errors_when_trying_to_run_scenario_s__;
            // 
            // cenetrPanel
            // 
            this.cenetrPanel.Controls.Add(this.messagesListBox);
            this.cenetrPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cenetrPanel.Location = new System.Drawing.Point(0, 56);
            this.cenetrPanel.Name = "cenetrPanel";
            this.cenetrPanel.Size = new System.Drawing.Size(506, 178);
            this.cenetrPanel.TabIndex = 2;
            // 
            // messagesListBox
            // 
            this.messagesListBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.messagesListBox.FormattingEnabled = true;
            this.messagesListBox.IntegralHeight = false;
            this.messagesListBox.Location = new System.Drawing.Point(0, 0);
            this.messagesListBox.Name = "messagesListBox";
            this.messagesListBox.Size = new System.Drawing.Size(506, 178);
            this.messagesListBox.TabIndex = 0;
            this.messagesListBox.DoubleClick += new System.EventHandler(this.messagesListBox_DoubleClick);
            // 
            // ScenarioValidationResult
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.closeButton;
            this.ClientSize = new System.Drawing.Size(506, 291);
            this.Controls.Add(this.cenetrPanel);
            this.Controls.Add(this.topPanel);
            this.Controls.Add(this.bottomPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ScenarioValidationResult";
            this.ShowInTaskbar = false;
            this.Text = Labels.ScenarioValidationResult_InitializeComponent_Scenario_Setup_Validation_Result;
            this.bottomPanel.ResumeLayout(false);
            this.topPanel.ResumeLayout(false);
            this.topPanel.PerformLayout();
            this.cenetrPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel bottomPanel;
        private System.Windows.Forms.Panel topPanel;
        private System.Windows.Forms.Button closeButton;
        private System.Windows.Forms.Panel cenetrPanel;
        private System.Windows.Forms.ListBox messagesListBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}