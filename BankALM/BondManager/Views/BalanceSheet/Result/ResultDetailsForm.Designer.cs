﻿namespace BondManager.Views.BalanceSheet.Result
{
    partial class ResultDetailsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.headerPanel = new System.Windows.Forms.Panel();
            this.aggregateBy = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.defaultDataShow = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.valuationCurrencyRadioButton = new System.Windows.Forms.RadioButton();
            this.productCurrencyRadioButton = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.scenarioComboBox = new System.Windows.Forms.ComboBox();
            this.reportTypeComboBox = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.aggregationTypeComboBox = new System.Windows.Forms.ComboBox();
            this.treegrid = new ALMSCommon.Controls.TreeGridView.TreeGridView();
            this.flickerPanel = new System.Windows.Forms.Panel();
            this.headerPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.treegrid)).BeginInit();
            this.SuspendLayout();
            // 
            // headerPanel
            // 
            this.headerPanel.Controls.Add(this.aggregationTypeComboBox);
            this.headerPanel.Controls.Add(this.scenarioComboBox);
            this.headerPanel.Controls.Add(this.aggregateBy);
            this.headerPanel.Controls.Add(this.label5);
            this.headerPanel.Controls.Add(this.defaultDataShow);
            this.headerPanel.Controls.Add(this.label4);
            this.headerPanel.Controls.Add(this.valuationCurrencyRadioButton);
            this.headerPanel.Controls.Add(this.productCurrencyRadioButton);
            this.headerPanel.Controls.Add(this.label3);
            this.headerPanel.Controls.Add(this.label8);
            this.headerPanel.Controls.Add(this.reportTypeComboBox);
            this.headerPanel.Controls.Add(this.label2);
            this.headerPanel.Controls.Add(this.label1);
            this.headerPanel.Controls.Add(this.flickerPanel);
            this.headerPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.headerPanel.Location = new System.Drawing.Point(0, 0);
            this.headerPanel.Name = "headerPanel";
            this.headerPanel.Size = new System.Drawing.Size(1197, 31);
            this.headerPanel.TabIndex = 6;
            // 
            // aggregateBy
            // 
            this.aggregateBy.DisplayMember = "Label";
            this.aggregateBy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.aggregateBy.DropDownWidth = 200;
            this.aggregateBy.FormattingEnabled = true;
            this.aggregateBy.Location = new System.Drawing.Point(847, 5);
            this.aggregateBy.Name = "aggregateBy";
            this.aggregateBy.Size = new System.Drawing.Size(67, 21);
            this.aggregateBy.TabIndex = 14;
            this.aggregateBy.ValueMember = "Rate";
            this.aggregateBy.SelectedIndexChanged += new System.EventHandler(this.aggregateBy_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label5.Location = new System.Drawing.Point(773, 8);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(73, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "Aggregate by:";
            // 
            // defaultDataShow
            // 
            this.defaultDataShow.DisplayMember = "Label";
            this.defaultDataShow.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.defaultDataShow.DropDownWidth = 200;
            this.defaultDataShow.FormattingEnabled = true;
            this.defaultDataShow.Location = new System.Drawing.Point(649, 5);
            this.defaultDataShow.Name = "defaultDataShow";
            this.defaultDataShow.Size = new System.Drawing.Size(120, 21);
            this.defaultDataShow.TabIndex = 12;
            this.defaultDataShow.ValueMember = "Rate";
            this.defaultDataShow.SelectedIndexChanged += new System.EventHandler(this.defaultDataShow_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label4.Location = new System.Drawing.Point(547, 8);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(99, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Default data show :";
            // 
            // valuationCurrencyRadioButton
            // 
            this.valuationCurrencyRadioButton.AutoSize = true;
            this.valuationCurrencyRadioButton.Location = new System.Drawing.Point(1079, 9);
            this.valuationCurrencyRadioButton.Name = "valuationCurrencyRadioButton";
            this.valuationCurrencyRadioButton.Size = new System.Drawing.Size(114, 17);
            this.valuationCurrencyRadioButton.TabIndex = 10;
            this.valuationCurrencyRadioButton.Text = "Valuation Currency";
            this.valuationCurrencyRadioButton.UseVisualStyleBackColor = true;
            // 
            // productCurrencyRadioButton
            // 
            this.productCurrencyRadioButton.AutoSize = true;
            this.productCurrencyRadioButton.Checked = true;
            this.productCurrencyRadioButton.Location = new System.Drawing.Point(975, 8);
            this.productCurrencyRadioButton.Name = "productCurrencyRadioButton";
            this.productCurrencyRadioButton.Size = new System.Drawing.Size(107, 17);
            this.productCurrencyRadioButton.TabIndex = 9;
            this.productCurrencyRadioButton.TabStop = true;
            this.productCurrencyRadioButton.Text = "Product Currency";
            this.productCurrencyRadioButton.UseVisualStyleBackColor = true;
            this.productCurrencyRadioButton.CheckedChanged += new System.EventHandler(this.ProductCurrencyRadioButtonCheckedChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label3.Location = new System.Drawing.Point(923, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Currency :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label8.Location = new System.Drawing.Point(369, 9);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(55, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "Scenario :";
            // 
            // scenarioComboBox
            // 
            this.scenarioComboBox.DisplayMember = "Label";
            this.scenarioComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.scenarioComboBox.DropDownWidth = 200;
            this.scenarioComboBox.FormattingEnabled = true;
            this.scenarioComboBox.Location = new System.Drawing.Point(423, 5);
            this.scenarioComboBox.Name = "scenarioComboBox";
            this.scenarioComboBox.Size = new System.Drawing.Size(120, 21);
            this.scenarioComboBox.TabIndex = 6;
            this.scenarioComboBox.ValueMember = "Rate";
            this.scenarioComboBox.SelectedIndexChanged += new System.EventHandler(this.ScenarioComboBoxSelectedIndexChanged);
            // 
            // reportTypeComboBox
            // 
            this.reportTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.reportTypeComboBox.FormattingEnabled = true;
            this.reportTypeComboBox.Location = new System.Drawing.Point(77, 5);
            this.reportTypeComboBox.Name = "reportTypeComboBox";
            this.reportTypeComboBox.Size = new System.Drawing.Size(150, 21);
            this.reportTypeComboBox.TabIndex = 1;
            this.reportTypeComboBox.SelectedIndexChanged += new System.EventHandler(this.ReportTypeComboBoxSelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(237, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Show :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Report Type :";
            // 
            // aggregationTypeComboBox
            // 
            this.aggregationTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.aggregationTypeComboBox.FormattingEnabled = true;
            this.aggregationTypeComboBox.Location = new System.Drawing.Point(275, 5);
            this.aggregationTypeComboBox.Name = "aggregationTypeComboBox";
            this.aggregationTypeComboBox.Size = new System.Drawing.Size(82, 21);
            this.aggregationTypeComboBox.TabIndex = 3;
            this.aggregationTypeComboBox.SelectedIndexChanged += new System.EventHandler(this.AggregationTypeComboBoxSelectedIndexChanged);
            // 
            // treegrid
            // 
            this.treegrid.AllowUserToAddRows = false;
            this.treegrid.AllowUserToDeleteRows = false;
            this.treegrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treegrid.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.treegrid.ImageList = null;
            this.treegrid.Location = new System.Drawing.Point(0, 31);
            this.treegrid.Name = "treegrid";
            this.treegrid.ReadOnly = true;
            this.treegrid.Size = new System.Drawing.Size(1197, 461);
            this.treegrid.TabIndex = 8;
            // 
            // flickerPanel
            // 
            this.flickerPanel.Location = new System.Drawing.Point(235, 0);
            this.flickerPanel.Name = "flickerPanel";
            this.flickerPanel.Size = new System.Drawing.Size(130, 31);
            this.flickerPanel.TabIndex = 9;
            // 
            // ResultDetailsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1197, 492);
            this.Controls.Add(this.treegrid);
            this.Controls.Add(this.headerPanel);
            this.Name = "ResultDetailsForm";
            this.Text = "Details";
            this.Shown += new System.EventHandler(this.ResultDetailsForm_Shown);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ResultDetailsFormKeyDown);
            this.headerPanel.ResumeLayout(false);
            this.headerPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.treegrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel headerPanel;
        private System.Windows.Forms.ComboBox reportTypeComboBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox aggregationTypeComboBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox scenarioComboBox;
        private ALMSCommon.Controls.TreeGridView.TreeGridView treegrid;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RadioButton valuationCurrencyRadioButton;
        private System.Windows.Forms.RadioButton productCurrencyRadioButton;
        private System.Windows.Forms.ComboBox defaultDataShow;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox aggregateBy;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel flickerPanel;



    }
}