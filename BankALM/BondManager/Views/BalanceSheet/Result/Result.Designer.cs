﻿using System.Drawing;
using ALMSCommon.Controls.TreeGridView;
using BondManager.Controls;
using BondManager.Resources.Language;

namespace BondManager.Views.BalanceSheet.Result
{
    partial class Result
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Result));
            this.resultHeaderPanel = new System.Windows.Forms.Panel();
            this.showErrorsButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.aggregationTypeComboBox = new System.Windows.Forms.ComboBox();
            this.exportAllButton = new System.Windows.Forms.Button();
            this.userDefinedButton = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.reportAnalysisTypeComboBox = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.reportTypeComboBox = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.scenarioComboBox = new System.Windows.Forms.ComboBox();
            this.printButton = new System.Windows.Forms.Button();
            this.configButton = new System.Windows.Forms.Button();
            this.resultLabel = new System.Windows.Forms.Label();
            this.treeImageList = new System.Windows.Forms.ImageList(this.components);
            this.resultDataGridView = new ALMSCommon.Controls.TreeGridView.TreeGridView();
            this.gridContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.scenarioComparisonToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resultHeaderPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.resultDataGridView)).BeginInit();
            this.gridContextMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // resultHeaderPanel
            // 
            this.resultHeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(160)))), ((int)(((byte)(160)))), ((int)(((byte)(160)))));
            this.resultHeaderPanel.Controls.Add(this.label1);
            this.resultHeaderPanel.Controls.Add(this.aggregationTypeComboBox);
            this.resultHeaderPanel.Controls.Add(this.exportAllButton);
            this.resultHeaderPanel.Controls.Add(this.userDefinedButton);
            this.resultHeaderPanel.Controls.Add(this.label10);
            this.resultHeaderPanel.Controls.Add(this.reportAnalysisTypeComboBox);
            this.resultHeaderPanel.Controls.Add(this.label9);
            this.resultHeaderPanel.Controls.Add(this.reportTypeComboBox);
            this.resultHeaderPanel.Controls.Add(this.label8);
            this.resultHeaderPanel.Controls.Add(this.scenarioComboBox);
            this.resultHeaderPanel.Controls.Add(this.printButton);
            this.resultHeaderPanel.Controls.Add(this.configButton);
            this.resultHeaderPanel.Controls.Add(this.resultLabel);
            this.resultHeaderPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.resultHeaderPanel.Location = new System.Drawing.Point(0, 0);
            this.resultHeaderPanel.Name = "resultHeaderPanel";
            this.resultHeaderPanel.Size = new System.Drawing.Size(1091, 34);
            this.resultHeaderPanel.TabIndex = 0;
            // 
            // showErrorsButton
            // 
            this.showErrorsButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.showErrorsButton.BackColor = System.Drawing.Color.Red;
            this.showErrorsButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.showErrorsButton.ForeColor = System.Drawing.Color.White;
            this.showErrorsButton.Location = new System.Drawing.Point(2, 35);
            this.showErrorsButton.Name = "showErrorsButton";
            this.showErrorsButton.Size = new System.Drawing.Size(186, 23);
            this.showErrorsButton.TabIndex = 12;
            this.showErrorsButton.Text = "WARNING! Calculate Errors";
            this.showErrorsButton.UseVisualStyleBackColor = false;
            this.showErrorsButton.Visible = false;
            this.showErrorsButton.Click += new System.EventHandler(this.ShowErrorsButtonClick);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(546, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "Show";
            // 
            // aggregationTypeComboBox
            // 
            this.aggregationTypeComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.aggregationTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.aggregationTypeComboBox.Items.AddRange(new object[] {
            "final",
            "average"});
            this.aggregationTypeComboBox.Location = new System.Drawing.Point(586, 8);
            this.aggregationTypeComboBox.Name = "aggregationTypeComboBox";
            this.aggregationTypeComboBox.Size = new System.Drawing.Size(99, 21);
            this.aggregationTypeComboBox.TabIndex = 10;
            this.aggregationTypeComboBox.SelectedIndexChanged += new System.EventHandler(this.AggregationTypeComboBoxSelectedIndexChanged);
            // 
            // exportAllButton
            // 
            this.exportAllButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.exportAllButton.BackColor = System.Drawing.SystemColors.Control;
            this.exportAllButton.Location = new System.Drawing.Point(932, 6);
            this.exportAllButton.Name = "exportAllButton";
            this.exportAllButton.Size = new System.Drawing.Size(75, 23);
            this.exportAllButton.TabIndex = 9;
            this.exportAllButton.Text = global::BondManager.Resources.Language.Labels.Result_InitializeComponent_Export_All;
            this.exportAllButton.UseVisualStyleBackColor = true;
            this.exportAllButton.Click += new System.EventHandler(this.ExportAllButtonClick);
            // 
            // userDefinedButton
            // 
            this.userDefinedButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.userDefinedButton.BackColor = System.Drawing.SystemColors.Control;
            this.userDefinedButton.Location = new System.Drawing.Point(48, 6);
            this.userDefinedButton.Name = "userDefinedButton";
            this.userDefinedButton.Size = new System.Drawing.Size(84, 23);
            this.userDefinedButton.TabIndex = 8;
            this.userDefinedButton.Text = global::BondManager.Resources.Language.Labels.Result_InitializeComponent_User_Defined;
            this.userDefinedButton.UseVisualStyleBackColor = true;
            this.userDefinedButton.Visible = false;
            this.userDefinedButton.Click += new System.EventHandler(this.UserDefinedButtonClick);
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label10.AutoSize = true;
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(116, 13);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(72, 13);
            this.label10.TabIndex = 7;
            this.label10.Text = "Analysis Type";
            // 
            // reportAnalysisTypeComboBox
            // 
            this.reportAnalysisTypeComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.reportAnalysisTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.reportAnalysisTypeComboBox.Location = new System.Drawing.Point(195, 8);
            this.reportAnalysisTypeComboBox.Name = "reportAnalysisTypeComboBox";
            this.reportAnalysisTypeComboBox.Size = new System.Drawing.Size(131, 21);
            this.reportAnalysisTypeComboBox.TabIndex = 6;
            this.reportAnalysisTypeComboBox.SelectedIndexChanged += new System.EventHandler(this.ReportAnalysisTypeComboBoxSelectedIndexChanged);
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.AutoSize = true;
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(330, 13);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(66, 13);
            this.label9.TabIndex = 7;
            this.label9.Text = "Report Type";
            // 
            // reportTypeComboBox
            // 
            this.reportTypeComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.reportTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.reportTypeComboBox.Location = new System.Drawing.Point(402, 8);
            this.reportTypeComboBox.Name = "reportTypeComboBox";
            this.reportTypeComboBox.Size = new System.Drawing.Size(131, 21);
            this.reportTypeComboBox.TabIndex = 6;
            this.reportTypeComboBox.SelectedIndexChanged += new System.EventHandler(this.ReportTypeComboBoxSelectedIndexChanged);
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(696, 11);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(49, 13);
            this.label8.TabIndex = 5;
            this.label8.Text = "Scenario";
            // 
            // scenarioComboBox
            // 
            this.scenarioComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.scenarioComboBox.DisplayMember = "Label";
            this.scenarioComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.scenarioComboBox.DropDownWidth = 200;
            this.scenarioComboBox.FormattingEnabled = true;
            this.scenarioComboBox.Location = new System.Drawing.Point(751, 7);
            this.scenarioComboBox.Name = "scenarioComboBox";
            this.scenarioComboBox.Size = new System.Drawing.Size(89, 21);
            this.scenarioComboBox.TabIndex = 4;
            this.scenarioComboBox.ValueMember = "Rate";
            this.scenarioComboBox.DropDown += new System.EventHandler(this.scenarioComboBox_DropDown);
            this.scenarioComboBox.SelectedIndexChanged += new System.EventHandler(this.ScenarioComboBoxSelectedIndexChanged);
            // 
            // printButton
            // 
            this.printButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.printButton.BackColor = System.Drawing.SystemColors.Control;
            this.printButton.Location = new System.Drawing.Point(851, 6);
            this.printButton.Name = "printButton";
            this.printButton.Size = new System.Drawing.Size(75, 23);
            this.printButton.TabIndex = 3;
            this.printButton.Text = "Report";
            this.printButton.UseVisualStyleBackColor = true;
            this.printButton.Click += new System.EventHandler(this.PrintButtonClick);
            // 
            // configButton
            // 
            this.configButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.configButton.BackColor = System.Drawing.SystemColors.Control;
            this.configButton.Location = new System.Drawing.Point(1013, 6);
            this.configButton.Name = "configButton";
            this.configButton.Size = new System.Drawing.Size(75, 23);
            this.configButton.TabIndex = 2;
            this.configButton.Text = global::BondManager.Resources.Language.Labels.Result_InitializeComponent_Config;
            this.configButton.UseVisualStyleBackColor = true;
            this.configButton.Click += new System.EventHandler(this.ConfigButtonClick);
            // 
            // resultLabel
            // 
            this.resultLabel.AutoSize = true;
            this.resultLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.resultLabel.ForeColor = System.Drawing.Color.White;
            this.resultLabel.Location = new System.Drawing.Point(5, 5);
            this.resultLabel.Name = "resultLabel";
            this.resultLabel.Size = new System.Drawing.Size(68, 24);
            this.resultLabel.TabIndex = 1;
            this.resultLabel.Text = "Result";
            // 
            // treeImageList
            // 
            this.treeImageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("treeImageList.ImageStream")));
            this.treeImageList.TransparentColor = System.Drawing.Color.Transparent;
            this.treeImageList.Images.SetKeyName(0, "folder.png");
            this.treeImageList.Images.SetKeyName(1, "document_16.png");
            // 
            // resultDataGridView
            // 
            this.resultDataGridView.AllowUserToAddRows = false;
            this.resultDataGridView.AllowUserToDeleteRows = false;
            this.resultDataGridView.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.resultDataGridView.ContextMenuStrip = this.gridContextMenuStrip;
            this.resultDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.resultDataGridView.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.resultDataGridView.ImageList = null;
            this.resultDataGridView.Location = new System.Drawing.Point(0, 34);
            this.resultDataGridView.Name = "resultDataGridView";
            this.resultDataGridView.ReadOnly = true;
            this.resultDataGridView.Size = new System.Drawing.Size(1091, 437);
            this.resultDataGridView.TabIndex = 1;
            // 
            // gridContextMenuStrip
            // 
            this.gridContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.scenarioComparisonToolStripMenuItem});
            this.gridContextMenuStrip.Name = "gridContextMenuStrip";
            this.gridContextMenuStrip.Size = new System.Drawing.Size(188, 26);
            this.gridContextMenuStrip.Opening += new System.ComponentModel.CancelEventHandler(this.GridContextMenuStripOpening);
            // 
            // scenarioComparisonToolStripMenuItem
            // 
            this.scenarioComparisonToolStripMenuItem.Name = "scenarioComparisonToolStripMenuItem";
            this.scenarioComparisonToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.scenarioComparisonToolStripMenuItem.Text = global::BondManager.Resources.Language.Labels.Result_InitializeComponent__Scenario_Comparison;
            this.scenarioComparisonToolStripMenuItem.Click += new System.EventHandler(this.ScenarioComparisonToolStripMenuItemClick);
            // 
            // Result
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.showErrorsButton);
            this.Controls.Add(this.resultDataGridView);
            this.Controls.Add(this.resultHeaderPanel);
            this.Name = "Result";
            this.Size = new System.Drawing.Size(1091, 471);
            this.Load += new System.EventHandler(this.ResultLoad);
            this.resultHeaderPanel.ResumeLayout(false);
            this.resultHeaderPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.resultDataGridView)).EndInit();
            this.gridContextMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private TreeGridView resultDataGridView;
        private System.Windows.Forms.Panel resultHeaderPanel;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox reportTypeComboBox;
        private System.Windows.Forms.ComboBox reportAnalysisTypeComboBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox scenarioComboBox;
        private System.Windows.Forms.Button printButton;
        private System.Windows.Forms.Button configButton;
        private System.Windows.Forms.Label resultLabel;
        private System.Windows.Forms.Button userDefinedButton;
        private System.Windows.Forms.Button exportAllButton;
        private System.Windows.Forms.ImageList treeImageList;
        private System.Windows.Forms.ContextMenuStrip gridContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem scenarioComparisonToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox aggregationTypeComboBox;
        private System.Windows.Forms.Button showErrorsButton;
    }
}
