using System;
using System.Globalization;
using System.Windows.Forms;
using BondManager.Resources.Language;
using Excel = Microsoft.Office.Interop.Excel;
using System.Runtime.InteropServices;
using System.IO;

namespace BondManager.Views.BalanceSheet.Result
{
    public partial class PrintForm : Form
    {
        readonly DataGridView resultDataGridView;

        public PrintForm(DataGridView resultDataGridView)
        {
            InitializeComponent();
            this.resultDataGridView = resultDataGridView;
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void PrintButton_Click(object sender, EventArgs e)
        {
            //template path
            try
            {
                if (csvRadioButton.Checked)
                {
                    DoExportCSV();
                }
                else
                {
                    DoExportExcel();
                }
            }
            catch (Exception ex)
            {
                Log.Exception("PrintButton_Click", ex);
                MessageBox.Show(
                    Labels.PrintForm_PrintButton_Click_Unable_to_generate_excel_file__Microsoft_Office_might_be_corrupted_or_not_installed,
                    "SOGALMS");
            }

            this.Close();
        }

        private void DoExportCSV()
        {
            Cursor _c = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;
            string outFolder = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) +
                   "\\SOGALMS\\Excel\\Reports";

            if (!Directory.Exists(outFolder)) Directory.CreateDirectory(outFolder);

            string designedPath = outFolder + "\\Result_" + DateTime.Now.ToString("dd_MM_yyyy_hh-mm-ss") + ".csv";

            using (StreamWriter writer = new StreamWriter(designedPath, false))
            {
                foreach (DataGridViewRow row in resultDataGridView.Rows)
                {
                    bool firstCell = true;
                    foreach (DataGridViewCell cell in row.Cells)
                    {
                        if (!cell.Visible) continue;

                        if (!firstCell) writer.Write(";");

                        writer.Write(FormatValue(cell.Value));

                        firstCell = false;
                    }
                    writer.WriteLine();
                }
                writer.Close();
            }

            System.Diagnostics.Process.Start(designedPath);
            Cursor.Current = _c;
        }

        private static string FormatValue(object value)
        {
            if (value == null) return String.Empty;

            string stringValue;
            if(value is double)
            {
                double decimalValue = (double) value;
                decimalValue = Math.Round(decimalValue, 2);
                stringValue = decimalValue.ToString();
                stringValue=stringValue.Replace(CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator,
                                    CultureInfo.InstalledUICulture.NumberFormat.NumberDecimalSeparator);
            }else
            {
                stringValue = value.ToString();
            }

            return "\"" +stringValue.Replace("\"", "\"\"") + "\"";
        }

        private void DoExportExcel()
        {
            Cursor _c = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;
            string templatePath = Path.GetDirectoryName(Application.ExecutablePath) + "\\Excel\\Template\\EmptyTemplate.xls ";

            string outFolder = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) +
                               "\\SOGALMS\\Excel\\Reports";

            if (!Directory.Exists(outFolder)) Directory.CreateDirectory(outFolder);

            string designedPath = outFolder + "\\Result_" + DateTime.Now.ToString("dd_MM_yyyy_hh-mm-ss") + ".xls";

            // initialize the Excel Application class
            Excel.Application app = new Excel.Application();
            app.Visible = false;

            // create the workbook object by opening the excel file.
            Excel.Workbook workBook = app.Workbooks.Open(templatePath,
                                                         0,
                                                         true,
                                                         5,
                                                         "",
                                                         "",
                                                         true,
                                                         Excel.XlPlatform.xlWindows,
                                                         "\t",
                                                         false,
                                                         false,
                                                         0,
                                                         true,
                                                         1,
                                                         0);

            /* get the active worksheet using sheet name or active sheet */
            Excel.Worksheet workSheet = (Excel.Worksheet)workBook.Sheets[1];
            int i = 1;

            const int di = 3;
            const int dj = 0;

            foreach (DataGridViewRow _row in resultDataGridView.Rows)
            {
                int j = 1;

                foreach (DataGridViewCell cell in _row.Cells)
                {
                    if (resultDataGridView[j - 1, i - 1].Value != null && j > 1)
                    {
                        ((Excel.Range)workSheet.Cells[i + di, j + dj]).Value2 = resultDataGridView[j - 1, i - 1].Value.ToString();
                    }
                    if (resultDataGridView.Rows[i - 1].DefaultCellStyle.Font != null)
                        ((Excel.Range)workSheet.Cells[i + di, j + dj]).Font.Bold = resultDataGridView.Rows[i - 1].DefaultCellStyle.Font.Bold;
                    if (resultDataGridView.Rows[i - 1].DefaultCellStyle.BackColor.Name != "0")
                        ((Excel.Range)workSheet.Cells[i + di, j + dj]).Interior.Color = System.Drawing.ColorTranslator.ToWin32(resultDataGridView.Rows[i - 1].DefaultCellStyle.BackColor);
                    j++;
                }
                i++;
            }

            //force a garbage collection
            GC.Collect();
            GC.WaitForPendingFinalizers();

            if (File.Exists(designedPath)) File.Delete(designedPath);

            workBook.Close(true, designedPath, true);
            Marshal.ReleaseComObject(workBook);
            GC.Collect();
            GC.WaitForPendingFinalizers();

            //Close running excel app
            app.Quit();

            System.Diagnostics.Process.Start(designedPath);
            Cursor.Current = _c;
        }
    }
}