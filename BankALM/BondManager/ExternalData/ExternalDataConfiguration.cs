﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BondManager.ExternalData
{
    public class ExternalDataConfiguration
    {
        public static readonly Dictionary<int, IExternalDataInterface> InterfaceClasses = new Dictionary<int, IExternalDataInterface>()
                                                                                              {{9000,new IsodevDeltaExternalDataInterface()},
                                                                                              {9999,new CashAdjUniversalSolver()},
                                                                                              {9001, new CAReferentielGfiInterface()}};
    }
}
