﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using ALMS.Products;
using ALMSCommon;
using BankALM.Infrastructure.Data;
using BondManager.Domain;
using BondManager.Views;
using NHibernate;

namespace BondManager.ExternalData
{
    public class IsodevDeltaExternalDataInterface : IExternalDataInterface
    {
        public const string FIELD_SEPARATOR = "\t";
        public const string LINE_SEPARATOR = "\n";

        public string Name { get { return "Interface stock ISODEV DELTA (BETA)"; } }

        private OpenFileDialog openFileDialog;

        public List<String> lines;


        public IsodevDeltaExternalDataInterface()
        {
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            //this.openFileDialog.DefaultExt = "xlsx";
            //this.openFileDialog.Filter = "XLSX-Excel files 2007+|*.xlsx|XLS-Excel files 2003|*.xls|All files|*.*";
            this.openFileDialog.Title = "Select FCT file for import";

        }

        public void Import(object sender, EventArgs eventArgs)
        {
            using (ISession session = SessionManager.OpenSession())
            {
                if (!Dialogs.Confirm("Choose the right valuation date first", "Are you sure you want to import a Delta file for a valuation date of " + Param.GetValuationDate(session)))
                    return;

                if (this.openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    int deletedBonds = 0;
                    int insertedBonds = 0;
                    StreamReader myFile = new StreamReader(this.openFileDialog.FileName);
                    string file = myFile.ReadToEnd();
                    myFile.Close();
                    lines = new List<string>(file.Split(LINE_SEPARATOR.ToCharArray(), StringSplitOptions.RemoveEmptyEntries));
                    Category rootCat;
                    Product product;

                    rootCat = Category.FindNodeByName(session, "crédits clients");
                    if (rootCat==null)
                    {
                        Dialogs.Error("No Category Found", "Cannot find [crédits clients] category, nothing was imported !");
                        return;
                    }
                    deletedBonds = rootCat.Bonds.Count;
                    rootCat.Bonds.Clear();
                    rootCat.Children.Clear();

                    session.Flush();

                    Category ratingCateg;
                    FCTLine prod;
                    int nextALMID = Category.GetNewALMID(session);
                    product = new Product();
                    product.GenerateALMID();
                    int nextProductALMID = product.ALMID;
                    foreach (string line in lines)
                    {
                        if (line.Length < 496) continue;
                        prod = new FCTLine(line);
                        ratingCateg = rootCat.Children.FirstOrDefault(c => c.Label == "rating " + prod.Rating);
                        if (ratingCateg == null)
                        {
                            ratingCateg = new Category();
                            ratingCateg.ALMID = nextALMID++;
                            ratingCateg.Label = "rating " + prod.Rating;
                            ratingCateg.Type = CategoryType.Normal;
                            ratingCateg.BalanceType = rootCat.BalanceType;
                            ratingCateg.IsVisibleChecked = true;
                            ratingCateg.IsVisibleCheckedModel = true;
                            ratingCateg.ReadOnly = false;
                            ratingCateg.isVisible = true;
                            ratingCateg.Parent = rootCat;
                            rootCat.Children.Add(ratingCateg);
                        }
                        product = new Product();
                        product.ALMID = nextProductALMID++;
                        product.Accounting = ProductAccounting.LAndR;
                        product.AmortizationType = ProductAmortizationType.CST_PAY;
                        product.Category = ratingCateg;
                        product.SecId = prod.SecId;
                        product.Description = prod.Description;
                        product.Coupon = prod.Coupon.ToString();
                        DateTime valuationDate = Param.GetValuationDate(session).GetValueOrDefault();
                        //product.Frequency = (ProductFrequency) prod.Frequency;

                        product.OrigFace = prod.Original_face.ToString();
                        product.ComputeBookValue(double.Parse(product.BookPrice));
                        product.CouponBasis = CouponBasis.A30360;
                        product.Frequency = ProductFrequency.Monthly;
                        if (prod.PremiereEcheance < valuationDate)
                        {//IN_BALANCE
                            product.Attrib=ProductAttrib.InBalance;
                            product.Duration = (prod.Maturity_date.Year - valuationDate.Year) * 12 + 
                                                prod.Maturity_date.Month - valuationDate.Month +1 ;
                            if (product.Duration + prod.Amortization_lag_if_cst_pay > prod.InitialDuration)
                            {
                                product.Amortization =
                                    (prod.Amortization_lag_if_cst_pay - prod.InitialDuration + product.Duration).ToString();
                            }
                            else
                            {
                                product.Amortization = "0";
                            }


                        }
                        else
                        {//IN_FUTURE
                            product.Attrib=ProductAttrib.InFuture;
                            product.StartDate = prod.PremiereEcheance;
                            product.Duration = (prod.Maturity_date.Year - prod.PremiereEcheance.Year) * 12 + 
                                                prod.Maturity_date.Month - prod.PremiereEcheance.Month + 1;
                            product.Amortization = prod.Amortization_lag_if_cst_pay.ToString();

                        }
                        ratingCateg.Bonds.Add(product);
                        insertedBonds++;
                    }
                    session.Flush();
                    MainForm.Instance.balance.RefreshTreeView();
                    Dialogs.Info("Import succeeded ISODEV DELTA", deletedBonds + " products was deleted,\n" + insertedBonds + " was added.");
                }

            }
        }
    }

    public class FCTLine
    {


        public String SecId { get; set; }
        public String Description { get; set; }
        public decimal Coupon { get; set; }
        public DateTime Maturity_date { get; set; }
        //public int Frequency { get; set; }
        public int InitialDuration { get; set; }
        public int Amortization_lag_if_cst_pay { get; set; }
        public String Rating { get; set; }
        public decimal Original_face { get; set; }
        public String State_of_debt { get; set; }
        public DateTime PremiereEcheance { get; set; }

        //public DateTime Next_paydate { get; set; }

        public FCTLine(string line)
        {
            SecId = line.Substring(12, 6).Trim();
            Description = line.Substring(23, 50).Trim();
            Coupon = decimal.Parse(line.Substring(85, 7).Replace(',','.'))/100;
            Maturity_date = DateFromAAAAMMJJ(line.Substring(133, 8));
            InitialDuration = int.Parse(line.Substring(129, 3));
            PremiereEcheance = DateFromAAAAMMJJ(line.Substring(120, 8));
            //Frequency = int.Parse(line.Substring(142, 1));
            Amortization_lag_if_cst_pay = int.Parse(line.Substring(160, 3));
            Rating = line.Substring(173, 1).Trim();
            Original_face = decimal.Parse(line.Substring(175, 15).Replace(',', '.'));
            State_of_debt = line.Substring(191, 1).Trim();
            //Next_paydate = DateFromAAAAMMJJ(line.Substring(355, 8));
        }

        public static DateTime DateFromAAAAMMJJ(string aaaammjj)
        {
            int y = int.Parse(aaaammjj.Substring(0, 4));
            int m = int.Parse(aaaammjj.Substring(4, 2));
            int d = int.Parse(aaaammjj.Substring(6, 2));
            return new DateTime(y, m, d);
        }
    }
}
