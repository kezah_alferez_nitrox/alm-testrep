﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using ALMSCommon.Forms;
using BankALM.Infrastructure.Data;
using BondManager.Calculations;
using BondManager.Domain;
using Microsoft.Office.Interop.Excel;
using NHibernate;
using ALMSCommon;
using Dialogs = ALMSCommon.Dialogs;

namespace BondManager.ExternalData
{
    public class CashAdjUniversalSolver : IExternalDataInterface
    {
        public static int resultMonths = 12;
        public string Name { get { return "Cash Adjustment Universal Solver ("+resultMonths+" months forward)"; } }
        private static BackgroundWorker localBg;
        private string cashAdjErrors;
        public void Import(object sender, EventArgs eventArgs)
        {
            cashAdjErrors = "";
            localBg = new BackgroundWorker();
            localBg.WorkerReportsProgress = true;
            localBg.WorkerSupportsCancellation = true;
            LoadForm.Instance.DoWork(new WorkParameters(this.Simulate, this.SimulationEnd, true), null);

        }

        private void SimulationEnd(Exception ex)
        {
            Application excel = null;
            Workbook workBook = null;

            try
            {
                excel = new Application();
                excel.Visible = false;

                workBook = excel.Workbooks.Add(XlWBATemplate.xlWBATWorksheet);

                Worksheet sheet = (Worksheet) workBook.Sheets[1];

                int row = 1;
                string[] lines = cashAdjErrors.Split('\n');
                string[] cells;
                for (int i = 0; i < lines.Length; i++)
                {
                    cells = lines[i].Split(',');
                    for (int j = 0; j < cells.Length; j++)
                        sheet.Cells[i+1, j+1] = cells[j];
                }

                Marshal.ReleaseComObject(sheet);
            }
            catch (Exception exx)
            {
                Log.Exception(this.Name, exx);

            }
            finally
            {
                if (null != workBook)
                {
                    Marshal.ReleaseComObject(workBook);
                }

                if (null != excel)
                {
                    excel.Visible = true;
                    Marshal.ReleaseComObject(excel);
                }
            }
            
            Console.WriteLine(cashAdjErrors);
        }

        private void Simulate(BackgroundWorker bg)
        {
            double[] initialCashAdjustments = new double[resultMonths];
            double[] cashAdjustments = new double[resultMonths];
            using (ISession session = SessionManager.OpenSession())
            {
                Category[] categories = Category.FindAll(session);
                int avancement = 0;
                Simulation simulation = new Simulation();
                simulation.Run(localBg, resultMonths);
                ScenarioData scenarioData = simulation[simulation.Scenarios.First()];//only the first scenario
                Category[] cashAdjcategories = scenarioData.Categories.Where(c => c.Type == CategoryType.CashAdj).ToArray();
                for (int i = 0; i < resultMonths; i++)
                {
                    foreach (Category cashAdjcategory in cashAdjcategories)
                    {
                        initialCashAdjustments[i] += scenarioData.CategoryBookValue[cashAdjcategory.Id][i];
                    }
                }
                cashAdjErrors += CashAdjVariation("INITIAL CASH ADJ (all categories)", new double[initialCashAdjustments.Length], initialCashAdjustments);
                foreach (Category category in categories)
                {
                    if (category.Type == CategoryType.Normal)
                    {
                        bg.ReportProgress(100 * avancement++ / categories.Length);
                        cashAdjustments = simulateCategoryExclusion(category,bg, session);
                        if (cashAdjustments != null)cashAdjErrors +=CashAdjVariation(category.FullPath(), cashAdjustments, initialCashAdjustments);
                    }
                }
                
            }
        }

        private static double[] simulateCategoryExclusion(Category category,BackgroundWorker bg, ISession session)
        {
            double[] result = new double[resultMonths];
            Product[] prods = ProductCache.products.FindAll(p => p.Category.ALMID == category.ALMID).ToArray();
            if(prods.Length==0)//if(category.Bonds.Count==0)
            {
                return null;
            }
            else
            {
                //terminal category, exclude all bonds and test
                bool[] initialExclusionState = new bool[prods.Length];
                for (int i = 0; i < prods.Length; i++)
                {
                    initialExclusionState[i] = prods[i].Exclude;
                    prods[i].Exclude = true;
                }
                session.Flush();
                Simulation simulation = new Simulation();
                simulation.Run(localBg, resultMonths);
                ScenarioData scenarioData = simulation[simulation.Scenarios.First()];//only the first scenario
                Category[] cashAdjcategories = scenarioData.Categories.Where(c => c.Type == CategoryType.CashAdj).ToArray();
                for (int i = 0; i < resultMonths; i++)
                {
                    foreach (Category cashAdjcategory in cashAdjcategories)
                    {
                        result[i] += scenarioData.CategoryBookValue[cashAdjcategory.Id][i];
                    }
                }
                for (int i = 0; i < prods.Length; i++)
                {
                    prods[i].Exclude = initialExclusionState[i];
                }
                session.Flush();
            }
            return result;
        }

        private static string CashAdjVariation(string CategoryName, double[] cashAdj, double[] cashAdjInitial)
        {
            string result = CategoryName+":";
            bool thereIsVariation = false;
            double stardardDelta = cashAdjInitial[0] - cashAdj[0];
            for (int i = 0; i < cashAdj.Length; i++)
            {
                if (Math.Abs(cashAdjInitial[i] - cashAdj[i]) - Math.Abs(stardardDelta) > 0.01) 
                    thereIsVariation = true;
                result += "," + (cashAdjInitial[i] - cashAdj[i]).ToString();
            }
            if (thereIsVariation)
                return result + "\n";
            else
                return "";
        }

    }
}
