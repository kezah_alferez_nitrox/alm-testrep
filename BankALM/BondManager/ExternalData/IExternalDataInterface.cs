﻿using System;

namespace BondManager.ExternalData
{
    public interface IExternalDataInterface
    {
        string Name { get; }
        void Import(object sender, EventArgs eventArgs);
    }
}
