using System;

namespace BondManager.Calculations
{
    public class SimulationException : Exception
    {
        public ValidationMessage ValidationMessage { get; set; }

        public SimulationException(ValidationMessage validationMessage, Exception innerException)
            : base(validationMessage.Message, innerException)
        {
            this.ValidationMessage = validationMessage;
        }
    }
}