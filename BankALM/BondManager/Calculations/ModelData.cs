using System;
using ALMSCommon;
using BondManager.Domain;

namespace BondManager.Calculations
{
    public class ModelData
    {
        private readonly ProductData productData;


        private readonly double[] default_;
        private readonly double[] sub_default;
        private readonly double[] paidInterest;
        private readonly double[] _subPaidSubCoupon;
        private readonly double[] sub_coupon_on_prepayement;
        private readonly double[] coupon_on_prepayement;
        private readonly double[] normalReinbursement;
        private readonly double[] prepayments;
        private readonly double[] lossesImpairment;
        private readonly double[] recovery;
        private readonly double[] incurred_but_not_reported;

        public double[] Incurred_but_not_reported
        {
            get { return incurred_but_not_reported; }
        }


        private readonly double[] cumulativeDelinquencies;
        private readonly double[] cumulativeLosses;
        private readonly double[] cumulativeCPR;

        private readonly double[] cumulativeDelinquenciesOnInitialNominal;
        private readonly double[] cumulativeLossesOnInitialNominal;
        private readonly double[] cumulativeCPROnInitialNominal;


        public ModelData(ProductData productData, int resultMonths)
        {
            this.productData = productData;


            default_ = new double[resultMonths];
            sub_default = new double[resultMonths];
            paidInterest = new double[resultMonths];
            this._subPaidSubCoupon = new double[resultMonths];
            sub_coupon_on_prepayement = new double[resultMonths];
            coupon_on_prepayement = new double[resultMonths];
            normalReinbursement = new double[resultMonths];
            prepayments = new double[resultMonths];
            lossesImpairment = new double[resultMonths];
            recovery = new double[resultMonths];
            incurred_but_not_reported = new double[resultMonths];
            cumulativeDelinquencies = new double[resultMonths];
            cumulativeLosses = new double[resultMonths];
            cumulativeCPR = new double[resultMonths];
            cumulatedReceivablePayable = new double[resultMonths];

            cumulativeDelinquenciesOnInitialNominal = new double[resultMonths];
            cumulativeLossesOnInitialNominal = new double[resultMonths];
            cumulativeCPROnInitialNominal = new double[resultMonths];


        }

        public void Iteration(int step, int lastStepCoupon, bool isCurrentStepCouponStep, double realCouponRateForIteration)
        {
            if (step > this.productData.StartStep && this.productData.RemainPrincipal[step - 1] != 0)
            {
                double dailyCoef = 1.0;

                if (AlmConfiguration.IsComputationDaily())
                {
                    DateTime date = productData.ValuationDate.AddDays(step - 1);
                    int daysInMonth = DateTime.DaysInMonth(date.Year, date.Month);
                    dailyCoef /= daysInMonth;
                }

                double face = this.productData.CurrFace[step - 1];

                double def_r = this.productData.CdrValues[step - 1] / 12 * dailyCoef;
                double prepay_r = this.productData.CprValues[step - 1] / 12 * dailyCoef;

                sub_default[step] = face * def_r;// *(1 - prepay_r) * (1 - nrr[i]);
                //OLD MArc : sub_paidInterest[i] = (1 - def_r) * (this.productData.CouponIncome[i]) + prepayments[i] * this.productData.MarginAccruedInterest[i];

                //New Serge : sub_paidInterest[i] = (this.CurrentNominal[i - 1] - this.MonthlyDefault[i]) * this.InterestRate[i]/12;
                //sub_paidInterest[i] = (this.CurrentNominal[i - 1] - this.MonthlyDefault[i]) * this.productData.MarginAccruedInterest[i];
                //this._subPaidSubCoupon[i] = (this.CurrentNominal[i - 1] - this.MonthlyDefault[i]) * (this.InterestRate[i]+this.productData.Spread[i]) / 12;

                this._subPaidSubCoupon[step] = (this.CurrentNominal[step - 1] - this.MonthlyDefault[step]) * realCouponRateForIteration;



                if (isCurrentStepCouponStep)
                {
                    double total_default = 0;
                    double total_paid = 0;
                    double total_ibnr_to_subtract = 0;
                    for (int j = lastStepCoupon + 1; j <= step; j++)
                    {
                        total_default += sub_default[j];
                        total_paid += this._subPaidSubCoupon[j];
                        total_ibnr_to_subtract += incurred_but_not_reported[j];
                    }
                    incurred_but_not_reported[step] = -1 * total_ibnr_to_subtract;
                    default_[step] = total_default;
                    paidInterest[step] = total_paid;
                }
                else
                {
                    incurred_but_not_reported[step] = sub_default[step];
                }



                //mtu OLD normalReinbursement[i] = face * (1 - def_r) * (1 - prepay_r) * nrr[i];
                //normalReinbursement[i] = (face - default_[i]) * (TheoricalFace[i - 1] - TheoricalFace[i]) / TheoricalFace[i - 1];
                if (TheoricalFace[step - 1] != 0)
                    normalReinbursement[step] = (face - sub_default[step]) *
                                                (TheoricalFace[step - 1] - TheoricalFace[step]) / TheoricalFace[step - 1];
                else
                    normalReinbursement[step] = 0;

                //mtu

                //mtu OLD prepayments[i] = face * prepay_r * (1 - def_r);// *(1 - nrr[i]);
                prepayments[step] = prepay_r * (face - default_[step] - normalReinbursement[step]);
                if (prepayments[step] < 0) prepayments[step] = 0;//prepayements cannont be negative
                sub_coupon_on_prepayement[step] = 0;
                if (!isCurrentStepCouponStep)
                {
                    for (int k = lastStepCoupon + 1; k <= step; k++)
                    {
                        //sub_coupon_on_prepayement[i] += prepayments[i] * (this.InterestRate[k]+this.productData.Spread[k]) / 12;
                        sub_coupon_on_prepayement[step] += prepayments[step] * realCouponRateForIteration;
                    }
                }
                else
                {
                    double total_coupon_on_prepayement = 0;
                    for (int j = lastStepCoupon + 1; j <= step; j++)
                    {
                        total_coupon_on_prepayement += sub_coupon_on_prepayement[j];
                    }
                    coupon_on_prepayement[step] = total_coupon_on_prepayement;
                }
                //mtu
                lossesImpairment[step] = -1 * default_[step] * this.productData.LgdValues[step] * dailyCoef;

                recovery[step] = default_[step] + lossesImpairment[step];

                cumulativeDelinquencies[step] = cumulativeDelinquencies[step - 1] + default_[step];
                cumulativeLosses[step] = cumulativeLosses[step - 1] + lossesImpairment[step];
                cumulativeCPR[step] = cumulativeCPR[step - 1] + prepayments[step];

                if (this.productData.CurrFace[this.productData.StartStep] != 0)
                {
                    if (this.productData.CurrFace[this.productData.StartStep] != 0)
                    {
                        cumulativeDelinquenciesOnInitialNominal[step] = cumulativeDelinquencies[step] / this.productData.CurrFace[this.productData.StartStep];
                        cumulativeLossesOnInitialNominal[step] = -cumulativeLosses[step] / this.productData.CurrFace[this.productData.StartStep];
                        cumulativeCPROnInitialNominal[step] = cumulativeCPR[step] / this.productData.CurrFace[this.productData.StartStep];
                    }
                    else
                    {
                        cumulativeDelinquenciesOnInitialNominal[step] = 0;
                        cumulativeLossesOnInitialNominal[step] = 0;
                        cumulativeCPROnInitialNominal[step] = 0;
                    }
                }

                //bondData.LiveFace[i] = bondData.LiveFace[i - 1] * (1 - def_r - (1 - def_r) * prepay_r);
                this.productData.LiveFace[step] = this.productData.LiveFace[step - 1] * (1 - def_r) * (1 - prepay_r);
            }
        }

        public double[] LossesImpairment
        {
            get { return lossesImpairment; }
        }

        public double[] MonthlyDefault
        {
            get { return sub_default; }
        }
        public double[] CouponOnPrepayement
        {
            get { return coupon_on_prepayement; }
        }
        public double[] MonthlyCouponOnPrepayement
        {
            get { return sub_coupon_on_prepayement; }
        }
        public double[] SubCouponIncome
        {
            get { return this._subPaidSubCoupon; }
        }
        public double[] InterestRate
        {
            get { return this.productData.CouponValues; }
        }

        public double[] CurrentNominal
        {
            get { return this.productData.CurrFace; }
        }

        public double[] TheoricalFace
        {
            get
            {
                double[] values = new double[this.productData.CurrFace.Length];
                for (int i = 0; i < values.Length; i++)
                {
                    //values[i] = this.productData.LiveFace[i] == 0
                    //                ? 0 : this.productData.CurrFace[i] / this.productData.LiveFace[i];

                    values[i] = this.productData.RemainPrincipal[i] * this.productData.CurrFace[this.productData.StartStep];
                }
                return values;
            }
        }

        public double[] Default
        {
            get { return default_; }
        }

        public double[] InterestIncomes
        {
            get { return paidInterest; }
        }

        public double[] NormalReimbursement
        {
            get { return normalReinbursement; }
        }

        public double[] Prepayments
        {
            get { return prepayments; }
        }

        public double[] LaggedRecovery
        {
            get
            {
                double[] laggedRecovery = new double[recovery.Length + productData.RecoveryLagInMonths];
                for (int step = 0; step < recovery.Length + productData.RecoveryLagInMonths; step++)
                {
                    if (productData.RecoveryLagInMonths > step)
                        laggedRecovery[step] = 0;
                    else
                        laggedRecovery[step] = recovery[step - productData.RecoveryLagInMonths];
                }
                return laggedRecovery;
            }
        }

        private double[] cumulatedReceivablePayable;


        public double[] Recovery
        {
            get
            {
                //if (this.productData.Product.Attrib == ProductAttrib.Roll)
                //{
                //    double subRolls_recovery = 0;
                //    for(int i
                //}
                //else
                //{
                return recovery;
                //}
            }
        }

        public double[] TotalReimbursementsAndRecovery
        {
            get { return lossesImpairment; } // todo
        }

        public double[] CPR
        {
            get
            {
                return this.productData.CprValues;
            }
        }

        public double[] CDR
        {
            get { return this.productData.CdrValues; }
        }

        public double[] LGD
        {
            get { return this.productData.LgdValues; }
        }



        public double[] CumulativeDelinquenciesOnInitialNominal
        {
            get { return cumulativeDelinquenciesOnInitialNominal; }
        }

        public double[] CumulativeLossesOnInitialNominal
        {
            get { return cumulativeLossesOnInitialNominal; }
        }

        public double[] CumulativeCprOnInitialNominal
        {
            get { return cumulativeCPROnInitialNominal; }
        }

        public double[] CumulatedReceivablePayable
        {
            get { return cumulatedReceivablePayable; }
            set { cumulatedReceivablePayable = value; }
        }
    }
}