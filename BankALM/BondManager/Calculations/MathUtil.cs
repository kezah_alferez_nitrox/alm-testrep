﻿using System;

namespace BondManager.Calculations
{
    public static class MathUtil
    {
        public static double GaussianMin(double k, double mu, double sigma)
        {
            if (sigma == 0) return mu;

            return mu + sigma * NormalMin((k - mu) / sigma);
        }

        public static double NormalMin(double a)
        {
            return -1.0 / Math.Sqrt(2 * Math.PI) * Math.Exp(-a * a / 2) + a * (1 - Phi(a));
        }

        public static double Phi(double x)
        {
            const double CDFa = 0.2316419;
            const double CDFb = 0.319381530;
            const double CDFc = -0.356563782;
            const double CDFd = 1.781477937;
            const double CDFe = -1.821255978;
            const double CDFf = 1.330274429;
            const double CDFh = 0.3989422;

            double k = 1.0 / (1.0 + CDFa * Math.Abs(x));

            if (x < -5.0) return 0.0;
            else if (Math.Abs(x) < 3.0e-14) return 0.5;
            else if (x > 5.0) return 1.0;
            else
            {
                double g = k * k;
                double temp = 1.0 - CDFh * Math.Exp(-x * x / 2) * (CDFb * k + CDFc * g + CDFd * g * k + CDFe * g * g + CDFf * g * g * k);
                if (x > 0.0) return temp;
                else return (1.0 - temp);
            }
        }
    }
}