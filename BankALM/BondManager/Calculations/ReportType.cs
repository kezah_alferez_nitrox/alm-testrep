using Eu.AlterSystems.ASNetLib.Core;

namespace BondManager.Calculations
{
    public enum ReportType
    {
        [EnumDescription("Annually")] Annually,
        [EnumDescription("Annually as calendar year")] AnnuallyAsCalendarYear,
        [EnumDescription("Quarterly")] Quarterly,
        [EnumDescription("Quarterly as calendar year")] QuarterlyAsCalendarYear,
        [EnumDescription("Monthly")] Monthly,
        [EnumDescription("Daily")] Daily,
        // todo [EnumDescription("User Defined")] UserDefined,
        // todo [EnumDescription("Summary")] Summary,
    }

    public enum ReportAggregationType
    {
        Final,
        Average,
        Sum
    }

    public enum ReportAggregationLevel
    {
        Product,
        Category
    }


    public enum ReportAnalysisType
    {
        [EnumDescription("Standard")] Standard,
        [EnumDescription("Financial Analysis")] FinancialAnalysis
    }

    public enum ResultDetailsFields
    {
        BookValue_OrderByALMID,
        BookValue,
        InvestmentParameter,
        LaggedRecovery,
        Default,
        LossImpairment,
        RemainPrincipal,
        FxPnlIncome,
        GainAndLosesIncome,
        PrincipalIncome,
        InterestIncome,
        CouponIncome,
        CurrFace,
        ActuarialPrice,
        BookPrice
    }
}