﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using ALMS.Products;
using NCalc;
using NCalc.Domain;

namespace BondManager.Calculations
{
    public class CouponExpressionEvaluator : ICouponExpressionEvaluator
    {
        private readonly DateTime valuationDate;
        private readonly IDictionary<string, double[]> variables;
        private readonly IEnumerable<IFundamentalVector> fundamentalVectors;
        private readonly bool isDaily;
        // private IDictionary<string, double> variances;
        private const string MinimumFunctionName = "MIN";
        private const string MaximumFunctionName = "MAX";
        public CouponValorisationType Type { get; set; }

        private readonly ConcurrentDictionary<int, ConcurrentDictionary<string, CouponResult>> cacheFundamental = new ConcurrentDictionary<int, ConcurrentDictionary<string, CouponResult>>();
        private readonly ConcurrentDictionary<int, ConcurrentDictionary<string, double>> cacheCoupon = new ConcurrentDictionary<int, ConcurrentDictionary<string, double>>();

        public CouponExpressionEvaluator(DateTime valuationDate, IDictionary<string, double[]> variables, IEnumerable<IFundamentalVector> fundamentalVectors, bool isDaily)
        {
            this.valuationDate = valuationDate;
            this.variables = variables;
            this.fundamentalVectors = fundamentalVectors;
            this.isDaily = isDaily;
        }

        //private void ComputeVariances()
        //{
        //    if (variances != null) return;
        //    variances = new Dictionary<string, double>(32, StringComparer.InvariantCultureIgnoreCase);
        //    foreach (KeyValuePair<string, double[]> pair in variables)
        //    {
        //        variances.Add(pair.Key, CalculateVariance(pair.Value));
        //    }
        //}

        public double Evaluate(string formula, DateTime fixingDate, IDiscountCurve discountCurve)
        {
            if (string.IsNullOrEmpty(formula))
            {
                return 0;
            }

            //ComputeVariances();

#if !DONT_CACHE_RESULTS
            double value;

            int key = this.GetDaysSinceDate(this.valuationDate, fixingDate) * 366 * 100 +
                this.GetDaysSinceDate(this.valuationDate, discountCurve.RefDate);

            string f = formula;

            ConcurrentDictionary<string, double> cache1;
            if (!this.cacheCoupon.TryGetValue(key, out cache1))
                this.cacheCoupon[key] = cache1 = new ConcurrentDictionary<string, double>();

            if (cache1.TryGetValue(f, out value))
                return value;

            if (!double.TryParse(formula, out value))
            {

                formula = Tools.PreProcessExpression(formula);

                ConcurrentDictionary<string, CouponResult> cache2;
                if (!this.cacheFundamental.TryGetValue(key, out cache2))
                    this.cacheFundamental[key] = cache2 = new ConcurrentDictionary<string, CouponResult>();
                value = this.EvaluateCoupon(formula, fixingDate, discountCurve, cache2).Value;

            }

            if (double.IsNaN(value))
                throw new Exception("Error during evaluation formula " + formula + " for date " + fixingDate.ToString("dd MMM yyyy"));
            cache1[f] = value;

            return value;
#else
           formula = Tools.PreProcessExpression(formula);
           return this.EvaluateCoupon(formula, fixingDate, discountCurve, new ConcurrentDictionary<string, CouponResult>()).Value;
#endif
        }

        private int GetDaysSinceDate(DateTime date1, DateTime date2)
        {
            return (int) (date1-date2).TotalDays;
        }

        public CouponResult EvaluateCoupon(string formula, DateTime fixingDate, IDiscountCurve discountCurve, ConcurrentDictionary<string, CouponResult> cache)
        {
            LogicalExpression logicalExpression = Expression.Compile(formula, false);
            return this.EvaluateExpression(logicalExpression, fixingDate, discountCurve, cache);
        }

        protected CouponResult EvaluateExpression(LogicalExpression expression, DateTime fixingDate, IDiscountCurve discountCurve, ConcurrentDictionary<string, CouponResult> cache)
        {
            CouponResult result;
            /*if (cache.TryGetValue(expression.ToString(), out result))
                return result;*/

            result = new CouponResult();
            DateTime date0 = this.valuationDate.AddDays(-1);
            int month = PricerUtil.DateDiffInSteps(discountCurve.RefDate, date0, isDaily);

            if (month < 0)
                month = 0;

            if (expression is BinaryExpression)
            {
                result = new CouponResult();
                BinaryExpression binaryExpression = (BinaryExpression)expression;
                CouponResult left = EvaluateExpression(binaryExpression.LeftExpression, fixingDate, discountCurve, cache);
                CouponResult right = EvaluateExpression(binaryExpression.RightExpression, fixingDate, discountCurve, cache);

                switch (binaryExpression.Type)
                {
                    case BinaryExpressionType.Plus: result = left + right; break;
                    case BinaryExpressionType.Times: result = left * right; break;
                    case BinaryExpressionType.Minus: result = left - right; break;
                    case BinaryExpressionType.Div: result = left / right; break;
                    default: throw new InvalidOperationException("This kind of operation is not managed");
                }
            }
            else if (expression is Function)
            {
                result = new CouponResult();
                Function function = (Function)expression;
                string name = function.Identifier.Name.ToUpperInvariant();

                if (name == MinimumFunctionName || name == MaximumFunctionName)
                {
                    if (function.Expressions.Length != 2)
                        throw new EvaluationException("This function must have two parameters");
                    CouponResult left = EvaluateExpression(function.Expressions[0], fixingDate, discountCurve, cache);
                    CouponResult right = EvaluateExpression(function.Expressions[1], fixingDate, discountCurve, cache);

                    if (name == MinimumFunctionName) result = Min(left, right);
                    else if (name == MaximumFunctionName) result = Max(left, right);
                }
                else
                {

                    result = new CouponResult();
                    if (function.Expressions.Length != 1) throw new EvaluationException("This function must have one parameter");
                    Expression functionExpression = new Expression(function);
                    try
                    {
                        result.Value = (double)functionExpression.Evaluate();
                        result.Type = CouponType.Constant;
                        result.Variance = 0;
                    }
                    catch (EvaluationException ex)
                    {
                        throw new EvaluationException("Only constants values are allowed in this function", ex);
                    }
                }
            }
            else if (expression is Identifier)
            {
                Identifier identifier = (Identifier)expression;

                IFundamentalVector fundamentalVector = null;
                foreach (IFundamentalVector vector in fundamentalVectors)
                {
                    if (string.Equals(vector.VariableName, identifier.Name, StringComparison.InvariantCultureIgnoreCase))
                    {
                        fundamentalVector = vector;
                        break;
                    }
                }

                if (fundamentalVector != null)
                {

                    if (cache.TryGetValue(expression.ToString(), out result))
                        return result;

                    //if (!variables.ContainsKey(identifier.Name) || !variances.ContainsKey(identifier.Name))
                    //    throw new EvaluationException("Variable " + identifier.Name + " is not defined");

                    //DateTime endDate = OffsetDateByFundamentalVector(fixingDate.AddDays(1), fundamentalVector).AddDays(-1);
                    double liborDuration = (fixingDate.Year * 12 + fixingDate.Month - discountCurve.RefDate.Year * 12 - discountCurve.RefDate.Month) / 12.0;
                    result.Variance = Convert.ToDouble(fundamentalVector.VolatilityValues[month]);// *Convert.ToDouble(fundamentalVector.VolatilityValues[month]) * liborDuration;
                    result.Variance = result.Variance * result.Variance;
                    result.Variance *= liborDuration;

                    result.Type = CouponType.Variable;

                    /* if (fundamentalVector.Type == FundamentalVectorType.Libor)
                     {

                         result.Value = discountCurve.Forward(discountCurve.RefDate , fixingDate, endDate) / DayCounter.GetCvg(fixingDate, endDate, fundamentalVector.DateConvention);
                     }
                     else if (fundamentalVector.Type == FundamentalVectorType.Swap)
                     {

                         result.Value = discountCurve.ComputeForwardCoupon(this, fundamentalVector, fixingDate);
                    
                     }*/
                    if (fundamentalVector.Type == FundamentalVectorType.Swap || fundamentalVector.Type == FundamentalVectorType.Libor)
                        result.Value = discountCurve.ComputeForwardCoupon(this, fundamentalVector, fixingDate);
                    else
                        throw new EvaluationException("Variable " + identifier.Name + " type not recognized");

                    cache[expression.ToString()] = result;

                }
                else
                {
                    result.Value = variables[identifier.Name][month];
                    result.Variance = 0;
                    result.Type = CouponType.Constant;
                }
            }
            else if (expression is ValueExpression || expression is UnaryExpression)
            {
                if (expression is ValueExpression) result.Value = Convert.ToDouble(((ValueExpression)expression).Value);
                else if (expression is UnaryExpression)
                {
                    result.Value = Convert.ToDouble(new Expression(expression).Evaluate());
                }
                result.Type = CouponType.Constant;
                result.Variance = 0;
            }
            else throw new InvalidOperationException("Invalid expression : " + expression);


            return result;
        }

        private static DateTime OffsetDateByFundamentalVector(DateTime date, IFundamentalVector fundamentalVector)
        {
            if (fundamentalVector == null) return date;

            switch (fundamentalVector.Unit)
            {
                case TimeUnit.Days:
                    return date.AddDays(fundamentalVector.Delta);
                case TimeUnit.Months:
                    return date.AddMonths(fundamentalVector.Delta);
                case TimeUnit.Years:
                    return date.AddYears(fundamentalVector.Delta);
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        protected static double CalculateVariance(double[] values)
        {
            double[] squaredValues = new double[values.Length];
            for (int i = 0; i < values.Length; i++) squaredValues[i] = values[i] * values[i];
            double average = CalculateAverage(values);
            return CalculateAverage(squaredValues) - average;
        }

        protected static double CalculateAverage(double[] values)
        {
            double sum = 0;
            foreach (double value in values) sum += value;
            return sum / values.Length;
        }

        protected static CouponResult Min(CouponResult a, CouponResult b)
        {
            CouponResult result = new CouponResult();
            if (a.Type == CouponType.Constant && b.Type != CouponType.Constant) return Min(b, a);
            else if (b.Type == CouponType.Constant && b.Value == 0)
            {
                if (a.Variance == 0)
                {
                    result.Value = Math.Min(a.Value, b.Value);
                    result.Variance = 0;
                    result.Type = CouponType.Constant;
                }
                else if (a.Type == CouponType.Function) throw new EvaluationException("Function evaluation is impossible in Min function");
                else
                {
                    result.Value = MathUtil.GaussianMin(0, a.Value, Math.Sqrt(a.Variance));
                    result.Variance = 0;
                    result.Type = CouponType.Function;
                }
            }
            else return Min(a - b, CouponResult.Null) + b;
            return result;
        }

        protected static CouponResult Max(CouponResult a, CouponResult b)
        {
            if (a.Type == CouponType.Constant && b.Type != CouponType.Constant) return Max(b, a);
            else if (b.Type == CouponType.Constant && b.Value == 0) return CouponResult.Null - Min(CouponResult.Null - a, CouponResult.Null);
            else return Max(a - b, CouponResult.Null) + b;
        }

        [DebuggerDisplay("Type = {Type}, Value = {Value}, Variance = {Variance}")]
        public struct CouponResult
        {
            public double Value;
            public double Variance;
            public CouponType Type;

            public static CouponResult operator +(CouponResult a, CouponResult b)
            {
                CouponResult result = new CouponResult();
                result.Value = a.Value + b.Value;
                result.Variance = a.Variance + b.Variance;
                if (a.Type == CouponType.Constant && b.Type == CouponType.Constant) result.Type = CouponType.Constant;
                else if (a.Type == CouponType.Function || b.Type == CouponType.Function) result.Type = CouponType.Function;
                else result.Type = CouponType.Variable;
                return result;
            }

            public static CouponResult operator *(CouponResult a, CouponResult b)
            {
                CouponResult result = new CouponResult();
                if (a.Type == CouponType.Constant && b.Type == CouponType.Constant ||
                    a.Type == CouponType.Constant && b.Type == CouponType.Function)
                {
                    result.Value = a.Value * b.Value;
                    result.Variance = 0;
                    result.Type = CouponType.Constant;
                }
                else if (b.Type == CouponType.Constant && a.Type != CouponType.Constant) return b * a;
                else if (b.Type == CouponType.Variable && a.Type == CouponType.Variable) throw new EvaluateException("Multiply two variables is impossible");
                else if (a.Type == CouponType.Constant && (b.Type == CouponType.Variable))
                {
                    result.Value = a.Value * b.Value;
                    result.Variance = a.Value * a.Value * b.Variance;
                    result.Type = b.Type;
                }
                else throw new EvaluateException("Impossible operation");
                return result;
            }

            public static CouponResult operator /(CouponResult a, CouponResult b)
            {
                CouponResult invB = b;
                invB.Value = 1 / invB.Value;
                return a * invB;
            }

            public static CouponResult operator -(CouponResult a, CouponResult b)
            {
                CouponResult oppB = b;
                oppB.Value = -oppB.Value;
                return a + oppB;
            }

            public static CouponResult Null
            {
                get
                {
                    CouponResult couponResult = new CouponResult();
                    couponResult.Value = 0;
                    couponResult.Variance = 0;
                    couponResult.Type = CouponType.Constant;
                    return couponResult;
                }
            }
        }

        public enum CouponType
        {
            Constant,
            Variable,
            Function
        }
    }
}
