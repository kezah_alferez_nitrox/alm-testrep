using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using ALMS.Products;
using ALMSCommon.Threading;
using BondManager.Domain;
using BondManager.Resources.Language;
using Iesi.Collections.Generic;
using ALMSCommon;

namespace BondManager.Calculations
{
    public class ProductData
    {
        private Product product;
        private readonly DateTime startDate;
        private readonly DateTime valuationDate;
        private int startStep;
        private int impactTreasuryStep;
        protected IList<IFundamentalVector> fundamentalVectors;
        protected ExpressionParser parser;
        private readonly bool shouldUpdateBookPrice;
        private IDiscountCurveCollection discountCurves;
        private int recoveryLagInMonths;
        private readonly double[] fxRate;



        private double[] cashflowProductFace;

        // Face values
        // nominal du produit en fin de mois 
        private readonly double[] currFace;

        private readonly double[] receivable;

        // growth du produit sur le de mois 
        private readonly double[] growth;

        // proportion du nominal qui n�est ni en d�faut, ni pr�pay�
        private readonly double[] liveFace;
        // proportion du principal initial non encore rembours� 
        private readonly double[] remainPrincipal;

        //PRICE values (MULTIPLIE PAR currFace [i])
        // prix dans le book marginal en fin de mois
        private readonly double[] bookPrice;
        // prix actualis�  marginal en fin de mois
        private readonly double[] actuarialPrice;
        // Accrued interest marginal en fin de mois couponIncome[Param.ResultSteps] : coupon re�us entre fin de mois et mois pr�c
        private readonly double[] marginAccruedInterest;

        // Income Values
        // Accrued interest en fin de mois couponIncome[Param.ResultSteps] : coupon re�us entre fin de mois et mois pr�c 
        //private readonly double[] accruedInterest = new double[Param.ResultSteps];
        // coupon re�us entre fin de mois et mois pr�c 
        private readonly double[] couponIncome;

        private readonly double[] ircValue;

        // Principal re�us entre  fin de mois et mois pr�c
        private readonly double[] principalIncome;
        // Losses entre fin de mois et mois pr�c�dent 
        private readonly double[] lossImpairment;

        private readonly double[] recovery;
        // Gain provenant des int�rets + amortissement
        private readonly double[] interestIncome;
        // Application Fees
        private readonly double[] applicationFee;
        // Application Charges
        private readonly double[] applicationCharge;

        // Prepayerment Fees
        private readonly double[] prepayementFee;



        // Gain provenant du P&L d�falqu� des amortissements
        private readonly double[] gainAndAndLossesIncome;

        //Gain des �changes de currencies
        private readonly double[] fxPNLIncome;

        private double[] marketBasis;

        private double[] investmentParameter;
        private double[] couponValues;
        private double[] rollRatioValues;

        private double[] cprValues;
        private double[] cdrValues;
        private double[] lgdValues;
        private double[] reserve_actualization_rate;
        private double[] reserve_cashflows;

        private ModelData modelData;

        private Iesi.Collections.Generic.ISet<Vector> usedVectors = new HashedSet<Vector>();
        private Iesi.Collections.Generic.ISet<Variable> usedVariables = new HashedSet<Variable>();
        private readonly List<ValidationMessage> validationErrors = new List<ValidationMessage>();
        private readonly List<ValidationMessage> validationWarnings = new List<ValidationMessage>();

        private readonly string coupon;
        private CashflowProduct cashflowProduct;
        private double yield;
        public double firstRateValue;
        public double[] Spread;
        public double initialSpread;

        private ICouponExpressionEvaluator valorisationEvaluator;
        private IFormulaEvaluator standardEvaluator;


        protected IDictionary<string, object>[] vectorAndVariableValues;
        protected IDictionary<string, double[]> namedVariableValues;
        protected ThreadSafeDictionary<string, CashflowProduct> allCashFlowProduct;

        private readonly int resultSteps;
        protected readonly bool IsDaily;

        public IList<ProductData> LinkedProductDatas { get; set; }

        public Mouvements[] Mouvements { get; set; }

        public double[] RealCouponRate;
        public double[] RealIRCRate;
        public double[] GapNoMaturityRate;
        private readonly double[] laggedRecovery;
        private readonly double[] modelDefault;
        private readonly double[] _amortizationValues;
        private double[] ircPercentageValues;
        private bool originStartDateForModel;

        public ProductData(Product product, DateTime startDate, DateTime valuationDate, int startStep, IList<IFundamentalVector> fundamentalVectors, ExpressionParser parser, ICouponExpressionEvaluator valorisationEvaluator, IFormulaEvaluator standardEvaluator, IDiscountCurveCollection discountCurves, ThreadSafeDictionary<string, CashflowProduct> allCashFlowProduct, bool shouldUpdateBookPrice, int resSteps, bool isDaily,bool originStartDateForModel)
            : this(resSteps)
        {
            this.resultSteps = resSteps;
            this.IsDaily = isDaily;
            ImpactTreasuryStep = -1;
            this.product = product;
            this.startDate = startDate;
            this.valuationDate = valuationDate;
            this.startStep = startStep;
            this.fundamentalVectors = fundamentalVectors;
            this.parser = parser;
            this.shouldUpdateBookPrice = shouldUpdateBookPrice;

            this.coupon = product.Coupon;

            this.valorisationEvaluator = valorisationEvaluator;
            this.standardEvaluator = standardEvaluator;
            this.reserve_actualization_rate = new double[resultSteps];
            this.reserve_cashflows = new double[resultSteps];

            this.discountCurves = discountCurves;
            this.allCashFlowProduct = allCashFlowProduct;

            this._amortizationValues = new double[resultSteps];
            this.product.GetFaceFunction = this.GetFace;
            this.OriginStartDateForModel = originStartDateForModel;

            if (string.IsNullOrEmpty(this.coupon))
            {
                this.coupon = "0";
            }

            if (product.IsModelAndNotDerivative)
            {
                this.cprValues = new double[resultSteps];
                this.cdrValues = new double[resultSteps];
                this.lgdValues = new double[resultSteps];
                this.modelData = new ModelData(this, resultSteps);
            }
        }

        public ProductData()
        {
        }

        public ProductData(int resultMonths)
        {
            fxRate = new double[resultMonths];

            currFace = new double[resultMonths];
            liveFace = new double[resultMonths];
            remainPrincipal = new double[resultMonths];
            bookPrice = new double[resultMonths];
            actuarialPrice = new double[resultMonths];
            marginAccruedInterest = new double[resultMonths];
            receivable = new double[resultMonths];
            couponIncome = new double[resultMonths];
            ircValue = new double[resultMonths];
            growth = new double[resultMonths];
            applicationFee = new double[resultMonths];
            applicationCharge = new double[resultMonths];
            laggedRecovery = new double[resultMonths];
            modelDefault = new double[resultMonths];
            prepayementFee = new double[resultMonths];
            principalIncome = new double[resultMonths];
            lossImpairment = new double[resultMonths];
            recovery = new double[resultMonths];
            interestIncome = new double[resultMonths];
            gainAndAndLossesIncome = new double[resultMonths];
            this.fxPNLIncome = new double[resultMonths];
            investmentParameter = new double[resultMonths];
            couponValues = new double[resultMonths];
            ircPercentageValues = new double[resultMonths];
            rollRatioValues = new double[resultMonths];
            this.marketBasis = new double[resultMonths];
            Spread = new double[resultMonths];
            RealCouponRate = new double[resultMonths];
            RealIRCRate = new double[resultMonths];
            GapNoMaturityRate = new double[resultMonths];
        }

        public void SetDiscoutCurves(IDiscountCurveCollection newDiscountCurves)
        {
            this.discountCurves = newDiscountCurves;
        }

        private DateTime GetStepStartDate(int step)
        {
            if (AlmConfiguration.IsComputationDaily())
            {
                return this.valuationDate.AddDays(step - 1).AddDays(-1);
            }

            DateTime stepStartDate = this.valuationDate.AddMonths(step - 1).AddDays(-1);


            //is the current start date within the month
            if (this.startDate > stepStartDate && this.startDate < stepStartDate.AddMonths(1))
                stepStartDate = this.startDate;

            return stepStartDate;

        }

        private DateTime GetStepEndDate(int step)
        {
            if (AlmConfiguration.IsComputationDaily())
            {
                return this.valuationDate.AddDays(step).AddDays(-1);
            }
            else
            {
                DateTime stepEndDate = this.valuationDate.AddMonths(step).AddDays(-1);
                DateTime productExpirationDate = this.product.GetExpiryDate(valuationDate);
                /*if (productExpirationDate < stepEndDate && productExpirationDate > stepEndDate.AddMonths(-1))
                    stepEndDate = productExpirationDate;*/
                //MTU : cas des rolls duration=1

                return stepEndDate;
            }
        }



        private DateTime RealCashFlowStartDate
        {
            get { return this.startDate; }
            //this.product.Attrib == ProductAttrib.InFuture ? this.startDate.AddDays(1).AddMonths(1).AddDays(-1) : this.startDate; }
        }

        private void ImplyYield()
        {
            try
            {
                GoalSeekCalculator.Result result = GoalSeekCalculator.Calculate(this.YieldGoal, 0.5);
                if (result.Kind == GoalSeekCalculator.ApproximateResultKind.NoSolutionWithinTolerance)
                {
                    this.validationErrors.Add(
                        new ValidationMessage(Labels.ProductData_ImplyYield_No_solution_found_for_ImplyYield));
                    this.yield = 0;
                }
                else
                {
                    this.yield = result.Value;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
                // todo : set to defaut or warn user ?!
            }
        }

        private double YieldGoal(double goalYield)
        {
            double price = this.CashflowProduct.ActuarialPrice(this.cashflowProductFace,
                this.startDate, goalYield, this.valorisationEvaluator, this.standardEvaluator, this.discountCurves);
            double actuarial = double.IsNaN(price) ? 0 : price;

            return (actuarial - this.actuarialPrice[this.startStep] / 100); // - this.AccruedInterest[0]);
        }

        public virtual double GetBookValueInValuationCurrency(int step)
        {
            return this.GetBookValue(step) * fxRate[step];
        }

        public virtual double GetBookValue(int step)
        {
            if (this.product.Attrib == ProductAttrib.Notional /*|| this.product.IsDerivative*/)
            {
                return 0;
            }

            return this.CurrFace[step] * this.GetBookPrice(step);
        }

        public virtual double GetAbsoluteBookValue(int step)
        {
            return this.CurrFace[step] * this.GetBookPrice(step);
        }

        public virtual double[] GetBookValues()
        {
            double[] result = new double[this.CurrFace.Length];
            for (int i = 0; i < result.Length; i++)
            {
                result[i] = this.GetBookValue(i);
            }

            return result;
        }

        //private double GetGapValue(int step, bool residual)
        //{
        //    if (!residual)
        //    {
        //        if (this.product.AmortizationType == ProductAmortizationType.NO_MATURITY)
        //            return this.currFace[step - 1];
        //        else
        //            return 
        //    }
        //}

        public double GetGap(int gapPeriodIndex, int step, int cashflowPresentation, bool lastGap = false)
        {
            int index = step + gapPeriodIndex;

            if (lastGap)
            {//used for gap de taux for the last value
                return step > 0 ? this.currFace[gapPeriodIndex] * this.product.Sign : 0;
            }

            if (this.product.AmortizationType == ProductAmortizationType.NO_MATURITY)
            {
                double coef;
                coef = GetGapCoef(step, cashflowPresentation);

                return step > 0 ? coef * this.currFace[gapPeriodIndex] * this.product.Sign : 0;
            }

            if (this.product.Attrib == ProductAttrib.Roll)
            {
                RollProductData rp = this as RollProductData;
                double sum = 0;
                foreach (ProductData subRolspd in rp.SubRollDatas)
                {
                    if (gapPeriodIndex >= subRolspd.startStep && index < resultSteps)
                        sum += subRolspd.principalIncome[index] * this.product.Sign;
                }
                return sum;
            }
            else
            {
                if (index < resultSteps && !double.IsNaN(this.principalIncome[index]) &&
                    (this.product.Attrib != ProductAttrib.InFuture || gapPeriodIndex > this.startStep))

                    if (cashflowPresentation == Param.GAP_STOCK)
                        return this.GetBookValue(index) * this.product.Sign;
                    else
                        return this.principalIncome[index] * this.product.Sign;
                //GAP : si Treasury alors principalIncome, si Sotck BookValue
            }
            return 0.0;
        }

        public double GetGapCoef(int step, int cashflowPresentation)
        {
            double coef;
            if (step > 0)
            {
                if (cashflowPresentation != Param.GAP_STOCK)
                    coef = this.GapNoMaturityRate[step - 1] - this.GapNoMaturityRate[step];
                else
                    coef = this.GapNoMaturityRate[step];
            }
            //GAP : MTU take in account CASHFLOW PRESENTATION, if Stock no -this.GapNoMaturityRate[step]
            else
                coef = 0;
            return coef;
        }

        public double GetBookPrice(int step)
        {
            if (this.product.ProductType == ProductType.StockEquity)
            {
                return this.BookPrice[step];
            }
            else
            {
                return this.BookPrice[step] / 100;
            }
        }

        public double GetBookDirtyPrice(int step)
        {
            if (this.product.ProductType == ProductType.StockEquity)
            {
                return this.BookPrice[step] + this.MarginAccruedInterest[step];
            }
            else
            {
                return this.BookPrice[step] / 100 + this.MarginAccruedInterest[step];
            }
        }

        public virtual double GetDirtyBookValue(int step)
        {
            return this.GetBookDirtyPrice(step) * this.CurrFace[step];
        }

        public virtual double GetAccruedInterest(int step)
        {
            double face = step == 0 ? this.CurrFace[0] : this.CurrFace[step - 1];
            if (this.product.InvestmentRule == ProductInvestmentRule.Growth)
                face += this.growth[step];
            if (step == 0 || !product.IsModelAndNotDerivative)
            {
                return face * this.MarginAccruedInterest[step];
            }
            if (receivable[step] != 0) return receivable[step];
            receivable[step] = GetAccruedInterest(step - 1) + this.InterestIncome[step] - this.CouponIncome[step] - GetAccruedActuarialValueWithPrincipalIncomeAndMonthlyDefault(step);
            return receivable[step];
        }


        public virtual void Run(int step)
        {
            DateTime stepStartDate = this.GetStepStartDate(step);

            DateTime expiryDate = ((IProduct)product).GetExpiryDate(this.valuationDate);

            if (this.product.AmortizationType != ProductAmortizationType.ADJUSTABLE_CAPPED_TERM &&
                this.product.AmortizationType != ProductAmortizationType.ADJUSTABLE_TERM &&
                (stepStartDate > expiryDate && this.product.Category.Type != CategoryType.Roll)) return;

            Util.setDefaultCultureInfoToThread();

            if (step < startStep)
            {
                //if (product.Attrib == ProductAttrib.InBalance && step > 0)
                //{
                //    this.currFace[step] = this.currFace[step] - 1;
                //    this.bookPrice[step] = this.bookPrice[step] - 1;
                //    this.actuarialPrice[step] = this.actuarialPrice[step] - 1;
                //}
                return;
            }

            if (step == this.startStep)
            {
                this.RunStartStep();
                return;
            }

            if (this.product.IsCashflowProduct)
            {
                if (this.RemainPrincipal[step - 1] != 0)
                {
                    this.RunCashFlow(step);
                }
            }
            else
            {
                throw new Exception("NonCashflow Product !");
                //this.RunNonCashflow(step);
            }

            ComputeFxPnlGainAndLosses(step);
        }

        //private void RunNonCashflow(int step)
        //{
        //    this.marginAccruedInterest[step] = 0;
        //    this.RemainPrincipal[step] = this.RemainPrincipal[step - 1];

        //    int expiryStep = PricerUtil.GetProductExpiryStep(product, this.valuationDate, IsDaily);

        //    if (step <= expiryStep && this.product.ProductType == ProductType.Swaption)
        //    {
        //        this.actuarialPrice[step] = this.actuarialPrice[step - 1] * (expiryStep - step) / (expiryStep - (step - 1)); //amortissement lin�aire
        //        if (this.product.Accounting == ProductAccounting.AFS || this.product.Accounting == ProductAccounting.HFT
        //            || step == expiryStep)
        //        {
        //            this.bookPrice[step] = this.GetSwaptionMarketPrice(step, this.product.IntDuration);
        //        }
        //        else
        //        {
        //            this.bookPrice[step] = this.actuarialPrice[step];
        //        }

        //        this.bookPrice[step] = this.bookPrice[step - 1];
        //        this.LiveFace[step] = this.LiveFace[step - 1];
        //        this.currFace[step] = this.currFace[step - 1];

        //    }
        //    else
        //    {
        //        this.actuarialPrice[step] = 0;
        //        this.bookPrice[step] = 0;
        //        this.LiveFace[step] = 0;
        //        this.currFace[step] = 0;
        //    }

        //    this.interestIncome[step] = this.CurrFace[step] * (this.actuarialPrice[step] / 100) - this.CurrFace[step - 1] * (this.actuarialPrice[step - 1] / 100);

        //    this.gainAndAndLossesIncome[step] = this.CurrFace[step] * (this.bookPrice[step] - this.actuarialPrice[step]) / 100 -
        //                                        this.CurrFace[step - 1] * (this.bookPrice[step - 1] - this.actuarialPrice[step - 1]) / 100;

        //    this.ComputePrepayementFees(step, GetStepEndDate(step));

        //}

        private double GetSwaptionMarketPrice(int step, int durationMonths)
        {
            DateTime stepDate = GetStepEndDate(step);
            DateTime expiryDate = this.valuationDate.AddMonths(durationMonths).AddDays(-1);

            double p = this.LinkedProductDatas.Sum(pd => pd.bookPrice[step]);

            double d = 0;// this.cashflowProduct.MarketDuration(this.cashflowProductFace, stepDate, this.marketBasis[step], this.initialSpread, this.valorisationEvaluator,
            //                                   this.standardEvaluator, this.discountCurves);
            foreach (ProductData pd in this.LinkedProductDatas)
            {
                if (pd.Product.CouponType != CouponType.Floating)
                {
                    d += pd.CashflowProduct.MarketDuration(pd.cashflowProductFace,
                                                                stepDate, pd.marketBasis[step],
                                                                    pd.initialSpread,
                                                                    pd.valorisationEvaluator,
                                                                    pd.standardEvaluator,
                                                                    pd.discountCurves)
                          / pd.RemainPrincipal[step];
                }
            }

            double totalMonths = durationMonths - step;

            if (totalMonths < 0)
            {
                Log.Warn("GetSwaptionMarketPrice: startDate > stepDate");
                Debug.Fail("GetSwaptionMarketPrice: startDate > stepDate");
            }

            double v = GetMarketDataVolatility(step, expiryDate) * Math.Sqrt(totalMonths / 12);

            //price is in base 100 so volatility should be corrected
            double price = (p - MathUtil.GaussianMin(p, 0, 100 * d * v));

            return price;
        }

        private double GetMarketDataVolatility(int step, DateTime date)
        {
            if (fundamentalVectors.Count == 0) return 0;

            int i1 = fundamentalVectors.Count - 1;
            while (PricerUtil.OffsetDateByFundamentalVector(startDate, fundamentalVectors[i1]) > date)
            {
                i1--;
            }

            int i2 = 0;
            while (PricerUtil.OffsetDateByFundamentalVector(startDate, fundamentalVectors[i2]) < date)
            {
                i2++;
            }

            IFundamentalVector v1 = fundamentalVectors[i1];
            IFundamentalVector v2 = fundamentalVectors[i2];

            double x = (date - valuationDate).TotalDays;
            double x1 = (PricerUtil.OffsetDateByFundamentalVector(this.startDate, v1) - this.valuationDate).TotalDays;
            double x2 = (PricerUtil.OffsetDateByFundamentalVector(this.startDate, v2) - this.valuationDate).TotalDays;

            double y1 = v1.VolatilityValues[step];
            double y2 = v2.VolatilityValues[step];

            double result = Interpolate(x1, y1, x2, y2, x);

            return result;
        }

        private static double Interpolate(double x1, double y1, double x2, double y2, double x)
        {
            return y1 * (x - x2) / (x1 - x2) + y2 * (x - x1) / (x2 - x1);
        }

        private void RunStartStep()
        {
            DateTime date = GetStepEndDate(this.startStep);

#if !DONT_CACHE_RESULTS
            string key = "P" + this.product + RealCashFlowStartDate;
            this.cashflowProduct = this.allCashFlowProduct.
                TryGetValueOrAdd(key, () => new CashflowProduct(RealCashFlowStartDate, this.product, this.IsDaily));
#else
            this.cashflowProduct = new CashflowProduct(RealCashFlowStartDate, this.product);
#endif

            IDiscountCurve discountCurve = this.discountCurves.DiscountCurves[this.startStep];

            this.LiveFace[this.startStep] = 1;
            this.RemainPrincipal[this.startStep] = 1;

            //if (product.ProductType == ProductType.Cap)
            //{

            //}

            if (this.valorisationEvaluator == null)
            {
                this.initialSpread = this.standardEvaluator.Evaluate(this.product.Spread, this.startDate);
            }
            else
            {
                this.initialSpread = this.valorisationEvaluator.Evaluate(this.product.Spread, this.startDate,
                                                                         discountCurve);
            }

            this.firstRateValue = this.CashflowProduct.GetFirstRateValue(date, this.initialSpread,
                                                                             this.valorisationEvaluator,
                                                                             this.standardEvaluator, this.discountCurves);

            this.cashflowProductFace = new double[this.CashflowProduct.NumCashFlows + 1];
            this.CashflowProduct.CalculateFace(this.cashflowProductFace, date, firstRateValue,
                                                                   this.valorisationEvaluator,
                                                                   this.standardEvaluator, this.discountCurves, this.startDate, false, this.ReserveCashflows);


            double actuarialPriceValorisation = string.IsNullOrEmpty(this.product.ActuarialPrice) ? 100 : this.valorisationEvaluator.Evaluate(this.product.ActuarialPrice, startDate, discountCurve);
            
            if (this.product.IsCashflowProduct)
            {
                double currentPrincipal = this.CashflowProduct.CurrentPrincipal(this.cashflowProductFace, date);
                if (this.product.ProductType != ProductType.StockEquity)
                {
                    double accr = this.CashflowProduct.AccruedInterest(this.cashflowProductFace, date, this.initialSpread,
                                                                       this.valorisationEvaluator,
                                                                       this.standardEvaluator, this.discountCurves);
                    //this.AccruedInterest[startStep] = this.currFace[startStep] * (double)accr;
                    this.marginAccruedInterest[this.startStep] = accr;
                    double marketPrice = (100 *
                                               (this.CashflowProduct.MarketPrice(this.cashflowProductFace, date,
                                                                                 this.marketBasis[this.startStep],
                                                                                 this.initialSpread,
                                                                                 this.valorisationEvaluator,
                                                                                 this.standardEvaluator,
                                                                                 this.discountCurves) - accr)) *
                                                                                 this.product.SignIfDerivative;
                    marketPrice = marketPrice / currentPrincipal * product.Sign;


                    if (this.product.StartDate != null && this.product.StartDate < this.valuationDate)
                    {
                        this.actuarialPrice[this.startStep] = actuarialPriceValorisation;

                    }
                    else
                    {
                        
                        //else
                        //{
                        this.actuarialPrice[this.startStep] = string.IsNullOrEmpty(this.product.ActuarialPrice)
                                                                  ? marketPrice
                                                                  : actuarialPriceValorisation;
                        //}
                    }

                    if (this.product.Accounting == ProductAccounting.HFT || this.product.Accounting == ProductAccounting.AFS)
                    {
                        this.bookPrice[this.startStep] = marketPrice;
                        if (product.Attrib == ProductAttrib.InFuture)
                            this.currFace[this.startStep] = this.currFace[this.startStep] / (marketPrice / 100); // ajustement pour que bookvalue[startstep]=valeur saisie

                        if (this.shouldUpdateBookPrice)
                        {
                            this.product.BookPrice = marketPrice.ToString("N4");
                            this.product.ComputeBookValue(marketPrice);
                        }
                        
                    }
                    else // HTM && L&R
                    {
                        this.bookPrice[this.startStep] = this.valorisationEvaluator.Evaluate(this.product.BookPrice, date, discountCurve);

                        if (this.product.StartDate == null || this.product.StartDate > this.valuationDate)
                        {
                            this.actuarialPrice[startStep] = this.bookPrice[startStep];
                        }

                        if (this.shouldUpdateBookPrice && (this.product.StartDate == null || this.product.StartDate > this.valuationDate))
                        {
                            this.product.ActuarialPrice = this.bookPrice[this.startStep].ToString("N4");
                        }
                    }

                    this.ImplyYield();
                    //compute ActuarialPrice in startStep
                    if (this.product.StartDate != null && this.product.StartDate < this.valuationDate)
                    {
                        this.ActuarialPrice[startStep] = 100 *
                                                     this.CashflowProduct.ActuarialPrice(this.cashflowProductFace,
                                                                                         valuationDate,
                                                                                         this.yield,
                                                                                         this.valorisationEvaluator,
                                                                                         this.standardEvaluator,
                                                                                         this.discountCurves)
                                                     / currentPrincipal * this.product.SignIfDerivative;

                    }
                    //if (product.Attrib == ProductAttrib.Roll || product.Attrib == ProductAttrib.InFuture)
                    //{
                    //    this.actuarialPrice[this.startStep] = this.bookPrice[startStep];
                    //}

                }
                else
                {
                    //this.AccruedInterest[startStep] = 0;
                    this.marginAccruedInterest[this.startStep] = 0;
                    this.actuarialPrice[this.startStep] = 0;
                    this.bookPrice[this.startStep] =
                    this.valorisationEvaluator.Evaluate(this.product.BookPrice, date, discountCurve);
                    this.yield = 0;
                }

                this.RemainPrincipal[this.startStep] = currentPrincipal;
            }
            else
            {
                //this.AccruedInterest[startStep] = 0;
                this.marginAccruedInterest[this.startStep] = 0;
                this.actuarialPrice[this.startStep] = this.bookPrice[this.startStep] =
                        this.valorisationEvaluator.Evaluate(this.product.BookPrice, date, discountCurve);

                int expiryStep = PricerUtil.GetProductExpiryStep(product, this.valuationDate, IsDaily);
                if (this.startStep <= expiryStep && this.product.ProductType == ProductType.Swaption)
                {
                    if (this.product.Accounting == ProductAccounting.AFS || this.product.Accounting == ProductAccounting.HFT
                        || this.startStep == expiryStep)
                    {
                        this.bookPrice[this.startStep] = this.GetSwaptionMarketPrice(this.startStep, this.product.IntDuration);
                        this.actuarialPrice[this.startStep] = string.IsNullOrEmpty(this.product.ActuarialPrice) ?
                            this.bookPrice[this.startStep] :
                            actuarialPriceValorisation;
                        if (this.shouldUpdateBookPrice)
                        {
                            this.product.BookPrice = this.bookPrice[this.startStep].ToString("N4");
                            this.product.ComputeBookValue(this.bookPrice[this.startStep]);
                        }
                    }
                    else
                    {
                        this.bookPrice[this.startStep] = this.actuarialPrice[this.startStep];
                    }


                    this.RemainPrincipal[this.startStep] = 1;
                }
            }

            if (!(this.product.IsReceived))
                this.RemainPrincipal[this.startStep] = -this.RemainPrincipal[this.startStep];

            this.CurrFace[this.startStep] *= this.RemainPrincipal[this.startStep];

            this.CouponIncome[this.startStep] = 0;

            this.interestIncome[this.startStep] = 0;
            this.PrincipalIncome[this.startStep] = 0;
            this.lossImpairment[this.startStep] = 0;

            RealIRCRate[startStep] = this.GetRealCouponRate(this.GetStepStartDate(startStep), this.GetStepEndDate(startStep), this.IrcPercentageValues, false);

            if (product.AmortizationType == ProductAmortizationType.RESERVE_CASHFLOW ||
                product.AmortizationType == ProductAmortizationType.RESERVE_NOMINAL)
            {
                for (int i = 0; i < this.cashflowProductFace.Length; i++)
                {
                    this.currFace[i] = this.cashflowProductFace[i];
                }

                CalculateBookPriceForReserves(startStep);
            }
            ComputeIRCValue(startStep);
            ComputeFxPnlGainAndLosses(startStep);
            this.ComputeApplicationFees(startStep, date);
            this.ComputeApplicationCharges(startStep, date);
            this.ComputePrepayementFees(startStep, date);
        }

        private void RunCashFlow(int step)
        {
            DateTime dt1 = this.GetStepStartDate(step);
            DateTime dt2 = this.GetStepEndDate(step);

            IDiscountCurve discountCurve = this.discountCurves.DiscountCurves[step];



            this.CashflowProduct.CalculateFace(this.cashflowProductFace, dt2, this.firstRateValue,
                                                               this.valorisationEvaluator,
                                                               this.standardEvaluator, this.discountCurves, this.startDate, true, this.ReserveCashflows);



            this.RemainPrincipal[step] = this.CashflowProduct.CurrentPrincipal(this.cashflowProductFace, dt2);
            if (!(this.product.IsReceived))
                this.RemainPrincipal[step] = -this.RemainPrincipal[step];

            this.CouponIncome[step] = this.currFace[step - 1] *
                                      this.CashflowProduct.InterestIncome(this.cashflowProductFace, dt1, dt2, this.initialSpread,
                                                                          this.valorisationEvaluator,
                                                                          this.standardEvaluator, this.discountCurves)
                                                              /
                                        this.RemainPrincipal[step - 1];
            this.PrincipalIncome[step] = this.currFace[step - 1] *
                                         this.CashflowProduct.PrincipalIncome(this.cashflowProductFace, dt1, dt2) /
                                         this.RemainPrincipal[step - 1];





            RealCouponRate[step] = this.GetRealCouponRate(dt1, dt2, this.CouponValues);
            RealIRCRate[step] = this.GetRealCouponRate(dt1, dt2, this.IrcPercentageValues, false);

            //if (this.RemainPrincipal[step - 1] != 0)
            //    this.PrincipalIncome[step] = currFace[step - 1] * (1 - remainPrincipal[step] / remainPrincipal[step - 1]);

            if (this.RemainPrincipal[step] != 0)
            {
                if (this.product.ProductType != ProductType.StockEquity)
                {
                    double accr = this.CashflowProduct.AccruedInterest(this.cashflowProductFace, dt2, this.initialSpread,
                                                                       this.valorisationEvaluator,
                                                                       this.standardEvaluator, this.discountCurves);

                    //this.AccruedInterest[step] = this.currFace[step - 1] * accr;
                    this.marginAccruedInterest[step] = accr /
                                                       this.RemainPrincipal[step];


                    //if ((product.Attrib == ProductAttrib.Roll ||  product.Attrib == ProductAttrib.InFuture) && 
                    //    (this.product.Accounting != ProductAccounting.AFS && this.product.Accounting != ProductAccounting.HFT))
                    //{
                    //    this.actuarialPrice[step] = 100;
                    //}
                    //else
                    //{

                    //if (this.actuarialPrice[step - 1] == 100)
                    //{
                    //    this.actuarialPrice[step] = 100;
                    //}
                    //else
                    //{
                        this.actuarialPrice[step] = 100 *
                                                    (this.CashflowProduct.ActuarialPrice(this.cashflowProductFace, dt2, this.yield, this.valorisationEvaluator, this.standardEvaluator, this.discountCurves) /
                                                     this.RemainPrincipal[step]); // - accr);
                    //}
                    //}
                    if (this.product.Accounting == ProductAccounting.AFS ||
                        this.product.Accounting == ProductAccounting.HFT)
                    {

                        this.bookPrice[step] = 100 *
                                               (this.CashflowProduct.MarketPrice(this.cashflowProductFace,
                                                                                 dt2, this.marketBasis[step],
                                                                                 this.initialSpread,
                                                                                 this.valorisationEvaluator,
                                                                                 this.standardEvaluator,
                                                                                 this.discountCurves) /
                                                this.RemainPrincipal[step] - accr); // * 
                        //product.SignIfDerivative;

                    }
                    else
                    {
                        this.bookPrice[step] = this.actuarialPrice[step];
                    }
                }
                else
                {
                    //this.AccruedInterest[step] = 0;
                    this.marginAccruedInterest[step] = 0;
                    this.actuarialPrice[step] = 0;


                }
            }
            else
            {
                this.actuarialPrice[step] = 100 * product.SwapLegSign;
                this.bookPrice[step] = 0;
            }

            if (this.product.ProductType == ProductType.StockEquity)
            {
                this.bookPrice[step] = this.valorisationEvaluator.Evaluate(this.product.BookPrice, dt2,
                                                                               discountCurve);
                this.PrincipalIncome[step] *= this.bookPrice[step];
            }

            if (this.product.IsModelAndNotDerivative)
            {
                this.modelData.Iteration(step,
                                        cashflowProduct.GetLastCouponStep(dt2, valuationDate, product.Attrib),
                                        cashflowProduct.IsCurrentStepCouponStep(dt2),
                                        this.RealCouponRate[step]);

                this.lossImpairment[step] = this.modelData.LossesImpairment[step];

                this.recovery[step] = this.modelData.Recovery[step];
                this.LaggedRecovery[step] = this.modelData.LaggedRecovery[step];
                this.Default[step] = this.modelData.Default[step];


                this.CouponIncome[step] = this.modelData.InterestIncomes[step] + this.modelData.CouponOnPrepayement[step];

                this.PrincipalIncome[step] = this.modelData.NormalReimbursement[step] + this.modelData.Prepayments[step];
                //this.modelData.Default[step]; //modelData.Recovery[step];
                //this.AccruedInterest[step] = this.AccruedInterest[step] * this.LiveFace[step] / this.LiveFace[step - 1];
            }
            else
            {
                this.lossImpairment[step] = 0;
                this.LiveFace[step] = this.LiveFace[step - 1];
            }



            if (this.RemainPrincipal[step - 1] * this.LiveFace[step - 1] != 0)
            {
                if (!this.product.IsModelAndNotDerivative)
                {
                    //formule de marc qu'on ne comprend pas
                    this.currFace[step] = this.RemainPrincipal[step] *
                                            this.LiveFace[step] * this.currFace[step - 1]
                                            / (this.RemainPrincipal[step - 1] * this.LiveFace[step - 1]);
                }
                else
                {
                    //mtu : formule de serge pour le cas des modeles ou la formule de marc �tait une approximation moyenne
                    this.currFace[step] = this.currFace[step - 1]
                                            - this.modelData.Prepayments[step]
                                            - this.modelData.NormalReimbursement[step]
                                            - this.modelData.MonthlyDefault[step];
                }
                //mtu
                if (product.InvestmentRule == ProductInvestmentRule.Growth)
                {
                    this.UpdateGrowth(this.vectorAndVariableValues, step);

                    double dailyCoef = 1.0;
                    if (AlmConfiguration.IsComputationDaily())
                    {
                        DateTime date = valuationDate.AddDays(step - 1);
                        int daysInMonth = DateTime.DaysInMonth(date.Year, date.Month);
                        dailyCoef /= daysInMonth;
                    }

                    this.Growth[step] = currFace[step] * this.investmentParameter[step] / 12 * dailyCoef;
                    this.currFace[step] += this.Growth[step];
                    this.principalIncome[step] -= this.Growth[step] * this.GetBookPrice(step);
                }
            }
            else
            {
                this.currFace[step] = 0;
            }

            if (this.product.ProductType != ProductType.StockEquity)
            {

                if (!this.product.IsModelAndNotDerivative)
                {
                    this.interestIncome[step] = this.CouponIncome[step]
                                                            +
                                                            this.CurrFace[step] * (this.actuarialPrice[step] / 100 + this.marginAccruedInterest[step]) -
                                                            this.CurrFace[step - 1] * (this.actuarialPrice[step - 1] / 100 + this.marginAccruedInterest[step - 1]) +
                                                            this.principalIncome[step];
                }
                else
                {
                    if (cashflowProduct.IsCurrentStepCouponStep(dt2))
                        this.CouponIncome[step] = this.GetAccruedInterest(step - 1) + this.modelData.SubCouponIncome[step];
                    else
                        this.CouponIncome[step] = this.modelData.MonthlyCouponOnPrepayement[step];

                    this.interestIncome[step] = this.modelData.SubCouponIncome[step] +
                                                GetAccruedActuarialValueWithPrincipalIncomeAndMonthlyDefault(step);
                }
            }
            else
            {
                this.interestIncome[step] = this.CouponIncome[step];

            }


            if (product.AmortizationType == ProductAmortizationType.RESERVE_CASHFLOW ||
                product.AmortizationType == ProductAmortizationType.RESERVE_NOMINAL)
            {
                CalculateBookPriceForReserves(step);
            }

            ComputeIRCValue(step);
            this.ComputePrepayementFees(step, dt2);
        }


        private void CalculateBookPriceForReserves(int step)
        {
            //first calculate the BookValue, then compute the bookPrice
            double bookValue = Util.GetBookValueForCashflow(step, Math.Min(product.IntDuration, resultSteps), reserve_cashflows, reserve_actualization_rate);

            if (currFace[step] != 0)
                bookPrice[step] = bookValue / currFace[step] * 100;
            else
                bookPrice[step] = 0;
            actuarialPrice[step] = bookPrice[step];
            interestIncome[step] = 0;
            //MTU : if calculate le remainingPrincipal then explosion
            //if (GetBookValue(0) != 0)
            //    remainPrincipal[step] = GetBookValue(step) / GetBookValue(0);
            //else
            //    remainPrincipal[step] = 0;
        }



        private void ComputeIRCValue(int step)
        {
            //if (this.product.ProductType != ProductType.StockEquity)
            //{
            //    if (this.RealCouponRate[step] == 0.0)
            //        this.IRCValue[step] = 0;
            //    else
            //    {
            //        if (this.product.ProductType != ProductType.StockEquity)
            //            this.IRCValue[step] = RealIRCRate[step]*1.0/
            //                                  this.RealCouponRate[step]*1.0*
            //                                  this.interestIncome[step]*1.0;
            //    }
            //}
            //else
            //{
            if (step > startStep)
                this.IRCValue[step] = RealIRCRate[step - 1] * this.GetBookValue(step - 1);
            else
            {
                if (step == startStep && this.product.Attrib != ProductAttrib.InFuture)
                    this.IRCValue[step] = RealIRCRate[step] * this.GetBookValue(step);
                else
                    this.IRCValue[step] = 0;
            }

            //}
        }



        public void ComputeFxPnlGainAndLosses(int step)
        {
            //mtu
            if (step < startStep+1)
            {

                this.FxPnlIncome[step] = 0;
                if (product.Attrib != ProductAttrib.InFuture)
                {
                    this.gainAndAndLossesIncome[step] += this.GetAbsoluteBookValue(step) -
                                                         this.actuarialPrice[step]*this.currFace[step]*fxRate[step]/100;
                }
                else
                {
                    this.gainAndAndLossesIncome[step] = 0;
                }

                return;
            }

            double actuarialPriceVarIncomes = 0;
            if (this.product.ProductType != ProductType.StockEquity)
                actuarialPriceVarIncomes = this.actuarialPrice[step] / 100 * this.CurrFace[step] -
                                           this.actuarialPrice[step - 1] / 100 * this.CurrFace[step - 1] +
                                           this.GetMonthlyDefault(step) +
                                           this.principalIncome[step];

            double bookValueStep = this.GetAbsoluteBookValue(step);
            double bookValuePrevStep = this.GetAbsoluteBookValue(step - 1);
            double bookValueVar = bookValueStep -
                                    bookValuePrevStep +
                                    this.principalIncome[step] +
                                    this.GetMonthlyDefault(step) -
                                    actuarialPriceVarIncomes;

            double allGains = this.fxRate[step] * bookValueStep -
                                this.fxRate[step - 1] * bookValuePrevStep +
                                this.fxRate[step] * (this.principalIncome[step] +
                                                     this.GetMonthlyDefault(step) -
                                                     actuarialPriceVarIncomes);

            double nonFxRelatedBookValueVar = this.fxRate[step] * bookValueVar;

            double fxPart = allGains - nonFxRelatedBookValueVar;

            if (this.product.Accounting != ProductAccounting.AFS || this.product.ProductType != ProductType.StockEquity)
            {
                this.FxPnlIncome[step] = fxPart;
                this.gainAndAndLossesIncome[step] += (allGains - fxPart) / fxRate[step];
            }
            else
            {
                this.FxPnlIncome[step] = 0;
                this.gainAndAndLossesIncome[step] += allGains / fxRate[step];
            }
        }

        public void ComputeApplicationFees(int step, DateTime dt2)
        {
            //application fee
            if (this.HasApplicationFees())
            {
                this.ApplicationFee[step] = this.standardEvaluator.Evaluate(this.product.ApplicationFee, dt2) * this.GetBookValue(step);
            }
        }



        public void ComputeApplicationCharges(int step, DateTime dt2)
        {
            //application fee
            if (this.HasApplicationCharges())
            {
                this.applicationCharge[step] = this.standardEvaluator.Evaluate(this.product.ApplicationCharge, dt2) * this.GetBookValue(step);
            }
        }

        public void ComputePrepayementFees(int step, DateTime dt2)
        {
            //application fee
            if (this.HasPrepayementFees())
            {
                this.PrepayementFee[step] = this.standardEvaluator.Evaluate(this.product.PrePayementFee, dt2) * this.modelData.Prepayments[step];
            }
        }

        public Iesi.Collections.Generic.ISet<Vector> UsedVectors
        {
            get { return this.usedVectors; }
        }

        public Iesi.Collections.Generic.ISet<Variable> UsedVariables
        {
            get { return this.usedVariables; }
        }

        public List<ValidationMessage> ValidationErrors
        {
            get { return this.validationErrors; }
        }
        public List<ValidationMessage> ValidationWarnings
        {
            get { return this.validationWarnings; }
        }


        public void ValidateBond(IDictionary<string, Vector> allVectors, IDictionary<string, Variable> allVariables, DynamicVariable[] dynamicVariables)
        {


            if (this.product.Attrib == ProductAttrib.Notional && this.product.IsModelAndNotDerivative)
            {
                this.validationErrors.Add(new ValidationMessage(this.product, "Notional attribute is not supported for Model products."));
                //todo Daily : g�rer les notionals en Daily ?
            }

            if (this.product.AmortizationType == ProductAmortizationType.NO_MATURITY)
            {
                if (this.product.IntDuration != Util.InfiniteDuration)
                    this.product.Duration = Util.InfiniteDuration;
                if (this.product.StartDate != null)
                    this.product.StartDate = null;
                if (this.product.ExpirationDate != null)
                    this.product.ExpirationDate = null;

            }

            if (this.product.AmortizationType == ProductAmortizationType.RESERVE_CASHFLOW ||
                this.product.AmortizationType == ProductAmortizationType.RESERVE_NOMINAL)
            {
                this.product.Accounting = ProductAccounting.HFT;
                this.product.Frequency = ProductFrequency.Monthly;
            }

            if (this.product.IntDuration == 0)
            {
                //this.validationErrors.Add(new ValidationMessage(this.product, "Product duration is 0, please exclude product."));
                this.product.Exclude = true;
            }



            if (this.product.Attrib == ProductAttrib.InFuture && (this.product.StartDate == null || this.product.StartDate < valuationDate))
            {
                this.validationErrors.Add(new ValidationMessage(this.product, "for the In Future products please set a Start Date after Valuation Date."));
            }

            if (this.product.ProductType == ProductType.Swap && this.product.OtherSwapLeg == null)
            {
                this.validationErrors.Add(new ValidationMessage(this.product, "missing other swap leg"));
            }

            if (product.AmortizationType == ProductAmortizationType.CST_PAY)
            {
                //verif valo du lag
                try
                {
                    if (!string.IsNullOrEmpty(product.Amortization))
                    {
                        int lag = int.Parse(product.Amortization);

                        if (lag > product.IntDuration)
                            this.validationErrors.Add(new ValidationMessage(this.product, "Incorrect number of months for CST_PAY lag in amortization formula, lag cannot be larger than Duration."));
                        else
                            product.LagNoAmortisationPeriodInMonths = lag;
                    }
                }
                catch (Exception e)
                {
                    this.validationErrors.Add(new ValidationMessage(this.product, "Incorrect number of months for CST_PAY lag in amortization formula, cause : " + e.Message));
                }

            }


            this.ValidateBondFormula("IRC", product.IRC, allVectors, allVariables);
            this.ValidateBondFormula("Complement", product.ComplementString, allVectors, allVariables);
            this.ValidateBondFormula("Coupon", this.coupon, allVectors, allVariables, dynamicVariables);
            this.ValidateBondFormula("Spread", this.product.Spread, allVectors, allVariables);
            this.ValidateBondFormula("MarketBasis", this.product.MarketBasis, allVectors, allVariables);

            this.ValidateBondFormula("Amortization", this.product.Amortization, allVectors, allVariables, dynamicVariables);

            this.ValidateBondFormula("BookPrice", this.product.BookPrice, allVectors, allVariables, dynamicVariables);

            this.ValidateBondFormula("RollRatio", this.product.RollRatio, allVectors, allVariables);

            this.ValidateBondFormula("ApplicationFee", this.product.ApplicationFee, allVectors, allVariables);

            this.ValidateBondFormula("ApplicationCharge", this.product.ApplicationCharge, allVectors, allVariables);

            if (this.product.InvestmentRule != ProductInvestmentRule.Constant)
            {
                this.ValidateBondFormula("InvestmentParameter", this.product.InvestmentParameter, allVectors, allVariables, dynamicVariables);
            }



            if (this.product.IsModelAndNotDerivative)
            {
                if (this.product.AmortizationType == ProductAmortizationType.VAR)
                {
                    this.ValidateBondFormula("TheoreticalCurrFace", this.product.TheoreticalCurrFace, allVectors,
                                             allVariables);
                }
                this.ValidateBondFormula("CDR", this.product.CDR, allVectors, allVariables);
                this.ValidateBondFormula("CPR", this.product.CPR, allVectors, allVariables);
                this.ValidateBondFormula("LGD", this.product.LGD, allVectors, allVariables);


                if (this.product.AmortizationType != ProductAmortizationType.VAR &&
                    this.product.Attrib != ProductAttrib.Roll && this.product.OrigFace == null)
                {
                    this.validationErrors.Add(new ValidationMessage(this.product, "OrigFace", ""));
                }

                try
                {
                    if (!string.IsNullOrEmpty(product.RecoveryLag)) int.Parse(product.RecoveryLag);
                }
                catch (Exception)
                {
                    this.validationErrors.Add(new ValidationMessage(this.product, "Incorrect recovery lag for product."));
                }
            }

            // TODO VALIDATION PRODUIT
            //if (this.product.IsCouponFormulaAllowed)
            //{
            //    ValidateBondFormula("Coupon", this.coupon, allVectors, allVariables);
            //}
            //else
            //{
            //    if (!Tools.IsDecimalOrPercentageString(this.coupon))
            //    {
            //        validationErrors.Add(new ValidationMessage(this.product, "Coupon", this.coupon));
            //    }
            //}
            // TODO END VALIDATION PRODUIT

            //if (this.product.AmortizationType == ProductAmortizationType.VAR)
            //{
            //    this.ValidateBondFormula("OrigFace", this.product.OrigFace, allVectors, allVariables);
            //}
            //else
            //{
            if (string.IsNullOrEmpty(this.product.OrigFace))
            {
                this.product.OrigFace = "0";
            }

            if (!Tools.IsDecimalString(this.product.OrigFace))
            {
                this.validationErrors.Add(new ValidationMessage(this.product, "OrigFace", this.product.OrigFace));
            }
            //}

            // TODO VALIDATION PRODUIT
            //if (this.product.ProductType == ProductType.StockEquity)
            //{
            //    ValidateBondFormula("BookPrice", this.product.BookPrice, allVectors, allVariables);
            //}
            //else
            //{
            //    if (string.IsNullOrEmpty(this.product.BookPrice)) this.product.BookPrice = "100";

            //    if (!Tools.IsDecimalString(this.product.BookPrice))
            //    {
            //        validationErrors.Add(new ValidationMessage(this.product, "BookPrice", this.product.BookPrice));
            //    }
            //}
            // TODO END VALIDATION PRODUIT
        }

        private void ValidateBondFormula(string property, string value, IDictionary<string, Vector> allVectors,
            IDictionary<string, Variable> allVariables, DynamicVariable[] dynamicVariables = null)
        {
            if (Tools.IsDecimalString(value))
            {
                return;
            }

            string[] operands = Tools.GetOperandsFromExpression(value);
            foreach (string operand in operands)
            {
                if (Tools.IsDecimalString(operand))
                {
                    continue;
                }
                if (allVectors.ContainsKey(operand))
                {
                    this.usedVectors.Add(allVectors[operand]);
                    continue;
                }
                if (allVariables.ContainsKey(operand))
                {
                    this.usedVariables.Add(allVariables[operand]);
                    continue;
                }

                if (dynamicVariables != null && dynamicVariables.Any(dv => string.Compare(dv.Name, operand, true) == 0))
                {
                    continue;
                }

                this.validationErrors.Add(new ValidationMessage(this.product, property, operand));
            }
        }

        public virtual void ValuateProduct(IDictionary<string, object>[] variables,
                                           IDictionary<string, double[]> namedVariables)
        {
            this.vectorAndVariableValues = variables;
            this.namedVariableValues = namedVariables;

            if (Product.ExpirationDate != null)
            {
                DateTime starting = this.Product.StartDate ?? this.valuationDate;

                Product.IntDuration =
                    Math.Max(
                        Util.GetMonthsBeetwenDates(starting, Product.ExpirationDate.GetValueOrDefault()), 1);
                Product.Duration = Product.IntDuration;
            }

            if (product.Attrib == ProductAttrib.InFuture &&
                product.StartDate != null &&
                product.StartDate != Util.LastDayOfMonth(product.StartDate??DateTime.Now))
            {
                this.validationWarnings.Add(new ValidationMessage(product,"StartDate","In Future product is not starting on last day of the month"));
            }
            if (product.Attrib == ProductAttrib.InFuture &&
                product.Frequency != ProductFrequency.AtMaturity &&
                product.Duration % (12/(int) product.Frequency)!=0)
            {
                this.validationWarnings.Add(new ValidationMessage(product, "Duration", "In Future product duration is not multiple of coupon frequency."));
            }

            for (int i = 0; i < resultSteps; i++)
            {
                ValuateProductStep(i);

                if (this.validationErrors.Count > 0)
                {
                    return;
                }
            }
        }

        public void ValuateProductStep(int step)
        {
            Util.setDefaultCultureInfoToThread();
            this.CouponValues[step] = this.GetComputedValue(this.coupon, vectorAndVariableValues[step], "Coupon");
            this.IrcPercentageValues[step] = this.GetComputedValue(this.product.IRC, vectorAndVariableValues[step], "IRC");
            if (product.AmortizationType == ProductAmortizationType.NO_MATURITY)
            {
                if (!string.IsNullOrEmpty(product.Amortization))
                {
                    this.GapNoMaturityRate[step] = this.GetComputedValue(this.product.Amortization, vectorAndVariableValues[step], "Gap Rate For No Maturity");
                }
                else
                {
                    this.GapNoMaturityRate[step] = 1;
                }
            }
            if (!string.IsNullOrEmpty(this.product.Spread) && this.product.Spread != "0")
            {
                this.Spread[step] = this.valorisationEvaluator.Evaluate(this.product.Spread, PricerUtil.StepToDate(startDate, step, IsDaily),
                                                                     this.discountCurves.DiscountCurves[step]);
            }
            else
                this.Spread[step] = 0;

            this.RollRatioValues[step] = this.GetComputedValue(this.product.RollRatio, vectorAndVariableValues[step], "RollRatio");
            this.marketBasis[step] = this.GetComputedValue(this.product.MarketBasis, vectorAndVariableValues[step], "MarketBasis");

            _amortizationValues[step] = this.GetComputedValue(this.Product.Amortization, vectorAndVariableValues[step], "Amortization");

            if (step == 0 && this.product.AmortizationType == ProductAmortizationType.VAR && _amortizationValues[0] != 1.00)
            {
                this.validationErrors.Add(new ValidationMessage(
                                              string.Format(
                                                  Labels.
                                                      ProductData_ValuateProduct_Invalid_amortization_for_product__0____should_equal_1_00__100___at_valuation_date,
                                                  this.product.ALMIDDisplayed)));
            }

            this.UpdateGrowth(vectorAndVariableValues, step);

            if (this.product.IsModelAndNotDerivative && step>= startStep)
            {
                int index;
                if (this.originStartDateForModel)
                {
                    index = step - startStep;
                }
                else
                {
                    index = step;
                }
                this.CdrValues[step] = this.GetComputedValue(this.product.CDR, vectorAndVariableValues[index], "CDR");
                this.CprValues[step] = this.GetComputedValue(this.product.CPR, vectorAndVariableValues[index], "CPR");
                this.LgdValues[step] = this.GetComputedValue(this.product.LGD, vectorAndVariableValues[index], "LGD");
            }

            int expiryStep = PricerUtil.GetProductExpiryStep(product, this.valuationDate, IsDaily);

            if (step >= startStep && step <= expiryStep)
            {
                switch (this.product.AmortizationType)
                {
                    case ProductAmortizationType.VAR_NOMINAL:
                        this.CurrFace[step] = this.GetComputedValue(this.product.Amortization, vectorAndVariableValues[step], "Amortization");
                        break;
                    case ProductAmortizationType.RESERVE_CASHFLOW:
                        this.reserve_actualization_rate[step] = this.GetComputedValue(this.product.ComplementString, vectorAndVariableValues[step], "ReserveActualizationRate");
                        this.ReserveCashflows[step] = this.GetComputedValue(this.product.Amortization, vectorAndVariableValues[step], "ReserveCashFlows");
                        break;
                    case ProductAmortizationType.RESERVE_NOMINAL:
                        this.reserve_actualization_rate[step] = this.GetComputedValue(this.product.ComplementString, vectorAndVariableValues[step], "ReserveActualizationRate");
                        double currFaceStep = this.GetComputedValue(this.product.Amortization, vectorAndVariableValues[step], "Amortization");
                        if (step < product.IntDuration)
                            this.ReserveCashflows[step] = currFaceStep -
                                                            this.GetComputedValue(this.product.Amortization, vectorAndVariableValues[step + 1], "Amortization");
                        else
                            this.ReserveCashflows[step] = 0;

                        break;
                    default:
                        this.CurrFace[step] = this.GetComputedValue(this.product.OrigFace, vectorAndVariableValues[step], "OrigFace");
                        break;
                }
            }

        }

        public void UpdateGrowth(IDictionary<string, object>[] variables, int i)
        {
            if (this.product.InvestmentRule != ProductInvestmentRule.Constant)
            {
                this.InvestmentParameter[i] = this.GetComputedValue(this.product.InvestmentParameter, variables[i], "InvestmentParameter");
            }
        }

        private double GetFace(DateTime date)
        {
            int month = (date.Year - this.valuationDate.Year) * 12 + (date.Month - this.valuationDate.Month) + 1;

            if (month < 0) month = 0;
            if (month >= _amortizationValues.Length) month = _amortizationValues.Length - 1;

            if (this.product.AmortizationType == ProductAmortizationType.VAR_NOMINAL)
            {
                if (this.CurrFace[startStep] != 0)
                    return _amortizationValues[month] / this.CurrFace[startStep];
                else
                    return 0;
            }

            return _amortizationValues[month];
        }

        //private double GetComputedPercent(string value, IDictionary<string, object> variables, string propertyName)
        //{
        //    double tempDecimal;
        //    if (double.TryParse(value, out tempDecimal))
        //    {
        //        tempDecimal = tempDecimal / 100;
        //    }
        //    else
        //    {
        //        tempDecimal = this.ComputeExpression(value, variables, propertyName);
        //    }
        //    return tempDecimal;
        //}

        private double GetComputedValue(string value, IDictionary<string, object> variables, string propertyName)
        {
            if (string.IsNullOrEmpty(value) || value == "0") return 0;
            if (value == "1") return 1;
            if (value == "100") return 100;

            double tempDecimal;
            if (!double.TryParse(value, out tempDecimal))
            {
                tempDecimal = this.ComputeExpression(value, variables, propertyName);
            }

            return tempDecimal;
        }

        private double ComputeExpression(string expression, IDictionary<string, object> variables, string propertyName)
        {
            double expressionValue = 0;
            try
            {
                expressionValue = this.parser.Evaluate(expression, variables);
            }
            catch (Exception ex)
            {
                Log.Warn(ex);
                this.validationErrors.Add(new ValidationMessage(this.product, propertyName, ex.Message));
            }

            return expressionValue;
        }

        public double ComputeExpectedPrincipalIncome(DateTime date1, DateTime date2)
        {
            return this.cashflowProduct.PrincipalIncome(this.cashflowProductFace, date1, date2);

        }

        public Product Product
        {
            get { return this.product; }
            set { this.product = value; }
        }

        public ModelData ModelData
        {
            get { return this.modelData; }
        }

        public double[] InvestmentParameter
        {
            get { return this.investmentParameter; }
            set { this.investmentParameter = value; }
        }

        public double[] CouponValues
        {
            get { return this.couponValues; }
            set { this.couponValues = value; }
        }

        public double[] CprValues
        {
            get { return this.cprValues; }
        }

        public double[] CdrValues
        {
            get { return this.cdrValues; }
        }

        public double[] LgdValues
        {
            get { return this.lgdValues; }
        }

        public double[] CurrFace
        {
            get { return this.currFace; }

        }

        public double[] BookPrice
        {
            get { return this.bookPrice; }

        }

        public double[] InterestIncome
        {
            get { return this.interestIncome; }
        }

        public double[] GainAndLosesIncome
        {
            get { return this.gainAndAndLossesIncome; }
        }

        /*public double[] Payment
        {
            get { return this.AccruedInterest; }
        }*/

        public double[] MarginAccruedInterest
        {
            get { return this.marginAccruedInterest; }
        }

        /*public double[] AccruedInterest
        {
            get { return this.accruedInterest; }
        }*/

        public double[] PrincipalIncome
        {
            get { return this.principalIncome; }
        }

        public double[] CouponIncome
        {
            get { return this.couponIncome; }
        }

        public double[] RemainPrincipal
        {
            get { return this.remainPrincipal; }
        }

        public double[] LiveFace
        {
            get { return this.liveFace; }
        }

        public CashflowProduct CashflowProduct
        {
            get { return this.cashflowProduct; }
        }

        public double[] FxRate
        {
            get { return this.fxRate; }
        }

        public double[] LossImpairment
        {
            get { return this.lossImpairment; }
        }

        public double[] Recovery
        {
            get { return this.recovery; }
        }

        public double[] ActuarialPrice
        {
            get { return this.actuarialPrice; }

        }

        public int StartStep
        {
            get { return this.startStep; }
            set { this.startStep = value; }
        }

        public double[] RollRatioValues
        {
            get { return this.rollRatioValues; }
        }

        public DateTime StartDate
        {
            get { return this.startDate; }
        }

        public ICouponExpressionEvaluator ValorisationEvaluator
        {
            get { return this.valorisationEvaluator; }
        }

        public IFormulaEvaluator StandardEvaluator
        {
            get { return this.standardEvaluator; }
        }

        public IDiscountCurveCollection DiscountCurves
        {
            get { return this.discountCurves; }
        }

        public Currency Currency { get; set; }

        public double[] ApplicationFee
        {
            get { return this.applicationFee; }
        }
        public double[] ApplicationCharge
        {
            get { return this.applicationCharge; }
        }

        public double[] PrepayementFee
        {
            get { return this.prepayementFee; }
        }

        public double[] FxPnlIncome
        {
            get { return this.fxPNLIncome; }
        }

        public double[] Growth
        {
            get { return this.growth; }
        }

        public int RecoveryLagInMonths
        {
            get
            {
                if (recoveryLagInMonths == 0 && !string.IsNullOrEmpty(product.RecoveryLag))
                {
                    recoveryLagInMonths = int.Parse(product.RecoveryLag);
                }
                return recoveryLagInMonths;
            }
        }

        public double[] LaggedRecovery
        {
            get { return laggedRecovery; }
        }

        public double[] Default
        {
            get { return modelDefault; }
        }

        public int ImpactTreasuryStep
        {
            get { return impactTreasuryStep; }
            set { impactTreasuryStep = value; }
        }

        public DateTime ValuationDate
        {
            get { return this.valuationDate; }
        }

        public double[] IRCValue
        {
            get { return ircValue; }
        }

        public double[] IrcPercentageValues
        {
            get { return ircPercentageValues; }
            set { ircPercentageValues = value; }
        }

        public double[] ReserveCashflows
        {
            get { return reserve_cashflows; }
        }

        public bool OriginStartDateForModel
        {
            get { return originStartDateForModel; }
            set { originStartDateForModel = value; }
        }

        public override string ToString()
        {
            return this.Product != null ? this.Product.ToString() : Labels.ProductData_ToString__no_product_;
        }

        public virtual void Clear()
        {
            marketBasis = null;
            couponValues = null;
            rollRatioValues = null;
            cprValues = null;
            cdrValues = null;
            lgdValues = null;

            fundamentalVectors = null;
            parser = null;
            discountCurves = null;

            modelData = null;

            usedVectors = null;
            usedVariables = null;

            cashflowProduct = null;

            valorisationEvaluator = null;
            standardEvaluator = null;
            discountCurves = null;

            vectorAndVariableValues = null;
            namedVariableValues = null;
            allCashFlowProduct = null;
        }

        public bool IsOffBalanceSheet()
        {
            return (this.Product.Category.BalanceType == BalanceType.Derivatives);
        }

        public double GetLossImpairmentInValuationCurrency(int step)
        {
            return lossImpairment[step] * fxRate[step];
        }

        public double GetPrincipalIncomeInValuationCurrency(int step)
        {
            return principalIncome[step] * fxRate[step];
        }

        public double GetInterestIncomeInValuationCurrency(int step)
        {
            return interestIncome[step] * fxRate[step];
        }

        public double GetLossesImpairmentInValuationCurrency(int step)
        {
            return ModelData.LossesImpairment[step] * fxRate[step];
        }

        public double GetGainAndLosesIncomeInValuationCurrency(int step)
        {
            return gainAndAndLossesIncome[step] * fxRate[step];
        }

        public double GetAllGainAndLosesIncomeInValuationCurrency(int step)
        {
            double all = 0;
            for (int i = this.startStep; i <= step; i++)
            {
                all += GetGainAndLosesIncomeInValuationCurrency(i);
            }
            return all;
        }

        public double GetCurrFaceInValuationCurrency(int step)
        {
            return currFace[step] * fxRate[step];
        }

        public bool HasApplicationFees()
        {
            return this.product.ApplicationFee != null && this.product.ApplicationFee.Trim().Length != 0;
        }
        public bool HasApplicationCharges()
        {
            return this.product.ApplicationCharge != null && this.product.ApplicationCharge.Trim().Length != 0;
        }

        public bool HasPrepayementFees()
        {
            return this.product.IsModelAndNotDerivative && this.product.PrePayementFee != null && this.product.PrePayementFee.Trim().Length != 0;
        }

        public double GetRealCouponRate(DateTime startDate, DateTime endDate, double[] values, bool applySpread = true)
        {
            //fixing : determiner quel step utiliser pour le couponvalues
            DateTime fixingDate;
            if (!applySpread && this.product.ProductType == ProductType.StockEquity)
                fixingDate = startDate;
            else
                fixingDate = this.product.CouponType == CouponType.Floating ? this.cashflowProduct.GetModelFixingDate(startDate, this.startDate) : this.startDate;

            int fixingStep = ScenarioVariablesEvaluator.ConvertDateToStep(fixingDate.AddDays(1), valuationDate, resultSteps, IsDaily);
            int currentStep = ScenarioVariablesEvaluator.ConvertDateToStep(endDate, valuationDate, resultSteps, IsDaily);
            double anualInterestRateInStep = values[fixingStep];
            //adding spread
            if (applySpread)
                anualInterestRateInStep += this.Spread[fixingStep];
            //appliquer le prorata temporis pour le CGV
            double monthlyInterestRateInStep = anualInterestRateInStep *
                                              DayCounter.GetCvg(startDate, endDate, this.product.CouponBasis);

            return monthlyInterestRateInStep;
        }

        public double GetCurrFaceForResidualGap(int step)
        {
            return (this.product.ProductType != ProductType.StockEquity
                        ? this.CurrFace[step]
                        : this.CurrFace[step] * this.BookPrice[step]);
        }

        //public double GetTCIValue(int step)
        //{
        //    string tci = this.product.IRC;

        //    if (tci == null || step<1)
        //        return 0;

        //    //double tciVal = this.standardEvaluator.Evaluate(this.product.IRC, PricerUtil.StepToDate(this.valuationDate,step,IsDaily));

        //    return IRCValue[step];//todo MTU : utiliser GetCVG() ?
        //}
        public static IList<IList<ProductData>> splitInBatches(IList<ProductData> all, int perBatchCount)
        {
            IList<IList<ProductData>> result = new List<IList<ProductData>>(all.Count / perBatchCount + 1);
            int i = 0;
            int index = 0;
            IList<ProductData> current = null;
            foreach (ProductData productData in all)
            {
                if (i == 0)
                {
                    current = new List<ProductData>(perBatchCount);
                    result.Add(current);
                }
                current.Add(productData);
                i++;
                if (i == perBatchCount)
                {
                    i = 0;
                    index++;
                }
            }
            return result;
        }

        public int FirstInterestStep()
        {
            if (this.cashflowProduct == null)
                return resultSteps;
            int interestsFlows = this.cashflowProduct.InterestCashflows.Count;
            if (interestsFlows > 0)
            {
                DateTime firstInterestDate = this.cashflowProduct.InterestCashflows[interestsFlows - 1].EndDate;
                return Util.GetMonthsBeetwenDates(valuationDate, firstInterestDate) + 1;
            }
            return 0;

        }

        public double GetMtm(ProductData otherSwapLegData, int step)
        {

            return this.Product.Sign *
                                      (this.GetCurrFaceInValuationCurrency(step) * this.GetBookPrice(step) +
                                       otherSwapLegData.GetCurrFaceInValuationCurrency(step) * otherSwapLegData.GetBookPrice(step));
        }

        public double GetLegMtm(int step)
        {

            return this.Product.Sign * (this.GetCurrFaceInValuationCurrency(step) * this.GetBookPrice(step));
        }


        public double GetLegMtmVariation(int step)
        {
            return GetLegMtm(step) - GetLegMtm(step - 1);
        }

        private double GetMonthlyDefault(int step)
        {
            return (product.IsModelAndNotDerivative ? this.modelData.MonthlyDefault[step] : 0);
        }

        private double GetAccruedActuarialValueWithPrincipalIncomeAndMonthlyDefault(int step)
        {
            double actuarialVariation;
            //if (this.product.Attrib==ProductAttrib.InFuture && step==StartStep)
            if (step == StartStep)
                actuarialVariation = 0;
            else
                actuarialVariation = this.CurrFace[step] * (this.actuarialPrice[step] / 100) - this.CurrFace[step - 1] * (this.actuarialPrice[step - 1] / 100);

            return actuarialVariation +
                //this.CurrFace[step-1]*(this.marginAccruedInterest[step]-this.marginAccruedInterest[step - 1])+
                                                            this.principalIncome[step] +
                                                            this.GetMonthlyDefault(step);
        }
    }

}