﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.JScript;
using Microsoft.JScript.Vsa;
using Convert = Microsoft.JScript.Convert;

namespace BondManager.Calculations
{
    public class DynamicVariableEvaluator
    {
        private readonly string[] argNames;
        private readonly int priority;
        private const string FunctionTemplate = @"( function EvaluatorFunction({0}) {{ {1} }} )";

        public static readonly Dictionary<string, string> ProductDataProperties =
            new Dictionary<string, string>
                {
                    {"CurrFace", "CurrFace"},
                    {"BookPrice", "BookPrice"},
                    {"ActuarialPrice", "ActuarialPrice"},
                    // {"Coupon", "CouponValues"},
                    {"InvestmentParameter", "InvestmentParameter"},
                    {"CouponIncome", "CouponIncome"},
                    {"PrincipalIncome", "PrincipalIncome"},
                    {"InterestIncome", "InterestIncome"},
                    {"GainAndLosesIncome", "GainAndLosesIncome"},
                    {"MarginAccruedInterest", "MarginAccruedInterest"},
                    {"LossImpairment", "LossImpairment"},
                    {"RemainPrincipal", "RemainPrincipal"},
                    {"LiveFace", "LiveFace"},
                };
        public static readonly Dictionary<string, string> CategoryDataProperties =
            new Dictionary<string, string>
                {
                    {"BookValue", "BookValue"},    
                    {"CouponIncome", "CouponIncome"},
                    {"PrincipalIncome", "PrincipalIncome"},
                    {"InterestIncome", "InterestIncome"},
                    {"GainAndLosesIncome", "GainAndLosesIncome"},
                    {"LossImpairment", "LossImpairment"}
                };

        private readonly Closure closure;

        private static readonly object VsaEngineLock = new object();

        private static VsaEngine vsaEngine;
        private string _functionText;

        public DynamicVariableEvaluator(string expression, string[] argNames, string[] productAlmIds, int[] categoryAlmIds, int priority)
        {
            this.argNames = argNames;
            this.priority = priority;
            expression = this.PreProcessExpression(expression, productAlmIds ?? new string[0], categoryAlmIds ?? new int[0]);
            _functionText = string.Format(FunctionTemplate, string.Join(",", argNames), expression);

            lock (VsaEngineLock)
            {
                vsaEngine = vsaEngine ?? VsaEngine.CreateEngine();
                this.closure = (Closure)Eval.JScriptEvaluate(_functionText, vsaEngine);
            }
        }

        public string[] ArgNames
        {
            get { return this.argNames; }
        }

        public int Priority
        {
            get { return this.priority; }
        }

        public T Evaluate<T>(params object[] args)
        {
            lock (VsaEngineLock)
            {
                object result = this.closure.Invoke(new object(), args);
                object coerced = Convert.Coerce(result, typeof(T));

                return (T)coerced;
            }
        }

        protected string PreProcessExpression(string expression, string[] almIds, int[] categoryAlmIds)
        {
            expression = expression ?? "";
            expression = PreProcessProductDataProperties(expression);
            expression = PreProcessCategoryDataProperties(expression);
            expression = PreProcessProductAlmIds(expression, almIds);
            expression = PreProcessCategoriesAlmIds(expression, categoryAlmIds);

            if (!expression.Contains("return"))
            {
                expression = "return " + expression + ";";
            }

            return expression;
        }

        private static string PreProcessProductDataProperties(string expression)
        {
            foreach (KeyValuePair<string, string> property in ProductDataProperties)
            {
                expression = expression.Replace("." + property.Key, ".get_" + property.Value + "()");
            }

            return expression;
        }

        private static string PreProcessCategoryDataProperties(string expression)
        {
            foreach (KeyValuePair<string, string> property in CategoryDataProperties)
            {
                expression = expression.Replace("." + property.Key, ".get_" + property.Value + "()");
            }

            return expression;
        }

        private static string PreProcessProductAlmIds(string expression, string[] almIds)
        {
            foreach (string productAlmId in almIds.OrderByDescending(x=>x.Length))
            {
                expression = expression.Replace(productAlmId, "products['" + productAlmId + "']");
            }

            return expression;
        }
        
        private static string PreProcessCategoriesAlmIds(string expression, int[] almIds)
        {
            
            foreach (int categoryAlmId in almIds.OrderByDescending(x=>x))
            {
                expression = expression.Replace("C"+categoryAlmId, "categories[" + categoryAlmId + "]");
            }

            return expression;
        }
    }
}