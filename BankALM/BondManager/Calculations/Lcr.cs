using System;
using System.Linq;
using System.Collections.Generic;
using ALMS.Products;
using BondManager.Domain;
using NHibernate;

namespace BondManager.Calculations
{
    public class Lcr
    {
        private DateTime valuationDate;
        
        private IEnumerable<ProductData> level1ProductData;
        private IEnumerable<ProductData> level2AProductData;
        private IEnumerable<ProductData> level2BProductData;
        private IEnumerable<ProductData> derivativeProductData;
        private IEnumerable<ProductData> assetNonDerivativeProductData;
        //private IEnumerable<ProductData> liabilityDerivativeProductData;
        private IEnumerable<ProductData> liabilityNonDerivativeProductData;
        private double[] dividends;

        private readonly double[] liquidAssets;
        private readonly double[] liquidAssetsL1;
        private readonly double[] liquidAssetsL2A;
        private readonly double[] liquidAssetsL2B;
        private readonly double[] netCashOutflows;
        private readonly double[] netCashOutflowsIn;
        private readonly double[] netCashOutflowsOut;

        public Lcr(DateTime valuationDate, IEnumerable<ProductData> productDatas, double[] dividends, int resultMonths)
        {
            this.dividends = dividends;
            this.valuationDate = valuationDate;

            liquidAssets = new double[resultMonths];
            liquidAssetsL1 = new double[resultMonths];
            liquidAssetsL2A = new double[resultMonths];
            liquidAssetsL2B = new double[resultMonths];
            netCashOutflows = new double[resultMonths];
            netCashOutflowsIn = new double[resultMonths];
            netCashOutflowsOut = new double[resultMonths];

            this.level1ProductData = productDatas.Where(pd => pd.Product.LiquidityEligibility == LiquidityEligibility.Level1).ToArray();
            this.level2AProductData = productDatas.Where(pd => pd.Product.LiquidityEligibility == LiquidityEligibility.Level2).ToArray();
            this.level2BProductData = productDatas.Where(pd => pd.Product.LiquidityEligibility == LiquidityEligibility.Level2B).ToArray();
            this.derivativeProductData = productDatas.Where(pd =>  pd.Product.IsDerivative).ToArray();
            this.assetNonDerivativeProductData = productDatas.Where(pd => pd.Product.Category.BalanceType == BalanceType.Asset && !pd.Product.IsDerivative).ToArray();
            this.liabilityNonDerivativeProductData = productDatas.Where(pd => pd.Product.Category.BalanceType == BalanceType.Liability && !pd.Product.IsDerivative).ToArray();
        }

        public double[] LiquidAssets
        {
            get { return this.liquidAssets; }
        }

        public double[] NetCashOutflows
        {
            get { return this.netCashOutflows; }
        }

        public double[] LiquidAssetsL1
        {
            get { return this.liquidAssetsL1; }
        }

        public double[] LiquidAssetsL2A
        {
            get { return this.liquidAssetsL2A; }
        }
        public double[] LiquidAssetsL2B
        {
            get { return this.liquidAssetsL2B; }
        }

        public double[] NetCashOutflowsIn
        {
            get { return this.netCashOutflowsIn; }
        }

        public double[] NetCashOutflowsOut
        {
            get { return this.netCashOutflowsOut; }
        }

        public void Compute(int step)
        {
            ComputeLiquidAssets(step);
            ComputeNetCashOutflows(step);
        }

        private void ComputeLiquidAssets(int step)
        {
            double l1 = this.level1ProductData.Sum(pd => (1 - pd.Product.LiquidityHaircut) * pd.CurrFace[step] * pd.FxRate[step]);
            double l2a = this.level2AProductData.Sum(pd => (1 - pd.Product.LiquidityHaircut) * pd.CurrFace[step] * pd.FxRate[step]);
            double l2b = this.level2BProductData.Sum(pd => (1 - pd.Product.LiquidityHaircut) * pd.CurrFace[step] * pd.FxRate[step]);

            this.LiquidAssetsL1[step] = l1;
            this.LiquidAssetsL2A[step] = l2a;
            this.LiquidAssetsL2B[step] = l2b;

            this.LiquidAssets[step] = l1 + Math.Min(2/3.0*l1, l2a + Math.Min(l2b, 3/17.0*(l1 + l2a)));
        }

        private void ComputeNetCashOutflows(int step)
        {
            double @out = GetOutNetCashOutflows(step);

            double @in = GetInNetCashOutflows(step);

            netCashOutflowsOut[step] = @out;
            netCashOutflowsIn[step] = @in;

            netCashOutflows[step] = @out - Math.Min(0.75 * @out, @in);
        }

        private double GetInNetCashOutflows(int step)
        {
            double inNonDerivatives = assetNonDerivativeProductData.Sum(pd => this.GetInNonDerivativesCashOutflows(pd, step));
            //    this.productDatas.Where(pd => pd.Product.Category.BalanceType == BalanceType.Asset && !pd.Product.IsDerivative).
            //        Sum(pd => this.GetInNonDerivativesCashOutflows(pd, step));

            double inDerivatives = derivativeProductData.Sum(pd => GetInDerivativesCashOutflows(pd, step));
            //    this.productDatas.Where(pd => pd.Product.Category.BalanceType == BalanceType.Asset && pd.Product.IsDerivative).
            //        Sum(pd => this.GetInDerivativesCashOutflows(pd, step));

            return inDerivatives + inNonDerivatives;
        }

        private double GetInNonDerivativesCashOutflows(ProductData pd, int step)
        {
            if (pd.Product.Duration.GetValueOrDefault() == Util.InfiniteDuration)
            {
                return pd.Product.LiquidityRunOff * pd.CurrFace[step] * pd.FxRate[step];
            }
            else
            {
                DateTime dt1 = this.valuationDate.AddMonths(step).AddDays(-1);
                DateTime dt2 = this.valuationDate.AddMonths(step + 1).AddDays(-1);

                double ratio = pd.RemainPrincipal[step] == 0
                                    ? 0
                                    : (double)pd.ComputeExpectedPrincipalIncome(dt1, dt2) 
                                        / pd.RemainPrincipal[step];

                return pd.Product.LiquidityRunOff*pd.CurrFace[step]*pd.FxRate[step]*ratio;
            }
        }

        private static double GetInDerivativesCashOutflows(ProductData pd, int step)
        {
            return Math.Max(0, pd.Product.Sign * pd.CurrFace[step] * pd.MarginAccruedInterest[step]);
        }

        private double GetOutNetCashOutflows(int step)
        {
            double outNonDerivatives = liabilityNonDerivativeProductData.Sum(pd => this.GetOutNonDerivativesCashOutflows(pd, step));
            //    this.productDatas.Where(pd => pd.Product.Category.BalanceType == BalanceType.Liability && !pd.Product.IsDerivative).
            //        Sum(pd => this.GetOutNonDerivativesCashOutflows(pd, step));

            double outDerivatives = derivativeProductData.Sum(pd => GetOutDerivativesCashOutflows(pd, step));
            //    this.productDatas.Where(pd => pd.Product.Category.BalanceType == BalanceType.Liability && pd.Product.IsDerivative).
            //        Sum(pd => this.GetOutDerivativesCashOutflows(pd, step));

            double result = outNonDerivatives + outDerivatives;

            if (step < this.dividends.Length - 1)
            {
                result += this.dividends[step + 1];
            }
            return result;
        }


        private double GetOutNonDerivativesCashOutflows(ProductData pd, int step)
        {
            if (pd.Product.Duration.GetValueOrDefault() == Util.InfiniteDuration)
            {
                return pd.Product.LiquidityRunOff * pd.CurrFace[step] * pd.FxRate[step];
            }
            else
            {
                DateTime dt1 = this.valuationDate.AddMonths(step).AddDays(-1);
                DateTime dt2 = this.valuationDate.AddMonths(step + 1).AddDays(-1);

                double ratio = pd.RemainPrincipal[step] == 0 ? 0 :
                    (double)pd.ComputeExpectedPrincipalIncome(dt1, dt2)  
                        / pd.RemainPrincipal[step];

                return pd.Product.LiquidityRunOff * pd.CurrFace[step] * pd.FxRate[step] * ratio;
            }
        }

        private static double GetOutDerivativesCashOutflows(ProductData pd, int step)
        {
            double result = 0;
            if (pd.Product.SwapLeg == null || pd.Product.SwapLegSign == 1)
            {
                result += pd.Product.LiquidityRunOff * pd.CurrFace[step] * pd.FxRate[step];
            }
            result += Math.Max(0, -pd.Product.Sign * pd.CurrFace[step] * pd.MarginAccruedInterest[step]);
            return result;
        }

        public void Clear()
        {
            this.dividends = null;
            this.derivativeProductData = null;
            this.assetNonDerivativeProductData = null;
            this.level1ProductData = null;
            this.level2AProductData = null;
            this.level2BProductData = null;
            this.liabilityNonDerivativeProductData = null;
        }
    }
}