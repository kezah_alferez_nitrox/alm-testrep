using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ALMS.Products;
using ALMSCommon;
using BankALM.Infrastructure.Data;
using BondManager.BusinessLogic;
using BondManager.Domain;
using BondManager.Views;
using NHibernate;

namespace BondManager.Calculations
{
    public class ScenarioData
    {
        private readonly ExpressionParser parser = new ExpressionParser();

        public const int PER_BATCH_FOR_PARALLELISATION = 1000;

        private readonly IDictionary<int, double[]> categoryBookValue = new SortedDictionary<int, double[]>();
        private readonly IDictionary<int, double[]> categoryIncome = new SortedDictionary<int, double[]>();
        private readonly IDictionary<string, double[]> analysisCategoryIncome = new SortedDictionary<string, double[]>();

        private readonly int dividendPaymentMonth;

        private readonly IDictionary<Dividend, double[]> dividendValues = new SortedDictionary<Dividend, double[]>();
        private readonly Dividend[] dividends;
        private readonly double[] actualizedGaps;
        private readonly double[] dividendsOnEquity;
        public readonly IDictionary<string, DynamicVariableEvaluator> dynamicVariableEvaluators;
        private readonly double[] gainLosesOnFinancialInstrument;
        private readonly double[] fx_PnL;
        private readonly IDictionary<int, double[]> impairementData = new Dictionary<int, double[]>();
        private readonly double[] impairments;
        private readonly IDictionary<OtherItemAssum,double[]> impairmentsRisk;
        private readonly double[] incomeBeforeImpairmentCharge;
        private readonly double[] incomePreTax;
        private readonly double[] gapActuarialRate;

        public readonly IDictionary<string, double[]> namedVariableValues;
        private readonly double[] interestReceivedOnDerivatives;
        private readonly double[] interestPayedOnDerivatives;
        private readonly double[] netBankingIncome;
        private readonly double[] netIncome;
        private readonly double[] netInterestIncome;

        private readonly IDictionary<OtherItemAssum, double[]> otherItemAssumptionValues =
            new Dictionary<OtherItemAssum, double[]>();

        private readonly double[] otherItemAssumptionsSum;
        private readonly double[] otherItemAssumptionsSumWithoutAmortizing;
        private readonly double[] commisionsAndOthersGroupValue;
        private readonly IDictionary<int, bool> productHasRoll = new SortedDictionary<int, bool>();

        private ResultData result = new ResultData();

        private readonly Scenario scenario;
        private readonly Solvency solvency;

        private readonly IDictionary<string, double[]> solvencyAdjustmentValues = new SortedDictionary<string, double[]>();
        private readonly double standardTax;
        private readonly double[] taxes;
        private readonly double[] totalAssets;
        private readonly double[] totalEquity;
        private readonly DateTime valuationDate;
        private readonly bool isMonoScenario;
        private readonly int valuationDateMonth;
        private ProductData assetCashAdjProductData;
        private readonly IDictionary<Currency, ProductData> assetsTreasuryProductDatas = new SortedDictionary<Currency, ProductData>();
        private readonly IDictionary<Currency, ProductData> liabilitiesTreasuryProductDatas = new SortedDictionary<Currency, ProductData>();
        private readonly IDictionary<Currency, ProductData> receivableProductDatas = new SortedDictionary<Currency, ProductData>();
        private readonly IDictionary<Currency, ProductData> payableProductDatas = new SortedDictionary<Currency, ProductData>();
        private Category[] categories;
        private DiscountCurveCollection discountCurveCollection;
        private IDictionary<Currency, double[]> fxRates;
        private ProductData modelImpairmentsProductData;
        private ProductData ibnrProductData;
        private Lcr lcr;
        private ProductData liabilitiesCashAdjProductData;
        private ProductData negativeDerivativesValuationProductData;
        private ProductData otherComprehensiveIncomeProductData;
        private OtherItemAssum[] otherItemAssumptions;
        private int applicationFeeIndex = -1;
        private int costInsuranceIndex = -1;
        private int applicationChargeIndex = -1;
        private int prepayementFeeIndex = -1;
        private int amortizingIndex = -1;
        private ProductData positiveDerivativesValuationProductData;
        private ProductData marginCallPaidProductData;
        private ProductData marginCallReceivedProductData;
        private IList<ProductData> productDatas;
        private readonly Dictionary<int, CategoryData> categoryData;
        private ProductData reservesProductData;
        private SolvencyAdjustment[] solvencyAdjustments;
        public IDictionary<string, object>[] vectorAndVariableValues;

        private readonly int resultMonths;
        private readonly SimulationParams parameters;
        private ICollection<Currency> currencies;

        public Mouvements[] GainLosesSourceMouvements { get; set; }
        public Mouvements[] TreasurySourceMouvements { get; set; }

        public ScenarioData(Scenario scenario, IDictionary<string, object>[] vectorAndVariableValues, IDictionary<string, DynamicVariableEvaluator> dynamicVariableEvaluators, IDictionary<string, double[]> namedVariableValues, IDictionary<Currency, double[]> fxRates, IList<ProductData> productDatas, Dictionary<int, CategoryData> categoryData, Category[] categories, OtherItemAssum[] otherItemAssumptions, SolvencyAdjustment[] solvencyAdjustments, Dividend[] dividends, double standardTax, DiscountCurveCollection discountCurveCollection, DateTime valuationDate, SimulationParams parameters, bool isMonoScenario)
        {
            this.parameters = parameters;

            this.valuationDate = valuationDate;
            this.isMonoScenario = isMonoScenario;
            this.scenario = scenario;
            this.vectorAndVariableValues = vectorAndVariableValues;
            this.dynamicVariableEvaluators = dynamicVariableEvaluators;
            this.namedVariableValues = namedVariableValues;
            this.fxRates = fxRates;
            this.productDatas = productDatas;
            this.categoryData = categoryData;
            this.categories = categories;
            this.otherItemAssumptions = otherItemAssumptions;
            this.solvencyAdjustments = solvencyAdjustments;
            this.dividends = dividends;
            this.standardTax = standardTax;
            this.discountCurveCollection = discountCurveCollection;
            this.resultMonths = parameters.ResultSteps;
            this.dividendsOnEquity = new double[resultMonths];
            this.otherItemAssumptionsSum = new double[resultMonths];
            this.otherItemAssumptionsSumWithoutAmortizing = new double[resultMonths];
            this.commisionsAndOthersGroupValue = new double[resultMonths];
            this.actualizedGaps = new double[resultMonths];
            this.totalEquity = new double[resultMonths];
            this.totalAssets = new double[resultMonths];
            this.impairments = new double[resultMonths];
            this.impairmentsRisk = new Dictionary<OtherItemAssum, double[]>();
            this.gapActuarialRate = new double[resultMonths];
            this.incomeBeforeImpairmentCharge = new double[resultMonths];
            this.interestReceivedOnDerivatives = new double[resultMonths];
            this.interestPayedOnDerivatives = new double[resultMonths];
            this.netInterestIncome = new double[resultMonths];
            this.netBankingIncome = new double[resultMonths];
            this.gainLosesOnFinancialInstrument = new double[resultMonths];
            this.fx_PnL = new double[resultMonths];
            this.incomePreTax = new double[resultMonths];
            this.taxes = new double[resultMonths];
            this.netIncome = new double[resultMonths];
            this.Gaps = new double[resultMonths + 1][];

            this.GainLosesSourceMouvements = new Mouvements[resultMonths];
            this.TreasurySourceMouvements = new Mouvements[resultMonths];

            foreach (Category category in categories)
            {
                if (category.Children.Count != 0) continue;

                this.CategoryBookValue[category.Id] = new double[this.ResultMonths];
                this.CategoryIncome[category.Id] = new double[this.ResultMonths];
                this.impairementData[category.Id] = new double[this.ResultMonths];
            }
            foreach (string analyseCategory in categories.Select(p => p.AnalysisCategory).Distinct().ToArray())
            {
                AnalysisCategoryIncome.Add(analyseCategory, new double[resultMonths]);
            }
            this.solvency = new Solvency(this.productDatas, this.solvencyAdjustmentValues, parameters);
            foreach (SolvencyAdjustment adjustment in solvencyAdjustments)
            {
                this.solvencyAdjustmentValues[adjustment.Description] = new double[this.ResultMonths];
            }

            foreach (Dividend dividend in this.dividends)
            {
                this.dividendValues[dividend] = new double[this.ResultMonths];
            }

            this.valuationDateMonth = this.ValuationDate.Month;

            this.dividendPaymentMonth = parameters.DividendPaymentMonth;

            this.result.Initialize(this.Categories, this.otherItemAssumptions, this.dividends);
            this.result.InitializeValues(this.ResultMonths);


            this.FindOtherItemAssumptionsIndexes();
        }

        private void FindOtherItemAssumptionsIndexes()
        {
            for (int oiaIndex = 0; oiaIndex < this.otherItemAssumptions.Count(); oiaIndex++)
            {
                if (this.otherItemAssumptions[oiaIndex].Descrip.ToLower() == "Application fees".ToLower())
                    this.applicationFeeIndex = oiaIndex;
                if (this.otherItemAssumptions[oiaIndex].Descrip.ToLower() == "Application charges".ToLower())
                    this.applicationChargeIndex = oiaIndex;
                if (this.otherItemAssumptions[oiaIndex].Descrip.ToLower() == "Prepayement fees".ToLower())
                    this.prepayementFeeIndex = oiaIndex;
                if (this.otherItemAssumptions[oiaIndex].Descrip.ToLower() == "Cost insurance".ToLower())
                    this.costInsuranceIndex = oiaIndex;
                if (this.otherItemAssumptions[oiaIndex].Descrip.ToLower() == "Amortizing".ToLower())
                    this.amortizingIndex = oiaIndex;

            }

            if (this.applicationFeeIndex == -1) throw new Exception("Application Fees not found in OtherItemAssumptions");
            if (this.applicationChargeIndex == -1) throw new Exception("Application Charges not found in OtherItemAssumptions");
            if (this.prepayementFeeIndex == -1) throw new Exception("Prepayement Fees not found in OtherItemAssumptions");
            if (this.costInsuranceIndex == -1) throw new Exception("Cost Insurance not found in OtherItemAssumptions");
            if (this.amortizingIndex == -1) throw new Exception("Amortizing not found in OtherItemAssumptions");
        }

        public int VariableCount
        {
            get { return namedVariableValues.Keys.Count; }
        }

        public Solvency Solvency
        {
            get { return this.solvency; }
        }

        public Lcr Lcr
        {
            get { return this.lcr; }
        }

        public DateTime ValuationDate
        {
            get { return this.valuationDate; }
        }

        public IDictionary<int, double[]> CategoryBookValue
        {
            get { return this.categoryBookValue; }
        }

        public IDictionary<int, double[]> CategoryIncome
        {
            get { return this.categoryIncome; }
        }

        public bool HasProductDetails
        {
            get { return this.ProductDatas != null; }
        }

        public int ResultMonths
        {
            get { return this.resultMonths; }
        }

        public IList<ProductData> ProductDatas
        {
            get { return this.productDatas; }
        }

        public double[] InterestReceivedOnDerivatives
        {
            get { return this.interestReceivedOnDerivatives; }
        }

        public double[] InterestPayedOnDerivatives
        {
            get { return this.interestPayedOnDerivatives; }
        }

        public Currency ValuationCurrency
        {
            get { return parameters.ValuationCurrency; }
        }

        public bool ComputeGap
        {
            get { return parameters.ComputeGap; }
        }

        public Category[] Categories
        {
            get { return categories; }
        }

        public IDictionary<string, double[]> AnalysisCategoryIncome
        {
            get { return analysisCategoryIncome; }
        }

        public double[] ActualizedGaps
        {
            get { return actualizedGaps; }
        }

        public double[][] Gaps { get; set; }

        public double[] GapActuarialRate
        {
            get { return gapActuarialRate; }
        }

        public void Compute(BackgroundWorker backgroundWorker)
        {
            if (this.scenario == null) return;
            bool computeAnalysisCategories;
            using (ISession session = SessionManager.OpenSession())
            {
                computeAnalysisCategories = Param.GetComputeAnalysisCategories(session);
            }

            this.FindSpecialBonds();

            this.ComputeProductHasRoll();



            this.ComputeProductsFxRates();

            this.UpdateProductsStartingBeforeValuationDate();

            if (ComputeGap)
            {
                for (int i = 0; i < resultMonths; i++)
                {
                    //todo GAP MTU : accepter des variables/vecteurs
                    double.TryParse(parameters.GapFixedRate, out this.GapActuarialRate[i]);

                    //this.standardEvaluator.Evaluate(this.product.Spread, this.startDate);
                }
            }
            IDictionary<string, ProductData> productDatasByAlmId =
                this.ProductDatas.Distinct(new AlmIdProductDataComparer()).ToDictionary(
                    pd => pd.Product.ALMIDDisplayed, pd => pd);


            List<ProductData> listNS = ProductDatas.Where(p => p.Product.ProductType != ProductType.Swaption).ToList();
            int batchSizeNS = listNS.Count / Environment.ProcessorCount / 2 + 1;
            IEnumerable<IEnumerable<ProductData>> datasNonSwaption = ProductData.splitInBatches(listNS, batchSizeNS);

            List<ProductData> listSS = ProductDatas.Where(p => p.Product.ProductType == ProductType.Swaption).ToList();
            int batchSizeSS = listSS.Count / Environment.ProcessorCount / 2 + 1;
            IEnumerable<IEnumerable<ProductData>> datasSwaption = ProductData.splitInBatches(listSS, batchSizeSS);

            Logger.Info("Splitted in batches Non-Swaptions batchSize=" + batchSizeNS + ", Swaption batchSize=" + batchSizeSS);

            for (int step = 0; step < this.ResultMonths; step++)
            {
                if (this.parameters.StopAfterResultMonths != -1 &&
                    step > this.parameters.StopAfterResultMonths)
                {
                    break;
                }

                if (backgroundWorker.CancellationPending)
                {
                    break;
                }
                //if (step != 0)
                //{//todo MTU verify why not working in 0
                Util.EvaluateDynamicVariables(step, productDatasByAlmId, categoryData, this.dynamicVariableEvaluators, this.result.LineDictionary, this.namedVariableValues, this.vectorAndVariableValues);
                //}

                Parallel.ForEach(ProductDatas, pd => pd.ValuateProductStep(step));
                //foreach (ProductData productData in productDatas)
                //{
                //    productData.ValuateProductStep(step);
                //}
                this.RunScenarioForBonds(step, datasNonSwaption, datasSwaption);

                this.UpdateImparmentsBonds(step);

                this.UpdateInvestmentRules(step);

                this.ComputeInterestOnDerivatives(step);
                this.UpdateOtherComprehensiveIncomeBonds(step);

                this.UpdateOtherItemAssumptionWithApplicationFeesAndPrepayementFees(step);
                this.AggregateSumsAndDictionnaryOtherItemsAssumption();
                this.ComputeGainLosesOnFinancialInstrument(step);

                this.AggregateIncome(step);
                this.ComputeNetInterestIncome(step);
                this.ComputeIncomeBeforeImpairmentCharge(step);
                this.ComputeIncomePreTax(step);
                this.ComputeTaxes(step);
                this.ComputeNetIncome(step);
                this.ComputeDividends(step);

                this.UpdateReservesBonds(step);




                this.UpdateReceivablePayableBonds(step);
                this.UpdateDerivativesValuationBonds(step);

                this.UpdateTreasuryBonds(step);



                this.ComputeSolvency(step);

                this.UpdateCashAdjBonds(step);

                this.AggregateBookValue(step);

                this.ComputeNetBankingIncome(step);
                this.ComputeImpairmentData(step);

                if (computeAnalysisCategories)
                    this.AgregateAnalysisIncome(step);

                this.result.Compute(this, step);

                AggregateCategoriesData(step);

                if (MainForm.IsBeta)
                {
                    foreach (ProductData pd in this.productDatas)
                    {
                        if (double.IsInfinity(pd.CurrFace[step]))
                            throw new Exception("Exception Current Face=Infinity at step=" + step.ToString() + " for product " + pd.Product.ToString());
                    }
                }
            }
            //this.UpdateOtherItemAssumptionWithApplicationFeesAndPrepayementFees();

            this.ComputeLcr();

            if (parameters.ComputeGap)
                ComputeGaps(productDatas.Where(b => !b.Product.IsDerivative && b.Product.Attrib != ProductAttrib.CashAdj), parameters.CashflowPresentation);
        }

        private double ZeroIfNaN(double value)
        {
            return double.IsNaN(value) ? 0 : value;
        }



        private void ComputeProductHasRoll()
        {
            foreach (ProductData productData in this.ProductDatas)
            {
                this.productHasRoll[productData.Product.Id] = parameters.RollEnabled && productData.Product.HasRoll &&
                                                              this.ProductDatas.Any(
                                                                  r =>
                                                                  r.Product.Attrib == ProductAttrib.Roll &&
                                                                  Product.IsRollOf(productData.Product.RollSpec,
                                                                                   r.Product.ALMID));
            }
        }

        private void ComputeProductsFxRates()
        {
            for (int step = 0; step < this.ResultMonths; step++)
            {
                foreach (ProductData productData in this.ProductDatas)
                {
                    double fxRate = GetProductFxRate(productData, step, this.fxRates);
                    productData.FxRate[step] = fxRate;
                }
            }
        }

        private void ComputeLcr()
        {
            double[] totalDividends = new double[parameters.MaxDuration];
            for (int i = 0; i < parameters.MaxDuration; i++)
            {
                totalDividends[i] = this.dividendValues.Sum(p => p.Value[i]);
            }
            this.lcr = new Lcr(this.ValuationDate, this.productDatas, totalDividends, parameters.ResultSteps);

            for (int i = 0; i < parameters.MaxDuration; i++)
            {
                this.lcr.Compute(i);
            }
        }

        private void FindSpecialBonds()
        {
            this.assetsTreasuryProductDatas.Clear();
            this.liabilitiesTreasuryProductDatas.Clear();
            this.receivableProductDatas.Clear();
            this.payableProductDatas.Clear();

            this.modelImpairmentsProductData = null;
            this.ibnrProductData = null;
            this.reservesProductData = null;
            this.otherComprehensiveIncomeProductData = null;
            this.assetCashAdjProductData = null;
            this.liabilitiesCashAdjProductData = null;
            this.positiveDerivativesValuationProductData = null;
            this.negativeDerivativesValuationProductData = null;

            foreach (ProductData productData in this.ProductDatas)
            {
                Product product = productData.Product;
                if (product.Attrib == ProductAttrib.Treasury)
                {
                    if (product.Category.BalanceType == BalanceType.Asset)
                    {
                        this.assetsTreasuryProductDatas.Add(productData.Currency, productData);
                    }
                    else if (product.Category.BalanceType == BalanceType.Liability)
                    {
                        this.liabilitiesTreasuryProductDatas.Add(productData.Currency, productData);
                    }
                }
                if (product.Attrib == ProductAttrib.Receivable)
                {
                    this.receivableProductDatas.Add(productData.Currency, productData);
                }
                if (product.Attrib == ProductAttrib.Payable)
                {
                    this.payableProductDatas.Add(productData.Currency, productData);
                }

                if (product.Attrib == ProductAttrib.PositiveDerivativesValuation)
                    this.positiveDerivativesValuationProductData = productData;
                if (product.Attrib == ProductAttrib.NegativeDerivativesValuation)
                    this.negativeDerivativesValuationProductData = productData;

                if (product.Category.Type == CategoryType.MarginCallPaid)
                    this.marginCallPaidProductData = productData;
                if (product.Category.Type == CategoryType.MarginCallReceived)
                    this.marginCallReceivedProductData = productData;


                if (product.Attrib == ProductAttrib.ModelImpairments) this.modelImpairmentsProductData = productData;
                if (product.Attrib == ProductAttrib.IBNR) this.ibnrProductData = productData;
                if (product.Attrib == ProductAttrib.Reserves) this.reservesProductData = productData;
                if (product.Attrib == ProductAttrib.OtherComprehensiveIncome)
                {
                    this.otherComprehensiveIncomeProductData = productData;
                    this.otherComprehensiveIncomeProductData.Mouvements = new Mouvements[resultMonths];
                }
                if (product.Attrib == ProductAttrib.CashAdj)
                {
                    if (product.Category.BalanceType == BalanceType.Asset) this.assetCashAdjProductData = productData;
                    if (product.Category.BalanceType == BalanceType.Liability)
                        this.liabilitiesCashAdjProductData = productData;
                }
            }

            this.currencies = assetsTreasuryProductDatas.Keys;

            this.AssertSpecialproductsExist();
        }

        private void AssertSpecialproductsExist()
        {
            Debug.Assert(this.positiveDerivativesValuationProductData != null);
            Debug.Assert(this.negativeDerivativesValuationProductData != null);
            Debug.Assert(this.modelImpairmentsProductData != null);
            Debug.Assert(this.reservesProductData != null);
            Debug.Assert(this.otherComprehensiveIncomeProductData != null);
            Debug.Assert(this.assetCashAdjProductData != null);
            Debug.Assert(this.liabilitiesCashAdjProductData != null);

            Debug.Assert(assetsTreasuryProductDatas.Count == liabilitiesTreasuryProductDatas.Count);
            Debug.Assert(assetsTreasuryProductDatas.Count == receivableProductDatas.Count);
            Debug.Assert(assetsTreasuryProductDatas.Count == payableProductDatas.Count);
        }

        private void ComputeDividends(int step)
        {
            if (step == 0) return;

            int monthNow = (this.valuationDateMonth + step - 2) % 12 + 1;

            if (monthNow != this.dividendPaymentMonth) return;

            int firstStep = Math.Max(0, step - 12) + 1;

            double lastYearNetIncome = 0;
            for (int i = firstStep; i <= step; i++)
            {
                lastYearNetIncome += this.netIncome[i];
            }

            foreach (Dividend dividend in this.dividends)
            {
                double value;
                if (dividend.IsValueAmount)
                {
                    value = (double)dividend.Minimum;
                }
                else
                {
                    value = (double)this.vectorAndVariableValues[step][dividend.Amount];
                }

                this.dividendValues[dividend][step] = Math.Max(0, value * lastYearNetIncome);
            }
        }

        public void AggregateCategoriesData(int step)
        {
            Category bs = Categories.FirstOrDefault(c => c.BalanceType == BalanceType.Portfolio);
            if (bs == null) return;
            foreach (ProductData pd in productDatas)
            {
                CategoryData data = categoryData[pd.Product.Category.ALMID];
                data.BookValue[step] += pd.GetBookValueInValuationCurrency(step);
                data.CouponIncome[step] += pd.CouponIncome[step] * pd.FxRate[step];
                data.GainAndLosesIncome[step] += pd.GainAndLosesIncome[step] * pd.FxRate[step];
                data.InterestIncome[step] += pd.InterestIncome[step] * pd.FxRate[step];
                data.LossImpairment[step] += pd.LossImpairment[step] * pd.FxRate[step];
                data.PrincipalIncome[step] += pd.PrincipalIncome[step] * pd.FxRate[step];
            }

            AggregateCategoryData(bs, step);
        }

        private void AggregateCategoryData(Category category, int step)
        {
            foreach (Category child in category.Children)
            {
                this.AggregateCategoryData(child, step);
                categoryData[category.ALMID].BookValue[step] += categoryData[child.ALMID].BookValue[step];
                categoryData[category.ALMID].CouponIncome[step] += categoryData[child.ALMID].CouponIncome[step];
                categoryData[category.ALMID].GainAndLosesIncome[step] += categoryData[child.ALMID].GainAndLosesIncome[step];
                categoryData[category.ALMID].InterestIncome[step] += categoryData[child.ALMID].InterestIncome[step];
                categoryData[category.ALMID].LossImpairment[step] += categoryData[child.ALMID].LossImpairment[step];
                categoryData[category.ALMID].PrincipalIncome[step] += categoryData[child.ALMID].PrincipalIncome[step];
            }

        }

        private void UpdateReceivablePayableBonds(int step)
        {
            foreach (Currency currency in currencies)
            {
                this.receivableProductDatas[currency].CurrFace[step] = 0;
                this.payableProductDatas[currency].CurrFace[step] = 0;
            }

            foreach (ProductData productData in this.ProductDatas)
            {
                double accr = productData.Product.Sign * productData.GetAccruedInterest(step);
                //MTU manage the recovery lag
                if (productData.Product.IsModelAndNotDerivative)
                {
                    double recovery_to_add = productData.ModelData.Recovery[step];
                    double recovery_to_substract = (productData.RecoveryLagInMonths < step ? productData.ModelData.Recovery[step - productData.RecoveryLagInMonths] : 0);

                    productData.ModelData.CumulatedReceivablePayable[step] = (step > 0 ? productData.ModelData.CumulatedReceivablePayable[step - 1] : 0)
                                                                                    + productData.Product.Sign * (recovery_to_add - recovery_to_substract);

                    accr += productData.ModelData.CumulatedReceivablePayable[step];
                }
                accr = this.ZeroIfNaN(accr);
                if (accr != 0)
                {
                    if (accr > 0)
                    {
                        this.receivableProductDatas[productData.Currency].CurrFace[step] += accr;
                    }
                    else
                    {
                        this.payableProductDatas[productData.Currency].CurrFace[step] += -accr;
                    }
                }


            }
        }

        //MTU
        private void UpdateDerivativesValuationBonds(int step)
        {
            if (this.positiveDerivativesValuationProductData == null ||
                this.negativeDerivativesValuationProductData == null ||
                this.marginCallPaidProductData == null ||
                this.marginCallReceivedProductData == null) return;

            this.positiveDerivativesValuationProductData.CurrFace[step] = 0;
            this.negativeDerivativesValuationProductData.CurrFace[step] = 0;
            this.marginCallReceivedProductData.CurrFace[step] = 0;
            this.marginCallPaidProductData.CurrFace[step] = 0;

            IList<Product> used = new List<Product>();
            foreach (ProductData productData in this.ProductDatas)
            {
                if (!(productData.Product.IsDerivative)) continue;
                if (used.Contains(productData.Product)) continue;

                int expiryStep = PricerUtil.GetProductExpiryStep(productData.Product, this.valuationDate, parameters.IsComputationDaily);

                if (step <= expiryStep)
                {
                    ProductData otherSwapLegData = this.ProductDatas.FirstOrDefault(pd => pd.Product == productData.Product.OtherSwapLeg);

                    if (otherSwapLegData == null) continue;

                    used.Add(otherSwapLegData.Product);
                    double mtm = productData.GetMtm(otherSwapLegData, step);

                    mtm = this.ZeroIfNaN(mtm);


                        if (mtm > 0)
                        {
                            this.positiveDerivativesValuationProductData.CurrFace[step] += mtm;
                        }
                        else
                        {
                            this.negativeDerivativesValuationProductData.CurrFace[step] -= mtm;
                        }
                        if (productData.Product.IsMargin)
                        {
                            if (mtm < 0)
                            {
                                this.marginCallPaidProductData.CurrFace[step] -= mtm;
                            }
                            else
                            {
                                this.marginCallReceivedProductData.CurrFace[step] += mtm;
                            }
                        }
                }
            }
        }

        private void UpdateReservesBonds(int step)
        {
            if (this.reservesProductData == null) return;

            if (step > 0)
            {
                double dividendSum = this.dividendValues.Values.Sum(dv => dv[step]);

                double prevReserves = this.reservesProductData.CurrFace[step - 1];
                this.reservesProductData.CurrFace[step] = prevReserves + this.netIncome[step] - dividendSum;
            }
        }

        private void ComputeInterestOnDerivatives(int step)
        {
            double interestReceived = 0;
            double interestPayed = 0;
            foreach (ProductData productData in this.ProductDatas)
            {
                Product product = productData.Product;
                if (!product.IsDerivative) continue;

                int sign = product.BalanceSign * product.SwapLegSign;
                double income = this.ZeroIfNaN(productData.GetInterestIncomeInValuationCurrency(step) * productData.Product.BalanceSign);
                if (sign > 0)
                {
                    interestReceived += income;
                }
                else
                {
                    interestPayed -= income;
                }
            }

            this.InterestReceivedOnDerivatives[step] = interestReceived;
            this.InterestPayedOnDerivatives[step] = interestPayed;
        }


        private void UpdateOtherComprehensiveIncomeBonds(int step)
        {
            if (this.otherComprehensiveIncomeProductData == null) return;

            Mouvements mouvements = otherComprehensiveIncomeProductData.Mouvements[step] = new Mouvements();

            double stepOci = 0;
            double debitOci = 0;
            foreach (ProductData productData in this.ProductDatas)
            {
                if (productData.Product.Accounting != ProductAccounting.AFS) continue;

                //mtu : compute application fees
                double pnl = productData.GetGainAndLosesIncomeInValuationCurrency(step) * productData.Product.Sign;
                pnl = this.ZeroIfNaN(pnl);
                mouvements.Add(productData.Product.ALMIDDisplayed + "\u00A0P&L", pnl);

                stepOci += pnl;

                int expiryStep = PricerUtil.GetProductExpiryStep(productData.Product, this.valuationDate, parameters.IsComputationDaily)+1;//MTU derivative pb 2014 Serge
                if (step == expiryStep)// && !productData.Product.IsDerivative)
                {
                    double all = productData.GetAllGainAndLosesIncomeInValuationCurrency(step) * productData.Product.Sign;
                    mouvements.Add(productData.Product.ALMIDDisplayed + "\u00A0DebitOCI", all);

                    debitOci += all;
                }
            }

            double prevOci = step == 0
                                 ? this.otherComprehensiveIncomeProductData.CurrFace[0]
                                 : this.otherComprehensiveIncomeProductData.CurrFace[step - 1];

            this.otherComprehensiveIncomeProductData.CurrFace[step] = prevOci + stepOci - debitOci;
            //if (assum.SubtotalGroup == OtherItemAssum.CommisionsAndOthersGroup)
            //    this.commisionsAndOthersGroupValue[i] += value;

        }

        private void UpdateImparmentsBonds(int step)
        {
            if (step == 0)
            {
                this.impairments[0] = 0;
                return;
            }

            if (this.modelImpairmentsProductData == null) return;

            double deltaImpairments = 0;
            double ibnr = 0;
            foreach (ProductData productData in this.ProductDatas)
            {
                if (productData.Product.IsModelAndNotDerivative)
                {
                    deltaImpairments += this.ZeroIfNaN(productData.GetLossImpairmentInValuationCurrency(step));
                    ibnr += productData.ModelData.Incurred_but_not_reported[step] * productData.FxRate[step];
                }
            }
            this.impairments[step] = deltaImpairments;

            //MTU_IMPAIR 
            this.modelImpairmentsProductData.CurrFace[step] = 0;

            //this.modelImpairmentsProductData.CurrFace[step] = (this.modelImpairmentsProductData.CurrFace[step - 1] +
            //                                              deltaImpairments);
            this.ibnrProductData.CurrFace[step] = this.ibnrProductData.CurrFace[step - 1] + ibnr;
            //MTU_IMPAIR 
        }

        private void UpdateRollBond(int step, ProductData rollProductData, double face)
        {
            if (!parameters.RollEnabled) return;

            if (rollProductData.Product.Attrib != ProductAttrib.Roll) return;

            if (step == 0)
            {
                return;
            }

            double sumDelta = 0;
            foreach (ProductData productData in this.ProductDatas)
            {
                Product product = productData.Product;
                if (product.Attrib == ProductAttrib.Notional) continue;

                if (product.RollSpec != Product.NoRoll &&
                    Product.IsRollOf(product.RollSpec, rollProductData.Product.ALMID))
                {
                    double val = productData.Product.BalanceSign * this.ZeroIfNaN(productData.GetPrincipalIncomeInValuationCurrency(step)) *
                                 productData.RollRatioValues[step];
                    if (val * productData.Product.BalanceSign > 0) sumDelta += val; // pour �viter les roll n�gatifs dans le cas des growth
                }
            }

            double currFace = face + rollProductData.Product.BalanceSign * sumDelta / rollProductData.FxRate[step];
            if (currFace != 0)
            {
                RollProductData roll = rollProductData as RollProductData;
                if (roll != null)
                {
                    roll.AddSubRoll(step, currFace, this.discountCurveCollection);
                }
            }
        }

        private void UpdateProductsStartingBeforeValuationDate()
        {
            foreach (ProductData productData in this.ProductDatas)
            {
                int stepsBeforeStart = (int)Math.Round((this.ValuationDate - productData.StartDate).TotalDays / 30.0);

                int expiryStep = PricerUtil.GetProductExpiryStep(productData.Product, this.valuationDate, parameters.IsComputationDaily);
                if (stepsBeforeStart >= expiryStep)
                {
                    productData.CurrFace[0] = 0;
                    continue;
                }

                if (productData.Product.InvestmentRule == ProductInvestmentRule.Growth)
                {
                    for (int i = 0; i < stepsBeforeStart; i++)
                    {
                        productData.CurrFace[0] += productData.CurrFace[0] * productData.InvestmentParameter[0] / 12;
                    }
                }
            }
        }

        private void UpdateInvestmentRules(int step)
        {
            if (step == 0) return;

            IDictionary<Currency, double> deltaBookValues = new Dictionary<Currency, double>();
            foreach (Currency currency in this.currencies)
            {
                deltaBookValues[currency] = 0;
            }

            foreach (ProductData productData in this.ProductDatas)
            {
                if (productData.StartStep >= step)
                {
                    continue;
                }

                // productData.UpdateGrowth(this.vectorAndVariableValues, step);
                double prevBookValue = this.ZeroIfNaN(productData.GetDirtyBookValue(step));

                if (productData.Product.InvestmentRule == ProductInvestmentRule.Growth)
                {
                    //productData.CurrFace[step] = productData.CurrFace[step]*
                    //                             (1 + productData.InvestmentParameter[step] / 12);
                }
                else if (productData.Product.InvestmentRule == ProductInvestmentRule.Var)
                {
                    productData.CurrFace[step] = productData.InvestmentParameter[step];
                }
                else if (productData.Product.InvestmentRule == ProductInvestmentRule.Roll)
                {
                    this.UpdateRollBond(step, productData, productData.InvestmentParameter[step]);
                }

                double deltaBookValue = productData.Product.InvestmentRule == ProductInvestmentRule.Constant ?
                    0 :
                    (this.ZeroIfNaN(productData.GetDirtyBookValue(step)) - prevBookValue) * productData.Product.Sign;

                if (productData.Product.ProductType == ProductType.Swaption)
                {
                    int expiryStep = PricerUtil.GetProductExpiryStep(productData.Product, this.valuationDate, parameters.IsComputationDaily);
                    if (step == expiryStep + 1)
                    {
                        if (productData.BookPrice[step - 1] > 0)
                        {
                            double bookValueBefore = productData.LinkedProductDatas.Sum(pd => this.ZeroIfNaN(pd.GetDirtyBookValue(step)) * pd.Product.Sign);
                            foreach (ProductData linkedPd in productData.LinkedProductDatas)
                            {
                                linkedPd.CurrFace[step] = productData.CurrFace[step - 1];
                            }

                            double bookValueAfter = productData.LinkedProductDatas.Sum(pd => this.ZeroIfNaN(pd.GetDirtyBookValue(step)) * pd.Product.Sign);

                            deltaBookValue += bookValueAfter - bookValueBefore;
                        }
                    }
                }

                deltaBookValues[productData.Currency] += deltaBookValue;
            }

            foreach (Currency currency in currencies)
            {
                ProductData assetsTreasuryProductData = this.assetsTreasuryProductDatas[currency];

                double bookDirtyPrice = this.ZeroIfNaN(assetsTreasuryProductData.GetBookDirtyPrice(step));

                if (bookDirtyPrice != 0)
                {
                    assetsTreasuryProductData.CurrFace[step] -= deltaBookValues[currency] / bookDirtyPrice;
                }
            }


        }

        private void UpdateCashAdjBonds(int step)
        {
            if (this.assetCashAdjProductData == null || this.liabilitiesCashAdjProductData == null) return;

            double balance = 0;
            foreach (ProductData productData in this.ProductDatas)
            {
                if (productData.Product.Attrib == ProductAttrib.Notional ||
                    productData.Product.IsDerivative) continue;

                //todo : MTU : tester les dates pour les In Future, pas de BookValue beetween
                //if (step == 0 && productData.Product.Attrib ==ProductAttrib.InFuture) continue; 

                double bookValue = this.ZeroIfNaN(productData.GetBookValueInValuationCurrency(step));

                balance += productData.Product.Sign * bookValue;
            }

            BalanceBL.AjustCashAdjBonds(ref this.assetCashAdjProductData.CurrFace[step],
                                        ref this.liabilitiesCashAdjProductData.CurrFace[step], balance);
        }

        private void UpdateTreasuryBonds(int step)
        {
            this.TreasurySourceMouvements[step] = new Mouvements();
            //nothing in step == 0, non mouvements in opening balance
            if (step == 0) return;


            IDictionary<Currency, double> treasury = new Dictionary<Currency, double>();
            foreach (Currency currency in currencies)
            {
                treasury[currency] = 0;
            }
            double value;
            
            foreach (ProductData productData in this.ProductDatas)
            {
                if (!productData.Product.IsDerivative &&
                    productData.Product.Attrib != ProductAttrib.Notional &&
                    productData.Product.ProductType != ProductType.Amortizing)
                {
                    value = this.ZeroIfNaN(productData.PrincipalIncome[step] * productData.Product.Sign * 1);
                    treasury[productData.Currency] += value;
                    this.TreasurySourceMouvements[step].Add(productData.Product.ALMIDDisplayed + " (P)", value);
                }
                value = this.ZeroIfNaN(productData.CouponIncome[step] * productData.Product.Sign);
                treasury[productData.Currency] += value;
                this.TreasurySourceMouvements[step].Add(productData.Product.ALMIDDisplayed + "(I)", value);


                if (productData.Product.IsModelAndNotDerivative && productData.RecoveryLagInMonths < step)
                { //MTU : recovery lag  integration
                    value = productData.ModelData.Recovery[step - productData.RecoveryLagInMonths];
                    treasury[productData.Currency] += value;
                    this.TreasurySourceMouvements[step].Add(productData.Product.ALMIDDisplayed + "(R)", value);
                }

                if ((productData.Product.Attrib == ProductAttrib.InFuture && step == productData.StartStep) ||
                    (productData.Product.Attrib == ProductAttrib.InBalance && step == productData.ImpactTreasuryStep))
                //(productData.Product.Attrib == ProductAttrib.InBalance && step == productData.ImpactTreasuryStep ))
                {
                    value = this.ZeroIfNaN(productData.GetBookValue(step) * productData.Product.Sign);
                    treasury[productData.Currency] -= value;
                    this.TreasurySourceMouvements[step].Add(productData.Product.ALMIDDisplayed + " Invest", -1 * value);
                }
                if (productData.Product.InvestmentRule == ProductInvestmentRule.Growth)
                {
                    value = this.ZeroIfNaN(productData.Product.Sign * productData.Growth[step] * productData.MarginAccruedInterest[step]);
                    treasury[productData.Currency] -= value;
                    this.TreasurySourceMouvements[step].Add(productData.Product.ALMIDDisplayed + " Growth", -1 * value);
                }
                if (productData.Product.IsDerivative && productData.Product.IsMargin)
                {//pour chaque leg on impact la tr�so de sa variation de Mtm du leg uniquement (comme �a chaque leg est pris en compte)
                    value = this.ZeroIfNaN(productData.GetLegMtmVariation(step));
                    treasury[productData.Currency] += value;
                    this.TreasurySourceMouvements[step].Add(productData.Product.ALMIDDisplayed + " Leg Margin Call", value);
                }

            }

            if (step > 0)
            {
                value = this.otherItemAssumptionsSumWithoutAmortizing[step];
                treasury[parameters.ValuationCurrency] += value;
                // MTU : pas de commissions charges en bilan d'ouverture
                this.TreasurySourceMouvements[step].Add("<OIA>", value);
            }

            treasury[parameters.ValuationCurrency] += this.taxes[step];
            this.TreasurySourceMouvements[step].Add("<Taxes>", this.taxes[step]);

            double dividendSum = this.dividendValues.Sum(p => p.Value[step]);
            treasury[parameters.ValuationCurrency] -= dividendSum;
            if (dividendSum > 0.01) this.TreasurySourceMouvements[step].Add("<Dividends>", -1 * dividendSum);
            this.TreasurySourceMouvements[step].Sort();

            foreach (Currency currency in currencies)
            {
                ProductData assetsTreasuryProductData = this.assetsTreasuryProductDatas[currency];
                ProductData liabilitiesTreasuryProductData = this.liabilitiesTreasuryProductDatas[currency];

                double liabilitiesDirtyBookPrice = this.ZeroIfNaN(liabilitiesTreasuryProductData.GetBookDirtyPrice(step));

                treasury[currency] -= liabilitiesTreasuryProductData.CurrFace[step] * liabilitiesDirtyBookPrice;

                double assetsDirtyBookPrice = this.ZeroIfNaN(assetsTreasuryProductData.GetBookDirtyPrice(step));

                if (assetsDirtyBookPrice != 1 || liabilitiesDirtyBookPrice != 1)
                {

                }

                if (assetsDirtyBookPrice != 0)
                {
                    assetsTreasuryProductData.CurrFace[step] += treasury[currency] / assetsDirtyBookPrice;

                    if (assetsTreasuryProductData.CurrFace[step] < 0 && liabilitiesDirtyBookPrice != 0)
                    {
                        liabilitiesTreasuryProductData.CurrFace[step] =
                            -assetsTreasuryProductData.CurrFace[step] *
                            assetsDirtyBookPrice /
                            liabilitiesDirtyBookPrice;
                        assetsTreasuryProductData.CurrFace[step] = 0;
                    }
                    else
                    {
                        liabilitiesTreasuryProductData.CurrFace[step] = 0;
                    }
                }
            }
        }


        private void RunScenarioForBonds(int step, IEnumerable<IEnumerable<ProductData>> datasNonSwaption, IEnumerable<IEnumerable<ProductData>> datasSwaption)
        {
            this.RunScenarioForProducts(step, datasNonSwaption);
            this.RunScenarioForProducts(step, datasSwaption);

        }

        private void RunScenarioForProducts(int step, IEnumerable<IEnumerable<ProductData>> datas)
        {
            bool executeInParallel = this.isMonoScenario;

#if DEBUG
            executeInParallel = false;
#endif
            ParallelOptions po = new ParallelOptions();
            Logger.Info("Starting RunScenarioForProducts with  executeInParallel=" + executeInParallel + ", with a ParallelOptions.MaxDegreeOfParallelism=" + po.MaxDegreeOfParallelism);

            if (executeInParallel)
            {
                //Parallel.ForEach(datas, pd => this.RunProductStep(pd, step));
                Parallel.ForEach(datas, pds => this.BatchRunProductStep(pds, step));
            }
            else
            {
                foreach (IEnumerable<ProductData> productDatas in datas)
                {
                    this.BatchRunProductStep(productDatas, step);
                }
            }


        }

        private void ComputeGaps(IEnumerable<ProductData> datas, int cashflowPresentation)
        {
            //gap computation
            //double forActualizationSum;
            IDictionary<int, int> CRDNul = new Dictionary<int, int>();


            ProductData[] treasuries =
                this.ProductDatas.Where(p => p.Product.Attrib == ProductAttrib.Treasury).ToArray();

            double sum;
            this.Gaps[0] = new double[resultMonths];
            //GAP : if Stock than do not do the for
            if (cashflowPresentation != Param.GAP_STOCK)
            {
                for (int i = 0; i < resultMonths; i++)
                {
                    sum = 0;
                    foreach (ProductData treasury in treasuries)
                    {
                        sum += treasury.CurrFace[i] * treasury.Product.Sign;
                    }
                    this.Gaps[0][i] = sum;
                }
            }


            ProductData[] subDatas = datas.Where(pd => pd.Product.Attrib != ProductAttrib.Treasury).ToArray();
            for (int line = 1; line < resultMonths; line++)
            {
                this.Gaps[line] = new double[resultMonths];
                //forActualizationSum = 0;
                for (int column = 0; column < resultMonths; column++)
                {
                    if (line <= resultMonths - 2 - column)
                    {
                        foreach (var productData in subDatas)
                        {
                            if (CRDNul.ContainsKey(productData.Product.ALMID) &&
                                (line > CRDNul[productData.Product.ALMID] + 2 || column > CRDNul[productData.Product.ALMID] - 1))
                                continue;
                            double gap = 0;
                            if (parameters.GapType == Param.GAP_TAUX
                                && productData.Product.ProductType == ProductType.BondVr
                                && column > 0
                                //&& (Math.Abs(productData.CouponValues[column - 1] - productData.CouponValues[column]) > 0.000001))
                                && productData.FirstInterestStep() == column)
                            {
                                //we are changing coupon value in % this month ?
                                gap = productData.GetGap(column, line, cashflowPresentation, true);
                                if (!CRDNul.ContainsKey(productData.Product.ALMID))
                                    CRDNul.Add(productData.Product.ALMID, column);
                            }
                            else
                            {
                                gap = productData.GetGap(column, line, cashflowPresentation);
                            }

                            this.Gaps[line][column] += gap;
                            //forActualizationSum += gap;
                        }
                    }
                }
            }

            this.Gaps[resultMonths] = new double[resultMonths];
            ProductData[] regularPD =
                    this.ProductDatas.Where(p => p.StartStep + p.Product.IntDuration >= resultMonths && !p.Product.isRollOrInFutureOrTreasury()).ToArray();
            ProductData[] rollsAndInFuturePD =
                this.ProductDatas.Where(p => p.StartStep + p.Product.IntDuration >= resultMonths && p.Product.isRollOrInFuture()).ToArray();


            double coeff;
            for (int column = 0; column < resultMonths; column++)
            {
                sum = 0;
                foreach (ProductData regPD in regularPD)
                {
                    if (regPD.Product.AmortizationType != ProductAmortizationType.NO_MATURITY)
                    {
                        sum += regPD.GetCurrFaceForResidualGap(resultMonths - 1) * regPD.Product.Sign;
                    }
                    else
                    {
                        if (resultMonths - 2 - column > 0)
                            sum += regPD.GapNoMaturityRate[resultMonths - 2 - column] * regPD.GetCurrFaceForResidualGap(column) * regPD.Product.Sign;
                        else
                            sum += regPD.GetCurrFaceForResidualGap(column) * regPD.Product.Sign;
                    }

                }
                foreach (ProductData rIFPD in rollsAndInFuturePD)
                {
                    if (rIFPD.Product.isRoll())
                    {
                        foreach (ProductData subRollPD in ((RollProductData)rIFPD).SubRollDatas)
                        {
                            if (subRollPD.StartStep <= column)
                            {
                                coeff = rIFPD.Product.AmortizationType != ProductAmortizationType.NO_MATURITY
                                            ? 1
                                            : rIFPD.GetGapCoef(column, cashflowPresentation);

                                sum += coeff * subRollPD.GetCurrFaceForResidualGap(resultMonths - 1) * rIFPD.Product.Sign;
                            }
                        }
                    }
                    else
                    {//InFuture
                        if (rIFPD.StartStep >= column)
                        {
                            coeff = rIFPD.Product.AmortizationType != ProductAmortizationType.NO_MATURITY
                                            ? 1
                                            : rIFPD.GetGapCoef(column, cashflowPresentation);//todo MTU : transformer colonne en step

                            sum += coeff * rIFPD.GetCurrFaceForResidualGap(resultMonths - 1) * rIFPD.Product.Sign;
                        }

                    }
                }
                this.Gaps[resultMonths][column] = sum;
            }

        }

        private void BatchRunProductStep(IEnumerable<ProductData> productDatas, int step)
        {
            Util.setDefaultCultureInfoToThread();
            foreach (ProductData productData in productDatas)
            {
                RunProductStep(productData, step);
            }
        }

        private void RunProductStep(ProductData productData, int step)
        {

            try
            {
                productData.Run(step);
            }
            catch (Exception ex)
            {
                throw new SimulationException(new ValidationMessage(productData.Product, ex.Message), ex);
            }
        }

        private void AggregateBookValue(int step)
        {
            foreach (ProductData productData in this.ProductDatas)
            {
                double[] categoryBookValueValue = this.CategoryBookValue[productData.Product.Category.Id];

                double bookValue = this.ZeroIfNaN(productData.GetBookValueInValuationCurrency(step));

                if (productData.Product.IsDerivative || productData.Product.Attrib == ProductAttrib.Notional)
                    continue;

                categoryBookValueValue[step] += bookValue;

                if (productData.Product.Category.BalanceType == BalanceType.Asset)
                {
                    this.totalAssets[step] += bookValue;
                }
                else if (productData.Product.Category.BalanceType == BalanceType.Equity)
                {
                    this.totalEquity[step] += bookValue;
                }
            }
        }

        private void AggregateIncome(int step)
        {
            foreach (ProductData productData in this.ProductDatas)
            {
                if (productData.Product.IsDerivative) continue;

                if (productData.Product.ProductType == ProductType.StockEquity)
                {
                    this.dividendsOnEquity[step] += this.ZeroIfNaN(productData.GetInterestIncomeInValuationCurrency(step));
                }
                else
                {
                    this.CategoryIncome[productData.Product.Category.Id][step] += this.ZeroIfNaN(productData.GetInterestIncomeInValuationCurrency(step));
                }
            }
        }



        private static double GetProductFxRate(ProductData productData, int i, IDictionary<Currency, double[]> fxRates)
        {
            if (productData.Product.Currency == null || !fxRates.ContainsKey(productData.Product.Currency))
            {
                return 1;
            }
            return fxRates[productData.Product.Currency][i];
        }

        private void ComputeIncomePreTax(int step)
        {
            this.incomePreTax[step] = this.incomeBeforeImpairmentCharge[step] + this.impairments[step];
        }

        private void ComputeTaxes(int step)
        {
            double tax = this.incomePreTax[step] * this.standardTax / 100;
            this.taxes[step] = /*(tax < 0) ? 0 : */ tax;
            this.taxes[step] = -this.taxes[step];
        }

        private void ComputeImpairmentData(int step)
        {
            if (step == 0) return;

            foreach (ProductData productData in this.ProductDatas)
            {
                if (productData.Product.IsModelAndNotDerivative)
                {
                    this.impairementData[productData.Product.Category.Id][step] +=
                        this.ZeroIfNaN(productData.GetLossesImpairmentInValuationCurrency(step));
                }
            }
        }

        private void ComputeNetIncome(int step)
        {
            this.netIncome[step] = this.incomePreTax[step] + this.taxes[step];
        }

        private void ComputeNetInterestIncome(int step)
        {
            double gainLoses = 0;
            foreach (ProductData productData in this.ProductDatas)
            {
                if (productData.Product.ProductType != ProductType.StockEquity)
                    this.netInterestIncome[step] += this.ZeroIfNaN(productData.GetInterestIncomeInValuationCurrency(step) * productData.Product.BalanceSign);

                //double prevGainLoses = step == 0 ? 0 : this.ZeroIfNaN(productData.GetGainAndLosesIncomeInValuationCurrency(step - 1));
                //gainLoses += this.ZeroIfNaN(productData.GetGainAndLosesIncomeInValuationCurrency(step)) - prevGainLoses;

            }

            this.netInterestIncome[step] += gainLoses;
        }

        private void ComputeIncomeBeforeImpairmentCharge(int step)
        {
            this.incomeBeforeImpairmentCharge[step] = this.netInterestIncome[step] + this.otherItemAssumptionsSum[step] +
                                                      this.gainLosesOnFinancialInstrument[step] + this.fx_PnL[step] + this.dividendsOnEquity[step]/*+ dividendsOnEquity[step]*/;
        }

        private void ComputeGainLosesOnFinancialInstrument(int step)
        {
            this.GainLosesSourceMouvements[step] = new Mouvements();

            foreach (ProductData productData in this.ProductDatas)
            {
                double gainLoses = 0;

                if (productData.Product.Accounting != ProductAccounting.AFS)
                {
                    double value = this.ZeroIfNaN(productData.GetGainAndLosesIncomeInValuationCurrency(step)) * productData.Product.Sign;
                    if (value > 0.01) this.GainLosesSourceMouvements[step].Add(productData.Product.ALMIDDisplayed + "\u00A0P&L", value);
                    gainLoses += value;
                }
                int expiryStep = PricerUtil.GetProductExpiryStep(productData.Product, this.valuationDate, parameters.IsComputationDaily)+1;//MTU derivative pb serge 2014
                if (productData.Product.Accounting == ProductAccounting.AFS && step == expiryStep)// && !productData.Product.IsDerivative)
                {
                    double value = this.ZeroIfNaN(productData.GetAllGainAndLosesIncomeInValuationCurrency(step)) * productData.Product.Sign;
                    if (Math.Abs(value) > 0.001) this.GainLosesSourceMouvements[step].Add(productData.Product.ALMIDDisplayed + "\u00A0DebitOCI", value);
                    gainLoses += value;
                }

                this.gainLosesOnFinancialInstrument[step] += gainLoses;

                this.fx_PnL[step] += this.ZeroIfNaN(productData.FxPnlIncome[step]) * productData.Product.Sign;
            }
        }

        private void ComputeNetBankingIncome(int step)
        {
            KeyValuePair<OtherItemAssum, double[]>[] commisionsOia =
                this.otherItemAssumptionValues.Where(
                    pair => OtherItemAssum.CommisionsAndOthersGroup.Equals(pair.Key.SubtotalGroup, StringComparison.InvariantCultureIgnoreCase))
                    .ToArray();

            double commissions = 0;

            foreach (KeyValuePair<OtherItemAssum, double[]> commision in commisionsOia)
            {
                commissions += this.otherItemAssumptionValues[commision.Key][step];
            }

            //if (commisionsOia.Length >= 0)
            //{
            //    comissions += commisionsOia.Sum(pair => pair.Value[step]);
            //}

            this.netBankingIncome[step] = this.netInterestIncome[step] +
                                          this.gainLosesOnFinancialInstrument[step] +
                                          commissions +
                                          this.fx_PnL[step] + this.dividendsOnEquity[step]/*+ dividendsOnEquity[step]*/;
        }

        private void AggregateSumsAndDictionnaryOtherItemsAssumption()
        {
            for (int i = 0; i < this.ResultMonths; i++)
            {
                this.otherItemAssumptionsSum[i] = 0;
            }

            foreach (OtherItemAssum assum in this.otherItemAssumptions)
            {
                this.otherItemAssumptionValues[assum] = new double[this.ResultMonths];

                for (int i = 0; i < this.ResultMonths; i++)
                {
                    double value = assum.Values[i];

                    this.otherItemAssumptionValues[assum][i] = value;
                    this.otherItemAssumptionsSum[i] += value;
                    if (assum.Descrip != "Amortizing")
                        otherItemAssumptionsSumWithoutAmortizing[i] += value;
                }
            }
        }

        //private void UpdateOtherItemAssumptionWithApplicationFeesAndPrepayementFees()
        //{
        //    //treate the application fee
        //    foreach (ProductData productData in this.ProductDatas)
        //    {
        //        if (productData.HasApplicationFees())
        //        {
        //            for (int i = 0; i < this.ResultMonths; i++)
        //            {
        //                this.otherItemAssumptions[this.applicationFeeIndex].Values[i] += productData.ApplicationFee[i];
        //                this.otherItemAssumptionValues[this.otherItemAssumptions[this.applicationFeeIndex]][i] += productData.ApplicationFee[i];
        //                this.otherItemAssumptionsSum[i] += productData.ApplicationFee[i];
        //            }
        //        }
        //        if (productData.HasApplicationCharges())
        //        {
        //            for (int i = 0; i < this.ResultMonths; i++)
        //            {
        //                this.otherItemAssumptions[this.applicationChargeIndex].Values[i] += productData.ApplicationCharge[i];
        //                this.otherItemAssumptionValues[this.otherItemAssumptions[this.applicationChargeIndex]][i] += productData.ApplicationCharge[i];
        //                this.otherItemAssumptionsSum[i] += productData.ApplicationCharge[i];
        //            }
        //        }

        //        if (productData.HasPrepayementFees())
        //        {
        //            for (int i = 0; i < this.ResultMonths; i++)
        //            {
        //                this.otherItemAssumptions[this.prepayementFeeIndex].Values[i] += productData.PrepayementFee[i];
        //                this.otherItemAssumptionValues[this.otherItemAssumptions[this.prepayementFeeIndex]][i] += productData.PrepayementFee[i];
        //                this.otherItemAssumptionsSum[i] += productData.PrepayementFee[i];
        //            }
        //        }
        //    }
        //}


        private void UpdateOtherItemAssumptionWithApplicationFeesAndPrepayementFees(int step)
        {
            if (step < 1) return;
            bool calcOIA = !parameters.IsComputationDaily || valuationDate.AddDays(step - 1).Day == 1;
            
            foreach (OtherItemAssum otherItemAssumption in otherItemAssumptions)
            {
                double computedValue = calcOIA ? Util.GetComputedValue(otherItemAssumption.Vect, vectorAndVariableValues[step], parser) : 0;
                otherItemAssumption.Values[step] += computedValue;
                if (otherItemAssumption.SubtotalGroup == OtherItemAssum.ImpairmentsRisk)
                {
                    if (!impairmentsRisk.ContainsKey(otherItemAssumption))
                    {
                        impairmentsRisk.Add(otherItemAssumption,new double[resultMonths]);    
                    }
                    impairmentsRisk[otherItemAssumption][step] += computedValue;
                }
                //otherItemAssumptionValues[otherItemAssumption][step] += otherItemAssumption.Values[step];
                //otherItemAssumptionsSum[step] += otherItemAssumption.Values[step];
            }


            //treate the application fee
            foreach (ProductData productData in this.ProductDatas)
            {
                if (productData.Product.ProductType == ProductType.Amortizing)
                {
                    this.otherItemAssumptions[this.amortizingIndex].Values[step] += -1 * productData.Product.Sign * productData.PrincipalIncome[step];
                }

                if (productData.HasApplicationFees())
                {
                    this.otherItemAssumptions[this.applicationFeeIndex].Values[step] += productData.ApplicationFee[step];
                    //this.otherItemAssumptionValues[this.otherItemAssumptions[this.applicationFeeIndex]][step] += productData.ApplicationFee[step];
                    //this.otherItemAssumptionsSum[step] += productData.ApplicationFee[step];
                }
                if (productData.HasApplicationCharges())
                {
                    this.otherItemAssumptions[this.applicationChargeIndex].Values[step] += productData.ApplicationCharge[step];
                    //this.otherItemAssumptionValues[this.otherItemAssumptions[this.applicationChargeIndex]][step] += productData.ApplicationCharge[step];
                    //this.otherItemAssumptionsSum[step] += productData.ApplicationCharge[step];
                }

                if (productData.HasPrepayementFees())
                {

                    this.otherItemAssumptions[this.prepayementFeeIndex].Values[step] += productData.PrepayementFee[step];
                    //this.otherItemAssumptionValues[this.otherItemAssumptions[this.prepayementFeeIndex]][step] += productData.PrepayementFee[step];
                    //this.otherItemAssumptionsSum[step] += productData.PrepayementFee[step];
                }
                if (productData.Product.AmortizationType == ProductAmortizationType.RESERVE_CASHFLOW ||
                    productData.Product.AmortizationType == ProductAmortizationType.RESERVE_NOMINAL)
                {
                    this.otherItemAssumptions[this.costInsuranceIndex].Values[step] += productData.ReserveCashflows[step] * productData.Product.Sign * (-1);
                }
            }
        }


        private void ComputeSolvency(int step)
        {
            foreach (SolvencyAdjustment adjustment in this.solvencyAdjustments)
            {
                double value = adjustment.Values[step];

                this.solvencyAdjustmentValues[adjustment.Description][step] = value;
            }

            this.solvency.Compute(step);
        }

        public double[] GetBookValue(Category category)
        {
            double[] bookValue;

            if (category.Children.Count > 0)
            {
                bookValue = new double[this.ResultMonths];
                foreach (Category child in category.Children)
                {
                    double[] values = this.GetBookValue(child);
                    for (int i = 0; i < this.ResultMonths; i++) bookValue[i] += values[i];
                }
                return bookValue;
            }

            return this.CategoryBookValue.TryGetValue(category.Id, out bookValue)
                       ? bookValue
                       : new double[this.ResultMonths];
        }

        //public double[] GetAnalysisCategoryBookValue(ISession session, string analysisCategory)
        //{
        //    double[] bookValue = new double[this.ResultMonths];
        //    IList<Category> categories = Category.FindAllByProperty(session, "AnalysisCategory", analysisCategory);

        //    foreach (Category category in categories)
        //    {
        //        if (category.Children.Count == 0)
        //        {
        //            double[] values = this.GetBookValue(category);
        //            for (int i = 0; i < this.ResultMonths; i++) bookValue[i] += values[i] * category.Sign;
        //        }
        //    }
        //    return bookValue;
        //}

        public void AgregateAnalysisIncome(int step)
        {

            double treasuryValue;
            foreach (ProductData productData in productDatas)
            {
                Category category = productData.Product.Category;
                string analysisCategory = category.AnalysisCategory;

                if (analysisCategory != Category.TREASURIES_ANALYSIS_CATEGORY)
                    treasuryValue = productData.IRCValue[step] * productData.FxRate[step];
                else
                {
                    treasuryValue = 0;
                }

                AnalysisCategoryIncome[Category.TREASURIES_ANALYSIS_CATEGORY][step] += treasuryValue * category.Sign;
                AnalysisCategoryIncome[analysisCategory][step] += (productData.GetInterestIncomeInValuationCurrency(step) - treasuryValue) * category.Sign;
                //AnalysisCategoryIncome[analysisCategory][step] += (productData.CouponIncome[step] * productData.FxRate[step] - treasuryValue) * category.Sign;
            }
            AnalysisCategoryIncome[Category.TREASURIES_ANALYSIS_CATEGORY][step] += 2 * (InterestReceivedOnDerivatives[step] - InterestPayedOnDerivatives[step]);
        }


        public double[] GetIncome(Category category)
        {
            double[] income;

            if (category.Children.Count > 0)
            {
                income = new double[this.ResultMonths];
                foreach (Category child in category.Children)
                {
                    double[] values = this.GetIncome(child);
                    for (int i = 0; i < this.ResultMonths; i++) income[i] += values[i];
                }
                return income;
            }

            return this.CategoryIncome.TryGetValue(category.Id, out income)
                       ? income
                       : new double[this.ResultMonths];
        }

        public double[] GetInterestIncomeBeforeImpairment()
        {
            return this.incomeBeforeImpairmentCharge;
        }

        public double[] GetImpairment()
        {
            return this.impairments;
        }
        public IDictionary<OtherItemAssum,double[]> GetImpairmentOIA()
        {
            return this.impairmentsRisk;
        }
        public double[] GetNetInterestIncome()
        {
            return this.netInterestIncome;
        }

        public double[] GetGainLosesOnFinancialInstrument()
        {
            return this.gainLosesOnFinancialInstrument;
        }

        public double[] GetFX_PnL()
        {
            return this.fx_PnL;
        }

        public double[] GetDividendsOnEquity()
        {
            return this.dividendsOnEquity;
        }

        public double[] GetIncomePreTax()
        {
            return this.incomePreTax;
        }

        public double[] GetTaxes()
        {
            return this.taxes;
        }

        public double[] GetNetIncome()
        {
            return this.netIncome;
        }

        public double[] GetTotalEquity()
        {
            return this.totalEquity;
        }

        public double[] GetTotalAssets()
        {
            return this.totalAssets;
        }

        public IDictionary<OtherItemAssum, double[]> GetOtherItemAssumptionsNotImpairments()
        {
            //return this.otherItemAssumptionValues.Where(oia => oia.Key.SubtotalGroup != OtherItemAssum.ImpairmentsRisk);
            Dictionary<OtherItemAssum, double[]> notImpairementsRiskOIA = new Dictionary<OtherItemAssum, double[]>();
            foreach (KeyValuePair<OtherItemAssum, double[]> otherItemAssumptionValue in otherItemAssumptionValues)
            {
                if (!otherItemAssumptionValue.Key.SubtotalGroup.Equals(OtherItemAssum.ImpairmentsRisk,StringComparison.InvariantCultureIgnoreCase))
                    notImpairementsRiskOIA.Add(otherItemAssumptionValue.Key, otherItemAssumptionValue.Value);
            }           
            return notImpairementsRiskOIA;
        }
        public IDictionary<OtherItemAssum, double[]> GetOtherItemAssumptionsImpairments()
        {
            //return this.otherItemAssumptionValues.Where(oia => oia.Key.SubtotalGroup != OtherItemAssum.ImpairmentsRisk);
            Dictionary<OtherItemAssum, double[]> notImpairementsRiskOIA = new Dictionary<OtherItemAssum, double[]>();
            foreach (KeyValuePair<OtherItemAssum, double[]> otherItemAssumptionValue in otherItemAssumptionValues)
            {
                if (otherItemAssumptionValue.Key.SubtotalGroup.Equals(OtherItemAssum.ImpairmentsRisk, StringComparison.InvariantCultureIgnoreCase))
                    notImpairementsRiskOIA.Add(otherItemAssumptionValue.Key, otherItemAssumptionValue.Value);
            }
            return notImpairementsRiskOIA;
        }


        public IDictionary<Dividend, double[]> GetDividendAmounts()
        {
            return this.dividendValues;
        }

        public double[] GetImpermentData(Category category)
        {
            double[] impairement;

            if (category.Children.Count > 0)
            {
                impairement = new double[this.ResultMonths];
                foreach (Category child in category.Children)
                {
                    double[] values = this.GetIncome(child);
                    for (int i = 0; i < this.ResultMonths; i++) impairement[i] += values[i];
                }
                return impairement;
            }

            return this.impairementData.TryGetValue(category.Id, out impairement)
                       ? impairement
                       : new double[this.ResultMonths];
        }

        public double[] GetNetBankingIncome()
        {
            return this.netBankingIncome;
        }

        public void Clear(bool isMonteCarlo)
        {
            if (isMonteCarlo)
            {
                this.productDatas = null;
                this.categories = null;
            }
            else
            {
                foreach (ProductData productData in this.ProductDatas)
                {
                    productData.Clear();
                }
            }

            this.vectorAndVariableValues = null;
            this.fxRates = null;
            this.lcr.Clear();
            this.solvency.Clear();
            this.discountCurveCollection = null;
            this.solvencyAdjustments = null;
            this.otherItemAssumptions = null;
            this.applicationFeeIndex = -1;
            this.applicationChargeIndex = -1;
            this.prepayementFeeIndex = -1;
            this.assetCashAdjProductData = null;
            this.assetCashAdjProductData = null;
            this.modelImpairmentsProductData = null;
            this.ibnrProductData = null;
            this.liabilitiesCashAdjProductData = null;
            this.otherComprehensiveIncomeProductData = null;
            this.receivableProductDatas.Clear();
            this.payableProductDatas.Clear();
            this.reservesProductData = null;
            this.assetsTreasuryProductDatas.Clear();
            this.liabilitiesTreasuryProductDatas.Clear();
            this.positiveDerivativesValuationProductData = null;
            this.negativeDerivativesValuationProductData = null;
            this.result = null;
        }

        public ProductData[] GetProductDatasByCategory(string parentCategory)
        {
            return this.ProductDatas == null ?
                null :
                //MTU take the sub categories too 
                //this.ProductDatas.Where(p => p.Product.Category.Id == categoryId).ToArray();
                this.ProductDatas.Where(p => p.Product.Category.FullPath().StartsWith(parentCategory)).ToArray();
        }

        public Dictionary<string, double[]> GetDynamicVariables()
        {
            IEnumerable<string> dynamicVariableNames =
                this.dynamicVariableEvaluators.OrderBy(p => p.Value.Priority).Select(ev => ev.Key.ToLowerInvariant());

            return this.namedVariableValues.Where(p => dynamicVariableNames.Contains(p.Key.ToLowerInvariant())).ToDictionary(p => p.Key,
                                                                                                          p => p.Value);
        }


    }

    public class Mouvements
    {
        private readonly List<Tuple<string, double>> items = new List<Tuple<string, double>>();

        public List<Tuple<string, double>> Items
        {
            get { return this.items; }
        }

        public void Add(string description, double amount)
        {
            if (Math.Abs(amount - 0) < 0.01) return;

            this.Items.Add(new Tuple<string, double>(description, amount));
        }

        public void Sort()
        {
            this.Items.Sort((tuple, tuple1) => tuple1.Item2.CompareTo(tuple.Item2));
        }

        public override string ToString()
        {
            return this.Items.Aggregate("", (a, b) => a + b.Item1 + ":\u00A0" + b.Item2.ToString("N0") + Environment.NewLine);
        }
    }

    public class AlmIdProductDataComparer : IEqualityComparer<ProductData>
    {
        #region IEqualityComparer<ProductData> Members

        public bool Equals(ProductData x, ProductData y)
        {
            return (x.Product == null && y.Product == null) ||
                   (x.Product != null && y.Product != null && x.Product.ALMIDDisplayed == y.Product.ALMIDDisplayed);
        }

        public int GetHashCode(ProductData obj)
        {
            return obj.Product.ALMIDDisplayed.GetHashCode();
        }

        #endregion
    }
}