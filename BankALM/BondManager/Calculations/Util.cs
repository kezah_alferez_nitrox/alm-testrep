using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using ALMS.Products;
using ALMSCommon;
using BankALM.Infrastructure.Data;
using BondManager.Core;
using BondManager.Domain;
using BondManager.Resources.Language;
using BondManager.Views;
using Iesi.Collections.Generic;
using NHibernate;
using NHibernate.Linq;

namespace BondManager.Calculations
{
    public class Util
    {
        public const double InfiniteDuration = 999;

        public static int GetMonthsBeetwenDates(DateTime startDate, DateTime date)
        {
            DateTime date0 = startDate;//.AddDays(-1);
            int month = (date.Year * 12 + date.Month - date0.Year * 12 - date0.Month);
            //if (month < 0) month = 0;
            //if (month >= Param.ResultMonths) month = Param.ResultMonths - 1;

            return month;
        }

        public static bool IsNotZero(double value)
        {
            return !IsZero(value);
        }

        public static bool IsZero(double value)
        {
            return Math.Abs(value) < 0.001;
        }

        public static void ComputeVariableValues(Scenario scenario,
                                                  ExpressionParser parser,
                                                  IDictionary<string, object>[] vectorValues,
                                                  IDictionary<string, double>[] variableValues,
                                                  IEnumerable<Variable> variablesToCompute,
                                                  ICollection<ValidationMessage> validationErrors,
                                                  SimulationParams parameters)
        {
            List<VariableValue> scenarioVariableValues = new List<VariableValue>(scenario.VariableValues);

            for (int i = 0; i < parameters.ResultSteps; i++)
            {
                variableValues[i] = new SortedDictionary<string, double>(StringComparer.CurrentCultureIgnoreCase);
            }

            foreach (Variable variable in variablesToCompute)
            {
                Variable variableCopy = variable;
                VariableValue variableValue =
                    scenarioVariableValues.Find(v => v.Variable.Id == variableCopy.Id);

                if (variableValue != null && variableValue.ValueIsArray())
                {
                    double[] arrayValue = variableValue.GetArrayValue();
                    for (int i = 0; i < arrayValue.Length; i++)
                    {
                        variableValues[i][variable.Name] = arrayValue[i];
                    }
                    continue;
                }

                if (variableValue != null && vectorValues[0].ContainsKey(variableValue.Value))
                {
                    for (int i = 0; i < parameters.ResultSteps; i++)
                        variableValues[i][variable.Name] = (double)vectorValues[i][variableValue.Value];
                }
                else
                {
                    for (int i = 0; i < parameters.ResultSteps; i++)
                    {
                        double expressionValue;
                        try
                        {
                            expressionValue = parser.Evaluate(variableValue.Value, vectorValues[i]);
                        }
                        catch (Exception ex)
                        {
                            Log.Warn(ex);
                            if (variableValue == null)
                                validationErrors.Add(
                                    new ValidationMessage(Labels.Util_ComputeVariableValues_The_ + variable.Name +
                                                          Labels.Util_ComputeVariableValues__variable_does_not_have_a_formula_in_ + scenario.Name +
                                                          Labels.Util_ComputeVariableValues__scenario_));
                            else
                                validationErrors.Add(
                                    new ValidationMessage(variableValue.Variable.Name + " : " + ex.Message));
                            break;
                        }
                        variableValues[i][variable.Name] = expressionValue;
                    }
                }
            }
        }

        public static string[] GetLiborVariableNames(string[] variableNames, IFundamentalVector[] fundamentalVectors)
        {
            List<string> liborVariables = new List<string>();
            foreach (string variable in variableNames)
            {
                foreach (IFundamentalVector fundamentalVector in fundamentalVectors)
                {
                    if (fundamentalVector.Type == FundamentalVectorType.Libor && fundamentalVector.VariableName == variable)
                    {
                        liborVariables.Add(variable);
                        break;
                    }
                }
            }
            return liborVariables.ToArray();
        }

        public static void ComputeVectorValues(DateTime valuationDate,
                                                IDictionary<string, object>[] vectorValues,
                                                IEnumerable<Vector> vectorsToCompute, int resultMonths, bool isDaily)
        {
            for (int i = 0; i < resultMonths; i++)
            {
                vectorValues[i] = new SortedDictionary<string, object>(StringComparer.CurrentCultureIgnoreCase);
            }

            foreach (Vector vector in vectorsToCompute)
            {
                List<VectorPoint> points = new List<VectorPoint>(vector.VectorPoints);
                points.Sort(delegate(VectorPoint vp1, VectorPoint vp2) { return vp1.Date.CompareTo(vp2.Date); });

                double[] values = GetVectorValues(valuationDate, points, resultMonths, isDaily);
                for (int i = 0; i < values.Length; i++)
                {
                    vectorValues[i][vector.Name] = values[i];
                }
            }
        }

        public static double[] GetVectorValues(DateTime valuationDate, IList<VectorPoint> points, int resultMonths, bool isDaily)
        {
            //todo : mtu : v�rifier le daily
            double[] values = new double[resultMonths];

            if (points == null || points.Count <= 1)
            {
                return values;
            }

            int monthStart = PricerUtil.DateToInt(valuationDate, isDaily);
            int monthEnd = monthStart + resultMonths;

            FillVectorValuesBeforeStartMonth(points, values, monthStart, isDaily);
            FillVectorValuesBetweenStartAndEndMonths(points, values, monthStart, monthEnd, isDaily);
            FillVectorValesAfterEndMonth(points, values, monthStart, monthEnd, isDaily);

            return values;
        }

        public static void FillVectorValuesBeforeStartMonth(IList<VectorPoint> points, double[] values, int monthStart, bool isDaily)
        {
            int monthPoint0 = PricerUtil.DateToInt(points[0].Date, isDaily);
            if (monthStart < monthPoint0)
            {
                for (int i = monthStart; i < monthPoint0; i++)
                {
                    values[i - monthStart] = points[0].Value;
                }
            }
        }

        public static void FillVectorValuesBetweenStartAndEndMonths(IList<VectorPoint> points, double[] values, int monthStart, int monthEnd, bool isDaily)
        {
            for (int pointIndex = 0; pointIndex < points.Count - 1; pointIndex++)
            {
                int monthPoint1 = PricerUtil.DateToInt(points[pointIndex].Date, isDaily);
                int monthPoint2 = PricerUtil.DateToInt(points[pointIndex + 1].Date, isDaily);

                monthPoint1 = Math.Max(monthStart, monthPoint1);
                monthPoint2 = Math.Min(monthEnd, monthPoint2);

                for (int i = monthPoint1; i < monthPoint2; i++)
                {
                    values[i - monthStart] = points[pointIndex].Value;
                }
            }
        }

        public static void FillVectorValesAfterEndMonth(IList<VectorPoint> points, double[] values, int monthStart, int monthEnd, bool isDaily)
        {
            int monthPointLast = PricerUtil.DateToInt(points[points.Count - 1].Date, isDaily);
            if (monthPointLast <= monthEnd)
            {
                for (int i = monthPointLast; i < monthEnd; i++)
                {
                    values[i - monthStart] = points[points.Count - 1].Value;
                }
            }
        }

        public static void ValidateVariables(
            ICollection<Scenario> selectedScenarios,
            IEnumerable<Variable> variablesToValidate,
            IDictionary<string, Vector> allVectors,
            Iesi.Collections.Generic.ISet<Vector> usedVectors,
            ICollection<ValidationMessage> validationErrors)
        {
            foreach (Variable variable in variablesToValidate)
            {
                foreach (VariableValue value in variable.VariableValues)
                {
                    if (!selectedScenarios.Contains(value.Scenario)) continue;

                    if (String.IsNullOrEmpty(value.Value))
                    {
                        validationErrors.Add(new ValidationMessage(value, null));
                        continue;
                    }

                    if (value.ValueIsArray())
                    {
                        continue;
                    }

                    string[] operands = Tools.GetOperandsFromExpression(value.Value);
                    foreach (string rawOperand in operands)
                    {
                        string operand = rawOperand.Trim();
                        double tempDecimal;
                        if (Double.TryParse(operand, out tempDecimal)) continue;

                        if (allVectors.ContainsKey(operand))
                        {
                            lock (usedVectors)
                            {
                                usedVectors.Add(allVectors[operand]);
                            }
                        }
                        else
                        {
                            validationErrors.Add(new ValidationMessage(value, operand));
                        }
                    }
                }
            }
        }

        public static double GetValueWithFormula(string formula, ISession session, int step=0, bool activateDynamicVariables = true)
        {


            if (String.IsNullOrEmpty(formula)) return 0;

            double? doubleTry = ParsingTools.StringToDouble(formula);
            if (doubleTry != null) return doubleTry.Value;

            List<ValidationMessage> validationErrors = new List<ValidationMessage>();
            DynamicVariable[] dynamicVariables = null;
            if (activateDynamicVariables) dynamicVariables=DynamicVariable.FindAll(session);


            formula = Tools.PreProcessExpression(formula);

            List<string> operands = new List<string>(Tools.GetOperandsFromExpression(formula));
            if (activateDynamicVariables)
            {
                int count = operands.Count;
                for (int i = 0; i < count; i++)
                {
                    DynamicVariable isDynVar =
                        dynamicVariables.FirstOrDefault(d => d.Name.ToLowerInvariant() == operands[i].ToLowerInvariant());
                    if (isDynVar != null)
                    {
                        operands.AddRange(Tools.GetOperandsFromExpression(isDynVar.Formula));
                    }
                }
            }
            SortedDictionary<string, object> values = new SortedDictionary<string, object>(StringComparer.InvariantCultureIgnoreCase);
            ExpressionParser parser = new ExpressionParser();

            // Vectors
            Vector[] vectors = VectorCache.FindNames( operands.ToArray());
            foreach (Vector vector in vectors) values.Add(vector.Name, vector.VectorPoints[step].Value);

            // Variables
            Scenario scenario = Scenario.FindDefault(session);
            if (scenario == null) return 0;
            Variable[] variables = Variable.FindNames(session, operands.ToArray());
            foreach (Variable currentVariable in variables)
            {
                VariableValue variableValue = VariableValue.Find(session, currentVariable, scenario);
                if (variableValue != null)
                {
                    values.Add(variableValue.Variable.Name.ToLowerInvariant(), GetValueWithFormula(variableValue.Value, session, step, activateDynamicVariables));
                }
            }
            IDictionary<string, DynamicVariableEvaluator> dynamicVariableEvaluators = null;
            if (activateDynamicVariables)
            {
                foreach (DynamicVariable dynamicVariable in dynamicVariables)
                {
                    values.Add(dynamicVariable.Name, 0.0);
                }

                dynamicVariableEvaluators =
                    InitializeDynamicVariables(dynamicVariables,
                                                    values.Keys.ToArray(),
                                                    new string[0],
                                                    new int[0],
                                                    validationErrors);


            }
            IDictionary<string, double[]> namedVariableValues = new SortedDictionary<string, double[]>();

            foreach (KeyValuePair<string, object> keyValuePair in values)
            {
                namedVariableValues.Add(keyValuePair.Key, new double[1]{(double)keyValuePair.Value});
            }
            if (activateDynamicVariables)
            {
                EvaluateDynamicVariables(0, null, null, dynamicVariableEvaluators,
                                         null,
                                         namedVariableValues,
                                         new SortedDictionary<string, object>[] {values});
            }
            return parser.Evaluate(formula, values);
        }

        public static void EvaluateDynamicVariables(int step,
                                                    IDictionary<string, ProductData> productDatasByAlmId,
                                                    IDictionary<int, CategoryData> categoryDatasByAlmId,
                                                    IDictionary<string, DynamicVariableEvaluator> dynamicVariableEvaluators,
                                                    IDictionary<string, ResultLine> LineDictionary,
                                                    IDictionary<string, double[]> namedVariableValues,
                                                    IDictionary<string, object>[] vectorAndVariableValues)
        {
            foreach (KeyValuePair<string, DynamicVariableEvaluator> pair in dynamicVariableEvaluators)
            {
                string name = pair.Key;
                DynamicVariableEvaluator evaluator = pair.Value;

                object[] args = new object[evaluator.ArgNames.Length];
                for (int i = 0; i < evaluator.ArgNames.Length; i++)
                {
                    string argName = evaluator.ArgNames[i];
                    if (argName == "t")
                    {
                        args[i] = step;
                    }
                    else if (argName == "products")
                    {
                        args[i] = productDatasByAlmId;
                    }
                    else if (argName == "categories")
                    {
                        args[i] = categoryDatasByAlmId;
                    }
                    else if (argName == "Result")
                    {
                        if (LineDictionary != null) args[i] = LineDictionary;
                    }
                    else
                    {
                        args[i] = namedVariableValues[argName];
                    }
                }

                try
                {
                    double value = evaluator.Evaluate<double>(args);
                    vectorAndVariableValues[step][name] = namedVariableValues[name][step] = value;
                }
                catch (Exception e)
                {
                    throw new ApplicationException("Erreur evaluating dynamic variable " + name + " : " + e.Message, e);
                }
            }
        }

        public static double GetValueWithFormula(string formula, bool activateDynamicVariables=false)
        {
            ISession session = null;
            try
            {
                session = SessionManager.OpenSession();
                return GetValueWithFormula(formula, session,0,activateDynamicVariables);
            }
            catch (Exception ex)
            {
                Log.Exception("GetValueWithFormula", ex);
                return 0;
            }
            finally
            {
                if (session != null) session.Dispose();
            }
        }
        public static void SetOrigFaceAndBookPriceForCashflow(Product product)
        {
            string amortFormula=product.Amortization;
            string completement = product.ComplementString;
            int duration = product.IntDuration;
            ISession session = null;
            double currentFace = 0;
            double[] reserveCashflows = null;
            double[] reserve_actualization_rate = null;
            try
            {
                session = SessionManager.OpenSession();
                
                int resultSteps = Param.GetMaxDuration(session);
                reserveCashflows = new double[resultSteps];
                reserve_actualization_rate = new double[resultSteps];

                for (int i = 0; i < Math.Min(resultSteps,duration); i++)
                {
                    if (product.AmortizationType==ProductAmortizationType.RESERVE_CASHFLOW)
                        reserveCashflows[i]=GetValueWithFormula(amortFormula, session, i,false);
                    else
                    {
                        if (i<resultSteps-1)
                            reserveCashflows[i] = GetValueWithFormula(amortFormula, session, i, false)-GetValueWithFormula(amortFormula, session, i+1, false);
                        else
                            reserveCashflows[i] = 0;
                    }
                    currentFace += reserveCashflows[i];
                    reserve_actualization_rate[i] = GetValueWithFormula(completement, session, i, false);
                }
            }
            catch (Exception ex)
            {
                Log.Exception("SetOrigFaceAndBookPriceForCashflow", ex);
                return;
            }
            finally
            {
                if (session != null) session.Dispose();
            }
            product.OrigFace= currentFace.ToString();
            double bookValue = GetBookValueForCashflow(0, duration, reserveCashflows, reserve_actualization_rate);
            product.BookValue = bookValue;
            if (currentFace != 0)
                product.BookPrice = (100*bookValue/currentFace).ToString();

        }

        public static double GetBookValueForCashflow(int start, int duration, double[] reserveCashflows, double[] reserve_actualization_rate)
        {
            double bookValue=0;

            for (int i = start; i < duration; i++)
            {
                bookValue += reserveCashflows[i] / Math.Pow(1 + reserve_actualization_rate[i], i-start + 1);
            }

            return bookValue;
        }

        public static IDictionary<string, T> CastDictionary<T>(IDictionary<string, object> dictionary)
        {
            SortedDictionary<string, T> newDictionary = new SortedDictionary<string, T>();
            foreach (KeyValuePair<string, object> pair in dictionary) newDictionary.Add(pair.Key, (T)pair.Value);
            return newDictionary;
        }

        public static IDictionary<string, T>[] CastDictionaries<T>(IDictionary<string, object>[] dictionaries)
        {
            SortedDictionary<string, T>[] newDictionaries = new SortedDictionary<string, T>[dictionaries.Length];
            for (int i = 0; i < newDictionaries.Length; i++)
            {
                SortedDictionary<string, T> newDictionary = newDictionaries[i];
                newDictionaries[i] = newDictionary;
            }
            return newDictionaries;
        }

        public static void GetVariableValues(IDictionary<string, object>[] dictionaries, IDictionary<string, double[]> namedVariableValues)
        {
            for (int i = 0; i < dictionaries.Length; i++)
            {
                IDictionary<string, object> dictionary = dictionaries[i];
                foreach (KeyValuePair<string, object> pair in dictionary)
                {
                    if (!namedVariableValues.ContainsKey(pair.Key.ToLowerInvariant()))
                    {
                        namedVariableValues.Add(pair.Key.ToLowerInvariant(), new double[dictionaries.Length]);
                    }
                    namedVariableValues[pair.Key.ToLowerInvariant()][i] = (double)pair.Value;
                }
            }
        }

        public static string[] GetDictionariesKeys(IDictionary<string, object>[] dictionaries)
        {
            List<string> keys = new List<string>();
            foreach (IDictionary<string, object> dictionary in dictionaries)
            {
                foreach (KeyValuePair<string, object> pair in dictionary)
                {
                    if (!keys.Contains(pair.Key)) keys.Add(pair.Key);
                }
            }
            return keys.ToArray();
        }

        public static void InitVariableAndVectorValues(IDictionary<string, object>[] vectorAndVariableValues, IDictionary<string, object>[] vectorValues, IDictionary<string, double>[] variableValues, DynamicVariable[] dynamicVariables, IDictionary<string, double[]> namedVariableValues, SimulationParams parameters)
        {
            for (int i = 0; i < parameters.ResultSteps; i++)
            {
                vectorAndVariableValues[i] = new SortedDictionary<string, object>(StringComparer.CurrentCultureIgnoreCase);

                foreach (KeyValuePair<string, object> vectorValue in vectorValues[i])
                {
                    vectorAndVariableValues[i][vectorValue.Key.ToLowerInvariant()] = vectorValue.Value;
                }

                foreach (KeyValuePair<string, double> variableValue in variableValues[i])
                {
                    vectorAndVariableValues[i][variableValue.Key.ToLowerInvariant()] = variableValue.Value;
                }

                if (dynamicVariables != null)
                {
                    foreach (DynamicVariable dynamicVariable in dynamicVariables)
                    {
                        vectorAndVariableValues[i][dynamicVariable.Name.ToLowerInvariant()] = 0.0;
                    }
                }
            }
            GetVariableValues(vectorAndVariableValues, namedVariableValues);
        }

        public static List<ValidationMessage> UpdateProductBookPrice(Product product)
        {
            if (product.Id <= 0) return null;

            List<ValidationMessage> validationMessages = null;

            using (ISession session = SessionManager.OpenSession())
            {
                Product product2 = ProductCache.FindById(product.Id);//EntityBase<Product, int>.Find(session, product.Id);
                if (product2 == null) return null;

                // Update of Accounting if modification
                // Dirty patching due to the structure of the code.
                if (product.Accounting != product2.Accounting)
                    product2.Accounting = product.Accounting;

                SimulationParams parameters = GetSimulationParams(session);

                ProductAnalysisData data = new ProductAnalysisData(session, parameters, product2);
                Scenario scenario = Scenario.FindDefault(session);
                if (scenario != null)
                {
                    validationMessages = data.ComputeBookPrice(scenario);
                }

                product.BookPrice = product2.BookPrice;
                session.Flush();
            }

            product.BookPriceValue = ParsingTools.StringToDouble(product.BookPrice).GetValueOrDefault();
            product.ComputeBookValue();

            return validationMessages;
        }

        public static SimulationParams GetSimulationParams(ISession session)
        {
            SimulationParams parameters = new SimulationParams()
                                              {
                                                  DividendPaymentMonth = Param.GetDividendPaymentMonth(session),
                                                  MaxDuration = Param.GetMaxDuration(session),
                                                  ResultSteps = Param.ResultMonths(session),
                                                  RollEnabled = Param.GetRollEnabled(session)
                                              };
            return parameters;
        }

        public static SortedDictionary<string, DynamicVariableEvaluator> InitializeDynamicVariables(DynamicVariable[] dynamicVariables, string[] variableNames, string[] productALMIds, int[] categoryAlmIds, List<ValidationMessage> validationErrors)
        {
            SortedDictionary<string, DynamicVariableEvaluator> result = new SortedDictionary<string, DynamicVariableEvaluator>();

            variableNames = variableNames.Concat(dynamicVariables.Select(dv => dv.Name)).ToArray();

            foreach (DynamicVariable dynamicVariable in dynamicVariables)
            {
                try
                {
                    string[] usedVariableNames = variableNames.Where(v => dynamicVariable.Formula != null && dynamicVariable.Formula.Contains(v)).Distinct().ToArray(); // todo : use something better than contains
                    string[] argNames = new[] { "t", "products", "categories", "Result" }.Concat(usedVariableNames).ToArray();

                    result[dynamicVariable.Name] = new DynamicVariableEvaluator(dynamicVariable.Formula, argNames, productALMIds, categoryAlmIds, dynamicVariable.Position);
                }
                catch (Exception ex)
                {
                    validationErrors.Add(new ValidationMessage(String.Format(Labels.Util_InitializeDynamicVariables_Error_in_dynamic_variable__0_____1_, dynamicVariable.Name, ex.Message)));
                }
            }

            return result;
        }

        public static void UpdateBookValueForDynamicVariables(BindingList<DynamicVariable> variables, ISession session)
        {
            IEnumerable<Product> products = ProductCache.products.FindAll(//session.Query<Product>().Where(
                p =>
                p.AmortizationType == ProductAmortizationType.VAR_NOMINAL && p.Amortization != null &&
                p.Amortization != "");
            foreach (Product prod in products)
            {
                if (variables.Any(v => prod.Amortization.IndexOf(v.Name, StringComparison.InvariantCultureIgnoreCase) >= 0))
                {
                    double valueWithFormula = GetValueWithFormula(prod.Amortization);
                    prod.OrigFace = valueWithFormula.ToString();
                    UpdateProductBookPrice(prod);
                    prod.ComputeBookValue();
                    prod.UpdateStatusProperty();
                    session.Save(prod);
                }
            }
            session.Flush();
           
        }
        public static double GetComputedValue(string value, IDictionary<string, object> variables, ExpressionParser parser)
        {
            double tempDecimal;
            if (!Double.TryParse(value, out tempDecimal))
            {
                tempDecimal = ComputeExpression(value, variables,parser);
            }
            return tempDecimal;
        }

        public static double ComputeExpression(string expression, IDictionary<string, object> variables, ExpressionParser parser)
        {
            double expressionValue = 0;
            try
            {
                expressionValue = parser.Evaluate(expression, variables);
            }
            catch (Exception ex)
            {
                Log.Warn(ex);
            }

            return expressionValue;
        }


        public static readonly CultureInfo DefaultCultureInfo = new CultureInfo("en-US");

        public static void setDefaultCultureInfoToThread()
        {
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
        }
        public static void SendMail(string address, string subject, string body, string fileName = null)
        {
            try
            {
                Mapi mapi = new Mapi();
                if (fileName != null) mapi.AddAttachment(fileName);
                mapi.AddRecipientTo(address);
                mapi.SendMailPopup(subject, body);
                
            }
            catch (Exception ex)
            {
                Logger.Exception("MAPI Exception", ex);
                string toLaunch;
                if (fileName != null)
                    toLaunch=String.Format("mailto:{0}?subject={1}&body={2}&Attach={3}", address, subject, body, fileName);
                else
                    toLaunch=String.Format("mailto:{0}?subject={1}&body={2}", address, subject, body);
                
                Process.Start(toLaunch);
            }
        }

        public static DialogResult InputBox(string title, string promptText, ref string value)
        {
            Form form = new Form();
            Label label = new Label();
            TextBox textBox = new TextBox();
            Button buttonOk = new Button();
            Button buttonCancel = new Button();

            form.Text = title;
            label.Text = promptText;
            textBox.Text = value;

            buttonOk.Text = "OK";
            buttonCancel.Text = "Cancel";
            buttonOk.DialogResult = DialogResult.OK;
            buttonCancel.DialogResult = DialogResult.Cancel;

            label.SetBounds(9, 20, 372, 13);
            textBox.SetBounds(12, 36, 372, 20);
            buttonOk.SetBounds(228, 72, 75, 23);
            buttonCancel.SetBounds(309, 72, 75, 23);

            label.AutoSize = true;
            textBox.Anchor = textBox.Anchor | AnchorStyles.Right;
            buttonOk.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            buttonCancel.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;

            form.ClientSize = new Size(396, 107);
            form.Controls.AddRange(new Control[] { label, textBox, buttonOk, buttonCancel });
            form.ClientSize = new Size(Math.Max(300, label.Right + 10), form.ClientSize.Height);
            form.FormBorderStyle = FormBorderStyle.FixedDialog;
            form.StartPosition = FormStartPosition.CenterScreen;
            form.MinimizeBox = false;
            form.MaximizeBox = false;
            form.AcceptButton = buttonOk;
            form.CancelButton = buttonCancel;

            DialogResult dialogResult = form.ShowDialog();
            value = textBox.Text;
            return dialogResult;
        }

        public static DateTime LastDayOfMonth(DateTime date)
        {
            date = date.AddMonths(1);
            date = date.AddDays(-date.Day);

            return date;
        }
    }
}