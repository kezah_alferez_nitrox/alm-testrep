using System;
using System.Collections.Generic;
using BondManager.Core;
using BondManager.Resources.Language;
using NCalc;

namespace BondManager.Calculations
{
    public class ExpressionParser
    {
        private static object Semaphor = new object();


        public double Evaluate(string formula, IDictionary<string, object> parameters)
        {
            if (string.IsNullOrEmpty(formula)) return 0;

            double value;
            if (double.TryParse(formula, out value)) return value;

            try
            {
                formula = formula.Replace("(0,", "(0.0,");
                formula = formula.Replace(",0)", ",0.0)");
                formula = formula.Replace(",0,", ",0.0,");

                formula = Tools.PreProcessExpression(formula);

                object result;
                // using http://ncalc.codeplex.com/
                //lock(Semaphor)
                //{
                    Expression expression = new Expression(formula.ToLowerInvariant(), EvaluateOptions.IgnoreCase);

                    expression.Parameters = parameters; //new Dictionary<string, object>(parameters, StringComparer.CurrentCultureIgnoreCase);
                    result = expression.Evaluate();
                //}
                double doubleResult = Convert.ToDouble(result);

                return doubleResult;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException(string.Format(Labels.ExpressionParser_Evaluate_, formula, ex.Message), ex);
            }
        }
    }
}