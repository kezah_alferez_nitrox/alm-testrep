using System;
using System.Collections.Generic;
using ALMS.Products;
using BondManager.Domain;
using NHibernate;

namespace BondManager.Calculations
{
    public class Solvency
    {
        private IList<ProductData> productDatas;
        private IDictionary<string, double[]> adjustments = new SortedDictionary<string, double[]>();

        private readonly double[] rwa;
        private readonly double[] coreEquity;
        private readonly double[] tiers1;
        private readonly double[] regulatoryCapital;
        private readonly double[] solvencyRatioOnCoreEquity;
        private readonly double[] solvencyRatioTiers1;
        private readonly double[] solvencyRatio;

        public double[] Rwa
        {
            get { return this.rwa; }
        }

        public double[] CoreEquity
        {
            get { return this.coreEquity; }
        }

        public double[] Tiers1
        {
            get { return this.tiers1; }
        }

        public double[] RegulatoryCapital
        {
            get { return this.regulatoryCapital; }
        }

        public double[] SolvencyRatioOnCoreEquity
        {
            get { return this.solvencyRatioOnCoreEquity; }
        }

        public double[] SolvencyRatioTiers1
        {
            get { return this.solvencyRatioTiers1; }
        }

        public double[] SolvencyRatio
        {
            get { return this.solvencyRatio; }
        }

        private int resultMonths;

        public Solvency(IList<ProductData> ProductDatas, IDictionary<string, double[]> adjustments, SimulationParams parameters)
        {
            this.productDatas = ProductDatas;
            this.adjustments = adjustments;

            this.resultMonths = parameters.ResultSteps;
            rwa = new double[this.resultMonths];
            coreEquity = new double[this.resultMonths];
            tiers1 = new double[this.resultMonths];
            regulatoryCapital = new double[this.resultMonths];
            solvencyRatioOnCoreEquity = new double[this.resultMonths];
            solvencyRatioTiers1 = new double[this.resultMonths];
            solvencyRatio = new double[this.resultMonths];
        }

        private double[] GetAdjustment(string key)
        {
            lock (typeof(Solvency))
            {
                if (!this.adjustments.ContainsKey(key))
                {
                    this.adjustments.Add(key, new double[this.resultMonths]);
                }
            }
            return this.adjustments[key];
        }

        public void Compute(int step)
        {
            double[] rwaAdjustment = this.GetAdjustment(SolvencyAdjustment.Types.RWAAdjustment);
            double[] coreEquityAdjustment = this.GetAdjustment(SolvencyAdjustment.Types.CoreEquityAdjustment);
            double[] tier1Adjustment = this.GetAdjustment(SolvencyAdjustment.Types.OtherTierOneAdjustment);
            double[] tiers2Adjustment = this.GetAdjustment(SolvencyAdjustment.Types.Tiers2Adjustment);

            foreach (ProductData ProductData in this.productDatas)
            {
                if (ProductData.Product.Exclude) continue;

                double currFace = ProductData.CurrFace[step];
                if (ProductData.Product.Category.BalanceType == BalanceType.Asset)
                {
                    double basel2 = ProductData.Product.Basel2Value;

                    if (ProductData.Product.ProductType == ProductType.StockEquity)
                    {
                        this.Rwa[step] += ProductData.GetBookValueInValuationCurrency(step) * basel2;
                    }
                    else
                    {
                        this.Rwa[step] += currFace * basel2;
                    }
                }
                else if (ProductData.Product.Category.BalanceType == BalanceType.Equity)
                {
                    if (ProductData.Product.Basel2 == Product.Basel2Types.CoreEquity)
                    {
                        this.CoreEquity[step] += currFace;
                    }
                    if (ProductData.Product.Basel2 == Product.Basel2Types.CoreEquity ||
                        ProductData.Product.Basel2 == Product.Basel2Types.OtherTiers1)
                    {
                        this.Tiers1[step] += currFace;
                    }

                    if (ProductData.Product.Basel2 == Product.Basel2Types.CoreEquity ||
                        ProductData.Product.Basel2 == Product.Basel2Types.OtherTiers1 ||
                        ProductData.Product.Basel2 == Product.Basel2Types.Tiers2)
                    {
                        this.RegulatoryCapital[step] += currFace;
                    }
                }
            }

            this.Rwa[step] += rwaAdjustment[step];
            this.CoreEquity[step] += coreEquityAdjustment[step];
            this.Tiers1[step] += coreEquityAdjustment[step] + tier1Adjustment[step];
            this.RegulatoryCapital[step] += coreEquityAdjustment[step] + tier1Adjustment[step] + tiers2Adjustment[step];

            if (Util.IsZero(this.Rwa[step])) return;

            this.SolvencyRatioOnCoreEquity[step] = this.CoreEquity[step] / this.Rwa[step];
            this.SolvencyRatioTiers1[step] = this.Tiers1[step] / this.Rwa[step];
            this.SolvencyRatio[step] = this.RegulatoryCapital[step] / this.Rwa[step];
        }

        public void Clear()
        {
            this.productDatas = null;
            this.adjustments = null;
        }
    }
}