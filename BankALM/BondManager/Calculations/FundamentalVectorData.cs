using System;
using System.Collections.Generic;
using ALMS.Products;
using BondManager.Domain;
using Iesi.Collections.Generic;
using NHibernate;

namespace BondManager.Calculations
{
    public class FundamentalVectorData : IFundamentalVector
    {
        private readonly FundamentalVector fundamentalVector;
        private readonly ExpressionParser parser;
        private readonly int months;
        private readonly List<ValidationMessage> validationErrors = new List<ValidationMessage>();

        private readonly double[] volatilityValues;
        public double[] VolatilityValues
        {
            get { return this.volatilityValues; }
        }

        public FundamentalVectorData(FundamentalVector fundamentalVector, ExpressionParser parser, int months)
        {
            this.fundamentalVector = fundamentalVector;
            this.parser = parser;
            this.months = months;
            volatilityValues = new double[months];
        }

        public void Validate(IDictionary<string, Vector> allVectors, IDictionary<string, Variable> allVariables, Iesi.Collections.Generic.ISet<Vector> usedVectors, Iesi.Collections.Generic.ISet<Variable> usedVariables)
        {
            Variable variable = this.fundamentalVector.Variable;
            if(variable == null || string.IsNullOrEmpty(variable.Name))
            {
                this.ValidationErrors.Add(new ValidationMessage(this.fundamentalVector, "Variable", null));
                return;
            }

            usedVariables.Add(variable);
            this.ValidateFormula("Volatility", this.fundamentalVector.Volatility, allVectors, allVariables, usedVectors, usedVariables);
        }

        private void ValidateFormula(string property, string value,
                                 IDictionary<string, Vector> allVectors, IDictionary<string, Variable> allVariables, 
                                 Iesi.Collections.Generic.ISet<Vector> usedVectors, Iesi.Collections.Generic.ISet<Variable> usedVariables)
        {
            if (Tools.IsDecimalString(value))
            {
                return;
            }

            string[] operands = Tools.GetOperandsFromExpression(value);
            foreach (string operand in operands)
            {
                if (Tools.IsDecimalString(operand))
                {
                    continue;
                }
                if (allVectors.ContainsKey(operand))
                {
                    usedVectors.Add(allVectors[operand]);
                    continue;
                }
                if (allVariables.ContainsKey(operand))
                {
                    usedVariables.Add(allVariables[operand]);
                    continue;
                }

                this.ValidationErrors.Add(new ValidationMessage(this.fundamentalVector, property, operand));
            }
        }

        public virtual void Valuate(IDictionary<string, object>[] variablesValues)
        {
            for (int i = 0; i < months; i++)
            {
                this.volatilityValues[i] = (double) this.parser.Evaluate(this.fundamentalVector.Volatility, variablesValues[i]);

                if (this.ValidationErrors.Count > 0)
                {
                    return;
                }
            }
        }

        #region Implementation of IFundamentalVector

        public string VariableName
        {
            get { return this.fundamentalVector.VariableName; }
        }

        public TimeUnit Unit
        {
            get { return this.fundamentalVector.Unit; }
        }

        public int Delta
        {
            get { return this.fundamentalVector.Delta; }
        }

        public CouponBasis DateConvention
        {
            get { return this.fundamentalVector.DateConvention; }
        }

        public FundamentalVectorType Type
        {
            get { return this.fundamentalVector.Type; }
        }

        public List<ValidationMessage> ValidationErrors
        {
            get { return this.validationErrors; }
        }

        public ProductFrequency Frequency
        {
            get { return this.fundamentalVector.Frequency; }
        }

        
        #endregion
    }
}