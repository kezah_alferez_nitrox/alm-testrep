﻿using BondManager.Domain;

namespace BondManager.Calculations
{
    public class SimulationParams
    {
        public bool RollEnabled { get; set; }
        public int MaxDuration { get; set; }
        public int ResultSteps { get; set; }
        public int StopAfterResultMonths { get; set; }
        public int DividendPaymentMonth { get; set; }
        public Currency ValuationCurrency { get; set; }
        public bool IsComputationDaily { get; set; }
        public bool ComputeGap { get; set; }
        public int CashflowPresentation { get; set; }
        public string GapFixedRate { get; set; }
        public int GapType { get; set; }
        public bool OriginForModel { get; set; }
    }
}