using System;
using System.Collections.Generic;
using ALMS.Products;
using BankALM.Infrastructure.Data;
using BondManager.Domain;
using NHibernate;

namespace BondManager.Calculations
{
    public class FormulaEvaluator : IFormulaEvaluator
    {
        private readonly ExpressionParser parser;
        private readonly DateTime valuationDate;
        private readonly IDictionary<string, object>[] variableValues;
        private readonly IDictionary<string, double?[]> existingExpressions = new SortedDictionary<string, double?[]>();

        public FormulaEvaluator(ExpressionParser parser, DateTime valuationDate, IDictionary<string, object>[] variableValues)
        {
            this.parser = parser;
            this.valuationDate = valuationDate;
            this.variableValues = variableValues;
        }

        public double Evaluate(string formula, DateTime date)
        {
            using (ISession session = SessionManager.OpenSession())
            {
                if (string.IsNullOrEmpty(formula)) formula = "0";

                double value;

                int month = Util.GetMonthsBeetwenDates(this.valuationDate, date);
                if (month < 0) 
                    month = 0;

#if !DONT_CACHE_RESULTS
                double?[] formulaValues;
                if (!existingExpressions.TryGetValue(formula, out formulaValues))
                {
                    formulaValues = new double?[Param.ResultMonths(session)];
                    existingExpressions[formula] = formulaValues;
                }

                if (formulaValues[month].HasValue)
                {
                    value = formulaValues[month].Value;
                }
                else
                {
#endif
                    value = (double) this.parser.Evaluate(formula, this.variableValues[month]);
#if !DONT_CACHE_RESULTS
                    formulaValues[month] = value;
                }
#endif
                session.Flush();
                return value;
            }
        }
    }
}