﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BondManager.Calculations
{
    public enum Tenor
    {
        Week,
        Month,
        Quarter,
        Semester,
        Year,
        Day
    }
}
