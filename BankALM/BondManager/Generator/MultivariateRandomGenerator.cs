using System;
using BondManager.Domain;

namespace BondManager.Generator
{
    public class MultivariateRandomGenerator
    {
        private readonly INormalRandom random;

        public double[,] Result { get; private set; }
        public bool IsValid { get; private set; }

        public MultivariateRandomGenerator()
            : this(new BoxMuller(new MersenneTwister())) { }

        public MultivariateRandomGenerator(INormalRandom random)
        {
            this.random = random;
            this.IsValid = false;
        }

        public void GenerateMultivariateNormals(int size, double dt, double[][] means, double[] vol, double[,] correlations, VolatilityConfig.SimulationModes[] simulationModes, bool[] isLog, double[] halfLives)
        {
            int variableCount = means.Length;
            double dts = Math.Sqrt(dt);
            double[,] cholesky = MatrixHelper.Cholesky(correlations);

            double[,] result = new double[variableCount, size];

            for (int index = 0; index < size; index++)
            {
                // generate standard normals
                double[] z = this.GenerateStandardNormalsVector(variableCount);

                // tranform to correlated standard normals
                for (int variableIndex = 0; variableIndex < variableCount; variableIndex++)
                {
                    double ss = 0;
                    for (int j = 0; j < variableCount; j++)
                    {
                        ss = ss + z[j] * cholesky[variableIndex, j];
                    }

                    VolatilityConfig.SimulationModes simulationMode = simulationModes[variableIndex];

                    double xt = 0;
                    if (index > 0) xt = result[variableIndex, index - 1];

                    double sigma = vol[variableIndex] * dts;
                    double xt1;
                    if (index == 0)
                    {
                        //TODO : prendre la valeur de d�part de la variable donn�e par ce sc�nario
                        xt1 = means[variableIndex][0];
                    }
                    else
                    {
                        double mu;
                        switch (simulationMode)
                        {
                            case VolatilityConfig.SimulationModes.MeanReversion:
                                //TODO : varibleCount doit etre remplac� par half-life
                                double k = Math.Log(2) / halfLives[variableIndex];
                                if (isLog[variableIndex]) mu = k * (Math.Log(means[variableIndex][index]) - Math.Log(xt)) * dt;
                                else mu = k * (means[variableIndex][index] - xt) * dt;
                                break;
                            case VolatilityConfig.SimulationModes.Mean:
                                if (isLog[variableIndex]) mu = Math.Log(means[variableIndex][index]) - Math.Log(means[variableIndex][index - 1]);
                                else mu = means[variableIndex][index] - means[variableIndex][index - 1];
                                break;
                            case VolatilityConfig.SimulationModes.Trend:
                                mu = means[variableIndex][index] * dt;
                                break;
                            default:
                                throw new InvalidOperationException();
                        }
                        if (!isLog[variableIndex]) xt1 = xt + mu + ss * sigma;
                        else xt1 = xt * Math.Exp(mu + ss * sigma - sigma * sigma / 2);
                    }
                    result[variableIndex, index] = xt1;

                }
            }

            this.Result = result;

            this.IsValid = !MatrixHelper.ContainsNaN(result);
        }

        private double[] GenerateStandardNormalsVector(int n)
        {
            double[] result = new double[n];
            for (int i = 0; i < n; i++)
            {
                result[i] = this.random.Next();
            }
            return result;
        }
    }
}