namespace BondManager.Generator
{
    /// <summary>
    /// Uniformly-distributed random number generator
    /// </summary>
    public interface IUniformRandom
    {
        double NextDouble();
        int NextInt32();
    }
}