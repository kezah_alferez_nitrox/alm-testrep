using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using ALMSCommon;
using BankALM.Infrastructure.Data;
using BondManager.BusinessLogic;
using BondManager.Domain;
using BondManager.Domain.Mapping;
using BondManager.Properties;
using log4net.Core;
using NHibernate;
using NHibernate.Linq;
using log4net.Config;

namespace BondManager
{
    public class Initialiser
    {
        public static void Initialise()
        {
            XmlConfigurator.Configure();

            Thread.CurrentThread.CurrentCulture = new CultureInfo(Constants.CULTURE_NAME);
            

            SessionManager.Configure(typeof(CategoryMap).Assembly, GetDataBaseFilePath(), InitializeData, false);

            InitializeData();
#if !DEBUG
            DeleteDataBaseFileIfOlderThanThisAssembly();
#endif

            BalanceBL.EnsureSpecialCategoriesAndBondsExist();
            BalanceBL.EnsureRollCategoryHasDefaultSubCategory();
            SolvencyAdjustment.EnsureExist();

            FixInfinityBookValue();

            //Test();
        }

        private static void FixInfinityBookValue()
        {
            using (ISession session = SessionManager.OpenSession())
            {
                Product[] products = ProductCache.products.//session.Query<Product>().
                    Where(p => p.BookValue == double.NegativeInfinity || p.BookValue == double.PositiveInfinity).
                    ToArray();

                foreach (Product product in products)
                {
                    Logger.Warn("Product[" + product.ALMIDDisplayed + "].BookValue = +/-Infinity");
                    product.BookPrice = "100";
                    product.BookValue = null;
                }

                session.Flush();
            }
        }

        private static void Test()
        {
            Product p1;
            Product p2;
            Product p3;
            using (ISession session = SessionManager.OpenSession())
            {
                p1 = new Product { Description = "1" };
                p2 = new Product { Description = "2" };
                p3 = new Product { Description = "3" };

                p1.LinkedProducts.Add(p2);
                p1.LinkedProducts.Add(p3);
                p2.LinkedProducts.Add(p1);
                p2.LinkedProducts.Add(p3);
                p3.LinkedProducts.Add(p1);
                p3.LinkedProducts.Add(p2);

                session.Save(p1);
                session.Save(p2);
                session.Save(p3);
                session.Flush();
            }

            using (ISession session = SessionManager.OpenSession())
            {
                Product pp1 = Product.Find(session, p1.Id);
                int count = pp1.LinkedProducts.Count;
            }

            //ThreadPool.QueueUserWorkItem(o =>
            //                                 {
            //                                     Stack<ISession> sessions = new Stack<ISession>();
            //                                     for (int i = 0; i < 300000; i++)
            //                                     {
            //                                         Debug.WriteLine("Query " + i);
            //                                         try
            //                                         {
            //                                             ISession session = SessionManager.OpenSession();
            //                                             sessions.Push(session);
            //                                             Category category = session.Query<Category>().FirstOrDefault();
            //                                             if(category == null) continue;

            //                                             string label = category.Label;
            //                                             category.Label = label + "!";
            //                                             session.Flush();
            //                                             category.Label = label;
            //                                             session.Flush();
            //                                         }
            //                                         catch (Exception exception)
            //                                         {
            //                                             MessageBox.Show(exception.ToString());
            //                                         }

            //                                     }
            //                                 });

            //ThreadPool.QueueUserWorkItem(o =>
            //                                 {
            //                                     for (int i = 0; i < 3000; i++)
            //                                     {
            //                                         Debug.WriteLine("ReadAllBytes " + i);
            //                                         string path = GetDataBaseFilePath();
            //                                         byte[] bytes = File.ReadAllBytes(path);
            //                                     }
            //                                 });
        }

        private static void DeleteDataBaseFileIfOlderThanThisAssembly()
        {
            string dataBaseFileName = GetDataBaseFilePath();
            DateTime dataBaseTime = new FileInfo(dataBaseFileName).LastWriteTime;

            // gets the datetime of the BankALM.exe file (probably the module run by the launcher)
            // so this will work with an installation in the ProgramFile directory
            string assemblyLocation = typeof(Initialiser).Assembly.Location;
            DateTime assemblyTime = new FileInfo(assemblyLocation).LastWriteTime;
            
            if (dataBaseTime < assemblyTime)
            {
                SessionManager.DeleteDataBase();
            }
        }

        private static void InitializeData()
        {
            using (ISession session = SessionManager.OpenSession())
            {
                Currency eur;
                if (session.Query<Currency>().Count() == 0)
                {
                    new Currency { Symbol = null, ShortName = "" }.Create(session);
                    eur = new Currency { Id = 2, Symbol = "�", ShortName = "EUR" };
                    eur.Create(session);

                    //new Currency { Symbol = "$", ShortName = "USD" }.Create(session);
                    //new Currency { Symbol = "�", ShortName = "GBP" }.Create(session);
                    //new Currency { Symbol = "Y", ShortName = "JPY" }.Create(session);

                    session.Flush();
                }
                else
                {
                    eur = Currency.FindFirstBySymbol(session, "�");
                    if (eur == null)
                    {
                        eur = new Currency { Id = 2, Symbol = "�", ShortName = "EUR" };
                        eur.Create(session);
                        session.Flush();
                    }
                }


                EnsureParamExists(session, Param.STANDARD_TAX, "");
                EnsureParamExists(session, Param.LONG_TERM_TAX, "");
                EnsureParamExists(session, Param.SPECIAL_TAX, "");
                EnsureParamExists(session, Param.VALUATION_CURRENCY, eur.Id.ToString());
                EnsureParamExists(session, Param.FILE_NAME, "New ALW");
                EnsureParamExists(session, Param.VALUATION_DATE,
                                  new DateTime(DateTime.Today.Year, 1, 1).ToString(Param.DATE_FORMAT));

                session.Flush();
            }
        }

        private static void EnsureParamExists(ISession session, string paramName, string paramValue)
        {
            if (Param.Find(session, paramName) != null) return;

            new Param { Id = paramName, Val = paramValue }.Create(session);
        }

        public static void NewDatabase()
        {
            SessionManager.DeleteDataBase();
            SessionManager.EnsureDataBaseExists();
        }

        public static string GetDataBaseFilePath()
        {
            // will save DB files into c:\users\[username]\AppData\Roaming\ALM_Solution\
            // this is clean
            // Modification, it will be saved into  c:\users\[username]\AppData\Local\ALM_Solution\
            // For the BETA, like that BETA and prod can be run in parallel.

            // test between BETA and PROD
            String fileLoc = typeof(CategoryMap).Assembly.CodeBase;
            Boolean isBeta = fileLoc.Contains("BETA");

            string appName = "ALM Solutions";
            string dbPath;
            if (isBeta)
                dbPath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
            else
                dbPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);

            string directoryName = ""+dbPath+"\\"+appName ?? "";
            // checks that directory exist, if not, create it.
            bool exists = System.IO.Directory.Exists(directoryName);
            if (!exists)
                System.IO.Directory.CreateDirectory(directoryName);
           
            string path = Path.Combine(directoryName, Settings.Default.DatabaseFile);
            return path;
        }
    }
}