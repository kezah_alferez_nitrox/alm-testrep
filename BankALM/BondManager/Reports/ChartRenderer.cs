﻿using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using ZedGraph;

namespace BondManager.Reports
{
    public class ChartRenderer
    {
        public static Color[] colors = new []
                                           {
                                               Color.Blue,
                                               Color.MediumOrchid,
                                               Color.MediumSeaGreen,                                               
                                               Color.LightSeaGreen,
                                               Color.OliveDrab,
                                               Color.OrangeRed,
                                               Color.Goldenrod,
                                               Color.MediumVioletRed,
                                               Color.MediumPurple,
                                           };
        public string Render(string[] xLabels, string[] yLabels, double[][] values)
        {
            string fileName = Path.GetTempFileName();

            using (ZedGraphControl zedGraph = new ZedGraphControl())
            {
                zedGraph.GraphPane.Title.IsVisible = false;
                zedGraph.GraphPane.XAxis.Title.IsVisible = false;
                zedGraph.GraphPane.YAxis.Title.IsVisible = false;
                zedGraph.GraphPane.CurveList.Clear();
                for (int i = 0; i < values.Length; i++)
                {
                    Color color = colors[i % colors.Length];
                    this.AddCurve(zedGraph, yLabels[i], values[i], color);
                }
                zedGraph.AxisChange();
                zedGraph.Invalidate();

                zedGraph.GraphPane.GetImage(800, 600, 150, true).Save(fileName, ImageFormat.Jpeg);
            }

            return fileName;
        }

        private void AddCurve(ZedGraphControl zedGraphControl, string label, double[] data, Color color)
        {
            GraphPane zedPane = zedGraphControl.GraphPane;

            PointPairList list = new PointPairList();
            for (int i = 0; i < data.Length; i++)
            {
                double x = i;
                double y = data[i];
                list.Add(x, y);
            }

            LineItem graphCurve = zedPane.AddCurve(label, list, color);
            graphCurve.Line.Width = 2;
            graphCurve.Symbol.IsVisible = false;
        }
    }
}