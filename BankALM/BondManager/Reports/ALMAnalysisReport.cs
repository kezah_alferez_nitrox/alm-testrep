﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using BankALM.Infrastructure.Data;
using BondManager.Calculations;
using BondManager.Core;
using BondManager.Domain;
using Microsoft.Office.Interop.Word;
using NHibernate;
using NHibernate.Linq;
using Variable = BondManager.Domain.Variable;
using System.Reflection;

namespace BondManager.Reports
{
    public class ALMAnalysisReport
    {
        private readonly Dictionary<Scenario, List<ResultRowModel>> resultLines;
        private readonly Dictionary<Scenario, ResultModel> resultTree;
        private readonly string user;
        private readonly BackgroundWorker backgroundworker;
        private Scenario baseScenario;
        private Document document;
        private int resultMonths;
        private Scenario[] scenarios;
        private ISession session;
        private DateTime valuationDate;
        private Variable[] variables;
        private IList<DateInterval> dateIntervals;
        private Application application;
        private string dateString;
        private string workspaceName;
        private readonly string appVersion;
        private const int PictureHeightCm = 9;
        private const int PictureWidthCm = 12;
        private const char WordNewLine = (char)11;

        public ALMAnalysisReport(Dictionary<Scenario, ResultModel> resultTree, string user, string appVersion, BackgroundWorker backgroundworker)
        {
            this.resultTree = resultTree;
            this.user = user;
            this.appVersion = appVersion;
            this.backgroundworker = backgroundworker;
            this.resultLines = resultTree.Select(p => new
                                                          {
                                                              p.Key,
                                                              Value = this.AllRecursive(p.Value.GetRows(ReportAnalysisType.Standard)).ToList()
                                                          }).ToDictionary(x => x.Key, x => x.Value);
        }

        public void Render()
        {
            try
            {
                this.application = application = new Application();
                application.Visible = true;
                object templateFileName = this.GetTemplateFileName();
                this.document = application.Documents.Open(templateFileName, false, true);

                this.session = SessionManager.OpenSession();
                this.session.FlushMode = FlushMode.Never;

                this.InitData();

                if (scenarios.Length == 0) return;

                this.RenderInternal();

                // this.document.SaveAs(@"c:\temp\ALMS.doc");
            }
            catch (Exception ex)
            {
                Log.Exception("Render ALMAnalysisReport", ex);
                #if DEBUG
                    throw (ex);
                #endif                
            }
            finally
            {
                if (this.session != null)
                {
                    this.session.Dispose();
                }

                if (application != null && !application.Visible)
                {
                    if (this.document != null)
                    {
                        this.document.Close();
                    }
                    application.Quit();
                }
            }
        }

        private void InitData()
        {
            this.dateString = DateTime.Now.ToShortDateString();
            this.workspaceName = Param.GetFileName(this.session);
            this.valuationDate = Param.GetValuationDate(this.session).GetValueOrDefault();
            this.resultMonths = Param.ResultMonths(this.session);
            this.scenarios = this.resultLines.Keys.OrderBy(s => s.Position).ToArray();
            this.baseScenario = this.scenarios.Single(s => s.IsDefault);
            this.variables = this.session.Query<Variable>().Fetch(v => v.VariableValues).Where(s => s.Selected).ToArray();
            this.dateIntervals = this.resultTree[this.scenarios[0]].DateIntervals;
        }

        private void RenderInternal()
        {
            this.RenderHeader();
            this.RenderFooter();
            backgroundworker.ReportProgress(5);
            this.RenderIndroduction();
            backgroundworker.ReportProgress(10);
            this.RenderScenarios();
            backgroundworker.ReportProgress(15);
            this.RenderImportantMarketData();
            backgroundworker.ReportProgress(20);
            this.RenderScenarioComparaisons();
            backgroundworker.ReportProgress(30);
            this.RenderAllScenariosDetails();
        }

        private void RenderAllScenariosDetails()
        {
            Range range = this.document.GoTo(WdGoToItem.wdGoToLine, WdGoToDirection.wdGoToLast);
            for (int index = 0; index < this.scenarios.Length; index++)
            {
                Scenario scenario = this.scenarios[index];
                this.RenderOneScenarioDetails(range, scenario, index);

                this.backgroundworker.ReportProgress(30 + 70 * (index + 1) / this.scenarios.Length);
            }
        }

        private void RenderOneScenarioDetails(Range range, Scenario scenario, int index)
        {
            if(index>0)
            {
                document.Paragraphs.Add();
                range = this.document.GoTo(WdGoToItem.wdGoToLine, WdGoToDirection.wdGoToLast);
                range.InsertBreak(WdBreakType.wdPageBreak);
                range = document.Range(range.End, range.End);
            }

            range.Text = "Scénario " + scenario.Name;
            range.ParagraphFormat.set_Style(WdBuiltinStyle.wdStyleHeading2);

            document.Paragraphs.Add();            
            range = this.document.GoTo(WdGoToItem.wdGoToLine, WdGoToDirection.wdGoToLast);

            RenderScenarioDetailsTable(range, scenario);
        }

        private void RenderScenarioDetailsTable(Range range, Scenario scenario)
        {
            IList<ResultRowModel> rows = this.FlattenTree(this.resultTree[scenario].GetRows(ReportAnalysisType.Standard));

            AddTable(range,"", rows.Count, dateIntervals.Count + 1,
                (cellRange, row, col) =>
                {
                    if (row == 0 && col == 0) return;
                    if (row == 0)
                    {
                        cellRange.Text = dateIntervals[col - 1].ToString(col == 1);
                    }
                    else if (col == 0)
                    {
                        cellRange.Text = rows[row].Description;
                    }
                    else
                    {
                        cellRange.Text = this.DataToString(rows[row][col - 1]);
                        cellRange.ParagraphFormat.Alignment = WdParagraphAlignment.wdAlignParagraphRight;
                    }
                }, table =>
                { 
                    table.Columns.First.Width = 100; 
                });
        }

        private IList<ResultRowModel> FlattenTree(IEnumerable<ResultRowModel> input, int indent = 0)
        {
            List<ResultRowModel> output = new List<ResultRowModel>();

            foreach (ResultRowModel row in input)
            {
                row.Description = new string(' ', indent * 2) + row.Description;
                output.Add(row);
                output.AddRange(this.FlattenTree(row.Children, indent + 1));
            }

            return output;
        }

        private void RenderScenarioComparaisons()
        {
            this.RenderScenarioComparaison("$sc_assets$","Comparaison scénarios : Total actif", rm => rm.ResultType == ResultType.BookValue &&
                                                                rm.Category != null &&
                                                                rm.Category.BalanceType == BalanceType.Asset);
            this.RenderScenarioComparaison("$sc_liabilities$","Comparaison scénarios : Total passif", rm => rm.ResultType == ResultType.BookValue &&
                                                                     rm.Category != null &&
                                                                     rm.Category.BalanceType == BalanceType.Liability);
            this.RenderScenarioComparaison("$sc_equity$","Comparaison scénarios : Total capitaux", rm => rm.ResultType == ResultType.BookValue &&
                                                                     rm.Category != null &&
                                                                     rm.Category.BalanceType == BalanceType.Equity);

            this.RenderScenarioComparaison("$sc_net_banking_income$","Comparaison scénarios : Net banking income", rm => rm.ResultType == ResultType.NetBankingIncome);
            this.RenderScenarioComparaison("$sc_impairement_charge$","Comparaison scénarios : Impairment charge", rm => rm.ResultType == ResultType.ImpairmentCharge);
            this.RenderScenarioComparaison("$sc_net_income$","Comparaison scénarios : Net Income", rm => rm.ResultType == ResultType.NetIncome);
            this.RenderScenarioComparaison("$sc_treasury$","Comparaison scénarios : Treasury", rm => rm.Category != null && rm.Category.Type == CategoryType.Treasury, AggregateTreasury);
            this.RenderScenarioComparaison("$sc_roe$","Comparaison scénarios : ROE", rm => rm.ResultType == ResultType.ROE);
            this.RenderScenarioComparaison("$sc_cost_income_ratio$","Comparaison scénarios : Cost/income ratio", rm => rm.ResultType == ResultType.CostIncomeRatio);

            this.RenderScenarioComparaison("$sc_yield_assets$","Comparaison scénarios : Yield assets", rm => rm.ResultType == ResultType.Yield && rm.Category != null &&
                                                                     rm.Category.BalanceType == BalanceType.Asset);
            this.RenderScenarioComparaison("$sc_yield_liabilities$","Comparaison scénarios : Yield liabilities", rm => rm.ResultType == ResultType.Yield && rm.Category != null &&
                                                                     rm.Category.BalanceType == BalanceType.Liability);
            this.RenderScenarioComparaison("$sc_yield_equity$","Comparaison scénarios : Yield equity", rm => rm.ResultType == ResultType.Yield && rm.Category != null &&
                                                                     rm.Category.BalanceType == BalanceType.Equity);

            this.RenderScenarioComparaison("$sc_lcr$","Comparaison scénarios : Liquidity coverage ratio", rm => rm.ResultType == ResultType.LCR);

        }

        private double AggregateTreasury(IEnumerable<ResultRowModel> rows, int i)
        {
            return rows.Sum(r => r.Category.BalanceType == BalanceType.Asset ? ObjectToDouble(r[i]) : -ObjectToDouble(r[i]));
        }

        private void RenderScenarioComparaison(string key,string title, Func<ResultRowModel, bool> selector, Func<IEnumerable<ResultRowModel>, int, double> aggregator = null)
        {
            if (aggregator == null)
            {
                aggregator = (rows, i) =>
                    {
                        object value = rows.First()[i];
                        return ObjectToDouble(value);
                    };
            }

            Dictionary<Scenario, double[]> scenarioValues = new Dictionary<Scenario, double[]>();

            foreach (Scenario scenario in scenarios)
            {
                double[] values = new double[dateIntervals.Count];
                ResultRowModel[] resultRowModels = this.resultLines[scenario].Where(selector).ToArray();

                for (int i = 0; i < dateIntervals.Count; i++)
                {
                    values[i] = aggregator(resultRowModels, i);
                }

                scenarioValues[scenario] = values;
            }


            Range range = this.AddTable(key,title, dateIntervals.Count + 1, this.scenarios.Length + 1,
                          (cellRange, row, col) =>
                          {
                              if (row == 0 && col == 0)
                              {
                              }
                              else if (row == 0)
                              {
                                  cellRange.Text = this.scenarios[col - 1].Name;
                              }
                              else if (col == 0)
                              {
                                  cellRange.Text = dateIntervals[row - 1].ToString(row == 1);
                              }
                              else
                              {
                                  cellRange.ParagraphFormat.Alignment = WdParagraphAlignment.wdAlignParagraphRight;

                                  Scenario scenario = this.scenarios[col - 1];

                                  object value = scenarioValues[scenario][row - 1];

                                  cellRange.Text = this.DataToString(value);

                                  if (scenario == this.baseScenario) return;

                                  double baseScenarioValue = scenarioValues[baseScenario][row - 1];

                                  double delta = (double)value - baseScenarioValue;
                                  if (delta == 0) return;

                                  cellRange = this.document.Range(cellRange.End - 1, cellRange.End - 1);
                                  cellRange.Text = WordNewLine + delta.ToString("N2");
                                  cellRange.Font.Color = delta > 0 ? WdColor.wdColorGreen : WdColor.wdColorRed;
                                  cellRange.Font.Size = 7;
                              }
                          });
            

            range = document.Range(range.End + 1, range.End + 1);

            string imageFileName = new ChartRenderer().Render(
                dateIntervals.Select((di, i) => di.ToString(i == 0)).ToArray(),
                scenarios.Select(s => s.Name).ToArray(),
                scenarioValues.Select(sv => sv.Value).ToArray());

            range.ParagraphFormat.Alignment = WdParagraphAlignment.wdAlignParagraphCenter;
            InlineShape picture = range.InlineShapes.AddPicture(imageFileName);
            picture.Height = application.CentimetersToPoints(PictureHeightCm);
            picture.Width = application.CentimetersToPoints(PictureWidthCm);

            range = range.Paragraphs.Add().Range;
            range.set_Style(WdBuiltinStyle.wdStyleHeading2);
            range.InsertBefore(title);
            range.InsertBefore(Environment.NewLine);

            File.Delete(imageFileName);
        }

        private static double ObjectToDouble(object value)
        {
            return value == null ? 0 : (double)value;
        }

        private IEnumerable<ResultRowModel> AllRecursive(IEnumerable<ResultRowModel> resultRowModels)
        {
            foreach (ResultRowModel row in resultRowModels)
            {
                foreach (ResultRowModel childRow in this.AllRecursive(row.Children))
                    yield return childRow;

                yield return row;
            }
        }

        private string DataToString(object data)
        {
            if (data == null || !(data is double)) return ""; 

            double doubleData = (double)data;
            if (doubleData == 0) return "";

            return doubleData.ToString("N2");
        }

        private void RenderImportantMarketData()
        {
            Vector[] vectors = VectorCache.FindByImportantMarketData();
               // this.session.Query<Vector>().Fetch(v => v.VectorPoints).Where(v => v.ImportantMarketData).ToArray();

            string[] names = new string[vectors.Length];
            double[][] values = new double[vectors.Length][];

            for (int index = 0; index < vectors.Length; index++)
            {
                Vector vector = vectors[index];
                names[index] = vector.Name;
                values[index] = Util.GetVectorValues(this.valuationDate, vector.VectorPoints, this.resultMonths, false);
            }

            string[] dates = new string[this.resultMonths + 1];
            dates[0] = "Opening Balance";
            for (int i = 0; i < this.resultMonths; i++)
            {
                dates[i + 1] = this.valuationDate.AddMonths(i + 1).ToString("MM/yyyy");
            }

            string imageFileName = new ChartRenderer().Render(dates, names, values);

            this.AddImage("$valeurs_marche$", imageFileName);
        }

        private void AddImage(string key, string fileName)
        {
            Range range = this.Find(key);
            range.Text = "";

            range.ParagraphFormat.Alignment = WdParagraphAlignment.wdAlignParagraphCenter;

            InlineShape picture = range.InlineShapes.AddPicture(fileName);
            picture.Height = application.CentimetersToPoints(PictureHeightCm);
            picture.Width = application.CentimetersToPoints(PictureWidthCm);
        }

        private void RenderScenarios()
        {
            string[][] data = new string[this.variables.Length + 1][];
            data[0] = new string[this.scenarios.Length + 1];

            for (int i = 0; i < this.scenarios.Length; i++)
            {
                Scenario scenario = this.scenarios[i];
                data[0][1 + i] = scenario.Name;
            }

            for (int i = 0; i < this.variables.Length; i++)
            {
                Variable variable = this.variables[i];
                data[i + 1] = new string[this.scenarios.Length + 1];

                data[i + 1][0] = variable.Name;

                for (int j = 0; j < this.scenarios.Length; j++)
                {
                    data[i + 1][j + 1] =
                        variable.VariableValues.Single(vv => vv.Scenario.Id == this.scenarios[j].Id).Value;
                }
            }

            this.AddTable("$description_scenarios$", data);
        }

        private void AddTable(string key, string[][] data)
        {
            if (data == null || data.Length == 0) return;

            Range range = this.Find(key);

            Table table = this.document.Tables.Add(range, data.Length, data[0].Length);
            table.set_Style("ALM Table");

            for (int i = 0; i < data.Length; i++)
            {
                for (int j = 0; j < data[i].Length; j++)
                {
                    range = table.Cell(i + 1, j + 1).Range;
                    range.Text = data[i][j];
                }
            }
        }

        private Range AddTable(string key,string title, int rowCount, int colCount, Action<Range, int, int> cellFiller, Action<Table> configure = null)
        {
            Range range = this.Find(key);

            Range titleRange = this.document.Range(range.Start - 1, range.Start - 1);

            titleRange.set_Style(WdBuiltinStyle.wdStyleHeading2);
            titleRange.InsertBefore(title);
            titleRange.InsertBefore(Environment.NewLine);
                
            return this.AddTable(range, title, rowCount, colCount, cellFiller, configure);
        }

        private Range AddTable(Range range, string title, int rowCount, int colCount, Action<Range, int, int> cellFiller, Action<Table> configure = null)
        {
            Table table = this.document.Tables.Add(range, rowCount, colCount);
            table.set_Style("ALM Table");

            if (configure != null) configure(table);

            for (int i = 0; i < rowCount; i++)
            {
                for (int j = 0; j < colCount; j++)
                {
                    range = table.Cell(i + 1, j + 1).Range;

                    cellFiller(range, i, j);
                }
            }
            
            return range;
        }


        private void RenderHeader()
        {
            Image image = Param.GetLogo(this.session);
            if (image == null) image = global::BondManager.Properties.Resources.logo200x200;

            string fileName = Path.GetTempFileName();
            Param.SaveLogoTo(image, fileName);

            Range range = this.Find("$h_logo$");
            range.Delete();
            range.InlineShapes.AddPicture(fileName);
        }

        private void RenderFooter()
        {
            this.ReplaceInTemplate("$f_version$", this.appVersion);//todo : get the ALMSolution version
            this.ReplaceInTemplate("$f_date_production$", dateString);
            this.ReplaceInTemplate("$f_nom_fichier$", this.workspaceName);
            this.ReplaceInTemplate("$f_nom_utilisateur$", this.user);
        }

        private void RenderIndroduction()
        {
            this.ReplaceInTemplate("$date_production$", dateString);
            this.ReplaceInTemplate("$nom_fichier$", this.workspaceName);
            this.ReplaceInTemplate("$nombre_scenarios$", this.resultLines.Keys.Count.ToString());
            this.ReplaceInTemplate("$nombre_variables$", this.variables.Length.ToString());
            int productCount = ProductCache.products.Count(p=>!p.Exclude);//this.session.Query<Product>().Count(p => !p.Exclude);
            this.ReplaceInTemplate("$nombre_lignes$", productCount.ToString());

            this.ReplaceInTemplate("$nom_utilisateur$", this.user);
        }

        private void ReplaceInTemplate(string key, string toReplaceWith)
        {
            Range range = this.Find(key);

            range.Delete();
            range.InsertAfter(toReplaceWith);
        }

        private Range Find(string key)
        {
            object toFindObj = key;

            foreach (Range range in document.StoryRanges)
            {
                if (range.Find.Execute(toFindObj))
                {
                    return range;
                }
            }

            throw new Exception("Invalid template : " + key + " is missing");
        }

        private object GetTemplateFileName()
        {
            string directoryName = Path.GetDirectoryName(typeof(ALMAnalysisReport).Assembly.Location);
            return Path.Combine(directoryName, @"Reports\ALMAnalysisReport.doc");
        }
    }
}