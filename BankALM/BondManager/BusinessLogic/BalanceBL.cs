using System;
using System.Collections.Generic;
using System.Linq;
using ALMS.Products;
using ALMSCommon;
using BankALM.Infrastructure.Data;
using BondManager.Domain;
using BondManager.Views.BalanceSheet.Balance;
using Eu.AlterSystems.ASNetLib.Core;
using NHibernate;
using NHibernate.Linq;

namespace BondManager.BusinessLogic
{
    public class BalanceBL
    {
        public static bool BalanceAdjustmentNeeded = true;

        public static void AjustBalanceIfNeeded()
        {
            if (BalanceAdjustmentNeeded)
            {
                AjustBalance();
                BalanceAdjustmentNeeded = false;
            }
        }

        /// <summary>
        /// Il faut que l'une des lignes serve � ajuster la position cash �quilibrant la balance comptable. Cette ligne a l'attribut "cash adj". 
        /// Il doit donc �tre cr�� syst�matiquement et automatiquement deux lignes, une � l'actif et l'autre au passif. 
        /// -	Si Sum(Asset) > Sum(Liabilities) + Sum(Equity), la ligne en liabilities cr��e avec l'attribut "cash adj" 
        ///     vera sa "book value" �tre �gale � la diff�rence (de fa�on � ajuster actif et passif)
        /// -	Si Sum(Asset) &lt; Sum(Liabilities) + Sum(Equity), la ligne en Assets cr��e avec l'attribut "cash adj"
        ///     vera sa "book value" �tre �gale � la diff�rence (de fa�on � ajuster actif et passif)
        /// </summary>
        public static void AjustBalance() // todo CDU : a se asigura ca se face flush pe sesiune inainte de a apela aceasta metoda si refresh dupa apel
        {
            using (ISession session = SessionManager.OpenSession())
            {
                double totalAssets;
                double totalLiabilities;
                double totalEquity;

                bool isNewBondLiability;
                bool isNewBondAsset;

                if (EntityBase<Category, int>.FindAll(session).Length == 0) return;

                Product cashAdjProductLiability = GetCashAdjBondByBalanceType(session, BalanceType.Liability,
                                                                              out isNewBondLiability);
                Product cashAdjProductAsset = GetCashAdjBondByBalanceType(session, BalanceType.Asset, out isNewBondAsset);

                Currency valuationCurrency = Param.GetValuationCurrency(session);
                if (valuationCurrency != null)
                {
                    cashAdjProductAsset.Currency = valuationCurrency;
                    cashAdjProductLiability.Currency = valuationCurrency;
                }

                ComputeTotalsForALM(session, out totalAssets, out totalLiabilities, out totalEquity);

                double delta = totalAssets - (totalLiabilities + totalEquity);

                double cashAdjAssetValue = cashAdjProductAsset.BookValue.GetValueOrDefault();
                double cashAdjLiabilitiesValue = cashAdjProductLiability.BookValue.GetValueOrDefault();

                AjustCashAdjBonds(ref cashAdjAssetValue, ref cashAdjLiabilitiesValue, delta);

                cashAdjProductAsset.BookValue = cashAdjAssetValue;
                cashAdjProductLiability.BookValue = cashAdjLiabilitiesValue;

                session.Flush();
            }
        }

        public static void ClearCache()
        {
            //for (int i = 0; i < cashAdjBondIdCache.Length; i++)
            //{
            //    cashAdjBondIdCache[i] = 0;
            //}
        }

        public static void AjustCashAdjBonds(ref double cashAdjBondAsset, ref double cashAdjBondLiability, double balance)
        {
            if (balance > 0)
            {
                cashAdjBondLiability += balance;
            }
            else if (balance < 0)
            {
                cashAdjBondAsset -= balance;
            }

            if (cashAdjBondAsset >= cashAdjBondLiability)
            {
                cashAdjBondAsset = cashAdjBondAsset - cashAdjBondLiability;
                cashAdjBondLiability = 0;
            }
            else
            {
                cashAdjBondLiability = cashAdjBondLiability - cashAdjBondAsset;
                cashAdjBondAsset = 0;
            }
        }

        private static void ComputeTotalsForALM(ISession session, out double assets, out double liabilities, out double equity)
        {
            assets = liabilities = equity = 0;

            Currency valuationCurrency = Param.GetValuationCurrency(session);

            Currency[] currencies = EntityBase<Currency, int>.FindAll(session).Union(new Currency[] { null }).ToArray();
            foreach (Currency currency in currencies)
            {
                double? assetsSum = 0;
                try
                {
                    assetsSum = ProductCache.products.FindAll(//session.Query<Product>().Where(
                            p =>
                            !p.Exclude && p.Attrib != ProductAttrib.Notional && p.BookValue != null &&
                            p.Currency == currency && p.Category.BalanceType == BalanceType.Asset && p.Attrib != ProductAttrib.InFuture).Sum(p => p.BookValue);
                }
                catch (Exception ex)
                {
                    Logger.Exception("cannot compute assetsSum in ComputeTotalsForALM", ex);
                }

                double? liabilitiesSum = 0;
                try
                {
                    liabilitiesSum = ProductCache.products.FindAll(//session.Query<Product>().Where(
                            p =>
                            !p.Exclude && p.Attrib != ProductAttrib.Notional && p.BookValue != null &&
                            p.Currency == currency && p.Category.BalanceType == BalanceType.Liability && p.Attrib != ProductAttrib.InFuture).Sum(p => p.BookValue);
                }
                catch (Exception ex)
                {
                    Logger.Exception("cannot compute liabilitiesSum in ComputeTotalsForALM", ex);
                }
                double? equitySum = 0;
                try
                {
                    equitySum = ProductCache.products.FindAll(//session.Query<Product>().Where(
                            p =>
                            !p.Exclude && p.Attrib != ProductAttrib.Notional && p.BookValue != null &&
                            p.Currency == currency && p.Category.BalanceType == BalanceType.Equity && p.Attrib != ProductAttrib.InFuture).Sum(p => p.BookValue);
                }
                catch (Exception ex)
                {
                    Logger.Exception("cannot compute liabilitiesSum in ComputeTotalsForALM", ex);
                }
                assets += CurrencyBL.ConvertToValuationCurrency(assetsSum ?? 0, currency, valuationCurrency);

                liabilities += CurrencyBL.ConvertToValuationCurrency(liabilitiesSum ?? 0, currency, valuationCurrency);

                equity += CurrencyBL.ConvertToValuationCurrency(equitySum ?? 0, currency, valuationCurrency);
            }
        }

        private static Product GetCashAdjBondByBalanceType(ISession session, BalanceType balanceType, out bool isNewBond)
        {
            isNewBond = false;

            Product product = ProductCache.FindFirstCashAdjByBalanceTypeAndAttrib( balanceType, ProductAttrib.CashAdj);

            if (product == null || product.Attrib != ProductAttrib.CashAdj)
            {
                // Create new Cash Adj Product
                product = new Product();
                product.Category = Category.CashAdjCategory(session, balanceType);
                product.Description = Constants.CASH_ADJ_BOND_DESCRIPTION;
                product.Attrib = ProductAttrib.CashAdj;
                product.Currency = Param.GetValuationCurrency(session);
                product.GenerateALMID();
                product.Duration = Param.GetDefaultBondDuration(session);
                product.BookPrice = "100";
              //  product.Create(session);
               // ProductCache.products.Add(product);

                if (ProductCache.products.Any(p => p.ALMIDDisplayed == product.ALMIDDisplayed))
                {
                    int pos = ProductCache.products.FindIndex(p => p.ALMIDDisplayed == product.ALMIDDisplayed);
                    ProductCache.products[pos] = product;
                }
                else
                    ProductCache.products.Add(product);
                session.Flush();

                isNewBond = true;
            }
            else
            {
             //   product.Refresh(session);
                
            }

            // cashAdjBondIdCache[intBlanceType] = Product.Id;

            return product;
        }

        public static void EnsureSpecialCategoriesAndBondsExist()
        {
            using (ISession session = SessionManager.OpenSession())
            {
                Category rootAssets = null;
                Category rootLiabilities = null;
                Category rootEquity = null;

                Category root = Category.FindBalanceSheet(session);
                if (root == null) return;


                foreach (Category child in root.Children)
                {
                    if (child.BalanceType == BalanceType.Asset) rootAssets = child;
                    if (child.BalanceType == BalanceType.Liability) rootLiabilities = child;
                    if (child.BalanceType == BalanceType.Equity) rootEquity = child;
                }

                Currency[] currencies = Currency.FindAllNotEmpty(session);
                EnsurePerCurrencyCategoryAndProducts(session, currencies, rootAssets, CategoryType.Treasury);
                EnsurePerCurrencyCategoryAndProducts(session, currencies, rootLiabilities, CategoryType.Treasury);
                EnsurePerCurrencyCategoryAndProducts(session, currencies, rootAssets, CategoryType.Receivable);
                EnsurePerCurrencyCategoryAndProducts(session, currencies, rootLiabilities, CategoryType.Payable);

                EnsureSpecialCategoryAndBondExist(session, CategoryType.CashAdj, true, true, false, rootAssets, rootLiabilities, rootEquity);
                EnsureSpecialCategoryAndBondExist(session, CategoryType.Treasury, true, true, false, rootAssets, rootLiabilities, rootEquity);
                //EnsureSpecialCategoryAndBondExist(session, CategoryType.Impairments, true, false, false, rootAssets, rootLiabilities, rootEquity);
                Category impairments = EnsureSpecialCategoryExists(session, rootAssets, CategoryType.Impairments);
                if (impairments.Bonds.Count > 0)
                { // migrate impairments bonds
                    Category modelImpairments = EnsureSpecialCategoryExists(session, impairments, CategoryType.ModelImpairments);
                    foreach (Product product in impairments.Bonds)
                    {
                        product.Attrib = ProductAttrib.ModelImpairments;
                        product.Category = modelImpairments;
                        modelImpairments.Bonds.Add(product);
                    }
                    //impairements.Bonds = new List<Product>();
                    session.Flush();
                    
                    
                }
                EnsureSpecialCategoryAndBondExist(session, CategoryType.ModelImpairments, true, false, false, impairments, null, null);

                EnsureSpecialCategoryAndBondExist(session, CategoryType.IBNR, true, false, false, rootAssets, rootLiabilities, rootEquity);
                EnsureSpecialCategoryAndBondExist(session, CategoryType.Reserves, false, false, true, rootAssets, rootLiabilities, rootEquity);
                EnsureSpecialCategoryAndBondExist(session, CategoryType.PositiveDerivativesValuation, true, false, false, rootAssets, rootLiabilities, rootEquity);
                EnsureSpecialCategoryAndBondExist(session, CategoryType.MarginCallPaid, true, false, false, rootAssets, rootLiabilities, rootEquity);
                EnsureSpecialCategoryAndBondExist(session, CategoryType.NegativeDerivativesValuation, false, true, false, rootAssets, rootLiabilities, rootEquity);
                EnsureSpecialCategoryAndBondExist(session, CategoryType.MarginCallReceived, false, true, false, rootAssets, rootLiabilities, rootEquity);

                EnsureOciExistsAndIsWellPlaced(session, rootLiabilities, rootEquity);

                EnsureOIA(session);

                DeleteObsoleteCategories(session);

                Category rootOffBS = Category.FindOffBalanceSheet(session);
                if (rootOffBS == null) return;
                rootOffBS.isVisible = true;
                if (rootOffBS.Children.Count == 0)
                {
                    CreateSpecialCategory(session, rootOffBS, "Swaps", BalanceType.Derivatives);
                    CreateSpecialCategory(session, rootOffBS, "Cap-Floor", BalanceType.Derivatives);
                    CreateSpecialCategory(session, rootOffBS, "Swaptions", BalanceType.Derivatives);
                }

                session.Flush();
            }
        }

        private static void EnsureOIA(ISession session)
        {
            Dictionary<string, string> oiaNames = new Dictionary<string, string>(){{"Commissions",OtherItemAssum.CommisionsAndOthersGroup},
                                                                                    {"Charges",OtherItemAssum.CostGroup},
                                                                                    {"Application fees",OtherItemAssum.CommisionsAndOthersGroup},
                                                                                    {"Application charges",OtherItemAssum.CostGroup},
                                                                                    {"Prepayement fees",OtherItemAssum.CommisionsAndOthersGroup},
                                                                                    {"Amortizing",OtherItemAssum.CostGroup},
                                                                                    {"Cost insurance",OtherItemAssum.CostGroup},
                                                                                    {"Impairments",OtherItemAssum.ImpairmentsRisk},};
            OtherItemAssum[] existingOIA = OtherItemAssum.FindAll(session);
            OtherItemAssum specialItem;
            foreach (KeyValuePair<string, string> oiaName in oiaNames)
            {
                specialItem = existingOIA.FirstOrDefault(o => String.Equals(o.Descrip, oiaName.Key, StringComparison.InvariantCultureIgnoreCase));
                if (specialItem != null)
                {
                    specialItem.CannotDelete = true;
                }
                else
                {
                    specialItem = new OtherItemAssum();
                    specialItem.CannotDelete = true;
                    specialItem.Descrip = oiaName.Key;
                    specialItem.SubtotalGroup = oiaName.Value;
                    specialItem.Vect = "0";
                    specialItem.IsVisibleChecked = true;
                    specialItem.Create(session);
                }
            }
            session.Flush();
        }

        private static void EnsurePerCurrencyCategoryAndProducts(ISession session, IEnumerable<Currency> currencies,
            Category parentCategory, CategoryType categoryType)
        {
            Category category = EnsureSpecialCategoryExists(session, parentCategory, categoryType);
            EnsurePerCurrencyProducts(session, category, currencies);
        }

        private static void EnsurePerCurrencyProducts(ISession session, Category category, IEnumerable<Currency> currencies)
        {
            Currency valuationCurrency = Param.GetValuationCurrency(session);
            foreach (Currency currency in currencies)
            {
                Product[] treasuryProducts = ProductCache.products.Where(p => p.Category.ALMID== category.ALMID && (p.Currency == currency || (p.Currency == null && currency == valuationCurrency))).ToArray();

                if (treasuryProducts.Length == 0)
                {
                    CreateProduct(session, category, currency);
                }
                else if (treasuryProducts.Length > 1)
                {
                    for (int i = 1; i < treasuryProducts.Length; i++)
                    {
                        Product product = treasuryProducts[i];
                        product.Category.Bonds.Remove(product);
                      //  product.Delete(session);
                        ProductCache.products.Remove(product);
                    }
                }
            }
        }

        private static void DeleteObsoleteCategories(ISession session)
        {
            Category[] toDelete = session.Query<Category>().
                Where(c => c.Type == CategoryType.InterestPayedOnDerivatives || c.Type == CategoryType.InterestReceivedOnDerivatives).
                ToArray();

            foreach (Category category in toDelete)
            {
                category.Parent.Children.Remove(category);
            }
        }

        private static void EnsureOciExistsAndIsWellPlaced(ISession session, Category rootLiabilities, Category rootEquity)
        {
            if (rootEquity == null || rootLiabilities == null)
            {
                Log.Error("EnsureOciExistsAndIsWellPlaced : rootEquity==null || rootLiabilities==null");
                return;
            }
            int ociManagement = Param.GetOCIManagementSelectedIndex(session);
            Category oci = GetSpecificChildByType(rootLiabilities, CategoryType.OtherComprehensiveIncome);
            if (oci == null) oci = GetSpecificChildByType(rootEquity, CategoryType.OtherComprehensiveIncome);
            if (oci == null)
            {
                oci = EnsureSpecialCategoryExists(session,
                                            (ociManagement == Param.OCI_MANAGEMENT_AS_EQUITY
                                                 ? rootEquity
                                                 : rootLiabilities), CategoryType.OtherComprehensiveIncome);
            }
            else
            {

                if (ociManagement == Param.OCI_MANAGEMENT_AS_EQUITY && oci.Parent == rootLiabilities)
                {
                    oci.Parent = rootEquity;
                    rootEquity.Children.Add(oci);
                }
                else if (ociManagement == Param.OCI_MANAGEMENT_AS_LIABILITY && oci.Parent == rootEquity)
                {
                    oci.Parent = rootLiabilities;
                    rootLiabilities.Children.Add(oci);
                }
            }
            EnsureSpecialBondExists(session, oci);
        }

        private static void EnsureSpecialCategoryAndBondExist(ISession session, CategoryType type, bool inAssets, bool inLiabilities, bool inEquity, Category rootAssets, Category rootLiabilities, Category rootEquity)
        {
            if (rootAssets != null && inAssets)
            {
                Category assetsSpecial = EnsureSpecialCategoryExists(session, rootAssets, type);
                EnsureSpecialBondExists(session, assetsSpecial);
            }
            if (rootLiabilities != null && inLiabilities)
            {
                Category liabilitiesSpecial = EnsureSpecialCategoryExists(session, rootLiabilities, type);
                EnsureSpecialBondExists(session, liabilitiesSpecial);
            }
            if (rootEquity != null && inEquity)
            {
                Category equitySpecial = EnsureSpecialCategoryExists(session, rootEquity, type);
                EnsureSpecialBondExists(session, equitySpecial);
            }
        }

        private static void EnsureSpecialBondExists(ISession session, Category category)
        {
            Product[] products = ProductCache.FindByCategory( category);
            if (products != null && products.Length >= 1) return;

            CreateProduct(session, category);
        }

        private static void CreateProduct(ISession session, Category category, Currency currency = null)
        {
            Product product = new Product();
            product.Category = category;
            product.Currency = currency;
            product.Description = EnumHelper.GetDescription(category.Type);
            if (currency != null)
            {
                product.Description = currency + " " + product.Description;
            }
            product.Attrib = Category.MapCategoryTypeToBondAttrib(category.Type);
            product.Duration = Param.GetDefaultBondDuration(session);
            product.Coupon = "0";
            product.Spread = "0";
            product.OrigFace = "0";
            product.Position = category.Bonds.Count + 1;
            product.GenerateALMID();
            if (product.Category.Type == CategoryType.Treasury)
            {
                product.ProductType = ProductType.BondVr;
                product.Duration = 999;
                product.CouponType = CouponType.Floating;
                product.Frequency = ProductFrequency.Monthly;
            }
            if (product.Category.Type == CategoryType.MarginCallPaid ||
                product.Category.Type == CategoryType.MarginCallReceived)
            {
                product.Frequency = ProductFrequency.Monthly;
            }
           // ProductCache.products.Add(product);//product.Create(session);
            if (ProductCache.products.Any(p => p.ALMIDDisplayed == product.ALMIDDisplayed))
            {
                int pos = ProductCache.products.FindIndex(p => p.ALMIDDisplayed == product.ALMIDDisplayed);
                ProductCache.products[pos] = product;
            }
            else
                ProductCache.products.Add(product);
            session.Flush();
        }

        private static void CreateSpecialCategory(ISession session, Category parent, string categLabel, BalanceType btype)
        {
            Category category = new Category();
            category.Label = categLabel;
            category.ReadOnly = true;
            category.Type = CategoryType.Normal;
            category.Parent = parent;
            category.BalanceType = btype;
            category.isVisible = parent.isVisible;
            category.ALMID = Category.GetNewALMID(session);
            parent.Children.Add(category);
            category.Create(session);
        }

        private static Category EnsureSpecialCategoryExists(ISession session, Category parent, CategoryType type)
        {
            Category child = GetSpecificChildByType(parent, type);

            if (child != null)
            {
                if (child.Type == CategoryType.Treasury &&
                    (child.AnalysisCategory == null ||
                    child.AnalysisCategory != Category.TREASURIES_ANALYSIS_CATEGORY))
                {
                    child.AnalysisCategory = Category.TREASURIES_ANALYSIS_CATEGORY;
                    session.Flush();
                }
                return child;
            }

            Category category = new Category();
            category.Label = EnumHelper.GetDescription(type);
            category.ReadOnly = true;
            category.Type = type;
            category.Parent = parent;
            category.BalanceType = parent.BalanceType;
            category.isVisible = parent.isVisible;
            category.ALMID = Category.GetNewALMID(session);
            parent.Children.Add(category);
            category.Create(session);

            return category;
        }

        private static Category GetSpecificChildByType(Category parent, CategoryType type)
        {
            return parent.Children.FirstOrDefault(child => child.Type == type);
        }

        public static void EnsureRollCategoryHasDefaultSubCategory()
        {
            using (ISession session = SessionManager.OpenSession())
            {
                //Category balanceSheet = Category.FindBalanceSheet(session);

                //if (balanceSheet==null)
                //    throw new Exception("balanceSheet is null @ EnsureRollCategoryHasDefaultSubCategory");

                //Category[] rollCategories = balanceSheet.Children.SelectMany(c => c.Children).Where(c => c.Type == CategoryType.Roll).ToArray();

                Category[] rollCategories = Category.FindByType(session, CategoryType.Roll).ToArray();

                foreach (Category rollCategory in rollCategories)
                {
                    if (rollCategory.Parent.Type == CategoryType.RollParent)
                        continue;
                    rollCategory.Type = CategoryType.RollParent;

                    Category defaultRollCategory = rollCategory.Children.FirstOrDefault(c => c.Label == Category.DEFAULT_ROLL_LABEL);
                    if (defaultRollCategory == null)
                    {
                        defaultRollCategory = new Category
                            {
                                Label = Category.DEFAULT_ROLL_LABEL,
                                Parent = rollCategory,
                                BalanceType = rollCategory.BalanceType,
                                Type = CategoryType.Roll,
                                ReadOnly = true
                            };
                        rollCategory.Children.Add(defaultRollCategory);
                    }


                    Product[] prodroll = ProductCache.products.FindAll(p=>p.Category.ALMID==rollCategory.ALMID).ToArray();
                    if (prodroll.Length > 0)
                    {
                        foreach (Product product in prodroll)
                        {
                            product.Category = defaultRollCategory;
                        }
                    }
                }

                session.Flush();
            }
        }
    }
}