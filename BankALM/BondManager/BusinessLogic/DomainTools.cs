using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BondManager.Domain;
using NHibernate;
using NHibernate.Linq;

namespace BondManager.BusinessLogic
{
    public class DomainTools
    {
        public static string[] GetAllVectorNames()
        {
            return VectorCache.vectors.Select(v => v.Name).ToArray();//session.Query<Vector>().Select(v=>v.Name).ToArray();
        }

        public static string[] GetAllVariableNames(ISession session)
        {
            return session.Query<Variable>().Select(v => v.Name).ToArray();
        }

        public static string[] GetAllVariablesAndVectorNames(ISession session)
        {
            return GetAllVectorNames().Union(GetAllVariableNames(session)).ToArray();
        }
    }
}
