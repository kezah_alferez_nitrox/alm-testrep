﻿using System;
using System.Diagnostics;
using System.Linq.Expressions;
using System.Reflection;
using ALMSCommon;

namespace BondManager.Core
{
    public class EntityArrayMapper<T>
    {
        private readonly MethodInfo[] getters;
        private readonly string[] propertyNames;
        private readonly MethodInfo[] setters;

        public EntityArrayMapper(params Expression<Func<T, object>>[] propertyExpressions)
        {
            Debug.Assert(propertyExpressions != null);

            this.propertyNames = new string[propertyExpressions.Length];
            for (int i = 0; i < propertyExpressions.Length; i++)
            {
                if (propertyExpressions[i] == null) continue;
                this.PropertyNames[i] = StaticReflector.GetAccessor(propertyExpressions[i]);
            }

            this.getters = new MethodInfo[this.PropertyNames.Length];
            this.setters = new MethodInfo[this.PropertyNames.Length];

            for (int i = 0; i < this.PropertyNames.Length; i++)
            {
                if (string.IsNullOrEmpty(this.PropertyNames[i])) continue;

                PropertyInfo propertyInfo = typeof(T).GetProperty(this.PropertyNames[i]);
                this.getters[i] = propertyInfo.GetGetMethod();
                this.setters[i] = propertyInfo.GetSetMethod();
            }
        }

        public object[] GetArray(object entity)
        {
            Debug.Assert(entity != null);
            Debug.Assert(entity is T);

            var result = new object[this.getters.Length];
            for (int i = 0; i < this.getters.Length; i++)
            {
                if (this.getters[i] == null) continue;
                result[i] = this.getters[i].Invoke(entity, null);
            }
            return result;
        }

        public void SetPropertyByIndex(object entity, int index, object value)
        {
            Debug.Assert(entity != null);
            Debug.Assert(entity is T);

            this.setters[index].Invoke(entity, new[] { value });
        }

        public object GetPropertyByIndex(object entity, int index)
        {
            Debug.Assert(entity != null);
            Debug.Assert(entity is T);

            return this.getters[index].Invoke(entity, null);
        }

        public int Count
        {
            get { return this.PropertyNames.Length; }
        }

        public Type[] Types
        {
            get
            {
                Type[] types = new Type[this.getters.Length];
                for (int i = 0; i < this.getters.Length; i++)
                {
                    types[i] = getters[i].ReturnType;
                }
                return types;
            }
        }

        public string[] PropertyNames
        {
            get { return this.propertyNames; }
        }
    }
}