using System;
using System.Collections.Generic;
using System.Linq;
using ALMS.Products;
using BankALM.Infrastructure.Data;
using BondManager.Calculations;
using BondManager.Domain;
using BondManager.Resources.Language;
using BondManager.Views.BalanceSheet.Balance;
using NHibernate;

namespace BondManager.Core
{
    public class ResultModel
    {
        public const string HeaderDateFormat = "MM/yyyy";
        private readonly ScenarioData scenarioData;
        private readonly Category[] categories;

        private int currentRowId;
        private readonly ResultAggregator resultAggregator;
        private readonly IList<DateInterval> dateIntervals;
        private readonly int resultMonths;
        private readonly ReportAnalysisType analysisType;
        private double[] analysisTreasury;

        public ResultModel(ScenarioData data, Category[] categories, Calculations.ReportType reportType, ReportAggregationType aggregationType, DateTime valuationDate, ReportAnalysisType analysisType)
        {
            this.scenarioData = data;
            this.categories = categories;
            this.ReportType = reportType;
            this.ValuationDate = valuationDate;

            this.resultMonths = scenarioData.ResultMonths;
            this.analysisType = analysisType;

            this.resultAggregator = new ResultAggregator(reportType, aggregationType, valuationDate, this.resultMonths);
            this.dateIntervals = this.resultAggregator.DateIntervals;
            this.ValueColumnsCount = Math.Min(this.dateIntervals.Count, this.resultMonths);
        }

        public Calculations.ReportType ReportType { get; private set; }
        public DateTime ValuationDate { get; private set; }

        public int ValueColumnsCount { get; private set; }
        public IList<ResultRowModel> Rows { get; private set; }

        public ScenarioData ScenarioData
        {
            get { return this.scenarioData; }
        }

        public IList<DateInterval> DateIntervals
        {
            get { return this.dateIntervals; }
        }

        public IList<ResultRowModel> GetRows(ReportAnalysisType analysisType)
        {
            if (this.Rows != null) return this.Rows;
            int cashflowType;
            using (ISession session = SessionManager.OpenSession())
            {
                cashflowType = Param.GetGapCashflowPresentation(session);
            }

            List<ResultRowModel> rows = new List<ResultRowModel>();

            this.InitColumns(rows);

            this.CreateBalanceSheetRows(rows, ResultSection.BookValue);

            this.CreateBalanceSheetRows(rows, ResultSection.Income);

            this.CreateNetInterestIncome(rows);

            if (analysisType == ReportAnalysisType.FinancialAnalysis) this.CreateAnalysisCategoriesWithNetAnalysisIncome(rows);

            this.CreateGainLosesOnFinancialInstrument(rows);

            this.CreateOtherItemAssuptions(rows);

            this.CreateNetIncomeBeforeImpairmentCharge(rows);

            this.CreateImpairementData(rows);

            this.CreateImpairments(rows);

            this.CreateIncomePreTax(rows);

            this.CreateIncomeTaxBenefit(rows);

            this.CreateNetIncome(rows);

            this.CreateDividends(rows);

            this.CreateRoe(rows);

            this.CreateCostIcomeRatio(rows);

            this.CreatePeriodicROA(rows);

            this.CreateBalanceSheetRows(rows, ResultSection.Yield);

            if (ALMSCommon.AlmConfiguration.IsBankALMModule())
            {
                this.CreateSolvencyRows(rows);

                this.CreateLcrRows(rows);
            }
            if (this.scenarioData.ComputeGap)
                this.CreateGapLines(rows, cashflowType);

            this.CreateDynamicVariableRows(rows);

            Replace0ByNull(rows);

            this.Rows = rows;

            return rows;
        }

        private void InitColumns(List<ResultRowModel> gridData)
        {
            ResultRowModel periodHeaderSummary = new ResultRowModel(this.currentRowId++, "", FormattingType.PeriodHeader,
                                                                    this.ValueColumnsCount);
            for (int i = 0; i < this.ValueColumnsCount; i++)
            {
                DateInterval interval = this.dateIntervals[i];

                var headerText = interval.ToString(i == 0);
                periodHeaderSummary[i] = headerText;
            }

            gridData.Add(periodHeaderSummary);
        }

        private void CreateImpairementData(List<ResultRowModel> gridData)
        {
            foreach (Category category in this.categories)
            {
                if (category.IsVisibleCheckedModel == true &&  ProductCache.products.FindAll(p=>p.Category.ALMID==category.ALMID).Any(b => b.IsModelAndNotDerivative))
                {
                    ResultRowModel rm = new ResultRowModel(this.currentRowId++, category.Label, FormattingType.Normal,
                                                           this.ValueColumnsCount);

                    double[] values = this.ScenarioData.GetImpermentData(category);
                    for (int i = 0; i < this.ValueColumnsCount; i++)
                        rm[i] = this.resultAggregator.DateIntervalSum(values, i);
                    gridData.Add(rm);
                }
            }
        }

        private void CreateImpairments(List<ResultRowModel> data)
        {
            ResultRowModel rm = new ResultRowModel(this.currentRowId++,
                                                   Labels.ResultModel_CreateImpairments_Impairment_Charge,
                                                   FormattingType.Header, this.ValueColumnsCount);
            ResultRowModel rmModel = new ResultRowModel(this.currentRowId++,
                                                   "Model Impairements",
                                                   FormattingType.Normal, this.ValueColumnsCount);
            ResultRowModel rmRisk;

            rm.ResultType = ResultType.ImpairmentCharge;
            double[] impairment = this.ScenarioData.GetImpairment();
            double dateIntervalSum;
            double[] total = new double[this.ValueColumnsCount];
            for (int i = 0; i < this.ValueColumnsCount; i++)
            {
                dateIntervalSum = this.resultAggregator.DateIntervalSum(impairment, i);
                rmModel[i] = dateIntervalSum;
                total[i] += dateIntervalSum;
            }
            rm.Children.Add(rmModel);

            IDictionary<OtherItemAssum, double[]> impairmentRisk = this.ScenarioData.GetImpairmentOIA();
            foreach (KeyValuePair<OtherItemAssum, double[]> iRisk in impairmentRisk)
            {
                rmRisk = new ResultRowModel(this.currentRowId++,
                                            iRisk.Key.Descrip,
                                            FormattingType.Normal, this.ValueColumnsCount);

                for (int i = 0; i < this.ValueColumnsCount; i++)
                {
                    dateIntervalSum = this.resultAggregator.DateIntervalSum(iRisk.Value, i);
                    rmRisk[i] = dateIntervalSum;
                    total[i] += dateIntervalSum;
                }
                rm.Children.Add(rmRisk);
            }
            
            
            //todo : integrate others impairements later

            //copute total
            for (int i = 0; i < this.ValueColumnsCount; i++)
            {
                rm[i] = total[i];
            }

            data.Add(rm);
        }

        private static void Replace0ByNull(List<ResultRowModel> gridData)
        {
            for (int i = 1; i < gridData.Count; i++)
            {
                for (int j = 2; j < gridData[i].RowValues.Length; j++)
                {
                    if (gridData[i].RowValues[j] != null && (Util.IsZero((double)gridData[i].RowValues[j])))
                        gridData[i].RowValues[j] = null;
                }
            }
        }

        private void CreatePeriodicROA(List<ResultRowModel> gridData)
        {
            ResultRowModel rm = new ResultRowModel(this.currentRowId++,
                                                   Labels.ResultModel_CreatePeriodicROA_Periodic_ROA,
                                                   FormattingType.Percentage, this.ValueColumnsCount);

            double[] netIncome = this.ScenarioData.GetNetIncome();
            double[] totalAssets = this.ScenarioData.GetTotalAssets();
            for (int i = 0; i < this.ValueColumnsCount; i++)
            {
                double intervalTotalAssets = this.resultAggregator.DateIntervalValue(totalAssets, i);
                if (Util.IsNotZero(intervalTotalAssets))
                {
                    try
                    {
                        rm[i] = this.resultAggregator.DateIntervalSum(netIncome, i) / intervalTotalAssets * 12 / this.dateIntervals[i].Length;
                    }
                    catch (OverflowException)
                    {
                        //MTU : do nothing as in totalAssets[i] == 0
                    }
                }
            }

            gridData.Add(rm);
        }

        private void CreateRoe(List<ResultRowModel> gridData)
        {
            ResultRowModel rm = new ResultRowModel(this.currentRowId++, Labels.ResultModel_CreateRoe_ROE,
                                                   FormattingType.Percentage, this.ValueColumnsCount);
            rm.ResultType = ResultType.ROE;
            double[] netIncome = this.ScenarioData.GetNetIncome();
            double[] totalEquity = this.ScenarioData.GetTotalEquity();
            for (int i = 0; i < this.ValueColumnsCount; i++)
            {
                double intervalTotalEquity = this.resultAggregator.DateIntervalValue(totalEquity, i);
                if (Util.IsNotZero(intervalTotalEquity))
                    rm[i] = this.resultAggregator.DateIntervalSum(netIncome, i) / intervalTotalEquity;
            }

            gridData.Add(rm);
        }

        private void CreateDividends(List<ResultRowModel> gridData)
        {
            ResultRowModel dividendsTotal = new ResultRowModel(this.currentRowId++,
                                                              Labels.ResultModel_CreateDividends_Dividends,
                                                              FormattingType.Header, this.ValueColumnsCount);
            gridData.Add(dividendsTotal);

            IDictionary<Dividend, double[]> dividendAmounts = this.ScenarioData.GetDividendAmounts();

            double[] sums = new double[this.ValueColumnsCount];
            foreach (KeyValuePair<Dividend, double[]> pair in dividendAmounts)
            {
                Dividend dividend = pair.Key;
                double[] values = pair.Value;
                ResultRowModel rm = new ResultRowModel(this.currentRowId++, dividend.Name, FormattingType.Normal,
                                                       this.ValueColumnsCount);

                for (int i = 1; i < this.ValueColumnsCount; i++)
                {
                    double value = this.resultAggregator.DateIntervalSum(values, i);
                    sums[i] += value;
                    rm[i] = value;
                }
                dividendsTotal.Children.Add(rm);
            }

            for (int i = 1; i < this.ValueColumnsCount; i++)
            {
                dividendsTotal[i] = sums[i];
            }
        }

        private void CreateNetIncome(List<ResultRowModel> data)
        {
            ResultRowModel rm = new ResultRowModel(this.currentRowId++, Labels.ResultModel_CreateNetIncome_Net_Income,
                                                   FormattingType.Header, this.ValueColumnsCount);
            rm.ResultType = ResultType.NetIncome;
            rm.SyntheseType = SyntheseType.NetIncome;
            double[] netIncome = this.ScenarioData.GetNetIncome();
            for (int i = 0; i < this.ValueColumnsCount; i++)
                rm[i] = this.resultAggregator.DateIntervalSum(netIncome, i);

            data.Add(rm);
        }

        private void CreateIncomeTaxBenefit(List<ResultRowModel> data)
        {
            ResultRowModel rm = new ResultRowModel(this.currentRowId++,
                                                   Labels.
                                                       ResultModel_CreateIncomeTaxBenefit_Income_tax_benefit____expense_,
                                                   FormattingType.Normal, this.ValueColumnsCount);

            double[] taxes = this.ScenarioData.GetTaxes();
            for (int i = 0; i < this.ValueColumnsCount; i++)
                rm[i] = this.resultAggregator.DateIntervalSum(taxes, i);

            data.Add(rm);
        }

        private void CreateIncomePreTax(List<ResultRowModel> data)
        {
            ResultRowModel rm = new ResultRowModel(this.currentRowId++,
                                                   Labels.ResultModel_CreateIncomePreTax_Income_Pre_Tax,
                                                   FormattingType.Header, this.ValueColumnsCount);

            double[] incomePreTax = this.ScenarioData.GetIncomePreTax();
            for (int i = 0; i < this.ValueColumnsCount; i++)
                rm[i] = this.resultAggregator.DateIntervalSum(incomePreTax, i);

            data.Add(rm);
        }

        private void CreateNetIncomeBeforeImpairmentCharge(List<ResultRowModel> data)
        {
            ResultRowModel rm = new ResultRowModel(this.currentRowId++,
                                                   Labels.
                                                       ResultModel_CreateNetIncomeBeforeImpairmentCharge_Income_before_impairment_charge,
                                                   FormattingType.Header, this.ValueColumnsCount);

            double[] interestIncomeBeforeImpairment = this.ScenarioData.GetInterestIncomeBeforeImpairment();
            for (int i = 0; i < this.ValueColumnsCount; i++)
                rm[i] = this.resultAggregator.DateIntervalSum(interestIncomeBeforeImpairment, i);

            data.Add(rm);
        }

        private void CreateNetBankingIncome(List<ResultRowModel> rows)
        {
            ResultRowModel rm = new ResultRowModel(this.currentRowId++,
                                                   Labels.ResultModel_CreateNetBankingIncome_Net_Banking_Income,
                                                   FormattingType.Header, this.ValueColumnsCount);
            rm.ResultType = ResultType.NetBankingIncome;

            double[] netBankingIncome = this.ScenarioData.GetNetBankingIncome();
            for (int i = 1; i < this.ValueColumnsCount; i++)
                rm[i] = this.resultAggregator.DateIntervalSum(netBankingIncome, i);

            rows.Add(rm);
        }

        private void CreateNetInterestIncome(List<ResultRowModel> data)
        {
            ResultRowModel rm = new ResultRowModel(this.currentRowId++,
                                                   Labels.ResultModel_CreateNetInterestIncome_Net_Interest_Income,
                                                   FormattingType.Header, this.ValueColumnsCount);

            double[] netInterestIncome = this.ScenarioData.GetNetInterestIncome();
            for (int i = 1; i < this.ValueColumnsCount; i++)
                rm[i] = this.resultAggregator.DateIntervalSum(netInterestIncome, i);

            data.Add(rm);
        }

        private void CreateSolvencyRows(List<ResultRowModel> rows)
        {
            Solvency solvency = this.ScenarioData.Solvency;
            this.CreateSolvabilityRow(rows, Labels.ResultModel_CreateSolvencyRows_RWA, solvency.Rwa,
                                      FormattingType.Normal);
            this.CreateSolvabilityRow(rows, Labels.ResultModel_CreateSolvencyRows_Core_Equity, solvency.CoreEquity,
                                      FormattingType.Normal);
            this.CreateSolvabilityRow(rows, Labels.ResultModel_CreateSolvencyRows_Tiers_1, solvency.Tiers1,
                                      FormattingType.Normal);
            this.CreateSolvabilityRow(rows, Labels.ResultModel_CreateSolvencyRows_Regulatory_Capital,
                                      solvency.RegulatoryCapital, FormattingType.Normal);
            this.CreateSolvabilityRow(rows, Labels.ResultModel_CreateSolvencyRows_Solvency_Ratio_on_Core_Equity,
                                      solvency.SolvencyRatioOnCoreEquity, FormattingType.Percentage);
            this.CreateSolvabilityRow(rows, Labels.ResultModel_CreateSolvencyRows_Solvency_Ratio_Tiers_1,
                                      solvency.SolvencyRatioTiers1, FormattingType.Percentage);
            this.CreateSolvabilityRow(rows, Labels.ResultModel_CreateSolvencyRows_Solvency_Ratio, solvency.SolvencyRatio,
                                      FormattingType.Percentage);
        }

        private void CreateLcrRows(List<ResultRowModel> rows)
        {
            Lcr lcr = this.ScenarioData.Lcr;
            ResultRowModel liquidAssetsL1Row = new ResultRowModel(this.currentRowId++,
                                                                  Labels.ResultModel_CreateLcrRows_L1_Liquid_assets,
                                                                  FormattingType.Normal, this.ValueColumnsCount);
            ResultRowModel liquidAssetsL2Row = new ResultRowModel(this.currentRowId++,
                                                                  Labels.ResultModel_CreateLcrRows_L2_Liquid_assets,
                                                                  FormattingType.Normal, this.ValueColumnsCount);
            ResultRowModel liquidAssetsL2ARow = new ResultRowModel(this.currentRowId++,
                                                                  "L2A Liquid assets",
                                                                  FormattingType.Normal, this.ValueColumnsCount);
            ResultRowModel liquidAssetsL2BRow = new ResultRowModel(this.currentRowId++,
                                                                  "L2B Liquid assets",
                                                                  FormattingType.Normal, this.ValueColumnsCount);

            liquidAssetsL2Row.Children.Add(liquidAssetsL2ARow);
            liquidAssetsL2Row.Children.Add(liquidAssetsL2BRow);

            ResultRowModel liquidAssetsRow = new ResultRowModel(this.currentRowId++,
                                                                Labels.ResultModel_CreateLcrRows_Liquid_assets,
                                                                FormattingType.Header, this.ValueColumnsCount);
            ResultRowModel netCashOutflowsInRow = new ResultRowModel(this.currentRowId++,
                                                                     Labels.ResultModel_CreateLcrRows_Cash_Inflows,
                                                                     FormattingType.Normal, this.ValueColumnsCount);
            ResultRowModel netCashOutflowsOutRow = new ResultRowModel(this.currentRowId++,
                                                                      Labels.ResultModel_CreateLcrRows_Cash_Outflows,
                                                                      FormattingType.Normal, this.ValueColumnsCount);
            ResultRowModel netCashOutflowsRow = new ResultRowModel(this.currentRowId++,
                                                                   Labels.ResultModel_CreateLcrRows_Net_Cash_Outflows,
                                                                   FormattingType.Header, this.ValueColumnsCount);
            ResultRowModel liquidityCoverageRatioRow = new ResultRowModel(this.currentRowId++,
                                                                          Labels.
                                                                              ResultModel_CreateLcrRows_Liquidity_Coverage_Ratio_,
                                                                          FormattingType.Header, this.ValueColumnsCount);
            liquidityCoverageRatioRow.ResultType = ResultType.LCR;
            for (int i = 0; i < this.ValueColumnsCount; i++)
            {
                liquidAssetsL1Row[i] = this.resultAggregator.DateIntervalValue(lcr.LiquidAssetsL1, i);
                liquidAssetsL2ARow[i] = this.resultAggregator.DateIntervalValue(lcr.LiquidAssetsL2A, i);
                liquidAssetsL2BRow[i] = this.resultAggregator.DateIntervalValue(lcr.LiquidAssetsL2B, i);

                liquidAssetsL2Row[i] = (double)liquidAssetsL2ARow[i] + (double)liquidAssetsL2BRow[i];

                netCashOutflowsInRow[i] = this.resultAggregator.DateIntervalValue(lcr.NetCashOutflowsIn, i);
                netCashOutflowsOutRow[i] = this.resultAggregator.DateIntervalValue(lcr.NetCashOutflowsOut, i);

                double liquidAssets = this.resultAggregator.DateIntervalValue(lcr.LiquidAssets, i);
                double netCashOutflows = this.resultAggregator.DateIntervalValue(lcr.NetCashOutflows, i);

                liquidAssetsRow[i] = liquidAssets;
                netCashOutflowsRow[i] = netCashOutflows;

                if (Util.IsNotZero(netCashOutflows))
                    liquidityCoverageRatioRow[i] = liquidAssets / netCashOutflows;
            }

            rows.Add(liquidAssetsRow);
            liquidAssetsRow.Children.Add(liquidAssetsL1Row);
            liquidAssetsRow.Children.Add(liquidAssetsL2Row);
            rows.Add(netCashOutflowsRow);
            netCashOutflowsRow.Children.Add(netCashOutflowsOutRow);
            netCashOutflowsRow.Children.Add(netCashOutflowsInRow);
            rows.Add(liquidityCoverageRatioRow);
        }

        private void CreateDynamicVariableRows(List<ResultRowModel> rows)
        {
            using (ISession session = SessionManager.OpenSession())
            {
                if (!Param.GetShowDynamicVariablesInResult(session)) return;
            }

            Dictionary<string, double[]> dynamicVariables = this.scenarioData.GetDynamicVariables();

            if (dynamicVariables == null || dynamicVariables.Count == 0) return;

            ResultRowModel root = new ResultRowModel(this.currentRowId++,
                                                                  Labels.ResultModel_CreateDynamicVariableRows_Dynamic_Variables,
                                                                  FormattingType.Header, this.ValueColumnsCount);

            foreach (KeyValuePair<string, double[]> pair in dynamicVariables)
            {
                ResultRowModel rm = new ResultRowModel(this.currentRowId++, pair.Key, FormattingType.Normal, this.ValueColumnsCount);

                for (int i = 0; i < this.ValueColumnsCount; i++)
                {
                    rm[i] = this.resultAggregator.DateIntervalValue(pair.Value, i);
                }

                root.Children.Add(rm);
            }

            rows.Add(root);
        }

        private void CreateSolvabilityRow(List<ResultRowModel> rows, string name, double[] values,
                                          FormattingType resType)
        {
            ResultRowModel rm = new ResultRowModel(this.currentRowId++, name, resType, this.ValueColumnsCount);

            for (int i = 0; i < this.ValueColumnsCount; i++)
                rm[i] = this.resultAggregator.DateIntervalValue(values, i);

            rows.Add(rm);
        }

        private void CreateGainLosesOnFinancialInstrument(List<ResultRowModel> data)
        {
            ResultRowModel all = new ResultRowModel(this.currentRowId++,
                                                    Labels.
                                                        ResultModel_CreateGainLosesOnFinancialInstrument_Gain_Losses_on_Financial_Instrument,
                                                    FormattingType.Header, this.ValueColumnsCount);

            ResultRowModel dividends = new ResultRowModel(this.currentRowId++, "Dividends on Equity",
                                                          FormattingType.Normal, this.ValueColumnsCount);
            double[] dividendsOnEquity = this.ScenarioData.GetDividendsOnEquity();
            for (int i = 1; i < this.ValueColumnsCount; i++)
                dividends[i] = this.resultAggregator.DateIntervalSum(dividendsOnEquity, i);
            all.Children.Add(dividends);

            ResultRowModel gainAndLoses = new ResultRowModel(this.currentRowId++, "Other Gain & Losses",
                                                             FormattingType.Normal, this.ValueColumnsCount);
            double[] gainLosesOnFinancialInstrument = this.ScenarioData.GetGainLosesOnFinancialInstrument();
            for (int i = 1; i < this.ValueColumnsCount; i++)
            {
                gainAndLoses[i] = this.resultAggregator.DateIntervalSum(gainLosesOnFinancialInstrument, i);
                all[i] = (double)gainAndLoses[i] + (double)dividends[i];
            }
            gainAndLoses.SourceMouvements = this.ScenarioData.GainLosesSourceMouvements;


            all.Children.Add(gainAndLoses);

            ResultRowModel fx_PNL = new ResultRowModel(this.currentRowId++, "FX P&L",
                                                                         FormattingType.Normal, this.ValueColumnsCount);
            double[] fx_PNL_values = this.ScenarioData.GetFX_PnL();
            for (int i = 1; i < this.ValueColumnsCount; i++)
            {
                fx_PNL[i] = this.resultAggregator.DateIntervalSum(fx_PNL_values, i);
                all[i] = (double)all[i] + (double)fx_PNL_values[i];
            }
            all.Children.Add(fx_PNL);

            data.Add(all);
        }

        private void CreateBalanceSheetRows(IList<ResultRowModel> gridData, ResultSection section)
        {
            ResultRowModel totalSummary;
            //Category root = this.categories.FirstOrDefault(c => c.Parent == null);
            Category root = this.categories.FirstOrDefault(c => c.BalanceType == BalanceType.BalanceSheet);
            if (root == null) return;

            Category assetCategory = root.Children.Single(c => c.BalanceType == BalanceType.Asset);
            Category liabilityCategory = root.Children.Single(c => c.BalanceType == BalanceType.Liability);
            Category equityCategory = root.Children.Single(c => c.BalanceType == BalanceType.Equity);

            this.CreateCategoryRows(gridData, assetCategory, section, SyntheseType.TotalBalanceSheet);

            totalSummary = new ResultRowModel(this.currentRowId++,
                                                             GetSectionLabel(section, null) +
                                                             " - Liabilities & Equities", FormattingType.Header,
                                                             this.ValueColumnsCount);
            totalSummary.ResultSection = section;
            gridData.Add(totalSummary);

            double[] liabilityTotals = this.CreateCategoryRows(totalSummary.Children, liabilityCategory, section,
                                                               SyntheseType.None);
            double[] equityTotals = this.CreateCategoryRows(totalSummary.Children, equityCategory, section,
                                                            SyntheseType.TotalEquity);

            for (int i = 0; i < this.ValueColumnsCount; i++)
            {
                double sum = liabilityTotals[i] + equityTotals[i];
                if (section == ResultSection.Income)
                {
                    sum += this.resultAggregator.DateIntervalSum(this.ScenarioData.InterestPayedOnDerivatives, i);
                }
                totalSummary[i] = sum;
            }




        }




        private void CreateAnalysisCategoriesWithNetAnalysisIncome(IList<ResultRowModel> gridData)
        {

            //ResultLine newIncomeAnalysisLine = new ResultLine("Net Interest Analysis", "Net Interest Analysis", FormattingType.Header);
            ResultRowModel analysisHeader = new ResultRowModel(this.currentRowId++,
                                                               GetSectionLabel(ResultSection.Income, null, true),
                                                               FormattingType.Header,
                                                               this.ValueColumnsCount);
            analysisTreasury = new double[resultMonths];
            double[] sum = new double[resultMonths];

            string[] allAnalysisCategories = Category.GetAllAnalysisCategories(null);
            double[] values;


            foreach (string analysisCategory in allAnalysisCategories)
            {
                values = this.CreateAnalysisCategoryRows(analysisHeader, analysisCategory, ResultSection.Income,
                                                         SyntheseType.TotalBalanceSheet);

                for (int i = 0; i < this.ValueColumnsCount; i++)
                {
                    sum[i] += values[i];
                }
            }

            //newIncomeAnalysisLine.Children.Add();
            analysisHeader.ResultSection = ResultSection.Income;
            gridData.Add(analysisHeader);


            for (int i = 0; i < this.ValueColumnsCount; i++)
            {
                analysisHeader[i] = sum[i];
            }

        }

        private void CreateGapLines(IList<ResultRowModel> gridData, int cashflowType)
        {

            //ResultLine newIncomeAnalysisLine = new ResultLine("Net Interest Analysis", "Net Interest Analysis", FormattingType.Header);
            ResultRowModel gapActualizedHeader = new ResultRowModel(this.currentRowId++,
                                                               "ACTUALIZED GAP 1%",
                                                               FormattingType.Header,
                                                               this.ValueColumnsCount);
            ResultRowModel gapOnEquityHeader = new ResultRowModel(this.currentRowId++,
                                                               "STATIC GAP / EQUITY",
                                                               FormattingType.Header,
                                                               this.ValueColumnsCount);

            ResultRowModel gapHeader = new ResultRowModel(this.currentRowId++,
                                                               "TOTAL GAP",
                                                               FormattingType.Header,
                                                               this.ValueColumnsCount);
            double[] sum = new double[resultMonths];


            double[] values;
            double[] actualizesGaps = new double[resultMonths];
            double[] gapOnEquityGaps = new double[resultMonths];
            double coef;

            for (int gapLine = 0; gapLine <= resultMonths; gapLine++)
            {
                values = this.CreateGap(gapLine, gapHeader, SyntheseType.TotalBalanceSheet);
                coef = Math.Pow((1.0 + scenarioData.GapActuarialRate[Math.Min(gapLine, resultMonths - 1)]), -1 * gapLine / 12.0);
                if (cashflowType == Param.GAP_STOCK)
                {
                    for (int col = 0; col < this.ValueColumnsCount; col++)
                    {
                        sum[col] += values[col] / -12;
                        actualizesGaps[col] += values[col] / -12 * coef / 100;
                        
                    }
                }

            }
            for (int col = 0; col < this.ValueColumnsCount; col++)
            {
                double regulatoryCapital = this.resultAggregator.DateIntervalValue(this.ScenarioData.Solvency.RegulatoryCapital, col);
                if (regulatoryCapital!=0)
                    gapOnEquityGaps[col] = actualizesGaps[col] / regulatoryCapital;
            }

            if (cashflowType == Param.GAP_STOCK)
            {
                gridData.Add(gapActualizedHeader);
                gridData.Add(gapOnEquityHeader);
            }
            gridData.Add(gapHeader);

            for (int col = 0; col < this.ValueColumnsCount; col++)
            {
                //if (i > 0)
                //{
                //    gapHeader[i - 1] = sum[i];
                //    gapActualizedHeader[i - 1] = actualizesGaps[i];
                //}
                gapHeader[col] = sum[col];
                gapActualizedHeader[col] = actualizesGaps[col];
                gapOnEquityHeader[col] = gapOnEquityGaps[col];

            }

        }

        private double[] CreateGap(int step, ResultRowModel headerRow, SyntheseType syntheseType)
        {
            //string type = GetSectionLabel(section, null, true);

            ResultRowModel headerSummary;
            if (step < resultMonths)
                headerSummary = new ResultRowModel(this.currentRowId++,
                                                              "GAP month " + step.ToString(),
                                                              FormattingType.Normal, this.ValueColumnsCount);
            else
                headerSummary = new ResultRowModel(this.currentRowId++,
                                                              "Residual Gap",
                                                              FormattingType.Normal, this.ValueColumnsCount);
            headerSummary.Category = null;
            headerSummary.SyntheseType = syntheseType;

            double[] gapsValues;

            gapsValues = this.GetGapsValues(step);

            for (int i = 0; i < gapsValues.Length; i++)
            {
                //if (i > 0) headerSummary[i - 1] = gapsValues[i];
                headerSummary[i] = gapsValues[i];
            }

            headerRow.Children.Add(headerSummary);
            return gapsValues;
        }

        private double[] CreateAnalysisCategoryRows(ResultRowModel headerRow, string analysisCategory, ResultSection section, SyntheseType syntheseType)
        {
            //string type = GetSectionLabel(section, null, true);

            ResultRowModel headerSummary = new ResultRowModel(this.currentRowId++,
                                                              string.Format("{0}", analysisCategory),
                                                              FormattingType.Normal, this.ValueColumnsCount);

            headerSummary.ResultSection = section;
            headerSummary.Category = null;
            headerSummary.SyntheseType = syntheseType;


            double[] income;

            income = this.GetAnalyseCategoryValuesBySection(analysisCategory);

            for (int i = 0; i < income.Length; i++)
            {
                headerSummary[i] = income[i];
            }

            headerRow.Children.Add(headerSummary);
            return income;
        }


        private double[] CreateCategoryRows(IList<ResultRowModel> gridData, Category category, ResultSection section, SyntheseType syntheseType)
        {
            string type = GetSectionLabel(section, category);

            ResultRowModel headerSummary = new ResultRowModel(this.currentRowId++,
                                                              string.Format("{0}", type + " - " + category.Label),
                                                              FormattingType.Header, this.ValueColumnsCount);

            headerSummary.ResultSection = section;
            headerSummary.Category = category;
            headerSummary.SyntheseType = syntheseType;

            if (section == ResultSection.BookValue)
            {
                headerSummary.ResultType = ResultType.BookValue;
            }
            else if (section == ResultSection.Yield)
            {
                headerSummary.ResultType = ResultType.Yield;
            }

            gridData.Add(headerSummary);

            this.FillGrid(section, category, headerSummary.Children);

            if (section == ResultSection.Income)
            {
                if (category.BalanceType == BalanceType.Asset)
                {
                    this.AddInterestOnDerivatives(category.BalanceType, headerSummary.Children,
                        Labels.ResultModel_AddInterestReceivedOnDerivarives_Interest_Received_On_Derivatives,
                        this.ScenarioData.InterestReceivedOnDerivatives);
                }
                else if (category.BalanceType == BalanceType.Liability)
                {
                    this.AddInterestOnDerivatives(category.BalanceType, headerSummary.Children,
                        Labels.ResultModel_CreateCategoryRows_Interest_Payed_On_Derivatives,
                        this.ScenarioData.InterestPayedOnDerivatives);
                }
            }

            double[] values = this.GetCategoryValuesBySection(category, section);

            for (int i = 0; i < values.Length; i++)
            {
                double value = values[i];

                if (section == ResultSection.Income)
                {
                    if (category.BalanceType == BalanceType.Asset)
                    {
                        value += this.resultAggregator.DateIntervalSum(this.ScenarioData.InterestReceivedOnDerivatives, i);
                    }
                    else if (category.BalanceType == BalanceType.Liability)
                    {
                        value += this.resultAggregator.DateIntervalSum(this.ScenarioData.InterestPayedOnDerivatives, i);
                    }
                }

                headerSummary[i] = value;
            }

            return values;
        }

        private void AddInterestOnDerivatives(BalanceType balanceType, IList<ResultRowModel> rows, string label, double[] values)
        {
            ResultRowModel rm = new ResultRowModel(this.currentRowId++,
                                                   label,
                                                   FormattingType.Normal, this.ValueColumnsCount);

            Category category = new Category();
            //no ALMID needed
            category.Label = label;
            category.BalanceType = balanceType;
            category.Type = balanceType == BalanceType.Asset ? CategoryType.InterestReceivedOnDerivatives : CategoryType.InterestPayedOnDerivatives;
            rm.Category = category;

            for (int i = 1; i < this.ValueColumnsCount; i++)
                rm[i] = this.resultAggregator.DateIntervalSum(values, i);

            rows.Add(rm);
        }

        private static string GetSectionLabel(ResultSection section, Category category, bool isAnalysisSpecific = false)
        {
            string type;
            switch (section)
            {
                case ResultSection.BookValue:
                    type = Labels.ResultModel_GetSectionLabel_Book_Values;
                    break;
                case ResultSection.Income:
                    if (category == null && isAnalysisSpecific)
                    {
                        type = "Net Interest & Div Analysis";
                    }
                    else
                    {
                        if (category != null && category.BalanceType == BalanceType.OffBalanceSheet)
                        {
                            type = Labels.ResultModel_GetSectionLabel_Off_Balance_Interest;
                        }
                        else
                        {
                            if (category != null && category.BalanceType == BalanceType.Asset)
                                type = Labels.ResultModel_GetSectionLabel_Interest_Income;
                            else
                                type = Labels.ResultModel_GetSectionLabel_Interest_Expenses;
                        }
                    }
                    break;
                case ResultSection.Yield:
                    type = Labels.ResultModel_GetSectionLabel_Yield;
                    break;
                default:
                    throw new ArgumentOutOfRangeException("section");
            }
            return type;
        }

        private void FillGrid(ResultSection section, Category cat, IList<ResultRowModel> gridData, bool isInterest)
        {
            foreach (Category subCat in cat.ChildrenOrdered)
            {
                if (!isInterest)
                {

                    ResultRowModel rm = new ResultRowModel(this.currentRowId++, subCat.Label, FormattingType.Normal,
                                                           this.ValueColumnsCount);

                    rm.Category = subCat;
                    rm.ResultSection = section;
                    if (subCat.Type == CategoryType.Treasury)
                        rm.SourceMouvements = this.ScenarioData.TreasurySourceMouvements;

                    double[] values = this.GetCategoryValuesBySection(subCat, section);

                    for (int i = 0; i < this.ValueColumnsCount; i++)
                        rm[i] = values[i];

                    gridData.Add(rm);

                    this.FillGrid(section, subCat, rm.Children, isInterest);
                }
            }
        }

        private void FillGrid(ResultSection section, Category cat, IList<ResultRowModel> gridData)
        {
            this.FillGrid(section, cat, gridData, false);
        }

        private double[] GetAnalyseCategoryValuesBySection(string analysisCategory)
        {
            double[] result = new double[this.ValueColumnsCount];

            for (int i = 1; i < this.ValueColumnsCount; i++)
            {
                result[i] = this.resultAggregator.DateIntervalSum(this.ScenarioData.AnalysisCategoryIncome[analysisCategory], i);
            }

            return result;

        }
        private double[] GetGapsValues(int step)
        {

            double[] result = new double[this.ValueColumnsCount];
            result[0] = this.ScenarioData.Gaps[step][0];
            for (int i = 1; i < this.ValueColumnsCount; i++)
            {
                result[i] = this.resultAggregator.DateIntervalValue(this.ScenarioData.Gaps[step], i);
            }

            return result;

        }


        private double[] GetCategoryValuesBySection(Category category, ResultSection section)
        {
            double[] allValues;
            double[] values = new double[this.ValueColumnsCount];

            switch (section)
            {
                case ResultSection.BookValue:
                    allValues = this.ScenarioData.GetBookValue(category);
                    values[0] = allValues[0];
                    for (int i = 1; i < this.ValueColumnsCount; i++)
                        values[i] = this.resultAggregator.DateIntervalValue(allValues, i);
                    break;
                case ResultSection.Income:
                    allValues = this.ScenarioData.GetIncome(category);
                    for (int i = 1; i < this.ValueColumnsCount; i++)
                        values[i] = this.resultAggregator.DateIntervalSum(allValues, i);
                    break;
                case ResultSection.Yield:
                    double[] income = this.ScenarioData.GetIncome(category);
                    double[] allBookValues = this.ScenarioData.GetBookValue(category);
                    for (int i = 1; i < this.ValueColumnsCount; i++)
                    {
                        double bookValue = this.resultAggregator.DateIntervalSum(allBookValues, i, 1);
                        double incomeSum = this.resultAggregator.DateIntervalSum(income, i);

                        try
                        {
                            if (Util.IsNotZero(bookValue))
                            {
                                values[i] = incomeSum / bookValue * 100;
                                int diff = 1; //this.dateIntervals[i].MonthEnd - this.dateIntervals[i].MonthStart + 1;
                                values[i] = values[i] * 12.0 / diff;
                            }
                        }
                        catch (OverflowException)
                        {
                            //MTU : do nothing, as in bookValue==0
                        }
                    }
                    break;
                default:
                    throw new ArgumentOutOfRangeException("section");
            }

            return values;
        }

        private void CreateCostIcomeRatio(List<ResultRowModel> rows)
        {
            double[] cost = new double[this.resultMonths];
            double[] commisions = new double[this.resultMonths];

            double[] gainLosesOnFinancialInstrument = this.ScenarioData.GetGainLosesOnFinancialInstrument();
            double[] netInterestIncome = this.ScenarioData.GetNetInterestIncome();

            IDictionary<OtherItemAssum, double[]> oia = this.ScenarioData.GetOtherItemAssumptionsNotImpairments();

            KeyValuePair<OtherItemAssum, double[]>[] costOia =
                oia.Where(pair => pair.Key.SubtotalGroup == OtherItemAssum.CostGroup).ToArray();
            KeyValuePair<OtherItemAssum, double[]>[] commisionsOia =
                oia.Where(pair => pair.Key.SubtotalGroup == OtherItemAssum.CommisionsAndOthersGroup).ToArray();

            for (int i = 0; i < this.resultMonths; i++)
            {
                if (costOia != null && costOia.Length >= 0)
                {
                    double sum = costOia.Sum(t => t.Value[i]);
                    cost[i] = sum;
                }

                if (commisionsOia != null && commisionsOia.Length >= 0)
                {
                    double sum = commisionsOia.Sum(t => t.Value[i]);
                    commisions[i] = sum;
                }
            }

            ResultRowModel rm = new ResultRowModel(this.currentRowId++,
                                                   Labels.ResultModel_CreateCostIcomeRation_Cost_Income_Ratio,
                                                   FormattingType.Percentage, this.ValueColumnsCount);
            rm.ResultType = ResultType.CostIncomeRatio;
            for (int i = 1; i < this.ValueColumnsCount; i++)
            {
                double a = this.resultAggregator.DateIntervalSum(cost, i);
                double b = this.resultAggregator.DateIntervalSum(commisions, i) + this.resultAggregator.DateIntervalSum(gainLosesOnFinancialInstrument, i) +
                            this.resultAggregator.DateIntervalSum(netInterestIncome, i);

                if (Util.IsNotZero(b))
                    rm[i] = Math.Abs(a / b);
            }

            rows.Add(rm);
        }

        private void CreateOtherItemAssuptions(List<ResultRowModel> gridData)
        {
            IDictionary<OtherItemAssum, double[]> allOia = this.ScenarioData.GetOtherItemAssumptionsNotImpairments();
            bool costExists = false;
            bool commisionExists = false;
            using (ISession session = SessionManager.OpenSession())
            {
                foreach (OtherItemAssum oia in allOia.Keys)
                {
                    session.Refresh(oia);
                }
            }

            string[] subTotalGroupNames = allOia.Keys.Select(o => o.SubtotalGroup).Distinct().ToArray(); // OtherItemAssum.GetAllSubgroups();
            IDictionary<string, int> subTotalGroups = new SortedDictionary<string, int>();

            //bool costExists = false;
            //bool commisionExists = false;

            //foreach (string groupName in subTotalGroupNames)
            //{
            //    if (groupName == OtherItemAssum.CommisionsAndOthersGroup) commisionExists = true;
            //    if (groupName == OtherItemAssum.CostGroup) costExists = true;
            //}

            foreach (string groupName in subTotalGroupNames)
            {
                if (string.IsNullOrEmpty(groupName)) continue;
                int position = 0;
                OtherItemAssum[] itemsInGroup = allOia.Keys.Where(o => o.SubtotalGroup == groupName).ToArray(); // OtherItemAssum.FindBySubtotalGroup(groupName);
                if (itemsInGroup != null && itemsInGroup.Length > 0)
                {
                    position = itemsInGroup.Min(oia => oia.Position);
                }
                subTotalGroups.Add(groupName, position);
            }

            subTotalGroupNames =
                subTotalGroupNames.OrderBy(
                    s => (!string.IsNullOrEmpty(s) && subTotalGroups.ContainsKey(s)) ? subTotalGroups[s] : int.MaxValue)
                    .ToArray();
            ResultRowModel rmt;
            foreach (string subTotalGroup in subTotalGroupNames)
            {
                if (subTotalGroup == OtherItemAssum.CostGroup && !commisionExists)
                {
                    this.CreateNetBankingIncome(gridData);
                    costExists = true;
                }
                if (subTotalGroup != null)
                {
                    rmt = new ResultRowModel(this.currentRowId++, subTotalGroup, FormattingType.Header,
                                                            this.ValueColumnsCount);

                }
                else
                {
                    continue;
                }

                OtherItemAssum[] oias = allOia.Keys.Where(o => o.SubtotalGroup == subTotalGroup).
                    OrderBy(o => o.Position).ToArray(); // OtherItemAssum.FindBySubtotalGroup(subTotalGroup);

                double[] totalVals = new double[this.ValueColumnsCount];

                foreach (OtherItemAssum oia in oias)
                {
                    string name = oia.Descrip;

                    if (!allOia.ContainsKey(oia)) continue;

                    double[] values = allOia[oia];
                    ResultRowModel rm = new ResultRowModel(this.currentRowId++, name, FormattingType.Normal,
                                                           this.ValueColumnsCount);
                    for (int j = 0; j < this.ValueColumnsCount; j++)
                    {
                        double dateIntervalSum = this.resultAggregator.DateIntervalSum(values, j);
                        rm[j] = dateIntervalSum;
                        totalVals[j] += dateIntervalSum;
                    }
                    if (oia.IsVisibleChecked == true)
                        rmt.Children.Add(rm);
                }
                for (int j = 0; j < this.ValueColumnsCount; j++)
                    rmt[j] = totalVals[j];
                gridData.Add(rmt);

                if (subTotalGroup == OtherItemAssum.CommisionsAndOthersGroup && !costExists)
                {
                    this.CreateNetBankingIncome(gridData);
                    commisionExists = true;
                }
            }
            if (!costExists && !commisionExists)
            {
                this.CreateNetBankingIncome(gridData);
            }
        }
    }


    public enum ResultSection
    {
        BookValue,
        Income,
        Yield
    }
}