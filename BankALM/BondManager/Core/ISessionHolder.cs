using NHibernate;

namespace BondManager.Core
{
    public interface ISessionHolder
    {
        ISession Session { get; }
        void SaveChangesInSession();
    }
}