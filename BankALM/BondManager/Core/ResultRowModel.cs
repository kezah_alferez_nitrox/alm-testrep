using System.Collections.Generic;
using BondManager.Calculations;
using BondManager.Domain;

namespace BondManager.Core
{
    public enum FormattingType
    {
        None,
        Normal,
        MasterHeader,
        PeriodHeader,
        Header,
        WhiteHeader,
        Footer,
        MasterFooter,
        Percentage
    }

    public enum SyntheseType
    {
        None,
        NetIncome,
        TotalBalanceSheet,
        TotalEquity
    }

    public class ResultRowModel
    {
        private readonly int id;
        private readonly object[] rowValues;

        public ResultSection? ResultSection { get; set; }
        public SyntheseType SyntheseType { get; set; }
        public ResultType ResultType { get; set; }
        public Mouvements[] SourceMouvements { get; set; }

        public ResultRowModel(int id, string description, FormattingType type, int valueCount)
        {
            this.id = id;
            this.ValueCount = valueCount;
            this.rowValues = new object[valueCount + 2];
            this.Type = type;
            this.Description = description;            

            this.Children = new List<ResultRowModel>();
        }

        public int ValueCount { get; private set; }

        public object this[int i]
        {
            get { return this.rowValues[i + 2]; }
            set{ this.rowValues[i + 2] = value;}
        }

        public string Description
        {
            get { return (string) this.rowValues[1]; }
            set { this.rowValues[1] = value; }
        }

        public FormattingType Type
        {
            get { return (FormattingType) this.rowValues[0]; }
            set { this.rowValues[0] = value; }
        }

        public object[] RowValues
        {
            get { return this.rowValues; }
            
        }

        public int Length
        {
            get { return rowValues.Length; }
        }

        public Category Category { get; set; }

        public int Id { get { return this.id; } }

        public IList<ResultRowModel> Children { get; set; }
    }

    public enum ResultType
    {
        Unspecified,
        BookValue,
        NetBankingIncome,
        ImpairmentCharge,
        NetIncome,
        CostIncomeRatio,
        ROE,
        Yield,
        LCR,
    }
}