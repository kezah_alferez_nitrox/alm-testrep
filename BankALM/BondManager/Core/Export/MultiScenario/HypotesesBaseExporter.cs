using System;
using System.Collections;
using System.Collections.Generic;
using BankALM.Infrastructure.Data;
using BondManager.Calculations;
using BondManager.Domain;
using BondManager.Resources.Language;
using Eu.AlterSystems.ASNetLib.Core;
using NHibernate;
using Excel = Microsoft.Office.Interop.Excel;

namespace BondManager.Core.Export.MultiScenario
{
    public class HypotesesBaseExporter
    {
        private readonly Excel.Worksheet worksheet;
        private readonly IDictionary<Scenario, ResultModel> simulation;
        private readonly ReportAggregationType aggregationType;
        private readonly Variable[] variables;

        private class BondData
        {
            public string Label { get; set; }
            public Product Product { get; set; }

            public BondData(string label, Product product)
            {
                this.Label = label;
                this.Product = product;
            }
        }

        public HypotesesBaseExporter(ISession session, Excel.Worksheet worksheet, IDictionary<Scenario, ResultModel> simulation, ReportAggregationType aggregationType)
        {
            this.worksheet = worksheet;
            this.simulation = simulation;
            this.aggregationType = aggregationType;

            this.variables = Variable.FindAll(session);
        }

        public void Export()
        {
            using (ISession session = SessionManager.OpenSession())
            {
                this.PrepareCells();

                IList<BondData> bondDatas = new List<BondData>();
                Category root = Category.FindPortfolio(session);
                FillBonds(root, bondDatas, 0);

                ExcelUtil.InsertRows(worksheet, 3, bondDatas.Count);

                for (int i = 0; i < bondDatas.Count; i++)
                {
                    ExcelUtil.SetCellValue(worksheet, 3 + i, 1, bondDatas[i].Label);
                }


                int scenarioNo = 0;
                foreach (Scenario scenario in simulation.Keys)
                {
                    ExcelUtil.SetCellValue(worksheet, 1, 2 + scenarioNo*8,
                                           Labels.HypotesesBaseExporter_Export_SCENARIO_ + scenario.Name + " (" +
                                           aggregationType.ToString() + ")");

                    for (int i = 0; i < bondDatas.Count; i++)
                    {
                        Product product = bondDatas[i].Product;
                        if (bondDatas[i].Product == null) continue;

                        ExcelUtil.SetCellValue(worksheet, 3 + i, 0 + 2 + scenarioNo*8,
                                               ReplaceVariables(product.Coupon, scenario));
                        ExcelUtil.SetCellValue(worksheet, 3 + i, 1 + 2 + scenarioNo*8,
                                               EnumHelper.GetDescription(product.AmortizationType));
                        ExcelUtil.SetCellValue(worksheet, 3 + i, 2 + 2 + scenarioNo*8,
                                               ReplaceVariables(product.CPR, scenario));
                        ExcelUtil.SetCellValue(worksheet, 3 + i, 3 + 2 + scenarioNo*8,
                                               ReplaceVariables(product.CDR, scenario));
                        ExcelUtil.SetCellValue(worksheet, 3 + i, 4 + 2 + scenarioNo*8,
                                               ReplaceVariables(product.LGD, scenario));
                        ExcelUtil.SetCellValue(worksheet, 3 + i, 5 + 2 + scenarioNo*8,
                                               string.IsNullOrEmpty(product.RollSpec) ? "n" : "y");
                        ExcelUtil.SetCellValue(worksheet, 3 + i, 6 + 2 + scenarioNo*8,
                                               Convert.ToString(EnumHelper.GetDescription(product.Frequency)[0]));
                        ExcelUtil.SetCellValue(worksheet, 3 + i, 7 + 2 + scenarioNo*8,
                                               product.Duration ?? Param.GetDefaultBondDuration(session));
                    }

                    scenarioNo++;
                }
            }
        }

        private string ReplaceVariables(string formula, Scenario scenario)
        {
            if (string.IsNullOrEmpty(formula)) return formula;

            foreach (Variable variable in variables)
            {
                if (formula.IndexOf(variable.Name, StringComparison.InvariantCultureIgnoreCase) >= 0)
                {
                    string value = string.Empty;
                    foreach (VariableValue variableValue in variable.VariableValues)
                    {
                        if (variableValue.Scenario.Id == scenario.Id)
                        {
                            value = variableValue.Value;
                            break;
                        }
                    }

                    if (value != null)
                    {
                        formula = formula.Replace(variable.Name, value);
                    }
                }
            }
            return formula;
        }

        private void FillBonds(Category category, IList<BondData> bonds, int level)
        {
            foreach (Category childCategory in category.ChildrenOrdered)
            {
                string label = new string(' ', level * 3) + childCategory.Label;
                Product[] prods = ProductCache.products.FindAll(p=>p.Category.ALMID==childCategory.ALMID).ToArray();
               if(prods.Length>0) //if (childCategory.Bonds.Count > 0)
                {
                    bonds.Add(new BondData(label, prods[0]));
                }
                else
                {
                    bonds.Add(new BondData(label, null));
                    this.FillBonds(childCategory, bonds, level + 1);
                }
            }
        }

        private void PrepareCells()
        {
            ExcelUtil.InsertColumns(this.worksheet, 10, 8 * (this.simulation.Count - 1));
            for (int i = 1; i < this.simulation.Count; i++)
            {
                ExcelUtil.CopyColumns(this.worksheet, 2, 8, 2 + i * 8);
            }
        }
    }
}