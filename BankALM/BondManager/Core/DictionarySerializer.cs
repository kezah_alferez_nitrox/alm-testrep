using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace BondManager.Core
{
    public class DictionarySerializer<TK, TV>
    {
        public struct SerializableKeyValuePair
        {
            public TK Key;
            public TV Value;
            public SerializableKeyValuePair(KeyValuePair<TK, TV> kvp)
            {
                this.Key = kvp.Key;
                this.Value = kvp.Value;
            }
        }

        private readonly XmlSerializer serializer = new XmlSerializer(typeof(List<SerializableKeyValuePair>));

        public void Serialize(IDictionary<TK, TV> dictionary, XmlWriter serializationStream)
        {
            this.serializer.Serialize(serializationStream, BuildItemList(dictionary));
        }
        public void Serialize(IDictionary<TK, TV> dictionary, TextWriter serializationStream)
        {
            this.serializer.Serialize(serializationStream, BuildItemList(dictionary));
        }
        public void Serialize(IDictionary<TK, TV> dictionary, Stream serializationStream)
        {
            this.serializer.Serialize(serializationStream, BuildItemList(dictionary));
        }

        public static IList<SerializableKeyValuePair> BuildItemList(IDictionary<TK, TV> dictionary)
        {
            List<SerializableKeyValuePair> list = new List<SerializableKeyValuePair>();
            foreach (KeyValuePair<TK, TV> nonSerializableKvp in dictionary)
            {
                list.Add(new SerializableKeyValuePair(nonSerializableKvp));
            }

            return list;
        }

        public IDictionary<TK, TV> Deserialize(XmlReader serializationStream)
        {
            List<SerializableKeyValuePair> dictionaryItems = this.serializer.Deserialize(serializationStream) as List<SerializableKeyValuePair>;
            return BuildDictionary(dictionaryItems);
        }
        public IDictionary<TK, TV> Deserialize(TextReader serializationStream)
        {
            List<SerializableKeyValuePair> dictionaryItems = this.serializer.Deserialize(serializationStream) as List<SerializableKeyValuePair>;
            return BuildDictionary(dictionaryItems);
        }
        public IDictionary<TK, TV> Deserialize(Stream serializationStream)
        {
            List<SerializableKeyValuePair> dictionaryItems = this.serializer.Deserialize(serializationStream) as List<SerializableKeyValuePair>;
            return BuildDictionary(dictionaryItems);
        }

        private static Dictionary<TK, TV> BuildDictionary(ICollection<SerializableKeyValuePair> dictionaryItems)
        {
            Dictionary<TK, TV> dictionary = new Dictionary<TK, TV>(dictionaryItems.Count);
            foreach (SerializableKeyValuePair item in dictionaryItems)
            {
                dictionary.Add(item.Key, item.Value);
            }

            return dictionary;
        }
    }
}