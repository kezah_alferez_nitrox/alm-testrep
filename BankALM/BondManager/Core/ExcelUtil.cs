using System;
using Microsoft.Office.Interop.Excel;
using System.Diagnostics;

namespace BondManager.Core
{
    public static class ExcelUtil
    {
        public static void InsertRows(Worksheet worksheet, int rowNumber, int count)
        {
            Range source = worksheet.get_Range("A" + (rowNumber + 1), Type.Missing).EntireRow;
            for (int i = 0; i < count; i++)
            {
                source.Insert(XlInsertShiftDirection.xlShiftDown, Type.Missing);
            }
        }

        public static void InsertColumns(Worksheet worksheet, int columnNumber, int count)
        {
            Range source = worksheet.get_Range(ColumnNumberToName(columnNumber) + "1", Type.Missing).EntireColumn;
            for (int i = 0; i < count; i++)
            {
                source.Insert(XlInsertShiftDirection.xlShiftToRight, Type.Missing);
            }
        }

        public static string ColumnNumberToName(int number)
        {
            const int letterCount = 'Z' - 'A' + 1;

            int code0 = number - 1;
            int code1 = code0 / letterCount;
            code0 = code0 % letterCount;

            string c = char.ConvertFromUtf32('A' + code0);
            if (code1 > 0) c = char.ConvertFromUtf32('A' + code1 - 1) + c;
            return c;
        }

        public static void CopyColumns(Worksheet worksheet, int source, int count, int destination)
        {
            Range sourceRange = worksheet.get_Range(ColumnNumberToName(source) + "1", ColumnNumberToName(source + count - 1) + "1").EntireColumn;
            Range destinationRange = worksheet.get_Range(ColumnNumberToName(destination) + "1", ColumnNumberToName(destination + count - 1) + "1").EntireColumn;

            sourceRange.Copy(destinationRange);
        }

        public static void SetCellValue(Worksheet worksheet, int row, int col, object value)
        {
            ((Range)worksheet.Cells[row, col]).Value2 = value;
        }

        public static void Merge(Worksheet worksheet, int startIndex, int count)
        {
            Range range = worksheet.get_Range(ColumnNumberToName(startIndex) + "1", ColumnNumberToName(startIndex + count - 1) + "1");

            range.Merge(true);
            range.HorizontalAlignment = XlHAlign.xlHAlignCenter;
        }

        public static void MakeRowBold(Worksheet worksheet, int row)
        {
            ((Range) worksheet.Cells[row, 1]).EntireRow.Font.Bold = true;
        }
    }
}