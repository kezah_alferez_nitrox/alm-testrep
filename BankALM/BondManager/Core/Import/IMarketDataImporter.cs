using System;
using System.ComponentModel;
using BondManager.Domain;

namespace BondManager.Core.Import
{
    public interface IMarketDataImporter
    {
        bool CanImportFrom(string fileName);
        Vector[] ImportFrom(string fileName, VectorGroup vectorGroup, DateTime startDate, DataDirection dataDirection, BackgroundWorker backgroundWorker);
    }
}