using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using ALMSCommon;
using Microsoft.Office.Interop.Excel;
using BondManager.Domain;
using NHibernate;
using ExcelApp = Microsoft.Office.Interop.Excel.Application;

namespace BondManager.Core.Import
{
    internal class ExcelMarketDataImporter : IMarketDataImporter
    {
        private readonly ISession session;
        private const int FirstRow = 1;
        private const int FirstCol = 1;

        private const int MaxRow = 1000;
        private const string CURRENCY = "Currency";

        private readonly IDictionary<string, Vector> cacheVectors = new SortedDictionary<string, Vector>();

        public ExcelMarketDataImporter(ISession session)
        {
            this.session = session;
        }

        public bool CanImportFrom(string fileName)
        {
            return Path.GetExtension(fileName) == "xls";
        }

        public Vector[] ImportFrom(string fileName, VectorGroup vectorGroup, DateTime startDate, DataDirection dataDirection, BackgroundWorker backgroundWorker)
        {
            List<Vector> vectors = new List<Vector>();

            InitialiseVectorCache();

            // initialize the Excel Application class
            ExcelApp app = null;
            Workbook workBook = null;

            try
            {
                app = new ExcelApp();
                app.Visible = false;

                // create the workbook object by opening the excel file.
                workBook = app.Workbooks.Open(fileName,
                                              0,
                                              true,
                                              5,
                                              "",
                                              "",
                                              true,
                                              XlPlatform.xlWindows,
                                              "\t",
                                              false,
                                              false,
                                              0,
                                              true,
                                              1,
                                              0);

                int count = workBook.Sheets.Count;
                for (int i = 1; i <= count; i++)
                {
                    backgroundWorker.ReportProgress(50 * i / count);
                    Worksheet sheet = (Worksheet)workBook.Sheets[i];
                    IEnumerable<Vector> importFromSheet = ImportFromSheet(sheet, vectorGroup, startDate, dataDirection);

                    if (importFromSheet != null)
                    {
                        Vector[] sheetVectors = importFromSheet.ToArray();
                        FindDuplicateNames(sheetVectors);
                        vectors.AddRange(sheetVectors);
                    }
                    Marshal.ReleaseComObject(sheet);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                Debugger.Break();
#endif
                Log.Exception(fileName, ex);
            }
            finally
            {
                if (null != workBook)
                {
                    workBook.Close(false, Type.Missing, Type.Missing);
                    Marshal.ReleaseComObject(workBook);
                }

                if (null != app)
                {
                    //Close running excel app
                    app.Quit();
                    //Use the Com Object interop marshall to release the excel object
                    Marshal.ReleaseComObject(app);
                }
            }

            return vectors.ToArray();
        }

        private static void FindDuplicateNames(Vector[] array)
        {
            IList<string> duplicateNames = new List<string>();
            for (int x = 0; x < array.Length; x++)
            {
                Vector v1 = array[x];
                for (int y = x + 1; y < array.Length; y++)
                {
                    Vector v2 = array[y];
                    if (v1.Name == v2.Name)
                    {
                        duplicateNames.Add(v1.Name);
                    }
                }
            }

            if (duplicateNames.Count > 0)
            {
                string message = "Cannot import becuse there are some duplicate vector names:\n" + string.Join("\n", duplicateNames.ToArray());

                MessageBox.Show(message, "Market Data Import", MessageBoxButtons.OK, MessageBoxIcon.Error);
                throw new Exception(message);
            }
        }

        private void InitialiseVectorCache()
        {
            cacheVectors.Clear();
            Vector[] allVectors = VectorCache.vectors.ToArray();//Vector.FindAll(this.session);
            foreach (Vector vector in allVectors)
            {
                cacheVectors[vector.Name] = vector;
            }
        }

        private IEnumerable<Vector> ImportFromSheet(Worksheet workSheet, VectorGroup vectorGroup, DateTime startDate, DataDirection dataDirection)
        {
            List<Vector> vectors = new List<Vector>();

            // find first non empty row
            int row = FirstRow - 1;
            Range cell;
            do
            {
                row++;
                cell = GetCell(workSheet, row, FirstCol, dataDirection);
            } while (IsEmptyCell(cell) && row < MaxRow);

            if (row == MaxRow) return null;

            // first non empty row in the header
            int headerRow = row;
            row++;

            // check for currency column
            string colHeader = GetCell(workSheet, headerRow, FirstCol + 1, dataDirection).Value2.ToString();
            bool hasCurrency = string.Compare(colHeader, CURRENCY, StringComparison.InvariantCultureIgnoreCase) == 0;
            int firstCol = hasCurrency ? FirstCol + 2 : FirstCol + 1;

            // check if data columns start with "today"
            colHeader = GetCell(workSheet, headerRow, firstCol, dataDirection).Value2.ToString();
            if (string.Compare(colHeader, "Today", StringComparison.InvariantCultureIgnoreCase) != 0)
            {
                // if first data column != Today we suppose it's 1 month from now
                startDate = startDate.AddMonths(1);
            }

            cell = GetCell(workSheet, row, FirstCol, dataDirection);
            while (!IsEmptyCell(cell))
            {
                Vector vector = GetVectorAtRow(workSheet, firstCol, row, startDate, dataDirection);
                vector.Group = vectorGroup;
                vectors.Add(vector);
                cell = GetCell(workSheet, ++row, FirstCol, dataDirection);
            }

            return vectors;
        }

        private Range GetCell(Worksheet workSheet, int row, int col, DataDirection dataDirection)
        {
            return (Range)(dataDirection == DataDirection.Horizontal
                                ? workSheet.Cells[row, col]
                                : workSheet.Cells[col, row]);
        }

        private Vector GetVectorAtRow(Worksheet sheet, int firstCol, int currentRow, DateTime startDate, DataDirection dataDirection)
        {
            object[,] data = dataDirection == DataDirection.Horizontal ?
                (object[,])((Range)sheet.Rows[currentRow, Type.Missing]).Value2 :
                (object[,])((Range)sheet.Columns[currentRow, Type.Missing]).Value2;

            if (dataDirection == DataDirection.Vertical)
            {
                data = Transpose(data);
            }

            string vectorName = data[1, FirstCol].ToString();
            Vector vector = GetVectorByNameOrCreateNew(vectorName);
            vector.Frequency = VectorFrequency.Monthly;

            vector.VectorPoints.Clear();

            int currentCol = firstCol;
            object cellValue = data[1, currentCol];
            while (cellValue != null)
            {
                double cellDecimalValue;
                try
                {
                    cellDecimalValue = (double)cellValue;
                }
                catch (Exception)
                {
                    int errorRow = dataDirection ==DataDirection.Horizontal ? currentRow : currentCol;
                    string errorColumn = ExcelTools.GetExcelColumnName(dataDirection == DataDirection.Horizontal ? currentCol : currentRow);

                    MessageBox.Show(string.Format("Error in cell {0}{1}: numeric value expected but \"{2}\" found",
                        errorColumn, errorRow, cellValue),
                        "Market Data Import", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    throw;
                }

                VectorPoint vectorPoint = new VectorPoint();
                vectorPoint.Date = startDate.AddMonths(currentCol - firstCol);
                vectorPoint.Value = cellDecimalValue;
                vectorPoint.Vector = vector;

                vector.VectorPoints.Add(vectorPoint);
                currentCol++;
                cellValue = data[1, currentCol];
            }

            return vector;
        }

        private object[,] Transpose(object[,] data)
        {
            int x1 = data.GetLowerBound(0);
            int x2 = data.GetUpperBound(0);
            int y1 = data.GetLowerBound(1);
            int y2 = data.GetUpperBound(1);

            object[,] result = (object[,])Array.CreateInstance(typeof(object), new[] { y2 - y1 + 1, x2 - x1 + 1 },
                                                                new[] {y1, x1 });

            for (int i = x1; i <= x2; i++)
            {
                for (int j = y1; j <= y2; j++)
                {
                    result[j, i] = data[i, j];
                }
            }

            return result;
        }

        private Vector GetVectorByNameOrCreateNew(string name)
        {
            Vector vector;

            if (!cacheVectors.TryGetValue(name, out vector))
            {
                vector = new Vector();
                vector.CreationDate = DateTime.Now;
                vector.Name = name;
            }

            return vector;
        }

        private static bool IsEmptyCell(Range cell)
        {
            return cell.Value2 == null || string.IsNullOrEmpty(cell.Value2.ToString());
        }
    }
}