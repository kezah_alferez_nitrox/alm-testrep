﻿using System;
using System.Collections.Generic;
using System.Text;
using BondManager.Calculations;

namespace BondManager.Core
{
    public class MultiResultModel
    {
        public IDictionary<Calculations.ReportType, ResultModel> ResultModels { get; set; }

        public MultiResultModel()
        {
            ResultModels = new Dictionary<Calculations.ReportType, ResultModel>();
        }
    }
}
