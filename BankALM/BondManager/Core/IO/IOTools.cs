﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BondManager.Domain;

namespace BondManager.Core.IO
{
    public class IOTools
    {
        public static Category[] migrateCategoriesToV2(Category[] initialCategs)
        {
            

            Category portfolio = new Category();
            portfolio.Label = "Portfolio";
            portfolio.Id = initialCategs.Max(c => c.Id) + 1;
            portfolio.BalanceType=BalanceType.Portfolio;

            Category offBS = new Category();
            offBS.Label = "Off Balance Sheet";
            offBS.BalanceType = BalanceType.OffBalanceSheet;
            offBS.ReadOnly = true;
            offBS.Type = CategoryType.Normal;
            offBS.Id = portfolio.Id + 2;
            offBS.Parent = portfolio;
            offBS.IsVisibleChecked = true;
            offBS.isVisible= true;
            offBS.IsVisibleCheckedModel = true;

            Category offBSSwaps = new Category();
            offBSSwaps.Label = "Swaps";
            offBSSwaps.BalanceType = BalanceType.Derivatives;
            offBSSwaps.ReadOnly = true;
            offBSSwaps.Type = CategoryType.Normal;
            offBSSwaps.Id = portfolio.Id + 3;
            offBSSwaps.Parent = offBS;
            offBSSwaps.IsVisibleChecked = true;
            offBSSwaps.isVisible = true;
            offBSSwaps.IsVisibleCheckedModel = true;

            Category offBSCapFloor = new Category();
            offBSCapFloor.Label = "Cap-Floor";
            offBSCapFloor.BalanceType = BalanceType.Derivatives;
            offBSCapFloor.ReadOnly = true;
            offBSCapFloor.Type = CategoryType.Normal;
            offBSCapFloor.Id = portfolio.Id + 4;
            offBSCapFloor.Parent = offBS;
            offBSCapFloor.IsVisibleChecked = true;
            offBSCapFloor.isVisible = true;
            offBSCapFloor.IsVisibleCheckedModel = true;

            Category offBSSwaptions = new Category();
            offBSSwaptions.Label = "Swaptions";
            offBSSwaptions.BalanceType = BalanceType.Derivatives;
            offBSSwaptions.ReadOnly = true;
            offBSSwaptions.Type = CategoryType.Normal;
            offBSSwaptions.Id = portfolio.Id + 5;
            offBSSwaptions.Parent = offBS;
            offBSSwaptions.IsVisibleChecked = true;
            offBSSwaptions.isVisible = true;
            offBSSwaptions.IsVisibleCheckedModel = true;

            IList<Category> categories = new List<Category>(initialCategs);

            Category bs = categories.First(c => c.Parent == null);
            bs.Parent = portfolio;
            bs.BalanceType = BalanceType.BalanceSheet;
            bs.ReadOnly = true;
            bs.Type = CategoryType.Normal;

            categories.Add(portfolio);
            categories.Add(offBS);
            categories.Add(offBSSwaps);
            categories.Add(offBSCapFloor);
            categories.Add(offBSSwaptions);

            return categories.ToArray();
        }
    }
}
