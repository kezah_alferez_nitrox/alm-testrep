using System;
using System.Collections;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using ALMS.Products;
using ALMSCommon;
using ALMSCommon.Forms;
using BankALM.Infrastructure.Data;
using BondManager.BusinessLogic;
using BondManager.Calculations;
using BondManager.Domain;

using Microsoft.Office.Interop.Excel;
using NHibernate;
using NHibernate.Linq;
using Labels = BondManager.Resources.Language.Labels;
using ExcelApp = Microsoft.Office.Interop.Excel.Application;
using System.Text;
using Application = System.Windows.Forms.Application;

namespace BondManager.Core.IO
{
    public class GlobalExportImport
    {
        public static DateTime EXCEL_MIN_DATE = new DateTime(1900, 1, 1).AddDays(-2);
        private const int BondIdColumn = 1;
        private const int CategoryColumn = 2;
        private const int FirstBondColumn = 3;
        private const int MAX_GETALMID_RETRIES = 10;
        private const int IMPORT_FLUSH_EACH = 100;

        private static readonly EntityArrayMapper<Product> BondMapper = new EntityArrayMapper<Product>(
            b => b.ProductType,
            b => b.Description,
            b => b.Exclude,
            b => b.SecId,
            b => b.IsModel,
            b => b.AmortizationType,
            b => b.Amortization,
            b => b.ComplementString,
            b => b.InvestmentRule,
            b => b.InvestmentParameter,
            b => b.Accounting,
            b => b.OrigFace,
            b => b.BookPrice,
            b => b.ActuarialPrice,
            b => b.BookValue,
            b => b.RollSpec,
            b => b.RollRatio,
            b => b.StartDate,
            b => b.Coupon,
            b => b.CouponType,
            b => b.CouponBasis,
            b => b.CouponCalcType,
            b => b.Frequency,
            b => b.Duration,
            b => b.ExpirationDate,
            b => b.Tax,
            b => b.Attrib,
            //b => b.OriginBasedOnStartDate,
            b => b.CPR,
            b => b.CDR,
            b => b.LGD,
            b => b.RecoveryLag,
            b => b.Basel2,
            b => b.Currency,
            b => b.IRC,
            b => b.RWA,
            b => b.LiquidityEligibility,
            b => b.LiquidityHaircut,
            b => b.LiquidityRunOff,
            b => b.SwapLeg,
            b => b.Spread,
            b => b.MarketBasis,
            b => b.FirstRate,
            b => b.OtherSwapLegAlmId,
            b => b.LinkedProductsAlmIds,
            b => b.ApplicationFee,
            b => b.ApplicationCharge,
            b => b.PrePayementFee
        );

        private readonly BackgroundWorker backgroundWorker;
        private Category rootCategory;

        private Currency[] allCurrencies;
        private int totalBondCount;

        public GlobalExportImport(BackgroundWorker backgroundWorker, Category rootCategory = null)
        {
            this.backgroundWorker = backgroundWorker;
            this.rootCategory = rootCategory;
            this.Errors = new List<string>();
        }

        public IList<string> Errors { get; private set; }

        public int AddedProductsCount { get; private set; }
        public int DeletedProductsCount { get; private set; }
        public int ChangedProductsCount { get; private set; }

        public bool Export()
        {
            this.Errors.Clear();

            ExcelApp excel = null;
            Workbook workBook = null;

            try
            {
                excel = new ExcelApp();
                excel.Visible = false;

                workBook = excel.Workbooks.Add(XlWBATemplate.xlWBATWorksheet);

                Worksheet sheet = (Worksheet)workBook.Sheets[1];

                this.Export(sheet);

                Marshal.ReleaseComObject(sheet);
            }
            catch (Exception ex)
            {
                Log.Exception("Global Excel Export", ex);

                this.Errors.Add("Excel error : " + ex.Message);

                return false;
            }
            finally
            {
                if (null != workBook)
                {
                    Marshal.ReleaseComObject(workBook);
                }

                if (null != excel)
                {
                    excel.Visible = true;
                    Marshal.ReleaseComObject(excel);
                }
            }

            return this.Errors.Count == 0;
        }

        private void Export(Worksheet worksheet)
        {
            DateTime start = DateTime.Now;
            int row = 1;
            worksheet.Cells[row, 1] = "BondInternalID";
            worksheet.Cells[row, 2] = "Category";

            for (int i = 0; i < BondMapper.PropertyNames.Length; i++)
            {
                worksheet.Cells[row, FirstBondColumn + i] = BondMapper.PropertyNames[i];
            }

            row++;

            using (ISession session = SessionManager.OpenSession())
            {
                this.totalBondCount = ProductCache.products.Count;//session.Query<Product>().Count();

                Category root;
                if (this.rootCategory != null && this.rootCategory.Id != 0)
                {
                    root = Category.Find(session, this.rootCategory.Id);
                }
                else
                {
                    root = Category.FindPortfolio(session);
                }
                this.Export(worksheet, ref row, root);
            }

            Debug.WriteLine((DateTime.Now - start).TotalSeconds);
        }

        private void Export(Worksheet worksheet, ref int row, Category category)
        {
            if (category == null) return;

            foreach (Category child in category.ChildrenOrdered)
            {
                this.Export(worksheet, ref row, child);
            }

            Product[] prods = ProductCache.products.FindAll(p=>p.Category.ALMID==category.ALMID).ToArray();
           if(prods.Length>0) // if (category.Bonds.Count > 0)
            {
                object[][] table = new object[prods.Length][];

                Type[] typesColonnes = null;

                int maxWidth = 0;
                int firstRow = row;
                for (int index = 0; index < prods.Length; index++)
                {
                    Product product = prods[index];
                    if (!ShouldExport(product))
                    {
                        continue;
                    }

                    product.OtherSwapLegAlmId = product.OtherSwapLeg == null ? "" : product.OtherSwapLeg.ALMIDDisplayed;
                    product.LinkedProductsAlmIds = "";

                    if (product.ProductType == ProductType.Swaption || product.ProductType == ProductType.Swap)
                    {
                        Debug.Assert(product.LinkedProducts != null);
                        product.LinkedProductsAlmIds = string.Join(";", product.LinkedProducts.Select(p => p.ALMIDDisplayed));
                    }

                    object[] bondFields = BondMapper.GetArray(product);

                    table[index] = new object[bondFields.Length + FirstBondColumn - 1];
                    if (table[index].Length > maxWidth) maxWidth = table[index].Length;
                    table[index][0] = product.ALMIDDisplayed;
                    table[index][1] = product.Category.FullPath();

                    if (index == 0)
                    {
                        typesColonnes = new Type[bondFields.Length];
                        for (int i = 0; i < bondFields.Length; i++) if (bondFields[i] != null) typesColonnes[i] = bondFields[i].GetType();
                    }

                    for (int i = 0; i < bondFields.Length; i++)
                        table[index][FirstBondColumn + i - 1] = bondFields[i];

                    row++;

                    this.backgroundWorker.ReportProgress(100 * row / this.totalBondCount);
                }

                object[,] converted = new object[table.Length, maxWidth];
                for (int y = 0; y < table.Length; y++)
                {
                    if (table[y] == null) continue;
                    for (int x = 0; x < table[y].Length; x++) converted[y, x] = ConvertToExcel(table[y][x]);
                }

                if (converted.GetLength(0) > 0 && converted.GetLength(1) > 0)
                {
                    worksheet.Range["A" + firstRow.ToString(),
                        ExcelUtil.ColumnNumberToName(maxWidth) + (row - 1).ToString()].Value2 = converted;
                }

                if (typesColonnes != null)
                {
                    for (int index = 0; index < typesColonnes.Length; index++)
                    {
                        Type type = typesColonnes[index];
                        if (type == typeof(DateTime)) worksheet.Range[ExcelUtil.ColumnNumberToName(index + FirstBondColumn) + "1",
                            ExcelUtil.ColumnNumberToName(index + FirstBondColumn) + "1"].EntireColumn.NumberFormat = "DD/MM/YYYY hh:mm";
                    }
                }
            }
        }

        private static object ConvertToExcel(object value)
        {
            if (value == null)
            {
                return null;
            }

            if (value is string || value is bool) return value;
            else if (value is DateTime) return ((DateTime)value).ToOADate();

            if (value.GetType().IsEnum || value is Currency)
            {
                return value.ToString();
            }

            string toString = Convert.ToString(value);
            return toString;
        }

        private string TryConvertFromExcel(string value, Type targetType, out object converted)
        {
            converted = null;

            if (string.IsNullOrEmpty(value))
            {
                if (targetType.IsValueType && !IsNullable(targetType))
                {
                    return Labels.GlobalExportImport_TryConvertFromExcel_Missing_value;
                }
                return null;
            }

            if (targetType == typeof(string))
            {
                converted = value;
                return null;
            }

            if (IsNullable(targetType))
            {
                Type[] genericArguments = targetType.GetGenericArguments();
                Debug.Assert(genericArguments.Length == 1);
                targetType = genericArguments[0];
            }

            if (targetType.IsEnum)
            {
                try
                {
                    converted = Enum.Parse(targetType, value, true);
                }
                catch (Exception)
                {
                    return Labels.GlobalExportImport_TryConvertFromExcel_Invalid_list_value + ":" + value;
                }
                return null;
            }

            if (targetType == typeof(Currency))
            {
                Currency currency =
                    this.allCurrencies.FirstOrDefault(
                        c => c.ShortName.Equals(value, StringComparison.InvariantCultureIgnoreCase));
                if (currency == null)
                {
                    return Labels.GlobalExportImport_TryConvertFromExcel_Invalid_currency + ":" + value;
                }
                converted = currency;
                return null;
            }

            if (targetType == typeof(bool))
            {
                if ("VRAI".Equals(value, StringComparison.InvariantCultureIgnoreCase))
                {
                    converted = true;
                    return null;
                }
                if ("FAUX".Equals(value, StringComparison.InvariantCultureIgnoreCase))
                {
                    converted = false;
                    return null;
                }
                bool parsed;
                if (bool.TryParse(value, out parsed))
                {
                    converted = parsed;
                    return null;
                }
                return Labels.GlobalExportImport_TryConvertFromExcel_Invalid_true_false_value + ":" + value;
            }

            if (targetType == typeof(int))
            {
                int parsed;
                if (int.TryParse(value, out parsed))
                {
                    converted = parsed;
                    return null;
                }
                return Labels.GlobalExportImport_TryConvertFromExcel_Invalid_integer_value + ":" + value;
            }

            if (targetType == typeof(double))
            {
                double parsed;
                //MTU sticky sticky
                value = value.Replace(",", ".");
                if (double.TryParse(value, out parsed))
                {
                    converted = parsed;
                    return null;
                }
                value = value.Replace(" ", "");
                if (double.TryParse(value, out parsed))
                {
                    converted = parsed;
                    return null;
                }
                return Labels.GlobalExportImport_TryConvertFromExcel_Invalid_double_value + ":" + value;
            }

            if (targetType == typeof(DateTime))
            {
                if (string.IsNullOrEmpty(value))
                {
                    return null;
                }

                DateTime dt;

                //MTU:first try to parse date
                try
                {
                    dt = DateTime.Parse(value, System.Globalization.CultureInfo.InstalledUICulture);
                    converted = dt;
                    Debug.Print(value + ";" + converted.ToString());
                    return null;
                }
                catch (Exception)
                {
                    //nothing to do here
                }

                double d;
                if (!double.TryParse(value, out d))
                {
                    return "Invalid date value" + ":" + value; // todo cdu
                }

                converted = EXCEL_MIN_DATE.AddDays(d);

                //DateTime dd= DateTime.FromOADate(d);

                Debug.Print(value + ";" + converted.ToString());
                return null;
            }


            throw new Exception("Unexpected type");
        }

        private static bool IsNullable(Type type)
        {
            return type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>);
        }

        public bool Import(string fileName)
        {
            this.Errors.Clear();

            ExcelApp excel = null;
            Workbook workBook = null;

            try
            {
                excel = new ExcelApp();
                excel.Visible = false;

                workBook = excel.Workbooks.Open(fileName, 0, true, 5, "", "", true, XlPlatform.xlWindows, "\t", false,
                                                false, 0, true, 1, 0);

                Worksheet sheet = (Worksheet)workBook.Sheets[1];

                this.Import(sheet);

                Marshal.ReleaseComObject(sheet);
            }
            catch (Exception ex)
            {
                Log.Exception(fileName, ex);

                this.Errors.Add("Excel error : " + ex.Message);

                return false;
            }
            finally
            {
                if (null != workBook)
                {
                    workBook.Close(false, Type.Missing, Type.Missing);
                    Marshal.ReleaseComObject(workBook);
                }

                if (null != excel)
                {
                    excel.Quit();
                    Marshal.ReleaseComObject(excel);
                }
            }

            return this.Errors.Count == 0;
        }

        public bool ImportFromClipboard()
        {
            this.Errors.Clear();
            string clipboardText = "";
            object[,] table;
            try
            {
            Tools.RunAsSTAThread(
                () =>
                {
                    clipboardText = Clipboard.GetText();
                    //dataObject = Clipboard.GetDataObject();
                });

                string[] lines = clipboardText.Split('\n');
                int row = 1;
                int [] lengths = new int[2];
                int [] starts = new int[2];
                lengths[0] = lines.Length;
                lengths[1] =lines[0].Split('\t').Length;
                starts[0] = 1;
                starts[1] = 1;
                table = (object[,])Array.CreateInstance((new object()).GetType(), lengths, starts);
                foreach (string line in lines)
                {
                    this.backgroundWorker.ReportProgress(80 * (row - 1) / lines.Length);
                    string[] sCells = line.Split('\t');
                    if (sCells.Length > 2 && !string.IsNullOrEmpty(sCells[1]))
                    {
                        for (int i = 0; i < sCells.GetLength(0); ++i)
                        {
                            table[row, i + 1] = sCells[i];
                        }
                    }
                    row++;

                }
                this.Import(table, 80);

            }
            catch (Exception ex)
            {
                Log.Exception("Import from Clipboard", ex);

                this.Errors.Add("Excel import from clipboard error : " + ex);

                return false;
            }

            return this.Errors.Count == 0;
        }

        private void Import(Worksheet worksheet, int startProgressBar = 0)
        {
            object[,] table = (object[,])worksheet.UsedRange.Value2;

            Import(table, startProgressBar);
        }

        private void Import(object[,] table, int startProgressBar)
        {
            DateTime start = DateTime.Now;

            Type[] bondPropertyTypes = BondMapper.Types;

            IList<Product> addedProducts = new List<Product>();
            IList<Product> foundProducts = new List<Product>();
            IList<KeyValuePair<Product, Product>> newSwapsToLink = new List<KeyValuePair<Product, Product>>();

            using (ISession session = SessionManager.OpenSession())
            {
                List<Category> allCategories = new List<Category>(Category.FindAll(session));

                if (this.rootCategory != null && this.rootCategory.Id != 0)
                {
                    this.rootCategory = Category.Find(session, this.rootCategory.Id);
                }
                else
                {
                    this.rootCategory = allCategories.FirstOrDefault(c => c.BalanceType == BalanceType.Portfolio);
                }

                if (rootCategory == null)
                {
                    this.AddError(0, 0, "Root Portfolio category not found");
                    return;
                }

                this.allCurrencies = Currency.FindAll(session);

                Product[] allProducts = this.rootCategory == null
                                            ? ProductCache.products.ToArray()//Product.FindAll(session)
                                            : this.GetAllProducts(this.rootCategory).ToArray();


                int rowCount = table.GetLength(0);
                int productsChanged = 0;
                Product lastSwapLeg = null;
                Logger.Info("Starting import of " + rowCount + " rows.");
                int flushCounter = 0;
                for (int row = 1; row < rowCount; row++)
                {
                    string[] bondProperties = GetBondProperties(table, row);
                    Logger.Info("Importing product " + row + " of " + rowCount);
                    Logger.Info(bondProperties);
                    if (bondProperties.All(string.IsNullOrEmpty)) continue;

                    string categoryName = GetCategory(table, row);

                    Category category = this.GetOrCreateCategory(session, allCategories, categoryName, row);
                    if (category == null)
                    {
                        this.AddError(row, CategoryColumn, "Parent category does not exist for:" + categoryName);
                        continue;
                    }

                    if (category.Parent.Bonds.Count > 0)
                    {
                        this.AddError(row, CategoryColumn, "Parent category is terminal (no sub-categories):" + categoryName);
                        continue;
                    }
                    if (category == null)
                    {
                        this.AddError(row, CategoryColumn, "Invalid category");
                        continue;
                    }
                    if (category.Children.Count > 0)
                    {
                        this.AddError(row, CategoryColumn,
                                      "Product not in terminal category (already has sub-categories):" + categoryName);
                        continue;
                    }

                    string almIdDisplayed = GetAlmId(table, row);
                    Product product = allProducts.FirstOrDefault(b => b.ALMIDDisplayed == almIdDisplayed);
                    if (product == null)
                    {
                        product = new Product();

                        if (!string.IsNullOrEmpty(almIdDisplayed))
                        {
                            int almId;
                            if (this.TryGetAlmIdFromAlmIdDisplayed(almIdDisplayed, category, out almId))
                            {
                                product.ALMID = almId;
                            }
                            else
                            {
                                this.AddError(row, FirstBondColumn, "Invalid product ID");
                                continue;
                            }
                        }

                        addedProducts.Add(product);
                    }
                    else
                    {
                        foundProducts.Add(product);
                    }

                    product.Position = row;

                    SetProductCategory(session, category, product);
                    bool productWasChanged = false;
                    object prop;
                    for (int i = 0; i < bondProperties.Length; i++)
                    {
                        try
                        {
                            object value;
                            string errorMessage = this.TryConvertFromExcel(bondProperties[i], bondPropertyTypes[i],
                                                                           out value);
                            //MTU : if value is -2146826246 then adderror (case of parse int("#N/A") from Excel
                            if (value != null && value.ToString() == "-2146826246")
                            {
                                this.AddError(row, FirstBondColumn + i, "Incoherent value " + value);
                            }
                            else
                            {
                                if (errorMessage != null)
                                {
                                    this.AddError(row, FirstBondColumn + i, errorMessage);
                                }
                                else
                                {
                                    if (this.Errors.Count == 0)
                                    {
                                        prop = BondMapper.GetPropertyByIndex(product, i);
                                        if (((prop == null && value != null) ||
                                             (prop != null && value == null) ||
                                             (prop != null && value != null && prop.ToString() != value.ToString())) &&
                                            product.ALMID != 0)
                                        {
                                            productWasChanged = true;
                                        }
                                        BondMapper.SetPropertyByIndex(product, i, value);
                                        //Logger.Info("Seting the value of "+product+", property "+BondMapper.PropertyNames[i]+" with "+value);
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            this.AddError(row, FirstBondColumn + i,
                                          Labels.GlobalExportImport_Import_Invalid_value + "-" + ex.Message);
                        }
                    }
                    if (product.Duration < 1) product.Duration = 1; //MTU : do not let duration=0 for imported products

                    if (Param.GetLinkSwaps2by2(session) && addedProducts.Contains(product))
                    {
                        if (product.ProductType == ProductType.Swap && lastSwapLeg == null &&
                            string.IsNullOrEmpty(product.SecId))
                            lastSwapLeg = product;
                        else if (product.ProductType == ProductType.Swap && lastSwapLeg != null &&
                                 string.IsNullOrEmpty(product.SecId))
                        {
                            newSwapsToLink.Add(new KeyValuePair<Product, Product>(lastSwapLeg, product));
                            lastSwapLeg = null;
                        }
                    }

                    if (productWasChanged) productsChanged++;

                    if (product.AmortizationType == ProductAmortizationType.VAR_NOMINAL)
                    {
                        product.OrigFace = Util.GetValueWithFormula(product.Amortization).ToString();
                    }
                    product.IntegrityCheck();

                    FixSwapLeg(product, addedProducts, foundProducts);

                    FixLinkedProducts(product, addedProducts, foundProducts);

                    product.FixBookValue();

                    // Set the IsReceivedFlag for Swaps
                    // Fix for xls import
                    if (product.ProductType == ProductType.Swap )
                    {
                        if(product.BookPriceValue>0)
                            product.SwapLeg = BondSwapLeg.Received;
                        else 
                            product.SwapLeg = BondSwapLeg.Paid;
                    }

                   //ProductCache.products.Add(product); //product.Save(session);
                    if (ProductCache.products.Any(p => p.ALMIDDisplayed == product.ALMIDDisplayed))
                    {
                        int pos = ProductCache.products.FindIndex(p => p.ALMIDDisplayed == product.ALMIDDisplayed);
                        ProductCache.products[pos] = product;
                    }
                    else
                        ProductCache.products.Add(product);

                    this.backgroundWorker.ReportProgress(startProgressBar + (100 - startProgressBar) * row / rowCount);
                    flushCounter++;
                    if (flushCounter == IMPORT_FLUSH_EACH)
                    {
                        Logger.Info("Before " + IMPORT_FLUSH_EACH + " products session.Flush");
                        session.Flush();
                        Logger.Info("After " + IMPORT_FLUSH_EACH + " products session.Flush");
                        flushCounter = 0;
                    }
                }
                session.Flush();
                Logger.Info("After prod creation and last session.flush.");
                string duplicate;
                if (DuplicateAlmIds(addedProducts, foundProducts, out duplicate))
                {
                    this.AddError(0, 0, "Duplicate product ID found: " + duplicate);
                }
                Logger.Info("After DuplicateAlmIds");

                if (this.Errors.Count == 0)
                {
                    if (Param.GetImportDeletesMissingProducts(SessionManager.OpenSession()))
                        this.DeletedProductsCount = DeleteMissingProducts(session, allProducts, foundProducts);
                    Logger.Info("After GetImportDeletesMissingProducts");
                    this.AddedProductsCount = addedProducts.Count;
                    this.ChangedProductsCount = productsChanged;

                    GenerateAlmIds(session, addedProducts, foundProducts);
                    Logger.Info("After GenerateAlmIds");
                    if (Param.GetLinkSwaps2by2(session))
                        FixNewSwapsLegs(newSwapsToLink);
                    Logger.Info("Before 1st session.Flush");
                    session.Flush();
                    Logger.Info("After 1st session.Flush");
                    FixNewSwapsLegsBySecId(session);
                    Logger.Info("After FixNewSwapsLegsBySecId");
                    FixBookpriceAndBookValueForAFS_HFT(addedProducts, "new");
                    Logger.Info("After FixBookpriceAndBookValueForAFS_HFT(new)");
                    FixBookpriceAndBookValueForAFS_HFT(foundProducts, "existing");
                    Logger.Info("After FixBookpriceAndBookValueForAFS_HFT(existing)");

                    Logger.Info("Before 2nd session.Flush");
                    session.Flush();
                    Logger.Info("After 2nd session.Flush");
                    BalanceBL.EnsureSpecialCategoriesAndBondsExist();
                    Logger.Info("After EnsureSpecialCategoriesAndBondsExist");
                    BalanceBL.AjustBalance();
                    Logger.Info("After AjustBalance");
                }
            }

            double duration = (DateTime.Now - start).TotalSeconds;
            Debug.WriteLine(duration);
            Logger.Info("Ending import in " + duration + " seconds.");
        }


        private void FixBookpriceAndBookValueForAFS_HFT(IList<Product> addedProducts, string label)
        {
            LoadForm.Instance.SetTitle("Calculating Bookprice for " + label + " products with AFS/HFT");
            LoadForm.Instance.ReinitTimer();
            this.backgroundWorker.ReportProgress(0);
            int i = 0;
            foreach (Product addedProduct in addedProducts)
            {
                if (addedProduct.Accounting == ProductAccounting.AFS || addedProduct.Accounting == ProductAccounting.HFT)
                {
                    Util.UpdateProductBookPrice(addedProduct);
                    this.UpdateBookValue(addedProduct);
                }
                this.backgroundWorker.ReportProgress(100 * i++ / addedProducts.Count);

            }
        }

        private static void FixNewSwapsLegs(IList<KeyValuePair<Product, Product>> newSwapsToLink)
        {
            foreach (KeyValuePair<Product, Product> swapPair in newSwapsToLink)
            {
                swapPair.Key.OtherSwapLeg = swapPair.Value;
                swapPair.Key.OtherSwapLegId = swapPair.Value.ALMID;

                swapPair.Value.OtherSwapLeg = swapPair.Key;
                swapPair.Value.OtherSwapLegId = swapPair.Key.ALMID;


            }
        }

        private static void FixNewSwapsLegsBySecId(ISession session)
        {

            Product otherLeg;
            IEnumerable<Product> allSwaps = ProductCache.products.FindAll(p => p.ProductType == ProductType.Swap);//Product.FindAll(session).Where(p => p.ProductType == ProductType.Swap);
            IEnumerable<Product> orphelinSwapsWithSecId = ProductCache.products.FindAll(p => p.ProductType == ProductType.Swap && p.OtherSwapLeg == null && p.SecId != null);//Product.FindAll(session).Where(p => p.ProductType == ProductType.Swap && p.OtherSwapLeg == null && p.SecId != null);

            foreach (Product prod in orphelinSwapsWithSecId)
            {
                otherLeg = allSwaps.FirstOrDefault(p => p.SecId == prod.SecId && p.ALMID != prod.ALMID && p.OtherSwapLeg == null);
                if (otherLeg != null)
                {
                    prod.OtherSwapLeg = otherLeg;
                    prod.OtherSwapLegId = otherLeg.ALMID;


                    otherLeg.OtherSwapLeg = prod;
                    otherLeg.OtherSwapLegId = prod.ALMID;


                }
            }
        }

        private void FixLinkedProducts(Product product, IList<Product> addedProducts, IList<Product> foundProducts)
        {
            if (string.IsNullOrEmpty(product.LinkedProductsAlmIds)) return;

            string[] almIds = product.LinkedProductsAlmIds.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries);

            foreach (string almId in almIds)
            {
                Product linkedProduct = addedProducts.FirstOrDefault(p => p.ALMIDDisplayed == almId);
                if (linkedProduct == null) linkedProduct = foundProducts.FirstOrDefault(p => p.ALMIDDisplayed == almId);

                if (linkedProduct != null)
                {
                    product.LinkedProducts.Add(linkedProduct);
                    linkedProduct.LinkedProducts.Add(product);
                }
            }
        }


        private void FixSwapLeg(Product product, IList<Product> addedProducts, IList<Product> foundProducts)
        {
            if (string.IsNullOrEmpty(product.OtherSwapLegAlmId)) return;

            Product otherSwapLeg = addedProducts.FirstOrDefault(p => p.ALMIDDisplayed == product.OtherSwapLegAlmId);
            if (otherSwapLeg == null) otherSwapLeg = foundProducts.FirstOrDefault(p => p.ALMIDDisplayed == product.OtherSwapLegAlmId);

            if (otherSwapLeg != null && otherSwapLeg.OtherSwapLegAlmId == product.ALMIDDisplayed)
            {
                product.OtherSwapLeg = otherSwapLeg;
                otherSwapLeg.OtherSwapLeg = product;
            }
        }

        private bool DuplicateAlmIds(IList<Product> addedProducts, IList<Product> foundProducts, out string duplicate)
        {
            Product[] products = addedProducts.Concat(foundProducts).ToArray();
            duplicate = null;

            for (int i = 0; i < products.Length; i++)
            {
                if (products[i].ALMID == 0) continue;

                for (int j = i + 1; j < products.Length; j++)
                {
                    if (products[i].ALMIDDisplayed == products[j].ALMIDDisplayed)
                    {
                        duplicate = products[i].ALMIDDisplayed;
                        return true;
                    }
                }
            }

            return false;
        }

        private bool TryGetAlmIdFromAlmIdDisplayed(string almId, Category category, out int intAlmId)
        {
            intAlmId = 0;
            if (string.IsNullOrEmpty(almId)) return false;

            if (category.Type == CategoryType.Roll && almId.StartsWith("R"))
            {
                return int.TryParse(almId.Substring(1), out intAlmId);
            }
            else
            {
                if (category.BalanceType == BalanceType.Asset && !almId.StartsWith("A")) return false;
                if (category.BalanceType == BalanceType.Liability && !almId.StartsWith("L")) return false;
                if (category.BalanceType == BalanceType.Equity && !almId.StartsWith("E")) return false;
            }

            return int.TryParse(almId.Substring(1), out intAlmId);
        }

        private void GenerateAlmIds(ISession session, IList<Product> addedProducts, IList<Product> foundProducts)
        {
            AlmIdGenerator almIdGenerator = new AlmIdGenerator();
            int retry;
            foreach (Product product in addedProducts)
            {
                if (product.ALMID == 0)
                {
                    retry = 0;
                    do
                    {
                        almIdGenerator.SetAlmId(session, product);
                        retry++;
                    } while (retry < MAX_GETALMID_RETRIES && (foundProducts.Any(p => p.ALMIDDisplayed == product.ALMIDDisplayed) ||
                             addedProducts.Any(p => p.ALMIDDisplayed == product.ALMIDDisplayed && p != product)));
                    if (retry == MAX_GETALMID_RETRIES) throw new ArgumentException("Impossible to get an ALMID for product " + product.Description + " into category " + product.Category.Label);
                }
            }

        }

        private static void SetProductCategory(ISession session, Category category, Product product)
        {
            if (product.Category == category) return;

            product.Category = category;
            product.Category.Bonds.Add(product);

            product.Category.Save(session);
        }

        private Category GetOrCreateCategory(ISession session, List<Category> allCategories, string categoryName, int row)
        {
            if (string.IsNullOrEmpty(categoryName))
            {
                this.AddError(row, 1, Labels.GlobalExportImport_Import_Invalid_category);
                return null;
            }

            Category category = allCategories.FirstOrDefault(c => c.FullPath() == categoryName);
            if (category == null)
            {
                category = this.CreateCategory(allCategories, categoryName);
                if (category == null)
                {
                    return null;
                }

                category.Save(session);
            }
            return category;
        }

        private static int DeleteMissingProducts(ISession session, IEnumerable<Product> allProducts, IEnumerable<Product> foundProducts)
        {
            int deletedCount = 0;
            foreach (Product product in allProducts)
            {
                if (ShouldExport(product) && foundProducts.All(p => p.ALMIDDisplayed != product.ALMIDDisplayed))
                {
                    deletedCount++;

                    if (product.Category != null)
                    {
                        product.Category.Bonds.Remove(product);
                    }

                    product.Category = null;
                  //  product.Delete(session);
                    ProductCache.products.Remove(product);
                }
            }

            return deletedCount;
        }

        private static bool ShouldExport(Product product)
        {
            return product.Category.Type == CategoryType.Normal || product.Category.Type == CategoryType.Roll;
        }

        private Category CreateCategory(List<Category> allCategories, string fullPath)
        {
            string[] pathParts = fullPath.Split('/');
            if (pathParts.Length <= 1) return null;

            Category parent = allCategories.Where(c => c.BalanceType == BalanceType.Portfolio).FirstOrDefault();
            if (parent == null) return null;

            return this.CreateCategory(pathParts, 0, parent);
        }

        private Category CreateCategory(string[] pathParts, int partIndex, Category parent)
        {
            if (partIndex >= pathParts.Length) return parent;

            if (parent.Bonds.Count > 0) return null;

            string label = pathParts[partIndex];
            Category category = parent.Children.FirstOrDefault(c => c.Label == label);

            if (category == null)
            {
                if (partIndex > 0)
                {

                    category = new Category
                    {
                        Parent = parent,
                        Label = label,
                        Bonds = new List<Product>(),
                        Children = new List<Category>(),
                        BalanceType = parent.BalanceType,
                        isVisible = parent.isVisible,
                        IsVisibleChecked = parent.IsVisibleChecked,
                        IsVisibleCheckedModel = parent.IsVisibleCheckedModel,
                        Type = parent.Type,
                        Position = parent.Children.Count,
                        ReadOnly = false
                    };

                    parent.Children.Add(category);

                }
                else
                {
                    return null;
                }
            }

            return CreateCategory(pathParts, partIndex + 1, category);
        }

        private List<Product> GetAllProducts(Category category)
        {
            List<Product> products = new List<Product>();
            products.AddRange(ProductCache.products.FindAll(p=>p.Category.ALMID==category.ALMID));
            foreach (Category child in category.ChildrenOrdered)
                products.AddRange(this.GetAllProducts(child));
            return products;
        }

        private void UpdateBookValue(Product product)
        {
            try
            {
                double bookPrice = Util.GetValueWithFormula(product.BookPrice, true);
                product.ComputeBookValue(bookPrice);
            }
            catch (Exception ex)
            {
                Logger.Warn(ex);
            }
        }

        private void AddError(int row, int column, string message)
        {
            this.Errors.Add(string.Format("Column {1}, row {0} : {2}", row + 1, ExcelTools.GetExcelColumnName(column), message));
        }

        private static string[] GetBondProperties(object[,] table, int row)
        {
            string[] properties = new string[BondMapper.Count];

            for (int j = 0; j < BondMapper.Count; j++)
            {
                //Range cell = ((Range)worksheet.Cells[row + 1, j + FirstBondColumn]);
                object value = table[row + 1, j + FirstBondColumn];

                if (value is double && Convert.ToString(value).Contains("%")) properties[j] = (double)value * 100 + "%";
                else properties[j] = Convert.ToString(value);
            }

            return properties;
        }

        private static string GetAlmId(object[,] table, int row)
        {
            return Convert.ToString(table[row + 1, BondIdColumn]);
        }

        private static string GetCategory(object[,] table, int row)
        {
            return Convert.ToString(table[row + 1, CategoryColumn]);
        }
    }
}