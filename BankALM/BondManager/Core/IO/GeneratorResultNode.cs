namespace BondManager.Core.IO
{
    public class GeneratorResultNode
    {
        public string Label { get; set; }
        public double[][] Data { get; set; }
        public GeneratorResultNode[] Nodes { get; set; }
        public bool IsExpanded { get; set; }
    }
}