using System;
using System.Xml.Serialization;
using BankALM.Infrastructure.Data;
using BondManager.Domain;
using NHibernate;

namespace BondManager.Core.IO
{
    [XmlRoot("ScenarioTemplateList")]
    public class ScenarioTemplate
    {
        public bool isLoad = false;

        private Variable[] variables;
        private VariableValue[] variableValues;
        private Scenario[] scenarios;
        private Vector[] vectors;
        private VectorPoint[] vectorPoints;

        private Currency[] currencies;
        private FXRate[] fxRates;
        private DateTime? valuationDate;
        private Currency valuationCurrency;

        private Dividend[] dividends;

        private FundamentalVector[] fundamentalVectors;

        private DynamicVariable[] dynamicVariables;


        [XmlElement]
        public FundamentalVector[] FundamentalVectors
        {
            get
            {
                using (ISession session = SessionManager.OpenSession())
                {
                    if (isLoad)
                        return fundamentalVectors;
                    else
                        return FundamentalVector.FindAll(session);
                }
            }
            set { fundamentalVectors = value; }
        }

        [XmlElement]
        public DynamicVariable[] DynamicVariables
        {
            get
            {
                using (ISession session = SessionManager.OpenSession())
                {
                    if (isLoad)
                        return dynamicVariables;
                    else
                        return DynamicVariable.FindAll(session);
                }
            }
            set { dynamicVariables = value; }
        }

        [XmlElement]
        public Variable[] Variables
        {
            get
            {
                using (ISession session = SessionManager.OpenSession())
                {
                    if (isLoad)
                        return variables;
                    else
                        return Variable.FindAll(session);
                }
            }
            set { variables = value; }
        }

        [XmlElement]
        public VariableValue[] VariableValues
        {
            get
            {
                using (ISession session = SessionManager.OpenSession())
                {
                    if (isLoad)
                        return variableValues;
                    else
                        return VariableValue.FindAll(session);
                }
            }
            set { variableValues = value; }
        }

        [XmlElement]
        public Scenario[] Scenarios
        {
            get
            {
                using (ISession session = SessionManager.OpenSession())
                {
                    if (isLoad)
                        return scenarios;
                    else
                        return Scenario.FindAll(session);
                }
            }
            set { scenarios = value; }
        }

        [XmlElement]
        public Vector[] Vectors
        {
            get
            {
                //using (ISession session = SessionManager.OpenSession())
                //{
                //    if (isLoad)
                //        return vectors;
                //    else
                //        return Vector.FindAll(session); // Vector.FindByGroup(VectorGroup.Workspace);
                //}
                return VectorCache.vectors.ToArray();
            }
            set { vectors = value; }
        }

        [XmlElement]
        public VectorPoint[] VectorPoints
        {
            get
            {
                //using (ISession session = SessionManager.OpenSession())
                //{
                //    if (isLoad)
                //        return vectorPoints;
                //    else
                //        return VectorPoint.FindAll(session);
                //            // VectorPoint.FindByVectorGroup(VectorGroup.Workspace);
                //}
                return VectorCache.getVectorPoints();
            }
            set { vectorPoints = value; }
        }

        [XmlElement]
        public Currency[] Currencies
        {
            get
            {
                using (ISession session = SessionManager.OpenSession())
                {
                    if (isLoad)
                        return currencies;
                    else
                        return Currency.FindAll(session);
                }
            }
            set { currencies = value; }
        }

        [XmlElement]
        public FXRate[] FxRates
        {
            get
            {
                using (ISession session = SessionManager.OpenSession())
                {
                    if (isLoad) return fxRates;
                    else return FXRate.FindAll(session);
                }
            }
            set { fxRates = value; }
        }

        [XmlElement]
        public DateTime? ValuationDate
        {
            get
            {
                using (ISession session = SessionManager.OpenSession())
                {
                    if (isLoad) return valuationDate;
                    else return Param.GetValuationDate(session);
                }
            }
            set { valuationDate = value; }
        }

        [XmlElement]
        public Currency ValuationCurrency
        {
            get
            {
                using (ISession session = SessionManager.OpenSession())
                {
                    if (isLoad) return valuationCurrency;
                    else return Param.GetValuationCurrency(session);
                }
            }
            set { valuationCurrency = value; }
        }

        [XmlElement]
        public Dividend[] Dividends
        {
            get
            {
                using (ISession session = SessionManager.OpenSession())
                {
                    if (isLoad) return dividends;
                    else return Dividend.FindAll(session);
                }
            }
            set { dividends = value; }
        }
    }
}
