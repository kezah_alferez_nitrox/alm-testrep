using System;
using System.IO;
using System.Xml;
using BankALM.Infrastructure.Data;
using BondManager.Domain;
using NHibernate;

namespace BondManager.Core.IO
{
    internal class CategoryXmlSerializer
    {
        private const int Version = 2;

        public string Save(string workspace)
        {
            using (ISession session = SessionManager.OpenSession())
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.AppendChild(xmlDoc.CreateXmlDeclaration("1.0", "utf-8", null));
                Category rootCategory = Category.FindPortfolio(session);
                var rootnode = CreateWorkspaceNode(workspace, xmlDoc);

                rootnode.AppendChild(this.AddCategoryNode(rootCategory, xmlDoc));
                xmlDoc.AppendChild(rootnode);

                string path = Environment.GetFolderPath(Environment.SpecialFolder.Personal) + "\\WorkspaceTemplates\\";
                string fileName = path + workspace + ".xml";
                Directory.CreateDirectory(path);
                xmlDoc.Save(fileName);

                return fileName;
            }
        }

        private static XmlNode CreateWorkspaceNode(string workspace, XmlDocument xmlDoc)
        {
            XmlNode rootnode = xmlDoc.CreateElement("", "workspace", "");
            XmlAttribute nameAttribute = xmlDoc.CreateAttribute("name");
            nameAttribute.Value = workspace;
            rootnode.Attributes.Append(nameAttribute);
            XmlAttribute versionAttribute = xmlDoc.CreateAttribute("version");
            versionAttribute.Value = Version.ToString();
            rootnode.Attributes.Append(versionAttribute);
            return rootnode;
        }

        private XmlNode AddCategoryNode(Category categoryNode, XmlDocument xmlDoc)
        {
            XmlNode node = NewNode(categoryNode, xmlDoc);
            if (categoryNode.Children.Count > 0)
            {
                foreach (Category child in categoryNode.ChildrenOrdered)
                {
                    node.AppendChild(this.AddCategoryNode(child, xmlDoc));
                }                
            }

            return node;
        }

        private XmlNode NewNode(Category cat, XmlDocument xmlDoc)
        {
            string balanceType = cat.BalanceType == BalanceType.Portfolio ? "" : ((int)cat.BalanceType).ToString();
            string readOnly = cat.BalanceType == BalanceType.Portfolio ? "" : cat.ReadOnly.ToString();
            string type = cat.BalanceType == BalanceType.Portfolio ? "" : cat.Type.ToString();

            return this.NewNode(cat.Label, balanceType, readOnly, type, xmlDoc);
        }

        private XmlNode NewNode(string name, string balanceType, string readOnly, string rollAccount, XmlDocument xmlDoc)
        {
            XmlNode newNode = xmlDoc.CreateElement("", "category", "");

            XmlAttribute attrname = xmlDoc.CreateAttribute("name");
            attrname.Value = name;

            newNode.Attributes.Append(attrname);

            if (balanceType != string.Empty)
            {
                XmlAttribute balanceTypeAttribute = xmlDoc.CreateAttribute("balanceType");
                balanceTypeAttribute.Value = balanceType;

                newNode.Attributes.Append(balanceTypeAttribute);
            }

            if (readOnly != string.Empty)
            {
                XmlAttribute attrreadOnly = xmlDoc.CreateAttribute("readOnly");
                attrreadOnly.Value = readOnly;

                newNode.Attributes.Append(attrreadOnly);
            }

            if (rollAccount != string.Empty)
            {
                XmlAttribute attrollAccount = xmlDoc.CreateAttribute("type");
                attrollAccount.Value = rollAccount;

                newNode.Attributes.Append(attrollAccount);
            }

            return newNode;
        }

        private Category NodeToCategory(XmlNode xNode, Category parent,ref int nextAlmid)
        {
            Category _xCat;
            if (xNode.ChildNodes.Count > 0)
            {
                _xCat = this.NewCategory(xNode, parent, ref nextAlmid);
                foreach (XmlNode _node in xNode.ChildNodes)
                {
                    _xCat.Children.Add(this.NodeToCategory(_node, _xCat, ref nextAlmid));
                }
                return _xCat;
            }
            else
            {
                return _xCat = this.NewCategory(xNode, parent, ref nextAlmid);
            }
        }

        public void Load(string filePath)
        {
            using (ISession session = SessionManager.OpenSession())
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(filePath);

                xmlDoc = this.EnsureUpCompatibilityForTemplate(xmlDoc, filePath);

                Category root = Category.FindPortfolio(session);
                if (root != null) root.Delete(session);
                int nextAlmid = Category.GetNewALMID(session);
                Category rootCat = this.NodeToCategory(xmlDoc.ChildNodes[1].ChildNodes[0], null,ref nextAlmid);
                
                rootCat.Create(session);
                session.Flush();
            }
        }

        private XmlDocument EnsureUpCompatibilityForTemplate(XmlDocument doc, string filename)
        {
            const string versionName = "version";
            bool versionChanged = false;

            int version = 1;
            if (doc.DocumentElement.HasAttribute(versionName))
            {
                int.TryParse(doc.DocumentElement.Attributes[versionName].Value, out version);
            }

            //different templates versioning updates - BEGIN
            if (version == 1)
            {
                doc = this.MigrateTemplateFromV1ToV2(doc);
                version = CategoryXmlSerializer.Version;
                versionChanged = true;
            }
            //different templates versioning updates - END

            //let's fucking save it !!!!
            if(versionChanged)
            {
                doc.Save(filename);
            }

            return doc;
        }

        private XmlDocument MigrateTemplateFromV1ToV2(XmlDocument doc)
        {
            XmlDocument newDoc = new XmlDocument();
            newDoc.LoadXml(Properties.Resources.WorkspaceTemplateV2);

            XmlNode oldVersionNode = newDoc.ImportNode(doc.DocumentElement.ChildNodes[0], true);

            newDoc.DocumentElement.ChildNodes[0].InsertBefore(oldVersionNode,
                                                              newDoc.DocumentElement.ChildNodes[0].ChildNodes[0]);


            // balanceType="4" readOnly="True" type="Standard"

            XmlAttribute balanceType = newDoc.CreateAttribute("balanceType");
            balanceType.Value = "0";
            XmlAttribute readOnly = newDoc.CreateAttribute("readOnly");
            readOnly.Value = "True";
            XmlAttribute type = newDoc.CreateAttribute("type");
            type.Value = "Standard";

            oldVersionNode.Attributes.Append(balanceType);
            oldVersionNode.Attributes.Append(readOnly);
            oldVersionNode.Attributes.Append(type);

            return newDoc;
        }

        #region NewCategory

        private Category NewCategory(string name, string balanceType, string readOnly, string type, Category parent, ref int nextAlmid)
        {
            Category newCategory = new Category();
            newCategory.ALMID = nextAlmid++;
            newCategory.Label = name;
            int bt;
            int.TryParse(balanceType, out bt);
            newCategory.BalanceType = (BalanceType) bt;

            newCategory.ReadOnly = bool.Parse(readOnly);
            if (type == "Standard") type = "Normal";
            newCategory.Type = (CategoryType) Enum.Parse(typeof (CategoryType), type, true);

            newCategory.Parent = parent;

            if ((parent == null) || (parent.Parent == null) || (parent.Parent.Parent == null))
                newCategory.isVisible = true;
            else
                newCategory.isVisible = false;

            return newCategory;
        }

        private Category NewCategory(XmlNode xNode, Category parrent, ref int nextAlmid)
        {
            if (xNode.Name == "category")
            {
                if (xNode.ParentNode.Name == "workspace")
                {
                    Category cat_root = new Category();
                    cat_root.ALMID = nextAlmid++;
                    cat_root.Label = xNode.Attributes.Item(0).Value;
                    cat_root.BalanceType = BalanceType.Portfolio;
                    cat_root.ReadOnly = true;
                    cat_root.Parent = null;
                    cat_root.Type = CategoryType.Normal;
                    cat_root.isVisible = true;

                    return cat_root;
                }
                else
                {
                    if (xNode.Attributes["balanceType"] == null)
                    {
                        XmlNode _auxnode = xNode;
                        do
                        {
                            _auxnode = _auxnode.ParentNode;
                        } while (_auxnode.Attributes["balanceType"] == null);
                        return this.NewCategory(xNode.Attributes["name"].Value, _auxnode.Attributes["balanceType"].Value,
                                                xNode.Attributes["readOnly"].Value, xNode.Attributes["type"].Value,
                                                parrent, ref nextAlmid);
                    }
                    else
                    {
                        return this.NewCategory(xNode.Attributes["name"].Value, xNode.Attributes["balanceType"].Value,
                                                xNode.Attributes["readOnly"].Value, xNode.Attributes["type"].Value,
                                                parrent, ref nextAlmid);
                    }
                }
            }
            else
                return null;
        }

        #endregion
    }
}