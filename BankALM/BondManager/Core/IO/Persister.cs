using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using ALMS.Products;
using BankALM.Infrastructure.Data;
using BondManager.Domain;
using BondManager.Views;
using NHibernate;

namespace BondManager.Core.IO
{
    public class Persister
    {
        private readonly IDictionary<int, Scenario> scenarioMap = new Dictionary<int, Scenario>();

        public void PersistCurrencies(Currency[] currencies, IDictionary<int, Currency> currencyMap)
        {
            this.PersistCurrencies(currencies, currencyMap, null);
        }

        public void PersistCurrencies(Currency[] currencies, IDictionary<int, Currency> currencyMap,
                                      BackgroundWorker backGroundWorker)
        {
            using (ISession session = SessionManager.OpenSession())
            {
                currencyMap.Clear();
                double aux = 10;
                if (currencies.Length != 0)
                    aux = 10.0 / currencies.Length;

                double totalProcent = 25;
                foreach (Currency currency in currencies)
                {
                    Currency existentCurrency = Currency.FindFirstBySymbol(session, currency.Symbol);
                    if (existentCurrency == null)
                    {
                        existentCurrency = currency.ClonePropertiesOnly();
                        existentCurrency.Create(session);
                    }
                    currencyMap.Add(currency.Id, existentCurrency);
                    if (backGroundWorker != null)
                        backGroundWorker.ReportProgress(Convert.ToInt32(totalProcent));
                    totalProcent = totalProcent + aux;
                }
                session.Flush();
            }
        }

        public void PersistCategoriesAndBonds(Category[] categories, Product[] products,
                                              IDictionary<int, Currency> currencyMap)
        {
            this.PersistCategoriesAndBonds(categories, products, currencyMap, null);
        }

        public void PersistCategoriesAndBonds(Category[] categories, Product[] products,
                                              IDictionary<int, Currency> currencyMap, BackgroundWorker backgroundWorker)
        {
            IDictionary<string, Product> productsByOldId = new Dictionary<string, Product>();

            using (IStatelessSession session = SessionManager.OpenStatelessSession())
            using (ITransaction transaction = session.BeginTransaction())
            {
                Category root = null;
                double aux = 15;
                if (categories.Length + products.Length != 0)
                    aux = 15.0 / (categories.Length + products.Length);

                double totalProcent = 35;

                root = categories.FirstOrDefault(c => c.BalanceType == BalanceType.Portfolio);

                if (root == null)
                {
                    Log.Error("BalanceXmlSerializer.Load: no root Portfolio category");
                    return;
                }
                else
                {
                    if (backgroundWorker != null)
                        backgroundWorker.ReportProgress(Convert.ToInt32(totalProcent));
                    totalProcent = totalProcent + aux;
                }

                IDictionary<int, Category> categoryMap = new Dictionary<int, Category>();

                this.ReBuildAndSaveCategoryTree(session, root, null, categories, categoryMap);

                foreach (Product product in products)
                {
                    Product clone = product.ClonePropertiesOnly();
                    clone.LinkedProductsIds = product.LinkedProductsIds;

                    this.FixProduct(clone);

                    clone.OtherSwapLegId = product.OtherSwapLegId;
                    clone.Currency = GetCurrency(product, currencyMap);
                    clone.Category = categoryMap[product.Category.ALMID];

                    totalProcent = totalProcent + aux;

                   // session.Insert(clone);
                    ProductCache.products.Add(clone);

                    if (backgroundWorker != null)
                        backgroundWorker.ReportProgress(Convert.ToInt32(totalProcent));

                    productsByOldId.Add(product.ALMIDDisplayed, clone);
                }

                IList<Product> toUpdate = new List<Product>();
                List<int> pos = new List<int>();
                foreach (Product product in productsByOldId.Values)
                {
                    int posi = ProductCache.products.FindIndex(p => p.ALMIDDisplayed == product.ALMIDDisplayed);
                    if (product.OtherSwapLegId > 0 && productsByOldId.ContainsKey(product.OtherSwapLegAlmId))
                    {
                        product.OtherSwapLeg = productsByOldId[product.OtherSwapLegAlmId];

                        this.FixSwapLeg(product);
                        this.FixSwapLeg(product.OtherSwapLeg);

                        toUpdate.Add(product);
                        pos.Add(posi);
                    }
                }

                //foreach (Product product in toUpdate.Distinct())
                //{
                //    session.Update(product);
                //}

                Product[] arr = toUpdate.Distinct().ToArray();
                int[] posd = pos.Distinct().ToArray();
                for (int i = 0; i < arr.Length; i++)
                {
                    ProductCache.products[posd[i]] = arr[i];
                }

                transaction.Commit();
            }

            FixLinkedProducts(productsByOldId);

        }

        private static void FixLinkedProducts(IDictionary<string, Product> productsByOldId)
        {
            using (ISession session = SessionManager.OpenSession())
            {
                IList<Product> toUpdate = new List<Product>();
                List<int> pos = new List<int>();
                foreach (Product product in productsByOldId.Values)
                {
                    int posi = ProductCache.products.FindIndex(p => p.ALMIDDisplayed == product.ALMIDDisplayed);
                    if (product.LinkedProductsAlm != null && product.LinkedProductsAlm.Length > 0)
                    {
                        foreach (Product product2 in productsByOldId.Where(p => product.LinkedProductsAlm.Contains(p.Key)).Select(p => p.Value))
                        {
                            product.LinkedProducts.Add(product2);
                            product2.LinkedProducts.Add(product);
                        }
                        toUpdate.Add(product);
                        pos.Add(posi);
                    }
                }

                //Product[] array = toUpdate.Distinct().ToArray();
                //foreach (Product product in array)
                //{
                //    session.Update(product);
                //}

                Product[] arr = toUpdate.Distinct().ToArray();
                int[] posd = pos.Distinct().ToArray();
                for (int i = 0; i < arr.Length; i++)
                {
                    ProductCache.products[posd[i]] = arr[i];
                }
                session.Flush();
            }
        }

        private void FixProduct(Product product)
        {
            if (product.Attrib == ProductAttrib.Roll)
            {
                product.InvestmentRule = ProductInvestmentRule.Roll;
            }
        }

        private void FixSwapLeg(Product product)
        {
            if (product == null || product.SwapLeg == null) return;

            if (product.SwapLeg == (BondSwapLeg)2)
            {
                product.SwapLeg = BondSwapLeg.Received;
                // product.CouponType = CouponType.Floating;
            }

            if (product.SwapLeg == (BondSwapLeg)3)
            {
                product.SwapLeg = BondSwapLeg.Paid;
                // product.CouponType = CouponType.Floating;
            }
        }

        private static Currency GetCurrency(Product product, IDictionary<int, Currency> currencyMap)
        {
            if (product.Currency == null || !currencyMap.ContainsKey(product.Currency.Id)) return null;

            return currencyMap[product.Currency.Id];
        }

        public void ReBuildAndSaveCategoryTree(IStatelessSession session, Category category, Category parent, Category[] allCategories, IDictionary<int, Category> categoryMap)
        {
            int oldId = category.ALMID;
            Category clonedParentCategory = category.ClonePropertiesOnly();
            clonedParentCategory.Parent = parent;
            session.Insert(clonedParentCategory);
            categoryMap.Add(oldId, clonedParentCategory);

            foreach (Category child in allCategories)
            {
                if (child.Parent == null || child.Parent.ALMID != oldId) continue;

                this.ReBuildAndSaveCategoryTree(session, child, clonedParentCategory, allCategories, categoryMap);
            }
        }

        public void PersistFxRates(FXRate[] rates, IDictionary<int, Currency> currencyMap)
        {
            using (ISession session = SessionManager.OpenSession())
            {
                foreach (FXRate rate in rates)
                {
                    FXRate newRate = new FXRate();
                    newRate.Value = rate.Value;
                    newRate.From = rate.From == null ? null : currencyMap[rate.From.Id];
                    newRate.To = rate.From == null ? null : currencyMap[rate.To.Id];
                    newRate.Create(session);
                }
                session.Flush();
            }
        }

        public void PersistValuationDate(DateTime? date)
        {
            using (ISession session = SessionManager.OpenSession())
            {
                if (date != null) Param.SetValuationDate(session, date);
                session.Flush();
            }
        }

        public void PersistValuationCurrency(Currency currency, IDictionary<int, Currency> currencyMap)
        {
            using (ISession session = SessionManager.OpenSession())
            {
                if (currency != null) Param.SetValuationCurrency(session, currencyMap[currency.Id]);
                session.Flush();
            }
        }

        public IDictionary<int, Variable> PersistVariables(Scenario[] scenarios, Variable[] variables,
                                                           VariableValue[] variableValues)
        {
            return this.PersistVariables(scenarios, variables, variableValues, null);
        }

        public IDictionary<int, Variable> PersistVariables(Scenario[] scenarios, Variable[] variables,
                                                           VariableValue[] variableValues,
                                                           BackgroundWorker backgroundWorker)
        {
            using (ISession session = SessionManager.OpenSession())
            {
                double auxprocess = 20;
                if (scenarios.Length + variables.Length + variableValues.Length != 0)
                    auxprocess = 20.0 / (scenarios.Length + variables.Length + variableValues.Length);

                int totalprocess = 50;
                IDictionary<int, Variable> variableMap = new Dictionary<int, Variable>();
                foreach (Scenario scenario in scenarios)
                {
                    Scenario savedScenario = scenario.ClonePropertiesOnly();
                    savedScenario.Create(session);
                    this.scenarioMap.Add(scenario.Id, savedScenario);
                    if (backgroundWorker != null)
                        backgroundWorker.ReportProgress(totalprocess);
                    totalprocess = totalprocess + Convert.ToInt32(auxprocess);
                }
                foreach (Variable variable in variables)
                {
                    Variable savedVariable = variable.ClonePropertiesOnly();
                    savedVariable.Create(session);
                    variableMap.Add(variable.Id, savedVariable);
                    if (backgroundWorker != null)
                        backgroundWorker.ReportProgress(totalprocess);
                    totalprocess = totalprocess + Convert.ToInt32(auxprocess);
                }

                foreach (VariableValue variableValue in variableValues)
                {
                    VariableValue savedVariableValue = variableValue.ClonePropertiesOnly();
                    savedVariableValue.Scenario = this.scenarioMap[variableValue.Scenario.Id];
                    savedVariableValue.Variable = variableMap[variableValue.Variable.Id];
                    savedVariableValue.Create(session);

                    savedVariableValue.Scenario.VariableValues.Add(savedVariableValue);
                    savedVariableValue.Variable.VariableValues.Add(savedVariableValue);
                    if (backgroundWorker != null)
                        backgroundWorker.ReportProgress(totalprocess);
                    totalprocess = totalprocess + Convert.ToInt32(auxprocess);
                }
                session.Flush();
                return variableMap;
            }
        }

        //public void PersistVectors(Vector[] vectors, VectorPoint[] points)
        //{
        //    this.PersistVectors(vectors, points, null);
        //}

        //public void PersistVectors(Vector[] vectors, VectorPoint[] points, BackgroundWorker backgroundWorker)
        //{
        //    using (IStatelessSession session = SessionManager.OpenStatelessSession())
        //    using (ITransaction transaction = session.BeginTransaction())
        //    {
        //        Console.WriteLine("Saving vectors & points: {0} vectors, {1} points", vectors.Length, points.Length);
        //        Stopwatch stopwatch = Stopwatch.StartNew();

        //        IDictionary<string, Vector> vectorMap = new Dictionary<string, Vector>();
        //        foreach (Vector vector in vectors)
        //        {
        //            Vector clonedVector = vector.ClonePropertiesOnly();
        //            vectorMap.Add(vector.Name, clonedVector);
        //            //session.Insert(clonedVector);

        //            foreach(VectorPoint point in vector.VectorPoints)
        //            {
        //                VectorPoint clonedVectorPoint = point.ClonePropertiesOnly();
        //                clonedVector.VectorPoints.Add(clonedVectorPoint);
        //            }

        //            VectorCache.vectors.Add(clonedVector);
        //        }

        //        Console.WriteLine("Finished saving vectors: {0}ms", stopwatch.ElapsedMilliseconds);
        //        stopwatch.Reset();
        //        stopwatch.Start();

        //        //int k = 0;
        //        //foreach (VectorPoint vectorPoint in points)
        //        //{
        //        //    // code for the tweak (dirty) to load corrupt files
        //        //    /*k++;
        //        //    // Correction of Bad saving Bug. This is temporary and needs to be removed ZZZ
        //        //    if (vectorPoint.Vector == null && BondManager.Views.MainForm.IsBeta)
        //        //    {
        //        //        Console.WriteLine("Vector is null: #" + k);
        //        //    }
        //        //    else
        //        //    {*/
        //        //    Vector vector = vectorMap[vectorPoint.Vector.Name];
        //        //    VectorPoint clonedVectorPoint = vectorPoint.ClonePropertiesOnly();
        //        //    clonedVectorPoint.Vector = vector;
        //        //   // session.Insert(clonedVectorPoint);

        //        //}

        //        Console.WriteLine("Finished saving points: {0}ms", stopwatch.ElapsedMilliseconds);

        //        stopwatch.Reset();
        //        stopwatch.Start();

        //        transaction.Commit();

        //        Console.WriteLine("Finished flushing: {0}ms", stopwatch.ElapsedMilliseconds);
        //    }
        //}

        public void PersistDividends(Dividend[] dividends)
        {
            using (ISession session = SessionManager.OpenSession())
            {
                foreach (Dividend dividend in dividends)
                {
                    Dividend newDividend = new Dividend();
                    newDividend.Amount = dividend.Amount;
                    newDividend.Minimum = dividend.Minimum;
                    newDividend.Name = dividend.Name;
                    newDividend.Create(session);
                }
                session.Flush();
            }
        }

        public void PersistOtherItemsAssum(OtherItemAssum[] otherItemAssms)
        {
            using (ISession session = SessionManager.OpenSession())
            {
                foreach (OtherItemAssum otherItemAssm in otherItemAssms)
                {
                    OtherItemAssum newOtherItemAssus = otherItemAssm.ClonePropertiesOnly();
                    newOtherItemAssus.Create(session);
                }
                session.Flush();
            }
        }

        public void PersistSolvencyAdjustments(SolvencyAdjustment[] adjustments)
        {
            using (ISession session = SessionManager.OpenSession())
            {
                foreach (SolvencyAdjustment adjustment in adjustments)
                {
                    SolvencyAdjustment newSolvencyAdjustment = new SolvencyAdjustment
                    {
                        Id = adjustment.Id,
                        Description = adjustment.Description,
                        Vector = adjustment.Vector,
                    };
                    newSolvencyAdjustment.Create(session);
                }
                session.Flush();
            }
        }

        public void PersistFundamentalVectors(FundamentalVector[] fundamentalVectors, IDictionary<int, Variable> variableMap, IDictionary<int, Currency> currencyMap)
        {
            using (ISession session = SessionManager.OpenSession())
            {
                foreach (FundamentalVector vector in fundamentalVectors)
                {
                    FundamentalVector fundamentalVector = new FundamentalVector();
                    fundamentalVector.Name = vector.Name;
                    fundamentalVector.Unit = vector.Unit;
                    fundamentalVector.Delta = vector.Delta;
                    fundamentalVector.Position = vector.Position;
                    fundamentalVector.Frequency = vector.Frequency;
                    fundamentalVector.Volatility = vector.Volatility;
                    fundamentalVector.Type = vector.Type;
                    fundamentalVector.DateConvention = vector.DateConvention;
                    fundamentalVector.Variable = vector.Variable == null ? null : variableMap[vector.Variable.Id];
                    fundamentalVector.Currency = vector.Currency == null ? null : currencyMap[vector.Currency.Id];
                    fundamentalVector.Create(session);
                }

                session.Flush();
            }
        }

        public void PersistDynamicVariables(DynamicVariable[] dynamicVariables)
        {
            using (ISession session = SessionManager.OpenSession())
            {
                foreach (DynamicVariable dynamicVariable in dynamicVariables)
                {
                    dynamicVariable.Create(session);
                }
                session.Flush();
            }
        }

        public void PersistVolatilityConfig(VolatilityConfig[] volatilityConfigs, IDictionary<int, Variable> variableMap)
        {
            using (ISession session = SessionManager.OpenSession())
            {
                foreach (VolatilityConfig volatilityConfig in volatilityConfigs)
                {
                    VolatilityConfig volatility = new VolatilityConfig();
                    volatility.Scenario = this.scenarioMap[volatilityConfig.Scenario.Id];
                    volatility.Selected = volatilityConfig.Selected;
                    volatility.Variable = variableMap[volatilityConfig.Variable.Id];
                    volatility.Volatility = volatilityConfig.Volatility;
                    volatility.Mean = volatilityConfig.Mean;
                    volatility.Create(session);
                }
                session.Flush();
            }
        }

        public void PersistVariableCorrelation(VariableCorrelation[] volatilityConfigs,
                                               IDictionary<int, Variable> variableMap)
        {
            using (ISession session = SessionManager.OpenSession())
            {
                foreach (VariableCorrelation correlation in volatilityConfigs)
                {
                    VariableCorrelation variableCorrelation = new VariableCorrelation();
                    variableCorrelation.Scenario = this.scenarioMap[correlation.Scenario.Id];
                    variableCorrelation.Variable1 = variableMap[correlation.Variable1.Id];
                    variableCorrelation.Variable2 = variableMap[correlation.Variable2.Id];
                    variableCorrelation.CorrelationValue = correlation.CorrelationValue;
                    variableCorrelation.Create(session);
                }
                session.Flush();
            }
        }

        public void DeleteScenarioData()
        {
            using (ISession session = SessionManager.OpenSession())
            {
                VariableCorrelation.DeleteAll(session);
                VolatilityConfig.DeleteAll(session);
                SolvencyAdjustment.DeleteAll(session);
                FundamentalVector.DeleteAll(session);
                VariableValue.DeleteAll(session);
                Variable.DeleteAll(session);
                Scenario.DeleteAll(session);
                VectorPoint.DeleteAll(session);
                Vector.DeleteAll(session);
                FXRate.DeleteAll(session);
                Dividend.DeleteAll(session);
                OtherItemAssum.DeleteAll(session);
                DynamicVariable.DeleteAll(session);
                session.Flush();
            }
        }

        public void DeleteBalanceData()
        {
            using (ISession session = SessionManager.OpenSession())
            {
                Category root = Category.FindPortfolio(session);
                if (root != null) root.Delete(session);
                session.Flush();
            }
        }

        public void PersistParameters(Param[] parameters, IDictionary<int, Currency> currencyMap)
        {
            using (ISession session = SessionManager.OpenSession())
            {
                Param.DeleteAll(session);
                foreach (Param param in parameters)
                {
                    if (param.Id == Param.VALUATION_CURRENCY)
                    {
                        param.Val = currencyMap[int.Parse(param.Val)].Id.ToString();
                    }
                }
                foreach (Param param in parameters)
                {
                    Param copy = param.Copy();
                    copy.Create(session);
                }
                session.Flush();
            }
        }
    }
}