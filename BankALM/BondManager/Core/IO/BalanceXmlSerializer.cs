using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using BankALM.Infrastructure.Data;
using BondManager.BusinessLogic;
using BondManager.Domain;
using NHibernate;


namespace BondManager.Core.IO
{
    internal class BalanceXmlSerializer : IBalanceSerializer
    {
        public void Save(string fileName)
        {

            BalanceTemplate _sof = new BalanceTemplate();
            _sof.isLoad = false;
            XmlSerializer serializer = new XmlSerializer(typeof(BalanceTemplate));

            using (MemoryStream memoryStream = new MemoryStream())
            {
                serializer.Serialize(memoryStream, _sof);
            }

            using (FileStream stream = File.Open(fileName, FileMode.Create))
            {
                serializer.Serialize(stream, _sof);
            }
        }

        public void Load(string fileName)
        {
            BalanceTemplate newSaveOpenFile;

            BalanceBL.ClearCache();

            using (FileStream stream = File.Open(fileName, FileMode.Open))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(BalanceTemplate));
                newSaveOpenFile = (BalanceTemplate)serializer.Deserialize(stream);
                stream.Close();
            }

            newSaveOpenFile.isLoad = true;
            if (newSaveOpenFile.Currencies == null) newSaveOpenFile.Currencies = new Currency[0];
            if (newSaveOpenFile.Categories == null) newSaveOpenFile.Categories = new Category[0];
            if (newSaveOpenFile.Products == null) newSaveOpenFile.Products = new Product[0];
            if (newSaveOpenFile.OthierItemAssums == null) newSaveOpenFile.OthierItemAssums = new OtherItemAssum[0];

            Category root = newSaveOpenFile.Categories.FirstOrDefault(c => c.Parent == null);
            if (root != null && root.BalanceType==BalanceType.BalanceSheet) MigrateToV2(newSaveOpenFile);


            IDictionary<int, Currency> currencyMap = new Dictionary<int, Currency>();

            using (ISession session = SessionManager.OpenSession())
            {

                Persister persister = new Persister();
                persister.DeleteBalanceData();
                persister.PersistCurrencies(newSaveOpenFile.Currencies, currencyMap);
                persister.PersistCategoriesAndBonds(newSaveOpenFile.Categories, newSaveOpenFile.Products, currencyMap);
                persister.PersistOtherItemsAssum(newSaveOpenFile.OthierItemAssums);
                session.Flush();
            }
        }

        private static void MigrateToV2(BalanceTemplate bt)
        {
            bt.Categories = IOTools.migrateCategoriesToV2(bt.Categories);
        }
    }
}