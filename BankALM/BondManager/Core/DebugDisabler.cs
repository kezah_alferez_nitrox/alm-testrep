using System;
using System.Diagnostics;

namespace BondManager.Core
{
    public class DebugDisabler : IDisposable
    {
        private readonly TraceListener[] listeners;

        public DebugDisabler()
        {
            this.listeners = new TraceListener[Debug.Listeners.Count];
            Debug.Listeners.CopyTo(this.listeners, 0);
            Debug.Listeners.Clear();
        }

        public void Dispose()
        {
            Debug.Listeners.AddRange(listeners);
        }
    }
}