using System;
using ALMS.Products;
using ALMSCommon;
using BondManager.Resources.Language;

namespace BondManager.Core
{
    public struct DateInterval
    {
        public readonly DateTime Start;
        public readonly int StartUnit;
        public readonly DateTime End;
        public readonly int EndUnit;
        public readonly int Length;
        private const string HeaderDateFormat = "MM/yyyy";
        private const string HeaderDailyDateFormat = "dd/MM/yyyy";

        public DateInterval(DateTime start, DateTime end, bool isDaily)
        {
            this.Start = start;
            this.End = end;

            this.StartUnit = PricerUtil.DateToInt(this.Start, isDaily);
            this.EndUnit = PricerUtil.DateToInt(this.End, isDaily);
            this.Length = this.EndUnit - this.StartUnit + 1;
        }

        public string ToString(bool isOpeningBalance)
        {
            string dateFormat = AlmConfiguration.IsComputationDaily() && this.Start == this.End ? HeaderDailyDateFormat : HeaderDateFormat;
            string headerText = isOpeningBalance
                                    ? Labels.ResultModel_InitColumns_Opening_Balance
                                    : this.Start.ToString(dateFormat);
            if (this.Start != this.End)
                headerText += " - " + this.End.ToString(dateFormat);
            return headerText;
        }
    }
}