﻿using BankALM.Infrastructure.Data;
using BondManager.Domain.Mapping;
using NUnit.Framework;

namespace BondManager.Domain.Test
{     
    public abstract class DbTestBase
    {
         [SetUp]
         public void SetUp()
         {
             SessionManager.Configure(typeof(CategoryMap).Assembly, ".\\test.sdf", ()=> { });
             SessionManager.DeleteDataBase();
             SessionManager.EnsureDataBaseExists();
         }

         [TearDown]
         public void TearDown()
         {
             SessionManager.DeleteDataBase();
         }
    }
}