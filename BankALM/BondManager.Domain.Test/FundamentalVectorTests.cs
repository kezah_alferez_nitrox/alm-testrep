﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankALM.Infrastructure.Data;
using NHibernate;
using NUnit.Framework;

namespace BondManager.Domain.Test
{
    [TestFixture]
    public class FundamentalVectorTests : DbTestBase
    {
        [Test]
        public void CreateAndFindOne()
        {
            using (ISession session = SessionManager.OpenSession())
            using (ITransaction transaction = session.BeginTransaction())
            {
                FundamentalVector fxrate = new FundamentalVector();
                fxrate.Create(session);
                transaction.Commit();
            }

            using (ISession session = SessionManager.OpenSession())
            {
                FundamentalVector fxrate = FundamentalVector.FindOne(session);
                Assert.IsNotNull(fxrate);
            }
        }


    }
}
