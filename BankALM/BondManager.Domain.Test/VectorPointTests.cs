﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankALM.Infrastructure.Data;
using NHibernate;
using NUnit.Framework;

namespace BondManager.Domain.Test
{
    [TestFixture]
    public class VectorPointTests : DbTestBase
    {
        [Test]
        public void CreateAndFindOne()
        {
            using (ISession session = SessionManager.OpenSession())
            using (ITransaction transaction = session.BeginTransaction())
            {
                VectorPoint fxrate = new VectorPoint();
                fxrate.Create(session);
                transaction.Commit();
            }

            using (ISession session = SessionManager.OpenSession())
            {
                VectorPoint fxrate = VectorPoint.FindOne(session);
                Assert.IsNotNull(fxrate);
            }
        }


    }
}
