using BondManager.Generator;
using NUnit.Framework;

namespace BondManager.Test.Generator
{
    [TestFixture]
    public class StatisticsTest
    {
        private const double Delta = 0.000000001;

        [Test]
        public void StandardDeviationTest()
        {
            // double[] values = new[] { 51.021337217, 48.374709828, 53.830598248, 50.264311948, 57.778784959, 44.112406465, 50.741014035, 48.558450536, 44.714343621, 57.759504939, 54.302278881, 51.472104948, 60.800921414, 51.384085807, 46.051950215, 45.641790622, 53.904892825, 54.041174911, 52.570687418, 48.530661658 };
            double[] values = new double[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            double stdev = Statistics.StandardDeviation(values);

            Assert.AreEqual(3.027650354, stdev, Delta);
        }

        [Test]
        public void CorrelationTest1()
        {
            double[] x = new double[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            double[] y = new double[] { 4, 4, 7, 1, 2, 8, 7, 4, 1, 9 };

            double correlation = Statistics.Correlation(x, y);

            Assert.AreEqual(0.208240233, correlation, Delta);
        }
    }
}