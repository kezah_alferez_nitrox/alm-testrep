﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using BondManager.Calculations;
using BondManager.Domain;
using NUnit.Framework;

namespace BondManager.Test.Calculus
{
    [TestFixture]
    public class DynamicVariableEvaluatorTests
    {
        [SetUp]
        public void Setup()
        {
            FieldInfo fieldInfo = typeof(Param).GetField("cachedMaxDuration", BindingFlags.NonPublic | BindingFlags.Static);
            fieldInfo.SetValue(null, 1);
        }

        [Test]
        public void TestProductDataArrayProperty()
        {
            ProductData p = new ProductData();
            p.PrincipalIncome[0] = 2;
            p.PrincipalIncome[1] = 3;

            DynamicVariableEvaluator evaluator = new DynamicVariableEvaluator(@"return t<2 ? 0 : p.get_PrincipalIncome()[t-1] + p.get_PrincipalIncome()[t-2]", 
                new[] { "t", "p" }, new string[0], new int[0],  0);

            double result = evaluator.Evaluate<double>(0, p);
            Assert.AreEqual(0.0, result);

            result = evaluator.Evaluate<double>(2, p);
            Assert.AreEqual(5.0, result);
        }

        [Test]
        public void TestProductDataDictionary()
        {
            ProductData p1 = new ProductData();
            p1.PrincipalIncome[0] = 2;
            p1.PrincipalIncome[1] = 3;

            ProductData p2 = new ProductData();
            p2.PrincipalIncome[0] = 5;
            p2.PrincipalIncome[1] = 7;

            IDictionary<string, ProductData> products = new SortedDictionary<string, ProductData>
                                                               {
                                                                   {"A0001", p1},
                                                                   {"A0002", p2},
                                                               };

            DynamicVariableEvaluator evaluator = new DynamicVariableEvaluator(@"return A0001.PrincipalIncome[t-1] + A0002.PrincipalIncome[t-2]", 
                new[] { "t", "products" }, products.Keys.ToArray(), new int[0], 0);

            double result = evaluator.Evaluate<double>(2, products);
            Assert.AreEqual(8.0, result);
        }
    }
}