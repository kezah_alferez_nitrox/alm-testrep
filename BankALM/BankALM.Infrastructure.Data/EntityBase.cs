﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Serialization;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Linq;

namespace BankALM.Infrastructure.Data
{
    [Serializable]
    public abstract class EntityBase<T, TId> : AbstractEntity<TId>, IComparable<T> where T : class
    {
        public static void DeleteAll(ISession session)
        {
            string sql = "DELETE FROM [" + typeof(T).Name + "]";
            session.CreateSQLQuery(sql).ExecuteUpdate();
        }

        public static T Find(ISession session, object id)
        {
            return session.Get<T>(id);
        }

        public static T[] FindAll(ISession session, params ICriterion[] criterias)
        {
            ICriteria criteria = session.CreateCriteria<T>();
            foreach (ICriterion criterion in criterias)
            {
                criteria.Add(criterion);
            }
            return criteria.List<T>().ToArray();
        }

        public static T[] FindAllByProperty(ISession session, string property, object value)
        {
            ICriteria criteria = session.CreateCriteria<T>();
            criteria.Add(Restrictions.Eq(property, value));
            return criteria.List<T>().ToArray();
        }

        public static T FindFirst(ISession session, params ICriterion[] criterias)
        {
            ICriteria criteria = session.CreateCriteria<T>();
            foreach (ICriterion criterion in criterias)
            {
                criteria.Add(criterion);
            }
            return criteria.SetMaxResults(1).UniqueResult<T>();
        }

        public static T FindOne(ISession session, params ICriterion[] criterias)
        {
            ICriteria criteria = session.CreateCriteria<T>();
            foreach (ICriterion criterion in criterias)
            {
                criteria.Add(criterion);
            }
            return criteria.UniqueResult<T>();
        }

        public virtual void Create(ISession session)
        {
            session.Save(this);
        }

        public virtual void Delete(ISession session)
        {
            session.Delete(this);
        }

        public virtual void Refresh(ISession session)
        {
            session.Refresh(this);
        }

        public virtual void Save(ISession session)
        {
            session.SaveOrUpdate(this);
        }

        public virtual object SaveCopy(ISession session)
        {
            return session.Merge(this);
        }

        public virtual void Update(ISession session)
        {
            session.Update(this);
        }

        public virtual T ClonePropertiesOnly()
        {
            if (!typeof(T).IsAbstract)
            {
                T newObj = Activator.CreateInstance<T>();
                PropertyInfo[] properties = typeof(T).GetProperties();
                foreach (PropertyInfo property in properties)
                {
                    if (EntityUtil.CanCloneProperty(property))
                    {
                        property.SetValue(newObj, property.GetValue(this, null), null);
                    }
                }
                return newObj;
            }
            else
            {
                return default(T);
            }
        }


        public static T Clone<T>(T source)
        {
            if (!typeof(T).IsSerializable)
            {
                throw new ArgumentException("The type must be serializable.", "source");
            }

            // Don't serialize a null object, simply return the default for that object
            if (Object.ReferenceEquals(source, null))
            {
                return default(T);
            }

            IFormatter formatter = new BinaryFormatter();
            Stream stream = new MemoryStream();
            using (stream)
            {
                formatter.Serialize(stream, source);
                stream.Seek(0, SeekOrigin.Begin);
                return (T)formatter.Deserialize(stream);
            }
        }



        public static int ExecuteDelete(ISession session, string hql)
        {
            return session.Delete(hql);
        }

        public IQueryable<T> GetQuery(ISession session)
        {
            return session.Query<T>();
        }

        public int CompareTo(T other)
        {
            PropertyInfo[] properties = typeof(T).GetProperties();
            bool result = true;
            object firstValue;
            object secondValue;

            foreach (PropertyInfo property in properties)
            {
                firstValue = property.GetValue(this, null);
                secondValue = property.GetValue(other, null);

                try
                {

                    if (firstValue != null)
                    {
                        if (!firstValue.Equals(secondValue))
                        {
                            result = false;
                            break;
                        }
                    }
                    else
                    {
                        if (secondValue != null)
                        {
                            result = false;
                            break;
                        }
                    }
                }
                catch (Exception)
                {
                    result = false;
                    break;
                }
            }
            return result ? 0 : 1;

        }
    }
}