using System;
using System.Data.SqlServerCe;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Text;
using BondManager.Domain;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Dialect;
using NHibernate.Tool.hbm2ddl;

namespace BankALM.Infrastructure.Data
{
    public class SessionManager
    {
        private static ISessionFactory factory;
        private static Assembly mappingAssembly;
        private static string dataBaseFilePath;
        private static Action dataInitializer;
        private static bool showSql;

        public static void Configure(Assembly newMappingAssembly, string newDataBaseFilePath, Action newDataInitializer, bool newShowSql = false)
        {
            mappingAssembly = newMappingAssembly;
            dataBaseFilePath = newDataBaseFilePath;
            dataInitializer = newDataInitializer;
            showSql = newShowSql;
        }

        private static ISessionFactory Factory
        {
            get
            {
                if (factory == null)
                {
                    CreateSessionFactory();
                }

                return factory;
            }
        }

        private static void CreateSessionFactory()
        {
            if (mappingAssembly == null || dataBaseFilePath == null)
            {
                throw new InvalidOperationException("Call Configure first");
            }

            EnsureDataBaseExists();

            string connectionString = GetConnectionString();

            Configuration configuration = GetConfiguration(connectionString);
            SchemaMetadataUpdater.QuoteTableAndColumns(configuration);
            VerifyConfiguration(configuration);
            factory = configuration.BuildSessionFactory();
        }

        public static ISession OpenSession()
        {
            return Factory.OpenSession();
        }

        public static IStatelessSession OpenStatelessSession()
        {
            return Factory.OpenStatelessSession();
        }

        private static Configuration GetConfiguration(string connectionString)
        {
            MsSqlCeConfiguration databaseConfig = MsSqlCeConfiguration.Standard.
                ConnectionString(connectionString).
                Dialect<MsSqlCe40Dialect>().
                Driver<CustomSqlServerCeDriver>().
                AdoNetBatchSize(1);
            
            if (showSql)
            {
                databaseConfig.ShowSql();
            }

            FluentConfiguration fluentConfiguration = Fluently.Configure()
                .Database(databaseConfig)
                .Mappings(m => m.FluentMappings.AddFromAssembly(mappingAssembly).
                                       Conventions.Add(new ClassConvention(),
                                                       new PropertyConvention(),
                                                       new HasManyConvention(),
                                                       new IdConvention())/*.ExportTo(@"c:\mappings")*/)
                .ExposeConfiguration(config=>
                    {
                        //string property = config.GetProperty("adonet.batch_size");
                        //config.SetProperty("adonet.batch_size", "100");
                    });

            Configuration configuration = fluentConfiguration.BuildConfiguration();

            return configuration;
        }

        private static void VerifyConfiguration(Configuration cfg)
        {
            try
            {
                new SchemaValidator(cfg).Validate();
            }
            catch (HibernateException)
            {
                UpdateSchema(cfg);
            }
        }

        private static void UpdateSchema(Configuration cfg)
        {
            StringBuilder script = new StringBuilder();

            SchemaUpdate update = new SchemaUpdate(cfg);
            update.Execute(s => script.Append(s), true);

            Debug.WriteLine(script);
        }

        public static void EnsureDataBaseExists()
        {
            if (!File.Exists(dataBaseFilePath))
            {
                using (SqlCeEngine engine = new SqlCeEngine(GetConnectionString()))
                {
                    engine.CreateDatabase();
                }

                dataInitializer();
            }
        }

        private static string GetConnectionString()
        {
            return "Data Source=" + dataBaseFilePath + ";Max Database Size = 1000;Max Buffer Size=50200;Temp File Max Size=4000;";
        }

        public static void DeleteDataBase()
        {
            if (File.Exists(dataBaseFilePath)) File.Delete(dataBaseFilePath);
            if(factory != null)
            {
                factory.Dispose();
                factory = null;   
            }            
        }
    }
}