using FluentNHibernate.Conventions;
using FluentNHibernate.Conventions.Instances;

namespace BankALM.Infrastructure.Data
{
    public class ClassConvention : IClassConvention
    {
        public void Apply(IClassInstance instance)
        {
            instance.LazyLoad();
            instance.BatchSize(20);
        }
    }
}
