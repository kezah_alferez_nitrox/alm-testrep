using System;

namespace BankALM.Infrastructure.Data
{
    public interface IGenericEntity<out TIdentity> //: IEquatable<IGenericEntity<TIdentity>>
    {
        TIdentity Id { get; }
    }
}
