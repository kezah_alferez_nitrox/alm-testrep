﻿using System;
using System.Reflection;
using System.Xml.Serialization;

namespace BankALM.Infrastructure.Data
{
    public class EntityUtil
    {
        public static bool CanCloneProperty(PropertyInfo property)
        {
            return property.Name != "Id" &&
                   (property.PropertyType.Namespace == "System" || property.PropertyType.IsValueType) &&
                   property.CanRead &&
                   property.CanWrite && property.GetCustomAttributes(typeof(XmlIgnoreAttribute), false).Length == 0;
        }


        public static uint ComputeChecksum(object obj)
        {
            if (obj == null) return 0;

            uint crc = 0;
            PropertyInfo[] properties = obj.GetType().GetProperties();
            foreach (PropertyInfo property in properties)
            {
                if (CanCloneProperty(property))
                {
                    string stringValue = Convert.ToString(property.GetValue(obj, null));
                    crc = crc ^ Crc32.ComputeChecksum(stringValue);
                }
            }

            return crc;
        }
    }
}