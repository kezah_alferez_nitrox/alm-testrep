using System;
using AssetModelingModule.Pricing;
using NUnit.Framework;

namespace AssetModelingModule.Domain.Test.Pricing
{
    [TestFixture]
    public class PricingFormulasTest
    {
        private readonly DateTime[] dates = new[]
            {
                DateTime.Parse("29/09/2010"),
                DateTime.Parse("05/10/2010"),
                DateTime.Parse("28/10/2010"),
                DateTime.Parse("28/11/2010"),
                DateTime.Parse("28/12/2010"),
                DateTime.Parse("28/03/2011"),
                DateTime.Parse("28/09/2011"),
                DateTime.Parse("28/09/2012"),
                DateTime.Parse("28/09/2013"),
                DateTime.Parse("28/09/2014"),
                DateTime.Parse("28/09/2015"),
                DateTime.Parse("28/09/2017"),
                DateTime.Parse("28/09/2020"),
                DateTime.Parse("28/09/2030"),
                DateTime.Parse("28/09/2040"),
            };

        private readonly double[] valeurs = new[]
            {
                0.50/100,
                0.55/100,
                0.60/100,
                0.65/100,
                0.78/100,
                0.90/100,
                1.10/100,
                1.30/100,
                1.60/100,
                1.80/100,
                2.00/100,
                2.50/100,
                2.80/100,
                3.00/100,
                3.20/100,
            };

        [Test]
        public void SwapPriceTest()
        {
            double price = PricingFunctions.SwapPrice(3.0/100, 0.78/100, DateTime.Parse("20/12/2015"), this.dates, this.valeurs);

            Assert.AreEqual(0.04154708, price, 0.000001);
        }
    }
}