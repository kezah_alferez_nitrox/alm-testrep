using System.Collections;
using System.Data.SqlServerCe;
using System.IO;
using System.Reflection;
using Castle.ActiveRecord;
using Castle.ActiveRecord.Framework.Config;
using NUnit.Framework;

namespace AssetModelingModule.Domain.Test
{
    public class ARTestBase
    {
        private const string DATABASE_FILE = "TestDatabase.sdf";
        private const string CONNECTION_STRING = "Data Source=" + DATABASE_FILE;

        [SetUp]
        public void SetUp()
        {
            if (File.Exists(DATABASE_FILE)) File.Delete(DATABASE_FILE);

            Hashtable properties = new Hashtable();

            properties.Add("hibernate.connection.driver_class", "NHibernate.Driver.SqlServerCeDriver");
            properties.Add("hibernate.dialect", "NHibernate.Dialect.MsSqlCeDialect");
            properties.Add("hibernate.connection.provider", "NHibernate.Connection.DriverConnectionProvider");
            properties.Add("hibernate.connection.connection_string", CONNECTION_STRING);
            properties.Add("hibernate.show_sql", "true");

            InPlaceConfigurationSource source = new InPlaceConfigurationSource();

            source.Add(typeof(ActiveRecordBase), properties);

            Assembly domain = Assembly.Load("AssetModelingModule.Domain");

            ActiveRecordStarter.Initialize(domain, source);

            using (SqlCeEngine engine = new SqlCeEngine(CONNECTION_STRING))
            {
                engine.CreateDatabase();
            }

            ActiveRecordStarter.CreateSchema();
        }

        [TearDown]
        public void TearDown()
        {
            ActiveRecordStarter.DropSchema();
            File.Delete(DATABASE_FILE);

            ActiveRecordStarter.ResetInitializationFlag();
        }
    }
}