using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Castle.ActiveRecord;
using NHibernate.Expression;

namespace AssetModelingModule.Domain
{
    [ActiveRecord]
    [Serializable]
    [XmlRoot]
    public class Currency : ActiveRecordBaseEx<Currency>
    {
        private int id;
        private string shortName;
        private string symbol;

        [PrimaryKey(PrimaryKeyType.HiLo)]
        [XmlElement]
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        [Property]
        [XmlElement]
        public string ShortName
        {
            get { return shortName; }
            set { shortName = value; }
        }

        [Property]
        [XmlElement]
        public string Symbol
        {
            get { return symbol; }
            set { symbol = value; }
        }

        public Currency Self
        {
            get { return this; }
        }

        public static Currency FindFirstBySymbol(string s)
        {
            return s == null ? FindFirst(Expression.IsNull("Symbol")) : FindFirst(Expression.Eq("Symbol", s));
        }

        private static IDictionary<string, Currency> cacheByShortName = null;
        public static Currency FindOrCreateByShortName(string shortName)
        {
            if (cacheByShortName == null)
            {
                cacheByShortName = new Dictionary<string, Currency>();

                Currency[] all = FindAll();
                foreach (Currency currency in all)
                {
                    string cachedShortName = currency.ShortName ?? "";
                    if (!cacheByShortName.ContainsKey(cachedShortName)) cacheByShortName.Add(cachedShortName, currency);
                }
            }

            shortName = shortName ?? "";
            if (!cacheByShortName.ContainsKey(shortName))
            {
                Currency currency = new Currency();
                currency.ShortName = currency.Symbol = shortName;
                currency.Create();
                cacheByShortName.Add(shortName, currency);
            }

            return cacheByShortName[shortName];
        }

        public static Currency[] FindAllNotEmpty()
        {
            return FindAll(Expression.IsNotNull("Symbol"));
        }

        public bool Equals(Currency other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return other.id == id;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof (Currency)) return false;
            return Equals((Currency) obj);
        }

        public override int GetHashCode()
        {
            return id;
        }

        public static bool operator ==(Currency left, Currency right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Currency left, Currency right)
        {
            return !Equals(left, right);
        }
    }
}
