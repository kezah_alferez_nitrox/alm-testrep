using System.Collections.Generic;
using Castle.ActiveRecord;
using NHibernate.Expression;
using System.Xml.Serialization;
using System;

namespace AssetModelingModule.Domain
{
    [ActiveRecord(Lazy = false)]
    [Serializable]
    [XmlRoot]
    public class Scenario : ActiveRecordBaseEx<Scenario>
    {
        private int id;
        private string name;
        private bool selected;
        private bool generated;
        private string description;
        private IList<VariableValue> variableValues = new List<VariableValue>();

        [PrimaryKey(PrimaryKeyType.HiLo)]
        [XmlElement]
        public virtual int Id
        {
            get { return id; }
            set { id = value; }
        }

        [Property(Unique = true)]
        [XmlElement]
        public virtual string Name
        {
            get { return name; }
            set { name = value; }
        }

        [Property]
        [XmlElement]
        public virtual bool Selected
        {
            get { return selected; }
            set { selected = value; }
        }

        [Property]
        [XmlElement]
        public virtual bool Generated
        {
            get { return generated; }
            set { generated = value; }
        }

        [Property]
        [XmlElement]
        public virtual string Description
        {
            get { return description; }
            set { description = value; }
        }

        [HasMany(Lazy = true, Inverse = true, Cascade = ManyRelationCascadeEnum.AllDeleteOrphan, BatchSize = 10)]
        [XmlIgnoreAttribute]
        public virtual IList<VariableValue> VariableValues
        {
            get { return variableValues; }
            set { variableValues = value; }
        }

        public virtual Scenario Self
        {
            get { return this; }
        }

        public override string ToString()
        {
            return Name;
        }

        public static Scenario[] FindAvailable()
        {
            return FindAll(Expression.And(
                Expression.Eq("Generated", false), 
                Expression.Eq("Selected", false)));
        }

        public static Scenario[] FindSelected()
        {
            return FindAll(Expression.And(
                Expression.Eq("Generated", false), 
                Expression.Eq("Selected", true)));
        }

        public static Scenario[] FindNonGenerated()
        {
            return FindAll(Expression.Eq("Generated", false));
        }

        public static Scenario[] FindGenerated()
        {
            return FindAll(Expression.Eq("Generated", true));
        }

        public static Scenario FindByName(string name)
        {
            return FindFirst(Expression.And(
                Expression.Eq("Generated", false), 
                Expression.Eq("Name", name)));
        }

        public static bool ExistsByName(string scenarioName)
        {
            return Exists("Generated=0 And Name = ?", scenarioName);
        }

        public static void DeleteGenerated()
        {
            int query = ExecuteDelete("from Scenario s where s.Generated=1");
        }

        public virtual bool Equals(Scenario obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return Equals(obj.name, name);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof (Scenario)) return false;
            return Equals((Scenario) obj);
        }

        public override int GetHashCode()
        {
            return (name != null ? name.GetHashCode() : 0);
        }

        public static bool operator ==(Scenario left, Scenario right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Scenario left, Scenario right)
        {
            return !Equals(left, right);
        }

        public static int GeneratedCount()
        {
            return Count(Expression.Eq("Generated", true));
        }
    }
}