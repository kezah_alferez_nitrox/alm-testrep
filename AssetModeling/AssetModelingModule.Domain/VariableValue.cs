using System;
using System.Collections.Generic;
using Castle.ActiveRecord;
using NHibernate.Expression;
using System.Xml.Serialization;

namespace AssetModelingModule.Domain
{
    [ActiveRecord(Lazy = false)]
    [Serializable]
    [XmlRoot]
    public class VariableValue : ActiveRecordBaseEx<VariableValue>
    {
        public const string ArrayPrefix = "array:";

        private int id;
        private Variable variable;
        private Scenario scenario;
        private string value;
        private bool generated;

        [PrimaryKey(PrimaryKeyType.HiLo)]
        [XmlElement]
        public virtual int Id
        {
            get { return id; }
            set { id = value; }
        }

        [BelongsTo(UniqueKey = "UniqueVariableScenario")]
        [XmlElement]
        public virtual Variable Variable
        {
            get { return variable; }
            set { variable = value; }
        }

        [BelongsTo(UniqueKey = "UniqueVariableScenario")]
        [XmlElement]
        public virtual Scenario Scenario
        {
            get { return scenario; }
            set { scenario = value; }
        }

        [Property(Length = 32000)]
        [XmlElement]
        public virtual string Value
        {
            get { return value; }
            set { this.value = value; }
        }

        [Property]
        [XmlElement]
        public virtual bool Generated
        {
            get { return generated; }
            set { this.generated = value; }
        }

        public virtual bool ValueIsArray()
        {
            return this.Value != null && this.Value.StartsWith(ArrayPrefix);
        }

        public virtual decimal[] GetArrayValue()
        {
            string[] arrayStrings = this.Value.Substring(7).Split(';');
            decimal[] result = new decimal[arrayStrings.Length];
            for (int i = 0; i < arrayStrings.Length; i++)
            {
                decimal decimalValue;
                decimal.TryParse(arrayStrings[i], out decimalValue);

                result[i] = decimalValue;
            }
            return result;
        }

        public static VariableValue Find(Variable variable, Scenario scenario)
        {
            return FindFirst(Expression.And(Expression.And(
                                 Expression.Eq("Variable", variable),
                                 Expression.Eq("Scenario", scenario)),
                                 Expression.Eq("Generated", false)));
        }

        public static VariableValue[] FindByVariable(Variable variable)
        {
            return FindAll(Expression.And(
                Expression.Eq("Variable", variable),
                Expression.Eq("Generated", false)));
        }

        public static VariableValue[] FindByScenario(Scenario scenario)
        {
            return FindAll(Expression.And(
                Expression.Eq("Scenario", scenario),
                Expression.Eq("Generated", false)));
        }
        public static VariableValue[] FindGenerated()
        {
            return FindAll(Expression.Eq("Generated", true));
        }

        public static void DeleteGenerated()
        {
            //string hql = "from VariableValue vv where exists(from Scenario s where vv.Scenario=s and s.Generated=1)";
            //var query = new SimpleQuery<VariableValue>(hql);

            IEnumerable<VariableValue> allGenerated = FindGenerated();
            foreach (VariableValue variableValue in allGenerated)
            {
                variableValue.Variable.VariableValues.Remove(variableValue);
                variableValue.Scenario.VariableValues.Remove(variableValue);
                variableValue.Delete();
            }
        }
    }
}