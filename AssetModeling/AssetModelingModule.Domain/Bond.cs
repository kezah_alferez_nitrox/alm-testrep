using System;
using Castle.ActiveRecord;
using Castle.ActiveRecord.Queries;
using System.Xml.Serialization;
using Eu.AlterSystems.ASNetLib.Core;
using NHibernate.Expression;

namespace AssetModelingModule.Domain
{
    [ActiveRecord(Lazy = false)]
    [Serializable]
    [XmlRoot]
    public class Bond : ActiveRecordBaseEx<Bond>
    {
        public enum BondStatus
        {
            None = 0,
            OK,
            Warning,
            Error
        }

        private int id;
        private Category category;
        private bool exclude;
        private BondStatus status;
        private string secId;
        private string description;
        private BondType type;
        //MTU private decimal? growth;
        private string growth;
        private BondAccounting accounting;
        private decimal? origFace;
        private string currFace;
        private decimal? bookPrice = 100.00m;
        private decimal? bookValue;
        private string marketPrice;
        private int aLMID;
        private string rollSpec;
        private string coupon;
        private BondPeriod period;
        private decimal? duration;
        private BondTax tax;
        private BondAttrib attrib;
        private bool isModel;
        private string cpr;
        private string cdr;
        private string theoreticalCurrFace;
        private string lgd;
        private decimal? basel2;
        private Currency currency;
        private string tci;
        private decimal? rwa;
        private bool isTotal;
        private bool? isVisibleChecked;

        public Bond()
        {
            this.isTotal = false;
            this.type = BondType.BULLET;
            this.accounting = BondAccounting.LAndR;
            this.period = BondPeriod.Quarterly;
            this.tax = BondTax.Standard;
            this.attrib = BondAttrib.InBalance;
            this.DateIssue = DateTime.Now;
            this.Maturity = DateTime.Now;
            this.Effective = DateTime.Now;
            this.FirstCouponDate = DateTime.Now;
        }

        [PrimaryKey(PrimaryKeyType.HiLo)]
        [XmlElement]
        public virtual int Id
        {
            get { return id; }
            set { id = value; }
        }

        [BelongsTo]
        [XmlElement]
        public virtual Category Category
        {
            get { return category; }
            set { category = value; }
        }

        [Property]
        [XmlElement]
        public virtual bool Exclude
        {
            get { return exclude; }
            set { exclude = value; }
        }

        [Property]
        [XmlElement]
        public virtual BondStatus Status
        {
            get { return status; }
            set { status = value; }
        }

        [Property]
        [XmlElement]
        public virtual string SecId
        {
            get { return secId; }
            set { secId = value; }
        }

        [Property]
        [XmlElement]
        public virtual string Description
        {
            get { return description; }
            set { description = value; }
        }

        [Property]
        [XmlElement]
        public virtual BondType Type
        {
            get { return type; }
            set { type = value; }
        }

        [Property]
        [XmlElement]
        public virtual string Growth
        {
            get { return growth; }
            set { growth = value; }
        }

        [Property]
        [XmlElement]
        public virtual BondAccounting Accounting
        {
            get { return accounting; }
            set { accounting = value; }
        }

        [Property]
        [XmlElement]
        public virtual decimal? OrigFace
        {
            get { return origFace; }
            set { origFace = value; }
        }

        [Property]
        [XmlElement]
        public virtual string CurrFace
        {
            get { return currFace; }
            set { currFace = value; }
        }

        [Property]
        [XmlElement]
        public virtual decimal? BookPrice
        {
            get { return bookPrice; }
            set { bookPrice = value; }
        }

        [Property]
        [XmlElement]
        public virtual decimal? BookValue
        {
            get { return bookValue; }
            set { bookValue = value; }
        }

        [Property]
        [XmlElement]
        public virtual string MarketPrice
        {
            get { return marketPrice; }
            set { marketPrice = value; }
        }

        [Property]
        [XmlElement]
        public virtual int ALMID
        {
            get { return aLMID; }
            set { aLMID = value; }
        }

        public virtual void GenerateALMID()
        {
            aLMID = GetMaxALMIDForCategory(category) + 1;
        }

        public virtual string ALMIDDisplayed
        {
            get { return (category == null) ? "" : string.Format("{0}{1:00#}", category.Code, aLMID); }
        }

        [Property]
        [XmlElement]
        public virtual string RollSpec
        {
            get { return rollSpec; }
            set { rollSpec = value; }
        }

        [Property]
        [XmlElement]
        public virtual string Coupon
        {
            get { return coupon; }
            set { coupon = value; }
        }

        [Property]
        [XmlElement]
        public virtual BondAttrib Attrib
        {
            get { return attrib; }
            set { attrib = value; }
        }

        [Property]
        [XmlElement]
        public virtual bool IsModel
        {
            get { return isModel; }
            set { isModel = value; }
        }

        [Property]
        [XmlElement]
        public virtual string CDR
        {
            get { return cdr; }
            set { cdr = value; }
        }

        [Property]
        [XmlElement]
        public virtual string CPR
        {
            get { return cpr; }
            set { cpr = value; }
        }

        [Property]
        [XmlElement]
        public virtual string TheoreticalCurrFace
        {
            get { return theoreticalCurrFace; }
            set { theoreticalCurrFace = value; }
        }

        [Property]
        [XmlElement]
        public virtual string LGD
        {
            get { return lgd; }
            set { lgd = value; }
        }

        [Property]
        [XmlElement]
        public virtual decimal? Basel2
        {
            get { return basel2; }
            set { basel2 = value; }
        }

        [Property]
        [XmlElement]
        public virtual BondPeriod Period
        {
            get { return period; }
            set { period = value; }
        }

        [Property]
        [XmlElement]
        public virtual decimal? Duration
        {
            get { return duration; }
            set { duration = value; }
        }

        [Property]
        [XmlElement]
        public virtual BondTax Tax
        {
            get { return tax; }
            set { tax = value; }
        }

        [Property]
        [XmlElement]
        public virtual string TCI
        {
            get { return tci; }
            set { tci = value; }
        }


        [Property]
        [XmlElement]
        public virtual bool? IsVisibleChecked
        {
            get { return isVisibleChecked; }
            set { isVisibleChecked = value; }
        }

        [BelongsTo]
        [XmlElement]
        public virtual Currency Currency
        {
            get { return currency; }
            set { currency = value; }
        }

       

        public virtual decimal? RWA
        {
            get { return IsTotal ? rwa : (Basel2.HasValue ? Basel2 * BookValue : null); }
            set { rwa = value; }
        }

        public virtual bool IsTotal
        {
            get { return isTotal; }
            set { isTotal = value; }
        }

        private decimal? ComputeBookValueInternal()
        {
            decimal? ret;
            try
            {
                ret = CurrFaceValue * BookPrice.Value / 100;
            }
            catch (OverflowException)
            {
                ret = null;
            }
            return ret;
        }

        public virtual decimal CurrFaceValue
        {
            get
            {
                decimal value;
                if (decimal.TryParse(CurrFace, out value))
                {
                    return value;
                }

                // todo ?!

                Vector vector = Vector.FindByName(CurrFace);
                if (vector == null || vector.VectorPoints.Count == 0) return 0;

                return vector.VectorPoints[0].Value;
            }
        }

        [Property]
        [XmlElement]
        public virtual BondFinancialType BondFinancialType { get; set; }

        [BelongsTo]
        [XmlElement]
        public virtual Bond SwapBondLink { get; set; }

        [Property]
        [XmlElement]
        public virtual string Name { get; set; }

        [Property]
        [XmlElement]
        public virtual string BondId { get; set; }

        [Property]
        [XmlElement]
        public virtual string Notional { get; set; }

        [Property]
        [XmlElement]
        public virtual TypeId TypeId { get; set; }

        [Property]
        [XmlElement]
        public virtual DateTime? DateIssue { get; set; }

        [Property]
        [XmlElement]
        public virtual DateTime? Maturity { get; set; }
        
        [Property]
        [XmlElement]
        public virtual CouponType CouponType { get; set; }

        [BelongsTo]
        [XmlElement]
        public virtual Variable BondIndex { get; set; }

        [Property]
        [XmlElement]
        public virtual int? Frequence { get; set; }

        [Property]
        [XmlElement]
        public virtual DateTime? FirstCouponDate { get; set; }

        [Property]
        [XmlElement]
        public virtual decimal? IssuedBookPrice { get; set; }

        [Property]
        [XmlElement]
        public virtual DayCountConvention DayCountConvention { get; set; }

        [Property]
        [XmlElement]
        public virtual DateTime? Effective { get; set; }

        [Property]
        [XmlElement]
        public virtual decimal? Spread { get; set; }

        [Property]
        [XmlElement]
        public virtual decimal? LatestIndex { get; set; }

        [Property]
        [XmlElement]
        public virtual BondPeriod ResetFrequence { get; set; }

        [Property]
        [XmlElement]
        public virtual BondSwapLeg Leg { get; set; }

        [Property]
        [XmlElement]
        public virtual int? Tenor { get; set; }

        [Property]
        [XmlElement]
        public virtual decimal? Strike { get; set; }

        public virtual void UpdateStatusProperty()
        {
            if (string.IsNullOrEmpty(CurrFace) || !BookPrice.HasValue)
            {
                Status = BookValue.HasValue ? BondStatus.Warning : BondStatus.Error;
            }
            else
            {
                if (BookValue.HasValue)
                {
                    Status = (BookValue.Value == ComputeBookValueInternal()) ? BondStatus.OK : BondStatus.Warning;
                }
                else
                {
                    Status = BondStatus.Error;
                }
            }
        }

        public virtual void ComputeBookValue()
        {
            BookValue = ComputeBookValueInternal();
        }

        // optimisation: copy BalanceType and IsRollAccount properties to Bond
        public static int GetMaxALMIDForCategory(Category category)
        {
            ScalarQuery<int> query;
            string hql =
                @"select max(b.ALMID)
                    from Bond b
                    inner join b.Category c
                    where                         
                ";
            if (category.Type == CategoryType.Roll)
            {
                hql += " c.Type = :type";
                query = new ScalarQuery<int>(typeof (Bond), hql);
                query.SetParameter("type", category.Type);
            }
            else
            {
                hql += " c.BalanceType = :balanceType";
                query = new ScalarQuery<int>(typeof(Bond), hql);
                query.SetParameter("balanceType", (int)category.BalanceType);
            }

            object countObj = query.Execute();
            int count = (int)countObj;

            return count;
        }

        public static Bond FindFirstCashAdjByBalanceTypeAndAttrib(BalanceType balanceType, BondAttrib attrib)
        {
            SimpleQuery<Bond> query = new SimpleQuery<Bond>(
                @"from Bond b
                    where
                        b.Category.BalanceType = :balanceType
                        and Attrib = :attrib
                ");

            query.SetParameter("balanceType", balanceType);
            query.SetParameter("attrib", attrib);
            Bond[] bonds = query.Execute();

            return bonds.Length > 0 ? bonds[0] : null;
        }

        public static Bond[] GetBondsByTypeAndAtrib(BalanceType balanceType, BondAttrib bondAtrib)
        {
            SimpleQuery<Bond> query = new SimpleQuery<Bond>(
                 @"from Bond b
                    where
                        b.Category.BalanceType = :balanceType
                        and Attrib = :attrib
                ");

            query.SetParameter("balanceType", balanceType);
            query.SetParameter("attrib", bondAtrib);
            Bond[] bonds = query.Execute();

            return bonds;
        }

        public static Bond[] FindByCategory(Category category)
        {
            return FindAll(Expression.Eq("Category", category));
        }

        public static Bond FindByID(int BondID)
        {
            return FindOne(Expression.Eq("Id", BondID));
        }

        public static Bond[] FindIsModel()
        {
            return FindAll(Expression.Eq("IsModel", true));
        }

        public static Bond[] FindAllNotExcluded()
        {
            return FindAll(Expression.Eq("Exclude", false));
        }

        public static Bond[] FindByAttrib(BondAttrib attrib)
        {
            return FindAll(Expression.Eq("Attrib", attrib));
        }
    }

    public enum BondType
    {
        [EnumDescription("BULLET")]
        BULLET,
        [EnumDescription("GROWTH")]
        GROWTH,
        [EnumDescription("LINEAR AMORT")]
        LINEAR_AMORT,
        [EnumDescription("CST PAY")]
        CST_PAY,
        [EnumDescription("VAR")]
        VAR,
    }

    public enum BondAccounting
    {
        [EnumDescription("L&R")]
        LAndR,
        [EnumDescription("HTM")]
        HTM,
        [EnumDescription("AFS")]
        AFS,
        [EnumDescription("HFT")]
        HFT,
        [EnumDescription("Other")]
        Other
    }

    public enum BondAttrib
    {
        [EnumDescription("In Balance")]
        InBalance,
        [EnumDescription("Notional")]
        Notional,
        [EnumDescription("Roll")]
        Roll,
        [EnumDescription("Cash Adj")]
        CashAdj,
        [EnumDescription("Treasury")]
        Treasury,
        [EnumDescription("Impairments")]
        Impairments,
        [EnumDescription("Receivable")]
        Receivable,
        [EnumDescription("Reserves")]
        Reserves,
        [EnumDescription("Other Comprehensive Income")]
        OtherComprehensiveIncome,
    }

    public enum BondTax
    {
        [EnumDescription("Standard")]
        Standard,
        [EnumDescription("Non Taxable")]
        NonTaxable,
        [EnumDescription("Long Term")]
        LongTerm,
        [EnumDescription("Special")]
        Special,
    }

    public enum BondPeriod
    {
        [EnumDescription("Monthly")]
        Monthly = 12,
        [EnumDescription("Quarterly")]
        Quarterly = 4,
        [EnumDescription("Semi Annual")]
        SemiAnnual = 2,
        [EnumDescription("Annual")]
        Annual = 1,
        [EnumDescription("At maturity")]
        AtMaturity = 0,
    }

    public enum BondFinancialType
    {
        [EnumDescription("Bond")]
        Bond = 0,
        [EnumDescription("Swap")]
        Swap = 1,
        [EnumDescription("Cap")]
        Cap = 2,
        [EnumDescription("Floor")]
        Floor = 3,
    }

    public enum TypeId
    {
        [EnumDescription("ISIN")]
        Isin = 0,
        [EnumDescription("Internal")]
        Internal = 1
    }

    public enum CouponType
    {
        [EnumDescription("Fixed")]
        Isin = 0,
        [EnumDescription("Variable")]
        Variable = 1,
        [EnumDescription("Formula")]
        Formula = 2
    }

    public enum DayCountConvention
    {
        [EnumDescription("30/360 US")]
        Convention30Us = 0,
        [EnumDescription("30E/360")]
        Convention30E = 1,
        [EnumDescription("30E/360 ISDA")]
        Convention30EISDA = 2,
        [EnumDescription("30E+/360")]
        Convention30EPlus = 3,
        [EnumDescription("Actual/Actual ICMA")]
        ConventionActualIcma = 4,
        [EnumDescription("Actual/Actual ISDA")]
        ConventionActualIsda = 5,
        [EnumDescription("Actual/365 Fixed")]
        ConventionActual365Fixed = 6,
        [EnumDescription("Actual/360")]
        ConventionActual360 = 7,
        [EnumDescription("Actual/365L")]
        ConventionActual365L = 8,
        [EnumDescription("Actual/Actual AFB")]
        ConventionActualAfb = 9
    }

    public enum BondSwapLeg
    {
        [EnumDescription("Received Fixed")]
        ReceivedFixed = 0,
        [EnumDescription("Pay Fixed")]
        PayFixed = 1,
        [EnumDescription("Received Float")]
        ReceivedFloat = 2,
        [EnumDescription("Pay Float")]
        PayFloat = 3
    }

    public enum BoundIndexType
    {
        [EnumDescription("EURO03M")]
        Euro03M = 0,
        [EnumDescription("EURO06M")]
        Euro06M = 1
    }
}