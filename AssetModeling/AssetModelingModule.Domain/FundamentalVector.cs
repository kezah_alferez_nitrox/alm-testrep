using Castle.ActiveRecord;
using NHibernate.Expression;

namespace AssetModelingModule.Domain
{
    [ActiveRecord]
    public class FundamentalVector : ActiveRecordBaseEx<FundamentalVector>
    {
        [PrimaryKey(PrimaryKeyType.HiLo)]
        public int Id { get; set; }

        [Property]
        public string Name { get; set; }

        [Property]
        public string Formula { get; set; }

        [Property]
        public int Position { get; set; }

        public new static FundamentalVector[] FindAll()
        {
            return FindAll(Order.Asc("Position"));
        }
    }

    public enum TimeUnit
    {
        Days,
        Months,
        Years
    }
}