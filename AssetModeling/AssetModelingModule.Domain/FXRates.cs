using Castle.ActiveRecord;
using NHibernate.Expression;

namespace AssetModelingModule.Domain
{
    [ActiveRecord]
    public class FXRate : ActiveRecordBase<FXRate>
    {
        private int id;
        private Currency from;
        private Currency to;
        private string value;

        [PrimaryKey(PrimaryKeyType.HiLo)]
        public virtual int Id
        {
            get { return id; }
            set { id = value; }
        }

        [BelongsTo("[From]", UniqueKey = "UniqueCurrencyPairFXRate")]
        public virtual Currency From
        {
            get { return from; }
            set { from = value; }
        }

        [BelongsTo("[To]", UniqueKey = "UniqueCurrencyPairFXRate")]
        public virtual Currency To
        {
            get { return to; }
            set { to = value; }
        }

        [Property]
        public string Value
        {
            get { return value; }
            set { this.value = value; }
        }

        public static FXRate GetRate(Currency from, Currency to)
        {
            return FindFirst(Expression.And(Expression.Eq("From", from), Expression.Eq("To", to)));
        }
    }
}