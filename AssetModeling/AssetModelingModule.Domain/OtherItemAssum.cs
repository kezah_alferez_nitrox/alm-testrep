using Castle.ActiveRecord;
using Castle.ActiveRecord.Queries;

namespace AssetModelingModule.Domain
{
    [ActiveRecord]
    public class OtherItemAssum : ActiveRecordBase<OtherItemAssum>
    {
        private int id;
        private string descrip;
        private string vect;

        private decimal[] values;

        [PrimaryKey(PrimaryKeyType.HiLo)]
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        [Property]
        public string Descrip
        {
            get {return descrip;}
            set {descrip = value;}
        }

        [Property]
        public string Vect
        {
            get { return vect; }
            set { vect = value; }
        }

        public decimal[] Values
        {
            get { return values; }
            set { values = value; }
        }

        private bool? isVisibleChecked;
        [Property]
        public bool? IsVisibleChecked
        {
            get { return isVisibleChecked; }
            set { isVisibleChecked = value; }
        }

        private string subtotalGroup;
        
        [Property]
        public string SubtotalGroup
        {
            get { return subtotalGroup; }
            set { subtotalGroup = value; }
        }
        
        private string status;

        [Property]
        public string Status
        {
            get { return status; }
            set { status = value; }
        }

        public static OtherItemAssum[] findDataByStatus(string stat)
        {
            return FindAllByProperty("Status", stat);
        }

        public bool Equals(OtherItemAssum obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.id == id && Equals(obj.descrip, descrip);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof (OtherItemAssum)) return false;
            return Equals((OtherItemAssum) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (id*397) ^ (descrip != null ? descrip.GetHashCode() : 0);
            }
        }

        public static bool operator ==(OtherItemAssum left, OtherItemAssum right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(OtherItemAssum left, OtherItemAssum right)
        {
            return !Equals(left, right);
        }

        public static OtherItemAssum[] findBySubtotalGroup(string SubtotalGroup)
        {
            return FindAllByProperty("SubtotalGroup", SubtotalGroup);
        }

        public static OtherItemAssum findByID(int OiaID)
        {
            return FindOne(NHibernate.Expression.Expression.Eq("Id", OiaID));
        }

        public static string[] GetAllSubgroups()
        {
            //List<string> allsubgroups = new List<string>();

            
            SimpleQuery<string> query = new SimpleQuery<string>(typeof(OtherItemAssum),
               @"select distinct oia.SubtotalGroup from OtherItemAssum oia
                ");

            string[] allsubgoups = query.Execute();
            return allsubgoups;
        }
    }
}
