using System;
using System.Collections.Generic;
using Castle.ActiveRecord;
using Castle.ActiveRecord.Queries;
using NHibernate.Expression;
using System.Xml.Serialization;

namespace AssetModelingModule.Domain
{
    [ActiveRecord(Lazy = false)]
    [Serializable]
    [XmlRoot]
    public class Vector : ActiveRecordBaseEx<Vector>
    {
        private int id;
        private string name;
        private VectorGroup group;
        private VectorFrequency frequency;
        private Currency currency;
        private IList<VectorPoint> vectorPoints = new List<VectorPoint>();
        private bool importantMarketData;

        [PrimaryKey(PrimaryKeyType.HiLo)]
        [XmlElement]
        public virtual int Id
        {
            get { return id; }
            set { id = value; }
        }

        [Property(Unique = true)]
        [XmlElement]
        public virtual string Name
        {
            get { return name; }
            set { name = value; }
        }

        [Property("[Group]", Index = "IDX_GROUP")]
        [XmlElement]
        public virtual VectorGroup Group
        {
            get { return group; }
            set { group = value; }
        }

        [Property]
        [XmlElement]
        public virtual VectorFrequency Frequency
        {
            get { return frequency; }
            set { frequency = value; }
        }

        [BelongsTo]
        [XmlElement]
        public virtual Currency Currency
        {
            get { return currency; }
            set { currency = value; }
        }

        [HasMany(Lazy = true, Inverse = true, Cascade = ManyRelationCascadeEnum.AllDeleteOrphan, BatchSize = 10, OrderBy = "Date")]
        [XmlIgnoreAttribute]
        public virtual IList<VectorPoint> VectorPoints
        {
            get { return vectorPoints; }
            set { vectorPoints = value; }
        }

        [Property]
        [XmlElement]
        public virtual bool ImportantMarketData
        {
            get { return importantMarketData; }
            set { importantMarketData = value; }
        }

        public virtual Vector Self
        {
            get { return this; }
        }

        public bool Equals(Vector other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return other.id == id;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof(Vector)) return false;
            return Equals((Vector)obj);
        }

        public override int GetHashCode()
        {
            return id;
        }

        public static bool operator ==(Vector left, Vector right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Vector left, Vector right)
        {
            return !Equals(left, right);
        }

        public virtual Vector Copy()
        {
            Vector vectorCopy = new Vector();
            vectorCopy.Name = Name;
            vectorCopy.Group = Group;
            vectorCopy.Frequency = Frequency;
            foreach (VectorPoint point in vectorPoints)
            {
                VectorPoint vectorPointCopy = point.Copy();
                vectorPointCopy.Vector = vectorCopy;
                vectorCopy.VectorPoints.Add(vectorPointCopy);
            }

            return vectorCopy;
        }

        public static void DeleteByGroup(VectorGroup group)
        {
            // todo
            foreach (Vector vector in Vector.FindByGroup(group))
            {
                Vector.DeleteAndFlush(vector);
            }
        }

        public static Vector[] FindByGroup(VectorGroup group)
        {
            return FindAllByProperty("Group", group);
        }

        public static Vector[] FindByImportantMarketData()
        {
            return FindAllByProperty("ImportantMarketData", true);
        }

        public static Vector FindByName(string name)
        {
            return FindFirst(Expression.Eq("Name", name));
        }

        public static Vector[] FindLikeName(string name)
        {
            return FindAll(Expression.Like("Name", name, MatchMode.Start));
        }

        public static Vector FindById(int id)
        {
            return FindFirst(Expression.Eq("Id", id));
        }

        public virtual VectorPoint GetCurrentValue(DateTime date)
        {
            date = date.Date;
            DateTime dateMin = date;
            DateTime dateMax = date.AddDays(1);
            switch (frequency)
            {
                case VectorFrequency.None:
                    break;
                case VectorFrequency.Monthly:
                    dateMin = dateMin.AddMonths(-1);
                    break;
                case VectorFrequency.Quaterly:
                    dateMin = dateMin.AddMonths(-3);
                    break;
                case VectorFrequency.SemiAnnual:
                    dateMin = dateMin.AddMonths(-6);
                    break;
                case VectorFrequency.Annual:
                    dateMin = dateMin.AddMonths(-12);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            SimpleQuery<VectorPoint> query = new SimpleQuery<VectorPoint>(
                @"from VectorPoint vp
                where
                    vp.Vector = :this
                    and vp.Date >= :dateMin
                    and vp.Date < :dateMax
                ");

            query.SetParameter("this", this);
            query.SetParameter("dateMin", dateMin);
            query.SetParameter("dateMax", dateMax);
            VectorPoint[] points = query.Execute();

            return points.Length > 0 ? points[0] : null;
        }

        public decimal GetValueAtDate(DateTime date)
        {
            if (this.VectorPoints.Count == 0) return 0;

            int i = 0;
            while (i < (this.VectorPoints.Count - 1) && date > this.VectorPoints[i].Date)
            {
                i++;
            }

            return this.VectorPoints[i].Value;
        }
    }

    public enum VectorGroup
    {
        Workspace,
        MarketData,
        MarketVol
    }

    public enum VectorFrequency
    {
        None,
        Monthly,
        Quaterly,
        SemiAnnual,
        Annual,
        AtMaturity,
    }
}