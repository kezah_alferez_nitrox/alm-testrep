using System;
using System.Globalization;
using Castle.ActiveRecord;

namespace AssetModelingModule.Domain
{
    [ActiveRecord]
    public class Param : ActiveRecordBase<Param>
    {
        private const string VALUATION_CURRENCY = "Valuation Currency";
        private const string VALUATION_DATE = "Valuation Date";
        private const string STANDARD_TAX = "Standard Rate";
        private const string LONG_TERM_TAX = "Long Term";
        private const string SPECIAL_TAX = "Special";
        private const string FILE_NAME = "FileName";
        private const string DATE_FORMAT = "o";

        private string id;
        private string val;

        [PrimaryKey(PrimaryKeyType.Assigned)]
        public string Id
        {
            get { return id; }
            set { id = value; }
        }

        [Property]
        public string Val
        {
            get { return val; }
            set { val = value; }
        }

        public virtual Param Copy()
        {
            Param copy = new Param();
            copy.Id = this.Id;
            copy.Val = this.Val;
            return copy;
        }

        private static Currency valuationCurrency = null;
        public static Currency ValuationCurrency
        {
            get
            {
                if (valuationCurrency == null)
                {
                    Param param = FindByPrimaryKey(VALUATION_CURRENCY, false);
                    if (param == null || param.Val == null) return null;

                    int currencyId = int.Parse(param.Val);
                    try
                    {
                        valuationCurrency = Currency.Find(currencyId);
                    }
                    catch (Exception)
                    {
                        // todo
                    }
                }
                return valuationCurrency;
            }
            set
            {
                valuationCurrency = value;

                Param valuationCurrencyParam = FindByPrimaryKey(VALUATION_CURRENCY);
                if (valuationCurrencyParam == null)
                {
                    valuationCurrencyParam = new Param();
                    valuationCurrencyParam.Id = VALUATION_CURRENCY;
                    valuationCurrencyParam.CreateAndFlush();
                }
                valuationCurrencyParam.Val = value.Id.ToString();
                valuationCurrencyParam.Update();
            }
        }

        public static string FileName
        {
            get
            {
                Param param = FindByPrimaryKey(FILE_NAME, false);
                if (param == null || param.Val == null) return null;

                string filename = param.Val;
                return filename;
            }
            set
            {
                Param param = FindByPrimaryKey(FILE_NAME, false);
                if (param == null)
                {
                    param = new Param();
                    param.Id = FILE_NAME;
                    param.CreateAndFlush();
                }
                param.Val = value == null ? null : value.ToString();
                param.Update();
            }
        }

        public static DateTime? ValuationDate
        {
            get
            {
                Param param = FindByPrimaryKey(VALUATION_DATE, false);
                if (param == null || param.Val == null) return null;

                DateTime valuationDate;
                if (!DateTime.TryParseExact(param.Val, DATE_FORMAT, null, DateTimeStyles.AdjustToUniversal, out valuationDate))
                {
                    return null;
                }

                return valuationDate;
            }
            set
            {
                Param param = FindByPrimaryKey(VALUATION_DATE, false);
                if (param == null)
                {
                    param = new Param();
                    param.Id = VALUATION_DATE;
                    param.CreateAndFlush();
                }
                param.Val = value == null ? null : value.Value.ToString(DATE_FORMAT);
                param.Update();
            }
        }

        private static decimal GetDecimalParamByName(string name)
        {
            Param param = FindByPrimaryKey(name, false);
            if (param == null || string.IsNullOrEmpty(param.Val)) return 0;

            decimal value;
            return decimal.TryParse(param.Val, out value) ? value : 0;
        }

        private static void SetDecimalParamByName(string name, decimal value)
        {
            Param param = FindByPrimaryKey(name, false);
            if (param == null)
            {
                param = new Param();
                param.Id = name;
                param.CreateAndFlush();
            }
            param.Val = value.ToString();
            param.Update();
        }

        public static decimal StandardTax
        {
            get
            {
                return GetDecimalParamByName(STANDARD_TAX);
            }
            set
            {
                SetDecimalParamByName(STANDARD_TAX, value);
            }
        }

        public static decimal LongTermTax
        {
            get
            {
                return GetDecimalParamByName(LONG_TERM_TAX);
            }
            set
            {
                SetDecimalParamByName(LONG_TERM_TAX, value);
            }
        }

        public static decimal SpecialTax
        {
            get
            {
                return GetDecimalParamByName(SPECIAL_TAX);
            }
            set
            {
                SetDecimalParamByName(SPECIAL_TAX, value);
            }
        }

        public static Param FindByID(string ParamId)
        {
            return FindOne(NHibernate.Expression.Expression.Eq("Id", ParamId));
        }

    }
}
