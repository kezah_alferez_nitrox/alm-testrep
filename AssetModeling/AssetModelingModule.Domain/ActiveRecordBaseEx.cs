using System;
using System.Reflection;
using Castle.ActiveRecord;
using NHibernate;

namespace AssetModelingModule.Domain
{
    public class ActiveRecordBaseEx<T> : ActiveRecordBase<T>
    {
        public virtual T ClonePropertiesOnly()
        {
            if (!typeof(T).IsAbstract)
            {
                T newObj = Activator.CreateInstance<T>();
                PropertyInfo[] properties = typeof(T).GetProperties();
                foreach (PropertyInfo property in properties)
                {
                    //Property
                    if (property.GetCustomAttributes(typeof(PropertyAttribute), true).Length == 1)
                    {
                        property.SetValue(newObj, property.GetValue(this, null), null);
                    }
                    ////Has many
                    //else if (property.GetCustomAttributes(typeof(HasManyAttribute), true).Length == 1)
                    //{
                    //}
                    ////Has and belongs to many
                    //else if (property.GetCustomAttributes(typeof(HasAndBelongsToManyAttribute), true).Length == 1)
                    //{
                    //}
                    ////Belongs to
                    //else if (property.GetCustomAttributes(typeof(BelongsToAttribute), true).Length == 1)
                    //{
                    //}   
                }
                return newObj;
            }
            else
            {
                return default(T);
            }
        }

        public static int ExecuteDelete(string hql)
        {
            object execute = Execute(delegate(ISession session, object instance)
                {
                    int results = session.Delete(hql);
                    return results;
                }, null);
            return (int) execute;
        }
    }
}