using System;
using Castle.ActiveRecord;
using System.Xml.Serialization;
using Castle.ActiveRecord.Queries;

namespace AssetModelingModule.Domain
{
    [ActiveRecord(Lazy = false)]
    [Serializable]
    [XmlRoot]
    public class VectorPoint : ActiveRecordBaseEx<VectorPoint>
    {
        private int id;
        private Vector vector;
        private DateTime date = DateTime.Now;
        private decimal value;

        [PrimaryKey(PrimaryKeyType.HiLo)]
        [XmlElement]
        public virtual int Id
        {
            get { return id; }
            set { id = value; }
        }

        [BelongsTo]
        [XmlElement]
        public virtual Vector Vector
        {
            get
            {
                return vector;
            }
            set
            {
                vector = value;
            }
        }

        [Property]
        [XmlElement]
        public virtual DateTime Date
        {
            get
            {
                return date;
            }
            set
            {
                date = value;
            }
        }

        [Property]
        [XmlElement]
        public virtual decimal Value{ 
            get
            {
                return value;
            } 
            set
            {
                this.value = value;
            }
        }

        public virtual VectorPoint Copy()
        {
            VectorPoint newVectorPoint = new VectorPoint();
            newVectorPoint.Date = Date;
            newVectorPoint.Value = Value;

            return newVectorPoint;
        }

        public static VectorPoint[] FindByVectorGroup(VectorGroup group)
        {
            SimpleQuery<VectorPoint> query = new SimpleQuery<VectorPoint>(
                @"from VectorPoint vp
                    where
                        vp.Vector.Group = :group
                ");

            query.SetParameter("group", group);
            VectorPoint[] vectorPoints = query.Execute();

            return vectorPoints;
        }

        public static void DeleteByGroup(VectorGroup group)
        {
            foreach (Vector vector in Vector.FindByGroup(group))
            {
                foreach (VectorPoint vectorpoint in vector.VectorPoints )
                    VectorPoint.Delete(vectorpoint);
            }
        }

        public static VectorPoint[] FindByVector(Vector vector)
        {
            return FindAllByProperty("Vector", vector);
        }

        public static VectorPoint[] FindByDate(DateTime date)
        {
            return FindAllByProperty("Date", date);
        }

        public static VectorPoint FindByMonthYear(int year, int month, Vector vector)
        {
            if (vector == null) return null;

            foreach( VectorPoint _vp in vector.VectorPoints)
            {
                if (_vp.date.Year == year && _vp.date.Month == month)
                    return _vp;
            }
            return null;
        }
    }
}