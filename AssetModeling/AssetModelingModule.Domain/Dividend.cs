using Castle.ActiveRecord;

namespace AssetModelingModule.Domain
{
    [ActiveRecord]
    public class Dividend : ActiveRecordBase<Dividend>
    {

        private int id;

        [PrimaryKey(PrimaryKeyType.HiLo)]
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        private string name;

        [Property]
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        private string amount;

        [Property]
        public string Amount
        {
            get { return amount; }
            set { amount = value; }
        }

        private decimal? minimum;

        [Property]
        public decimal? Minimum
        {
            get { return minimum; }
            set { minimum = value; }
        }

        private string status;

        [Property]
        public string Status
        {
            get { return status; }
            set { status = value; }
        }

        public static Dividend[] FindByStatus(string stats)
        {
            return FindAllByProperty("Status", stats);
        }
        
        public bool IsValueAmount
        {
            get
            {
                decimal temp;
                return decimal.TryParse(Amount, out temp);
            }
        }

        public bool Equals(Dividend obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.id == id && Equals(obj.name, name);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof (Dividend)) return false;
            return Equals((Dividend) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (id*397) ^ (name != null ? name.GetHashCode() : 0);
            }
        }
    }
}
