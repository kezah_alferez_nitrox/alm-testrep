using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Castle.ActiveRecord;
using Castle.ActiveRecord.Queries;
using Eu.AlterSystems.ASNetLib.Core;
using NHibernate.Expression;

namespace AssetModelingModule.Domain
{
    [ActiveRecord(Lazy = false)]
    [Serializable]
    [XmlRoot]
    public class Category : ActiveRecordBaseEx<Category>
    {
        private int id;
        private string label;
        private Category parent;
        private IList<Bond> bonds = new List<Bond>();
        private IList<Category> children = new List<Category>();
        private BalanceType balanceType;
        private CategoryType type = CategoryType.Normal;
        private bool readOnly;
        private bool? visible;
        private bool? isVisibleChecked;
        private bool? isVisibleCheckedModel;

        [PrimaryKey(PrimaryKeyType.HiLo)]
        [XmlElement]
        public virtual int Id
        {
            get { return id; }
            set { id = value; }
        }

        [Property]
        [XmlElement]
        public virtual string Label
        {
            get { return label; }
            set { label = value; }
        }

        [BelongsTo]
        [XmlElement]
        public virtual Category Parent
        {
            get { return parent; }
            set { parent = value; }
        }

        [HasMany(Lazy = true, Inverse = true, Cascade = ManyRelationCascadeEnum.AllDeleteOrphan, BatchSize = 10, OrderBy = "Attrib")]
        [XmlIgnore]
        public virtual IList<Bond> Bonds
        {
            get { return bonds; }
            set { bonds = value; }
        }

        [HasMany(Lazy = true, Inverse = true, Cascade = ManyRelationCascadeEnum.AllDeleteOrphan, BatchSize = 10, OrderBy = "Type")]
        [XmlIgnore]
        public virtual IList<Category> Children
        {
            get { return children; }
            set { children = value; }
        }

        [Property(Index = "IDX_BALANCE_TYPE")]
        [XmlElement]
        public virtual BalanceType BalanceType
        {
            get { return balanceType; }
            set { balanceType = value; }
        }

        [Property]
        [XmlElement]
        public virtual CategoryType Type
        {
            get { return type; }
            set { type = value; }
        }

        [Property]
        [XmlElement]
        public virtual bool ReadOnly
        {
            get { return readOnly; }
            set { readOnly = value; }
        }

        [Property]
        [XmlElement]
        public virtual bool? isVisible
        {
            get { return visible; }
            set { visible = value; }
        }

        [Property]
        [XmlElement]
        public virtual bool? IsVisibleCheckedModel
        {
            get { return isVisibleCheckedModel; }
            set { isVisibleCheckedModel = value; }
        }

        [Property]
        [XmlElement]
        public virtual bool? IsVisibleChecked
        {
            get { return isVisibleChecked; }
            set { isVisibleChecked = value; }
        }

        [XmlIgnore]
        public virtual bool IsNew
        {
            get { return id == default(int); }
        }

        public virtual string FullPath()
        {
            string path = this.Label;
            Category category = this;
            while (category.Parent != null)
            {
                category = category.Parent;
                path = category.Label + "/" + path;
            }
            return path;
        }

        public bool Equals(Category other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return id != 0 && other.id == id;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof(Category)) return false;
            return Equals((Category)obj);
        }

        public override int GetHashCode()
        {
            return id;
        }

        public static bool operator ==(Category left, Category right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Category left, Category right)
        {
            return !Equals(left, right);
        }

        public static string BalanceTypeCode(BalanceType balanceType)
        {
            switch (balanceType)
            {
                case BalanceType.None:
                    return "";
                case BalanceType.Asset:
                    return "A";
                case BalanceType.Liability:
                    return "L";
                case BalanceType.Equity:
                    return "E";
                default:
                    throw new ArgumentOutOfRangeException("balanceType");
            }
        }

        public virtual string Code
        {
            get
            {
                return (Type == CategoryType.Roll) ? "R" : BalanceTypeCode(this.BalanceType);
            }
        }

        public static Category FindRoot()
        {
            return FindOne(Expression.IsNull("Parent"));
        }

        public static Category FindNodeByName(string lbl)
        {
            return FindOne(Expression.Eq("Label", lbl));
        }

        public static Category FindNodeByID(int categoryID)
        {
            return FindOne(Expression.Eq("Id", categoryID));
        }

        public static Category[] FindByType(CategoryType cattype)
        {
            return FindAllByProperty("Type", cattype);
        }

        public static Category[] FindByBalanceType(BalanceType baltype)
        {
            return FindAllByProperty("BalanceType", baltype);
        }

        public static Category[] FindByBalanceTypeAndCatType(BalanceType baltype, CategoryType cattype)
        {
            return FindAll(Expression.Eq("BalanceType", baltype), Expression.Eq("Type", cattype));
        }

        public virtual decimal GetTotalValueForCategoryTree()
        {
            ScalarQuery<decimal> query = new ScalarQuery<decimal>(typeof(Bond),
                  @"select sum(b.BookValue)
                    from Bond b
                    left join b.Category c1
                    left join b.Category.Parent c2
                    where        
                        b.Exclude = 0 and (     
                            b.Category=:cat 
                            or c1.Parent=:cat
                            or c2.Parent=:cat
                        )");

            query.SetParameter("cat", this);
            object sumObj = query.Execute();
            decimal sum = (decimal)sumObj;

            return sum;
        }

        public static Category ParrentCategory(BalanceType baltype)
        {
            Category[] categories = FindByBalanceType(baltype);
            if (categories.Length == 0) return null;

            Category auxCat = categories[0];
            while (auxCat.parent.parent != null)
            {
                auxCat = auxCat.parent;
            }

            return auxCat;
        }

        public static Category CashAdjCategory(BalanceType baltype)
        {
            foreach (Category cat in ParrentCategory(baltype).Children)
            {
                if (cat.Type == CategoryType.CashAdj)
                    return cat;
            }
            return null;
        }

        public static BondAttrib MapCategoryTypeToBondAttrib(CategoryType type)
        {
            switch (type)
            {
                case CategoryType.Normal:
                    return BondAttrib.InBalance;
                case CategoryType.Roll:
                    return BondAttrib.Roll;
                case CategoryType.CashAdj:
                    return BondAttrib.CashAdj;
                case CategoryType.Treasury:
                    return BondAttrib.Treasury;
                case CategoryType.Impairments:
                    return BondAttrib.Impairments;
                case CategoryType.Receivable:
                    return BondAttrib.Receivable;
                case CategoryType.Reserves:
                    return BondAttrib.Reserves;
                case CategoryType.OtherComprehensiveIncome:
                    return BondAttrib.OtherComprehensiveIncome;

                default:
                    throw new ArgumentOutOfRangeException("type");
            }
        }
    }


    public enum BalanceType
    {
        None = 0,
        Asset,
        Liability,
        Equity
    }

    public enum CategoryType
    {
        Normal = 1,
        Roll = 2,
        CashAdj = 3,
        Treasury = 4,
        Impairments = 5,
        Receivable = 6,
        Reserves = 7,
        [EnumDescription("Other Comprehensive Income")]
        OtherComprehensiveIncome = 8,
    }

}