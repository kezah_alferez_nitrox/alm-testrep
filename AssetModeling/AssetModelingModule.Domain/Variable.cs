using System.Collections.Generic;
using Castle.ActiveRecord;
using NHibernate.Expression;
using System;
using System.Xml.Serialization;

namespace AssetModelingModule.Domain
{
    [ActiveRecord(Lazy = false)]
    [Serializable]
    [XmlRoot]
    public class Variable : ActiveRecordBaseEx<Variable>
    {
        private int id;
        private string name;
        private bool selected;
        private VariableType variableType;
        private IList<VariableValue> variableValues = new List<VariableValue>();

        [PrimaryKey(PrimaryKeyType.HiLo)]
        [XmlElement]
        public virtual int Id
        {
            get { return id; }
            set { id = value; }
        }


        [Property(Unique = true)]
        [XmlElement]
        public virtual string Name
        {
            get { return name; }
            set { name = value; }
        }

        [Property]
        [XmlElement]
        public virtual bool Selected
        {
            get { return selected; }
            set { selected = value; }
        }

        [Property]
        [XmlElement]
        public virtual VariableType VariableType
        {
            get { return variableType; }
            set { variableType = value; }
        }

        [XmlIgnore]
        public virtual Variable Self
        {
            get { return this; }
        }

        
        [HasMany(Lazy = true, Inverse = true, Cascade = ManyRelationCascadeEnum.AllDeleteOrphan, BatchSize = 10)]
        [XmlIgnore]
        public virtual IList<VariableValue> VariableValues
        {
            get { return variableValues; }
            set { variableValues = value; }
        }

        public static Variable[] FindAvailable()
        {
            return FindAll(Expression.Eq("Selected", false));
        }

        public static Variable[] FindSelected()
        {
            return FindAll(Expression.Eq("Selected", true));
        }

        public static Variable FindByName(string _name)
        {
            return FindFirst(Expression.Eq("Name",_name));
        }

        public static bool ExistsByName(string variableName)
        {
            return Exists("Name = ?", variableName);
        }

        public static bool ExistById(int id)
        {
            return Exists("Id = ?", id);
        }
    }

    public enum VariableType
    {
        Number,
        Vector,
        Formula,
        Array
    }
}