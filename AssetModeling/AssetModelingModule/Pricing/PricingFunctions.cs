using System;

namespace AssetModelingModule.Pricing
{
    public static class PricingFunctions
    {
        // Interpolation de taux continu sur la courbe
        public static double InterpoleTaux(DateTime maturite, DateTime[] datesObservees,
                                           double[] zc)
        {
            int n = datesObservees.Length;

            double result = Math.Log(1 + zc[0]);

            int i = 0;
            while (maturite > datesObservees[i] && (i < n - 1))
            {
                if (maturite < datesObservees[i + 1])
                {
                    result = Math.Log(1 + zc[i]) +
                             (maturite - datesObservees[i]).TotalDays /
                             (datesObservees[i + 1] - datesObservees[i]).TotalDays *
                             (Math.Log(1 + zc[i + 1]) - Math.Log(1 + zc[i]));
                }
                else
                {
                    result = Math.Log(1 + zc[i + 1]);
                }
                i++;
            }

            return result;
        }

        // calcul de discount facteur entre maturite1 et maturite2
        public static double DF(DateTime maturite1, DateTime maturite2, DateTime[] datesObservees,
                                double[] zc)
        {
            double t1 = (maturite1 - datesObservees[0]).TotalDays / 365;
            double t2 = (maturite2 - datesObservees[0]).TotalDays / 365;
            double taux1 = InterpoleTaux(maturite1, datesObservees, zc);
            double taux2 = InterpoleTaux(maturite2, datesObservees, zc);

            if ((taux1 != 0) && (taux2 != 0))
            {
                return Math.Exp(-taux2 * t2 + taux1 * t1);
            }

            return double.NaN; // probl�me de date
        }

        // calcul de discount facteur entre maturite1 et maturite2 avec un spread de cr�dit et de liquidit�
        public static double DFspread(DateTime maturite1, DateTime maturite2, double spread,
                                      DateTime[] datesObservees, double[] zc)
        {
            double t1 = (maturite1 - datesObservees[0]).TotalDays / 365;
            double t2 = (maturite2 - datesObservees[0]).TotalDays / 365;
            double taux1 = InterpoleTaux(maturite1, datesObservees, zc) + spread;
            double taux2 = InterpoleTaux(maturite2, datesObservees, zc) + spread;

            if ((taux1 != 0) && (taux2 != 0))
            {
                return Math.Exp(-taux2 * t2 + taux1 * t1);
            }

            return double.NaN;
        }


        // calcul du prix d'une obligation, frequency vaut YYYY (si annuel) ou q si trimestriel
        public static double Oblig(double coupon, string frequency, DateTime expiry, DateTime dateNextCoupon,
                                   double spread, DateTime[] datesObservees, double[] zc)
        {
            int freq = (frequency == "q") ? 4 : 1;

            DateTime today = datesObservees[0];

            double result = DFspread(today, dateNextCoupon, spread, datesObservees, zc)
                              * coupon * (dateNextCoupon - today).TotalDays / 365
                             + DFspread(today, expiry, spread, datesObservees, zc);

            DateTime dateCoupon = DateAdd(frequency, 1, dateNextCoupon);

            while ((expiry - dateCoupon).TotalDays / 365 > -1.0 / 8)
            {
                result += DFspread(today, dateCoupon, spread, datesObservees, zc)
                           * coupon / freq;
                dateCoupon = DateAdd(frequency, 1, dateCoupon);
            }

            return result;
        }

        // oblig. � taux variable quarterly
        public static double ObligTV(double nextVarCoupon, DateTime expiry, double spread,
                            double marketBasis, DateTime[] datesObservees, double[] zc)
        {
            DateTime dateJour = datesObservees[0];
            DateTime dateNextCouponTv = expiry;
            double spreadSumAct = DFspread(dateJour, dateNextCouponTv, marketBasis, datesObservees, zc);

            while (dateNextCouponTv > DateAdd("q", 1, datesObservees[0]))
            {
                dateNextCouponTv = DateAdd("q", -1, dateNextCouponTv);
                spreadSumAct = spreadSumAct + DFspread(dateJour, dateNextCouponTv, marketBasis, datesObservees, zc);
            }

            return (1 + nextVarCoupon) * DFspread(dateJour, dateNextCouponTv, marketBasis, datesObservees, zc)
                        + (spread * spreadSumAct);
        }

        private static DateTime DateAdd(string frequency, int quantity, DateTime dateTime)
        {
            if (frequency == "q")
            {
                return dateTime.AddMonths(3 * quantity);
            }

            return dateTime.AddYears(quantity); // yyyy
        }

        //  calcul d'un swap TF annuel contre TV
        public static double SwapPrice(double coupon, double nextVarCoupon, DateTime expiry,
                                       DateTime[] datesObservees, double[] zc)
        {
            DateTime dateJour = datesObservees[0];

            DateTime dateNextCouponTf = expiry;
            while ((dateNextCouponTf > DateAdd("YYYY", 1, datesObservees[0])))
            {
                dateNextCouponTf = DateAdd("YYYY", -1, dateNextCouponTf);
            }

            DateTime dateNextCouponTv = expiry;
            while ((dateNextCouponTv > DateAdd("q", 1, datesObservees[0])))
            {
                dateNextCouponTv = DateAdd("q", -1, dateNextCouponTv);
            }
            return Oblig(coupon, "YYYY", expiry, dateNextCouponTf, 0, datesObservees, zc)
                    - ObligTV(nextVarCoupon, expiry, 0, 0, datesObservees, zc);
        }
    }
}