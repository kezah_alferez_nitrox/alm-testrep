using System;
using AssetModelingModule.Domain;

namespace AssetModelingModule.Pricing
{
    public class Pricer
    {
        public static readonly Func<DateTime, DateTime>[] VectorFormulas = {
                                                                         d => d.AddDays(1),
                                                                         d => d.AddDays(7),
                                                                         d => d.AddMonths(1),
                                                                         d => d.AddMonths(2),
                                                                         d => d.AddMonths(3),
                                                                         d => d.AddMonths(6),
                                                                         d => d.AddYears(1),
                                                                         d => d.AddYears(2),
                                                                         d => d.AddYears(3),
                                                                         d => d.AddYears(4),
                                                                         d => d.AddYears(5),
                                                                         d => d.AddYears(7),
                                                                         d => d.AddYears(10),
                                                                         d => d.AddYears(20),
                                                                         d => d.AddYears(30),
                                                                     };

        public static readonly string[] VectorNames = {
                                                    "JJ",
                                                    "1W",
                                                    "E1M",
                                                    "E2M",
                                                    "E3M",
                                                    "E6M",
                                                    "E1Y",
                                                    "E2Y",
                                                    "E3Y",
                                                    "E4Y",
                                                    "E5Y",
                                                    "E7Y",
                                                    "E10Y",
                                                    "E20Y",
                                                    "E30Y",
                                                };


        private static void CreateFundamentalVectors()
        {
            FundamentalVector[] vectors = new FundamentalVector[VectorNames.Length];
            for (int i = 0; i < VectorNames.Length; i++)
            {
                vectors[i] = new FundamentalVector();
                vectors[i].Position = i;
                vectors[i].Name = VectorNames[i];
                vectors[i].Save();
            }
        }

        public static void EnsureFundamentalVectorsExist()
        {
            int length = FundamentalVector.FindAll().Length;
            if (length == 0)
            {
                CreateFundamentalVectors();
            }
        }

        public static double SwapPrice(double coupon, double nextVarCoupon, DateTime expiry)
        {
            DateTime valuationDate = Param.ValuationDate ?? DateTime.Today;
            int count = VectorFormulas.Length;
            DateTime[] datesObservees = new DateTime[count];
            for (int i = 0; i < count; i++)
            {
                datesObservees[i] = VectorFormulas[i](valuationDate);
            }

            FundamentalVector[] vectors = FundamentalVector.FindAll();
            if (vectors.Length != count) return double.NaN;

            double[] zc = ComputeFundamentalVectorValues(vectors, datesObservees);

            return PricingFunctions.SwapPrice(coupon, nextVarCoupon, expiry, datesObservees, zc);
        }

        private static double[] ComputeFundamentalVectorValues(FundamentalVector[] vectors, DateTime[] dates)
        {
            double[] result = new double[vectors.Length];

            for (int i = 0; i < vectors.Length; i++)
            {
                double value;
                string formula = vectors[i].Formula;
                bool percent = false;
                if (formula.Contains("%"))
                {
                    formula = formula.Replace("%", "");
                    percent = true;
                }
                formula = formula.Trim();
                if (double.TryParse(formula, out value))
                {
                    result[i] = percent ? value / 100 : value;
                }else
                {
                    Vector vector = Vector.FindByName(vectors[i].Formula);
                    if(vector==null) continue;

                    result[i] = (double) vector.GetValueAtDate(dates[i]);
                }
            }

            return result;
        }
    }
}