﻿using System;
using System.Threading;
using System.Windows.Forms;
using AssetModelingModule.Domain;
using AssetModelingModule.Views.BalanceSheet;
using Castle.ActiveRecord;

namespace AssetModelingModule
{
    internal static class Program
    {
        /// <summary>
        /// Point d'entrée principal de l'application.
        /// </summary>
        [STAThread]
        private static void Main(string[] args)
        {
            // Test();
#if !DEBUG
            return;
            Application.ThreadException += Application_ThreadException;
#endif

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            BalanceSheet balanceSheet = new BalanceSheet(args);
            balanceSheet.WindowState = FormWindowState.Maximized;
            Application.Run(balanceSheet);
        }

        private static void Test()
        {
            Initialiser.Initialise();

            using(new SessionScope())
            {
                var scenario = new Scenario();
                scenario.Name = "1";
                scenario.Generated = true;
                scenario.Selected = true;
                scenario.Save();

                var variable = new Variable();
                variable.Name = "1";
                variable.Selected = true;
                variable.Save();

                var variableValue = new VariableValue();
                variableValue.Value = "5";
                variableValue.Variable = variable;
                variableValue.Scenario = scenario;
                variable.VariableValues.Add(variableValue);
                scenario.VariableValues.Add(variableValue);
            }

            using (new SessionScope())
            {
                Scenario.DeleteGenerated();
            }
        }

#if !DEBUG
        private static void Application_ThreadException(object sender, ThreadExceptionEventArgs e)
        {
            Cursor.Current = Cursors.Default;
            Log.Exception("ThreadException", e.Exception);      
        }
#endif
    }
}