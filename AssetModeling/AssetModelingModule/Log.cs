using System;
using log4net;

namespace AssetModelingModule
{
    public class Log
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(Log));

        public static void Exception(string message, Exception ex)
        {
            log.Error(message, ex);
        }

        public static void Error(string message)
        {
            log.Error(message);
        }

        public static void Warn(string message)
        {
            log.Warn(message);
        }

        public static void Warn(Exception ex)
        {
            log.Warn(ex);
        }
    }
}