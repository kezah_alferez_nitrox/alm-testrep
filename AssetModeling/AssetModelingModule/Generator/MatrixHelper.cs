using System;
using System.Diagnostics;
using System.Linq;

namespace AssetModelingModule.Generator
{
    public static class MatrixHelper
    {
        public static double[,] Cholesky(double[,] m)
        {
            Debug.Assert(m != null);
            Debug.Assert(m.GetLength(0) == m.GetLength(1));

            var n = m.GetLength(0);

            var result = new double[n, n];

            for (var i = 0; i < n; i++)
            {
                for (var j = 0; j <= i; j++)
                {
                    double ss = 0;
                    for (var k = 0; k < j; k++)
                    {
                        ss = ss + result[i, k] * result[j, k];
                    }

                    if (i == j)
                    {
                        result[i, i] = Math.Sqrt(m[i, i] - ss);
                    }
                    else
                    {
                        result[i, j] = (m[i, j] - ss) / result[j, j];
                    }
                }
            }
            return result;
        }

        public static bool IsPositiveDefinite(double[,] m)
        {
            for (int i = 1; i <= m.GetLength(0); i++)
            {
                double det = Determinant(m, i);
                if (det <= 0) return false;
            }
            return true;
        }

        public static double Determinant(double[,] m)
        {
            if (m == null || m.Length == 0 || m.GetLength(0) != m.GetLength(1)) return 0;

            return Determinant(m, m.GetLength(0));
        }

        public static double Determinant(double[,] m, int size)
        {
            if (m == null || m.Length == 0 || m.GetLength(0) != m.GetLength(1)) return 0;

            double result = 0;

            if (size == 1)
            {
                result = m[0, 0];
                return result;
            }

            if (size == 2)
            {
                result = m[0, 0] * m[1, 1] - m[0, 1] * m[1, 0];
                return result;
            }

            for (int i = 0; i < size; i++)
            {
                double[,] temp = new double[size - 1, size - 1];

                for (int j = 1; j < size; j++)
                {
                    for (int k = 0; k < size; k++)
                    {

                        if (k < i)
                        {
                            temp[j - 1, k] = m[j, k];
                        }
                        else if (k > i)
                        {
                            temp[j - 1, k - 1] = m[j, k];
                        }

                    }
                }

                result += m[0, i] * Math.Pow(-1, i) * Determinant(temp);
            }

            return result;
        }

        public static double[] MatrixStdev(double[,] matrix)
        {
            double[] stdevs = new double[matrix.Length];
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                double[] v = GetRow(matrix, i);
                stdevs[i] = Statistics.StandardDeviation(v);
            }
            return stdevs;
        }


        public static double[] MatrixMeans(double[,] matrix)
        {
            double[] means = new double[matrix.Length];

            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                double[] v = GetRow(matrix, i);
                means[i] = v.Average();
            }

            return means;
        }

        public static double[,] CorrelationMatrix(double[,] matrix)
        {
            double[,] correlationMatrix = new double[matrix.GetLength(0), matrix.GetLength(0)];
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(0); j++)
                {
                    double[] v1 = GetRow(matrix, i);
                    double[] v2 = GetRow(matrix, j);
                    correlationMatrix[i, j] = Statistics.Correlation(v1, v2);
                }
            }

            return correlationMatrix;
        }

        public static double[] GetRow(double[,] m, int r)
        {
            int n = m.GetLength(1);
            double[] result = new double[n];

            for (int i = 0; i < n; i++)
            {
                result[i] = m[r, i];
            }

            return result;
        }

        public static bool ContainsNaN(double[,] m)
        {
            foreach (double d in m)
            {
                if (double.IsNaN(d))
                {
                    return true;
                }
            }
            return false;
        }

        public static void Dump(double[,] m)
        {
            Debug.Write("[");

            for (int i = 0; i < m.GetLength(0); i++)
            {
                for (int j = 0; j < m.GetLength(1); j++)
                {
                    Debug.Write(m[i, j] + " ");
                }
                Debug.Write("; ");
            }

            Debug.Write("]");
        }
    }
}