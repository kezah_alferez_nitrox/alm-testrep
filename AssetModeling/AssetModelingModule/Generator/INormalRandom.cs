namespace AssetModelingModule.Generator
{
    /// <summary>
    /// Normaly-distributed (gaussian) random number generator
    /// </summary>
    public interface INormalRandom
    {
        double Next();
    }
}