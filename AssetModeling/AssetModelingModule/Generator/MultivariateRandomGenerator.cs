namespace AssetModelingModule.Generator
{
    public class MultivariateRandomGenerator
    {
        private readonly INormalRandom random;

        public double[,] Result { get; private set; }
        public bool IsValid { get; private set; }

        public MultivariateRandomGenerator() : this(new BoxMuller(new MersenneTwister()))
        {
        }

        public MultivariateRandomGenerator(INormalRandom random)
        {
            this.random = random;
            this.IsValid = false;
        }

        // generates multivariate normals
        // n is the number of multivariate normals to generate
        // mu is an array of means
        // sig is an array of standard deviations
        // rho is a 2 dim correlation array
        public void GenerateMultivariateNormals(int size, double[][] means, double[] stdev, double[,] correlations)
        {
            var n = means.Length;
            var cholesky = MatrixHelper.Cholesky(correlations);

            // create x which will be the return values
            double[,] result = new double[n,size];

            for (var k = 0; k < size; k++)
            {
                // generate standard normals
                var z = this.GenerateStandardNormalsVector(n);

                // tranform to correlated standard normals
                for (var i = 0; i < n; i++)
                {
                    double ss = 0;
                    for (var j = 0; j < n; j++)
                    {
                        ss = ss + z[j]*cholesky[i, j];
                    }
                    result[i, k] = ss*stdev[i] + means[i][k];
                }
            }

            this.Result = result;

            this.IsValid = !MatrixHelper.ContainsNaN(result);
        }

        // generates a vector of iid standard normals
        // n is the number of normals to generate
        private double[] GenerateStandardNormalsVector(int n)
        {
            var result = new double[n];
            for (int i = 0; i < n; i++)
            {
                result[i] = this.random.Next();
            }
            return result;
        }
    }
}