using System;
using System.Diagnostics;
using System.Windows.Forms;
using AssetModelingModule.Domain;
using Castle.ActiveRecord;
using NHibernate;

namespace AssetModelingModule
{
    internal class PersistenceSession
    {
        private static SessionScope scope;

        private static ISessionScope CurrentSessionScope
        {
            get
            {
                return SessionScope.Current;
            }
        }

        internal static void Flush()
        {
            if (CurrentSessionScope != null)
            {
                try
                {
                    CurrentSessionScope.Flush();
                }
                catch (Exception ex)
                {
                    Application.UseWaitCursor = false;
                    Log.Exception("SessionScope.Flush", ex);
#if DEBUG
                    Debugger.Break();
#endif
                }
            }
        }

        internal static void StartNew()
        {
            if (CurrentSessionScope != null)
            {
                try
                {
                    Flush();
                    DisposeCurrentSessionScope();
                }
                catch (Exception ex)
                {
                    Log.Exception("PersistChanges", ex);
#if DEBUG
                    Debugger.Break();
#endif
                }
            }

            scope = new SessionScope();
        }

        private static void DisposeCurrentSessionScope()
        {
            Clear();
            CurrentSessionScope.Dispose();
        }

        internal static void Clear()
        {
            try
            {
                CurrentNHibernateSession.Clear();
            }
            catch (Exception ex)
            {
                Log.Exception("Clear", ex);
#if DEBUG
                Debugger.Break();
#endif
            }
        }

        public static ISession CurrentNHibernateSession
        {
            get
            {
                return ActiveRecordMediator
                    .GetSessionFactoryHolder()
                    .CreateSession(typeof(Category));
            }
        }

        internal static void Evict<T>(T arObject) where T : ActiveRecordBase<T>
        {
            ActiveRecordMediator
                .GetSessionFactoryHolder()
                .CreateSession(typeof(T))
                .Evict(arObject);
        }

    }
}
