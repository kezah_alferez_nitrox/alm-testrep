using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace AssetModelingModule.Controls
{
    public class AutoCompleteEditingControl : DataGridViewTextBoxEditingControl
    {
        private Form parentForm;

        private readonly Form autoCompleteForm;
        private readonly ListBox listBox;

        

        private string[] valueList;

        public AutoCompleteEditingControl()
        {
            autoCompleteForm = new Form();
            autoCompleteForm.ShowInTaskbar = false;
            autoCompleteForm.ControlBox = false;
            autoCompleteForm.TabStop = false;
            autoCompleteForm.FormBorderStyle = FormBorderStyle.FixedDialog;
            autoCompleteForm.Margin = new Padding(0);
            autoCompleteForm.Padding = new Padding(0);
            autoCompleteForm.Size = new Size(120, 95);

            listBox = new ListBox();
            listBox.TabStop = false;
            listBox.IntegralHeight = false;
            listBox.Dock = DockStyle.Fill;
            listBox.Click += listBox_Click;

            autoCompleteForm.Controls.Add(listBox);

           
        }

        private void listBox_Click(object sender, EventArgs e)
        {
            AddAutoCompleteWord();
        }

        public string[] ValueList
        {
            get { return valueList; }
            set
            {
                valueList = value;
                if (valueList!=null) Array.Sort(valueList);
            }
        }

        protected override void OnParentChanged(EventArgs e)
        {
            base.OnParentChanged(e);

            parentForm = FindForm();
        }

        private bool _aux = true;

        protected override void OnEnter(EventArgs e)
        {
            base.OnEnter(e);
            if (_aux)
                DoAutoComplete();
            _aux = true;
        }

        protected override void OnLeave(EventArgs e)
        {
            base.OnLeave(e);

            EndAutoComplete();

            _aux = false;
        }

        protected override bool ProcessCmdKey(ref Message m, Keys k)
        {
            // Intercept Enter/Up/Down Key in a TextBox
            if (m.Msg == 256 && autoCompleteForm.Visible)
            {
                //if (k == Keys.Enter)
                //{
                //    AddAutoCompleteWord();
                //    return true;
                //}
                if (k == Keys.Up)
                {
                    if (listBox.SelectedIndex > 0)
                    {
                        listBox.SelectedIndex--;
                    }
                    else if (listBox.Items.Count > 0)
                    {
                        listBox.SelectedIndex = listBox.Items.Count - 1;
                    }
                    return true;
                }
                if (k == Keys.Down)
                {
                    if (listBox.SelectedIndex < listBox.Items.Count - 1)
                    {
                        listBox.SelectedIndex++;
                    }
                    else if (listBox.Items.Count > 0)
                    {
                        listBox.SelectedIndex = 0;
                    }
                    return true;
                }
                if (k == Keys.Escape)
                {
                    EndAutoComplete();
                    return true;
                }
            }

            return base.ProcessCmdKey(ref m, k);
        }

        protected override void OnKeyUp(KeyEventArgs e)
        {
            base.OnKeyUp(e);

            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Up ||
                e.KeyCode == Keys.Down || e.KeyCode == Keys.Escape)
            {
                return;
            }

            DoAutoComplete();
        }



        protected override void OnMouseClick(MouseEventArgs e)
        {
            base.OnMouseClick(e);
            if (e.Button == MouseButtons.Left)
            {
                DoAutoComplete();
            }
        }

        private void DoAutoComplete()
        {
            if (valueList == null) return;

            string textBeforeCaret = Text.Substring(0, SelectionStart);
            string lastWord = GetLastWordInSentence(textBeforeCaret);
            string[] filteredWords = GetFilteredWords(lastWord);
            ShowWordList(filteredWords);
        }

        private void AddAutoCompleteWord()
        {
            if (!listBox.Visible || listBox.SelectedIndex < 0) return;

            string wordToAdd = (string)listBox.SelectedItem;
            string lastWord = GetLastWordInSentence(Text.Substring(0, SelectionStart));

            if (wordToAdd != lastWord)
            {
                string partToAdd = wordToAdd.Substring(lastWord.Length, wordToAdd.Length - lastWord.Length);

                SelectedText = partToAdd;
            }
            autoCompleteForm.Hide();
        }

        private void ShowWordList(string[] words)
        {
            listBox.Items.Clear();

            if (words.Length > 0)
            {
                listBox.Items.AddRange(words);

                autoCompleteForm.ClientSize = new Size(120, listBox.ItemHeight * Math.Min(words.Length, 10) + 4);

                if (!autoCompleteForm.Visible)
                {
                    autoCompleteForm.StartPosition = FormStartPosition.Manual;
                    autoCompleteForm.Location = GetListBoxLocation();
                    autoCompleteForm.Show(parentForm);
                    Focus();

                    parentForm.LocationChanged += parentForm_LocationChanged;
                }

                if (listBox.SelectedIndex < 0) listBox.SelectedIndex = 0;
            }
            else
            {
                autoCompleteForm.Hide();
            }
        }

        private void EndAutoComplete()
        {
            autoCompleteForm.Hide();
        }

        private void parentForm_LocationChanged(object sender, EventArgs e)
        {
            if (autoCompleteForm.Visible)
            {
                autoCompleteForm.Location = GetListBoxLocation();
            }
        }

        private Point GetListBoxLocation()
        {
            DataGridView grid = EditingControlDataGridView;

            Rectangle cellDisplayRectangle = grid.GetCellDisplayRectangle(grid.CurrentCellAddress.X,
                                                                          grid.CurrentCellAddress.Y, true);

            return grid.PointToScreen(new Point(cellDisplayRectangle.X,
                                                cellDisplayRectangle.Y + cellDisplayRectangle.Height));
        }

        private string[] GetFilteredWords(string filterWord)
        {
            List<string> filteredWords = new List<string>();

            foreach (string word in valueList)
            {
                if (word.StartsWith(filterWord, StringComparison.InvariantCultureIgnoreCase))
                {
                    filteredWords.Add(word);
                }
            }

            string[] array = filteredWords.ToArray();

            return array;
        }

        private string GetLastWordInSentence(string sentance)
        {
            const string regexStr = "\\b\\w*$";

            Match match = Regex.Match(sentance, regexStr);

            return match.Value;
        }
    }
}