using System;
using System.Windows.Forms;

namespace AssetModelingModule.Controls
{
    public class AutoCompleteCell : DataGridViewTextBoxCell
    {
        public string[] valueList;

        public string[] ValueList
        {
            get { return valueList; }
            set { valueList = value; }
        }

        public override void InitializeEditingControl(int rowIndex,
                                                      object initialFormattedValue,
                                                      DataGridViewCellStyle dataGridViewCellStyle)
        {
            base.InitializeEditingControl(rowIndex, initialFormattedValue,
                                          dataGridViewCellStyle);

            AutoCompleteEditingControl editingControl = (AutoCompleteEditingControl) DataGridView.EditingControl;

            DataGridViewColumn dgvc = OwningColumn;
            if (dgvc is AutoCompleteColumn)
            {
                AutoCompleteColumn autoCompleteColumn = (AutoCompleteColumn) dgvc;

                if (valueList == null)
                {
                    editingControl.ValueList = autoCompleteColumn.ValueList;
                }
                else
                {
                    editingControl.ValueList = ValueList;
                }
            }
        }

        public override Type EditType
        {
            get { return typeof (AutoCompleteEditingControl); }
        }

      
    }
}