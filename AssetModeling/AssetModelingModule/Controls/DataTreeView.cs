using System.Windows.Forms;
using AssetModelingModule.Domain;

namespace AssetModelingModule.Controls
{
    public class DataTreeView : TreeView
    {
        public TreeNode _foundnode;

        public void LoadNodes()
        {
            Nodes.Clear();

            Category root = Category.FindRoot();
            if (root == null) return;

            TreeNode node = new TreeNode(root.Label);
            node.Tag = root;
            AddChildNodes(node, root);
            Nodes.Add(node);

            node.Expand();
        }


        public void UpdateVisibles(TreeNode RootNode)
        {
            //foreach (Category cat in Category.FindAll())
            //{
            //    if (cat.isVisible == true && cat.Parent != null)
            //    {
            //        GoThrough(RootNode, cat.Id);
            //    }
            //}
            foreach (Category cat in Category.FindAll())
            {
                this._foundnode = null;
                _searchNode(RootNode, cat.Id);
                if (_foundnode != null && cat.Label != "Balance Sheet")
                    if (_foundnode.Parent.IsExpanded)
                    {
                        cat.isVisible = true;
                    }
                    else
                    {
                        cat.isVisible = false;
                    }
            }
        }

        public void LoadVisibleNodes()
        {
            Category root = Category.FindRoot();
            Nodes.Clear();

            TreeNode RootNode = new TreeNode(root.Label);
            RootNode.Tag = root;
            AddChildNodes(RootNode, root);
            Nodes.Add(RootNode);
            RootNode.Expand();

            TreeNode node1 = RootNode;
            

            foreach (Category cat in Category.FindAll())
            {
                if (cat.isVisible == true && cat.Parent != null)
                {
                    GoThrough(node1, cat.Id);
                }
            }
        }
        

        private void GoThrough(TreeNode node, int id)
        {
            foreach (TreeNode _node in node.Nodes)
            {
                if ((_node.Tag as Category).Id == id)
                    _node.Parent.Expand() ;
                this.GoThrough(_node, id);
            }

        }


        private void _searchNode(TreeNode node, int id)
        {
            if ((node.Tag as Category).Id == id)
            {
                _foundnode = node;
                return;
            }
            else
            {
                if (node.FirstNode != null)
                    _searchNode(node.FirstNode, id);
                if (node.NextNode != null)
                    _searchNode(node.NextNode, id);

                return;
            }
        }

        private void AddChildNodes(TreeNode parentNode, Category parent)
        {
            foreach (Category category in parent.Children)
            {
                TreeNode childNode = new TreeNode(category.Label);

                childNode.Tag = category;
                parentNode.Nodes.Add(childNode);
                AddChildNodes(childNode, category);
            }
        }

        public void RenameNode()
        {
            if (null != SelectedNode)
            {
                LabelEdit = true;
                SelectedNode.BeginEdit();
                Views.BalanceSheet.BalanceSheet.modifiedValues = true;
            }
        }

        public void AddNode()
        {
            if (null == SelectedNode) return;

            Category parent = (Category)SelectedNode.Tag;

            Category category = new Category();
            category.Parent = parent;
            category.IsVisibleChecked = true;
            category.isVisible = true;

            TreeNode childNode = new TreeNode(string.Empty);
            childNode.Tag = category;
            SelectedNode.Nodes.Add(childNode);
            SelectedNode = childNode;
            LabelEdit = true;
            childNode.BeginEdit();
            Views.BalanceSheet.BalanceSheet.modifiedValues = true;
        }

        public void DeleteNode()
        {
            if (null == SelectedNode) return;
            if (SelectedNode.Nodes.Count > 0) return;

            Category category = (Category)SelectedNode.Tag;

            category.Parent.Children.Remove(category);
            category.Delete();

            Nodes.Remove(SelectedNode);
            Views.BalanceSheet.BalanceSheet.modifiedValues = true;
        }

        protected override void OnAfterLabelEdit(NodeLabelEditEventArgs e)
        {
            if (!string.IsNullOrEmpty(e.Label))
            {
                Category category = (Category)e.Node.Tag;
                category.Label = e.Label;

                if (category.IsNew)
                {
                    category.BalanceType = category.Parent.BalanceType;
                    category.Type = category.Parent.Type;
                    category.Create();
                    category.Parent.Children.Add(category);
                }else
                {
                    category.Update();
                }

                e.Node.Text = e.Label;
                e.Node.EndEdit(false);

                SelectedNode = e.Node;
                OnAfterSelect(new TreeViewEventArgs(e.Node));
            }
            else
            {
                e.CancelEdit = true;
                Category category = (Category)e.Node.Tag;

                if (category.IsNew)
                {
                    TreeNode parent = e.Node.Parent;
                    e.Node.EndEdit(true);
                    SelectedNode = parent;
                    parent.Nodes.Remove(e.Node);
                }
            }
            LabelEdit = false;
            base.OnAfterLabelEdit(e);
        }

        protected override void OnMouseClick(MouseEventArgs e)
        {
            base.OnMouseClick(e);

            if (e.Button != MouseButtons.Right) return;

            TreeNode node = GetNodeAt(e.Location);
            if (null != node)
            {
                SelectedNode = node;
            }
        }
    }
}