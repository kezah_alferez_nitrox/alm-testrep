using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;

namespace AssetModelingModule.Controls
{
    public class DataGridViewEx : DataGridView
    {
        private const float IMAGE_TRANSPARENCY = 0.20f;
        public DataGridViewEx()
        {
            // this.ResizeRedraw = true;
            this.DoubleBuffered = true;

            this.FontChanged += DataGridViewEx_FontChanged;
        }

        void DataGridViewEx_FontChanged(object sender, System.EventArgs e)
        {
            DataGridView grid = (DataGridView)sender;
            Font boldFont = new Font(grid.Font, FontStyle.Bold);
            foreach (DataGridViewColumn column in grid.Columns)
            {
                column.HeaderCell.Style.Font = boldFont;
            }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);

            PaintBackgroundImage(e.Graphics, e.ClipRectangle);
        }

        protected void PaintBackgroundImage(Graphics graphics, Rectangle clipBounds)
        {
            if (BackgroundImage == null) return;

            Rectangle gridBounds = new Rectangle(0, 0, Width, Height);
            gridBounds = AjustBoundsByHeaders(gridBounds);

            Rectangle destBounds = gridBounds;

            Rectangle srcBounds = new Rectangle();
            srcBounds.Size = BackgroundImage.Size;

            destBounds.X += (gridBounds.Width - srcBounds.Width) / 2 -  HorizontalScrollingOffset;
            destBounds.Y += (gridBounds.Height - srcBounds.Height) / 2 - VerticalScrollingOffset;

            if (destBounds.X - gridBounds.X < 0)
            {
                if (destBounds.X - gridBounds.X + srcBounds.Height < 0) return;

                srcBounds.X = -destBounds.X + gridBounds.X;
                destBounds.X = gridBounds.X;
                srcBounds.Width -= srcBounds.X;
            }

            if (destBounds.Y - gridBounds.Y < 0)
            {
                if (destBounds.Y - gridBounds.Y + srcBounds.Height < 0) return;

                srcBounds.Y = -destBounds.Y + gridBounds.Y;
                destBounds.Y = gridBounds.Y;
                srcBounds.Height -= srcBounds.Y;
            }

            destBounds.Size = srcBounds.Size;

            float[][] ptsArray = { new float[] { 1, 0, 0, 0, 0 }, new float[] { 0, 1, 0, 0, 0 }, new float[] { 0, 0, 1, 0, 0 }, new float[] { 0, 0, 0, IMAGE_TRANSPARENCY, 0 }, new float[] { 0, 0, 0, 0, 1 } };
            ColorMatrix clrMatrix = new ColorMatrix(ptsArray);
            ImageAttributes imgAttributes = new ImageAttributes();
            imgAttributes.SetColorMatrix(clrMatrix, ColorMatrixFlag.Default, ColorAdjustType.Bitmap);

            graphics.DrawImage(BackgroundImage, destBounds, srcBounds.X, srcBounds.Y, srcBounds.Width, srcBounds.Height, GraphicsUnit.Pixel, imgAttributes);
        }

        private Rectangle AjustBoundsByHeaders(Rectangle rect)
        {
            if (ColumnHeadersVisible)
            {
                rect.Y += ColumnHeadersHeight;
                rect.Height -= ColumnHeadersHeight;
            }
            if (RowHeadersVisible)
            {
                rect.X += RowHeadersWidth;
                rect.Width -= RowHeadersWidth;
            }

            return rect;
        }
    }
}