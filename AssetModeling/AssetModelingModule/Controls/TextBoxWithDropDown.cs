﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Eu.AlterSystems.ASNetLib.Core;

namespace AssetModelingModule.Controls
{
    public partial class TextBoxWithDropDown : UserControl
    {
        private int? _value;

        public TextBoxWithDropDown()
        {
            InitializeComponent();
            
            comboBox1.DataSource = EnumHelper.ToList(typeof(TenorPeriod));
            comboBox1.DisplayMember = "Value";
            comboBox1.ValueMember = "Key";
            comboBox1.SelectedIndex = 0;
        }

        public int? Value
        {
            get{ return _value;}
            set
            {
                _value = value;
                if (comboBox1.SelectedIndex == 1 && value % (int)TenorPeriod.Years != 0)
                {
                    comboBox1.SelectedIndex = 0;
                    this.regexTextBox1.Text = value.ToString();
                }
                else
                {
                    this.regexTextBox1.Text = (value / (int)comboBox1.SelectedValue).ToString();
                }
            }
        }

        private void regexTextBox1_TextChanged(object sender, EventArgs e)
        {
            CalculateValue();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            CalculateValue();
        }
        private void CalculateValue()
        {
            int val = 0;
            if (int.TryParse(this.regexTextBox1.Text, out val))
            {
                _value = val * (int)comboBox1.SelectedValue;
            }
        }

        public enum TenorPeriod
        {
            [EnumDescription("Months")]
            Months = 1,
            [EnumDescription("Years")]
            Years = 12
        }
    }
}
