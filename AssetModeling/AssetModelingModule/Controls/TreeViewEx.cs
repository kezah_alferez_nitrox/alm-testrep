using System;
using System.Windows.Forms;

namespace AssetModelingModule.Controls
{
    public class TreeViewEx : TreeView
    {
        const int WM_LBUTTONDBLCLK = 0x0203;
        protected override void WndProc(ref Message m)
        {
            if (m.Msg == WM_LBUTTONDBLCLK)
            {
                this.OnDoubleClick(new EventArgs()); // disable expand/colapse on double click
                return;
            }
            base.WndProc(ref m);
        }
    }

}