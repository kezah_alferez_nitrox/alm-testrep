﻿namespace AssetModelingModule.Controls
{
    partial class TextBoxWithDropDown
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.regexTextBox1 = new AssetModelingModule.Controls.RegexTextBox();
            this.SuspendLayout();
            // 
            // comboBox1
            // 
            this.comboBox1.Dock = System.Windows.Forms.DockStyle.Right;
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(35, 0);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(75, 21);
            this.comboBox1.TabIndex = 1;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // regexTextBox1
            // 
            this.regexTextBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(182)))), ((int)(((byte)(64)))));
            this.regexTextBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.regexTextBox1.Location = new System.Drawing.Point(0, 0);
            this.regexTextBox1.Name = "regexTextBox1";
            this.regexTextBox1.Regex = "^\\d*$";
            this.regexTextBox1.Size = new System.Drawing.Size(30, 20);
            this.regexTextBox1.TabIndex = 0;
            this.regexTextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.regexTextBox1.TextChanged += new System.EventHandler(this.regexTextBox1_TextChanged);
            // 
            // TextBoxWithDropDown
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.regexTextBox1);
            this.Name = "TextBoxWithDropDown";
            this.Size = new System.Drawing.Size(110, 21);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private RegexTextBox regexTextBox1;
        private System.Windows.Forms.ComboBox comboBox1;
    }
}
