﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using AssetModelingModule.Domain;
using Eu.AlterSystems.ASNetLib.Core;

namespace AssetModelingModule.Controls
{
    public class EnumComboBoxController
    {
        public EnumComboBoxController(ComboBox comboBox, Type type)
        {
            comboBox.DisplayMember = "Value";
            comboBox.ValueMember = "Key";
            comboBox.DataSource = EnumHelper.ToList(type);
        }
    }
}
