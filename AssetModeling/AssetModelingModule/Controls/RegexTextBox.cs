﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace AssetModelingModule.Controls
{
    public class RegexTextBox : TextBox
    {
        private string previousText;

        public RegexTextBox()
        {
            this.Regex = @"^[-]?\d*(\.\d*)?$";
            this.TextAlign = HorizontalAlignment.Right;
            this.PreviewKeyDown += new PreviewKeyDownEventHandler(OnPreviewKeyDown);
            this.TextChanged += new EventHandler(OnTextChanged);
        }
        
        /// <summary>
        /// Regex ^[-]?\d+$ Numeric
        /// Regex ^[-]?\d+(\.\d+)?$ Decimal
        /// Regex ^[-]?\d{0,2}(\.\d{0,5})?$ Decimal(2,5)
        /// </summary>
        [Browsable(true)]
        [Description(@"Ex Regex: Numeric - ^[-]?\d+$; Decimal - ^[-]?\d+(\.\d+)?$; Decimal(2,5) - ^[-]?\d{0,2}(\.\d{0,5})?$ Decimal(2,5)")]
        public string Regex { get; set; }

        public void ValidateText()
        {
            if (!string.IsNullOrEmpty(this.Text) && !string.IsNullOrEmpty(Regex) && !new Regex(Regex).IsMatch(this.Text))
            {
                this.Text = string.Empty;
            }
        }
        

        void OnPreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            previousText = this.Text;
        }

        void OnTextChanged(object sender, EventArgs e)
        {
            int startSelection = this.SelectionStart;

            if (!string.IsNullOrEmpty(this.Text) && !string.IsNullOrEmpty(Regex) && !new Regex(Regex).IsMatch(this.Text))
            {
                this.Text = previousText;
                if (startSelection > 0)
                    this.SelectionStart = startSelection - 1;
            }
        }
    }
}
