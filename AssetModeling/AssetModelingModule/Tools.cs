using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Reflection;
using System.Windows.Forms;

namespace AssetModelingModule
{
    internal class Tools
    {
        private static readonly char[] operators = new char[] { ' ', '\t', '+', '-', '/', '*', '(', ')', '%', ',' };

        public static string[] GetOperandsFromExpression(string expression)
        {
            if (expression == null) return new string[0];

            expression = PreProcessExpression(expression);

            string[] operandsAndFunctions = expression.Split(operators, StringSplitOptions.RemoveEmptyEntries);
            List<string> operands = new List<string>();
            foreach (string operandOrFunction in operandsAndFunctions)
            {
                if (!IsFunctionName(operandOrFunction))
                {
                    operands.Add(operandOrFunction);
                }
            }
            return operands.ToArray();
        }

        public static bool InDesignMode
        {
            get
            {
                return LicenseManager.UsageMode == LicenseUsageMode.Designtime;
            }
        }

        public static object CallPrivateInstanceMethod<T>(T target, string methodName, params object[] args)
        {
            Type type = typeof(T);

            MethodInfo methodInfo = type.GetMethod(methodName, BindingFlags.Instance | BindingFlags.NonPublic);
            if (methodInfo == null) return null;

            return methodInfo.Invoke(target, args);
        }

        private static readonly CultureInfo[] accepedCultures = new CultureInfo[]
                                                                    {
                                                                        new CultureInfo("fr-FR"),
                                                                        new CultureInfo("en-US"), 
                                                                        
                                                                    };
        public static decimal? ClipboardValueToDecimal(string str)
        {
            foreach (CultureInfo accepedCulture in accepedCultures)
            {
                decimal decimalValue;
                if (Decimal.TryParse(str,
                                     NumberStyles.AllowDecimalPoint | NumberStyles.AllowLeadingSign |
                                     NumberStyles.AllowThousands, accepedCulture, out decimalValue))
                {
                    return decimalValue;
                }
            }

            return null;
        }

        public static string EscapeIdentifier(string name)
        {
            return name.Replace(' ', '_'); // todo: replace +,- etc
        }

        public static int GetMonthNumber(DateTime date)
        {
            return date.Year * 12 + date.Month;
        }

        public static string PreProcessExpression(string expression)
        {
            if (String.IsNullOrEmpty(expression)) return expression;

            expression = expression.Replace("%", "*0.01");

            foreach (string functionName in Constants.SupportedFunctions)
            {
                expression = expression.Replace(functionName.ToUpperInvariant(), functionName);
                expression = expression.Replace(functionName.ToLowerInvariant(), functionName);
            }

            return expression;
        }

        public static bool IsFunctionName(string text)
        {
            foreach (string functionName in Constants.SupportedFunctions)
            {
                if (text == functionName || text == functionName.ToUpperInvariant() || text == functionName.ToLowerInvariant())
                {
                    return true;
                }
            }

            return false;
        }

        public static bool IsDecimalString(string value)
        {
            decimal tempDecimal;
            return Decimal.TryParse(value, out tempDecimal);
        }

        public static Form GetMainForm()
        {
            return Application.OpenForms.Count == 0 ? null : Application.OpenForms[0];
        }

        public static DialogResult ShowDialog(Form form)
        {
            Form mainForm = null; // GetMainForm();

            if (mainForm == null)
            {
                return form.ShowDialog();
            }
            else
            {
                return form.ShowDialog(mainForm);
            }
        }

        public static DialogResult ShowDialogWithPrivateSession(Form form)
        {
            DialogResult result;

            PersistenceSession.Flush();
            result = ShowDialog(form);
            PersistenceSession.Flush();

            return result;
        }
    }

    public class EventArgs<T> : EventArgs
    {
        private readonly T _value;

        public EventArgs(T value)
        {
            _value = value;
        }

        public T Value
        {
            get { return _value; }
        }
    }
}