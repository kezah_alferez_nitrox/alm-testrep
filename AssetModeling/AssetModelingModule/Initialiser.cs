using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections;
using System.Data.SqlServerCe;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Threading;
using AssetModelingModule.BusinessLogic;
using AssetModelingModule.Domain;
using AssetModelingModule.Pricing;
using AssetModelingModule.Properties;
using Castle.ActiveRecord;
using Castle.ActiveRecord.Framework.Config;
using log4net.Config;
using NHibernate.Expression;

namespace AssetModelingModule
{
    public class Initialiser
    {
        private static bool isInitialised;

        public static void Initialise()
        {
            if (isInitialised) return;
            isInitialised = true;

#if !DEBUG
            DeleteDataBaseFileIfOlderThanThisAssembly();
#endif

            XmlConfigurator.Configure();

            Thread.CurrentThread.CurrentCulture = new CultureInfo(Constants.CULTURE_NAME);

            ConfigureActiveRecord();

            PersistenceSession.StartNew();

            BalanceBL.EnsureSpecialCategoriesAndBondsExist();

            Pricer.EnsureFundamentalVectorsExist();

            PersistenceSession.Flush();
        }

        private static void DeleteDataBaseFileIfOlderThanThisAssembly()
        {
            string dataBaseFileName = GetDataBaseFilePath();
            DateTime dataBaseTime = new FileInfo(dataBaseFileName).LastWriteTime;
            string assemblyLocation = typeof(Initialiser).Assembly.Location;

            DateTime assemblyTime = new FileInfo(assemblyLocation).LastWriteTime;
            if (dataBaseTime < assemblyTime)
            {
                DeleteDataBaseFile();
            }
        }

        public static string GetDataBaseFilePath()
        {
            string path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),
                           Settings.Default.DatabaseFile);
            return path;
        }

        private static string GetConnectionString()
        {
            return string.Format(@"Data Source={0}", GetDataBaseFilePath());
        }

        private static void ConfigureActiveRecord()
        {
            Hashtable properties = new Hashtable();

            properties.Add("hibernate.connection.driver_class", "NHibernate.Driver.SqlServerCeDriver");
            properties.Add("hibernate.dialect", "NHibernate.Dialect.MsSqlCeDialect");
            properties.Add("hibernate.connection.provider", "NHibernate.Connection.DriverConnectionProvider");
            properties.Add("hibernate.connection.connection_string",
                           GetConnectionString());
            // properties.Add("hibernate.cache.use_second_level_cache", "false");
#if DEBUG
            //properties.Add("hibernate.show_sql", "true");
#endif
            InPlaceConfigurationSource config = new InPlaceConfigurationSource();

            config.Add(typeof(ActiveRecordBase), properties);

            Assembly domain = Assembly.Load("AssetModelingModule.Domain");
            ActiveRecordStarter.ResetInitializationFlag();
            ActiveRecordStarter.Initialize(domain, config);
            EnsureDataBaseIsUpToDateOrDeleteIt(domain);

            EnsureDataBaseExists();
        }

        public static void DeleteDataBaseFile()
        {
            string dataBaseFileName = GetDataBaseFilePath();
            if (File.Exists(dataBaseFileName))
            {
                FileInfo info = new FileInfo(dataBaseFileName);
                File.Delete(dataBaseFileName);
            }
        }

        public static void EnsureDataBaseExists()
        {
            string dataBaseFileName = GetDataBaseFilePath();

            if (!File.Exists(dataBaseFileName))
            {
                using (SqlCeEngine engine = new SqlCeEngine(GetConnectionString()))
                {
                    engine.CreateDatabase();
                }

                ActiveRecordStarter.CreateSchema();

                ExecuteInitSQLStatements();
            }
        }

        
        private static void EnsureDataBaseIsUpToDateOrDeleteIt(Assembly domain)
        {
            string dataBaseFileName = GetDataBaseFilePath();
            if (!File.Exists(dataBaseFileName)) return;

            Type[] types = domain.GetTypes().Where(t => t.GetCustomAttributes(true).
                Any(a => a.GetType() == typeof(ActiveRecordAttribute))).ToArray();

            try
            {
                using (new SessionScope())
                {
                    foreach (Type type in types)
                    {
                        object findFirst = ActiveRecordMediator.FindFirst(type, Expression.Sql("1=1"));
                    }
                }
            }
            catch (Exception ex)
            {

                Log.Error(ex.ToString());
                DeleteDataBaseFile();
            }
        }

        private static void ExecuteInitSQLStatements()
        {
            ExecuteSQLStatements(Properties.Resources.InitBaseScript);
        }

        public static void ExecuteSQLStatements(string statements)
        {
            string[] sqls = statements.Split(new char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);

            using (SqlCeConnection connection = new SqlCeConnection(GetConnectionString()))
            {
                connection.Open();
                foreach (string sql in sqls)
                {
                    using (SqlCeCommand command = new SqlCeCommand(sql, connection))
                    {
                        command.ExecuteNonQuery();
                    }
                }

                connection.Close();
            }
        }
    }
}