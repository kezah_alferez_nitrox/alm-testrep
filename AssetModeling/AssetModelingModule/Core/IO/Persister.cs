using System;
using System.Collections.Generic;
using System.Diagnostics;
using AssetModelingModule.Domain;
using System.ComponentModel;

namespace AssetModelingModule.Core.IO
{
    public class Persister
    {
        public void PersistCurrencies(Currency[] currencies, IDictionary<int, Currency> currencyMap)
        {
            PersistCurrencies(currencies, currencyMap, null);
        }

        public void PersistCurrencies(Currency[] currencies, IDictionary<int, Currency> currencyMap, BackgroundWorker backGroundWorker)
        {
            currencyMap.Clear();
            decimal aux = 10;
            if (currencies.Length != 0)
                aux = 10 / currencies.Length;
            decimal totalProcent = 25;
            foreach (Currency currency in currencies)
            {
                Currency existentCurrency = Currency.FindFirstBySymbol(currency.Symbol);
                if (existentCurrency == null)
                {
                    existentCurrency = currency.ClonePropertiesOnly();
                    existentCurrency.Create();
                }
                currencyMap.Add(currency.Id, existentCurrency);
                if (backGroundWorker != null)
                    backGroundWorker.ReportProgress(Convert.ToInt32(totalProcent));
                totalProcent = totalProcent + aux;
            }
        }

        public void PersistCategoriesAndBonds(Category[] categories, Bond[] bonds, IDictionary<int, Currency> currencyMap)
        {
            PersistCategoriesAndBonds(categories, bonds, currencyMap, null);
        }

        public void PersistCategoriesAndBonds(Category[] categories, Bond[] bonds, IDictionary<int, Currency> currencyMap, BackgroundWorker backgroundWorker)
        {
            Category root = null;
            decimal aux = 15;
            if (categories.Length + bonds.Length != 0)
                aux = 15 / (categories.Length + bonds.Length);
            decimal totalProcent = 35;
            foreach (Category cat in categories)
            {
                if (cat.Parent == null)
                {
                    root = cat;
                    if (backgroundWorker != null)
                        backgroundWorker.ReportProgress(Convert.ToInt32(totalProcent));
                    totalProcent = totalProcent + aux;
                    break;
                }
            }
            if (root == null)
            {
                Log.Error("BalanceXmlSerializer.Load: no root category");
                return;
            }

            IDictionary<int, Category> categoryMap = new Dictionary<int, Category>();

            Category savedRoot = ReBuildCategoryTree(root, categories, categoryMap);

            foreach (Bond bond in bonds)
            {
                Bond clone = bond.ClonePropertiesOnly();
                clone.Currency = bond.Currency == null ? null : currencyMap[bond.Currency.Id];
                clone.Category = categoryMap[bond.Category.Id];
                clone.Category.Bonds.Add(clone);
                if (backgroundWorker != null)
                    backgroundWorker.ReportProgress(Convert.ToInt32(totalProcent));
                totalProcent = totalProcent + aux;
            }

            savedRoot.CreateAndFlush();
        }

        public Category ReBuildCategoryTree(Category rootCategory, Category[] allCategories, IDictionary<int, Category> categoryMap)
        {
            int oldId = rootCategory.Id;
            Category rootClonedCategory = rootCategory.ClonePropertiesOnly();
            categoryMap.Add(oldId, rootClonedCategory);

            foreach (Category child in allCategories)
            {
                if (child.Parent == null || child.Parent.Id != oldId) continue;

                Category savedChildCategory = ReBuildCategoryTree(child, allCategories, categoryMap);
                savedChildCategory.Parent = rootClonedCategory;
                rootClonedCategory.Children.Add(savedChildCategory);
            }

            return rootClonedCategory;
        }

        public void PersistFxRates(FXRate[] rates, IDictionary<int, Currency> currencyMap)
        {
            foreach (FXRate rate in rates)
            {
                FXRate newRate = new FXRate();
                newRate.Value = rate.Value;
                newRate.From = rate.From == null ? null : currencyMap[rate.From.Id];
                newRate.To = rate.From == null ? null : currencyMap[rate.To.Id];
                newRate.Create();
            }
        }

        public void PersistValuationDate(DateTime? date)
        {
            if (date != null) Param.ValuationDate = date;
        }

        public void PersistValuationCurrency(Currency currency, IDictionary<int, Currency> currencyMap)
        {
            if (currency != null) Param.ValuationCurrency = currencyMap[currency.Id];
        }

        public void PersistVariables(Scenario[] scenarios, Variable[] variables, VariableValue[] variableValues)
        {
            PersistVariables(scenarios, variables, variableValues, null);
        }

        public void PersistVariables(Scenario[] scenarios, Variable[] variables, VariableValue[] variableValues, BackgroundWorker backgroundWorker)
        {
            decimal auxprocess = 20;
            if (scenarios.Length + variables.Length + variableValues.Length != 0)
                auxprocess = 20 / (scenarios.Length + variables.Length + variableValues.Length);
            int totalprocess = 50;
            IDictionary<int, Scenario> scenarioMap = new Dictionary<int, Scenario>();
            IDictionary<int, Variable> variableMap = new Dictionary<int, Variable>();
            foreach (Scenario scenario in scenarios)
            {
                Scenario savedScenario = scenario.ClonePropertiesOnly();
                savedScenario.Create();
                scenarioMap.Add(scenario.Id, savedScenario);
                if (backgroundWorker != null)
                    backgroundWorker.ReportProgress(totalprocess);
                totalprocess = totalprocess + Convert.ToInt32(auxprocess);
            }
            foreach (Variable variable in variables)
            {
                Variable savedVariable = variable.ClonePropertiesOnly();
                savedVariable.Create();
                variableMap.Add(variable.Id, savedVariable);
                if (backgroundWorker != null)
                    backgroundWorker.ReportProgress(totalprocess);
                totalprocess = totalprocess + Convert.ToInt32(auxprocess);
            }

            foreach (VariableValue variableValue in variableValues)
            {
                VariableValue savedVariableValue = variableValue.ClonePropertiesOnly();
                savedVariableValue.Scenario = scenarioMap[variableValue.Scenario.Id];
                savedVariableValue.Variable = variableMap[variableValue.Variable.Id];
                savedVariableValue.Create();

                savedVariableValue.Scenario.VariableValues.Add(savedVariableValue);
                savedVariableValue.Variable.VariableValues.Add(savedVariableValue);
                if (backgroundWorker != null)
                    backgroundWorker.ReportProgress(totalprocess);
                totalprocess = totalprocess + Convert.ToInt32(auxprocess);
            }
        }

        public void PersistVectors(Vector[] vectors, VectorPoint[] points)
        {
            PersistVectors(vectors, points, null);
        }

        public void PersistVectors(Vector[] vectors, VectorPoint[] points, BackgroundWorker backgroundWorker)
        {
            //decimal totalprocess = 70;
            //decimal auxprocess = 20;
            //if (points.Length + vectors.Length != 0)
            //        auxprocess = 20 / (points.Length + vectors.Length); 

            Console.WriteLine("Saving vectors & points: {0} vectors, {1} points", vectors.Length, points.Length);
            Stopwatch stopwatch = Stopwatch.StartNew();

            PersistenceSession.Flush();

            IDictionary<int, Vector> vectorMap = new Dictionary<int, Vector>();
            foreach (Vector vector in vectors)
            {
                int oldId = vector.Id;
                Vector savedVector = vector.ClonePropertiesOnly();
                vectorMap.Add(oldId, savedVector);
                //if (backgroundWorker != null)
                //    backgroundWorker.ReportProgress(Convert.ToInt32(totalprocess));
                //totalprocess = totalprocess + auxprocess;
            }

            Console.WriteLine("Finished saving vectors: {0}ms", stopwatch.ElapsedMilliseconds);
            stopwatch.Reset(); stopwatch.Start();

            foreach (VectorPoint vectorPoint in points)
            {
                Vector vector = vectorMap[vectorPoint.Vector.Id];
                VectorPoint savedVectorPoint = vectorPoint.ClonePropertiesOnly();
                savedVectorPoint.Vector = vector;
                vector.VectorPoints.Add(savedVectorPoint);
                //if (backgroundWorker != null)
                //    backgroundWorker.ReportProgress(Convert.ToInt32(totalprocess));
                //totalprocess = totalprocess + auxprocess;
            }

            Console.WriteLine("Finished saving points: {0}ms", stopwatch.ElapsedMilliseconds);
            stopwatch.Reset(); stopwatch.Start();

            foreach (Vector value in vectorMap.Values)
            {
                value.Create();
            }

            Console.WriteLine("Finished creating vectors: {0}ms", stopwatch.ElapsedMilliseconds);
            stopwatch.Reset(); stopwatch.Start();

            PersistenceSession.Flush();

            Console.WriteLine("Finished flushing: {0}ms", stopwatch.ElapsedMilliseconds);
        }

        public void PersistDividends(Dividend[] dividends)
        {
            foreach (Dividend dividend in dividends)
            {
                Dividend newDividend = new Dividend();
                newDividend.Amount = dividend.Amount;
                newDividend.Minimum = dividend.Minimum;
                newDividend.Name = dividend.Name;
                newDividend.Status = dividend.Status;
                newDividend.Create();
            }
        }

        public void PersistOtherItemsAssum(OtherItemAssum[] otherItemAssms)
        {
            foreach (OtherItemAssum otherItemAssm in otherItemAssms)
            {
                OtherItemAssum newOtherItemAssus = new OtherItemAssum();
                newOtherItemAssus.Descrip = otherItemAssm.Descrip;
                newOtherItemAssus.Status = otherItemAssm.Status;
                newOtherItemAssus.Vect = otherItemAssm.Vect;
                newOtherItemAssus.Create();
            }
        }

        public void DeleteScenarioData()
        {
            //VariableValue
            //ActiveRecordBase<Variable>.DeleteAll();
            //ActiveRecordBase<Scenario>.DeleteAll();
            //VectorValue
            //ActiveRecordBase<Vector>.DeleteAll();
            //ActiveRecordBase<FXRate>.DeleteAll();
            //ActiveRecordBase<Dividend>.DeleteAll();
            //ActiveRecordBase<OtherItemAssum>.DeleteAll();

            string sql = @"
                DELETE FROM VariableValue;
                DELETE FROM Variable;
                DELETE FROM Scenario;
                DELETE FROM VectorPoint;
                DELETE FROM Vector;
                DELETE FROM FXRate;
                DELETE FROM Dividend;
                DELETE FROM OtherItemAssum;";

            Initialiser.ExecuteSQLStatements(sql);
        }

        public void DeleteBalanceData()
        {
            Category root = Category.FindRoot();
            if (root != null) root.Delete();

            //            string sql = @"
            //                DELETE FROM Bond;
            //                DELETE FROM Category;";

            //            Initialiser.ExecuteSQLStatements(sql);
        }

        public void PersistParameters(Param[] parameters, IDictionary<int, Currency> currencyMap)
        {
            Param.DeleteAll();
            foreach (Param param in parameters)
            {
                if (param.Id == "Valuation Currency")
                {
                    param.Val = currencyMap[int.Parse(param.Val)].Id.ToString();
                }
            }
            foreach (Param param in parameters)
            {
                Param copy = param.Copy();
                copy.Create();
            }
        }
    }
}