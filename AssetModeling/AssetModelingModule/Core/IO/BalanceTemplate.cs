using System.Xml.Serialization;
using AssetModelingModule.Domain;

namespace AssetModelingModule.Core.IO
{
    [XmlRoot("SaveOpenFile")]
    public class BalanceTemplate
    {
        public bool isLoad = false;

        private Category[] categories;
        private Bond[] bonds;
        private Currency[] currencies;

        [XmlElement]
        public Category[] Categories
        {
            get
            {
                if (isLoad)
                    return categories;
                else
                    return Category.FindAll();
            }

            set { categories = value; }

        }

        [XmlElement]
        public Bond[] Bonds
        {
            get
            {
                if (isLoad)
                    return bonds;
                else
                    return Bond.FindAll();
            }
            set { bonds = value; }
        }

        [XmlElement]
        public Currency[] Currencies
        {
            get
            {
                if (isLoad)
                    return currencies;
                else
                    return Currency.FindAll();
            }
            set { currencies = value; }
        }
    }
}