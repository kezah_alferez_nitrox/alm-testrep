using System.IO;
using System.Collections.Generic;
using System.Xml.Serialization;
using AssetModelingModule.BusinessLogic;
using AssetModelingModule.Domain;

namespace AssetModelingModule.Core.IO
{
    internal class BalanceXmlSerializer : IBalanceSerializer
    {
        public void Save(string fileName)
        {
            PersistenceSession.Flush();

            BalanceTemplate _sof = new BalanceTemplate();
            _sof.isLoad = false;
            XmlSerializer serializer = new XmlSerializer(typeof(BalanceTemplate));

            using (MemoryStream memoryStream = new MemoryStream())
            {
                serializer.Serialize(memoryStream, _sof);
            }

            using (FileStream stream = File.Open(fileName, FileMode.Create))
            {
                serializer.Serialize(stream, _sof);
            }

        }

        public void Load(string fileName)
        {
            BalanceTemplate newSaveOpenFile;

            BalanceBL.ClearCache();

            using (FileStream stream = File.Open(fileName, FileMode.Open))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(BalanceTemplate));
                newSaveOpenFile = (BalanceTemplate)serializer.Deserialize(stream);
                stream.Close();
            }

            newSaveOpenFile.isLoad = true;
            if (newSaveOpenFile.Currencies == null) newSaveOpenFile.Currencies = new Currency[0];
            if (newSaveOpenFile.Categories == null) newSaveOpenFile.Categories = new Category[0];
            if (newSaveOpenFile.Bonds == null) newSaveOpenFile.Bonds = new Bond[0];

            IDictionary<int, Currency> currencyMap = new Dictionary<int, Currency>();

            PersistenceSession.Flush();

            Persister persister = new Persister();
            persister.DeleteBalanceData();
            persister.PersistCurrencies(newSaveOpenFile.Currencies, currencyMap);
            persister.PersistCategoriesAndBonds(newSaveOpenFile.Categories, newSaveOpenFile.Bonds, currencyMap);
        }
    }
}