using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Text;
using System.Xml.Serialization;
using AssetModelingModule.BusinessLogic;
using AssetModelingModule.Domain;
using Castle.ActiveRecord;
using System.ComponentModel;

namespace AssetModelingModule.Core.IO
{
    public class WorkspaceXmlSerializer
    {
        private const string GzippedXmlFileHeader = "GXW";
        private const byte FileVersion = 1;
        private const int GzippedXmlSignatureLength = 6;
        private const int GzippedXmlFileHeaderLength = 7;

        public string Filename { get; set; }

        public GeneratorResultNode[] GeneratorResultNodes { get; private set; }
        public int[] ScenarioSetupColumnsOrder { get; private set; }

        public WorkspaceXmlSerializer()
            : this(null, null)
        {
        }

        public WorkspaceXmlSerializer(GeneratorResultNode[] generatorResultNodes, int[] scenarioSetupColumnsOrder)
        {
            this.ScenarioSetupColumnsOrder = scenarioSetupColumnsOrder;
            this.GeneratorResultNodes = generatorResultNodes;
        }

        public void Save(BackgroundWorker backgroundWorker)
        {
            using (new SessionScope())
            {
                WorkspaceTemplate workspaceTemplate = new WorkspaceTemplate();
                backgroundWorker.ReportProgress(3);
                workspaceTemplate.LoadFromDatabase(backgroundWorker);
                workspaceTemplate.GeneratorResultNodes = this.GeneratorResultNodes;
                workspaceTemplate.ScenarioSetupColumnsOrder = this.ScenarioSetupColumnsOrder;

                using (FileStream stream = File.Open(Filename, FileMode.Create))
                {
                    byte[] bytes = Encoding.Unicode.GetBytes(GzippedXmlFileHeader);
                    stream.Write(bytes, 0, bytes.Length);
                    stream.WriteByte(FileVersion);
                    backgroundWorker.ReportProgress(90);
                    using (GZipStream gZipStream = new GZipStream(stream, CompressionMode.Compress))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(WorkspaceTemplate));
                        serializer.Serialize(gZipStream, workspaceTemplate); 
                        backgroundWorker.ReportProgress(99);
                    }
                }
            }
        }
        
        public void Load(BackgroundWorker backgroundWorker)
        {
            using (new SessionScope())
            {
                BalanceBL.ClearCache();

                WorkspaceTemplate workspaceTemplate = LoadWorkspaceTemplate(Filename);

                if (workspaceTemplate.Categories == null) workspaceTemplate.Categories = new Category[0];
                if (workspaceTemplate.Bonds == null) workspaceTemplate.Bonds = new Bond[0];
                if (workspaceTemplate.Scenarios == null) workspaceTemplate.Scenarios = new Scenario[0];
                if (workspaceTemplate.VariableValues == null) workspaceTemplate.VariableValues = new VariableValue[0];
                if (workspaceTemplate.Variables == null) workspaceTemplate.Variables = new Variable[0];
                if (workspaceTemplate.Vectors == null) workspaceTemplate.Vectors = new Vector[0];
                if (workspaceTemplate.VectorPoints == null) workspaceTemplate.VectorPoints = new VectorPoint[0];
                if (workspaceTemplate.Currencies == null) workspaceTemplate.Currencies = new Currency[0];
                if (workspaceTemplate.FxRates == null) workspaceTemplate.FxRates = new FXRate[0];
                if (workspaceTemplate.Parameters == null) workspaceTemplate.Parameters = new Param[0];
                if (workspaceTemplate.Dividends == null) workspaceTemplate.Dividends = new Dividend[0];
                if (workspaceTemplate.OIAs == null) workspaceTemplate.OIAs = new OtherItemAssum[0];

                backgroundWorker.ReportProgress(5);

                Persister persister = new Persister();

                persister.DeleteScenarioData();
                persister.DeleteBalanceData();

                backgroundWorker.ReportProgress(25);

                IDictionary<int, Currency> currencyMap = new Dictionary<int, Currency>();

                persister.PersistCurrencies(workspaceTemplate.Currencies, currencyMap, backgroundWorker);

                persister.PersistCategoriesAndBonds(workspaceTemplate.Categories, workspaceTemplate.Bonds, currencyMap,
                                                    backgroundWorker);

                persister.PersistVariables(workspaceTemplate.Scenarios, workspaceTemplate.Variables,
                                           workspaceTemplate.VariableValues, backgroundWorker);

                persister.PersistVectors(workspaceTemplate.Vectors, workspaceTemplate.VectorPoints, backgroundWorker);

                backgroundWorker.ReportProgress(90);
                persister.PersistFxRates(workspaceTemplate.FxRates, currencyMap);
                backgroundWorker.ReportProgress(94);
                persister.PersistParameters(workspaceTemplate.Parameters, currencyMap);
                backgroundWorker.ReportProgress(96);
                persister.PersistOtherItemsAssum(workspaceTemplate.OIAs);
                backgroundWorker.ReportProgress(98);
                persister.PersistDividends(workspaceTemplate.Dividends);
                backgroundWorker.ReportProgress(100);

                this.GeneratorResultNodes = workspaceTemplate.GeneratorResultNodes;
                this.ScenarioSetupColumnsOrder = workspaceTemplate.ScenarioSetupColumnsOrder;
            }
        }

        private static WorkspaceTemplate LoadWorkspaceTemplate(string filename)
        {
            WorkspaceTemplate workspaceTemplate;

            using (FileStream stream = File.Open(filename, FileMode.Open))
            {
                stream.Seek(GzippedXmlSignatureLength, SeekOrigin.Begin);
                int version = stream.ReadByte();

                using (GZipStream gZipStream = new GZipStream(stream, CompressionMode.Decompress))
                {
                    switch (version)
                    {
                        case 1:
                            workspaceTemplate = DeserializeXmlScenarioTemplate(gZipStream);
                            break;
                        default:
                            throw new IOException("Invalid file format");
                    }
                }
            }

            return workspaceTemplate;
        }

        private static WorkspaceTemplate DeserializeXmlScenarioTemplate(Stream stream)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(WorkspaceTemplate));
            WorkspaceTemplate workspaceTemplate = (WorkspaceTemplate)serializer.Deserialize(stream);
            return workspaceTemplate;
        }
    }
}