using System;
using System.Xml;
using AssetModelingModule.Domain;

namespace AssetModelingModule.Core.IO
{
    class CategoryXmlSerializer
    {

        public void Save(string workspace)
        {
            XmlDocument _xml_doc = new XmlDocument();
            Category[] cats = Category.FindAll();
            XmlNode rootnode = _xml_doc.CreateElement("", "workspace", "");
            XmlAttribute rootAttribute = _xml_doc.CreateAttribute("name");
            rootAttribute.Value = workspace;
            rootnode.Attributes.Append(rootAttribute);

            rootnode.AppendChild(xNode(cats[0], _xml_doc));
            _xml_doc.AppendChild(rootnode);

            _xml_doc.Save(@"c:\_tmp\sg\Template" + workspace + ".xml");
        }

        private XmlNode xNode(Category cat, XmlDocument xml_doc)
        {
            XmlNode _xNode;
            if (cat.Children.Count > 0)
            {
                _xNode = NewNode(cat, xml_doc);
                foreach (Category _cat in cat.Children)
                {
                    _xNode.AppendChild(xNode(_cat, xml_doc));
                }
                return _xNode;
            }
            else
            {
                return _xNode = NewNode(cat, xml_doc);
            }
        }

        private Category xCat(XmlNode xNode, Category parent)
        {
            Category _xCat;
            if (xNode.ChildNodes.Count > 0)
            {
                _xCat = NewCategory(xNode, parent);
                foreach (XmlNode _node in xNode.ChildNodes)
                {
                    _xCat.Children.Add(xCat(_node, _xCat));
                }
                return _xCat;
            }
            else
            {
                return _xCat = NewCategory(xNode, parent);
            }
        }

        public void Load(string filePath)
        {
            XmlDocument xml_doc = new XmlDocument();
            xml_doc.Load(filePath);

            PersistenceSession.Flush();

            Category root = Category.FindRoot();
            if (root != null) root.DeleteAndFlush();

            Category _rootCat = xCat(xml_doc.ChildNodes[1].ChildNodes[0], null);
            _rootCat.CreateAndFlush();
        }

        #region NewCategory

        private Category NewCategory(string name, string balanceType, string readOnly, string type, Category parent)
        {
            Category newCategory = new Category();

            newCategory.Label = name;

            if (balanceType == "1")
                newCategory.BalanceType = BalanceType.Asset;
            else if (balanceType == "2")
                newCategory.BalanceType = BalanceType.Liability;
            else if (balanceType == "3")
                newCategory.BalanceType = BalanceType.Equity;

            newCategory.ReadOnly = bool.Parse(readOnly);

            newCategory.Type = (CategoryType)Enum.Parse(typeof(CategoryType), type, true);

            newCategory.Parent = parent;

            if ((parent == null) || (parent.Parent == null) || (parent.Parent.Parent == null))
                newCategory.isVisible = true;
            else
                newCategory.isVisible = false;

            return newCategory;
        }

        private Category NewCategory(XmlNode xNode, Category parrent)
        {
            if (xNode.Name == "category")
            {
                if (xNode.ParentNode.Name == "workspace")
                {
                    Category cat_root = new Category();

                    cat_root.Label = xNode.Attributes.Item(0).Value;
                    cat_root.BalanceType = BalanceType.None;
                    cat_root.ReadOnly = true;
                    cat_root.Parent = null;
                    cat_root.Type = CategoryType.Normal;
                    cat_root.isVisible = true;

                    return cat_root;
                }
                else
                {
                    if (xNode.Attributes["balanceType"] == null)
                    {
                        XmlNode _auxnode = xNode;
                        do
                        {
                            _auxnode = _auxnode.ParentNode;
                        }
                        while (_auxnode.Attributes["balanceType"] == null);
                        return NewCategory(xNode.Attributes["name"].Value, _auxnode.Attributes["balanceType"].Value, xNode.Attributes["readOnly"].Value, xNode.Attributes["type"].Value, parrent);
                    }
                    else
                    {
                        return NewCategory(xNode.Attributes["name"].Value, xNode.Attributes["balanceType"].Value, xNode.Attributes["readOnly"].Value, xNode.Attributes["type"].Value, parrent);
                    }
                }
            }
            else
                return null;
        }


        #endregion

        #region NewNode
        private XmlNode NewNode(string name, string balanceType, string readOnly, string rollAccount, XmlDocument xmlDoc)
        {
            XmlNode newNode = xmlDoc.CreateElement("", "category", "");

            XmlAttribute attrname = xmlDoc.CreateAttribute("name");
            attrname.Value = name;

            newNode.Attributes.Append(attrname);

            if (balanceType != string.Empty && balanceType != "None")
            {
                XmlAttribute attrballance = xmlDoc.CreateAttribute("balanceType");
                if (balanceType == BalanceType.Asset.ToString())
                    attrballance.Value = "1";
                else if (balanceType == BalanceType.Liability.ToString())
                    attrballance.Value = "2";
                else if (balanceType == BalanceType.Equity.ToString())
                    attrballance.Value = "3";


                newNode.Attributes.Append(attrballance);
            }

            if (readOnly != string.Empty && balanceType != "None")
            {
                XmlAttribute attrreadOnly = xmlDoc.CreateAttribute("readOnly");
                attrreadOnly.Value = readOnly;

                newNode.Attributes.Append(attrreadOnly);
            }

            if (rollAccount != string.Empty && balanceType != "None")
            {
                XmlAttribute attrollAccount = xmlDoc.CreateAttribute("rollAccount");
                attrollAccount.Value = rollAccount;

                newNode.Attributes.Append(attrollAccount);
            }

            return newNode;
        }

        private XmlNode NewNode(Category cat, XmlDocument _xml_doc)
        {
            if (cat.Parent != null)
            {
                if (cat.Parent.BalanceType == BalanceType.None)
                {
                    return NewNode(cat.Label, cat.BalanceType.ToString(), cat.ReadOnly.ToString(), cat.Type.ToString(), _xml_doc);
                }
                else
                {
                    return NewNode(cat.Label, cat.ReadOnly.ToString(), cat.Type.ToString(), _xml_doc);
                }
            }
            else
            {
                return NewNode(cat.Label, cat.BalanceType.ToString(), cat.ReadOnly.ToString(), cat.Type.ToString(), _xml_doc);
            }
        }

        private XmlNode NewNode(string name, XmlDocument xmldoc)
        {
            return NewNode(name, string.Empty, string.Empty, string.Empty, xmldoc);
        }

        private XmlNode NewNode(string name, string readOnly, string rollAccount, XmlDocument xmldoc)
        {
            return NewNode(name, string.Empty, readOnly, rollAccount, xmldoc);
        }
        #endregion


    }
}
