

namespace AssetModelingModule.Core.IO
{
     internal interface IScenarioSerializer
    {
        void Save(string fileName);
        void Load(string fileName);
    }
}
