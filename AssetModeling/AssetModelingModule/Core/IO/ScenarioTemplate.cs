using System;
using System.Xml.Serialization;
using AssetModelingModule.Domain;

namespace AssetModelingModule.Core.IO
{
    [XmlRoot("ScenarioTemplateList")]
    public class ScenarioTemplate
    {
        public bool isLoad = false;

        private Variable[] variables;
        private VariableValue[] variableValues;
        private Scenario[] scenarios;
        private Vector[] vectors;
        private VectorPoint[] vectorPoints;

        private Currency[] currencies;
        private FXRate[] fxRates;
        private DateTime? valuationDate;
        private Currency valuationCurrency;

        private Dividend[] dividends;
        private OtherItemAssum[] othierItemAssums;

        [XmlElement]
        public Variable[] Variables
        {
            get
            {
                if (isLoad)
                    return variables;
                else
                    return Variable.FindAll();
            }
            set { variables = value; }
        }

        [XmlElement]
        public VariableValue[] VariableValues
        {
            get
            {
                if (isLoad)
                    return variableValues;
                else
                    return VariableValue.FindAll();
            }
            set { variableValues = value; }
        }

        [XmlElement]
        public Scenario[] Scenarios
        {
            get
            {
                if (isLoad)
                    return scenarios;
                else
                    return Scenario.FindAll();
            }
            set { scenarios = value; }
        }

        [XmlElement]
        public Vector[] Vectors
        {
            get
            {
                if (isLoad)
                    return vectors;
                else
                    return Vector.FindAll(); // Vector.FindByGroup(VectorGroup.Workspace);
            }
            set { vectors = value; }
        }

        [XmlElement]
        public VectorPoint[] VectorPoints
        {
            get
            {
                if (isLoad)
                    return vectorPoints;
                else
                    return VectorPoint.FindAll();// VectorPoint.FindByVectorGroup(VectorGroup.Workspace);
            }
            set { vectorPoints = value; }
        }

        [XmlElement]
        public Currency[] Currencies
        {
            get
            {
                if (isLoad)
                    return currencies;
                else
                    return Currency.FindAll();
            }
            set { currencies = value; }
        }

        [XmlElement]
        public FXRate[] FxRates
        {
            get
            {
                if (isLoad) return fxRates;
                else return FXRate.FindAll();
            }
            set { fxRates = value; }
        }

        [XmlElement]
        public DateTime? ValuationDate
        {
            get
            {
                if (isLoad) return valuationDate;
                else return Param.ValuationDate;
            }
            set { valuationDate = value; }
        }

        [XmlElement]
        public Currency ValuationCurrency
        {
            get
            {
                if (isLoad) return valuationCurrency;
                else return Param.ValuationCurrency;
            }
            set { valuationCurrency = value; }
        }

        [XmlElement]
        public Dividend[] Dividends
        {
            get
        {
            if (isLoad) return dividends;
            else return Dividend.FindAll();
        }
        set { dividends = value; }
        }

        [XmlElement]
        public OtherItemAssum[] OthierItemAssums
        {
            get
            {
                if (isLoad) return othierItemAssums;
                else return OtherItemAssum.FindAll();
            }
            set { othierItemAssums = value; }
        }
    
    }
}
