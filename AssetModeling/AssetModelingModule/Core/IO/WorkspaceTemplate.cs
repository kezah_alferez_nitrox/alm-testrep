using System.Xml.Serialization;
using AssetModelingModule.Domain;
using Castle.ActiveRecord;
using System.ComponentModel;

namespace AssetModelingModule.Core.IO
{
    [XmlRoot("Workspace")]
    public class WorkspaceTemplate
    {
        private Category[] categories;
        private Bond[] bonds;

        private Variable[] variables;
        private VariableValue[] variableValues;
        private Scenario[] scenarios;
        private Vector[] vectors;
        private VectorPoint[] vectorPoints;

        private Currency[] currencies;
        private FXRate[] fxRates;
        private Param[] parameters;

        private OtherItemAssum[] oias;
        private Dividend[] dividends;

        public void LoadFromDatabase(BackgroundWorker backgroundWorker)
        {
            categories = ActiveRecordBase<Category>.FindAll();
            backgroundWorker.ReportProgress(5);
            bonds = ActiveRecordBase<Bond>.FindAll();
            backgroundWorker.ReportProgress(10);
            variables = ActiveRecordBase<Variable>.FindAll();
            backgroundWorker.ReportProgress(11);
            variableValues = ActiveRecordBase<VariableValue>.FindAll();
            backgroundWorker.ReportProgress(13);
            scenarios = ActiveRecordBase<Scenario>.FindAll();
            backgroundWorker.ReportProgress(14);
            vectors = ActiveRecordBase<Vector>.FindAll(); // Vector.FindByGroup(VectorGroup.Workspace);
            backgroundWorker.ReportProgress(15);
            vectorPoints = ActiveRecordBase<VectorPoint>.FindAll();
            backgroundWorker.ReportProgress(20);
                // VectorPoint.FindByVectorGroup(VectorGroup.Workspace);
            currencies = ActiveRecordBase<Currency>.FindAll();
            backgroundWorker.ReportProgress(21);
            fxRates = ActiveRecordBase<FXRate>.FindAll();
            backgroundWorker.ReportProgress(22);
            parameters = ActiveRecordBase<Param>.FindAll();
            backgroundWorker.ReportProgress(23);
            oias = ActiveRecordBase<OtherItemAssum>.FindAll();
            backgroundWorker.ReportProgress(24);
            dividends = ActiveRecordBase<Dividend>.FindAll();
            backgroundWorker.ReportProgress(25);

        }

        [XmlElement]
        public Category[] Categories
        {
            get { return categories; }
            set { categories = value; }
        }

        [XmlElement]
        public Bond[] Bonds
        {
            get { return bonds; }
            set { bonds = value; }
        }

        [XmlElement]
        public Variable[] Variables
        {
            get { return variables; }
            set { variables = value; }
        }

        [XmlElement]
        public VariableValue[] VariableValues
        {
            get { return variableValues; }
            set { variableValues = value; }
        }

        [XmlElement]
        public Scenario[] Scenarios
        {
            get { return scenarios; }
            set { scenarios = value; }
        }

        [XmlElement]
        public Vector[] Vectors
        {
            get { return vectors; }
            set { vectors = value; }
        }

        [XmlElement]
        public VectorPoint[] VectorPoints
        {
            get { return vectorPoints; }
            set { vectorPoints = value; }
        }

        [XmlElement]
        public Currency[] Currencies
        {
            get { return currencies; }
            set { currencies = value; }
        }

        [XmlElement]
        public FXRate[] FxRates
        {
            get { return fxRates; }
            set { fxRates = value; }
        }

        [XmlElement]
        public Param[] Parameters
        {
            get { return parameters; }
            set { parameters = value; }
        }

        [XmlElement]
        public OtherItemAssum[] OIAs
        {
            get { return oias; }
            set { oias = value; }
        }

        [XmlElement]
        public Dividend[] Dividends
        {
            get { return dividends; }
            set { dividends = value; }
        }

        [XmlElement]
        public GeneratorResultNode[] GeneratorResultNodes { get; set; }

        [XmlElement]
        public int[] ScenarioSetupColumnsOrder { get; set; }
    }
}