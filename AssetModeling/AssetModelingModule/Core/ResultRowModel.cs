namespace AssetModelingModule.Core
{
    public enum ResultRowType
    {
        None,
        Normal,
        MasterHeader,
        PeriodHeader,
        Header,
        WhiteHeader,
        Footer,
        MasterFooter,
        Percentage
    }

    public class ResultRowModel
    {
        private readonly object[] rowValues;

        public ResultSection? ResultSection { get; set; }

        public ResultRowModel(string description, ResultRowType type, int valueNumber)
        {
            this.rowValues = new object[valueNumber + 2];
            this.Type = type;
            this.Description = description;            
        }

        public object this[int i]
        {
            get { return this.rowValues[i + 2]; }
            set{ this.rowValues[i + 2] = value;}
        }

        public string Description
        {
            get { return (string) this.rowValues[1]; }
            set { this.rowValues[1] = value; }
        }

        public ResultRowType Type
        {
            get { return (ResultRowType) this.rowValues[0]; }
            set { this.rowValues[0] = value; }
        }

        public object[] RowValues
        {
            get { return this.rowValues; }
        }
    }
}