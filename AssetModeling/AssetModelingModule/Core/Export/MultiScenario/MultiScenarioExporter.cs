using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using AssetModelingModule.Calculus;
using Castle.ActiveRecord;
using Microsoft.Office.Interop.Excel;
using Application = System.Windows.Forms.Application;
using Scenario = AssetModelingModule.Domain.Scenario;

namespace AssetModelingModule.Core.Export.MultiScenario
{
    public class MultiScenarioExporter
    {
        private readonly Simulation simulation;
        private readonly ReportType reportType;
        private readonly DateTime valuationDate;
        private int resultColumns = 0;
        private int resultRows = 0;

        private readonly IDictionary<Scenario, ResultModel> models = new Dictionary<Scenario, ResultModel>();

        public MultiScenarioExporter(Simulation simulation, ReportType reportType, DateTime valuationDate)
        {
            this.simulation = simulation;
            this.reportType = reportType;
            this.valuationDate = valuationDate;
        }

        public void Export()
        {
            Cursor.Current = Cursors.WaitCursor;
            string templatePath = Path.GetDirectoryName(Application.ExecutablePath) + "\\Excel\\Template\\MultiScenarioTemplate.xls ";

            string outFolder = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) +
                               "\\SOGALMS\\Excel\\Reports";

            if (!Directory.Exists(outFolder)) Directory.CreateDirectory(outFolder);

            string outputPath = outFolder + "\\MultiScenario_" + DateTime.Now.ToString("dd_MM_yyyy_hh-mm-ss") + ".xls";
            if (File.Exists(outputPath)) File.Delete(outputPath);

            ApplicationClass app = null;
            Workbook workBook = null;

            try
            {
                app = new ApplicationClass();
                app.Visible = false;

                workBook = app.Workbooks.Open(templatePath,
                                              0,
                                              true,
                                              5,
                                              "",
                                              "",
                                              true,
                                              XlPlatform.xlWindows,
                                              "\t",
                                              false,
                                              false,
                                              0,
                                              true,
                                              1,
                                              0);

                this.ExportToWorkbook(workBook);

                workBook.Close(true, outputPath, true);
            }
            catch (Exception ex)
            {
                Log.Exception("MultiScenario export ", ex);
                throw;
            }
            finally
            {
                if (null != workBook)
                {
                    try
                    {
                        workBook.Close(false, Type.Missing, Type.Missing);
                    }
                    catch{/*ignored*/}

                    Marshal.ReleaseComObject(workBook);
                }
                if (null != app)
                {
                    app.Quit();
                    Marshal.ReleaseComObject(app);
                }
            }

            GC.Collect();
            GC.WaitForPendingFinalizers();

            System.Diagnostics.Process.Start(outputPath);

            Cursor.Current = Cursors.Default;
        }

        private void ExportToWorkbook(Workbook workbook)
        {
            using (new SessionScope(FlushAction.Never))
            {
                this.PrepareData();

                SyntheseExporter syntheseExporter = new SyntheseExporter((Worksheet) workbook.Sheets[1],
                                                                         this.models, this.resultColumns);
                syntheseExporter.Export();

                HypotesesBaseExporter hypotesesBaseExporter =
                    new HypotesesBaseExporter((Worksheet) workbook.Sheets[2], this.models);
                hypotesesBaseExporter.Export();

                BalanceSheetExporter balanceSheetExporter =
                    new BalanceSheetExporter((Worksheet) workbook.Sheets[3], this.models, this.resultColumns,
                                             this.resultRows);
                balanceSheetExporter.Export();

                ProfitAndLossExporter profitAndLossExporter =
                    new ProfitAndLossExporter((Worksheet) workbook.Sheets[4], this.models, this.resultColumns,
                                              this.resultRows);
                profitAndLossExporter.Export();
            }
        }

        private void PrepareData()
        {
            foreach (Scenario scenario in this.simulation.Scenarios)
            {
                ScenarioData data = this.simulation[scenario];
                ResultModel resultModel = new ResultModel(data, this.reportType, this.valuationDate);
                this.models.Add(scenario, resultModel);

                this.resultRows = resultModel.GetRows().Count;
                this.resultColumns = Math.Max(this.resultColumns, resultModel.DateIntervals.Count);
            }            
        }
    }
}