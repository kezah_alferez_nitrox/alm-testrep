using System.Collections.Generic;
using Microsoft.Office.Interop.Excel;
using Scenario = AssetModelingModule.Domain.Scenario;

namespace AssetModelingModule.Core.Export.MultiScenario
{
    public abstract class BaseResultExporter
    {
        private readonly Worksheet worksheet;
        private readonly IDictionary<Scenario, ResultModel> models;
        private readonly int columnCount;
        private readonly int rowCount;

        public BaseResultExporter(Worksheet worksheet, IDictionary<Scenario, ResultModel> simulation, int columns, int rows)
        {
            this.worksheet = worksheet;
            this.models = simulation;
            this.columnCount = columns;
            this.rowCount = rows;
        }

        public void Export()
        {
            if (this.models.Count == 0) return;

            this.PrepareCells();

            ExcelUtil.InsertRows(worksheet, 3, rowCount);

            int scenarioNo = 0;
            foreach (Scenario scenario in this.models.Keys)
            {
                ExcelUtil.SetCellValue(worksheet, 1, 2 + scenarioNo * columnCount, "SCENARIO " + scenario.Name);

                ResultModel result = this.models[scenario];
                IList<ResultRowModel> resultRows = result.GetRows();

                int skippedRows = 0;
                for (int rowNo = 0; rowNo < resultRows.Count; rowNo++)
                {
                    ResultRowModel row = resultRows[rowNo];

                    if(rowNo == 0)
                    {
                        for (int colNo = 0; colNo < columnCount; colNo++)
                        {
                            ExcelUtil.SetCellValue(this.worksheet, 2,
                                2 + scenarioNo * columnCount + colNo, row[colNo]);
                        }
                        continue;
                    }

                    if (this.SkipRow(row))
                    {
                        skippedRows++;
                        continue;
                    }

                    if (scenarioNo == 0)
                    {
                        ExcelUtil.SetCellValue(this.worksheet, 2 + rowNo - skippedRows, 1, row.Description);
                        if(row.Type == ResultRowType.Footer || row.Type == ResultRowType.MasterFooter)
                        {
                            ExcelUtil.MakeRowBold(this.worksheet, 2 + rowNo - skippedRows);
                        }
                    }

                    for (int colNo = 0; colNo < columnCount; colNo++)
                    {
                        ExcelUtil.SetCellValue(this.worksheet, 2 + rowNo - skippedRows, 
                            2 + scenarioNo * columnCount + colNo, row[colNo]);
                    }
                }

                scenarioNo++;
            }
        }

        protected abstract bool SkipRow(ResultRowModel row);

        private void PrepareCells()
        {
            ExcelUtil.InsertColumns(this.worksheet, 3, this.columnCount * this.models.Count - 2);

            ExcelUtil.Merge(this.worksheet, 2, columnCount);
            for (int i = 1; i < this.models.Count; i++)
            {
                ExcelUtil.Merge(this.worksheet, 2 + i * this.columnCount, columnCount);
                ExcelUtil.CopyColumns(this.worksheet, 2, columnCount, 2 + i * this.columnCount);
            }
        }
    }
}