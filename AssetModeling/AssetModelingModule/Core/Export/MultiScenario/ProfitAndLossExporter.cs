using System.Collections.Generic;
using Microsoft.Office.Interop.Excel;
using Scenario = AssetModelingModule.Domain.Scenario;

namespace AssetModelingModule.Core.Export.MultiScenario
{
    public class ProfitAndLossExporter : BaseResultExporter
    {
        public ProfitAndLossExporter(Worksheet worksheet, IDictionary<Scenario, ResultModel> simulation, int columns, int rows) : base(worksheet, simulation, columns, rows)
        {
        }

        protected override bool SkipRow(ResultRowModel row)
        {
            return (row.ResultSection.GetValueOrDefault() == ResultSection.BookValue  ||
                row.ResultSection.GetValueOrDefault() == ResultSection.Yield ||
                    row.Type == ResultRowType.MasterHeader || row.Type == ResultRowType.Header);
        }
    }
}