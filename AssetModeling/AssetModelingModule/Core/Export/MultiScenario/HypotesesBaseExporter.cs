using System;
using System.Collections.Generic;
using AssetModelingModule.Domain;
using Eu.AlterSystems.ASNetLib.Core;
using Microsoft.Office.Interop.Excel;
using Scenario = AssetModelingModule.Domain.Scenario;

namespace AssetModelingModule.Core.Export.MultiScenario
{
    public class HypotesesBaseExporter
    {
        private readonly Worksheet worksheet;
        private readonly IDictionary<Scenario, ResultModel> simulation;
        private readonly Variable[] variables;

        private class BondData
        {
            public string Label { get; set; }
            public Bond Bond { get; set; }

            public BondData(string label, Bond bond)
            {
                this.Label = label;
                this.Bond = bond;
            }
        }

        public HypotesesBaseExporter(Worksheet worksheet, IDictionary<Scenario, ResultModel> simulation)
        {
            this.worksheet = worksheet;
            this.simulation = simulation;

            this.variables = Variable.FindSelected();
        }

        public void Export()
        {
            this.PrepareCells();

            IList<BondData> bondDatas = new List<BondData>();
            Category root = Category.FindRoot();
            FillBonds(root, bondDatas, 0);

            ExcelUtil.InsertRows(worksheet, 3, bondDatas.Count);

            for (int i = 0; i < bondDatas.Count; i++)
            {
                ExcelUtil.SetCellValue(worksheet, 3 + i, 1, bondDatas[i].Label);
            }


            int scenarioNo = 0;
            foreach (Scenario scenario in simulation.Keys)
            {
                ExcelUtil.SetCellValue(worksheet, 1, 2 + scenarioNo * 8, "SCENARIO " + scenario.Name);

                for (int i = 0; i < bondDatas.Count; i++)
                {
                    Bond bond = bondDatas[i].Bond;
                    if (bondDatas[i].Bond == null) continue;

                    ExcelUtil.SetCellValue(worksheet, 3 + i, 0 + 2 + scenarioNo * 8, ReplaceVariables(bond.Coupon, scenario));
                    ExcelUtil.SetCellValue(worksheet, 3 + i, 1 + 2 + scenarioNo * 8, EnumHelper.GetDescription(bond.Type));
                    ExcelUtil.SetCellValue(worksheet, 3 + i, 2 + 2 + scenarioNo * 8, ReplaceVariables(bond.CPR, scenario));
                    ExcelUtil.SetCellValue(worksheet, 3 + i, 3 + 2 + scenarioNo * 8, ReplaceVariables(bond.CDR, scenario));
                    ExcelUtil.SetCellValue(worksheet, 3 + i, 4 + 2 + scenarioNo * 8, ReplaceVariables(bond.LGD, scenario));
                    ExcelUtil.SetCellValue(worksheet, 3 + i, 5 + 2 + scenarioNo * 8, string.IsNullOrEmpty(bond.RollSpec) ? "n" : "y");
                    ExcelUtil.SetCellValue(worksheet, 3 + i, 6 + 2 + scenarioNo * 8, Convert.ToString(EnumHelper.GetDescription(bond.Period)[0]));
                    ExcelUtil.SetCellValue(worksheet, 3 + i, 7 + 2 + scenarioNo * 8, bond.Duration ?? Constants.DEFAULT_BOND_DURATION);
                }

                scenarioNo++;
            }
        }

        private string ReplaceVariables(string formula, Scenario scenario)
        {
            if (string.IsNullOrEmpty(formula)) return formula;

            foreach (Variable variable in variables)
            {
                if (formula.IndexOf(variable.Name, StringComparison.InvariantCultureIgnoreCase) >= 0)
                {
                    string value = string.Empty;
                    foreach (VariableValue variableValue in variable.VariableValues)
                    {
                        if (variableValue.Scenario.Id == scenario.Id)
                        {
                            value = variableValue.Value;
                            break;
                        }
                    }

                    if (value != null)
                    {
                        formula = formula.Replace(variable.Name, value);
                    }
                }
            }
            return formula;
        }

        private void FillBonds(Category category, IList<BondData> bonds, int level)
        {
            foreach (Category childCategory in category.Children)
            {
                string label = new string(' ', level * 3) + childCategory.Label;

                if (childCategory.Bonds.Count > 0)
                {
                    bonds.Add(new BondData(label, childCategory.Bonds[0]));
                }
                else
                {
                    bonds.Add(new BondData(label, null));
                    this.FillBonds(childCategory, bonds, level + 1);
                }
            }
        }

        private void PrepareCells()
        {
            ExcelUtil.InsertColumns(this.worksheet, 10, 8 * (this.simulation.Count - 1));
            for (int i = 1; i < this.simulation.Count; i++)
            {
                ExcelUtil.CopyColumns(this.worksheet, 2, 8, 2 + i * 8);
            }
        }
    }
}