using System;
using System.Collections.Generic;
using Microsoft.Office.Interop.Excel;
using Scenario = AssetModelingModule.Domain.Scenario;

namespace AssetModelingModule.Core.Export.MultiScenario
{
    public class SyntheseExporter
    {
        private const int BalanceSheetInitialRow = 7;
        private const int EquityInitialRow = 11;
        private const int NetIncomeInitialRow = 3;

        private readonly Worksheet worksheet;
        private readonly IDictionary<Scenario, ResultModel> models;
        private readonly int columnCount;

        public SyntheseExporter(Worksheet worksheet, IDictionary<Scenario, ResultModel> simulation, int columns)
        {
            this.worksheet = worksheet;
            this.models = simulation;
            this.columnCount = columns;
        }

        public void Export()
        {
            if (this.models.Count == 0) return;

            ExcelUtil.InsertRows(this.worksheet, EquityInitialRow, this.models.Count - 1);
            ExcelUtil.InsertRows(this.worksheet, BalanceSheetInitialRow, this.models.Count - 1);
            ExcelUtil.InsertRows(this.worksheet, NetIncomeInitialRow, this.models.Count - 1);

            ExcelUtil.InsertColumns(this.worksheet, 3, this.columnCount - 2);

            int i = 0;
            foreach (Scenario scenario in this.models.Keys)
            {
                ResultModel result = this.models[scenario];
                IList<ResultRowModel> resultRows = result.GetRows();

                int firstRow = NetIncomeInitialRow;
                ExcelUtil.SetCellValue(this.worksheet, firstRow + i, 1, scenario.Name);
                this.FillRow(firstRow, i, resultRows, "Net Income");

                firstRow = BalanceSheetInitialRow + this.models.Count - 1;
                ExcelUtil.SetCellValue(this.worksheet, firstRow + i, 1, scenario.Name);
                this.FillRow(firstRow, i, resultRows, "Book Values - Total Asset");

                firstRow = EquityInitialRow + this.models.Count * 2 - 2;
                ExcelUtil.SetCellValue(this.worksheet, firstRow + i, 1, scenario.Name);
                this.FillRow(firstRow, i, resultRows, "Book Values - Total Equity");

                i++;
            }
        }

        private void FillRow(int firstRow, int i, IList<ResultRowModel> resultRows, string label)
        {
            for (int j = 0; j < this.columnCount; j++)
            {
                ExcelUtil.SetCellValue(this.worksheet, firstRow - 1, 2 + j, resultRows[0][j]);
                ResultRowModel row = FindRow(resultRows, label);
                if (row != null)
                {
                    ExcelUtil.SetCellValue(this.worksheet, firstRow + i, 2 + j, row[j]);
                }
            }
        }

        private static ResultRowModel FindRow(IList<ResultRowModel> rows, string label)
        {
            foreach (ResultRowModel row in rows)
            {
                if (row.Description.IndexOf(label, StringComparison.InvariantCultureIgnoreCase) >= 0)
                {
                    return row;
                }
            }
            return null;
        }
    }
}