using System.Collections.Generic;
using Microsoft.Office.Interop.Excel;
using Scenario = AssetModelingModule.Domain.Scenario;

namespace AssetModelingModule.Core.Export.MultiScenario
{
    public class BalanceSheetExporter : BaseResultExporter
    {
        public BalanceSheetExporter(Worksheet worksheet, IDictionary<Scenario, ResultModel> simulation, int columns, int rows) : base(worksheet, simulation, columns, rows)
        {
        }

        protected override bool SkipRow(ResultRowModel row)
        {
            return (!row.ResultSection.HasValue ||
                    row.ResultSection.GetValueOrDefault() != ResultSection.BookValue ||
                    row.Type == ResultRowType.MasterHeader || row.Type == ResultRowType.Header);
        }
    }
}