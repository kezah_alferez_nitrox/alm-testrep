using System;

namespace AssetModelingModule.Core
{
    public struct DateInterval
    {
        public readonly DateTime Start;
        public readonly int MonthStart;
        public readonly DateTime End;
        public readonly int MonthEnd;
        public readonly int Length;

        public DateInterval(DateTime start, DateTime end)
        {
            this.Start = start;
            this.End = end;

            this.MonthStart = Tools.GetMonthNumber(this.Start);
            this.MonthEnd = Tools.GetMonthNumber(this.End);
            this.Length = this.MonthEnd - this.MonthStart + 1;
        }
    }
}