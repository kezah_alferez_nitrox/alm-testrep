﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;

namespace AssetModelingModule.Core
{
    /// <summary>
    /// See http://msdn.microsoft.com/en-us/library/ms993236.aspx
    /// </summary>
    public class PropertyComparer<T> : IComparer<T>
    {
        // The following code contains code implemented by Rockford Lhotka:
        // http://msdn.microsoft.com/library/default.asp?url=/library/en-us/dnadvnet/html/vbnet01272004.asp

        private readonly PropertyDescriptor _property;
        private readonly ListSortDirection _direction;

        public PropertyComparer(PropertyDescriptor property, ListSortDirection direction)
        {
            _property = property;
            _direction = direction;
        }

        #region IComparer<T>

        public int Compare(T xWord, T yWord)
        {
            // Get property values
            object xValue = GetPropertyValue(xWord, _property.Name);
            object yValue = GetPropertyValue(yWord, _property.Name);

            // Determine sort order
            return _direction == ListSortDirection.Ascending ? 
                CompareAscending(xValue, yValue) : 
                CompareDescending(xValue, yValue);
        }

        public bool Equals(T xWord, T yWord)
        {
            return xWord.Equals(yWord);
        }

        public int GetHashCode(T obj)
        {
            return obj.GetHashCode();
        }

        #endregion

        // Compare two property values of any type
        private static int CompareAscending(object xValue, object yValue)
        {
            int result;

            // If values implement IComparer
            if(null == xValue && null == yValue)
            {
                result = 0;
            }
            else if (null == xValue)
            {
                result = 1;
            }
            else if (null == yValue)
            {
                result = -1;
            }
            else if (xValue is IComparable)
            {
                result = ((IComparable) xValue).CompareTo(yValue);
            }
                // If values don't implement IComparer but are equivalent
            else if (xValue.Equals(yValue))
            {
                result = 0;
            }
                // Values don't implement IComparer and are not equivalent, so compare as string values
            else result = xValue.ToString().CompareTo(yValue.ToString());

            // Return result
            return result;
        }

        private static int CompareDescending(object xValue, object yValue)
        {
            // Return result adjusted for ascending or descending sort order ie
            // multiplied by 1 for ascending or -1 for descending
            return CompareAscending(xValue, yValue)*-1;
        }

        private static object GetPropertyValue(T value, string property)
        {
            // Get property
            PropertyInfo propertyInfo = value.GetType().GetProperty(property);

            // Return value
            return propertyInfo.GetValue(value, null);
        }
    }
}