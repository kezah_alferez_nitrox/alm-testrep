using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using AssetModelingModule.Domain;
using Microsoft.Office.Interop.Excel;

namespace AssetModelingModule.Core.Import
{
    internal class ExcelMarketDataImporter : IMarketDataImporter
    {
        private const int FIRST_ROW = 1;
        private const int FIRST_COL = 1;

        private const int MAX_ROW = 1000;
        private const string CURRENCY = "Currency";
        private const string VOLVECTORS = "VolVectors";

        private readonly IDictionary<string, Vector> cacheVectors = new Dictionary<string, Vector>();

        public bool CanImportFrom(string fileName)
        {
            return Path.GetExtension(fileName) == "xls";
        }

        public Vector[] ImportFrom(string fileName, DateTime startDate, BackgroundWorker backgroundWorker)
        {
            List<Vector> vectors = new List<Vector>();

            InitialiseVectorCache();

            // initialize the Excel Application class
            ApplicationClass app = null;
            Workbook workBook = null;

            try
            {
                app = new ApplicationClass();
                app.Visible = false;

                // create the workbook object by opening the excel file.
                workBook = app.Workbooks.Open(fileName,
                                              0,
                                              true,
                                              5,
                                              "",
                                              "",
                                              true,
                                              XlPlatform.xlWindows,
                                              "\t",
                                              false,
                                              false,
                                              0,
                                              true,
                                              1,
                                              0);

                int count = workBook.Sheets.Count;
                for (int i = 1; i <= count; i++)
                {
                    backgroundWorker.ReportProgress(50 * i / count);
                    Worksheet sheet = (Worksheet)workBook.Sheets[i];
                    vectors.AddRange(ImportFromSheet(sheet, startDate));
                    Marshal.ReleaseComObject(sheet);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                Debugger.Break();
#endif
                Log.Exception(fileName, ex);
            }
            finally
            {
                if (null != workBook)
                {
                    workBook.Close(false, Type.Missing, Type.Missing);
                    Marshal.ReleaseComObject(workBook);
                }

                if (null != app)
                {
                    //Close running excel app
                    app.Quit();
                    //Use the Com Object interop marshall to release the excel object
                    Marshal.ReleaseComObject(app);
                }
            }

            return vectors.ToArray();
        }

        private void InitialiseVectorCache()
        {
            cacheVectors.Clear();
            Vector[] allVectors = Vector.FindAll();
            foreach (Vector vector in allVectors)
            {
                cacheVectors[vector.Name] = vector;
            }
        }

        private IEnumerable<Vector> ImportFromSheet(Worksheet workSheet, DateTime startDate)
        {
            Range cells = workSheet.Cells;

            List<Vector> vectors = new List<Vector>();

            // find first non empty row
            int row = FIRST_ROW - 1;
            Range cell;
            do
            {
                row++;
                cell = ((Range)cells[row, FIRST_COL]);
            } while (IsEmptyCell(cell) && row < MAX_ROW);

            if (row == MAX_ROW) return null;

            // first non empty row in the header
            int headerRow = row;
            row++;

            // check for currency column
            string colHeader = ((Range)cells[headerRow, FIRST_COL + 1]).Value2.ToString();
            bool hasCurrency = string.Compare(colHeader, CURRENCY, StringComparison.InvariantCultureIgnoreCase) == 0;
            int firstCol = hasCurrency ? FIRST_COL + 2 : FIRST_COL + 1;

            // check if data columns start with "today"
            colHeader = ((Range)cells[headerRow, firstCol]).Value2.ToString();
            if (string.Compare(colHeader, "Today", StringComparison.InvariantCultureIgnoreCase) != 0)
            {
                // if first data column != Today we suppose it's 1 month from now
                startDate = startDate.AddMonths(1);
            }

            cell = ((Range)cells[row, FIRST_COL]);
            while (!IsEmptyCell(cell))
            {
                vectors.Add(GetVectorAtRow(workSheet, hasCurrency, firstCol, row, startDate));
                cell = ((Range)cells[++row, FIRST_COL]);
            }

            return vectors;
        }

        private Vector GetVectorAtRow(Worksheet sheet, bool hasCurrency, int firstCol, int currentRow, DateTime startDate)
        {
            object[,] rowValues = (object[,])((Range)sheet.Rows[currentRow, Type.Missing]).Value2;

            string vectorName = rowValues[1, FIRST_COL].ToString();
            Vector vector = GetVectorByNameOrCreateNew(vectorName);
            vector.Frequency = VectorFrequency.Monthly;
            vector.Group = GetVectorGroupByImportName(sheet.Name);

            vector.VectorPoints.Clear();

            if (hasCurrency)
            {
                string currencyName = rowValues[1, FIRST_COL + 1].ToString();
                vector.Currency = Currency.FindOrCreateByShortName(currencyName);
            }

            int currentCol = firstCol;
            object cellValue = rowValues[1, currentCol];
            while (cellValue != null)
            {
                decimal cellDecimalValue = (decimal)((double)cellValue);

                VectorPoint vectorPoint = new VectorPoint();
                vectorPoint.Date = startDate.AddMonths(currentCol - firstCol);
                vectorPoint.Value = cellDecimalValue;
                vectorPoint.Vector = vector;

                vector.VectorPoints.Add(vectorPoint);
                currentCol++;
                cellValue = rowValues[1, currentCol];
            }

            return vector;
        }

        private static VectorGroup GetVectorGroupByImportName(string name)
        {
            if (string.Compare(name, VOLVECTORS, StringComparison.InvariantCultureIgnoreCase) == 0)
            {
                return VectorGroup.MarketVol;
            }
            else
            {
                return VectorGroup.MarketData;
            }
        }

        private Vector GetVectorByNameOrCreateNew(string name)
        {
            Vector vector;

            if (!cacheVectors.TryGetValue(name, out vector))
            {
                vector = new Vector();
                vector.Name = name;
            }

            return vector;
        }

        private static bool IsEmptyCell(Range cell)
        {
            return cell.Value2 == null || string.IsNullOrEmpty(cell.Value2.ToString());
        }
    }
}