using System;
using System.ComponentModel;
using AssetModelingModule.Domain;

namespace AssetModelingModule.Core.Import
{
    public interface IMarketDataImporter
    {
        bool CanImportFrom(string fileName);
        Vector[] ImportFrom(string fileName, DateTime startDate, BackgroundWorker backgroundWorker);
    }
}