using System;
using System.ComponentModel;
using System.Globalization;
using System.Threading;
using AssetModelingModule.BusinessLogic;
using AssetModelingModule.Domain;
using Castle.ActiveRecord;

namespace AssetModelingModule.Core.Import
{
    public class MarketDataImport
    {
        private readonly string fileName;
        private readonly DateTime startDate;

        public MarketDataImport(string fileName, DateTime startDate)
        {
            this.fileName = fileName;
            this.startDate = startDate;
        }

        public void Import(BackgroundWorker backgroundWorker)
        {
            using (new SessionScope())
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo(Constants.CULTURE_NAME);

                IMarketDataImporter importer = new ExcelMarketDataImporter();

                Vector[] vectors = importer.ImportFrom(fileName, startDate, backgroundWorker);

                int count = vectors.Length;
                for (int i = 0; i < count; i++)
                {
                    backgroundWorker.ReportProgress(50 + 49*(i + 1)/count);
                    Vector vector = vectors[i];
                    vector.Save();
                }

                PersistenceSession.Flush();
                CurrencyBL.ResetValuationDateFxRates();
            }
        }
    }
}