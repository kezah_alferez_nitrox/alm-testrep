using System;
using System.Collections.Generic;
using AssetModelingModule.Calculus;
using AssetModelingModule.Domain;
using AssetModelingModule.Resources.Language;
using Castle.ActiveRecord;

namespace AssetModelingModule.Core
{
    public class ResultModel
    {
        private const string HeaderDateFormat = "MM/yyyy";

        private readonly ScenarioData scenarioData;

        public ReportType ReportType { get; private set; }
        public DateTime ValuationDate { get; private set; }

        public int ValueColumnsCount { get; private set; }
        public IList<ResultRowModel> Rows { get; private set; }
        public IList<DateInterval> DateIntervals { get; private set; }

        private readonly int valuationMonth;

        public ResultModel(ScenarioData data, ReportType reportType, DateTime valuationDate)
        {
            this.scenarioData = data;
            this.ReportType = reportType;
            this.ValuationDate = valuationDate;
            this.valuationMonth = Tools.GetMonthNumber(valuationDate);

            this.ComputeDateIntervals();
        }

        private void ComputeDateIntervals()
        {
            this.DateIntervals = new List<DateInterval>();

            DateTime firstMonth = LastDayOfMonth(this.ValuationDate);
            DateTime lastMonth = LastDayOfMonth(firstMonth.AddMonths(Constants.RESULT_MONTHS - 1));

            DateTime date = firstMonth;

            this.DateIntervals.Add(new DateInterval(firstMonth, firstMonth));

            int increment = 0;
            do
            {
                switch (this.ReportType)
                {
                    case ReportType.Annually:
                        increment = 12;
                        break;
                    case ReportType.AnnuallyAsCalendarYear:
                        increment = 12 - (date.Month - 1) % 12;
                        break;
                    case ReportType.Quarterly:
                        increment = 3;
                        break;
                    case ReportType.QuarterlyAsCalendarYear:
                        increment = 3 - (date.Month - 1) % 3;
                        break;
                    case ReportType.Monthly:
                        increment = 1;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                if (LastDayOfMonth(date.AddMonths(increment)) >= lastMonth)
                {
                    increment = Tools.GetMonthNumber(lastMonth) - Tools.GetMonthNumber(date);
                }

                this.DateIntervals.Add(new DateInterval(date, LastDayOfMonth(date.AddMonths(increment - 1))));
                date = LastDayOfMonth(date.AddMonths(increment));
            } while (date < lastMonth && increment != 0);

            this.ValueColumnsCount = Math.Min(this.DateIntervals.Count, Constants.RESULT_MONTHS);
        }

        private static DateTime LastDayOfMonth(DateTime date)
        {
            date = date.AddMonths(1);
            date = date.AddDays(-date.Day);

            return date;
        }

        public IList<ResultRowModel> GetRows()
        {
            if (Rows != null) return Rows;

            List<ResultRowModel> rows = new List<ResultRowModel>();

            this.InitColumns(rows);

            this.CreatePerCategoryRows(rows, ResultSection.BookValue);

            this.CreatePerCategoryRowsInterest(rows, ResultSection.Income);

            this.CreateNetInterestIncome(rows);

            this.CreateGainLosesOnFinancialInstrument(rows);

            this.CreateOtherItemAssuptions(rows);

            this.CreateNetIncomeBeforeImpairmentCharge(rows);

            this.CreateImpairementData(rows);

            this.CreateImpairments(rows);

            this.CreateIncomePreTax(rows);

            this.CreateIncomeTaxBenefit(rows);

            this.CreateNetIncome(rows);

            this.CreateDividends(rows);

            this.CreateROE(rows);

            this.CreatePeriodicROA(rows);

            this.CreatePerCategoryRows(rows, ResultSection.Yield);

            Replace0ByNull(rows);

            this.Rows = rows;

            return rows;
        }

        private void InitColumns(List<ResultRowModel> gridData)
        {
            ResultRowModel periodHeaderSummary = new ResultRowModel("", ResultRowType.PeriodHeader,
                                                                    this.ValueColumnsCount);
            for (int i = 0; i < this.ValueColumnsCount; i++)
            {
                DateInterval interval = this.DateIntervals[i];

                string headerText = i == 0 ? Labels.ResultModel_InitColumns_Opening_Balance : interval.Start.ToString(HeaderDateFormat);
                if (interval.Start != interval.End)
                {
                    headerText += " - " + interval.End.ToString(HeaderDateFormat);
                }
                periodHeaderSummary[i] = headerText;
            }

            gridData.Add(periodHeaderSummary);
        }

        private void CreateImpairementData(List<ResultRowModel> gridData)
        {
            foreach (Category category in Category.FindAll())
            {
                if (category.IsVisibleCheckedModel == true)
                {
                    ResultRowModel rm = new ResultRowModel(category.Label, ResultRowType.Normal, this.ValueColumnsCount);

                    decimal[] values = this.scenarioData.GetImpairementData()[category];
                    for (int i = 0; i < this.ValueColumnsCount; i++)
                    {
                        rm[i] = this.DateIntervalSum(values, i);
                    }
                    gridData.Add(rm);
                }
            }
        }

        private static void Replace0ByNull(List<ResultRowModel> gridData)
        {
            for (int i = 1; i < gridData.Count; i++)
            {
                for (int j = 2; j < gridData[i].RowValues.Length; j++)
                {
                    if (gridData[i].RowValues[j] != null && ((decimal)gridData[i].RowValues[j] == 0m))
                    {
                        gridData[i].RowValues[j] = null;
                    }
                }
            }
        }

        private void CreatePeriodicROA(List<ResultRowModel> gridData)
        {
            ResultRowModel rm = new ResultRowModel(Labels.ResultModel_CreatePeriodicROA_Periodic_ROA, ResultRowType.Percentage, this.ValueColumnsCount);

            decimal[] netIncome = this.scenarioData.GetNetIncome();
            decimal[] totalAssets = this.scenarioData.GetTotalAssets();
            for (int i = 0; i < this.ValueColumnsCount; i++)
            {
                if (totalAssets[i] != 0)
                {
                    rm[i] = this.DateIntervalSum(netIncome, i) / totalAssets[i] * 12 / this.DateIntervals[i].Length;
                }
            }

            gridData.Add(rm);
        }

        private void CreateROE(List<ResultRowModel> gridData)
        {
            ResultRowModel rm = new ResultRowModel(Labels.ResultModel_CreateROE_ROE, ResultRowType.Percentage, this.ValueColumnsCount);

            decimal[] netIncome = this.scenarioData.GetNetIncome();
            decimal[] totalEquity = this.scenarioData.GetTotalEquity();
            for (int i = 0; i < this.ValueColumnsCount; i++)
            {
                if (totalEquity[i] != 0)
                {
                    rm[i] = this.DateIntervalSum(netIncome, i) / totalEquity[i];
                }
            }

            gridData.Add(rm);
        }

        private void CreateDividends(List<ResultRowModel> gridData)
        {
            ResultRowModel headerSummary = new ResultRowModel(Labels.ResultModel_CreateDividends_Dividends, ResultRowType.Normal, this.ValueColumnsCount);
            gridData.Add(headerSummary);

            Dividend[] dividends = ActiveRecordBase<Dividend>.FindAll();

            decimal[] netIncome = this.scenarioData.GetNetIncome();
            IDictionary<Dividend, decimal[]> dividendAmounts = this.scenarioData.GetDividendAmounts();

            foreach (Dividend dividend in dividends)
            {
                ResultRowModel rm = new ResultRowModel(dividend.Name, ResultRowType.Normal, this.ValueColumnsCount);

                for (int i = 0; i < this.ValueColumnsCount; i++)
                {
                    decimal value = dividendAmounts[dividend][i] * this.DateIntervalSum(netIncome, i);
                    if (dividend.IsValueAmount && value < dividend.Minimum.GetValueOrDefault())
                    {
                        value = dividend.Minimum.GetValueOrDefault();
                    }
                    rm[i] = value;
                }
                gridData.Add(rm);
            }
        }

        private decimal DateIntervalValue(decimal[] values, int step)
        {
            return step == 0 ? values[0] : values[this.DateIntervals[step].MonthEnd - this.valuationMonth + 1];
        }

        private decimal DateIntervalSum(decimal[] values, int step)
        {
            if (step == 0) return 0;

            decimal sum = 0;
            DateInterval interval = this.DateIntervals[step];
            for (int i = interval.MonthStart; i <= interval.MonthEnd; i++)
            {
                sum += values[i - this.valuationMonth + 1];
            }

            return sum;
        }

        private void CreateNetIncome(List<ResultRowModel> data)
        {
            ResultRowModel rm = new ResultRowModel(Labels.ResultModel_CreateNetIncome_Net_Income, ResultRowType.Header, this.ValueColumnsCount);

            decimal[] netIncome = this.scenarioData.GetNetIncome();
            for (int i = 0; i < this.ValueColumnsCount; i++)
            {
                rm[i] = this.DateIntervalSum(netIncome, i);
            }

            data.Add(rm);
        }

        private void CreateIncomeTaxBenefit(List<ResultRowModel> data)
        {
            ResultRowModel rm = new ResultRowModel(Labels.ResultModel_CreateIncomeTaxBenefit_Income_tax_benefit____expense_, ResultRowType.Normal,
                                                   this.ValueColumnsCount);

            decimal[] taxes = this.scenarioData.GetTaxes();
            for (int i = 0; i < this.ValueColumnsCount; i++)
            {
                rm[i] = this.DateIntervalSum(taxes, i);
            }

            data.Add(rm);
        }

        private void CreateIncomePreTax(List<ResultRowModel> data)
        {
            ResultRowModel rm = new ResultRowModel(Labels.ResultModel_CreateIncomePreTax_Income_Pre_Tax, ResultRowType.Header, this.ValueColumnsCount);

            decimal[] incomePreTax = this.scenarioData.GetIncomePreTax();
            for (int i = 0; i < this.ValueColumnsCount; i++)
            {
                rm[i] = this.DateIntervalSum(incomePreTax, i);
            }

            data.Add(rm);
        }

        private void CreateNetIncomeBeforeImpairmentCharge(List<ResultRowModel> data)
        {
            ResultRowModel rm = new ResultRowModel(Labels.ResultModel_CreateNetIncomeBeforeImpairmentCharge_Income_before_impairment_charge, ResultRowType.Header,
                                                   this.ValueColumnsCount);

            decimal[] interestIncomeBeforeImpairment = this.scenarioData.GetInterestIncomeBeforeImpairment();
            for (int i = 0; i < this.ValueColumnsCount; i++)
            {
                rm[i] = this.DateIntervalSum(interestIncomeBeforeImpairment, i);
            }

            data.Add(rm);
        }

        private void CreateImpairments(List<ResultRowModel> data)
        {
            ResultRowModel rm = new ResultRowModel(Labels.ResultModel_CreateImpairments_Impairment_Charge, ResultRowType.Footer, this.ValueColumnsCount);

            decimal[] impairment = this.scenarioData.GetImpairment();
            for (int i = 0; i < this.ValueColumnsCount; i++)
            {
                rm[i] = this.DateIntervalSum(impairment, i);
            }

            data.Add(rm);
        }

        private void CreateNetInterestIncome(List<ResultRowModel> data)
        {
            ResultRowModel rm = new ResultRowModel(Labels.ResultModel_CreateNetInterestIncome_Net_Interest_Income, ResultRowType.Header, this.ValueColumnsCount);

            decimal[] netInterestIncome = this.scenarioData.GetNetInterestIncome();
            for (int i = 1; i < this.ValueColumnsCount; i++)
            {
                rm[i] = this.DateIntervalSum(netInterestIncome, i);
            }

            data.Add(rm);
        }

        private void CreateGainLosesOnFinancialInstrument(List<ResultRowModel> data)
        {
            ResultRowModel rm = new ResultRowModel(Labels.ResultModel_CreateGainLosesOnFinancialInstrument_Gain_Losses_on_Financial_Instrument, ResultRowType.Header,
                                                   this.ValueColumnsCount);

            decimal[] gainLosesOnFinancialInstrument = this.scenarioData.GetGainLosesOnFinancialInstrument();
            for (int i = 1; i < this.ValueColumnsCount; i++)
            {
                rm[i] = this.DateIntervalSum(gainLosesOnFinancialInstrument, i);
            }

            data.Add(rm);
        }

        private void CreatePerCategoryRows(List<ResultRowModel> gridData, ResultSection section)
        {
            Category root = Category.FindRoot();

            decimal[] totalLiabilitiesAndEquities = new decimal[this.ValueColumnsCount];

            foreach (Category rootSubCategory in root.Children)
            {
                string type = GetSectionLabel(section, rootSubCategory);

                ResultRowModel headerSummary = new ResultRowModel(
                    string.Format("{0}", type + " - " + rootSubCategory.Label), ResultRowType.Header,
                    this.ValueColumnsCount);
                headerSummary.ResultSection = section;

                if (rootSubCategory.isVisible == true)
                {
                    gridData.Add(headerSummary);

                    this.FillGrid(section, rootSubCategory, gridData, 0);

                    ResultRowModel footerSummary =
                        new ResultRowModel(string.Format(type + " - Total {0}", rootSubCategory.Label),
                                           ResultRowType.Footer,
                                           this.ValueColumnsCount);
                    footerSummary.ResultSection = section;

                    decimal[] values = this.GetCategoryValuesBySection(rootSubCategory, section);
                    for (int i = 0; i < this.ValueColumnsCount; i++)
                    {
                        footerSummary[i] = values[i];
                        if (rootSubCategory.BalanceType == BalanceType.Equity ||
                            rootSubCategory.BalanceType == BalanceType.Liability)
                        {
                            totalLiabilitiesAndEquities[i] += values[i];
                        }
                    }

                    gridData.Add(footerSummary);
                }
            }

            ResultRowModel totalSummary =
                new ResultRowModel(GetSectionLabel(section, null) + Labels.ResultModel_CreatePerCategoryRows____Total_Liabilities___Equities,
                                   ResultRowType.MasterFooter, this.ValueColumnsCount);
            totalSummary.ResultSection = section;

            for (int i = 0; i < this.ValueColumnsCount; i++)
            {
                totalSummary[i] = totalLiabilitiesAndEquities[i];
            }
            gridData.Add(totalSummary);
        }

        private void CreatePerCategoryRowsInterest(List<ResultRowModel> gridData, ResultSection section)
        {
            Category root = Category.FindRoot();

            decimal[] totalLiabilitiesAndEquities = new decimal[this.ValueColumnsCount];

            foreach (Category rootSubCategory in root.Children)
            {
                if (rootSubCategory.isVisible == true)
                {
                    if (rootSubCategory.BalanceType != BalanceType.Equity)
                        this.FillGrid(section, rootSubCategory, gridData, 0, true);

                    ResultRowModel footerSummary;

                    if (rootSubCategory.BalanceType == BalanceType.Asset)
                    {
                        footerSummary = new ResultRowModel(Labels.ResultModel_CreatePerCategoryRowsInterest_Total_Interest_Income, ResultRowType.Footer,
                                                           this.ValueColumnsCount);
                        footerSummary.ResultSection = section;

                        decimal[] values = this.GetCategoryValuesBySection(rootSubCategory, section);
                        for (int i = 0; i < this.ValueColumnsCount; i++)
                            footerSummary[i] = values[i];
                        gridData.Add(footerSummary);
                    }
                    else if (rootSubCategory.BalanceType == BalanceType.Equity ||
                             rootSubCategory.BalanceType == BalanceType.Liability)
                    {
                        decimal[] values = this.GetCategoryValuesBySection(rootSubCategory, section);
                        for (int i = 0; i < this.ValueColumnsCount; i++)
                        {
                            totalLiabilitiesAndEquities[i] += values[i];
                        }

                        if (rootSubCategory.BalanceType == BalanceType.Equity &&
                            rootSubCategory.IsVisibleChecked == true)
                        {
                            footerSummary = new ResultRowModel(rootSubCategory.Label, ResultRowType.Normal,
                                                               this.ValueColumnsCount);
                            footerSummary.ResultSection = section;

                            for (int i = 0; i < this.ValueColumnsCount; i++)
                            {
                                footerSummary[i] = values[i];
                            }
                            gridData.Add(footerSummary);
                        }
                    }
                }
            }

            ResultRowModel totalSummary = new ResultRowModel(Labels.ResultModel_CreatePerCategoryRowsInterest_Total_Interest_Expense, ResultRowType.MasterFooter,
                                                             this.ValueColumnsCount);
            totalSummary.ResultSection = section;

            for (int i = 0; i < this.ValueColumnsCount; i++)
            {
                totalSummary[i] = totalLiabilitiesAndEquities[i];
            }
            gridData.Add(totalSummary);
        }

        private static string GetSectionLabel(ResultSection section, Category category)
        {
            string type;
            switch (section)
            {
                case ResultSection.BookValue:
                    type = Labels.ResultModel_GetSectionLabel_Book_Values;
                    break;
                case ResultSection.Income:
                    if (category != null && category.BalanceType == BalanceType.Asset)
                    {
                        type = Labels.ResultModel_GetSectionLabel_Interest_Income;
                    }
                    else
                    {
                        type = Labels.ResultModel_GetSectionLabel_Interest_Expenses;
                    }
                    break;
                case ResultSection.Yield:
                    type = Labels.ResultModel_GetSectionLabel_Yield;
                    break;
                default:
                    throw new ArgumentOutOfRangeException("section");
            }
            return type;
        }

        private void FillGrid(ResultSection section, Category cat, List<ResultRowModel> gridData, int level,
                              bool interest)
        {
            foreach (Category subCat in cat.Children)
            {
                if (!subCat.isVisible.GetValueOrDefault()) continue;
                if (interest) if (!subCat.IsVisibleChecked.GetValueOrDefault()) continue;

                ResultRowModel rm = new ResultRowModel(subCat.Label, ResultRowType.Normal, this.ValueColumnsCount);
                rm.ResultSection = section;

                decimal[] values = this.GetCategoryValuesBySection(subCat, section);

                rm.Description = new string(' ', level * 10) + rm.Description;
                for (int i = 0; i < this.ValueColumnsCount; i++)
                {
                    rm[i] = values[i];
                }

                gridData.Add(rm);

                level++;
                this.FillGrid(section, subCat, gridData, level);
                level--;
            }
        }

        private void FillGrid(ResultSection section, Category cat, List<ResultRowModel> gridData, int level)
        {
            this.FillGrid(section, cat, gridData, level, false);
        }

        private decimal[] GetCategoryValuesBySection(Category category, ResultSection section)
        {
            decimal[] allValues;
            decimal[] values = new decimal[this.ValueColumnsCount];

            switch (section)
            {
                case ResultSection.BookValue:
                    allValues = this.scenarioData.GetBookValue(category);
                    values[0] = allValues[0];
                    for (int i = 1; i < this.ValueColumnsCount; i++)
                    {
                        try
                        {
                            values[i] = this.DateIntervalValue(allValues, i);
                        }
                        catch (OverflowException)
                        {
                        }
                    }
                    break;
                case ResultSection.Income:
                    allValues = this.scenarioData.GetIncome(category);
                    for (int i = 1; i < this.ValueColumnsCount; i++)
                    {
                        try
                        {
                            values[i] = this.DateIntervalSum(allValues, i);
                        }
                        catch (OverflowException)
                        {
                        }
                    }
                    break;
                case ResultSection.Yield:
                    decimal[] income = this.scenarioData.GetIncome(category);
                    decimal[] allBookValues = this.scenarioData.GetBookValue(category);
                    for (int i = 1; i < this.ValueColumnsCount; i++)
                    {
                        try
                        {
                            decimal bookValue = this.DateIntervalValue(allBookValues, i);
                            decimal incomeSum = this.DateIntervalSum(income, i);
                            if (bookValue != 0) values[i] = incomeSum / bookValue * 100;
                        }
                        catch (OverflowException)
                        {
                            //MTU : do nothing, as in bookValue==0
                        }
                    }
                    break;
                default:
                    throw new ArgumentOutOfRangeException("section");
            }

            return values;
        }

        private void CreateOtherItemAssuptions(List<ResultRowModel> gridData)
        {
            string[] subtotalgroups = OtherItemAssum.GetAllSubgroups();

            bool swapped;
            do
            {
                swapped = false;
                for (int i = 0; i < subtotalgroups.Length - 1; i++)
                {
                    if ((subtotalgroups[i] == null ? 0 : OtherItemAssum.findBySubtotalGroup(subtotalgroups[i]).Length) <
                        (subtotalgroups[i + 1] == null
                             ? 0
                             : OtherItemAssum.findBySubtotalGroup(subtotalgroups[i + 1]).Length))
                    {
                        string aux;
                        aux = subtotalgroups[i];
                        subtotalgroups[i] = subtotalgroups[i + 1];
                        subtotalgroups[i + 1] = aux;
                        swapped = true;
                    }
                }
            } while (swapped);


            foreach (string subTotalGroup in subtotalgroups)
            {
                OtherItemAssum[] oias = OtherItemAssum.findBySubtotalGroup(subTotalGroup);
                decimal[] totalVals = new decimal[this.ValueColumnsCount];

                IDictionary<string, decimal[]> allOIA = this.scenarioData.GetOtherItemAssumptions();
                foreach (OtherItemAssum oia in oias)
                {
                    string name = oia.Descrip;

                    if (!allOIA.ContainsKey(name)) continue;

                    decimal[] values = allOIA[name];
                    ResultRowModel rm = new ResultRowModel(name, ResultRowType.Normal, this.ValueColumnsCount);
                    for (int j = 0; j < this.ValueColumnsCount; j++)
                    {
                        rm[j] = this.DateIntervalSum(values, j);
                        totalVals[j] = totalVals[j] + this.DateIntervalSum(values, j);
                    }
                    if (oia.IsVisibleChecked == true)
                        gridData.Add(rm);
                }

                if (oias.Length > 1 && subTotalGroup != null)
                {
                    ResultRowModel rmt = new ResultRowModel(subTotalGroup, ResultRowType.Footer, this.ValueColumnsCount);
                    for (int j = 0; j < this.ValueColumnsCount; j++)
                    {
                        rmt[j] = totalVals[j];
                    }
                    gridData.Add(rmt);
                }
            }
        }
    }

    public enum ResultSection
    {
        BookValue,
        Income,
        Yield
    }
}