using System.Reflection;
using System.Windows.Forms;
using AssetModelingModule.Resources.Language;

namespace AssetModelingModule.Views
{
    public partial class About : Form
    {
        public About()
        {
            InitializeComponent();

            this.Text = Labels.About_About_About_ + Constants.APPLICATION_NAME;

            appNameLabel.Text = Constants.APPLICATION_NAME;
            appVersionLabel.Text = Labels.About_About_Version_ + Assembly.GetExecutingAssembly().GetName().Version;
        }
    }
}