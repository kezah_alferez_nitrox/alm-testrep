using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using AssetModelingModule.Domain;

namespace AssetModelingModule.Views.BalanceSheet.Parameters
{
    public partial class AddScenarios : Form
    {
        private BindingList<Scenario> availableList;
        private BindingList<Scenario> selectedList;

        public AddScenarios()
        {
            InitializeComponent();
        }

        private void RefreshAvailable()
        {
            Scenario[] scenarios = Scenario.FindAvailable();
            availableList = new BindingList<Scenario>(new List<Scenario>(scenarios));
            availableListBox.DataSource = availableList;
        }

        private void RefreshSelected()
        {
            Scenario[] scenarios = Scenario.FindSelected();
            selectedList = new BindingList<Scenario>(new List<Scenario>(scenarios));
            selectedListBox.DataSource = selectedList;
        }

        private void AddScenarios_Load(object sender, System.EventArgs e)
        {
            availableListBox.DisplayMember = "Name";
            availableListBox.ValueMember = "Self";
            RefreshAvailable();
            availableListBox.SelectedItem = null;

            selectedListBox.DisplayMember = "Name";
            selectedListBox.ValueMember = "Self";
            RefreshSelected();
            selectedListBox.SelectedItem = null;

        }

        private void addNewButton_Click(object sender, System.EventArgs e)
        {
            CreateScenario dialog = new CreateScenario();
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                CreateAvailableScenarios(dialog.ScenarioName);
                selectedListBox.SelectedItem = null;
                Scenario _sc = Scenario.FindByName(dialog.ScenarioName);
                availableListBox.SelectedItem = _sc;
                Views.BalanceSheet.BalanceSheet.modifiedScenarioTemplate = true;
            }
        }

        private void CreateAvailableScenarios(string scenarioName)
        {
            Scenario newScenario = availableList.AddNew();
            newScenario.Name = scenarioName;
            newScenario.Selected = false;
            newScenario.Create();

            availableList.ResetBindings();
        }

        private void addButton_Click(object sender, System.EventArgs e)
        {
            Scenario scenario = availableListBox.SelectedValue as Scenario;
            if(scenario == null) return;

            availableList.Remove(scenario);
            availableListBox.SelectedItem = null;
            selectedList.Add(scenario);
            selectedListBox.SelectedItem = scenario;

            RefreshDescription(scenario);

            Views.BalanceSheet.BalanceSheet.modifiedScenarioTemplate = true;
        }

        private void removeButton_Click(object sender, System.EventArgs e)
        {
            Scenario scenario = selectedListBox.SelectedValue as Scenario;
            if (scenario == null) return;

            selectedList.Remove(scenario);
            selectedListBox.SelectedItem = null;
            availableList.Add(scenario);
            availableListBox.SelectedItem = scenario;

            RefreshDescription(scenario);
            Views.BalanceSheet.BalanceSheet.modifiedScenarioTemplate = true;
        }

        private void okButton_Click(object sender, System.EventArgs e)
        {
            foreach (Scenario _scenario in selectedList)
            {
                _scenario.Selected = true;
            }
            foreach (Scenario _scenario in availableList)
            {
                _scenario.Selected = false;
            }

            PersistenceSession.Flush();
        }

        private void availableListBox_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            RefreshDescription(availableListBox.SelectedValue as Scenario);
        }

        private void RefreshDescription(Scenario scenario)
        {
            //Scenario scenario = availableListBox.SelectedValue as Scenario;
            if (scenario == null)
            {
                descriptionTextBox.Text = "";
                return;
            }

            descriptionTextBox.Text = scenario.Description;
        }

        private void descriptionTextBox_TextChanged(object sender, System.EventArgs e)
        {
            Scenario scenario = availableListBox.SelectedValue as Scenario;
            if (scenario == null) scenario = selectedListBox.SelectedValue as Scenario;
            if (scenario == null) return;

            scenario.Description = descriptionTextBox.Text;
        }

        private void availableListBox_Enter(object sender, System.EventArgs e)
        {
            selectedListBox.SelectedItem = null;
        }

        private void selectedListBox_Enter(object sender, System.EventArgs e)
        {
            availableListBox.SelectedItem = null;
        }

        private void selectedListBox_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            RefreshDescription(selectedListBox.SelectedItem as Scenario);
        }

    }
}
