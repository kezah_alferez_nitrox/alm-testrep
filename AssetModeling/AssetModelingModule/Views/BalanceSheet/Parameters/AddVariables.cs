using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using AssetModelingModule.Domain;

namespace AssetModelingModule.Views.BalanceSheet.Parameters
{
    public partial class AddVariables : Form
    {
        private BindingList<Variable> availableList;
        private BindingList<Variable> selectedList;

        public AddVariables()
        {
            InitializeComponent();
        }

        private void AddVariables_Load(object sender, System.EventArgs e)
        {
            availableListBox.DisplayMember = "Name";
            availableListBox.ValueMember = "Self";
            availableListBox.DataSource = availableList = new BindingList<Variable>(new List<Variable>(Variable.FindAvailable()));

            selectedListBox.DisplayMember = "Name";
            selectedListBox.ValueMember = "Self";
            selectedListBox.DataSource = selectedList = new BindingList<Variable>(new List<Variable>(Variable.FindSelected()));
        }

        private void addButton_Click(object sender, System.EventArgs e)
        {
            Variable variable = availableListBox.SelectedValue as Variable;
            if (variable == null) return;

            availableList.Remove(variable);
            selectedList.Add(variable);
            Views.BalanceSheet.BalanceSheet.modifiedScenarioTemplate = true;
        }

        private void removeButton_Click(object sender, System.EventArgs e)
        {
            Variable variable = selectedListBox.SelectedValue as Variable;
            if (variable == null) return;

            selectedList.Remove(variable);
            availableList.Add(variable);
            Views.BalanceSheet.BalanceSheet.modifiedScenarioTemplate = true;
        }

        private void okButton_Click(object sender, System.EventArgs e)
        {
            foreach (Variable _variable in selectedList)
            {
                _variable.Selected = true;
            }
            foreach (Variable _variable in availableList)
            {
                _variable.Selected = false;
            }

            PersistenceSession.Flush();
        }
    }
}