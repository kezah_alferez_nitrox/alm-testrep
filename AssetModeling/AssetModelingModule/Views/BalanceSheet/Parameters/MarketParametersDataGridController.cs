using System;
using System.Windows.Forms;
using ALMSCommon;
using AssetModelingModule.Core;
using AssetModelingModule.Domain;
using AssetModelingModule.Resources.Language;

namespace AssetModelingModule.Views.BalanceSheet.Parameters
{
    public class MarketParametersDataGridController
    {


        private readonly DataGridView grid;

        private DataGridViewTextBoxColumn curvDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn dayDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn onemDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn threemDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn sixDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn oneyDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn twoyDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn fiveyDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn tenyDataGridViewTextBoxColumn;

        private DataGridViewColumn[] columns;

        public MarketParametersDataGridController(DataGridView grid)
        {
            this.grid = grid;
            grid.AutoGenerateColumns = false;
            grid.AllowUserToAddRows = false;

            grid.RowTemplate.Height = Constants.GRID_ROW_HEIGHT;
            InitColumns();
        }

        private void InitColumns()
        {
            curvDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            dayDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            onemDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            threemDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            sixDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            oneyDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            twoyDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            fiveyDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            tenyDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();

            //
            //curvDataGridViewTextBoxColumn
            //

            curvDataGridViewTextBoxColumn.DataPropertyName = "Curve";
            curvDataGridViewTextBoxColumn.HeaderText = Labels.MarketParametersDataGridController_InitColumns_Vector;
            curvDataGridViewTextBoxColumn.Name = "Name";
            curvDataGridViewTextBoxColumn.Width = 100;

            //
            //dayDataGridViewTextBoxColumn
            //
            dayDataGridViewTextBoxColumn.HeaderText = Labels.MarketParametersDataGridController_InitColumns_Day;
            dayDataGridViewTextBoxColumn.Name = "Day";
            dayDataGridViewTextBoxColumn.DataPropertyName = "Day";
            dayDataGridViewTextBoxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            //
            //onemDataGridViewTextBoxColumn
            //
            onemDataGridViewTextBoxColumn.HeaderText = Labels.MarketParametersDataGridController_InitColumns__1M;
            onemDataGridViewTextBoxColumn.Name = "1M";
            onemDataGridViewTextBoxColumn.DataPropertyName = "Onem";
            onemDataGridViewTextBoxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            //
            //threemDataGridViewTextBoxColumn
            //
            threemDataGridViewTextBoxColumn.HeaderText = Labels.MarketParametersDataGridController_InitColumns__3M;
            threemDataGridViewTextBoxColumn.Name = "3M";
            threemDataGridViewTextBoxColumn.DataPropertyName = "Threem";
            threemDataGridViewTextBoxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            //
            //sixDataGridViewTextBoxColumn
            //
            sixDataGridViewTextBoxColumn.HeaderText = Labels.MarketParametersDataGridController_InitColumns__6M;
            sixDataGridViewTextBoxColumn.Name = "6M";
            sixDataGridViewTextBoxColumn.DataPropertyName = "Sixm";
            sixDataGridViewTextBoxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            //
            //oneyDataGridViewTextBoxColumn
            //
            oneyDataGridViewTextBoxColumn.HeaderText = Labels.MarketParametersDataGridController_InitColumns__1Y;
            oneyDataGridViewTextBoxColumn.Name = "1Y";
            oneyDataGridViewTextBoxColumn.DataPropertyName = "Oney";
            oneyDataGridViewTextBoxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            //
            //twoyDataGridViewTextBoxColumn
            //
            twoyDataGridViewTextBoxColumn.HeaderText = Labels.MarketParametersDataGridController_InitColumns__2Y;
            twoyDataGridViewTextBoxColumn.Name = "2Y";
            twoyDataGridViewTextBoxColumn.DataPropertyName = "Twoy";
            twoyDataGridViewTextBoxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            //
            //fiveyDataGridViewTextBoxColumn
            //
            fiveyDataGridViewTextBoxColumn.HeaderText = Labels.MarketParametersDataGridController_InitColumns__5Y;
            fiveyDataGridViewTextBoxColumn.Name = "5Y";
            fiveyDataGridViewTextBoxColumn.DataPropertyName = "Fivey";
            fiveyDataGridViewTextBoxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            //
            //tenyDataGridViewTextBoxColumn
            //
            tenyDataGridViewTextBoxColumn.HeaderText = Labels.MarketParametersDataGridController_InitColumns__10Y;
            tenyDataGridViewTextBoxColumn.Name = "10Y";
            tenyDataGridViewTextBoxColumn.DataPropertyName = "Teny";
            tenyDataGridViewTextBoxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            columns = new DataGridViewColumn[]
            {
                curvDataGridViewTextBoxColumn,
                dayDataGridViewTextBoxColumn,
                onemDataGridViewTextBoxColumn,
                threemDataGridViewTextBoxColumn,
                sixDataGridViewTextBoxColumn,
                oneyDataGridViewTextBoxColumn,
                twoyDataGridViewTextBoxColumn,
                fiveyDataGridViewTextBoxColumn,
                tenyDataGridViewTextBoxColumn,
            };
            grid.Columns.Clear();
            grid.Columns.AddRange(columns);
        }

        public void Refresh(bool ResetColumns, DateTime ValuationDate)
        {

            if (VectorList != null)
                VectorList.Clear();

            SortableBindingList<MarketParameters> gridData = new SortableBindingList<MarketParameters>();

            foreach (Vector vect in Vector.FindByImportantMarketData())
            {
                MarketParameters mp = new MarketParameters();
                mp.Curve = vect.Name;

                foreach (VectorPoint vp in vect.VectorPoints)
                {
                    if (vp.Date >= ValuationDate && mp.Day == null)
                    {
                        mp.Day = ToPercent(vp.Value);
                    }

                    int monthsApart = 12 * (ValuationDate.Year - vp.Date.Year) + ValuationDate.Month - vp.Date.Month;
                    monthsApart = Math.Abs(monthsApart);

                    if (monthsApart >= 1 && mp.Onem == null)
                    {
                        mp.Onem = ToPercent(vp.Value);
                    }

                    if (monthsApart >= 3 && mp.Threem == null)
                    {
                        mp.Threem = ToPercent(vp.Value);
                    }

                    if (monthsApart >= 6 && mp.Sixm == null)
                    {
                        mp.Sixm = ToPercent(vp.Value);
                    }

                    if (monthsApart >= 12 && mp.Oney == null)
                    {
                        mp.Oney = ToPercent(vp.Value);
                    }
                    if (monthsApart >= 24 && mp.Twoy == null)
                    {
                        mp.Twoy = ToPercent(vp.Value);
                    }
                    if (monthsApart >= 60 && mp.Fivey == null)
                    {
                        mp.Fivey = ToPercent(vp.Value);
                    }
                    if (monthsApart >= 120 && mp.Teny == null)
                    {
                        mp.Teny = ToPercent(vp.Value);
                    }
                }
                gridData.Add(mp);
            }

            if (ResetColumns)
            {
                InitColumns();
            }
            VectorList = gridData;
        }

        private static string ToPercent(decimal value)
        {
            return string.Format("{0:P}", value);
        }

        private SortableBindingList<MarketParameters> VectorList
        {
            get { return grid.DataSource as SortableBindingList<MarketParameters>; }
            set { grid.DataSource = value; }
        }

    }
}
