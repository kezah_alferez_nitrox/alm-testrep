using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using AssetModelingModule.Domain;
using AssetModelingModule.Resources.Language;
using Eu.AlterSystems.ASNetLib.Core;

namespace AssetModelingModule.Views.BalanceSheet.Parameters
{
    public class FundamentalVectorsGridControler
    {
        private readonly DataGridView grid;
        private DataGridViewTextBoxColumn nameColumn;
        private DataGridViewTextBoxColumn deltaColumn;
        private DataGridViewComboBoxColumn unitColumn;
        private DataGridViewComboBoxColumn variableColumn;
        private BindingList<FundamentalVector> gridData;

        public FundamentalVectorsGridControler(DataGridView grid)
        {
            this.grid = grid;
            this.grid.EditMode = DataGridViewEditMode.EditOnEnter;
            grid.AutoGenerateColumns = false;

            this.InitColumns();
        }

        private void InitColumns()
        {
            this.nameColumn = new DataGridViewTextBoxColumn();
            this.deltaColumn = new DataGridViewTextBoxColumn();
            this.unitColumn = new DataGridViewComboBoxColumn();
            this.variableColumn = new DataGridViewComboBoxColumn();

            // 
            // nameColumn
            // 
            this.nameColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
            this.nameColumn.HeaderText = Labels.FundamentalVectorsGridControler_InitColumns_Name;
            this.nameColumn.Name = "nameColumn";
            this.nameColumn.ReadOnly = true;
            this.nameColumn.Width = 100;
            this.nameColumn.DataPropertyName = "Name";
            // 
            // deltaColumn
            // 
            this.deltaColumn.HeaderText = Labels.FundamentalVectorsGridControler_InitColumns_Date_Offset;
            this.deltaColumn.Name = "deltaColumn";
            // todo this.deltaColumn.DataPropertyName = "Delta";
            this.deltaColumn.DefaultCellStyle.Format = "N0";
            // 
            // unitColumn
            // 
            this.unitColumn.HeaderText = Labels.FundamentalVectorsGridControler_InitColumns_Unit;
            this.unitColumn.Name = "unitColumn";
            // todo this.unitColumn.DataPropertyName = "Unit";
            this.unitColumn.DisplayMember = "Value";
            this.unitColumn.ValueMember = "Key";
            this.unitColumn.DataSource = EnumHelper.ToList(typeof (TimeUnit));
            // 
            // variableColumn
            // 
            this.variableColumn.HeaderText = Labels.FundamentalVectorsGridControler_InitColumns_Variable;
            this.variableColumn.Name = "variableColumn";
            // todo this.variableColumn.DataPropertyName = "Variable";

            this.grid.Columns.AddRange(new DataGridViewColumn[]
                {
                    this.nameColumn,
                    this.deltaColumn,
                    this.unitColumn,
                    this.variableColumn
                });
        }

        public void Refresh()
        {
            FundamentalVector[] vectors = FundamentalVector.FindAll();
            this.gridData = new BindingList<FundamentalVector>(new List<FundamentalVector>(vectors));
            this.grid.DataSource = this.gridData;
        }
    }
}