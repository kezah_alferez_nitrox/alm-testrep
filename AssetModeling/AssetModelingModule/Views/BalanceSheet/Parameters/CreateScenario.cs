using System.Windows.Forms;
using AssetModelingModule.Domain;

namespace AssetModelingModule.Views.BalanceSheet.Parameters
{
    public partial class CreateScenario : Form
    {
        private bool valid;

        public CreateScenario()
        {
            InitializeComponent();
        }

        public string ScenarioName
        {
            get
            {
                return scenarioNameTextBox.Text;
            }
        }

        private void okButton_Click(object sender, System.EventArgs e)
        {
            if (Scenario.ExistsByName(scenarioNameTextBox.Text))
            {
                MessageBox.Show(string.Format(Properties.Resources.ScenarioAlreadyExists, scenarioNameTextBox.Text),
                    "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                valid = false;
                return;
            }

            valid = true;
        }

        private void CreateScenario_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = !valid;
        }

        private void cancelButton_Click(object sender, System.EventArgs e)
        {
            valid = true;
        }

        private void scenarioNameTextBox_TextChanged(object sender, System.EventArgs e)
        {
            okButton.Enabled = !string.IsNullOrEmpty(scenarioNameTextBox.Text);
        }
    }
}