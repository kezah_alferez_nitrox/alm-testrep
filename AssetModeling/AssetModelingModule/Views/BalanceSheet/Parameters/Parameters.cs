using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using ALMSCommon.Forms;
using AssetModelingModule.BusinessLogic;
using AssetModelingModule.Controls;
using AssetModelingModule.Core.Import;
using AssetModelingModule.Domain;
using AssetModelingModule.Resources.Language;

namespace AssetModelingModule.Views.BalanceSheet.Parameters
{
    public partial class Parameters : UserControl
    {
        public delegate void ValuationCurrencyChangedEventHandler(object sender, EventArgs e);

        public event ValuationCurrencyChangedEventHandler ValuationCurrencyChanged = delegate { };

        private readonly List<DataGridViewColumn> columns = new List<DataGridViewColumn>();

        private readonly MarketParametersDataGridController _marketParametersDataGridView;

        private Variable[] variables;
        private Scenario[] scenarios;
        private FundamentalVectorsGridControler fundamentalVectorsGridControler;

        public Parameters()
        {
            InitializeComponent();

            valuationCurrencyComboBox.DisplayMember = "Symbol";
            valuationCurrencyComboBox.ValueMember = "Self";

            scenariosSetupDataGridView.EditMode = DataGridViewEditMode.EditOnEnter;

            this.scenariosSetupDataGridView.CellValueChanged += this.scenariosSetupDataGridView_CellValueChanged;

            if (!DesignMode && !Tools.InDesignMode)
            {
                _marketParametersDataGridView = new MarketParametersDataGridController(marketParametersDataGridView);

                this.fundamentalVectorsGridControler = new FundamentalVectorsGridControler(this.fundamentalVectorsGrid);
            }
        }

        private void Parameters_Load(object sender, EventArgs e)
        {
            if (DesignMode || Tools.InDesignMode) return;

            fxRatesComboBox.SelectedItem = 0;
            equityIndexComboBox.SelectedIndex = 0;

            RefreshScenarioSetup();

            _marketParametersDataGridView.Refresh(false, valuationDateTimePicker.Value.Date);
            fundamentalVectorsGridControler.Refresh();

            LoadFundamentalVectors();

            valuationDateTimePicker.ValueChanged += this.valuationDateTimePicker_ValueChanged;
        }

        private void LoadFundamentalVectors()
        {
            
        }

        public int[] GetScenarioSetupColumnsOrder()
        {
            int[] result = new int[scenariosSetupDataGridView.Columns.Count];
            for (int i = 0; i < scenariosSetupDataGridView.Columns.Count; i++)
            {
                int index = this.scenariosSetupDataGridView.Columns[i].DisplayIndex;
                result[i] = index;
            }
            return result;
        }

        public void SetSetupColumnsOrder(int[] order)
        {
            if(order == null) return;

            for (int i = 0; i < order.Length; i++)
            {
                if(i>=this.scenariosSetupDataGridView.Columns.Count) break;
                this.scenariosSetupDataGridView.Columns[i].DisplayIndex = order[i];
            }
        }

        private void LoadCurrenciesComboBox()
        {
            valuationCurrencyComboBox.SelectedIndexChanged -= this.valuationCurrencyComboBox_SelectedIndexChanged;

            valuationCurrencyComboBox.DataSource = Currency.FindAllNotEmpty();

            if (Param.ValuationCurrency != null)
            {
                foreach (object item in valuationCurrencyComboBox.Items)
                {
                    Currency currency = item as Currency;
                    if (currency == null) continue;
                    if (currency.ShortName == Param.ValuationCurrency.ShortName)
                    {
                        valuationCurrencyComboBox.SelectedItem = currency;
                    }
                }
            }

            valuationCurrencyComboBox.SelectedIndexChanged += this.valuationCurrencyComboBox_SelectedIndexChanged;
        }

        public void InitFxRates()
        {
            Currency valuationCurrency = Param.ValuationCurrency;
            if (valuationCurrency == null) return;

            Currency[] allCurrencies = Currency.FindAllNotEmpty();

            fxRatesComboBox.Items.Clear();

            CurrencyBL.ResetValuationDateFxRates();
            foreach (Currency currency in allCurrencies)
            {
                if (valuationCurrency != currency)
                {
                    decimal fxRate = CurrencyBL.FxRate(currency, valuationCurrency);
                    fxRatesComboBox.Items.Add(string.Format("{0}/{1} {2}", currency.Symbol, valuationCurrency.Symbol,
                                                            fxRate));
                }
            }
        }

        private void RefreshScenarioSetup()
        {
            columns.Clear();
            AutoCompleteColumn[] variableColumns = new AutoCompleteColumn[(Variable.FindSelected()).Length];
            scenariosSetupDataGridView.Columns.Clear();
            scenariosSetupDataGridView.Rows.Clear();

            // set columns
            DataGridViewTextBoxColumn scenarioColumn = new DataGridViewTextBoxColumn();
            scenarioColumn.HeaderText = Labels.Parameters_RefreshScenarioSetup_Scenario;
            scenarioColumn.ReadOnly = true;
            scenarioColumn.DefaultCellStyle.BackColor = Color.LightGray;
            scenarioColumn.Frozen = true;
            columns.Add(scenarioColumn);

            string[] vectorNames = DomainTools.GetAllVectorNames();

            variables = Variable.FindSelected();
            int _i = 0;
            foreach (Variable variable in variables)
            {
                variableColumns[_i] = new AutoCompleteColumn();
                variableColumns[_i].HeaderText = variable.Name;
                variableColumns[_i].Name = variable.Name;
                variableColumns[_i].ValueList = vectorNames;
                _i++;
            }
            columns.AddRange(variableColumns);
            scenariosSetupDataGridView.Columns.AddRange(columns.ToArray());

            FillScenarioSetup();

            if (Param.ValuationDate != null)
            {
                valuationDateTimePicker.Value = Param.ValuationDate.Value;
            }

            InitFxRates();

            LoadCurrenciesComboBox();
        }

        private void FillScenarioSetup()
        {
            this.scenariosSetupDataGridView.CellValueChanged -= this.scenariosSetupDataGridView_CellValueChanged;

            scenarios = Scenario.FindSelected();
            if (scenarios.Length <= 0) return;

            scenariosSetupDataGridView.Rows.Add(scenarios.Length);

            for (int i = 0; i < scenarios.Length; i++)
            {
                scenariosSetupDataGridView[0, i].Value = scenarios[i].Name;
                IList<VariableValue> variableValues = scenarios[i].VariableValues;
                foreach (VariableValue variableValue in variableValues)
                {
                    int columnIndex = FindColumnIndexbyName(variableValue.Variable.Name);
                    if (columnIndex <= 0) continue;

                    scenariosSetupDataGridView[columnIndex, i].Value = variableValue.Value;
                }
            }

            this.scenariosSetupDataGridView.CellValueChanged += this.scenariosSetupDataGridView_CellValueChanged;
        }

        private int FindColumnIndexbyName(string columnName)
        {
            for (int i = 0; i < scenariosSetupDataGridView.Columns.Count; i++)
            {
                if (scenariosSetupDataGridView.Columns[i].Name == columnName)
                {
                    return i;
                }
            }

            return -1;
        }

        private void addScenariosButton_Click(object sender, EventArgs e)
        {
            if (Tools.ShowDialogWithPrivateSession(new AddScenarios()) == DialogResult.OK)
            {
                RefreshScenarioSetup();
            }
        }

        private void addVariablesButton_Click(object sender, EventArgs e)
        {
            if (Tools.ShowDialogWithPrivateSession(new AddVariables()) == DialogResult.OK)
            {
                RefreshScenarioSetup();
            }
        }

        private void scenariosSetupDataGridView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex <= 0) return;

            Scenario scenario = scenarios[e.RowIndex];
            Variable variable = variables[e.ColumnIndex - 1];

            VariableValue value = VariableValue.Find(variable, scenario);

            if (value == null)
            {
                value = new VariableValue();
                value.Variable = variable;
                value.Scenario = scenario;
            }

            string _val = Convert.ToString(scenariosSetupDataGridView[e.ColumnIndex, e.RowIndex].Value);

            if (_val != value.Value)
            {
                BalanceSheet.modifiedScenarioTemplate = true;
                value.Value = _val;
                value.SaveAndFlush();
            }

        }

        public void RefreshParameters()
        {
            PersistenceSession.Flush();

            RefreshScenarioSetup();

            _marketParametersDataGridView.Refresh(true, valuationDateTimePicker.Value.Date);
            fundamentalVectorsGridControler.Refresh();
        }

        private void importMarketDataButton_Click(object sender, EventArgs e)
        {
            if (marketDataOpenFileDialog.ShowDialog() == DialogResult.OK)
            {
                MarketDataImport marketDataImport = new MarketDataImport(marketDataOpenFileDialog.FileName, valuationDateTimePicker.Value);

                LoadForm.Instance.DoWork(new WorkParameters(marketDataImport.Import, null), null);
                BalanceSheet.modifiedScenarioTemplate = true;
            }
        }

        private void valuationCurrencyComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            Param.ValuationCurrency = valuationCurrencyComboBox.SelectedItem as Currency;
            PersistenceSession.Flush();

            BalanceBL.AjustBalance();

            CurrencyBL.SetSpecialBondsToValuationCurrency();

            ValuationCurrencyChanged(this, new EventArgs());
        }

        private void valuationDateTimePicker_ValueChanged(object sender, EventArgs e)
        {
            BalanceSheet.ValuationDate = valuationDateTimePicker.Value.Date;
            Param.ValuationDate = valuationDateTimePicker.Value.Date;
            PersistenceSession.Flush();
        }

        public void FindAndSelectVariableValue(VariableValue value)
        {
            foreach (DataGridViewRow row in scenariosSetupDataGridView.Rows)
            {
                if ((string)row.Cells[0].Value == value.Scenario.Name)
                {
                    foreach (DataGridViewColumn column in scenariosSetupDataGridView.Columns)
                    {
                        if (column.Name == value.Variable.Name)
                        {
                            scenariosSetupDataGridView.ClearSelection();
                            scenariosSetupDataGridView[column.Index, row.Index].Selected = true;
                            return;
                        }
                    }
                    return;
                }
            }
        }
    }
}
