﻿using AssetModelingModule.Resources.Language;

namespace AssetModelingModule.Views.BalanceSheet.Parameters
{
    partial class Parameters
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.parametersSplitContainer = new System.Windows.Forms.SplitContainer();
            this.marketParametersGroupBox = new System.Windows.Forms.GroupBox();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.marketParametersDataGridView = new System.Windows.Forms.DataGridView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.marketParamtersPanel = new System.Windows.Forms.Panel();
            this.equityIndexComboBox = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.fxRatesComboBox = new System.Windows.Forms.ComboBox();
            this.importMarketDataButton = new System.Windows.Forms.Button();
            this.valuationCurrencyComboBox = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.valuationDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.scenariosSetupGroupBox = new System.Windows.Forms.GroupBox();
            this.scenariosSetupDataGridView = new System.Windows.Forms.DataGridView();
            this.scenarioSetupPanel = new System.Windows.Forms.Panel();
            this.addVariablesButton = new System.Windows.Forms.Button();
            this.addScenariosButton = new System.Windows.Forms.Button();
            this.marketDataOpenFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.fundamentalVectorsGrid = new System.Windows.Forms.DataGridView();
            this.currencyComboBox = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.parametersSplitContainer.Panel1.SuspendLayout();
            this.parametersSplitContainer.Panel2.SuspendLayout();
            this.parametersSplitContainer.SuspendLayout();
            this.marketParametersGroupBox.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.marketParametersDataGridView)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.marketParamtersPanel.SuspendLayout();
            this.scenariosSetupGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scenariosSetupDataGridView)).BeginInit();
            this.scenarioSetupPanel.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fundamentalVectorsGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // parametersSplitContainer
            // 
            this.parametersSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.parametersSplitContainer.Location = new System.Drawing.Point(0, 0);
            this.parametersSplitContainer.Name = "parametersSplitContainer";
            this.parametersSplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // parametersSplitContainer.Panel1
            // 
            this.parametersSplitContainer.Panel1.Controls.Add(this.marketParametersGroupBox);
            // 
            // parametersSplitContainer.Panel2
            // 
            this.parametersSplitContainer.Panel2.Controls.Add(this.scenariosSetupGroupBox);
            this.parametersSplitContainer.Size = new System.Drawing.Size(929, 536);
            this.parametersSplitContainer.SplitterDistance = 276;
            this.parametersSplitContainer.TabIndex = 1;
            // 
            // marketParametersGroupBox
            // 
            this.marketParametersGroupBox.Controls.Add(this.splitContainer1);
            this.marketParametersGroupBox.Controls.Add(this.marketParamtersPanel);
            this.marketParametersGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.marketParametersGroupBox.Location = new System.Drawing.Point(0, 0);
            this.marketParametersGroupBox.Name = "marketParametersGroupBox";
            this.marketParametersGroupBox.Size = new System.Drawing.Size(929, 276);
            this.marketParametersGroupBox.TabIndex = 0;
            this.marketParametersGroupBox.TabStop = false;
            this.marketParametersGroupBox.Text = Labels.Parameters_InitializeComponent_Market_Parameters;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(3, 78);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.groupBox2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.groupBox1);
            this.splitContainer1.Size = new System.Drawing.Size(923, 195);
            this.splitContainer1.SplitterDistance = 447;
            this.splitContainer1.TabIndex = 6;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.marketParametersDataGridView);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(447, 195);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = Labels.Parameters_InitializeComponent_Important_Market_Data;
            // 
            // marketParametersDataGridView
            // 
            this.marketParametersDataGridView.AllowUserToAddRows = false;
            this.marketParametersDataGridView.BackgroundColor = System.Drawing.SystemColors.Window;
            this.marketParametersDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.marketParametersDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.marketParametersDataGridView.Location = new System.Drawing.Point(3, 16);
            this.marketParametersDataGridView.Name = "marketParametersDataGridView";
            this.marketParametersDataGridView.ReadOnly = true;
            this.marketParametersDataGridView.Size = new System.Drawing.Size(441, 176);
            this.marketParametersDataGridView.TabIndex = 3;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.fundamentalVectorsGrid);
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(472, 195);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = Labels.Parameters_InitializeComponent_Fundamental_Market_Data;
            // 
            // marketParamtersPanel
            // 
            this.marketParamtersPanel.Controls.Add(this.equityIndexComboBox);
            this.marketParamtersPanel.Controls.Add(this.label4);
            this.marketParamtersPanel.Controls.Add(this.label3);
            this.marketParamtersPanel.Controls.Add(this.fxRatesComboBox);
            this.marketParamtersPanel.Controls.Add(this.importMarketDataButton);
            this.marketParamtersPanel.Controls.Add(this.valuationCurrencyComboBox);
            this.marketParamtersPanel.Controls.Add(this.label2);
            this.marketParamtersPanel.Controls.Add(this.valuationDateTimePicker);
            this.marketParamtersPanel.Controls.Add(this.label1);
            this.marketParamtersPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.marketParamtersPanel.Location = new System.Drawing.Point(3, 16);
            this.marketParamtersPanel.Name = "marketParamtersPanel";
            this.marketParamtersPanel.Size = new System.Drawing.Size(923, 62);
            this.marketParamtersPanel.TabIndex = 0;
            // 
            // equityIndexComboBox
            // 
            this.equityIndexComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.equityIndexComboBox.FormattingEnabled = true;
            this.equityIndexComboBox.Items.AddRange(new object[] {
            "CAC     3,124.35"});
            this.equityIndexComboBox.Location = new System.Drawing.Point(370, 34);
            this.equityIndexComboBox.Name = "equityIndexComboBox";
            this.equityIndexComboBox.Size = new System.Drawing.Size(121, 21);
            this.equityIndexComboBox.TabIndex = 8;
            this.equityIndexComboBox.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(262, 34);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = Labels.Parameters_InitializeComponent_Equity_Index__;
            this.label4.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(4, 34);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = Labels.Parameters_InitializeComponent_FX_Rates__;
            // 
            // fxRatesComboBox
            // 
            this.fxRatesComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.fxRatesComboBox.FormattingEnabled = true;
            this.fxRatesComboBox.Items.AddRange(new object[] {
            "€/$     1.28974"});
            this.fxRatesComboBox.Location = new System.Drawing.Point(94, 34);
            this.fxRatesComboBox.Name = "fxRatesComboBox";
            this.fxRatesComboBox.Size = new System.Drawing.Size(133, 21);
            this.fxRatesComboBox.TabIndex = 5;
            // 
            // importMarketDataButton
            // 
            this.importMarketDataButton.Location = new System.Drawing.Point(526, 7);
            this.importMarketDataButton.Name = "importMarketDataButton";
            this.importMarketDataButton.Padding = new System.Windows.Forms.Padding(5);
            this.importMarketDataButton.Size = new System.Drawing.Size(114, 46);
            this.importMarketDataButton.TabIndex = 4;
            this.importMarketDataButton.Text = Labels.Parameters_InitializeComponent_Import_Market_Data;
            this.importMarketDataButton.UseVisualStyleBackColor = true;
            this.importMarketDataButton.Click += new System.EventHandler(this.importMarketDataButton_Click);
            // 
            // valuationCurrencyComboBox
            // 
            this.valuationCurrencyComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.valuationCurrencyComboBox.FormattingEnabled = true;
            this.valuationCurrencyComboBox.Location = new System.Drawing.Point(370, 8);
            this.valuationCurrencyComboBox.Name = "valuationCurrencyComboBox";
            this.valuationCurrencyComboBox.Size = new System.Drawing.Size(121, 21);
            this.valuationCurrencyComboBox.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(262, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(102, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = Labels.Parameters_InitializeComponent_Valuation_Currency__;
            // 
            // valuationDateTimePicker
            // 
            this.valuationDateTimePicker.CustomFormat = "";
            this.valuationDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.valuationDateTimePicker.Location = new System.Drawing.Point(94, 8);
            this.valuationDateTimePicker.Name = "valuationDateTimePicker";
            this.valuationDateTimePicker.Size = new System.Drawing.Size(133, 20);
            this.valuationDateTimePicker.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = Labels.Parameters_InitializeComponent_Valuation_Date__;
            // 
            // scenariosSetupGroupBox
            // 
            this.scenariosSetupGroupBox.Controls.Add(this.scenariosSetupDataGridView);
            this.scenariosSetupGroupBox.Controls.Add(this.scenarioSetupPanel);
            this.scenariosSetupGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scenariosSetupGroupBox.Location = new System.Drawing.Point(0, 0);
            this.scenariosSetupGroupBox.Name = "scenariosSetupGroupBox";
            this.scenariosSetupGroupBox.Size = new System.Drawing.Size(929, 256);
            this.scenariosSetupGroupBox.TabIndex = 0;
            this.scenariosSetupGroupBox.TabStop = false;
            this.scenariosSetupGroupBox.Text = Labels.Parameters_InitializeComponent_Scenarios_Setup;
            // 
            // scenariosSetupDataGridView
            // 
            this.scenariosSetupDataGridView.AllowUserToAddRows = false;
            this.scenariosSetupDataGridView.AllowUserToDeleteRows = false;
            this.scenariosSetupDataGridView.AllowUserToOrderColumns = true;
            this.scenariosSetupDataGridView.BackgroundColor = System.Drawing.SystemColors.Window;
            this.scenariosSetupDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.scenariosSetupDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scenariosSetupDataGridView.Location = new System.Drawing.Point(3, 53);
            this.scenariosSetupDataGridView.Name = "scenariosSetupDataGridView";
            this.scenariosSetupDataGridView.Size = new System.Drawing.Size(923, 200);
            this.scenariosSetupDataGridView.TabIndex = 1;
            // 
            // scenarioSetupPanel
            // 
            this.scenarioSetupPanel.Controls.Add(this.addVariablesButton);
            this.scenarioSetupPanel.Controls.Add(this.addScenariosButton);
            this.scenarioSetupPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.scenarioSetupPanel.Location = new System.Drawing.Point(3, 16);
            this.scenarioSetupPanel.Name = "scenarioSetupPanel";
            this.scenarioSetupPanel.Size = new System.Drawing.Size(923, 37);
            this.scenarioSetupPanel.TabIndex = 0;
            // 
            // addVariablesButton
            // 
            this.addVariablesButton.Location = new System.Drawing.Point(127, 5);
            this.addVariablesButton.Name = "addVariablesButton";
            this.addVariablesButton.Size = new System.Drawing.Size(114, 23);
            this.addVariablesButton.TabIndex = 1;
            this.addVariablesButton.Text = Labels.Parameters_InitializeComponent_Add_Variables____;
            this.addVariablesButton.UseVisualStyleBackColor = true;
            this.addVariablesButton.Click += new System.EventHandler(this.addVariablesButton_Click);
            // 
            // addScenariosButton
            // 
            this.addScenariosButton.Location = new System.Drawing.Point(7, 5);
            this.addScenariosButton.Name = "addScenariosButton";
            this.addScenariosButton.Size = new System.Drawing.Size(114, 23);
            this.addScenariosButton.TabIndex = 0;
            this.addScenariosButton.Text = Labels.Parameters_InitializeComponent_Add_Scenarios____;
            this.addScenariosButton.UseVisualStyleBackColor = true;
            this.addScenariosButton.Click += new System.EventHandler(this.addScenariosButton_Click);
            // 
            // marketDataOpenFileDialog
            // 
            this.marketDataOpenFileDialog.FileName = "openFileDialog1";
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewTextBoxColumn1.HeaderText = Labels.Parameters_InitializeComponent_Vector;
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 153;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn2.HeaderText = Labels.Parameters_InitializeComponent_Formula;
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.currencyComboBox);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(3, 16);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(466, 29);
            this.panel1.TabIndex = 0;
            // 
            // fundamentalVectorsGrid
            // 
            this.fundamentalVectorsGrid.AllowUserToAddRows = false;
            this.fundamentalVectorsGrid.AllowUserToDeleteRows = false;
            this.fundamentalVectorsGrid.AllowUserToOrderColumns = true;
            this.fundamentalVectorsGrid.AllowUserToResizeRows = false;
            this.fundamentalVectorsGrid.BackgroundColor = System.Drawing.SystemColors.Window;
            this.fundamentalVectorsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.fundamentalVectorsGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fundamentalVectorsGrid.Location = new System.Drawing.Point(3, 45);
            this.fundamentalVectorsGrid.Name = "fundamentalVectorsGrid";
            this.fundamentalVectorsGrid.Size = new System.Drawing.Size(466, 147);
            this.fundamentalVectorsGrid.TabIndex = 3;
            // 
            // currencyComboBox
            // 
            this.currencyComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.currencyComboBox.FormattingEnabled = true;
            this.currencyComboBox.Location = new System.Drawing.Point(66, 5);
            this.currencyComboBox.Name = "currencyComboBox";
            this.currencyComboBox.Size = new System.Drawing.Size(121, 21);
            this.currencyComboBox.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(5, 5);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(55, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = Labels.Parameters_InitializeComponent_Currency__;
            // 
            // Parameters
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.parametersSplitContainer);
            this.Name = "Parameters";
            this.Size = new System.Drawing.Size(929, 536);
            this.Load += new System.EventHandler(this.Parameters_Load);
            this.parametersSplitContainer.Panel1.ResumeLayout(false);
            this.parametersSplitContainer.Panel2.ResumeLayout(false);
            this.parametersSplitContainer.ResumeLayout(false);
            this.marketParametersGroupBox.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.marketParametersDataGridView)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.marketParamtersPanel.ResumeLayout(false);
            this.marketParamtersPanel.PerformLayout();
            this.scenariosSetupGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scenariosSetupDataGridView)).EndInit();
            this.scenarioSetupPanel.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fundamentalVectorsGrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer parametersSplitContainer;
        private System.Windows.Forms.GroupBox marketParametersGroupBox;
        private System.Windows.Forms.Panel marketParamtersPanel;
        private System.Windows.Forms.ComboBox equityIndexComboBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox fxRatesComboBox;
        private System.Windows.Forms.Button importMarketDataButton;
        private System.Windows.Forms.ComboBox valuationCurrencyComboBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox scenariosSetupGroupBox;
        private System.Windows.Forms.DataGridView scenariosSetupDataGridView;
        private System.Windows.Forms.Panel scenarioSetupPanel;
        private System.Windows.Forms.Button addVariablesButton;
        private System.Windows.Forms.Button addScenariosButton;
        private System.Windows.Forms.OpenFileDialog marketDataOpenFileDialog;
        private System.Windows.Forms.DateTimePicker valuationDateTimePicker;
        private System.Windows.Forms.DataGridView marketParametersDataGridView;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridView fundamentalVectorsGrid;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox currencyComboBox;
        private System.Windows.Forms.Label label5;
    }
}
