﻿using AssetModelingModule.Resources.Language;

namespace AssetModelingModule.Views.BalanceSheet
{
    partial class BalanceSheet
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BalanceSheet));
            this.mainTabControl = new System.Windows.Forms.TabControl();
            this.balanceSheetTabPage = new System.Windows.Forms.TabPage();
            this.balance = new AssetModelingModule.Views.BalanceSheet.Balance.Balance();
            this.parametersTabPage = new System.Windows.Forms.TabPage();
            this.parameters = new AssetModelingModule.Views.BalanceSheet.Parameters.Parameters();
            this.resultTabPage = new System.Windows.Forms.TabPage();
            this.result = new AssetModelingModule.Views.BalanceSheet.Result.Result();
            this.mainToolStrip = new System.Windows.Forms.ToolStrip();
            this.newToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.openToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.saveToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.copyToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.pasteToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.vectorManagerToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.computeResultsStripButton = new System.Windows.Forms.ToolStripButton();
            this.multiScenarioToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.fontNameToolStripComboBox = new System.Windows.Forms.ToolStripComboBox();
            this.fontSizeToolStripComboBox = new System.Windows.Forms.ToolStripComboBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openWorkspacetoolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openLastToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveWorkspaceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clearAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.applyScenarioTemplate = new System.Windows.Forms.ToolStripMenuItem();
            this.saveScenarioTemplateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pasteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.selectAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vectorManagerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dynamicVectorsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.variableDefinitionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.otherItemsAssumptionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.taxAssumptionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dividendToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.currenciesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showBankALMHelpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.saveScenarioFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.openScenarioFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.workspaceOpenFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.workspaceSaveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.mainTabControl.SuspendLayout();
            this.balanceSheetTabPage.SuspendLayout();
            this.parametersTabPage.SuspendLayout();
            this.resultTabPage.SuspendLayout();
            this.mainToolStrip.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainTabControl
            // 
            this.mainTabControl.Controls.Add(this.balanceSheetTabPage);
            this.mainTabControl.Controls.Add(this.parametersTabPage);
            this.mainTabControl.Controls.Add(this.resultTabPage);
            this.mainTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainTabControl.Location = new System.Drawing.Point(0, 49);
            this.mainTabControl.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.mainTabControl.Name = "mainTabControl";
            this.mainTabControl.SelectedIndex = 0;
            this.mainTabControl.Size = new System.Drawing.Size(808, 402);
            this.mainTabControl.TabIndex = 1;
            this.mainTabControl.SelectedIndexChanged += new System.EventHandler(this.mainTabControl_SelectedIndexChanged);
            // 
            // balanceSheetTabPage
            // 
            this.balanceSheetTabPage.Controls.Add(this.balance);
            this.balanceSheetTabPage.Location = new System.Drawing.Point(4, 22);
            this.balanceSheetTabPage.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.balanceSheetTabPage.Name = "balanceSheetTabPage";
            this.balanceSheetTabPage.Padding = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.balanceSheetTabPage.Size = new System.Drawing.Size(800, 376);
            this.balanceSheetTabPage.TabIndex = 0;
            this.balanceSheetTabPage.Text = Labels.BalanceSheet_InitializeComponent_Balance_Sheet;
            this.balanceSheetTabPage.UseVisualStyleBackColor = true;
            // 
            // balance
            // 
            this.balance.Dock = System.Windows.Forms.DockStyle.Fill;
            this.balance.Location = new System.Drawing.Point(2, 3);
            this.balance.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.balance.Name = "balance";
            this.balance.Size = new System.Drawing.Size(796, 370);
            this.balance.TabIndex = 0;
            // 
            // parametersTabPage
            // 
            this.parametersTabPage.Controls.Add(this.parameters);
            this.parametersTabPage.Location = new System.Drawing.Point(4, 22);
            this.parametersTabPage.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.parametersTabPage.Name = "parametersTabPage";
            this.parametersTabPage.Padding = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.parametersTabPage.Size = new System.Drawing.Size(800, 376);
            this.parametersTabPage.TabIndex = 1;
            this.parametersTabPage.Text = Labels.BalanceSheet_InitializeComponent_Parameters;
            this.parametersTabPage.UseVisualStyleBackColor = true;
            // 
            // parameters
            // 
            this.parameters.Dock = System.Windows.Forms.DockStyle.Fill;
            this.parameters.Location = new System.Drawing.Point(2, 3);
            this.parameters.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.parameters.Name = "parameters";
            this.parameters.Size = new System.Drawing.Size(796, 370);
            this.parameters.TabIndex = 0;
            // 
            // resultTabPage
            // 
            this.resultTabPage.Controls.Add(this.result);
            this.resultTabPage.Location = new System.Drawing.Point(4, 22);
            this.resultTabPage.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.resultTabPage.Name = "resultTabPage";
            this.resultTabPage.Padding = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.resultTabPage.Size = new System.Drawing.Size(800, 376);
            this.resultTabPage.TabIndex = 2;
            this.resultTabPage.Text = Labels.BalanceSheet_InitializeComponent_Result;
            this.resultTabPage.UseVisualStyleBackColor = true;
            // 
            // result
            // 
            this.result.Dock = System.Windows.Forms.DockStyle.Fill;
            this.result.Location = new System.Drawing.Point(2, 3);
            this.result.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.result.Name = "result";
            this.result.Size = new System.Drawing.Size(796, 370);
            this.result.TabIndex = 0;
            // 
            // mainToolStrip
            // 
            this.mainToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripButton,
            this.openToolStripButton,
            this.saveToolStripButton,
            this.toolStripSeparator1,
            this.toolStripButton4,
            this.copyToolStripButton,
            this.pasteToolStripButton,
            this.toolStripSeparator2,
            this.vectorManagerToolStripButton,
            this.computeResultsStripButton,
            this.multiScenarioToolStripButton,
            this.toolStripSeparator3,
            this.fontNameToolStripComboBox,
            this.fontSizeToolStripComboBox});
            this.mainToolStrip.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.mainToolStrip.Location = new System.Drawing.Point(0, 24);
            this.mainToolStrip.Name = "mainToolStrip";
            this.mainToolStrip.Padding = new System.Windows.Forms.Padding(0, 2, 1, 0);
            this.mainToolStrip.Size = new System.Drawing.Size(808, 25);
            this.mainToolStrip.TabIndex = 3;
            this.mainToolStrip.Text = "toolStrip1";
            // 
            // newToolStripButton
            // 
            this.newToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.newToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("newToolStripButton.Image")));
            this.newToolStripButton.ImageTransparentColor = System.Drawing.Color.Black;
            this.newToolStripButton.Name = "newToolStripButton";
            this.newToolStripButton.Size = new System.Drawing.Size(23, 20);
            this.newToolStripButton.Text = Labels.BalanceSheet_InitializeComponent_New;
            // 
            // openToolStripButton
            // 
            this.openToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.openToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("openToolStripButton.Image")));
            this.openToolStripButton.ImageTransparentColor = System.Drawing.Color.Black;
            this.openToolStripButton.Name = "openToolStripButton";
            this.openToolStripButton.Size = new System.Drawing.Size(23, 20);
            this.openToolStripButton.Text = Labels.BalanceSheet_InitializeComponent_Open;
            this.openToolStripButton.Click += new System.EventHandler(this.openToolStripButton_Click);
            // 
            // saveToolStripButton
            // 
            this.saveToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.saveToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("saveToolStripButton.Image")));
            this.saveToolStripButton.ImageTransparentColor = System.Drawing.Color.Black;
            this.saveToolStripButton.Name = "saveToolStripButton";
            this.saveToolStripButton.Size = new System.Drawing.Size(23, 20);
            this.saveToolStripButton.Text = Labels.BalanceSheet_InitializeComponent_Save;
            this.saveToolStripButton.Click += new System.EventHandler(this.saveToolStripButton_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 23);
            // 
            // toolStripButton4
            // 
            this.toolStripButton4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton4.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton4.Image")));
            this.toolStripButton4.ImageTransparentColor = System.Drawing.Color.Black;
            this.toolStripButton4.Name = "toolStripButton4";
            this.toolStripButton4.Size = new System.Drawing.Size(23, 20);
            this.toolStripButton4.Text = Labels.BalanceSheet_InitializeComponent_Cut;
            // 
            // copyToolStripButton
            // 
            this.copyToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.copyToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("copyToolStripButton.Image")));
            this.copyToolStripButton.ImageTransparentColor = System.Drawing.Color.Black;
            this.copyToolStripButton.Name = "copyToolStripButton";
            this.copyToolStripButton.Size = new System.Drawing.Size(23, 20);
            this.copyToolStripButton.Text = Labels.BalanceSheet_InitializeComponent_Copy;
            this.copyToolStripButton.Click += new System.EventHandler(this.copyToolStripButton_Click);
            // 
            // pasteToolStripButton
            // 
            this.pasteToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.pasteToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("pasteToolStripButton.Image")));
            this.pasteToolStripButton.ImageTransparentColor = System.Drawing.Color.Black;
            this.pasteToolStripButton.Name = "pasteToolStripButton";
            this.pasteToolStripButton.Size = new System.Drawing.Size(23, 20);
            this.pasteToolStripButton.Text = Labels.BalanceSheet_InitializeComponent_Paste;
            this.pasteToolStripButton.Click += new System.EventHandler(this.pasteToolStripButton_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 23);
            // 
            // vectorManagerToolStripButton
            // 
            this.vectorManagerToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.vectorManagerToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("vectorManagerToolStripButton.Image")));
            this.vectorManagerToolStripButton.ImageTransparentColor = System.Drawing.Color.Black;
            this.vectorManagerToolStripButton.Name = "vectorManagerToolStripButton";
            this.vectorManagerToolStripButton.Size = new System.Drawing.Size(23, 20);
            this.vectorManagerToolStripButton.Text = Labels.BalanceSheet_InitializeComponent_Vector_Manager;
            this.vectorManagerToolStripButton.Click += new System.EventHandler(this.vectorManagerToolStripButton_Click);
            // 
            // computeResultsStripButton
            // 
            this.computeResultsStripButton.Image = global::AssetModelingModule.Properties.Resources._48px_Gnome_accessories_calculator_svg;
            this.computeResultsStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.computeResultsStripButton.Name = "computeResultsStripButton";
            this.computeResultsStripButton.Size = new System.Drawing.Size(76, 20);
            this.computeResultsStripButton.Text = Labels.BalanceSheet_InitializeComponent_Calculate;
            this.computeResultsStripButton.Click += new System.EventHandler(this.computeResultsStripButton_Click);
            // 
            // multiScenarioToolStripButton
            // 
            this.multiScenarioToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("multiScenarioToolStripButton.Image")));
            this.multiScenarioToolStripButton.ImageTransparentColor = System.Drawing.Color.White;
            this.multiScenarioToolStripButton.Name = "multiScenarioToolStripButton";
            this.multiScenarioToolStripButton.Size = new System.Drawing.Size(164, 20);
            this.multiScenarioToolStripButton.Text = "Multi-scenario Simulation";
            this.multiScenarioToolStripButton.Click += new System.EventHandler(this.multiScenarioToolStripButton_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 23);
            // 
            // fontNameToolStripComboBox
            // 
            this.fontNameToolStripComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.fontNameToolStripComboBox.Name = "fontNameToolStripComboBox";
            this.fontNameToolStripComboBox.Size = new System.Drawing.Size(144, 23);
            this.fontNameToolStripComboBox.SelectedIndexChanged += new System.EventHandler(this.fontNameToolStripComboBox_SelectedIndexChanged);
            // 
            // fontSizeToolStripComboBox
            // 
            this.fontSizeToolStripComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.fontSizeToolStripComboBox.Items.AddRange(new object[] {
            "5",
            "5.5",
            "6",
            "6.5",
            "7",
            "7.5",
            "8",
            "8.5",
            "9",
            "10",
            "11",
            "12"});
            this.fontSizeToolStripComboBox.Name = "fontSizeToolStripComboBox";
            this.fontSizeToolStripComboBox.Size = new System.Drawing.Size(89, 23);
            this.fontSizeToolStripComboBox.SelectedIndexChanged += new System.EventHandler(this.fontSizeToolStripComboBox_SelectedIndexChanged);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem,
            this.toolsToolStripMenuItem,
            this.optionsToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(808, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "balanceMenuStrip";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openWorkspacetoolStripMenuItem,
            this.openLastToolStripMenuItem,
            this.saveWorkspaceToolStripMenuItem,
            this.clearAllToolStripMenuItem,
            this.toolStripSeparator7,
            this.newToolStripMenuItem,
            this.openToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.saveAsToolStripMenuItem,
            this.toolStripSeparator4,
            this.applyScenarioTemplate,
            this.saveScenarioTemplateToolStripMenuItem,
            this.toolStripSeparator5,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = Labels.BalanceSheet_InitializeComponent__File;
            // 
            // openWorkspacetoolStripMenuItem
            // 
            this.openWorkspacetoolStripMenuItem.Name = "openWorkspacetoolStripMenuItem";
            this.openWorkspacetoolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.openWorkspacetoolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.openWorkspacetoolStripMenuItem.Text = Labels.BalanceSheet_InitializeComponent_Open_Workspace____;
            this.openWorkspacetoolStripMenuItem.Click += new System.EventHandler(this.openWorkspacetoolStripMenuItem_Click);
            // 
            // openLastToolStripMenuItem
            // 
            this.openLastToolStripMenuItem.Name = "openLastToolStripMenuItem";
            this.openLastToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.openLastToolStripMenuItem.Text = Labels.BalanceSheet_InitializeComponent_Open_Last;
            // 
            // saveWorkspaceToolStripMenuItem
            // 
            this.saveWorkspaceToolStripMenuItem.Name = "saveWorkspaceToolStripMenuItem";
            this.saveWorkspaceToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.saveWorkspaceToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.saveWorkspaceToolStripMenuItem.Text = Labels.BalanceSheet_InitializeComponent_Save_Workspace____;
            this.saveWorkspaceToolStripMenuItem.Click += new System.EventHandler(this.saveWorkspaceToolStripMenuItem_Click);
            // 
            // clearAllToolStripMenuItem
            // 
            this.clearAllToolStripMenuItem.Name = "clearAllToolStripMenuItem";
            this.clearAllToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.clearAllToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.clearAllToolStripMenuItem.Text = Labels.BalanceSheet_InitializeComponent_Clear_All;
            this.clearAllToolStripMenuItem.Click += new System.EventHandler(this.clearALLStripMenuItem_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(216, 6);
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.newToolStripMenuItem.Text = Labels.BalanceSheet_InitializeComponent_New_Portfolio;
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.openToolStripMenuItem.Text = Labels.BalanceSheet_InitializeComponent_Import_Portfolio;
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.saveToolStripMenuItem.Text = Labels.BalanceSheet_InitializeComponent_Save;
            this.saveToolStripMenuItem.Visible = false;
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.saveAsToolStripMenuItem.Text = Labels.BalanceSheet_InitializeComponent_Export_portfolio;
            this.saveAsToolStripMenuItem.Click += new System.EventHandler(this.saveAsToolStripMenuItem_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(216, 6);
            // 
            // applyScenarioTemplate
            // 
            this.applyScenarioTemplate.Name = "applyScenarioTemplate";
            this.applyScenarioTemplate.Size = new System.Drawing.Size(219, 22);
            this.applyScenarioTemplate.Text = Labels.BalanceSheet_InitializeComponent_Import_Scenario;
            this.applyScenarioTemplate.Click += new System.EventHandler(this.applyScenarioTemplate_Click);
            // 
            // saveScenarioTemplateToolStripMenuItem
            // 
            this.saveScenarioTemplateToolStripMenuItem.Name = "saveScenarioTemplateToolStripMenuItem";
            this.saveScenarioTemplateToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.saveScenarioTemplateToolStripMenuItem.Text = Labels.BalanceSheet_InitializeComponent_Export_Scenario;
            this.saveScenarioTemplateToolStripMenuItem.Click += new System.EventHandler(this.saveScenarioTemplateToolStripMenuItem_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(216, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q)));
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.exitToolStripMenuItem.Text = Labels.BalanceSheet_InitializeComponent_E_xit;
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cutToolStripMenuItem,
            this.copyToolStripMenuItem,
            this.pasteToolStripMenuItem,
            this.toolStripSeparator6,
            this.selectAllToolStripMenuItem});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.editToolStripMenuItem.Text = Labels.BalanceSheet_InitializeComponent__Edit;
            // 
            // cutToolStripMenuItem
            // 
            this.cutToolStripMenuItem.Name = "cutToolStripMenuItem";
            this.cutToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X)));
            this.cutToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.cutToolStripMenuItem.Text = Labels.BalanceSheet_InitializeComponent_Cut;
            // 
            // copyToolStripMenuItem
            // 
            this.copyToolStripMenuItem.Name = "copyToolStripMenuItem";
            this.copyToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this.copyToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.copyToolStripMenuItem.Text = Labels.BalanceSheet_InitializeComponent_Copy;
            this.copyToolStripMenuItem.Click += new System.EventHandler(this.copyToolStripMenuItem_Click);
            // 
            // pasteToolStripMenuItem
            // 
            this.pasteToolStripMenuItem.Name = "pasteToolStripMenuItem";
            this.pasteToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V)));
            this.pasteToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.pasteToolStripMenuItem.Text = Labels.BalanceSheet_InitializeComponent_Paste;
            this.pasteToolStripMenuItem.Click += new System.EventHandler(this.pasteToolStripMenuItem_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(161, 6);
            // 
            // selectAllToolStripMenuItem
            // 
            this.selectAllToolStripMenuItem.Enabled = false;
            this.selectAllToolStripMenuItem.Name = "selectAllToolStripMenuItem";
            this.selectAllToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A)));
            this.selectAllToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.selectAllToolStripMenuItem.Text = Labels.BalanceSheet_InitializeComponent_Select_All;
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.vectorManagerToolStripMenuItem,
            this.dynamicVectorsToolStripMenuItem,
            this.variableDefinitionToolStripMenuItem,
            this.otherItemsAssumptionsToolStripMenuItem,
            this.taxAssumptionsToolStripMenuItem,
            this.dividendToolStripMenuItem});
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.toolsToolStripMenuItem.Text = Labels.BalanceSheet_InitializeComponent__Tools;
            // 
            // vectorManagerToolStripMenuItem
            // 
            this.vectorManagerToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("vectorManagerToolStripMenuItem.Image")));
            this.vectorManagerToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Black;
            this.vectorManagerToolStripMenuItem.Name = "vectorManagerToolStripMenuItem";
            this.vectorManagerToolStripMenuItem.Size = new System.Drawing.Size(208, 22);
            this.vectorManagerToolStripMenuItem.Text = Labels.BalanceSheet_InitializeComponent_Static_Vectors___;
            this.vectorManagerToolStripMenuItem.Click += new System.EventHandler(this.vectorManagerToolStripMenuItem_Click);
            // 
            // dynamicVectorsToolStripMenuItem
            // 
            this.dynamicVectorsToolStripMenuItem.Name = "dynamicVectorsToolStripMenuItem";
            this.dynamicVectorsToolStripMenuItem.Size = new System.Drawing.Size(208, 22);
            this.dynamicVectorsToolStripMenuItem.Text = Labels.BalanceSheet_InitializeComponent_Dynamic_Vectors___;
            this.dynamicVectorsToolStripMenuItem.Click += new System.EventHandler(this.dynamicVectorsToolStripMenuItem_Click);
            // 
            // variableDefinitionToolStripMenuItem
            // 
            this.variableDefinitionToolStripMenuItem.Name = "variableDefinitionToolStripMenuItem";
            this.variableDefinitionToolStripMenuItem.Size = new System.Drawing.Size(208, 22);
            this.variableDefinitionToolStripMenuItem.Text = Labels.BalanceSheet_InitializeComponent_Variable_Definition____;
            this.variableDefinitionToolStripMenuItem.Click += new System.EventHandler(this.variableDefinitionToolStripMenuItem_Click);
            // 
            // otherItemsAssumptionsToolStripMenuItem
            // 
            this.otherItemsAssumptionsToolStripMenuItem.Name = "otherItemsAssumptionsToolStripMenuItem";
            this.otherItemsAssumptionsToolStripMenuItem.Size = new System.Drawing.Size(208, 22);
            this.otherItemsAssumptionsToolStripMenuItem.Text = Labels.BalanceSheet_InitializeComponent_Other_Items_Assumptions;
            this.otherItemsAssumptionsToolStripMenuItem.Click += new System.EventHandler(this.otherItemsAssumptionsToolStripMenuItem_Click);
            // 
            // taxAssumptionsToolStripMenuItem
            // 
            this.taxAssumptionsToolStripMenuItem.Name = "taxAssumptionsToolStripMenuItem";
            this.taxAssumptionsToolStripMenuItem.Size = new System.Drawing.Size(208, 22);
            this.taxAssumptionsToolStripMenuItem.Text = Labels.BalanceSheet_InitializeComponent_Tax_Assumptions;
            this.taxAssumptionsToolStripMenuItem.Click += new System.EventHandler(this.taxAssumptionsToolStripMenuItem_Click);
            // 
            // dividendToolStripMenuItem
            // 
            this.dividendToolStripMenuItem.Name = "dividendToolStripMenuItem";
            this.dividendToolStripMenuItem.Size = new System.Drawing.Size(208, 22);
            this.dividendToolStripMenuItem.Text = Labels.BalanceSheet_InitializeComponent_Dividend;
            this.dividendToolStripMenuItem.Click += new System.EventHandler(this.dividendToolStripMenuItem_Click);
            // 
            // optionsToolStripMenuItem
            // 
            this.optionsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.currenciesToolStripMenuItem});
            this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
            this.optionsToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.optionsToolStripMenuItem.Text = Labels.BalanceSheet_InitializeComponent__Options;
            // 
            // currenciesToolStripMenuItem
            // 
            this.currenciesToolStripMenuItem.Name = "currenciesToolStripMenuItem";
            this.currenciesToolStripMenuItem.Size = new System.Drawing.Size(130, 22);
            this.currenciesToolStripMenuItem.Text = Labels.BalanceSheet_InitializeComponent__Currencies;
            this.currenciesToolStripMenuItem.Click += new System.EventHandler(this.currenciesToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showBankALMHelpToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = Labels.BalanceSheet_InitializeComponent__Help;
            // 
            // showBankALMHelpToolStripMenuItem
            // 
            this.showBankALMHelpToolStripMenuItem.Name = "showBankALMHelpToolStripMenuItem";
            this.showBankALMHelpToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.showBankALMHelpToolStripMenuItem.Text = Labels.BalanceSheet_InitializeComponent_Show_Bank_ALM_Help;
            this.showBankALMHelpToolStripMenuItem.Click += new System.EventHandler(this.showBankALMHelpToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.aboutToolStripMenuItem.Text = Labels.BalanceSheet_InitializeComponent_About;
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // openFileDialog
            // 
            this.openFileDialog.DefaultExt = "alm";
            this.openFileDialog.Filter = Labels.BalanceSheet_InitializeComponent_ALM_Files+"|*.amm|"+Labels.BalanceSheet_InitializeComponent_All_files+"|*.*";
            // 
            // saveFileDialog
            // 
            this.saveFileDialog.DefaultExt = "alm";
            this.saveFileDialog.FileName = Labels.BalanceSheet_InitializeComponent_New_ALM;
            this.saveFileDialog.Filter = Labels.BalanceSheet_InitializeComponent_ALM_Files+"|*.amm|"+Labels.BalanceSheet_InitializeComponent_All_files+"|*.*";
            // 
            // saveScenarioFileDialog
            // 
            this.saveScenarioFileDialog.DefaultExt = "sct";
            this.saveScenarioFileDialog.FileName = Labels.BalanceSheet_InitializeComponent_New_SCT;
            this.saveScenarioFileDialog.Filter = Labels.BalanceSheet_InitializeComponent_SCT_Files+"|*.ams|"+Labels.BalanceSheet_InitializeComponent_All_files+"|*.*";
            // 
            // openScenarioFileDialog
            // 
            this.openScenarioFileDialog.DefaultExt = "sct";
            this.openScenarioFileDialog.Filter = Labels.BalanceSheet_InitializeComponent_SCT_Files+"|*.ams|"+Labels.BalanceSheet_InitializeComponent_All_files+"|*.*";
            // 
            // workspaceOpenFileDialog
            // 
            this.workspaceOpenFileDialog.DefaultExt = "alw";
            this.workspaceOpenFileDialog.Filter = Labels.BalanceSheet_InitializeComponent_Workspace_Files+"|*.amw|"+Labels.BalanceSheet_InitializeComponent_All_files+"|*.*";
            // 
            // workspaceSaveFileDialog
            // 
            this.workspaceSaveFileDialog.DefaultExt = "alw";
            this.workspaceSaveFileDialog.FileName = Labels.BalanceSheet_InitializeComponent_New_Workspace;
            this.workspaceSaveFileDialog.Filter = Labels.BalanceSheet_InitializeComponent_Workspace_Files+"|*.amw|"+Labels.BalanceSheet_InitializeComponent_All_files+"|*.*";
            // 
            // BalanceSheet
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(808, 451);
            this.Controls.Add(this.mainTabControl);
            this.Controls.Add(this.mainToolStrip);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.Name = "BalanceSheet";
            this.Text = Labels.BalanceSheet_InitializeComponent_ALM_Solutions;
            this.Load += new System.EventHandler(this.BalanceSheet_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.BalanceSheet_FormClosing);
            this.HelpRequested += new System.Windows.Forms.HelpEventHandler(this.BalanceSheet_HelpRequested);
            this.mainTabControl.ResumeLayout(false);
            this.balanceSheetTabPage.ResumeLayout(false);
            this.parametersTabPage.ResumeLayout(false);
            this.resultTabPage.ResumeLayout(false);
            this.mainToolStrip.ResumeLayout(false);
            this.mainToolStrip.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl mainTabControl;
        private System.Windows.Forms.TabPage parametersTabPage;
        private System.Windows.Forms.TabPage resultTabPage;
        private Result.Result result;
        private Parameters.Parameters parameters;
        private System.Windows.Forms.TabPage balanceSheetTabPage;
        private Balance.Balance balance;
        private System.Windows.Forms.ToolStrip mainToolStrip;
        private System.Windows.Forms.ToolStripButton newToolStripButton;
        private System.Windows.Forms.ToolStripButton openToolStripButton;
        private System.Windows.Forms.ToolStripButton saveToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton toolStripButton4;
        private System.Windows.Forms.ToolStripButton copyToolStripButton;
        private System.Windows.Forms.ToolStripButton pasteToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton vectorManagerToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.ToolStripComboBox fontNameToolStripComboBox;
        private System.Windows.Forms.ToolStripComboBox fontSizeToolStripComboBox;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem applyScenarioTemplate;
        private System.Windows.Forms.ToolStripMenuItem saveScenarioTemplateToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pasteToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripMenuItem selectAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vectorManagerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem variableDefinitionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem otherItemsAssumptionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem taxAssumptionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dividendToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem optionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem currenciesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog saveScenarioFileDialog;
        private System.Windows.Forms.OpenFileDialog openScenarioFileDialog;
        private System.Windows.Forms.ToolStripButton computeResultsStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripMenuItem openWorkspacetoolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveWorkspaceToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog workspaceOpenFileDialog;
        private System.Windows.Forms.SaveFileDialog workspaceSaveFileDialog;
        private System.Windows.Forms.ToolStripMenuItem clearAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openLastToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showBankALMHelpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dynamicVectorsToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton multiScenarioToolStripButton;
    }
}