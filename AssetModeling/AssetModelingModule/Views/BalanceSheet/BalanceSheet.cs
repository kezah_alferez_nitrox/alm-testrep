using System;
using System.Collections;
using System.ComponentModel;
using System.Configuration;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Text;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using ALMSCommon;
using ALMSCommon.Forms;
using AssetModelingModule.BusinessLogic;
using AssetModelingModule.Calculus;
using AssetModelingModule.Core.IO;
using AssetModelingModule.Domain;
using AssetModelingModule.Properties;
using AssetModelingModule.Resources.Language;
using AssetModelingModule.Views.BalanceSheet.Result;
using AssetModelingModule.Views.Generator;
using AssetModelingModule.Views.Options.FxRates;
using AssetModelingModule.Views.Variables;
using AssetModelingModule.Views.Vectors;

namespace AssetModelingModule.Views.BalanceSheet
{
    public partial class BalanceSheet : Form, IModuleMainForm
    {
        private bool restart = false;
        private string[] args;

        public bool Restart
        {
            get { return this.restart; }
        }


        public static DateTime ValuationDate = DateTime.Now;

        private string currentFilePath;
        private string currentScenarioFilePath;

        private readonly string lastFilePath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) +
                                               "\\SOGALMS\\lastFiles.txt";

        public static bool modifiedScenarioTemplate = false;
        public static bool modifiedValues = false;

        private readonly IBalanceSerializer balanceSerializer;
        private readonly IScenarioSerializer scenarioTemplate;

        private void AddOpenLast(string filename, string newfilename)
        {
            ArrayList arrText = new ArrayList();
            if (File.Exists(this.lastFilePath))
            {
                string sline = string.Empty;

                StreamReader _sr = new StreamReader(this.lastFilePath);
                while (sline != null)
                {
                    sline = _sr.ReadLine();
                    if (!string.IsNullOrEmpty(sline))
                        arrText.Add(sline);
                }

                _sr.Close();
                _sr.Dispose();
                File.Delete(this.lastFilePath);
            }
            StreamWriter _sw = File.CreateText(this.lastFilePath);
            _sw.WriteLine(filename);
            if (arrText.Count > 0)
            {
                if (arrText.Contains(filename))
                    arrText.Remove(filename);
                if (arrText.Contains(newfilename))
                    arrText.Remove(newfilename);
                foreach (string str in arrText)
                    _sw.WriteLine(str);
            }
            _sw.Flush();
            _sw.Close();
            _sw.Dispose();
            this.RefreshOpenLast();
        }

        public BalanceSheet()
        {
            this.Font = Constants.Fonts.MainFont;

            Initialiser.Initialise();

            this.InitializeComponent();

            this.balanceSerializer = new BalanceXmlSerializer();
            this.scenarioTemplate = new ScenarioXmlSerializer();
            InstalledFontCollection fontCollection = new InstalledFontCollection();
            foreach (FontFamily fontFamily in fontCollection.Families)
            {
                this.fontNameToolStripComboBox.Items.Add(fontFamily.Name);
            }

            this.parameters.ValuationCurrencyChanged += this.balance.Parameters_ValuationCurrencyChanged;
            this.Shown += new EventHandler(BalanceSheet_Shown);
            Constants.BalanceSheetForm = this;
        }

        public BalanceSheet(string[] args)
            : this()
        {
            this.args = args;
        }

        void BalanceSheet_Shown(object sender, EventArgs e)
        {
            OpenPassedWorkSpace();
        }

        private void OpenPassedWorkSpace()
        {
            if (args != null && args.Length > 0)
            {
                string fileName = args[0];
                OpenFile(fileName);
            }
        }

        public void OpenFile(string fileName)
        {
            if (File.Exists(fileName))
            {
                string extension = Path.GetExtension(fileName);
                if (extension == ".amw")
                {
                    this.OpenWorkspace(fileName);
                }
                else if (extension == ".amm")
                {
                    this.ImportPortfolio(fileName);
                }
                else if (extension == ".ams")
                {
                    this.LoadScenario(fileName);
                }
            }
        }

        public string CurrentFilePath
        {
            get { return this.currentFilePath; }
            set { this.currentFilePath = value; }
        }


        private string currentWorkspacePath;

        public string CurrentWorkspacePath
        {
            get { return this.currentWorkspacePath; }
            set
            {
                this.currentWorkspacePath = value;
                string currentFile = Path.GetFileNameWithoutExtension(value);

                if (string.IsNullOrEmpty(currentFile)) currentFile = "New";

                this.Text = currentFile;
                Param.FileName = currentFile;
            }
        }

        public string CurrentScenarioFilePath
        {
            get { return this.currentScenarioFilePath; }
            set
            {
                this.currentScenarioFilePath = value;
            }
        }

        private void mainTabControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            PersistenceSession.Flush();

            BalanceBL.AjustBalanceIfNeeded();

            int selectedIndex = ((TabControl)sender).SelectedIndex;

            if (selectedIndex == 0)
            {
                this.balance.RefreshSelectedCategory();
            }
            else if (selectedIndex == 3)
            {
                this.multiScenarioResult.Reload();
            }
        }

        private string _TemplateFileName = string.Empty;
        private MultiScenarioResult multiScenarioResult;
        private GeneratorResultNode[] generatorResultNodes;
        private MultiScenarioParameters multiScenarioParameters;
        private int[] setSetupColumnsOrder;

        private void New()
        {
            if (MessageBox.Show(Properties.Resources.NewFilePrompt, "", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) !=
                DialogResult.OK)
                return;

            this.CurrentFilePath = string.Empty;

            if (this._TemplateFileName != string.Empty)
            {
                new CategoryXmlSerializer().Load(this._TemplateFileName);
            }

            BalanceBL.EnsureSpecialCategoriesAndBondsExist();

            PersistenceSession.Flush();

            this.result.InvalidateResult();
        }

        private void Save()
        {
            if (string.IsNullOrEmpty(this.CurrentFilePath))
            {
                if (this.saveFileDialog.ShowDialog() == DialogResult.OK)
                {
                    this.CurrentFilePath = this.saveFileDialog.FileName;
                }
            }

            this.SaveInternal();
        }

        private void SaveAs()
        {
            if (this.saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                this.CurrentFilePath = this.saveFileDialog.FileName;
                this.SaveInternal();
            }
        }

        protected void SaveInternal()
        {
            if (string.IsNullOrEmpty(this.CurrentFilePath)) return;

            PersistenceSession.Flush();

            try
            {
                this.balanceSerializer.Save(this.CurrentFilePath);
            }
            catch (Exception ex)
            {
                Log.Exception("balanceSerializer.Save(CurrentFilePath);", ex);
                MessageBox.Show(Labels.BalanceSheet_SaveInternal_Unable_to_save_file_ + this.CurrentFilePath);
                return;
            }
            modifiedValues = false;
        }

        private void Open()
        {
            if (modifiedValues)
                if (MessageBox.Show(Properties.Resources.NewFilePrompt, "", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) !=
                    DialogResult.OK)
                    return;

            if (this.openFileDialog.ShowDialog() != DialogResult.OK) return;

            ImportPortfolio(this.openFileDialog.FileName);
        }

        private void ImportPortfolio(string fileName)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.CurrentFilePath = fileName;

                try
                {
                    this.balanceSerializer.Load(this.CurrentFilePath);
                    BalanceBL.EnsureSpecialCategoriesAndBondsExist();
                }
                catch (Exception ex)
                {
                    Log.Exception("balanceSerializer.Load(CurrentFilePath);", ex);
                    MessageBox.Show(Labels.BalanceSheet_ImportPortfolio_Unable_to_open_file_ + this.CurrentFilePath);
                    return;
                }

                PersistenceSession.Flush();

                this.balance.Reload();
                modifiedValues = false;

                this.result.InvalidateResult();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void ClipboardCopy()
        {
            if (this.mainTabControl.SelectedTab == this.balanceSheetTabPage)
            {
                this.balance.ClipboardCopy();
            }
            else if (this.mainTabControl.SelectedTab == this.resultTabPage)
            {
                this.result.ClipboardCopy();
            }
        }

        private void ClipboardPaste()
        {
            if (this.mainTabControl.SelectedTab == this.balanceSheetTabPage)
            {
                this.balance.ClipboardPaste();
            }
        }

        private void vectorManagerToolStripButton_Click(object sender, EventArgs e)
        {
            this.ShowVectorManager();
        }

        private void ShowVectorManager()
        {
            Tools.ShowDialogWithPrivateSession(new VectorManager());

            BalanceBL.AjustBalanceIfNeeded();

            this.balance.RefreshSelectedCategory();

            this.RefreshParameters();
        }

        private void copyToolStripButton_Click(object sender, EventArgs e)
        {
            this.ClipboardCopy();
        }

        private void pasteToolStripButton_Click(object sender, EventArgs e)
        {
            this.ClipboardPaste();
        }

        private void openToolStripButton_Click(object sender, EventArgs e)
        {
            //Open();
            this.openWorkspacetoolStripMenuItem_Click(sender, e);
        }

        private void saveToolStripButton_Click(object sender, EventArgs e)
        {
            //Save();
            this.saveWorkspaceToolStripMenuItem_Click(sender, e);
        }

        public void RefreshResult()
        {
            this.result.RefreshResultGrid();
        }

        public void RefreshParameters()
        {
            this.parameters.RefreshParameters();
        }

        private void fontNameToolStripComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.UpdateFormFont();
        }

        private void fontSizeToolStripComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.UpdateFormFont();
        }

        private void UpdateFormFont()
        {
            string fontName = this.fontNameToolStripComboBox.SelectedItem as string;
            string fontSizeString = this.fontSizeToolStripComboBox.SelectedItem as string ?? "";
            float fontSize;
            if (!string.IsNullOrEmpty(fontName) &&
                (float.TryParse(fontSizeString, out fontSize) ||
                 float.TryParse(fontSizeString.Replace('.', ','), out fontSize)))
            {
                try
                {
                    Font f = new Font(fontName, fontSize);
                    this.Font = f;
                    this.Refresh();
                }
                catch (Exception ex)
                {
                    Log.Warn(ex);
                }
            }
        }

        public void OnValuationCurrencyChange()
        {
            this.balance.OnFxRatesChange();
            this.parameters.InitFxRates();
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Open();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Save();
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.SaveAs();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.restart = false;
            this.Close();
        }

        private void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.ClipboardCopy();
        }

        private void pasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.ClipboardPaste();
        }

        private void vectorManagerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.ShowVectorManager();
        }

        private void variableDefinitionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Tools.ShowDialogWithPrivateSession(new VariableDefinition()) == DialogResult.OK)
            {
                this.RefreshParameters();
            }
        }

        private void otherItemsAssumptionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Tools.ShowDialogWithPrivateSession(new OtherItemsAssumptions()) == DialogResult.OK)
            {
                // RefreshResult();
            }
        }

        private void taxAssumptionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Tools.ShowDialogWithPrivateSession(new TaxAssumptions());
        }

        private void dividendToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Tools.ShowDialogWithPrivateSession(new DividendForm());
        }

        private void currenciesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Tools.ShowDialogWithPrivateSession(new FxRatesEditor());

            this.OnValuationCurrencyChange();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ((new About())).ShowDialog();
        }

        private void saveScenarioTemplateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.saveScenarioFileDialog.ShowDialog() == DialogResult.OK)
            {
                this.currentScenarioFilePath = this.saveScenarioFileDialog.FileName;

                PersistenceSession.Flush();

                this.Cursor = Cursors.WaitCursor;
                try
                {
                    this.scenarioTemplate.Save(this.currentScenarioFilePath);
                    modifiedScenarioTemplate = false;
                }
                catch (Exception ex)
                {
                    Log.Exception("scenarioTemplate.Save(currentScenarioFilePath);", ex);
                    MessageBox.Show(Labels.BalanceSheet_saveScenarioTemplateToolStripMenuItem_Click_Unable_to_save_scenario_ + this.currentScenarioFilePath);
                    return;
                }
                finally
                {
                    this.Cursor = Cursors.Default;
                }
            }
        }

        private void applyScenarioTemplate_Click(object sender, EventArgs e)
        {
            if (modifiedScenarioTemplate)
            {
                if (MessageBox.Show(Properties.Resources.NewFilePrompt, "", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) !=
                    DialogResult.OK)
                    return;
            }

            if (this.openScenarioFileDialog.ShowDialog() != DialogResult.OK) return;

            LoadScenario(this.openScenarioFileDialog.FileName);
        }

        private void LoadScenario(string fileName)
        {
            this.currentScenarioFilePath = fileName;

            this.Cursor = Cursors.WaitCursor;

            try
            {
                this.scenarioTemplate.Load(this.currentScenarioFilePath);
                modifiedScenarioTemplate = false;
            }
            catch (Exception ex)
            {
                Log.Exception("scenarioTemplate.Load(currentScenarioFilePath);", ex);
                MessageBox.Show(Labels.BalanceSheet_LoadScenario_Unable_to_apply_scenario_ + this.currentScenarioFilePath);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

            this.parameters.RefreshParameters();
            this.result.InvalidateResult();
        }

        private void BalanceSheet_Load(object sender, EventArgs e)
        {
            string path = Constants.TEMPLATE_FILE_DIRECTORY_PATH;

            if (!Directory.Exists(path)) return;

            string[] files = Directory.GetFiles(path, "*.xml");
            foreach (string filepath in files)
            {
                string filename = filepath.Replace(path, string.Empty);
                if (filename.EndsWith(".xml"))
                {
                    string _name = filename.Replace(".xml", string.Empty);
                    ToolStripMenuItem _newToolStrip = new ToolStripMenuItem("With " + _name + " Template");
                    this.newToolStripMenuItem.DropDownItems.Add(_newToolStrip);
                    _newToolStrip.Tag = filepath;
                    _newToolStrip.Click += this._newToolStrip_Click;
                }
            }
            Param param = Param.FindByID("FileName");
            this.currentFilePath = "New ALW";
            if (param != null)
                this.currentFilePath = Param.FileName;


            this.Text = this.currentFilePath;
            this.RefreshOpenLast();
        }

        private void _newToolStrip_Click(object sender, EventArgs e)
        {
            this._TemplateFileName = (string)((ToolStripMenuItem)sender).Tag;
            this.New();
            this.balance.Reload();
        }

        private void computeResultsStripButton_Click(object sender, EventArgs e)
        {
            this.balance.BeforeRunSimulation();
            this.result.BeforeRunSimulation();

            PersistenceSession.Flush();

            BalanceBL.AjustBalance();

            LoadForm.Instance.DoWork(new WorkParameters(this.result.RunSimulation, this.SimulationEnd), null);
        }

        public void SimulationEnd()
        {
            if (this.result.SimulationEnd())
            {
                this.mainTabControl.SelectedIndex = 2;
            }
        }

        public void FindAndSelectBond(Bond bond, string property)
        {
            this.mainTabControl.SelectedIndex = 0;
            this.balance.FindAndSelectBond(bond, property);
        }

        public void FindAndSelectVariableValue(VariableValue value)
        {
            this.mainTabControl.SelectedIndex = 1;
            this.parameters.FindAndSelectVariableValue(value);
        }

        private void BalanceSheet_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!this.restart)
            {
                PersistenceSession.Flush();
            }

            if (modifiedValues)
            {
                if (
                    MessageBox.Show(Labels.BalanceSheet_BalanceSheet_FormClosing_Are_you_sure_you_want_to_quit___There_are_unsaved_Balance_Sheet_modifications, "",
                                    MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                {
                    e.Cancel = true;
                    return;
                }
            }

            if (modifiedScenarioTemplate)
            {
                if (
                    MessageBox.Show(Labels.BalanceSheet_BalanceSheet_FormClosing_Are_you_sure_you_want_to_quit___There_are_unsaved_Scenario_Setup_modifications, "",
                                    MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                {
                    e.Cancel = true;
                    return;
                }
            }
        }

        private void saveWorkspaceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.workspaceSaveFileDialog.ShowDialog() != DialogResult.OK) return;

            PersistenceSession.Flush();

            if (this.multiScenarioResult != null)
            {
                this.generatorResultNodes = this.multiScenarioResult.GetGeneratorResult();
            }

            int[] scenarioSetupColumnsOrder = this.parameters.GetScenarioSetupColumnsOrder();

            WorkspaceXmlSerializer workspaceSerializer = new WorkspaceXmlSerializer(this.generatorResultNodes, scenarioSetupColumnsOrder);
            this.Cursor = Cursors.WaitCursor;
            try
            {
                workspaceSerializer.Filename = this.workspaceSaveFileDialog.FileName;
                TimeSpan ts = new TimeSpan(0, 0,
                                           int.Parse(ConfigurationManager.AppSettings["LAST_SAVE_DURATION"]));
                LoadForm.Instance.DoWork(new WorkParameters(workspaceSerializer.Save, null), ts);
                ConfigurationManager.AppSettings["LAST_SAVE_DURATION"] =
                    LoadForm.Instance.WorkDurationInSeconds.ToString();
                modifiedValues = false;
                modifiedScenarioTemplate = false;

                this.AddOpenLast(this.currentWorkspacePath, this.workspaceSaveFileDialog.FileName);
                this.CurrentWorkspacePath = this.workspaceSaveFileDialog.FileName;
            }
            catch (Exception ex)
            {
                Log.Exception("workspaceSerializer.Save(workspaceSaveFileDialog.FileName);", ex);
                MessageBox.Show(Labels.BalanceSheet_saveWorkspaceToolStripMenuItem_Click_Unable_to_save_workspace_file_ + this.CurrentFilePath);
                return;
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void RefreshOpenLast()
        {
            this.openLastToolStripMenuItem.DropDownItems.Clear();

            if (!File.Exists(this.lastFilePath)) return;

            string sline = string.Empty;
            using (StreamReader _sr = new StreamReader(this.lastFilePath))
            {
                int i = 0;
                while (sline != null)
                {
                    sline = _sr.ReadLine();
                    if (!string.IsNullOrEmpty(sline))
                    {
                        if (i < 5)
                        {
                            ToolStripMenuItem _toolStripMenuItem = new ToolStripMenuItem();
                            _toolStripMenuItem.Text = sline;
                            _toolStripMenuItem.Click += this._toolStripMenuItem_Click;
                            this.openLastToolStripMenuItem.DropDownItems.Add(_toolStripMenuItem);
                            i++;
                        }
                    }
                }
                _sr.Close();
            }
        }

        private void _toolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.OpenWorkspace(((ToolStripMenuItem)sender).Text);
        }


        private void openWorkspacetoolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (modifiedValues)
            {
                if (MessageBox.Show(Properties.Resources.NewFilePrompt, "", MessageBoxButtons.OKCancel,
                                    MessageBoxIcon.Question) != DialogResult.OK)
                    return;
            }

            if (modifiedScenarioTemplate)
            {
                if (MessageBox.Show(Properties.Resources.NewFilePrompt, "", MessageBoxButtons.OKCancel,
                                    MessageBoxIcon.Question) != DialogResult.OK)
                    return;
            }


            if (this.workspaceOpenFileDialog.ShowDialog() != DialogResult.OK) return;

            this.Cursor = Cursors.WaitCursor;

            PersistenceSession.Flush();

            this.OpenWorkspace(this.workspaceOpenFileDialog.FileName);
        }

        private void OpenWorkspace(string fileName)
        {
            WorkspaceXmlSerializer workspaceSerializer = new WorkspaceXmlSerializer();
            workspaceSerializer.Filename = fileName;
            TimeSpan ts = new TimeSpan(0, 0, int.Parse(ConfigurationManager.AppSettings["LAST_LOAD_DURATION"]));
            LoadForm.Instance.DoWork(
                new WorkParameters(bw => this.LoadWorkspace(workspaceSerializer, bw), this.SimulationOpenWorkspaceEnd),
                ts);
            this.AddOpenLast(this.currentWorkspacePath, fileName);
            this.CurrentWorkspacePath = fileName;
        }

        private void LoadWorkspace(WorkspaceXmlSerializer serializer, BackgroundWorker worker)
        {
            serializer.Load(worker);
            this.generatorResultNodes = serializer.GeneratorResultNodes;
            this.setSetupColumnsOrder = serializer.ScenarioSetupColumnsOrder;
        }

        public void SimulationOpenWorkspaceEnd()
        {
            try
            {
                ConfigurationManager.AppSettings["LAST_LOAD_DURATION"] =
                    LoadForm.Instance.WorkDurationInSeconds.ToString();
                BalanceBL.EnsureSpecialCategoriesAndBondsExist();

                PersistenceSession.StartNew();

                modifiedScenarioTemplate = false;
                modifiedValues = false;
            }
            catch (Exception ex)
            {
                Log.Exception("workspaceSerializer.Load(workspaceOpenFileDialog.FileName);", ex);
                MessageBox.Show(Labels.BalanceSheet_SimulationOpenWorkspaceEnd_Unable_to_load_workspace_ + this.currentScenarioFilePath);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            this.parameters.RefreshParameters();
            this.balance.Reload();

            this.result.InvalidateResult();

            this.parameters.SetSetupColumnsOrder(this.setSetupColumnsOrder);

            if (this.generatorResultNodes != null && this.generatorResultNodes.Length > 0)
            {
                this.ShowMultiScenarioResult();
                this.multiScenarioResult.SetGeneratorResult(generatorResultNodes);
            }
            else
            {
                if (this.mainTabControl.TabPages.Count > 3)
                {
                    this.mainTabControl.TabPages.RemoveAt(3);
                    this.multiScenarioResult = null;
                }
            }
        }

        private void clearALLStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(Properties.Resources.ClearAllPrompt, "", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) !=
                DialogResult.OK)
                return;
            Initialiser.DeleteDataBaseFile();
            Initialiser.EnsureDataBaseExists();
            this.AddOpenLast(this.currentWorkspacePath, this.workspaceSaveFileDialog.FileName);
            this.CurrentWorkspacePath = "New Workspace";
            //Application.Restart();
            this.restart = true;
            this.Close();
        }

        private void BalanceSheet_HelpRequested(object sender, HelpEventArgs hlpevent)
        {
            string helpFile = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location),
                                           "sogalms.chm");
            Process.Start(helpFile);
        }

        private void showBankALMHelpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BalanceSheet_HelpRequested(sender, null);
        }

        private void dynamicVectorsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowDynamicVectorManager();
        }

        private static void ShowDynamicVectorManager()
        {
            Tools.ShowDialogWithPrivateSession(new DynamicVectorManager());
        }

        private void multiScenarioToolStripButton_Click(object sender, EventArgs e)
        {
            if (this.multiScenarioParameters == null)
            {
                this.multiScenarioParameters = new MultiScenarioParameters();
            }

            if (Tools.ShowDialogWithPrivateSession(this.multiScenarioParameters) != DialogResult.Cancel)
            {
                this.RefreshScenarioGenerator(this.multiScenarioParameters.Simulation);
            }
        }

        private void RefreshScenarioGenerator(Simulation simulation)
        {
            this.ShowMultiScenarioResult();
            this.multiScenarioResult.Reload(simulation);
        }

        private void ShowMultiScenarioResult()
        {
            if (this.multiScenarioResult == null)
            {
                this.multiScenarioResult = new MultiScenarioResult();
                this.multiScenarioResult.Dock = DockStyle.Fill;
                TabPage page = new TabPage();
                page.Text = Labels.BalanceSheet_ShowMultiScenarioResult_Multi_scenario_Simulation_Result;
                page.Controls.Add(this.multiScenarioResult);
                this.mainTabControl.TabPages.Add(page);
            }

            this.mainTabControl.SelectedIndex = 3;
        }

        public string[] Args
        {
            get { return this.args; }
            set { args = value; }
        }
    }
}