using System;
using System.Windows.Forms;
using AssetModelingModule.Domain;

namespace AssetModelingModule.Views.BalanceSheet.Balance
{
    public partial class AddNewBondsPrompt : Form
    {
        public AddNewBondsPrompt()
        {
            InitializeComponent();
            listView1.Items[0].Selected = true;
        }

        public int RowCount
        {
            get { return (int) rowCountNumericUpDown.Value; }
        }

        public BondFinancialType Type
        {
            get { return (BondFinancialType) Enum.Parse(typeof (BondFinancialType), listView1.SelectedItems[0].Text); }
        }

        private void AddNewRowsPrompt_Load(object sender, EventArgs e)
        {
            rowCountNumericUpDown.Focus();
            rowCountNumericUpDown.Select(0, rowCountNumericUpDown.Text.Length);
        }

        private void OkButtonClick(object sender, EventArgs e)
        {
            Close();
        }

        private void ListView1DoubleClick(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count != 0)
            {
                DialogResult = DialogResult.OK;
                Close();
            }
        }
    }
}