using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using ALMSCommon;
using AssetModelingModule.BusinessLogic;
using AssetModelingModule.Controls;
using AssetModelingModule.Core;
using AssetModelingModule.Domain;
using AssetModelingModule.Properties;
using AssetModelingModule.Resources.Language;
using AssetModelingModule.Views.BalanceSheet.Balance.BondDetail;
using Castle.ActiveRecord;
using Eu.AlterSystems.ASNetLib.Core;

namespace AssetModelingModule.Views.BalanceSheet.Balance
{
    public class DetailsGridController
    {
        private const int DECIMAL_CELL_WIDTH = 70;
        private const string TheoreticalCurrFaceColumnName = "TheoreticalCurrFace";
        private const string CPRColumnName = "CPR";
        private const string CDRColumnName = "CDR";
        private const string LGDColumnName = "LGD";
        private const string CREATE_NEW = "Create New";

        private readonly DataGridView grid;
        private ComboBox ChachedComboBox;

        private DataGridViewTextBoxColumn aLMIDDataGridViewTextBoxColumn;
        private DataGridViewComboBoxColumn accountingDataGridViewComboBoxColumn;
        private DataGridViewComboBoxColumn attribDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn basel2DataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn bookPriceDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn bookValueDataGridViewTextBoxColumn;
        private Category category;
        private AutoCompleteColumn cdrColumn;
        private DataGridViewColumn[] columns;
        private List<DataGridViewColumn> copyAbleColumns;
        private AutoCompleteColumn couponColumn;
        private AutoCompleteColumn cprColumn;
        private AutoCompleteColumn currFaceColumn;
        private DataGridViewComboBoxColumn currencyDataGridViewComboBoxColumn;
        private DataGridViewTextBoxColumn descriptionDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn durationDataGridViewTextBoxColumn;
        private DataGridViewCheckBoxColumn excludeDataGridViewCheckBoxColumn;
        private AutoCompleteColumn growthDataGridViewTextBoxColumn;
        private DataGridViewCheckBoxColumn isModelColumn;
        private AutoCompleteColumn lgdColumn;
        private AutoCompleteColumn marketPriceColumn;
        private DataGridViewTextBoxColumn origFaceDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn paymentDataGridViewTextBoxColumn;
        private DataGridViewComboBoxColumn periodDataGridViewComboBoxColumn;
        private DataGridViewComboBoxColumn rollSpecDataGridViewComboBoxColumn;
        private DataGridViewTextBoxColumn rwaDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn secIdDataGridViewTextBoxColumn;
        private DataGridViewImageColumn statusDataGridViewTextBoxColumn;
        private DataGridViewComboBoxColumn taxDataGridViewComboBoxColumn;
        private DataGridViewTextBoxColumn tciDataGridViewTextBoxColumn;
        private AutoCompleteColumn theoreticalCurrFaceColumn;
        private DataGridViewComboBoxColumn typeDataGridViewComboBoxColumn;
        private DataGridViewTextBoxColumn maturityDataGridViewTextBoxColumn;
        private DataGridViewComboBoxColumn typeIdDataGridViewComboBoxColumn;
        private DataGridViewComboBoxColumn dayCountConventionDataGridViewComboBoxColumn;
        private DataGridViewTextBoxColumn issuedBookPriceDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn reofferedBookPriceDataGridViewTextBoxColumn;
        private string[] variableAndVectorNames;

        public DetailsGridController(DataGridView grid)
        {
            this.grid = grid;
            grid.AutoGenerateColumns = false;
            grid.AllowUserToAddRows = false;
            grid.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
            grid.RowTemplate.Height = Constants.GRID_ROW_HEIGHT;

            InitColumns();

            this.grid.UserDeletingRow += grid_UserDeletingRow;
            this.grid.UserDeletedRow += grid_UserDeletedRow;
            this.grid.CellBeginEdit += grid_CellBeginEdit;
            this.grid.CellValueChanged += grid_CellValueChanged;
            this.grid.CellFormatting += grid_CellFormatting;
            this.grid.CellPainting += grid_CellPainting;
            this.grid.DataError += grid_DataError;
            this.grid.ColumnHeaderMouseClick += grid_ColumnHeaderMouseClick;
            this.grid.CurrentCellDirtyStateChanged += grid_CurrentCellDirtyStateChanged;
            this.grid.EditingControlShowing += grid_EditingControlShowing;
            this.grid.CellParsing += grid_CellParsing;
            this.grid.CellEndEdit += grid_CellEndEdit;
        }

        private SortableBindingList<Bond> BondList
        {
            get { return grid.DataSource as SortableBindingList<Bond>; }
            set { grid.DataSource = value; }
        }

        private void grid_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            ChachedComboBox = null;
        }


        private void grid_CellParsing(object sender, DataGridViewCellParsingEventArgs e)
        {
            if (grid.Columns[e.ColumnIndex].Name == "Basel2" || grid.Columns[e.ColumnIndex].Name == "Growth")
            {
                if (e.Value != null)
                {
                    string value = e.Value.ToString().Trim().Replace(" ", "");
                    bool percent = value.EndsWith("%");
                    value = value.Replace("%", "");

                    decimal parsedValue;
                    e.ParsingApplied = decimal.TryParse(value, out parsedValue);
                    if (e.ParsingApplied)
                    {
                        if (percent) parsedValue /= 100;
                        e.Value = parsedValue;
                    }
                }
            }
        }


        private void grid_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control is ComboBox)
            {
                e.Control.Enter += DetailsGridController_Enter;
                if ((e.Control as ComboBox).Items.Contains(CREATE_NEW))
                {
                    ChachedComboBox = (ComboBox) e.Control;
                }
            }
        }

        private static void DetailsGridController_Enter(object sender, EventArgs e)
        {
            var comboBox = (ComboBox) sender;
            if (comboBox.Visible) comboBox.DroppedDown = true;
        }

        private void InitColumns()
        {
            UpdateVariableAndVectorNameList();

            excludeDataGridViewCheckBoxColumn = new DataGridViewCheckBoxColumn();
            statusDataGridViewTextBoxColumn = new DataGridViewImageColumn();
            secIdDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            descriptionDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            typeDataGridViewComboBoxColumn = new DataGridViewComboBoxColumn();
            growthDataGridViewTextBoxColumn = new AutoCompleteColumn();
            origFaceDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            currFaceColumn = new AutoCompleteColumn();
            bookPriceDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            bookValueDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            marketPriceColumn = new AutoCompleteColumn();
            aLMIDDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            rollSpecDataGridViewComboBoxColumn = new DataGridViewComboBoxColumn();
            couponColumn = new AutoCompleteColumn();
            paymentDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            attribDataGridViewTextBoxColumn = new DataGridViewComboBoxColumn();
            accountingDataGridViewComboBoxColumn = new DataGridViewComboBoxColumn();
            periodDataGridViewComboBoxColumn = new DataGridViewComboBoxColumn();
            durationDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            taxDataGridViewComboBoxColumn = new DataGridViewComboBoxColumn();
            isModelColumn = new DataGridViewCheckBoxColumn();
            lgdColumn = new AutoCompleteColumn();
            theoreticalCurrFaceColumn = new AutoCompleteColumn();
            cprColumn = new AutoCompleteColumn();
            cdrColumn = new AutoCompleteColumn();
            basel2DataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            currencyDataGridViewComboBoxColumn = new DataGridViewComboBoxColumn();
            tciDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            rwaDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            maturityDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            typeIdDataGridViewComboBoxColumn = new DataGridViewComboBoxColumn();
            dayCountConventionDataGridViewComboBoxColumn = new DataGridViewComboBoxColumn();
            issuedBookPriceDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            reofferedBookPriceDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            // 
            // excludeDataGridViewCheckBoxColumn
            // 
            excludeDataGridViewCheckBoxColumn.DataPropertyName = "Exclude";
            excludeDataGridViewCheckBoxColumn.HeaderText = Labels.DetailsGridController_InitColumns_Exclude;
            excludeDataGridViewCheckBoxColumn.Name = "Exclude";
            excludeDataGridViewCheckBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            excludeDataGridViewCheckBoxColumn.Width = 40;
            // 
            // statusDataGridViewTextBoxColumn
            // 
            statusDataGridViewTextBoxColumn.DataPropertyName = "Status";
            statusDataGridViewTextBoxColumn.HeaderText = Labels.DetailsGridController_InitColumns_Status;
            statusDataGridViewTextBoxColumn.Name = "Status";
            statusDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            statusDataGridViewTextBoxColumn.Width = 40;
            // 
            // secIdDataGridViewTextBoxColumn
            // 
            secIdDataGridViewTextBoxColumn.DataPropertyName = "SecId";
            secIdDataGridViewTextBoxColumn.HeaderText = Labels.DetailsGridController_InitColumns_SecId;
            secIdDataGridViewTextBoxColumn.Name = "SecId";
            secIdDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            secIdDataGridViewTextBoxColumn.Width = 40;
            // 
            // descriptionDataGridViewTextBoxColumn
            // 
            descriptionDataGridViewTextBoxColumn.DataPropertyName = "Description";
            descriptionDataGridViewTextBoxColumn.HeaderText = Labels.DetailsGridController_InitColumns_Description;
            descriptionDataGridViewTextBoxColumn.Name = "Description";
            descriptionDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            // 
            // typeDataGridViewTextBoxColumn
            // 
            typeDataGridViewComboBoxColumn.DataPropertyName = "Type";
            typeDataGridViewComboBoxColumn.HeaderText = Labels.DetailsGridController_InitColumns_Type;
            typeDataGridViewComboBoxColumn.Name = "Type";
            typeDataGridViewComboBoxColumn.DisplayMember = "Value";
            typeDataGridViewComboBoxColumn.ValueMember = "Key";
            typeDataGridViewComboBoxColumn.DataSource = EnumHelper.ToList(typeof (BondType));
            typeDataGridViewComboBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            typeDataGridViewComboBoxColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;
            // 
            // growthDataGridViewTextBoxColumn
            // 
            growthDataGridViewTextBoxColumn.DataPropertyName = "Growth";
            growthDataGridViewTextBoxColumn.HeaderText = Labels.DetailsGridController_InitColumns_Growth;
            growthDataGridViewTextBoxColumn.Name = "Growth";
            growthDataGridViewTextBoxColumn.DefaultCellStyle.Format = Constants.MONEY_CELL_FORMAT;
            growthDataGridViewTextBoxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            growthDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            growthDataGridViewTextBoxColumn.Width = DECIMAL_CELL_WIDTH;
            growthDataGridViewTextBoxColumn.ValueList = variableAndVectorNames;

            // 
            // origFaceDataGridViewTextBoxColumn
            // 
            origFaceDataGridViewTextBoxColumn.DataPropertyName = "OrigFace";
            origFaceDataGridViewTextBoxColumn.HeaderText = Labels.DetailsGridController_InitColumns_Orig_Face;
            origFaceDataGridViewTextBoxColumn.Name = "OrigFace";
            origFaceDataGridViewTextBoxColumn.DefaultCellStyle.Format = Constants.MONEY_CELL_FORMAT;
            origFaceDataGridViewTextBoxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            origFaceDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            origFaceDataGridViewTextBoxColumn.Width = DECIMAL_CELL_WIDTH;
            // 
            // currFaceDataGridViewTextBoxColumn
            // 
            currFaceColumn.DataPropertyName = "CurrFace";
            currFaceColumn.HeaderText = Labels.DetailsGridController_InitColumns_Curr_Face;
            currFaceColumn.Name = "CurrFace";
            currFaceColumn.DefaultCellStyle.Format = Constants.MONEY_CELL_FORMAT;
            currFaceColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            currFaceColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            currFaceColumn.Width = DECIMAL_CELL_WIDTH;
            // 
            // bookPriceDataGridViewTextBoxColumn
            // 
            bookPriceDataGridViewTextBoxColumn.DataPropertyName = "BookPrice";
            bookPriceDataGridViewTextBoxColumn.HeaderText = Labels.DetailsGridController_InitColumns_Book_Price;
            bookPriceDataGridViewTextBoxColumn.Name = "BookPrice";
            bookPriceDataGridViewTextBoxColumn.DefaultCellStyle.Format = "N4";
            bookPriceDataGridViewTextBoxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            bookPriceDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            bookPriceDataGridViewTextBoxColumn.Width = 45;
            // 
            // bookValueDataGridViewTextBoxColumn
            // 
            bookValueDataGridViewTextBoxColumn.DataPropertyName = "BookValue";
            bookValueDataGridViewTextBoxColumn.HeaderText = Labels.DetailsGridController_InitColumns_Book_Value;
            bookValueDataGridViewTextBoxColumn.Name = "BookValue";
            bookValueDataGridViewTextBoxColumn.DefaultCellStyle.Format = Constants.MONEY_CELL_FORMAT;
            bookValueDataGridViewTextBoxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            bookValueDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            bookValueDataGridViewTextBoxColumn.Width = DECIMAL_CELL_WIDTH + 15;
            // 
            // marketPriceDataGridViewTextBoxColumn
            // 
            marketPriceColumn.DataPropertyName = "MarketPrice";
            marketPriceColumn.HeaderText = Labels.DetailsGridController_InitColumns_Market_Price;
            marketPriceColumn.Name = "Market Price";
            marketPriceColumn.DefaultCellStyle.Format = Constants.MONEY_CELL_FORMAT;
            marketPriceColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            marketPriceColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            marketPriceColumn.Width = DECIMAL_CELL_WIDTH;
            marketPriceColumn.ValueList = variableAndVectorNames;
            // 
            // aLMIDDataGridViewTextBoxColumn
            // 
            aLMIDDataGridViewTextBoxColumn.DataPropertyName = "ALMIDDisplayed";
            aLMIDDataGridViewTextBoxColumn.HeaderText = Labels.DetailsGridController_InitColumns_ID;
            aLMIDDataGridViewTextBoxColumn.Name = "ALMID";
            aLMIDDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            aLMIDDataGridViewTextBoxColumn.ReadOnly = true;
            aLMIDDataGridViewTextBoxColumn.DefaultCellStyle.BackColor = Color.LightGray;
            aLMIDDataGridViewTextBoxColumn.Width = 30;
            // 
            // rollSpecDataGridViewTextBoxColumn
            // 
            rollSpecDataGridViewComboBoxColumn.DataPropertyName = "RollSpec";
            rollSpecDataGridViewComboBoxColumn.HeaderText = Labels.DetailsGridController_InitColumns_Roll_Spec;
            rollSpecDataGridViewComboBoxColumn.Name = "RollSpec";
            rollSpecDataGridViewComboBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            rollSpecDataGridViewComboBoxColumn.Width = 60;
            rollSpecDataGridViewComboBoxColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;


            // 
            // couponDataGridViewTextBoxColumn
            // 
            couponColumn.DataPropertyName = "Coupon";
            couponColumn.HeaderText = Labels.DetailsGridController_InitColumns_Coupon;
            couponColumn.Name = "Coupon";
            couponColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            couponColumn.Width = 60;
            couponColumn.ValueList = variableAndVectorNames;
            // 
            // paymentDataGridViewTextBoxColumn
            // 
            paymentDataGridViewTextBoxColumn.DataPropertyName = "Payment";
            paymentDataGridViewTextBoxColumn.HeaderText = Labels.DetailsGridController_InitColumns_Payment;
            paymentDataGridViewTextBoxColumn.Name = "Payment";
            paymentDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            paymentDataGridViewTextBoxColumn.Width = 60;
            // 
            // attribDataGridViewTextBoxColumn
            // 
            attribDataGridViewTextBoxColumn.DataPropertyName = "Attrib";
            attribDataGridViewTextBoxColumn.HeaderText = Labels.DetailsGridController_InitColumns_Attrib;
            attribDataGridViewTextBoxColumn.Name = "Attrib";
            attribDataGridViewTextBoxColumn.DisplayMember = "Value";
            attribDataGridViewTextBoxColumn.ValueMember = "Key";
            attribDataGridViewTextBoxColumn.DataSource = EnumHelper.ToList(typeof (BondAttrib));
            attribDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            attribDataGridViewTextBoxColumn.Width = 65;
            attribDataGridViewTextBoxColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;
            // 
            // taxDataGridViewTextBoxColumn
            // 
            taxDataGridViewComboBoxColumn.DataPropertyName = "Tax";
            taxDataGridViewComboBoxColumn.HeaderText = Labels.DetailsGridController_InitColumns_Tax;
            taxDataGridViewComboBoxColumn.Name = "Tax";
            taxDataGridViewComboBoxColumn.DisplayMember = "Value";
            taxDataGridViewComboBoxColumn.ValueMember = "Key";
            taxDataGridViewComboBoxColumn.DataSource = EnumHelper.ToList(typeof (BondTax));
            taxDataGridViewComboBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            taxDataGridViewComboBoxColumn.Width = 70;
            taxDataGridViewComboBoxColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;
            // 
            // accountingDataGridViewTextBoxColumn
            // 
            accountingDataGridViewComboBoxColumn.DataPropertyName = "Accounting";
            accountingDataGridViewComboBoxColumn.HeaderText = Labels.DetailsGridController_InitColumns_Accounting;
            accountingDataGridViewComboBoxColumn.Name = "Accounting";
            accountingDataGridViewComboBoxColumn.DisplayMember = "Value";
            accountingDataGridViewComboBoxColumn.ValueMember = "Key";
            accountingDataGridViewComboBoxColumn.DataSource = EnumHelper.ToList(typeof (BondAccounting));
            accountingDataGridViewComboBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            accountingDataGridViewComboBoxColumn.Width = 60;
            accountingDataGridViewComboBoxColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;
            // 
            // periodDataGridViewTextBoxColumn
            // 
            periodDataGridViewComboBoxColumn.DataPropertyName = "Period";
            periodDataGridViewComboBoxColumn.HeaderText = Labels.DetailsGridController_InitColumns_Period;
            periodDataGridViewComboBoxColumn.Name = "Period";
            periodDataGridViewComboBoxColumn.DisplayMember = "Value";
            periodDataGridViewComboBoxColumn.ValueMember = "Key";
            periodDataGridViewComboBoxColumn.DataSource = EnumHelper.ToList(typeof (BondPeriod));
            periodDataGridViewComboBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            periodDataGridViewComboBoxColumn.Width = 70;
            periodDataGridViewComboBoxColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;
            // 
            // durationDataGridViewTextBoxColumn
            // 
            durationDataGridViewTextBoxColumn.DataPropertyName = "Duration";
            durationDataGridViewTextBoxColumn.HeaderText = Labels.DetailsGridController_InitColumns_Duration;
            durationDataGridViewTextBoxColumn.Name = "Duration";
            durationDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            durationDataGridViewTextBoxColumn.DefaultCellStyle.Format = Constants.DURATION_CELL_FORMAT;
            durationDataGridViewTextBoxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            durationDataGridViewTextBoxColumn.Width = 50;
            // 
            // isModelColumn
            // 
            isModelColumn.DataPropertyName = "IsModel";
            isModelColumn.HeaderText = Labels.DetailsGridController_InitColumns_Model;
            isModelColumn.Name = "Model";
            isModelColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            isModelColumn.Width = 40;
            // 
            // theoreticalCurrFaceColumn
            // 
            theoreticalCurrFaceColumn.DataPropertyName = "TheoreticalCurrFace";
            theoreticalCurrFaceColumn.HeaderText = Labels.DetailsGridController_InitColumns_Theoretical_Curr_Face;
            theoreticalCurrFaceColumn.Name = TheoreticalCurrFaceColumnName;
            theoreticalCurrFaceColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            theoreticalCurrFaceColumn.DefaultCellStyle.Format = Constants.MONEY_CELL_FORMAT;
            theoreticalCurrFaceColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            theoreticalCurrFaceColumn.Width = 70;
            theoreticalCurrFaceColumn.ValueList = variableAndVectorNames;
            // 
            // cprColumn
            // 
            cprColumn.DataPropertyName = "CPR";
            cprColumn.HeaderText = Labels.DetailsGridController_InitColumns_CPR;
            cprColumn.Name = CPRColumnName;
            cprColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            cprColumn.DefaultCellStyle.Format = Constants.MONEY_CELL_FORMAT;
            cprColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            cprColumn.Width = 70;
            cprColumn.ValueList = variableAndVectorNames;
            // 
            // cdrColumn
            // 
            cdrColumn.DataPropertyName = "CDR";
            cdrColumn.HeaderText = Labels.DetailsGridController_InitColumns_CDR;
            cdrColumn.Name = CDRColumnName;
            cdrColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            cdrColumn.DefaultCellStyle.Format = Constants.MONEY_CELL_FORMAT;
            cdrColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            cdrColumn.Width = 70;
            cdrColumn.ValueList = variableAndVectorNames;
            // 
            // lgdColumn
            // 
            lgdColumn.DataPropertyName = "LGD";
            lgdColumn.HeaderText = Labels.DetailsGridController_InitColumns_LGD;
            lgdColumn.Name = LGDColumnName;
            lgdColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            lgdColumn.DefaultCellStyle.Format = Constants.MONEY_CELL_FORMAT;
            lgdColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            lgdColumn.ValueList = variableAndVectorNames;
            lgdColumn.Width = 70;
            // 
            // basel2DataGridViewTextBoxColumn
            // 
            basel2DataGridViewTextBoxColumn.DataPropertyName = "Basel2";
            basel2DataGridViewTextBoxColumn.HeaderText = Labels.DetailsGridController_InitColumns_Basel_II;
            basel2DataGridViewTextBoxColumn.Name = "Basel2";
            basel2DataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            basel2DataGridViewTextBoxColumn.DefaultCellStyle.Format = Constants.PERCENTAGE_CELL_FORMAT;
            basel2DataGridViewTextBoxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            basel2DataGridViewTextBoxColumn.Width = 50;
            // 
            // currencyDataGridViewComboBoxColumn
            // 
            currencyDataGridViewComboBoxColumn.DataPropertyName = "Currency";
            currencyDataGridViewComboBoxColumn.HeaderText = Labels.DetailsGridController_InitColumns_Currency;
            currencyDataGridViewComboBoxColumn.Name = "Currency";
            currencyDataGridViewComboBoxColumn.DisplayMember = "Symbol";
            currencyDataGridViewComboBoxColumn.ValueMember = "Self";
            currencyDataGridViewComboBoxColumn.DataSource = ActiveRecordBase<Currency>.FindAll();
            currencyDataGridViewComboBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            currencyDataGridViewComboBoxColumn.Width = 55;
            currencyDataGridViewComboBoxColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;
            // 
            // tciDataGridViewTextBoxColumn
            // 
            tciDataGridViewTextBoxColumn.DataPropertyName = "TCI";
            tciDataGridViewTextBoxColumn.HeaderText = Labels.DetailsGridController_InitColumns_TCI;
            tciDataGridViewTextBoxColumn.Name = "TCI";
            tciDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            // 
            // rwaDataGridViewTextBoxColumn
            // 
            rwaDataGridViewTextBoxColumn.DataPropertyName = "RWA";
            rwaDataGridViewTextBoxColumn.HeaderText = Labels.DetailsGridController_InitColumns_RWA;
            rwaDataGridViewTextBoxColumn.Name = "RWA";
            rwaDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            rwaDataGridViewTextBoxColumn.Width = 100;
            rwaDataGridViewTextBoxColumn.ReadOnly = true;
            rwaDataGridViewTextBoxColumn.DefaultCellStyle.BackColor = Color.LightGray;
            rwaDataGridViewTextBoxColumn.DefaultCellStyle.Format = Constants.MONEY_CELL_FORMAT;
            rwaDataGridViewTextBoxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            // 
            // maturityDataGridViewTextBoxColumn
            // 
            maturityDataGridViewTextBoxColumn.DataPropertyName = "Maturity";
            maturityDataGridViewTextBoxColumn.HeaderText = Labels.DetailsGridController_InitColumns_Maturity;
            maturityDataGridViewTextBoxColumn.Name = "Maturity";
            maturityDataGridViewTextBoxColumn.ReadOnly = true;
            maturityDataGridViewTextBoxColumn.DefaultCellStyle.Format = "dd/MM/yyyy";
            maturityDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            maturityDataGridViewTextBoxColumn.Width = 70;
            // 
            // typeIdDataGridViewComboBoxColumn
            // 
            typeIdDataGridViewComboBoxColumn.DataPropertyName = "TypeId";
            typeIdDataGridViewComboBoxColumn.HeaderText = Labels.DetailsGridController_InitColumns_Type_Id;
            typeIdDataGridViewComboBoxColumn.Name = "TypeId";
            typeIdDataGridViewComboBoxColumn.DisplayMember = "Value";
            typeIdDataGridViewComboBoxColumn.ValueMember = "Key";
            typeIdDataGridViewComboBoxColumn.DataSource = EnumHelper.ToList(typeof(TypeId));
            typeIdDataGridViewComboBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            typeIdDataGridViewComboBoxColumn.Width = 70;
            typeIdDataGridViewComboBoxColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;
            // 
            // issuedBookPriceDataGridViewTextBoxColumn
            // 
            issuedBookPriceDataGridViewTextBoxColumn.DataPropertyName = "IssuedBookPrice";
            issuedBookPriceDataGridViewTextBoxColumn.HeaderText = Labels.DetailsGridController_InitColumns_Issued_Book_Price;
            issuedBookPriceDataGridViewTextBoxColumn.Name = "BookPrice";
            issuedBookPriceDataGridViewTextBoxColumn.DefaultCellStyle.Format = Constants.VECTOR_POINT_CELL_FORMAT;
            issuedBookPriceDataGridViewTextBoxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            issuedBookPriceDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            issuedBookPriceDataGridViewTextBoxColumn.Width = 100;
            // 
            // reofferedBookPriceDataGridViewTextBoxColumn
            // 
            reofferedBookPriceDataGridViewTextBoxColumn.DataPropertyName = "BookPrice";
            reofferedBookPriceDataGridViewTextBoxColumn.HeaderText = Labels.DetailsGridController_InitColumns_Reoffered_Book_Price;
            reofferedBookPriceDataGridViewTextBoxColumn.Name = "BookPrice";
            reofferedBookPriceDataGridViewTextBoxColumn.ReadOnly =true;
            reofferedBookPriceDataGridViewTextBoxColumn.DefaultCellStyle.Format = Constants.VECTOR_POINT_CELL_FORMAT;
            reofferedBookPriceDataGridViewTextBoxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            reofferedBookPriceDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            reofferedBookPriceDataGridViewTextBoxColumn.Width = 100;
            // 
            // dayCountConventionDataGridViewComboBoxColumn
            // 
            dayCountConventionDataGridViewComboBoxColumn.DataPropertyName = "DayCountConvention";
            dayCountConventionDataGridViewComboBoxColumn.HeaderText = Labels.DetailsGridController_InitColumns_Day_Count_Convention;
            dayCountConventionDataGridViewComboBoxColumn.Name = "DayCountConvention";
            dayCountConventionDataGridViewComboBoxColumn.DisplayMember = "Value";
            dayCountConventionDataGridViewComboBoxColumn.ValueMember = "Key";
            dayCountConventionDataGridViewComboBoxColumn.DataSource = EnumHelper.ToList(typeof(DayCountConvention));
            dayCountConventionDataGridViewComboBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            dayCountConventionDataGridViewComboBoxColumn.Width = 100;
            dayCountConventionDataGridViewComboBoxColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;

            columns = new DataGridViewColumn[]
                          {
                              excludeDataGridViewCheckBoxColumn,
                              statusDataGridViewTextBoxColumn,
                              secIdDataGridViewTextBoxColumn,
                              descriptionDataGridViewTextBoxColumn,
                              isModelColumn,
                              typeDataGridViewComboBoxColumn,
                              growthDataGridViewTextBoxColumn,
                              accountingDataGridViewComboBoxColumn,
                              origFaceDataGridViewTextBoxColumn,
                              currFaceColumn,
                              bookPriceDataGridViewTextBoxColumn,
                              bookValueDataGridViewTextBoxColumn,
                              marketPriceColumn,
                              aLMIDDataGridViewTextBoxColumn,
                              rollSpecDataGridViewComboBoxColumn,
                              couponColumn,
                              periodDataGridViewComboBoxColumn,
                              durationDataGridViewTextBoxColumn,
                              taxDataGridViewComboBoxColumn,
                              attribDataGridViewTextBoxColumn,
                              theoreticalCurrFaceColumn,
                              cprColumn,
                              cdrColumn,
                              lgdColumn,
                              basel2DataGridViewTextBoxColumn,
                              currencyDataGridViewComboBoxColumn,
                              tciDataGridViewTextBoxColumn,
                              rwaDataGridViewTextBoxColumn,
                              maturityDataGridViewTextBoxColumn,
                              typeIdDataGridViewComboBoxColumn,
                              issuedBookPriceDataGridViewTextBoxColumn,
                              reofferedBookPriceDataGridViewTextBoxColumn,
                              dayCountConventionDataGridViewComboBoxColumn,
                          };

            copyAbleColumns = new List<DataGridViewColumn>(new DataGridViewColumn[]
                                                               {
                                                                   descriptionDataGridViewTextBoxColumn,
                                                                   typeDataGridViewComboBoxColumn,
                                                                   growthDataGridViewTextBoxColumn,
                                                                   accountingDataGridViewComboBoxColumn,
                                                                   origFaceDataGridViewTextBoxColumn,
                                                                   currFaceColumn,
                                                                   bookPriceDataGridViewTextBoxColumn,
                                                                   bookValueDataGridViewTextBoxColumn,
                                                                   marketPriceColumn,
                                                                   rollSpecDataGridViewComboBoxColumn,
                                                                   couponColumn,
                                                                   periodDataGridViewComboBoxColumn,
                                                                   durationDataGridViewTextBoxColumn,
                                                                   taxDataGridViewComboBoxColumn,
                                                                   attribDataGridViewTextBoxColumn,
                                                                   theoreticalCurrFaceColumn,
                                                                   cprColumn,
                                                                   cdrColumn,
                                                                   lgdColumn,
                                                                   basel2DataGridViewTextBoxColumn,
                                                                   currencyDataGridViewComboBoxColumn,
                                                               });

            grid.Columns.Clear();
            grid.Columns.AddRange(columns);
        }

        private void UpdateVariableAndVectorNameList()
        {
            variableAndVectorNames = DomainTools.GetAllvariablesAndVectorNames();
        }


        private void grid_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (!grid.IsCurrentCellDirty) return;

            DataGridViewColumn column = grid.CurrentCell.OwningColumn;
            if (column is DataGridViewCheckBoxColumn ||
                column is DataGridViewComboBoxColumn)
            {
                grid.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }
        }

        private void grid_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (grid.Rows.Count == 0) return;

            grid.ClearSelection();
            for (int i = 0; i < grid.Rows.Count - 1; i++)
            {
                grid[e.ColumnIndex, i].Selected = true;
            }
        }

        private void grid_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (0 == grid.Rows.Count ||
                e.ColumnIndex < 0) return;

            string colName = grid.Columns[e.ColumnIndex].Name;

            if (IsTotalAtIndex(e.RowIndex) &&
                colName != "Description" &&
                colName != "BookValue" &&
                colName != "RWA")
            {
                e.Graphics.FillRectangle(new SolidBrush(e.CellStyle.BackColor), e.CellBounds);
                e.Graphics.DrawLine(new Pen(VisualStyleInformation.TextControlBorder),
                                    e.CellBounds.X, e.CellBounds.Y + e.CellBounds.Height - 1,
                                    e.CellBounds.X + e.CellBounds.Width - 1, e.CellBounds.Y + e.CellBounds.Height - 1);
                e.Handled = true;
            }
        }

        private void FormatGrowthCell(int rowIndex)
        {
            var bondType = (BondType) grid["Type", rowIndex].Value;

            DataGridViewCell growthCell = grid["Growth", rowIndex];
            growthCell.ReadOnly = (bondType != BondType.GROWTH);
            growthCell.Style.BackColor = growthCell.ReadOnly ? Color.LightGray : Color.White;
            if (growthCell.ReadOnly) growthCell.Value = null;
        }

        private void grid_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            string colName = grid.Columns[e.ColumnIndex].Name;

            if (colName == "Type")
            {
                FormatCurrFaceCell(e.RowIndex);
                FormatThCurrFaceCell(e.RowIndex);
                FormatGrowthCell(e.RowIndex);
            }

            if ("Exclude" == colName ||
                "BookValue" == colName ||
                "Basel2" == colName ||
                "Currency" == colName)
            {
                ComputeGridTotal(BondList);

                Bond bond = GetBondAtRowIndex(e.RowIndex);
                bond.UpdateStatusProperty();
                ResetRow(e.RowIndex);

                BalanceBL.BalanceAdjustmentNeeded = true;
            }

            if ("CurrFace" == colName ||
                "BookPrice" == colName)
            {
                Bond bond = GetBondAtRowIndex(e.RowIndex);
                if (!string.IsNullOrEmpty(bond.CurrFace) && bond.BookPrice.HasValue)
                {
                    bond.ComputeBookValue();
                    bond.UpdateStatusProperty();
                    if (null != BondList)
                    {
                        BondList.ResetItem(e.RowIndex);
                        ComputeGridTotal(BondList);
                    }
                }
                else
                {
                    bond.UpdateStatusProperty();
                    ResetRow(e.RowIndex);
                }

                BalanceBL.BalanceAdjustmentNeeded = true;
            }

            if (colName == "Model")
            {
                FormatModelCells(e.RowIndex);
            }

            if (colName == rollSpecDataGridViewComboBoxColumn.Name)
            {
                if (grid[e.ColumnIndex, e.RowIndex].Value.ToString() == CREATE_NEW)
                {
                    Category[] rollCategories = Category.FindByBalanceTypeAndCatType(category.BalanceType,
                                                                                     CategoryType.Roll);
                    if (rollCategories.Length == 0) return;

                    Category currentRollCat = rollCategories[0];
                    int maxALMID = Bond.GetMaxALMIDForCategory(currentRollCat);

                    var bond = new Bond();
                    bond.Category = currentRollCat;
                    bond.Currency = Param.ValuationCurrency;
                    bond.Duration = Constants.DEFAULT_BOND_DURATION;
                    bond.ALMID = maxALMID + 1;
                    bond.Attrib = BondAttrib.Roll;

                    bond.Create();
                    currentRollCat.Bonds.Add(bond);

                    PersistenceSession.Flush();

                    RefreshRollBonds();
                    grid[e.ColumnIndex, e.RowIndex].Value = bond.ALMIDDisplayed;
                    ChachedComboBox.Text = bond.ALMIDDisplayed;
                    grid.UpdateCellValue(e.ColumnIndex, e.RowIndex);
                }
            }

            BalanceSheet.modifiedValues = true;
        }

        private void FormatModelCells(int rowIndex)
        {
            var isModel = (bool) grid["Model", rowIndex].Value;

            FormatThCurrFaceCell(rowIndex);
            EnableCell(rowIndex, CPRColumnName, isModel);
            EnableCell(rowIndex, CDRColumnName, isModel);
            EnableCell(rowIndex, LGDColumnName, isModel);
        }

        private void EnableCell(int rowIndex, string colName, bool enabled)
        {
            DataGridViewCell cell = grid[colName, rowIndex];
            cell.ReadOnly = !enabled;
            cell.Style.BackColor = !enabled ? Color.LightGray : Color.White;
        }

        private void FormatCurrFaceCell(int rowIndex)
        {
            var bondType = (BondType) grid["Type", rowIndex].Value;

            ((AutoCompleteCell) grid["CurrFace", rowIndex]).ValueList =
                (bondType == BondType.VAR)
                    ? variableAndVectorNames
                    : new string[0];
        }

        private void FormatThCurrFaceCell(int rowIndex)
        {
            var bondType = (BondType) grid["Type", rowIndex].Value;
            var isModel = (bool) grid["Model", rowIndex].Value;

            EnableCell(rowIndex, TheoreticalCurrFaceColumnName, bondType == BondType.VAR && isModel);
        }


        public void ResetRow(int rowIndex)
        {
            if (null != BondList)
            {
                BondList.ResetItem(rowIndex);
            }
        }

        public void ComputeGridTotal()
        {
            ComputeGridTotal(BondList);
        }

        private static void ComputeGridTotal(SortableBindingList<Bond> gridData)
        {
            if (null == gridData ||
                0 == gridData.Count ||
                !gridData[gridData.Count - 1].IsTotal)
            {
                return;
            }

            decimal sumBookValue = 0;
            decimal sumRWA = 0;
            foreach (Bond bond in gridData)
            {
                if (!bond.Exclude && !bond.IsTotal && bond.Attrib != BondAttrib.Notional)
                {
                    sumBookValue +=
                        CurrencyBL.ConvertToValuationCurrency(bond.BookValue.HasValue ? bond.BookValue.Value : 0,
                                                              bond.Currency);
                    sumRWA += CurrencyBL.ConvertToValuationCurrency(bond.RWA.HasValue ? bond.RWA.Value : 0,
                                                                    bond.Currency);
                }
            }

            Bond total = gridData[gridData.Count - 1];
            total.BookValue = sumBookValue;
            total.RWA = sumRWA;
            gridData.ResetItem(gridData.Count - 1);
        }

        private static void AddTotalRow(SortableBindingList<Bond> gridData)
        {
            if (null == gridData || 0 == gridData.Count) return;

            var totalBond = new Bond();
            totalBond.IsTotal = true;
            totalBond.Description = GetTotalRowDescription();
            gridData.Add(totalBond);

            ComputeGridTotal(gridData);
        }

        private static string GetTotalRowDescription()
        {
            return string.Format(Labels.DetailsGridController_GetTotalRowDescription_TOTAL___0__, Param.ValuationCurrency.Symbol);
        }

        private void grid_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            e.Cancel = IsTotalAtIndex(e.RowIndex);
        }

        private bool IsTotalAtIndex(int rowIndex)
        {
            Bond bond = GetBondAtRowIndex(rowIndex);

            return (null == bond) ? false : bond.IsTotal;
        }

        private void grid_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            string colName = grid.Columns[e.ColumnIndex].Name;
            Bond bond = GetBondAtRowIndex(e.RowIndex);

            if (colName == "Growth")
            {
                FormatGrowthCell(e.RowIndex);
            }

            if (colName == "CurrFace")
            {
                FormatCurrFaceCell(e.RowIndex);
            }

            if (colName == CPRColumnName || colName == CDRColumnName || colName == LGDColumnName)
            {
                EnableCell(e.RowIndex, colName, bond.IsModel);
            }

            if (colName == TheoreticalCurrFaceColumnName)
            {
                EnableCell(e.RowIndex, TheoreticalCurrFaceColumnName, bond.Type == BondType.VAR && bond.IsModel);
            }

            if (colName == "Status")
            {
                switch (bond.Status)
                {
                    case Bond.BondStatus.None:
                        e.Value = Properties.Resources.gray_sphere;
                        break;
                    case Bond.BondStatus.OK:
                        e.Value = Properties.Resources.green_sphere;
                        break;
                    case Bond.BondStatus.Warning:
                        e.Value = Properties.Resources.yellow_sphere;
                        break;
                    case Bond.BondStatus.Error:
                        e.Value = Properties.Resources.red_sphere;
                        break;
                }
            }

            if (bond.IsTotal)
            {
                e.CellStyle.BackColor = Color.LemonChiffon;
                e.CellStyle.SelectionBackColor = e.CellStyle.BackColor;
                e.CellStyle.SelectionForeColor = e.CellStyle.ForeColor;
                e.CellStyle.Font = new Font(e.CellStyle.Font, FontStyle.Bold);
            }

            if (bond.BondFinancialType == BondFinancialType.Swap)
            {
                grid.Rows[e.RowIndex].DefaultCellStyle.BackColor = Constants.Colors.SwapLeg;
            }
        }

        private Bond GetBondAtRowIndex(int rowIndex)
        {
            if (0 > rowIndex) return null;

            return grid.Rows[rowIndex].DataBoundItem as Bond;
        }

        private static void grid_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            e.Cancel = false;
            e.ThrowException = false;

            Debug.WriteLine("Details Grid DataError: " + e.Exception);
        }

        private void grid_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            // todo: show confirmation

            var bond = e.Row.DataBoundItem as Bond;
            if (bond != null)
            {
                category.Bonds.Remove(bond);
                bond.Delete();

                BalanceBL.AjustBalanceIfNeeded();

                BalanceSheet.modifiedValues = true;
            }
        }

        private void grid_UserDeletedRow(object sender, DataGridViewRowEventArgs e)
        {
            CheckEmpryGrid();
        }

        public void DeleteRows(int menuRowIndex)
        {
            if (grid.SelectedRows.Count == 0)
            {
                DeleteRow(grid.Rows[menuRowIndex]);
            }
            else
            {
                foreach (DataGridViewRow row in grid.SelectedRows)
                {
                    DeleteRow(row);
                }
            }
            CheckEmpryGrid();

            BalanceBL.BalanceAdjustmentNeeded = true;
        }

        private void DeleteRow(DataGridViewRow row)
        {
            var bond = row.DataBoundItem as Bond;
            if ((bond != null && bond.IsTotal) || row.Index == -1) return;

            if (bond != null)
            {
                category.Bonds.Remove(bond);
                bond.Delete();
                if (bond.Attrib == BondAttrib.Roll)
                {
                    RemoveRollFromBonds(bond.ALMIDDisplayed);
                    RefreshRollBonds();
                }

                if (bond.BondFinancialType == BondFinancialType.Swap)
                {
                    DataGridViewRow gridViewRow = GetDataGridViewRowByBond(bond.SwapBondLink);
                    if (gridViewRow != null)
                    {
                        bond.SwapBondLink.SwapBondLink = null;
                        DeleteRow(gridViewRow);
                    }
                }
            }

            grid.Rows.Remove(row);
            BalanceSheet.modifiedValues = true;

            
        }

        private DataGridViewRow GetDataGridViewRowByBond(Bond bond)
        {
            foreach (DataGridViewRow row in grid.Rows)
            {
                if (row.DataBoundItem == bond)
                    return row;
            }

            return null;
        }

        private static void RemoveRollFromBonds(string rollId)
        {
            foreach (Bond bond in Bond.FindAll())
            {
                if (bond.RollSpec == rollId)
                {
                    bond.RollSpec = null;
                }
            }
        }

        private void CheckEmpryGrid()
        {
            if (grid.Rows.Count == 1) grid.Rows.Clear();
        }

        public void Refresh(Category newCategory, bool resetColumns)
        {
            if (resetColumns)
            {
                InitColumns();
            }

            bool categoryTypeChanged = category == null || newCategory == null ||
                                       category.BalanceType != newCategory.BalanceType;

            category = newCategory;

            if (null == category)
            {
                BondList = null;
                return;
            }

            if (category.IsNew)
            {
                return;
            }

            if (category.Type == CategoryType.Treasury || category.Type == CategoryType.CashAdj)
            {
                grid.RowTemplate.HeaderCell.ContextMenuStrip.Enabled = false;
            }
            else
            {
                grid.RowTemplate.HeaderCell.ContextMenuStrip.Enabled = true;
            }

            PersistenceSession.Flush();

            var gridData = new SortableBindingList<Bond>();

            category = Category.Find(category.Id);

            foreach (Bond bond in category.Bonds)
            {
                gridData.Add(bond);
            }

            AddTotalRow(gridData);

            BondList = gridData;

            if (category.Type != CategoryType.Normal)
            {
                attribDataGridViewTextBoxColumn.ReadOnly = true;
                attribDataGridViewTextBoxColumn.DataSource = EnumHelper.ToList(typeof (BondAttrib));

                excludeDataGridViewCheckBoxColumn.ReadOnly = true;
            }
            else
            {
                attribDataGridViewTextBoxColumn.ReadOnly = false;
                var list = new ArrayList();
                list.Add(new KeyValuePair<Enum, string>(BondAttrib.InBalance,
                                                        EnumHelper.GetDescription(BondAttrib.InBalance)));
                list.Add(new KeyValuePair<Enum, string>(BondAttrib.Notional,
                                                        EnumHelper.GetDescription(BondAttrib.Notional)));
                attribDataGridViewTextBoxColumn.DataSource = list;

                excludeDataGridViewCheckBoxColumn.ReadOnly = false;
            }

            if (category.Type == CategoryType.Treasury || category.Type == CategoryType.CashAdj)
            {
                grid.AllowUserToDeleteRows = false;
            }
            else
            {
                grid.AllowUserToDeleteRows = true;
            }
            if (category.Type == CategoryType.Roll)
            {
                origFaceDataGridViewTextBoxColumn.ReadOnly = true;
                origFaceDataGridViewTextBoxColumn.DefaultCellStyle.BackColor = Color.LightGray;
                currFaceColumn.ReadOnly = true;
                currFaceColumn.DefaultCellStyle.BackColor = Color.LightGray;
            }
            else
            {
                currFaceColumn.ReadOnly = false;
                currFaceColumn.DefaultCellStyle.BackColor = Color.White;
                origFaceDataGridViewTextBoxColumn.ReadOnly = false;
                origFaceDataGridViewTextBoxColumn.DefaultCellStyle.BackColor = Color.White;
            }

            if (resetColumns || categoryTypeChanged)
            {
                RefreshRollBonds();
            }
        }

        private void RefreshRollBonds()
        {
            Bond[] rollBonds = null;
            if (category != null)
                rollBonds = Bond.GetBondsByTypeAndAtrib(category.BalanceType, BondAttrib.Roll);
            var roll = new List<string>();
            roll.Add(CREATE_NEW);
            foreach (Bond bond in rollBonds)
            {
                roll.Add(bond.ALMIDDisplayed);
            }

            rollSpecDataGridViewComboBoxColumn.DataSource = roll;
        }

        public void AddNewBonds(int bondCount)
        {
            int maxALMID = Bond.GetMaxALMIDForCategory(category);

            for (int i = 0; i < bondCount; i++)
            {
                var bond = new Bond();
                bond.Category = category;
                bond.Currency = Param.ValuationCurrency;
                bond.Duration = Constants.DEFAULT_BOND_DURATION;
                bond.ALMID = maxALMID + i + 1;
                bond.Attrib = (category.Type == CategoryType.Roll) ? BondAttrib.Roll : BondAttrib.InBalance;

                bond.Create();

                category.Bonds.Add(bond);
            }

            Refresh(category, false);
        }

        public List<Bond> AddNewBonds(int bondCount, BondFinancialType type)
        {
            int maxALMID = Bond.GetMaxALMIDForCategory(category);
            List<Bond> addedBonds = new List<Bond>();

            for (int i = 0; i < bondCount; i++)
            {
                var bond = new Bond
                               {
                                   BondFinancialType = type,
                                   Category = category,
                                   Currency = Param.ValuationCurrency,
                                   Duration = Constants.DEFAULT_BOND_DURATION,
                                   ALMID = maxALMID + i + 1,
                                   Attrib =
                                       (category.Type == CategoryType.Roll) ? BondAttrib.Roll : BondAttrib.InBalance
                               };

                bond.Create();
                addedBonds.Add(bond);
                category.Bonds.Add(bond);

                if (bond.BondFinancialType == BondFinancialType.Swap)
                {
                    bond.SwapBondLink = CreateSwapBondLink();
                    bond.SwapBondLink.SwapBondLink = bond;
                    category.Bonds.Add(bond.SwapBondLink);
                }
            }

            Refresh(category, false);

            return addedBonds;
        }

        private Bond CreateSwapBondLink()
        {
            Bond bond = new Bond
                            {
                                BondFinancialType = BondFinancialType.Swap,
                                Category = category,
                                Currency = Param.ValuationCurrency,
                                Duration = Constants.DEFAULT_BOND_DURATION,
                                Attrib = (category.Type == CategoryType.Roll) ? BondAttrib.Roll : BondAttrib.InBalance,
                                Leg = BondSwapLeg.PayFloat,
                                ResetFrequence = BondPeriod.Quarterly
            };

            bond.Create();

            return bond;
        }

        public void ClipboardCopy()
        {
            if (grid.GetCellCount(DataGridViewElementStates.Selected) > 0)
            {
                DataObject data = grid.GetClipboardContent();
                if (data != null)
                {
                    Clipboard.SetDataObject(grid.GetClipboardContent());
                }
            }
        }

        public void ClipboadPaste()
        {
            string clipboardTxt = Clipboard.GetText();
            if (string.IsNullOrEmpty(clipboardTxt)) return;

            string[] clipboardRows = clipboardTxt.Split(new[] {Environment.NewLine},
                                                        StringSplitOptions.RemoveEmptyEntries);
            if (clipboardRows.Length == 0) return;

            bool insertRows;
            if (grid.CurrentCell == null || grid.CurrentCell.RowIndex < 0)
            {
                insertRows = true;
            }
            else
            {
                DialogResult dialogResult = (new PasteRowsPrompt()).ShowDialog();
                if (dialogResult == DialogResult.Cancel) return;

                insertRows = (dialogResult == DialogResult.Yes);
            }

            int startRowIndex;
            int startColIndex;
            if (insertRows)
            {
                startRowIndex = grid.Rows.Count == 0 ? 0 : grid.Rows.Count - 1;
                startColIndex = 0;
                AddNewBonds(clipboardRows.Length);
            }
            else
            {
                if (grid.CurrentCell == null) return;
                startRowIndex = grid.CurrentCell.RowIndex;
                startColIndex = grid.CurrentCell.ColumnIndex;
            }

            var clipboardCells = new List<string[]>();

            for (int i = 0; i < clipboardRows.Length; i++)
            {
                string[] row = clipboardRows[i].Split(Constants.VALUE_SEPARATOR);
                clipboardCells.Add(row);
            }

            for (int i = 0; i < clipboardCells.Count; i++)
            {
                int rowNumber = startRowIndex + i;
                if (rowNumber >= grid.Rows.Count - 1) break;

                for (int j = 0; j < clipboardCells[i].Length; j++)
                {
                    int colNumber = startColIndex + j;
                    if (colNumber >= grid.Columns.Count) break;

                    if (grid.Columns[colNumber].Name == "ALMID"
                        || grid.Columns[colNumber].Name == "Status")
                        continue;

                    Type colType = grid.Columns[colNumber].ValueType;
                    string clipboardValue = clipboardCells[i][j];
                    grid[colNumber, rowNumber].Value = ClipboadToColumn(colType, clipboardValue);
                }

                Bond bond = GetBondAtRowIndex(rowNumber);
                bond.UpdateStatusProperty();
                ResetRow(rowNumber);
            }
        }

        private static object ClipboadToColumn(Type colType, string clipboardValue)
        {
            object colValue = null;

            if (colType.IsEnum)
            {
                foreach (KeyValuePair<Enum, string> pair in EnumHelper.ToList(colType))
                {
                    if (clipboardValue == pair.Value)
                    {
                        colValue = pair.Key;
                        break;
                    }
                }
                // colValue = Enum.Parse(colType, clipboardValue, true);
            }
            else if (colType == typeof (Currency))
            {
                colValue = Currency.FindFirstBySymbol(clipboardValue);
            }
            else if (colType == typeof (decimal?))
            {
                colValue = Tools.ClipboardValueToDecimal(clipboardValue);
            }
            else
            {
                colValue = clipboardValue;
            }

            return colValue;
        }

        public bool IsColumnCopyAble(int _columnIndex)
        {
            return copyAbleColumns.Contains(grid.Columns[_columnIndex]);
        }

        public void CurrenciesChanged()
        {
            Refresh(category, true);
            //if (grid.Rows.Count > 1)
            //{

            //    grid["Description", grid.Rows.Count - 1].Value = GetTotalRowDescription();
            //}

            //ComputeGridTotal();
        }

        public void SelectCellFor(Bond bond, string property)
        {
            if (!grid.Columns.Contains(property)) return;

            foreach (DataGridViewRow row in grid.Rows)
            {
                var rowBond = (Bond) row.DataBoundItem;
                if (rowBond.Id == bond.Id)
                {
                    grid.ClearSelection();
                    grid.FirstDisplayedScrollingRowIndex = row.Index;
                    DataGridViewCell cell = grid[property, row.Index];
                    if (cell != null)
                    {
                        cell.Selected = true;
                    }
                    return;
                }
            }
        }

        public bool IsModelRow(int rowIndex)
        {
            Bond bond = GetBondAtRowIndex(rowIndex);
            return bond == null ? false : bond.IsModel;
        }

        public Bond GetBondAtIndex(int rowIndex)
        {
            return GetBondAtRowIndex(rowIndex);
        }

        public void EndEdit()
        {
            grid.EndEdit();
        }

        public void EditBond(Bond editBond)
        {
            var bonds = new List<Bond>();
            for (int i = 0; i < grid.Rows.Count - 1; i++)
            {
                Bond bond = (Bond) grid.Rows[i].DataBoundItem;
                
                if (bond.BondFinancialType == BondFinancialType.Swap && (bond.Leg == BondSwapLeg.PayFloat || bond.Leg == BondSwapLeg.ReceivedFloat))
                    continue;

                bonds.Add(bond);
            }

            new BondNavigator(bonds, bonds.IndexOf(editBond)).ShowDialog();
        }
    }
}