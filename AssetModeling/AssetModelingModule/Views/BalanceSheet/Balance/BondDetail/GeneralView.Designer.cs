﻿using System.Windows.Forms;
using AssetModelingModule.Resources.Language;

namespace AssetModelingModule.Views.BalanceSheet.Balance.BondDetail
{
    partial class GeneralView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label excludeLabel;
            System.Windows.Forms.Label secIdLabel;
            System.Windows.Forms.Label descriptionLabel;
            System.Windows.Forms.Label typeLabel;
            System.Windows.Forms.Label statusLabel;
            System.Windows.Forms.Label accountingLabel;
            System.Windows.Forms.Label origFaceLabel;
            System.Windows.Forms.Label currFaceLabel;
            System.Windows.Forms.Label bookPriceLabel;
            System.Windows.Forms.Label bookValueLabel;
            System.Windows.Forms.Label marketPriceLabel;
            System.Windows.Forms.Label idLabel;
            System.Windows.Forms.Label periodLabel;
            System.Windows.Forms.Label durationLabel;
            System.Windows.Forms.Label taxLabel;
            System.Windows.Forms.Label attribLabel;
            System.Windows.Forms.Label basel2Label;
            System.Windows.Forms.Label tCILabel;
            System.Windows.Forms.Label rWALabel;
            System.Windows.Forms.Label label5;
            System.Windows.Forms.Label label6;
            System.Windows.Forms.Label label7;
            System.Windows.Forms.Label label8;
            System.Windows.Forms.Label label23;
            System.Windows.Forms.Label currencyLabel;
            System.Windows.Forms.Label couponLabel;
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tCITextBox = new System.Windows.Forms.TextBox();
            this.bondBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.periodComboBox = new System.Windows.Forms.ComboBox();
            this.couponTextBox = new System.Windows.Forms.TextBox();
            this.idTextBox = new System.Windows.Forms.TextBox();
            this.marketPriceTextBox = new System.Windows.Forms.TextBox();
            this.typeComboBox = new System.Windows.Forms.ComboBox();
            this.accountingComboBox = new System.Windows.Forms.ComboBox();
            this.secIdTextBox = new System.Windows.Forms.TextBox();
            this.excludeCheckBox = new System.Windows.Forms.CheckBox();
            this.descriptionTextBox = new System.Windows.Forms.TextBox();
            this.currencyComboBox = new System.Windows.Forms.ComboBox();
            this.attribComboBox = new System.Windows.Forms.ComboBox();
            this.taxComboBox = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.regexTextBox1 = new AssetModelingModule.Controls.RegexTextBox();
            this.regexTextBox2 = new AssetModelingModule.Controls.RegexTextBox();
            this.regexTextBox4 = new AssetModelingModule.Controls.RegexTextBox();
            this.regexTextBox5 = new AssetModelingModule.Controls.RegexTextBox();
            this.regexTextBox7 = new AssetModelingModule.Controls.RegexTextBox();
            this.regexTextBox6 = new AssetModelingModule.Controls.RegexTextBox();
            this.regexTextBoxIssuedBookPrice = new AssetModelingModule.Controls.RegexTextBox();
            excludeLabel = new System.Windows.Forms.Label();
            secIdLabel = new System.Windows.Forms.Label();
            descriptionLabel = new System.Windows.Forms.Label();
            typeLabel = new System.Windows.Forms.Label();
            statusLabel = new System.Windows.Forms.Label();
            accountingLabel = new System.Windows.Forms.Label();
            origFaceLabel = new System.Windows.Forms.Label();
            currFaceLabel = new System.Windows.Forms.Label();
            bookPriceLabel = new System.Windows.Forms.Label();
            bookValueLabel = new System.Windows.Forms.Label();
            marketPriceLabel = new System.Windows.Forms.Label();
            idLabel = new System.Windows.Forms.Label();
            periodLabel = new System.Windows.Forms.Label();
            durationLabel = new System.Windows.Forms.Label();
            taxLabel = new System.Windows.Forms.Label();
            attribLabel = new System.Windows.Forms.Label();
            basel2Label = new System.Windows.Forms.Label();
            tCILabel = new System.Windows.Forms.Label();
            rWALabel = new System.Windows.Forms.Label();
            label5 = new System.Windows.Forms.Label();
            label6 = new System.Windows.Forms.Label();
            label7 = new System.Windows.Forms.Label();
            label8 = new System.Windows.Forms.Label();
            label23 = new System.Windows.Forms.Label();
            currencyLabel = new System.Windows.Forms.Label();
            couponLabel = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bondBindingSource)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // excludeLabel
            // 
            excludeLabel.AutoSize = true;
            excludeLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            excludeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            excludeLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(137)))), ((int)(((byte)(94)))));
            excludeLabel.Location = new System.Drawing.Point(3, 26);
            excludeLabel.Name = "excludeLabel";
            excludeLabel.Size = new System.Drawing.Size(129, 30);
            excludeLabel.TabIndex = 0;
            excludeLabel.Text = Labels.GeneralView_InitializeComponent_Exclude_;
            excludeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // secIdLabel
            // 
            secIdLabel.AutoSize = true;
            secIdLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            secIdLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            secIdLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(137)))), ((int)(((byte)(94)))));
            secIdLabel.Location = new System.Drawing.Point(3, 83);
            secIdLabel.Name = "secIdLabel";
            secIdLabel.Size = new System.Drawing.Size(129, 26);
            secIdLabel.TabIndex = 2;
            secIdLabel.Text = "SecId:";
            secIdLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // descriptionLabel
            // 
            descriptionLabel.AutoSize = true;
            descriptionLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            descriptionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            descriptionLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(137)))), ((int)(((byte)(94)))));
            descriptionLabel.Location = new System.Drawing.Point(3, 0);
            descriptionLabel.Name = "descriptionLabel";
            descriptionLabel.Size = new System.Drawing.Size(129, 26);
            descriptionLabel.TabIndex = 6;
            descriptionLabel.Text = Labels.GeneralView_InitializeComponent_Description_;
            descriptionLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // typeLabel
            // 
            typeLabel.AutoSize = true;
            typeLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            typeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            typeLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(137)))), ((int)(((byte)(94)))));
            typeLabel.Location = new System.Drawing.Point(3, 109);
            typeLabel.Name = "typeLabel";
            typeLabel.Size = new System.Drawing.Size(129, 27);
            typeLabel.TabIndex = 8;
            typeLabel.Text = Labels.GeneralView_InitializeComponent_Type_;
            typeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // statusLabel
            // 
            statusLabel.AutoSize = true;
            statusLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            statusLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            statusLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(137)))), ((int)(((byte)(94)))));
            statusLabel.Location = new System.Drawing.Point(309, 26);
            statusLabel.Name = "statusLabel";
            statusLabel.Size = new System.Drawing.Size(129, 30);
            statusLabel.TabIndex = 10;
            statusLabel.Text = Labels.GeneralView_InitializeComponent_Status_;
            statusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // accountingLabel
            // 
            accountingLabel.AutoSize = true;
            accountingLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            accountingLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            accountingLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(137)))), ((int)(((byte)(94)))));
            accountingLabel.Location = new System.Drawing.Point(3, 136);
            accountingLabel.Name = "accountingLabel";
            accountingLabel.Size = new System.Drawing.Size(129, 27);
            accountingLabel.TabIndex = 11;
            accountingLabel.Text = Labels.GeneralView_InitializeComponent_Accounting_;
            accountingLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // origFaceLabel
            // 
            origFaceLabel.AutoSize = true;
            origFaceLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            origFaceLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            origFaceLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(137)))), ((int)(((byte)(94)))));
            origFaceLabel.Location = new System.Drawing.Point(3, 163);
            origFaceLabel.Name = "origFaceLabel";
            origFaceLabel.Size = new System.Drawing.Size(129, 26);
            origFaceLabel.TabIndex = 13;
            origFaceLabel.Text = Labels.GeneralView_InitializeComponent_Orig_Face_;
            origFaceLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // currFaceLabel
            // 
            currFaceLabel.AutoSize = true;
            currFaceLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            currFaceLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            currFaceLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(137)))), ((int)(((byte)(94)))));
            currFaceLabel.Location = new System.Drawing.Point(3, 189);
            currFaceLabel.Name = "currFaceLabel";
            currFaceLabel.Size = new System.Drawing.Size(129, 26);
            currFaceLabel.TabIndex = 15;
            currFaceLabel.Text = Labels.GeneralView_InitializeComponent_Curr_Face_;
            currFaceLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // bookPriceLabel
            // 
            bookPriceLabel.AutoSize = true;
            bookPriceLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            bookPriceLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            bookPriceLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(137)))), ((int)(((byte)(94)))));
            bookPriceLabel.Location = new System.Drawing.Point(3, 215);
            bookPriceLabel.Name = "bookPriceLabel";
            bookPriceLabel.Size = new System.Drawing.Size(129, 26);
            bookPriceLabel.TabIndex = 17;
            bookPriceLabel.Text = Labels.GeneralView_InitializeComponent_Issued_Book_Price_;
            bookPriceLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // bookValueLabel
            // 
            bookValueLabel.AutoSize = true;
            bookValueLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            bookValueLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            bookValueLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(137)))), ((int)(((byte)(94)))));
            bookValueLabel.Location = new System.Drawing.Point(3, 241);
            bookValueLabel.Name = "bookValueLabel";
            bookValueLabel.Size = new System.Drawing.Size(129, 26);
            bookValueLabel.TabIndex = 19;
            bookValueLabel.Text = Labels.GeneralView_InitializeComponent_Book_Value_;
            bookValueLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // marketPriceLabel
            // 
            marketPriceLabel.AutoSize = true;
            marketPriceLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            marketPriceLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            marketPriceLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(137)))), ((int)(((byte)(94)))));
            marketPriceLabel.Location = new System.Drawing.Point(3, 267);
            marketPriceLabel.Name = "marketPriceLabel";
            marketPriceLabel.Size = new System.Drawing.Size(129, 32);
            marketPriceLabel.TabIndex = 21;
            marketPriceLabel.Text = Labels.GeneralView_InitializeComponent_Market_Price_;
            marketPriceLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // idLabel
            // 
            idLabel.AutoSize = true;
            idLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            idLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            idLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(137)))), ((int)(((byte)(94)))));
            idLabel.Location = new System.Drawing.Point(309, 241);
            idLabel.Name = "idLabel";
            idLabel.Size = new System.Drawing.Size(129, 26);
            idLabel.TabIndex = 23;
            idLabel.Text = Labels.GeneralView_InitializeComponent_Id_;
            idLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // periodLabel
            // 
            periodLabel.AutoSize = true;
            periodLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            periodLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            periodLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(137)))), ((int)(((byte)(94)))));
            periodLabel.Location = new System.Drawing.Point(309, 267);
            periodLabel.Name = "periodLabel";
            periodLabel.Size = new System.Drawing.Size(129, 32);
            periodLabel.TabIndex = 29;
            periodLabel.Text = Labels.GeneralView_InitializeComponent_Period_;
            periodLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            periodLabel.Visible = false;
            // 
            // durationLabel
            // 
            durationLabel.AutoSize = true;
            durationLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            durationLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            durationLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(137)))), ((int)(((byte)(94)))));
            durationLabel.Location = new System.Drawing.Point(309, 83);
            durationLabel.Name = "durationLabel";
            durationLabel.Size = new System.Drawing.Size(129, 26);
            durationLabel.TabIndex = 31;
            durationLabel.Text = Labels.GeneralView_InitializeComponent_Duration_;
            durationLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // taxLabel
            // 
            taxLabel.AutoSize = true;
            taxLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            taxLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            taxLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(137)))), ((int)(((byte)(94)))));
            taxLabel.Location = new System.Drawing.Point(309, 109);
            taxLabel.Name = "taxLabel";
            taxLabel.Size = new System.Drawing.Size(129, 27);
            taxLabel.TabIndex = 33;
            taxLabel.Text = Labels.GeneralView_InitializeComponent_Tax_;
            taxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // attribLabel
            // 
            attribLabel.AutoSize = true;
            attribLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            attribLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            attribLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(137)))), ((int)(((byte)(94)))));
            attribLabel.Location = new System.Drawing.Point(309, 136);
            attribLabel.Name = "attribLabel";
            attribLabel.Size = new System.Drawing.Size(129, 27);
            attribLabel.TabIndex = 35;
            attribLabel.Text = Labels.GeneralView_InitializeComponent_Attrib_;
            attribLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // basel2Label
            // 
            basel2Label.AutoSize = true;
            basel2Label.Dock = System.Windows.Forms.DockStyle.Fill;
            basel2Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            basel2Label.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(137)))), ((int)(((byte)(94)))));
            basel2Label.Location = new System.Drawing.Point(309, 163);
            basel2Label.Name = "basel2Label";
            basel2Label.Size = new System.Drawing.Size(129, 26);
            basel2Label.TabIndex = 45;
            basel2Label.Text = Labels.GeneralView_InitializeComponent_Basel2_;
            basel2Label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tCILabel
            // 
            tCILabel.AutoSize = true;
            tCILabel.Dock = System.Windows.Forms.DockStyle.Fill;
            tCILabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tCILabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(137)))), ((int)(((byte)(94)))));
            tCILabel.Location = new System.Drawing.Point(309, 189);
            tCILabel.Name = "tCILabel";
            tCILabel.Size = new System.Drawing.Size(129, 26);
            tCILabel.TabIndex = 49;
            tCILabel.Text = "TCI:";
            tCILabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // rWALabel
            // 
            rWALabel.AutoSize = true;
            rWALabel.Dock = System.Windows.Forms.DockStyle.Fill;
            rWALabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            rWALabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(137)))), ((int)(((byte)(94)))));
            rWALabel.Location = new System.Drawing.Point(309, 215);
            rWALabel.Name = "rWALabel";
            rWALabel.Size = new System.Drawing.Size(129, 26);
            rWALabel.TabIndex = 51;
            rWALabel.Text = Labels.GeneralView_InitializeComponent_RWA_;
            rWALabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Dock = System.Windows.Forms.DockStyle.Fill;
            label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(137)))), ((int)(((byte)(94)))));
            label5.Location = new System.Drawing.Point(309, 56);
            label5.Name = "label5";
            label5.Size = new System.Drawing.Size(129, 31);
            label5.TabIndex = 43;
            label5.Text = Labels.GeneralView_InitializeComponent_LGD_;
            label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Dock = System.Windows.Forms.DockStyle.Fill;
            label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(137)))), ((int)(((byte)(94)))));
            label6.Location = new System.Drawing.Point(3, 56);
            label6.Name = "label6";
            label6.Size = new System.Drawing.Size(129, 31);
            label6.TabIndex = 41;
            label6.Text = Labels.GeneralView_InitializeComponent_CDR_;
            label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label7
            // 
            label7.AutoSize = true;
            label7.Dock = System.Windows.Forms.DockStyle.Fill;
            label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(137)))), ((int)(((byte)(94)))));
            label7.Location = new System.Drawing.Point(309, 30);
            label7.Name = "label7";
            label7.Size = new System.Drawing.Size(129, 26);
            label7.TabIndex = 39;
            label7.Text = Labels.GeneralView_InitializeComponent_CPR_;
            label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label8
            // 
            label8.AutoSize = true;
            label8.Dock = System.Windows.Forms.DockStyle.Fill;
            label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(137)))), ((int)(((byte)(94)))));
            label8.Location = new System.Drawing.Point(3, 30);
            label8.Name = "label8";
            label8.Size = new System.Drawing.Size(129, 26);
            label8.TabIndex = 37;
            label8.Text = Labels.GeneralView_InitializeComponent_Theoretical_Curr_Face_;
            label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label23
            // 
            label23.AutoSize = true;
            label23.Dock = System.Windows.Forms.DockStyle.Fill;
            label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label23.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(137)))), ((int)(((byte)(94)))));
            label23.Location = new System.Drawing.Point(3, 0);
            label23.Name = "label23";
            label23.Size = new System.Drawing.Size(129, 30);
            label23.TabIndex = 4;
            label23.Text = Labels.GeneralView_InitializeComponent_Model_;
            label23.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // currencyLabel
            // 
            currencyLabel.AutoSize = true;
            currencyLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            currencyLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            currencyLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(137)))), ((int)(((byte)(94)))));
            currencyLabel.Location = new System.Drawing.Point(3, 56);
            currencyLabel.Name = "currencyLabel";
            currencyLabel.Size = new System.Drawing.Size(129, 27);
            currencyLabel.TabIndex = 47;
            currencyLabel.Text = Labels.GeneralView_InitializeComponent_Currency_;
            currencyLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            currencyLabel.Visible = false;
            // 
            // couponLabel
            // 
            couponLabel.AutoSize = true;
            couponLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            couponLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            couponLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(137)))), ((int)(((byte)(94)))));
            couponLabel.Location = new System.Drawing.Point(309, 56);
            couponLabel.Name = "couponLabel";
            couponLabel.Size = new System.Drawing.Size(129, 27);
            couponLabel.TabIndex = 27;
            couponLabel.Text = Labels.GeneralView_InitializeComponent_Coupon_;
            couponLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            couponLabel.Visible = false;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(235)))), ((int)(((byte)(240)))));
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 22F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 28F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 22F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 28F));
            this.tableLayoutPanel1.Controls.Add(rWALabel, 2, 8);
            this.tableLayoutPanel1.Controls.Add(tCILabel, 2, 7);
            this.tableLayoutPanel1.Controls.Add(currencyLabel, 0, 2);
            this.tableLayoutPanel1.Controls.Add(basel2Label, 2, 6);
            this.tableLayoutPanel1.Controls.Add(attribLabel, 2, 5);
            this.tableLayoutPanel1.Controls.Add(taxLabel, 2, 4);
            this.tableLayoutPanel1.Controls.Add(durationLabel, 2, 3);
            this.tableLayoutPanel1.Controls.Add(periodLabel, 2, 10);
            this.tableLayoutPanel1.Controls.Add(couponLabel, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.couponTextBox, 3, 2);
            this.tableLayoutPanel1.Controls.Add(idLabel, 2, 9);
            this.tableLayoutPanel1.Controls.Add(marketPriceLabel, 0, 10);
            this.tableLayoutPanel1.Controls.Add(this.marketPriceTextBox, 1, 10);
            this.tableLayoutPanel1.Controls.Add(bookValueLabel, 0, 9);
            this.tableLayoutPanel1.Controls.Add(bookPriceLabel, 0, 8);
            this.tableLayoutPanel1.Controls.Add(currFaceLabel, 0, 7);
            this.tableLayoutPanel1.Controls.Add(origFaceLabel, 0, 6);
            this.tableLayoutPanel1.Controls.Add(accountingLabel, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.typeComboBox, 1, 4);
            this.tableLayoutPanel1.Controls.Add(typeLabel, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.accountingComboBox, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.secIdTextBox, 1, 3);
            this.tableLayoutPanel1.Controls.Add(secIdLabel, 0, 3);
            this.tableLayoutPanel1.Controls.Add(statusLabel, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.excludeCheckBox, 1, 1);
            this.tableLayoutPanel1.Controls.Add(excludeLabel, 0, 1);
            this.tableLayoutPanel1.Controls.Add(descriptionLabel, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.descriptionTextBox, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.regexTextBox1, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.regexTextBox2, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.regexTextBox4, 1, 9);
            this.tableLayoutPanel1.Controls.Add(this.regexTextBox5, 3, 3);
            this.tableLayoutPanel1.Controls.Add(this.currencyComboBox, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.attribComboBox, 3, 5);
            this.tableLayoutPanel1.Controls.Add(this.taxComboBox, 3, 4);
            this.tableLayoutPanel1.Controls.Add(this.regexTextBoxIssuedBookPrice, 1, 8);
            this.tableLayoutPanel1.Controls.Add(this.regexTextBox6, 3, 6);
            this.tableLayoutPanel1.Controls.Add(this.tCITextBox, 3, 7);
            this.tableLayoutPanel1.Controls.Add(this.regexTextBox7, 3, 8);
            this.tableLayoutPanel1.Controls.Add(this.idTextBox, 3, 9);
            this.tableLayoutPanel1.Controls.Add(this.periodComboBox, 3, 10);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableLayoutPanel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(137)))), ((int)(((byte)(94)))));
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 11;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(614, 299);
            this.tableLayoutPanel1.TabIndex = 30;
            // 
            // tCITextBox
            // 
            this.tCITextBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(182)))), ((int)(((byte)(64)))));
            this.tCITextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bondBindingSource, "TCI", true));
            this.tCITextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tCITextBox.Location = new System.Drawing.Point(444, 192);
            this.tCITextBox.Name = "tCITextBox";
            this.tCITextBox.Size = new System.Drawing.Size(100, 20);
            this.tCITextBox.TabIndex = 50;
            // 
            // bondBindingSource
            // 
            this.bondBindingSource.DataSource = typeof(AssetModelingModule.Domain.Bond);
            // 
            // periodComboBox
            // 
            this.periodComboBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(182)))), ((int)(((byte)(64)))));
            this.periodComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.bondBindingSource, "Period", true));
            this.periodComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.periodComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.periodComboBox.FormattingEnabled = true;
            this.periodComboBox.Location = new System.Drawing.Point(444, 270);
            this.periodComboBox.Name = "periodComboBox";
            this.periodComboBox.Size = new System.Drawing.Size(100, 21);
            this.periodComboBox.TabIndex = 30;
            this.periodComboBox.Visible = false;
            // 
            // couponTextBox
            // 
            this.couponTextBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(182)))), ((int)(((byte)(64)))));
            this.couponTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bondBindingSource, "Coupon", true));
            this.couponTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.couponTextBox.Location = new System.Drawing.Point(444, 59);
            this.couponTextBox.Name = "couponTextBox";
            this.couponTextBox.Size = new System.Drawing.Size(100, 20);
            this.couponTextBox.TabIndex = 28;
            this.couponTextBox.Visible = false;
            // 
            // idTextBox
            // 
            this.idTextBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(182)))), ((int)(((byte)(64)))));
            this.idTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bondBindingSource, "BondId", true));
            this.idTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.idTextBox.Location = new System.Drawing.Point(444, 244);
            this.idTextBox.Name = "idTextBox";
            this.idTextBox.Size = new System.Drawing.Size(100, 20);
            this.idTextBox.TabIndex = 24;
            // 
            // marketPriceTextBox
            // 
            this.marketPriceTextBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(182)))), ((int)(((byte)(64)))));
            this.marketPriceTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bondBindingSource, "MarketPrice", true));
            this.marketPriceTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.marketPriceTextBox.Location = new System.Drawing.Point(138, 270);
            this.marketPriceTextBox.Name = "marketPriceTextBox";
            this.marketPriceTextBox.Size = new System.Drawing.Size(100, 20);
            this.marketPriceTextBox.TabIndex = 22;
            // 
            // typeComboBox
            // 
            this.typeComboBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(182)))), ((int)(((byte)(64)))));
            this.typeComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.bondBindingSource, "Type", true));
            this.typeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.typeComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.typeComboBox.FormattingEnabled = true;
            this.typeComboBox.Location = new System.Drawing.Point(138, 112);
            this.typeComboBox.Name = "typeComboBox";
            this.typeComboBox.Size = new System.Drawing.Size(100, 21);
            this.typeComboBox.TabIndex = 9;
            // 
            // accountingComboBox
            // 
            this.accountingComboBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(182)))), ((int)(((byte)(64)))));
            this.accountingComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.bondBindingSource, "Accounting", true));
            this.accountingComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.accountingComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.accountingComboBox.FormattingEnabled = true;
            this.accountingComboBox.Location = new System.Drawing.Point(138, 139);
            this.accountingComboBox.Name = "accountingComboBox";
            this.accountingComboBox.Size = new System.Drawing.Size(100, 21);
            this.accountingComboBox.TabIndex = 12;
            // 
            // secIdTextBox
            // 
            this.secIdTextBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(182)))), ((int)(((byte)(64)))));
            this.secIdTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bondBindingSource, "SecId", true));
            this.secIdTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.secIdTextBox.Location = new System.Drawing.Point(138, 86);
            this.secIdTextBox.Name = "secIdTextBox";
            this.secIdTextBox.Size = new System.Drawing.Size(100, 20);
            this.secIdTextBox.TabIndex = 3;
            // 
            // excludeCheckBox
            // 
            this.excludeCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.bondBindingSource, "Exclude", true));
            this.excludeCheckBox.Location = new System.Drawing.Point(138, 29);
            this.excludeCheckBox.Name = "excludeCheckBox";
            this.excludeCheckBox.Size = new System.Drawing.Size(104, 24);
            this.excludeCheckBox.TabIndex = 1;
            this.excludeCheckBox.UseVisualStyleBackColor = true;
            // 
            // descriptionTextBox
            // 
            this.descriptionTextBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(182)))), ((int)(((byte)(64)))));
            this.descriptionTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bondBindingSource, "Description", true));
            this.descriptionTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.descriptionTextBox.Location = new System.Drawing.Point(138, 3);
            this.descriptionTextBox.Name = "descriptionTextBox";
            this.descriptionTextBox.Size = new System.Drawing.Size(165, 20);
            this.descriptionTextBox.TabIndex = 7;
            // 
            // currencyComboBox
            // 
            this.currencyComboBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(182)))), ((int)(((byte)(64)))));
            this.currencyComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bondBindingSource, "Currency", true));
            this.currencyComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.currencyComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.currencyComboBox.FormattingEnabled = true;
            this.currencyComboBox.Location = new System.Drawing.Point(138, 59);
            this.currencyComboBox.Name = "currencyComboBox";
            this.currencyComboBox.Size = new System.Drawing.Size(100, 21);
            this.currencyComboBox.TabIndex = 48;
            this.currencyComboBox.Visible = false;
            // 
            // attribComboBox
            // 
            this.attribComboBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(182)))), ((int)(((byte)(64)))));
            this.attribComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.bondBindingSource, "Attrib", true));
            this.attribComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.attribComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.attribComboBox.FormattingEnabled = true;
            this.attribComboBox.Location = new System.Drawing.Point(444, 139);
            this.attribComboBox.Name = "attribComboBox";
            this.attribComboBox.Size = new System.Drawing.Size(100, 21);
            this.attribComboBox.TabIndex = 36;
            // 
            // taxComboBox
            // 
            this.taxComboBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(182)))), ((int)(((byte)(64)))));
            this.taxComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.bondBindingSource, "Tax", true));
            this.taxComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.taxComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.taxComboBox.FormattingEnabled = true;
            this.taxComboBox.Location = new System.Drawing.Point(444, 112);
            this.taxComboBox.Name = "taxComboBox";
            this.taxComboBox.Size = new System.Drawing.Size(100, 21);
            this.taxComboBox.TabIndex = 34;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tableLayoutPanel1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(137)))), ((int)(((byte)(94)))));
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(620, 318);
            this.groupBox1.TabIndex = 31;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = Labels.GeneralView_InitializeComponent_General;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tableLayoutPanel2);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(137)))), ((int)(((byte)(94)))));
            this.groupBox2.Location = new System.Drawing.Point(0, 318);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(620, 106);
            this.groupBox2.TabIndex = 32;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = Labels.GeneralView_InitializeComponent_Model;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(235)))), ((int)(((byte)(240)))));
            this.tableLayoutPanel2.ColumnCount = 4;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 22F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 28F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 22F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 28F));
            this.tableLayoutPanel2.Controls.Add(label5, 2, 2);
            this.tableLayoutPanel2.Controls.Add(this.textBox4, 3, 2);
            this.tableLayoutPanel2.Controls.Add(label6, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.textBox5, 1, 2);
            this.tableLayoutPanel2.Controls.Add(label7, 2, 1);
            this.tableLayoutPanel2.Controls.Add(this.textBox6, 3, 1);
            this.tableLayoutPanel2.Controls.Add(label8, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.textBox7, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.checkBox1, 1, 0);
            this.tableLayoutPanel2.Controls.Add(label23, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableLayoutPanel2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(137)))), ((int)(((byte)(94)))));
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.Size = new System.Drawing.Size(614, 87);
            this.tableLayoutPanel2.TabIndex = 31;
            // 
            // textBox4
            // 
            this.textBox4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(182)))), ((int)(((byte)(64)))));
            this.textBox4.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bondBindingSource, "LGD", true));
            this.textBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox4.Location = new System.Drawing.Point(444, 59);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(100, 20);
            this.textBox4.TabIndex = 44;
            // 
            // textBox5
            // 
            this.textBox5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(182)))), ((int)(((byte)(64)))));
            this.textBox5.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bondBindingSource, "CDR", true));
            this.textBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox5.Location = new System.Drawing.Point(138, 59);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(100, 20);
            this.textBox5.TabIndex = 42;
            // 
            // textBox6
            // 
            this.textBox6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(182)))), ((int)(((byte)(64)))));
            this.textBox6.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bondBindingSource, "CPR", true));
            this.textBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox6.Location = new System.Drawing.Point(444, 33);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(100, 20);
            this.textBox6.TabIndex = 40;
            // 
            // textBox7
            // 
            this.textBox7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(182)))), ((int)(((byte)(64)))));
            this.textBox7.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bondBindingSource, "TheoreticalCurrFace", true));
            this.textBox7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox7.Location = new System.Drawing.Point(138, 33);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(100, 20);
            this.textBox7.TabIndex = 38;
            // 
            // checkBox1
            // 
            this.checkBox1.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.bondBindingSource, "IsModel", true));
            this.checkBox1.Location = new System.Drawing.Point(138, 3);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(104, 24);
            this.checkBox1.TabIndex = 5;
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // regexTextBox1
            // 
            this.regexTextBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(182)))), ((int)(((byte)(64)))));
            this.regexTextBox1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bondBindingSource, "OrigFace", true, System.Windows.Forms.DataSourceUpdateMode.OnValidation, ""));
            this.regexTextBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.regexTextBox1.Location = new System.Drawing.Point(138, 166);
            this.regexTextBox1.Name = "regexTextBox1";
            this.regexTextBox1.Regex = "^[-]?\\d*(\\.\\d*)?$";
            this.regexTextBox1.Size = new System.Drawing.Size(100, 20);
            this.regexTextBox1.TabIndex = 53;
            this.regexTextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // regexTextBox2
            // 
            this.regexTextBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(182)))), ((int)(((byte)(64)))));
            this.regexTextBox2.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bondBindingSource, "CurrFace", true, System.Windows.Forms.DataSourceUpdateMode.OnValidation, ""));
            this.regexTextBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.regexTextBox2.Location = new System.Drawing.Point(138, 192);
            this.regexTextBox2.Name = "regexTextBox2";
            this.regexTextBox2.Regex = "^[-]?\\d*(\\.\\d*)?$";
            this.regexTextBox2.Size = new System.Drawing.Size(100, 20);
            this.regexTextBox2.TabIndex = 54;
            this.regexTextBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // regexTextBox4
            // 
            this.regexTextBox4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(182)))), ((int)(((byte)(64)))));
            this.regexTextBox4.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bondBindingSource, "BookValue", true, System.Windows.Forms.DataSourceUpdateMode.OnValidation, ""));
            this.regexTextBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.regexTextBox4.Location = new System.Drawing.Point(138, 244);
            this.regexTextBox4.Name = "regexTextBox4";
            this.regexTextBox4.Regex = "^[-]?\\d*(\\.\\d*)?$";
            this.regexTextBox4.Size = new System.Drawing.Size(100, 20);
            this.regexTextBox4.TabIndex = 56;
            this.regexTextBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // regexTextBox5
            // 
            this.regexTextBox5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(182)))), ((int)(((byte)(64)))));
            this.regexTextBox5.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bondBindingSource, "Duration", true, System.Windows.Forms.DataSourceUpdateMode.OnValidation, ""));
            this.regexTextBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.regexTextBox5.Location = new System.Drawing.Point(444, 86);
            this.regexTextBox5.Name = "regexTextBox5";
            this.regexTextBox5.Regex = "^[-]?\\d*(\\.\\d*)?$";
            this.regexTextBox5.Size = new System.Drawing.Size(100, 20);
            this.regexTextBox5.TabIndex = 57;
            this.regexTextBox5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // regexTextBox7
            // 
            this.regexTextBox7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(182)))), ((int)(((byte)(64)))));
            this.regexTextBox7.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bondBindingSource, "RWA", true, System.Windows.Forms.DataSourceUpdateMode.OnValidation, ""));
            this.regexTextBox7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.regexTextBox7.Location = new System.Drawing.Point(444, 218);
            this.regexTextBox7.Name = "regexTextBox7";
            this.regexTextBox7.Regex = "^[-]?\\d*(\\.\\d*)?$";
            this.regexTextBox7.Size = new System.Drawing.Size(100, 20);
            this.regexTextBox7.TabIndex = 59;
            this.regexTextBox7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // regexTextBox6
            // 
            this.regexTextBox6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(182)))), ((int)(((byte)(64)))));
            this.regexTextBox6.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bondBindingSource, "Basel2", true, System.Windows.Forms.DataSourceUpdateMode.OnValidation, ""));
            this.regexTextBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.regexTextBox6.Location = new System.Drawing.Point(444, 166);
            this.regexTextBox6.Name = "regexTextBox6";
            this.regexTextBox6.Regex = "^[-]?\\d*(\\.\\d*)?$";
            this.regexTextBox6.Size = new System.Drawing.Size(100, 20);
            this.regexTextBox6.TabIndex = 58;
            this.regexTextBox6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // regexTextBoxIssuedBookPrice
            // 
            this.regexTextBoxIssuedBookPrice.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(182)))), ((int)(((byte)(64)))));
            this.regexTextBoxIssuedBookPrice.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bondBindingSource, "IssuedBookPrice", true, System.Windows.Forms.DataSourceUpdateMode.OnValidation, ""));
            this.regexTextBoxIssuedBookPrice.Location = new System.Drawing.Point(138, 218);
            this.regexTextBoxIssuedBookPrice.Name = "regexTextBoxIssuedBookPrice";
            this.regexTextBoxIssuedBookPrice.Regex = "^[-]?\\d*(\\.\\d*)?$";
            this.regexTextBoxIssuedBookPrice.Size = new System.Drawing.Size(100, 20);
            this.regexTextBoxIssuedBookPrice.TabIndex = 60;
            this.regexTextBoxIssuedBookPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // GeneralView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(235)))), ((int)(((byte)(240)))));
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(211)))), ((int)(((byte)(194)))), ((int)(((byte)(110)))));
            this.Name = "GeneralView";
            this.Size = new System.Drawing.Size(620, 428);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bondBindingSource)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.BindingSource bondBindingSource;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.ComboBox typeComboBox;
        private System.Windows.Forms.TextBox secIdTextBox;
        private System.Windows.Forms.CheckBox excludeCheckBox;
        private System.Windows.Forms.TextBox descriptionTextBox;
        private System.Windows.Forms.ComboBox accountingComboBox;
        private System.Windows.Forms.TextBox couponTextBox;
        private System.Windows.Forms.TextBox idTextBox;
        private System.Windows.Forms.TextBox marketPriceTextBox;
        private System.Windows.Forms.TextBox tCITextBox;
        private System.Windows.Forms.ComboBox currencyComboBox;
        private System.Windows.Forms.ComboBox attribComboBox;
        private System.Windows.Forms.ComboBox taxComboBox;
        private System.Windows.Forms.ComboBox periodComboBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.CheckBox checkBox1;
        private AssetModelingModule.Controls.RegexTextBox regexTextBox1;
        private AssetModelingModule.Controls.RegexTextBox regexTextBox2;
        private AssetModelingModule.Controls.RegexTextBox regexTextBox4;
        private AssetModelingModule.Controls.RegexTextBox regexTextBox5;
        private AssetModelingModule.Controls.RegexTextBox regexTextBox6;
        private AssetModelingModule.Controls.RegexTextBox regexTextBox7;
        private AssetModelingModule.Controls.RegexTextBox regexTextBoxIssuedBookPrice;


    }
}
