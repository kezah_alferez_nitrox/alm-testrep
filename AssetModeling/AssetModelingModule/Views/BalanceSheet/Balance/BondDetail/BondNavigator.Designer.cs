﻿using AssetModelingModule.Resources.Language;

namespace AssetModelingModule.Views.BalanceSheet.Balance.BondDetail
{
    partial class BondNavigator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BondNavigator));
            this.IndexLabel = new System.Windows.Forms.Label();
            this.PreviousButton = new System.Windows.Forms.Button();
            this.NextButton = new System.Windows.Forms.Button();
            this.ContentPanel = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.bOk = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.bCancel2 = new System.Windows.Forms.Button();
            this.bOk2 = new System.Windows.Forms.Button();
            this.bPrevious2 = new System.Windows.Forms.Button();
            this.bNext2 = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // IndexLabel
            // 
            this.IndexLabel.AutoSize = true;
            this.IndexLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IndexLabel.Location = new System.Drawing.Point(11, 11);
            this.IndexLabel.Name = "IndexLabel";
            this.IndexLabel.Size = new System.Drawing.Size(113, 24);
            this.IndexLabel.TabIndex = 0;
            this.IndexLabel.Text = Labels.BondNavigator_InitializeComponent_IndexLabel;
            // 
            // PreviousButton
            // 
            this.PreviousButton.Image = global::AssetModelingModule.Properties.Resources.left_16;
            this.PreviousButton.Location = new System.Drawing.Point(108, 11);
            this.PreviousButton.Name = "PreviousButton";
            this.PreviousButton.Size = new System.Drawing.Size(91, 37);
            this.PreviousButton.TabIndex = 1;
            this.PreviousButton.Text = Labels.BondNavigator_InitializeComponent_Previous;
            this.PreviousButton.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.PreviousButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.PreviousButton.UseVisualStyleBackColor = true;
            this.PreviousButton.Click += new System.EventHandler(this.PreviousButton_Click);
            // 
            // NextButton
            // 
            this.NextButton.Image = global::AssetModelingModule.Properties.Resources.right_16;
            this.NextButton.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.NextButton.Location = new System.Drawing.Point(205, 11);
            this.NextButton.Name = "NextButton";
            this.NextButton.Size = new System.Drawing.Size(91, 37);
            this.NextButton.TabIndex = 2;
            this.NextButton.Text = Labels.BondNavigator_InitializeComponent_Next;
            this.NextButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.NextButton.UseVisualStyleBackColor = true;
            this.NextButton.Click += new System.EventHandler(this.NextButton_Click);
            // 
            // ContentPanel
            // 
            this.ContentPanel.AutoScroll = true;
            this.ContentPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ContentPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ContentPanel.Location = new System.Drawing.Point(0, 94);
            this.ContentPanel.Margin = new System.Windows.Forms.Padding(0);
            this.ContentPanel.Name = "ContentPanel";
            this.ContentPanel.Size = new System.Drawing.Size(804, 792);
            this.ContentPanel.TabIndex = 3;
            // 
            // panel1
            // 
            this.panel1.AutoSize = true;
            this.panel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.bOk);
            this.panel1.Controls.Add(this.PreviousButton);
            this.panel1.Controls.Add(this.NextButton);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 886);
            this.panel1.Margin = new System.Windows.Forms.Padding(0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(804, 60);
            this.panel1.TabIndex = 4;
            // 
            // button1
            // 
            this.button1.Image = global::AssetModelingModule.Properties.Resources.block_16;
            this.button1.Location = new System.Drawing.Point(11, 11);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(91, 37);
            this.button1.TabIndex = 4;
            this.button1.Text = Labels.BondNavigator_InitializeComponent_Cancel;
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.CancelClick);
            // 
            // bOk
            // 
            this.bOk.Image = global::AssetModelingModule.Properties.Resources.tick_16;
            this.bOk.Location = new System.Drawing.Point(302, 11);
            this.bOk.Name = "bOk";
            this.bOk.Size = new System.Drawing.Size(91, 37);
            this.bOk.TabIndex = 3;
            this.bOk.Text = Labels.BondNavigator_InitializeComponent_Ok;
            this.bOk.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bOk.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.bOk.UseVisualStyleBackColor = true;
            this.bOk.Click += new System.EventHandler(this.OkClick);
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.bCancel2);
            this.panel2.Controls.Add(this.bOk2);
            this.panel2.Controls.Add(this.bPrevious2);
            this.panel2.Controls.Add(this.bNext2);
            this.panel2.Controls.Add(this.pictureBox1);
            this.panel2.Controls.Add(this.IndexLabel);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(804, 94);
            this.panel2.TabIndex = 5;
            // 
            // bCancel2
            // 
            this.bCancel2.Image = global::AssetModelingModule.Properties.Resources.block_16;
            this.bCancel2.Location = new System.Drawing.Point(11, 46);
            this.bCancel2.Name = "bCancel2";
            this.bCancel2.Size = new System.Drawing.Size(91, 37);
            this.bCancel2.TabIndex = 8;
            this.bCancel2.Text = Labels.BondNavigator_InitializeComponent_Cancel;
            this.bCancel2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bCancel2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.bCancel2.UseVisualStyleBackColor = true;
            this.bCancel2.Click += new System.EventHandler(this.CancelClick);
            // 
            // bOk2
            // 
            this.bOk2.Image = global::AssetModelingModule.Properties.Resources.tick_16;
            this.bOk2.Location = new System.Drawing.Point(302, 46);
            this.bOk2.Name = "bOk2";
            this.bOk2.Size = new System.Drawing.Size(91, 37);
            this.bOk2.TabIndex = 7;
            this.bOk2.Text = Labels.BondNavigator_InitializeComponent_Ok;
            this.bOk2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bOk2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.bOk2.UseVisualStyleBackColor = true;
            this.bOk2.Click += new System.EventHandler(this.OkClick);
            // 
            // bPrevious2
            // 
            this.bPrevious2.Image = global::AssetModelingModule.Properties.Resources.left_16;
            this.bPrevious2.Location = new System.Drawing.Point(108, 46);
            this.bPrevious2.Name = "bPrevious2";
            this.bPrevious2.Size = new System.Drawing.Size(91, 37);
            this.bPrevious2.TabIndex = 5;
            this.bPrevious2.Text = Labels.BondNavigator_InitializeComponent_Previous;
            this.bPrevious2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bPrevious2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.bPrevious2.UseVisualStyleBackColor = true;
            this.bPrevious2.Click += new System.EventHandler(this.PreviousButton_Click);
            // 
            // bNext2
            // 
            this.bNext2.Image = global::AssetModelingModule.Properties.Resources.right_16;
            this.bNext2.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bNext2.Location = new System.Drawing.Point(205, 46);
            this.bNext2.Name = "bNext2";
            this.bNext2.Size = new System.Drawing.Size(91, 37);
            this.bNext2.TabIndex = 6;
            this.bNext2.Text = Labels.BondNavigator_InitializeComponent_Next;
            this.bNext2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.bNext2.UseVisualStyleBackColor = true;
            this.bNext2.Click += new System.EventHandler(this.NextButton_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.Location = new System.Drawing.Point(727, 14);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(64, 64);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.ContentPanel, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.panel2, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 94F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(804, 946);
            this.tableLayoutPanel1.TabIndex = 6;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "bond.jpg");
            this.imageList1.Images.SetKeyName(1, "swap.png");
            this.imageList1.Images.SetKeyName(2, "cap.gif");
            this.imageList1.Images.SetKeyName(3, "floor.gif");
            // 
            // BondNavigator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(222)))), ((int)(((byte)(228)))));
            this.ClientSize = new System.Drawing.Size(804, 946);
            this.Controls.Add(this.tableLayoutPanel1);
            this.MaximumSize = new System.Drawing.Size(820, 1000);
            this.Name = "BondNavigator";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = Labels.BondNavigator_InitializeComponent_Bond_editor;
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label IndexLabel;
        private System.Windows.Forms.Button PreviousButton;
        private System.Windows.Forms.Button NextButton;
        private System.Windows.Forms.Panel ContentPanel;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button bOk;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button bCancel2;
        private System.Windows.Forms.Button bOk2;
        private System.Windows.Forms.Button bPrevious2;
        private System.Windows.Forms.Button bNext2;
        private System.Windows.Forms.ImageList imageList1;
    }
}