﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using AssetModelingModule.Domain;
using Eu.AlterSystems.ASNetLib.Core;

namespace AssetModelingModule.Views.BalanceSheet.Balance.BondDetail
{
    public partial class GeneralView : UserControl
    {
        public GeneralView()
        {
            InitializeComponent();
            typeComboBox.DataSource = EnumHelper.ToList(typeof(BondType));
            typeComboBox.DisplayMember = "Value";
            typeComboBox.ValueMember = "Key";
            typeComboBox.SelectedIndex = 0;

            accountingComboBox.DataSource = EnumHelper.ToList(typeof(BondAccounting));
            accountingComboBox.DisplayMember = "Value";
            accountingComboBox.ValueMember = "Key";
            accountingComboBox.SelectedIndex = 0;

            periodComboBox.DataSource = EnumHelper.ToList(typeof(BondPeriod));
            periodComboBox.DisplayMember = "Value";
            periodComboBox.ValueMember = "Key";
            periodComboBox.SelectedIndex = 0;

            taxComboBox.DataSource = EnumHelper.ToList(typeof(BondTax));
            taxComboBox.DisplayMember = "Value";
            taxComboBox.ValueMember = "Key";
            taxComboBox.SelectedIndex = 0;

            attribComboBox.DataSource = EnumHelper.ToList(typeof(BondAttrib));
            attribComboBox.DisplayMember = "Value";
            attribComboBox.ValueMember = "Key";
            attribComboBox.SelectedIndex = 0;

            if(!this.DesignMode)
            {
                //currencyComboBox.DataSource = Currency.FindAllNotEmpty().Select(c => c.ShortName).ToList();
                //currencyComboBox.SelectedIndex = 0;               
            }

            SetModelControlEnable(checkBox1.Checked);
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            SetModelControlEnable(checkBox1.Checked);
        }

        private void SetModelControlEnable(bool enable)
        {
            textBox4.Enabled = enable;
            textBox5.Enabled = enable;
            textBox6.Enabled = enable;
            textBox7.Enabled = enable;
        }

        public Bond Bond
        {
            set
            {
                bondBindingSource.Add(value);
                ControlsVisibilities(value);
            }
        }

        private void ControlsVisibilities(Bond bond)
        {
            if (bond.BondFinancialType == BondFinancialType.Bond)
            {
                currencyComboBox.Visible = tableLayoutPanel1.Controls["currencyLabel"].Visible = true;
            }
            else if (bond.BondFinancialType == BondFinancialType.Swap)
            {
                if (bond.Leg == BondSwapLeg.PayFloat || bond.Leg == BondSwapLeg.ReceivedFloat)
                    couponTextBox.Visible = tableLayoutPanel1.Controls["couponLabel"].Visible = true;
            }
        }
    }
}
