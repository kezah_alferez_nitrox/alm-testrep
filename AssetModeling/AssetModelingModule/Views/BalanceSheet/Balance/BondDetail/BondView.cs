﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using AssetModelingModule.Domain;
using Eu.AlterSystems.ASNetLib.Core;
using UIComponents;

namespace AssetModelingModule.Views.BalanceSheet.Balance.BondDetail
{
    public partial class BondView : UserControl
    {
        public BondView()
        {
            InitializeComponent();

            cuponTypeComboBox.DataSource = EnumHelper.ToList(typeof(CouponType));
            cuponTypeComboBox.DisplayMember = "Value";
            cuponTypeComboBox.ValueMember = "Key";
            cuponTypeComboBox.SelectedIndex = 0;

            typeIdcomboBox.DataSource = EnumHelper.ToList(typeof(TypeId));
            typeIdcomboBox.DisplayMember = "Value";
            typeIdcomboBox.ValueMember = "Key";
            typeIdcomboBox.SelectedIndex = 0;

            accountingComboBox.DataSource = EnumHelper.ToList(typeof(BondAccounting));
            accountingComboBox.DisplayMember = "Value";
            accountingComboBox.ValueMember = "Key";
            accountingComboBox.SelectedIndex = 0;

            dayCountComboBox.DataSource = EnumHelper.ToList(typeof(DayCountConvention));
            dayCountComboBox.DisplayMember = "Value";
            dayCountComboBox.ValueMember = "Key";
            dayCountComboBox.SelectedIndex = 0;

            xpPanel1.PanelState = XPPanelState.Collapsed;
        }

        public Bond Bond
        {
            set
            {
                bindingSource1.Add(value);
                generalView1.Bond = value;
            }
        }

        private void ShowIndexAndSpread()
        {
            labelIndex.Visible =
                labelSpread.Visible =
                comboBoxIndex.Visible = regexTextBoxSpread.Visible = (CouponType)((KeyValuePair<Enum, string>)(cuponTypeComboBox.SelectedItem)).Key == CouponType.Variable;
        }

        private void cuponTypeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            ShowIndexAndSpread();
            if( (CouponType)((KeyValuePair<Enum, string>)(cuponTypeComboBox.SelectedItem)).Key == CouponType.Isin)
            {
                this.regexTextBoxCoupon.Regex = @"^[-]?\d*(\.\d*)?$";
                this.regexTextBoxCoupon.TextAlign = HorizontalAlignment.Right;
                this.regexTextBoxCoupon.ValidateText();
            }
            else
            {
                this.regexTextBoxCoupon.Regex = "";
                this.regexTextBoxCoupon.TextAlign = HorizontalAlignment.Left;
                this.regexTextBoxCoupon.ValidateText();
            }
        }
   }
}
