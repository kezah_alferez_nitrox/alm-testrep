﻿using System.Text.RegularExpressions;
using AssetModelingModule.Resources.Language;
using UIComponents;

namespace AssetModelingModule.Views.BalanceSheet.Balance.BondDetail
{
    partial class BondView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label accountingLabel1;
            this.xpPanel1 = new UIComponents.XPPanel(462);
            this.generalView1 = new AssetModelingModule.Views.BalanceSheet.Balance.BondDetail.GeneralView();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.typeIdcomboBox = new System.Windows.Forms.ComboBox();
            this.cuponTypeComboBox = new System.Windows.Forms.ComboBox();
            this.accountingComboBox = new System.Windows.Forms.ComboBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.labelIndex = new System.Windows.Forms.Label();
            this.dateTimePicker4 = new System.Windows.Forms.DateTimePicker();
            this.labelSpread = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.dayCountComboBox = new System.Windows.Forms.ComboBox();
            this.comboBoxIndex = new System.Windows.Forms.ComboBox();
            this.regexTextBox1 = new AssetModelingModule.Controls.RegexTextBox();
            this.regexTextBoxSpread = new AssetModelingModule.Controls.RegexTextBox();
            this.regexTextBoxCoupon = new AssetModelingModule.Controls.RegexTextBox();
            accountingLabel1 = new System.Windows.Forms.Label();
            this.xpPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // accountingLabel1
            // 
            accountingLabel1.AutoSize = true;
            accountingLabel1.Dock = System.Windows.Forms.DockStyle.Fill;
            accountingLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            accountingLabel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(137)))), ((int)(((byte)(94)))));
            accountingLabel1.Location = new System.Drawing.Point(303, 26);
            accountingLabel1.Name = "accountingLabel1";
            accountingLabel1.Size = new System.Drawing.Size(126, 27);
            accountingLabel1.TabIndex = 23;
            accountingLabel1.Text = Labels.BondView_InitializeComponent_Accounting_;
            accountingLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // xpPanel1
            // 
            this.xpPanel1.BackColor = System.Drawing.Color.Transparent;
            this.xpPanel1.CaptionCornerType = ((UIComponents.CornerType)((UIComponents.CornerType.TopLeft | UIComponents.CornerType.TopRight)));
            this.xpPanel1.CaptionGradient.End = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(213)))), ((int)(((byte)(247)))));
            this.xpPanel1.CaptionGradient.Start = System.Drawing.Color.White;
            this.xpPanel1.CaptionGradientMode = System.Drawing.Drawing2D.LinearGradientMode.Horizontal;
            this.xpPanel1.CaptionUnderline = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.tableLayoutPanel1.SetColumnSpan(this.xpPanel1, 4);
            this.xpPanel1.Controls.Add(this.generalView1);
            this.xpPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xpPanel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.xpPanel1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.xpPanel1.HorzAlignment = System.Drawing.StringAlignment.Near;
            this.xpPanel1.ImageItems.ImageSet = null;
            this.xpPanel1.Location = new System.Drawing.Point(3, 190);
            this.xpPanel1.Name = "xpPanel1";
            this.xpPanel1.PanelGradient.End = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(223)))), ((int)(((byte)(247)))));
            this.xpPanel1.PanelGradient.Start = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(223)))), ((int)(((byte)(247)))));
            this.xpPanel1.PanelGradientMode = System.Drawing.Drawing2D.LinearGradientMode.Horizontal;
            this.xpPanel1.Size = new System.Drawing.Size(594, 462);
            this.xpPanel1.TabIndex = 30;
            this.xpPanel1.TextColors.Foreground = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(93)))), ((int)(((byte)(198)))));
            this.xpPanel1.TextHighlightColors.Foreground = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(142)))), ((int)(((byte)(255)))));
            this.xpPanel1.VertAlignment = System.Drawing.StringAlignment.Center;
            // 
            // generalView1
            // 
            this.generalView1.AutoScroll = true;
            this.generalView1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(235)))), ((int)(((byte)(240)))));
            this.generalView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.generalView1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(211)))), ((int)(((byte)(194)))), ((int)(((byte)(110)))));
            this.generalView1.Location = new System.Drawing.Point(2, 35);
            this.generalView1.Name = "generalView1";
            this.generalView1.Size = new System.Drawing.Size(590, 424);
            this.generalView1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(137)))), ((int)(((byte)(94)))));
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(126, 26);
            this.label1.TabIndex = 0;
            this.label1.Text = Labels.BondView_InitializeComponent_Date_issue_;
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(137)))), ((int)(((byte)(94)))));
            this.label2.Location = new System.Drawing.Point(303, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(126, 26);
            this.label2.TabIndex = 1;
            this.label2.Text = Labels.BondView_InitializeComponent_Maturity_;
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.CustomFormat = "dd/MM/yyyy";
            this.dateTimePicker1.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.bindingSource1, "DateIssue", true, System.Windows.Forms.DataSourceUpdateMode.OnValidation, null, "d"));
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker1.Location = new System.Drawing.Point(135, 3);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(96, 20);
            this.dateTimePicker1.TabIndex = 2;
            // 
            // bindingSource1
            // 
            this.bindingSource1.DataSource = typeof(AssetModelingModule.Domain.Bond);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(137)))), ((int)(((byte)(94)))));
            this.label3.Location = new System.Drawing.Point(3, 26);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(126, 27);
            this.label3.TabIndex = 4;
            this.label3.Text = Labels.BondView_InitializeComponent_Name_;
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(137)))), ((int)(((byte)(94)))));
            this.label6.Location = new System.Drawing.Point(303, 53);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(126, 27);
            this.label6.TabIndex = 7;
            this.label6.Text = Labels.BondView_InitializeComponent_Type_ID_;
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(137)))), ((int)(((byte)(94)))));
            this.label7.Location = new System.Drawing.Point(3, 53);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(126, 27);
            this.label7.TabIndex = 8;
            this.label7.Text = Labels.BondView_InitializeComponent_ID_;
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(182)))), ((int)(((byte)(64)))));
            this.textBox1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bindingSource1, "Name", true));
            this.textBox1.Location = new System.Drawing.Point(135, 29);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(127, 20);
            this.textBox1.TabIndex = 10;
            // 
            // textBox3
            // 
            this.textBox3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(182)))), ((int)(((byte)(64)))));
            this.textBox3.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bindingSource1, "BondId", true));
            this.textBox3.Location = new System.Drawing.Point(135, 56);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(127, 20);
            this.textBox3.TabIndex = 12;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(137)))), ((int)(((byte)(94)))));
            this.label4.Location = new System.Drawing.Point(3, 80);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(126, 27);
            this.label4.TabIndex = 14;
            this.label4.Text = Labels.BondView_InitializeComponent_Coupon_Type_;
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(137)))), ((int)(((byte)(94)))));
            this.label5.Location = new System.Drawing.Point(303, 80);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(126, 27);
            this.label5.TabIndex = 15;
            this.label5.Text = Labels.BondView_InitializeComponent_Coupon_;
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // typeIdcomboBox
            // 
            this.typeIdcomboBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(182)))), ((int)(((byte)(64)))));
            this.typeIdcomboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.bindingSource1, "TypeId", true));
            this.typeIdcomboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.typeIdcomboBox.FormattingEnabled = true;
            this.typeIdcomboBox.Location = new System.Drawing.Point(435, 56);
            this.typeIdcomboBox.Name = "typeIdcomboBox";
            this.typeIdcomboBox.Size = new System.Drawing.Size(128, 21);
            this.typeIdcomboBox.TabIndex = 21;
            // 
            // cuponTypeComboBox
            // 
            this.cuponTypeComboBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(182)))), ((int)(((byte)(64)))));
            this.cuponTypeComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.bindingSource1, "CouponType", true));
            this.cuponTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cuponTypeComboBox.FormattingEnabled = true;
            this.cuponTypeComboBox.Location = new System.Drawing.Point(135, 83);
            this.cuponTypeComboBox.Name = "cuponTypeComboBox";
            this.cuponTypeComboBox.Size = new System.Drawing.Size(127, 21);
            this.cuponTypeComboBox.TabIndex = 22;
            this.cuponTypeComboBox.SelectedIndexChanged += new System.EventHandler(this.cuponTypeComboBox_SelectedIndexChanged);
            // 
            // accountingComboBox
            // 
            this.accountingComboBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(182)))), ((int)(((byte)(64)))));
            this.accountingComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.bindingSource1, "Accounting", true));
            this.accountingComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.accountingComboBox.FormattingEnabled = true;
            this.accountingComboBox.Location = new System.Drawing.Point(435, 29);
            this.accountingComboBox.Name = "accountingComboBox";
            this.accountingComboBox.Size = new System.Drawing.Size(128, 21);
            this.accountingComboBox.TabIndex = 24;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(235)))), ((int)(((byte)(240)))));
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 22F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 28F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 22F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 28F));
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label11, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.dateTimePicker1, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label12, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.cuponTypeComboBox, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.labelIndex, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.textBox1, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label7, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.textBox3, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label2, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.dateTimePicker4, 3, 0);
            this.tableLayoutPanel1.Controls.Add(accountingLabel1, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.accountingComboBox, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.label6, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.typeIdcomboBox, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.label5, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.labelSpread, 2, 4);
            this.tableLayoutPanel1.Controls.Add(this.label10, 2, 5);
            this.tableLayoutPanel1.Controls.Add(this.dateTimePicker2, 3, 5);
            this.tableLayoutPanel1.Controls.Add(this.dayCountComboBox, 2, 6);
            this.tableLayoutPanel1.Controls.Add(this.xpPanel1, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.comboBoxIndex, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.regexTextBox1, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.regexTextBoxSpread, 3, 4);
            this.tableLayoutPanel1.Controls.Add(this.regexTextBoxCoupon, 3, 3);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(194)))), ((int)(((byte)(110)))));
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 8;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(600, 655);
            this.tableLayoutPanel1.TabIndex = 29;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.label11, 2);
            this.label11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(137)))), ((int)(((byte)(94)))));
            this.label11.Location = new System.Drawing.Point(3, 160);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(294, 27);
            this.label11.TabIndex = 18;
            this.label11.Text = Labels.BondView_InitializeComponent_Day_Count_Convention_;
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(137)))), ((int)(((byte)(94)))));
            this.label12.Location = new System.Drawing.Point(3, 134);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(126, 26);
            this.label12.TabIndex = 19;
            this.label12.Text = Labels.BondView_InitializeComponent_Frequence_;
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelIndex
            // 
            this.labelIndex.AutoSize = true;
            this.labelIndex.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelIndex.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelIndex.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(137)))), ((int)(((byte)(94)))));
            this.labelIndex.Location = new System.Drawing.Point(3, 107);
            this.labelIndex.Name = "labelIndex";
            this.labelIndex.Size = new System.Drawing.Size(126, 27);
            this.labelIndex.TabIndex = 16;
            this.labelIndex.Text = Labels.BondView_InitializeComponent_Index_;
            this.labelIndex.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dateTimePicker4
            // 
            this.dateTimePicker4.CustomFormat = "dd/MM/yyyy";
            this.dateTimePicker4.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.bindingSource1, "Maturity", true));
            this.dateTimePicker4.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker4.Location = new System.Drawing.Point(435, 3);
            this.dateTimePicker4.Name = "dateTimePicker4";
            this.dateTimePicker4.Size = new System.Drawing.Size(96, 20);
            this.dateTimePicker4.TabIndex = 3;
            // 
            // labelSpread
            // 
            this.labelSpread.AutoSize = true;
            this.labelSpread.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelSpread.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSpread.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(137)))), ((int)(((byte)(94)))));
            this.labelSpread.Location = new System.Drawing.Point(303, 107);
            this.labelSpread.Name = "labelSpread";
            this.labelSpread.Size = new System.Drawing.Size(126, 27);
            this.labelSpread.TabIndex = 20;
            this.labelSpread.Text = "Spread:";
            this.labelSpread.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(137)))), ((int)(((byte)(94)))));
            this.label10.Location = new System.Drawing.Point(303, 134);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(126, 26);
            this.label10.TabIndex = 17;
            this.label10.Text = Labels.BondView_InitializeComponent_First_Coupon_Date_;
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.CustomFormat = "dd/MM/yyyy";
            this.dateTimePicker2.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.bindingSource1, "FirstCouponDate", true));
            this.dateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker2.Location = new System.Drawing.Point(435, 137);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(96, 20);
            this.dateTimePicker2.TabIndex = 29;
            // 
            // dayCountComboBox
            // 
            this.dayCountComboBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(182)))), ((int)(((byte)(64)))));
            this.tableLayoutPanel1.SetColumnSpan(this.dayCountComboBox, 2);
            this.dayCountComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.bindingSource1, "DayCountConvention", true));
            this.dayCountComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.dayCountComboBox.FormattingEnabled = true;
            this.dayCountComboBox.Location = new System.Drawing.Point(303, 163);
            this.dayCountComboBox.Name = "dayCountComboBox";
            this.dayCountComboBox.Size = new System.Drawing.Size(130, 21);
            this.dayCountComboBox.TabIndex = 13;
            // 
            // comboBoxIndex
            // 
            this.comboBoxIndex.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.bindingSource1, "BondIndex", true));
            this.comboBoxIndex.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxIndex.FormattingEnabled = true;
            this.comboBoxIndex.Location = new System.Drawing.Point(135, 110);
            this.comboBoxIndex.Name = "comboBoxIndex";
            this.comboBoxIndex.Size = new System.Drawing.Size(127, 21);
            this.comboBoxIndex.TabIndex = 31;
            // 
            // regexTextBox1
            // 
            this.regexTextBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(182)))), ((int)(((byte)(64)))));
            this.regexTextBox1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bindingSource1, "Frequence", true, System.Windows.Forms.DataSourceUpdateMode.OnValidation, ""));
            this.regexTextBox1.Location = new System.Drawing.Point(135, 137);
            this.regexTextBox1.Name = "regexTextBox1";
            this.regexTextBox1.Regex = "^\\d*$";
            this.regexTextBox1.Size = new System.Drawing.Size(127, 20);
            this.regexTextBox1.TabIndex = 32;
            this.regexTextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // regexTextBoxSpread
            // 
            this.regexTextBoxSpread.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(182)))), ((int)(((byte)(64)))));
            this.regexTextBoxSpread.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bindingSource1, "Spread", true, System.Windows.Forms.DataSourceUpdateMode.OnValidation, ""));
            this.regexTextBoxSpread.Location = new System.Drawing.Point(435, 110);
            this.regexTextBoxSpread.Name = "regexTextBoxSpread";
            this.regexTextBoxSpread.Regex = "^[-]?\\d{0,1}(\\.\\d{0,2})?$";
            this.regexTextBoxSpread.Size = new System.Drawing.Size(128, 20);
            this.regexTextBoxSpread.TabIndex = 33;
            this.regexTextBoxSpread.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // regexTextBoxCoupon
            // 
            this.regexTextBoxCoupon.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(182)))), ((int)(((byte)(64)))));
            this.regexTextBoxCoupon.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bindingSource1, "Coupon", true));
            this.regexTextBoxCoupon.Location = new System.Drawing.Point(435, 83);
            this.regexTextBoxCoupon.Name = "regexTextBoxCoupon";
            this.regexTextBoxCoupon.Regex = "^[-]?\\d*(\\.\\d*)?$";
            this.regexTextBoxCoupon.Size = new System.Drawing.Size(128, 20);
            this.regexTextBoxCoupon.TabIndex = 34;
            this.regexTextBoxCoupon.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // BondView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "BondView";
            this.Size = new System.Drawing.Size(600, 655);
            this.xpPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.BindingSource bindingSource1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox typeIdcomboBox;
        private System.Windows.Forms.ComboBox cuponTypeComboBox;
        private System.Windows.Forms.ComboBox accountingComboBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.DateTimePicker dateTimePicker4;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label labelSpread;
        private System.Windows.Forms.Label labelIndex;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox dayCountComboBox;
        private UIComponents.XPPanel xpPanel1;
        private GeneralView generalView1;
        private System.Windows.Forms.ComboBox comboBoxIndex;
        private AssetModelingModule.Controls.RegexTextBox regexTextBox1;
        private AssetModelingModule.Controls.RegexTextBox regexTextBoxSpread;
        private AssetModelingModule.Controls.RegexTextBox regexTextBoxCoupon;
    }
}
