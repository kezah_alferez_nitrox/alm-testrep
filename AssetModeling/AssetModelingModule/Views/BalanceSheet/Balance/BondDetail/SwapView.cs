﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using AssetModelingModule.Domain;
using AssetModelingModule.Pricing;
using Eu.AlterSystems.ASNetLib.Core;
using UIComponents;

namespace AssetModelingModule.Views.BalanceSheet.Balance.BondDetail
{
    public partial class SwapView : UserControl
    {
        private Bond bond1;
        private Bond bond2;

        public SwapView()
        {
            this.InitializeComponent();

            var legSource = EnumHelper.ToList(typeof(BondSwapLeg));

            this.leg1ComboBox.DataSource = new ArrayList() { legSource[0], legSource[1] };
            this.leg1ComboBox.DisplayMember = "Value";
            this.leg1ComboBox.ValueMember = "Key";
            this.leg1ComboBox.SelectedIndex = 0;

            this.leg2ComboBox.DataSource = new ArrayList() { legSource[3], legSource[2] };
            this.leg2ComboBox.DisplayMember = "Value";
            this.leg2ComboBox.ValueMember = "Key";
            this.leg2ComboBox.SelectedIndex = 1;

            this.currency1ComboBox.DisplayMember = "Value";
            this.currency1ComboBox.ValueMember = "Key";
            this.currency1ComboBox.SelectedIndex = 0;

            this.currency2ComboBox.DisplayMember = "Value";
            this.currency2ComboBox.ValueMember = "Key";
            this.currency2ComboBox.SelectedIndex = 0;

            this.payFreq1ComboBox.DataSource = EnumHelper.ToList(typeof(BondPeriod));
            this.payFreq1ComboBox.DisplayMember = "Value";
            this.payFreq1ComboBox.ValueMember = "Key";
            this.payFreq1ComboBox.SelectedIndex = 0;

            this.payFreq2Combobox.DataSource = EnumHelper.ToList(typeof(BondPeriod));
            this.payFreq2Combobox.DisplayMember = "Value";
            this.payFreq2Combobox.ValueMember = "Key";
            this.payFreq2Combobox.SelectedIndex = 0;

            this.reset2ComboBox.DataSource = EnumHelper.ToList(typeof(BondPeriod));
            this.reset2ComboBox.DisplayMember = "Value";
            this.reset2ComboBox.ValueMember = "Key";
            this.reset2ComboBox.SelectedIndex = 0;

            this.dayCount1ComboBox.DataSource = EnumHelper.ToList(typeof(DayCountConvention));
            this.dayCount1ComboBox.DisplayMember = "Value";
            this.dayCount1ComboBox.ValueMember = "Key";
            this.dayCount1ComboBox.SelectedIndex = 0;

            this.dayCount2ComboBox.DataSource = EnumHelper.ToList(typeof(DayCountConvention));
            this.dayCount2ComboBox.DisplayMember = "Value";
            this.dayCount2ComboBox.ValueMember = "Key";
            this.dayCount2ComboBox.SelectedIndex = 0;

            this.comboBoxIndex.DisplayMember = "Name";
            this.comboBoxIndex.ValueMember = "Self";
            this.comboBoxIndex.DataSource = new BindingList<Variable>(new List<Variable>(Variable.FindAvailable()));

            this.xpPanel1.PanelState = XPPanelState.Collapsed;

            this.bindingSource1.ListChanged += this.BindingSourceChanged;
            this.bindingSource2.ListChanged += this.BindingSourceChanged;
        }

        private void BindingSourceChanged(object sender, EventArgs e)
        {
            this.RefreshReofferedBookPrice();
        }

        private void Leg1ComboBoxSelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.leg1ComboBox.SelectedIndex != this.leg2ComboBox.SelectedIndex)
                this.leg2ComboBox.SelectedIndex = this.leg1ComboBox.SelectedIndex;
        }

        private void Leg2ComboBoxSelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.leg2ComboBox.SelectedIndex != this.leg1ComboBox.SelectedIndex)
                this.leg1ComboBox.SelectedIndex = this.leg2ComboBox.SelectedIndex;
        }

        public Bond Bond
        {
            set
            {
                this.bond1 = value;
                this.bond2 = value.SwapBondLink;
                this.bindingSource1.Add(value);
                this.bindingSource2.Add(value.SwapBondLink);
                this.generalView1.Bond = value;
                this.generalView2.Bond = value.SwapBondLink;
            }
        }

        private void reofferedBookPriceTextBox_Click(object sender, EventArgs e)
        {
            this.RefreshReofferedBookPrice();
        }

        private void RefreshReofferedBookPrice()
        {
            if (this.bond1 == null || this.bond2 == null) return;

            double coupon1 = this.Evaluate(this.bond1.Coupon);
            double coupon2 = this.Evaluate(this.bond2.Coupon);

            DateTime expiry = (Param.ValuationDate ?? DateTime.Today).AddMonths((int) this.bond2.Duration);

            double price = Pricer.SwapPrice(coupon1/100, coupon2/100, expiry);

            this.reofferedBookPriceTextBox.Text = double.IsNaN(price) ? "" : price.ToString();
        }

        private double Evaluate(string text)
        {
            double value;
            if (!double.TryParse(text, out value))
            {
                value = double.NaN;
            }
            return value;
        }
    }
}