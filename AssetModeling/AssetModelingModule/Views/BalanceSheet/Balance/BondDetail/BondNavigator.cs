﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using AssetModelingModule.Domain;

namespace AssetModelingModule.Views.BalanceSheet.Balance.BondDetail
{
    public partial class BondNavigator : Form
    {
        private int _currentIndex;

        public BondNavigator(IList<Bond> bonds, int position)
        {
            InitializeComponent();

            Bonds = bonds;

            CurrentIndex = position;

            Top = 0;
            MaximumSize = new Size(MaximumSize.Width, Screen.GetWorkingArea(this).Height);
            MinimumSize = new Size(MinimumSize.Width, Screen.GetWorkingArea(this).Height);
        }

        public IList<Bond> Bonds { get; set; }

        public BondFinancialType BondFinancialType { get; set; }


        public int CurrentIndex
        {
            get { return _currentIndex; }
            set
            {
                _currentIndex = value;

                IndexLabel.Text = string.Format("{0}   {1}/{2}", Bonds[value].BondFinancialType, CurrentIndex + 1, Bonds.Count);
                bPrevious2.Enabled = PreviousButton.Enabled = value != 0;
                bNext2.Enabled = NextButton.Enabled = value != Bonds.Count - 1;
                pictureBox1.Image = imageList1.Images[(int) Bonds[value].BondFinancialType];
                Control c = GetView(Bonds[value].BondFinancialType, Bonds[value]);
                c.Dock = DockStyle.Fill;
                ContentPanel.SuspendLayout();
                ContentPanel.Controls.Clear();
                ContentPanel.Controls.Add(c);
                ContentPanel.Refresh();
                ContentPanel.ResumeLayout();
            }
        }

        public Control GetView(BondFinancialType bondFinancialType, Bond bond)
        {
            switch (bondFinancialType)
            {
                case BondFinancialType.Bond:
                    return new BondView() { Bond = bond };
                case BondFinancialType.Swap:
                    return new SwapView() { Bond = bond };
                case BondFinancialType.Cap:
                    return new BondView() { Bond = bond };
                case BondFinancialType.Floor:
                    return new BondView() { Bond = bond };
                default:
                    return null;
            }
        }

        private void PreviousButton_Click(object sender, EventArgs e)
        {
            CurrentIndex--;
        }

        private void NextButton_Click(object sender, EventArgs e)
        {
            CurrentIndex++;
        }

        private void CancelClick(object sender, EventArgs e)
        {
            Close();
        }

        private void OkClick(object sender, EventArgs e)
        {
            Close();
        }
    }
}