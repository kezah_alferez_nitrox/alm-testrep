﻿using AssetModelingModule.Resources.Language;
using UIComponents;

namespace AssetModelingModule.Views.BalanceSheet.Balance.BondDetail
{
    partial class SwapView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.xpPanel1 = new UIComponents.XPPanel(443);
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.generalView1 = new AssetModelingModule.Views.BalanceSheet.Balance.BondDetail.GeneralView();
            this.generalView2 = new AssetModelingModule.Views.BalanceSheet.Balance.BondDetail.GeneralView();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.label7 = new System.Windows.Forms.Label();
            this.leg1ComboBox = new System.Windows.Forms.ComboBox();
            this.currency1ComboBox = new System.Windows.Forms.ComboBox();
            this.dateTimePicker4 = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.payFreq1ComboBox = new System.Windows.Forms.ComboBox();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.leg2ComboBox = new System.Windows.Forms.ComboBox();
            this.bindingSource2 = new System.Windows.Forms.BindingSource(this.components);
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.currency2ComboBox = new System.Windows.Forms.ComboBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker3 = new System.Windows.Forms.DateTimePicker();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.reset2ComboBox = new System.Windows.Forms.ComboBox();
            this.payFreq2Combobox = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.regexTextBox2 = new AssetModelingModule.Controls.RegexTextBox();
            this.textBoxWithDropDown1 = new AssetModelingModule.Controls.TextBoxWithDropDown();
            this.regexTextBox1 = new AssetModelingModule.Controls.RegexTextBox();
            this.comboBoxIndex = new System.Windows.Forms.ComboBox();
            this.dayCount2ComboBox = new System.Windows.Forms.ComboBox();
            this.dayCount1ComboBox = new System.Windows.Forms.ComboBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.reofferedBookPriceTextBox = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.xpPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource2)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // xpPanel1
            // 
            this.xpPanel1.BackColor = System.Drawing.Color.Transparent;
            this.xpPanel1.CaptionCornerType = ((UIComponents.CornerType)((UIComponents.CornerType.TopLeft | UIComponents.CornerType.TopRight)));
            this.xpPanel1.CaptionGradient.End = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(213)))), ((int)(((byte)(247)))));
            this.xpPanel1.CaptionGradient.Start = System.Drawing.Color.White;
            this.xpPanel1.CaptionGradientMode = System.Drawing.Drawing2D.LinearGradientMode.Horizontal;
            this.xpPanel1.CaptionUnderline = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.tableLayoutPanel1.SetColumnSpan(this.xpPanel1, 8);
            this.xpPanel1.Controls.Add(this.tableLayoutPanel2);
            this.xpPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xpPanel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.xpPanel1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.xpPanel1.HorzAlignment = System.Drawing.StringAlignment.Near;
            this.xpPanel1.ImageItems.ImageSet = null;
            this.xpPanel1.Location = new System.Drawing.Point(3, 224);
            this.xpPanel1.Name = "xpPanel1";
            this.xpPanel1.PanelGradient.End = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(223)))), ((int)(((byte)(247)))));
            this.xpPanel1.PanelGradient.Start = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(223)))), ((int)(((byte)(247)))));
            this.xpPanel1.PanelGradientMode = System.Drawing.Drawing2D.LinearGradientMode.Horizontal;
            this.xpPanel1.Size = new System.Drawing.Size(794, 443);
            this.xpPanel1.TabIndex = 0;
            this.xpPanel1.TextColors.Foreground = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(93)))), ((int)(((byte)(198)))));
            this.xpPanel1.TextHighlightColors.Foreground = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(142)))), ((int)(((byte)(255)))));
            this.xpPanel1.VertAlignment = System.Drawing.StringAlignment.Center;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.generalView1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.generalView2, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(2, 35);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 439F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(790, 403);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // generalView1
            // 
            this.generalView1.AutoScroll = true;
            this.generalView1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(235)))), ((int)(((byte)(240)))));
            this.generalView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.generalView1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(211)))), ((int)(((byte)(194)))), ((int)(((byte)(110)))));
            this.generalView1.Location = new System.Drawing.Point(3, 3);
            this.generalView1.Name = "generalView1";
            this.generalView1.Size = new System.Drawing.Size(389, 433);
            this.generalView1.TabIndex = 0;
            // 
            // generalView2
            // 
            this.generalView2.AutoScroll = true;
            this.generalView2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(235)))), ((int)(((byte)(240)))));
            this.generalView2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.generalView2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(211)))), ((int)(((byte)(194)))), ((int)(((byte)(110)))));
            this.generalView2.Location = new System.Drawing.Point(398, 3);
            this.generalView2.Name = "generalView2";
            this.generalView2.Size = new System.Drawing.Size(389, 433);
            this.generalView2.TabIndex = 0;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(235)))), ((int)(((byte)(240)))));
            this.tableLayoutPanel1.ColumnCount = 8;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9.338024F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.60601F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10.01599F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.02398F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10.10553F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.58267F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 11.2568F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.071F));
            this.tableLayoutPanel1.Controls.Add(this.xpPanel1, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label12, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.textBox1, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label7, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.leg1ComboBox, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.currency1ComboBox, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.dateTimePicker4, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.payFreq1ComboBox, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.dateTimePicker2, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.label6, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.leg2ComboBox, 5, 0);
            this.tableLayoutPanel1.Controls.Add(this.label8, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.label9, 4, 1);
            this.tableLayoutPanel1.Controls.Add(this.label10, 4, 2);
            this.tableLayoutPanel1.Controls.Add(this.label14, 4, 3);
            this.tableLayoutPanel1.Controls.Add(this.label15, 4, 4);
            this.tableLayoutPanel1.Controls.Add(this.currency2ComboBox, 5, 2);
            this.tableLayoutPanel1.Controls.Add(this.dateTimePicker1, 5, 3);
            this.tableLayoutPanel1.Controls.Add(this.dateTimePicker3, 5, 4);
            this.tableLayoutPanel1.Controls.Add(this.label16, 4, 6);
            this.tableLayoutPanel1.Controls.Add(this.label17, 4, 5);
            this.tableLayoutPanel1.Controls.Add(this.reset2ComboBox, 5, 5);
            this.tableLayoutPanel1.Controls.Add(this.payFreq2Combobox, 5, 6);
            this.tableLayoutPanel1.Controls.Add(this.label13, 6, 6);
            this.tableLayoutPanel1.Controls.Add(this.label5, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.label18, 6, 1);
            this.tableLayoutPanel1.Controls.Add(this.label11, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.label19, 6, 2);
            this.tableLayoutPanel1.Controls.Add(this.label22, 6, 5);
            this.tableLayoutPanel1.Controls.Add(this.label21, 6, 3);
            this.tableLayoutPanel1.Controls.Add(this.label20, 6, 4);
            this.tableLayoutPanel1.Controls.Add(this.textBox6, 5, 1);
            this.tableLayoutPanel1.Controls.Add(this.regexTextBox2, 7, 3);
            this.tableLayoutPanel1.Controls.Add(this.textBoxWithDropDown1, 7, 4);
            this.tableLayoutPanel1.Controls.Add(this.regexTextBox1, 7, 6);
            this.tableLayoutPanel1.Controls.Add(this.comboBoxIndex, 7, 5);
            this.tableLayoutPanel1.Controls.Add(this.dayCount2ComboBox, 7, 2);
            this.tableLayoutPanel1.Controls.Add(this.dayCount1ComboBox, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.textBox2, 3, 3);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 7);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(211)))), ((int)(((byte)(194)))), ((int)(((byte)(110)))));
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 9;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(800, 670);
            this.tableLayoutPanel1.TabIndex = 31;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(137)))), ((int)(((byte)(94)))));
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 27);
            this.label1.TabIndex = 0;
            this.label1.Text = Labels.SwapView_InitializeComponent_Leg_1_;
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(137)))), ((int)(((byte)(94)))));
            this.label12.Location = new System.Drawing.Point(3, 133);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(68, 27);
            this.label12.TabIndex = 19;
            this.label12.Text = Labels.SwapView_InitializeComponent_Pay_Freq_;
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(137)))), ((int)(((byte)(94)))));
            this.label3.Location = new System.Drawing.Point(3, 27);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 26);
            this.label3.TabIndex = 4;
            this.label3.Text = Labels.SwapView_InitializeComponent_Notional_;
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(182)))), ((int)(((byte)(64)))));
            this.textBox1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bindingSource1, "Notional", true));
            this.textBox1.Location = new System.Drawing.Point(77, 30);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(111, 20);
            this.textBox1.TabIndex = 10;
            // 
            // bindingSource1
            // 
            this.bindingSource1.DataSource = typeof(AssetModelingModule.Domain.Bond);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(137)))), ((int)(((byte)(94)))));
            this.label7.Location = new System.Drawing.Point(3, 53);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(68, 27);
            this.label7.TabIndex = 8;
            this.label7.Text = Labels.SwapView_InitializeComponent_Currency_;
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // leg1ComboBox
            // 
            this.leg1ComboBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(182)))), ((int)(((byte)(64)))));
            this.leg1ComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.bindingSource1, "Leg", true));
            this.leg1ComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.leg1ComboBox.FormattingEnabled = true;
            this.leg1ComboBox.Location = new System.Drawing.Point(77, 3);
            this.leg1ComboBox.Name = "leg1ComboBox";
            this.leg1ComboBox.Size = new System.Drawing.Size(111, 21);
            this.leg1ComboBox.TabIndex = 31;
            this.leg1ComboBox.SelectedIndexChanged += new System.EventHandler(this.Leg1ComboBoxSelectedIndexChanged);
            // 
            // currency1ComboBox
            // 
            this.currency1ComboBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(182)))), ((int)(((byte)(64)))));
            this.currency1ComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.bindingSource1, "Currency", true));
            this.currency1ComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.currency1ComboBox.FormattingEnabled = true;
            this.currency1ComboBox.Items.AddRange(new object[] {
            "EUR"});
            this.currency1ComboBox.Location = new System.Drawing.Point(77, 56);
            this.currency1ComboBox.Name = "currency1ComboBox";
            this.currency1ComboBox.Size = new System.Drawing.Size(111, 21);
            this.currency1ComboBox.TabIndex = 32;
            // 
            // dateTimePicker4
            // 
            this.dateTimePicker4.CustomFormat = "dd/MM/yyyy";
            this.dateTimePicker4.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.bindingSource1, "Effective", true));
            this.dateTimePicker4.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker4.Location = new System.Drawing.Point(77, 83);
            this.dateTimePicker4.Name = "dateTimePicker4";
            this.dateTimePicker4.Size = new System.Drawing.Size(96, 20);
            this.dateTimePicker4.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(137)))), ((int)(((byte)(94)))));
            this.label2.Location = new System.Drawing.Point(3, 106);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 27);
            this.label2.TabIndex = 1;
            this.label2.Text = Labels.SwapView_InitializeComponent_Maturity_;
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(137)))), ((int)(((byte)(94)))));
            this.label4.Location = new System.Drawing.Point(3, 80);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 26);
            this.label4.TabIndex = 33;
            this.label4.Text = Labels.SwapView_InitializeComponent_Effective_;
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // payFreq1ComboBox
            // 
            this.payFreq1ComboBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(182)))), ((int)(((byte)(64)))));
            this.payFreq1ComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.bindingSource1, "Period", true));
            this.payFreq1ComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.payFreq1ComboBox.FormattingEnabled = true;
            this.payFreq1ComboBox.Location = new System.Drawing.Point(77, 136);
            this.payFreq1ComboBox.Name = "payFreq1ComboBox";
            this.payFreq1ComboBox.Size = new System.Drawing.Size(111, 21);
            this.payFreq1ComboBox.TabIndex = 21;
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.CustomFormat = "dd/MM/yyyy";
            this.dateTimePicker2.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.bindingSource1, "Maturity", true));
            this.dateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker2.Location = new System.Drawing.Point(77, 109);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(96, 20);
            this.dateTimePicker2.TabIndex = 29;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(137)))), ((int)(((byte)(94)))));
            this.label6.Location = new System.Drawing.Point(201, 27);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(74, 26);
            this.label6.TabIndex = 7;
            this.label6.Text = Labels.SwapView_InitializeComponent_Leg_ID;
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // leg2ComboBox
            // 
            this.leg2ComboBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(182)))), ((int)(((byte)(64)))));
            this.leg2ComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.bindingSource2, "Leg", true));
            this.leg2ComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.leg2ComboBox.FormattingEnabled = true;
            this.leg2ComboBox.Items.AddRange(new object[] {
            "PayFloat",
            "ReceivedFloat"});
            this.leg2ComboBox.Location = new System.Drawing.Point(481, 3);
            this.leg2ComboBox.Name = "leg2ComboBox";
            this.leg2ComboBox.Size = new System.Drawing.Size(110, 21);
            this.leg2ComboBox.TabIndex = 35;
            this.leg2ComboBox.SelectedIndexChanged += new System.EventHandler(this.Leg2ComboBoxSelectedIndexChanged);
            // 
            // bindingSource2
            // 
            this.bindingSource2.DataSource = typeof(AssetModelingModule.Domain.Bond);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(137)))), ((int)(((byte)(94)))));
            this.label8.Location = new System.Drawing.Point(401, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(74, 27);
            this.label8.TabIndex = 36;
            this.label8.Text = Labels.SwapView_InitializeComponent_Leg_2_;
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(137)))), ((int)(((byte)(94)))));
            this.label9.Location = new System.Drawing.Point(401, 27);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(74, 26);
            this.label9.TabIndex = 37;
            this.label9.Text = Labels.SwapView_InitializeComponent_Notional_;
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(137)))), ((int)(((byte)(94)))));
            this.label10.Location = new System.Drawing.Point(401, 53);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(74, 27);
            this.label10.TabIndex = 38;
            this.label10.Text = Labels.SwapView_InitializeComponent_Currency_;
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(137)))), ((int)(((byte)(94)))));
            this.label14.Location = new System.Drawing.Point(401, 80);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(74, 26);
            this.label14.TabIndex = 39;
            this.label14.Text = Labels.SwapView_InitializeComponent_Effective_;
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(137)))), ((int)(((byte)(94)))));
            this.label15.Location = new System.Drawing.Point(401, 106);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(74, 27);
            this.label15.TabIndex = 40;
            this.label15.Text = Labels.SwapView_InitializeComponent_Maturity_;
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // currency2ComboBox
            // 
            this.currency2ComboBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(182)))), ((int)(((byte)(64)))));
            this.currency2ComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.bindingSource2, "Currency", true));
            this.currency2ComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.currency2ComboBox.FormattingEnabled = true;
            this.currency2ComboBox.Items.AddRange(new object[] {
            "EUR"});
            this.currency2ComboBox.Location = new System.Drawing.Point(481, 56);
            this.currency2ComboBox.Name = "currency2ComboBox";
            this.currency2ComboBox.Size = new System.Drawing.Size(110, 21);
            this.currency2ComboBox.TabIndex = 42;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.CustomFormat = "dd/MM/yyyy";
            this.dateTimePicker1.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.bindingSource2, "Effective", true));
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker1.Location = new System.Drawing.Point(481, 83);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(96, 20);
            this.dateTimePicker1.TabIndex = 43;
            // 
            // dateTimePicker3
            // 
            this.dateTimePicker3.CustomFormat = "dd/MM/yyyy";
            this.dateTimePicker3.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.bindingSource2, "Maturity", true));
            this.dateTimePicker3.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker3.Location = new System.Drawing.Point(481, 109);
            this.dateTimePicker3.Name = "dateTimePicker3";
            this.dateTimePicker3.Size = new System.Drawing.Size(96, 20);
            this.dateTimePicker3.TabIndex = 44;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(137)))), ((int)(((byte)(94)))));
            this.label16.Location = new System.Drawing.Point(401, 160);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(74, 27);
            this.label16.TabIndex = 41;
            this.label16.Text = Labels.SwapView_InitializeComponent_Pay_Freq_;
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(137)))), ((int)(((byte)(94)))));
            this.label17.Location = new System.Drawing.Point(401, 133);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(74, 27);
            this.label17.TabIndex = 45;
            this.label17.Text = Labels.SwapView_InitializeComponent_Reset_Freq_;
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // reset2ComboBox
            // 
            this.reset2ComboBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(182)))), ((int)(((byte)(64)))));
            this.reset2ComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.bindingSource2, "ResetFrequence", true));
            this.reset2ComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.reset2ComboBox.FormattingEnabled = true;
            this.reset2ComboBox.Location = new System.Drawing.Point(481, 136);
            this.reset2ComboBox.Name = "reset2ComboBox";
            this.reset2ComboBox.Size = new System.Drawing.Size(110, 21);
            this.reset2ComboBox.TabIndex = 46;
            // 
            // payFreq2Combobox
            // 
            this.payFreq2Combobox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(182)))), ((int)(((byte)(64)))));
            this.payFreq2Combobox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.bindingSource2, "Period", true));
            this.payFreq2Combobox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.payFreq2Combobox.FormattingEnabled = true;
            this.payFreq2Combobox.Location = new System.Drawing.Point(481, 163);
            this.payFreq2Combobox.Name = "payFreq2Combobox";
            this.payFreq2Combobox.Size = new System.Drawing.Size(110, 21);
            this.payFreq2Combobox.TabIndex = 47;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(137)))), ((int)(((byte)(94)))));
            this.label13.Location = new System.Drawing.Point(597, 160);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(84, 27);
            this.label13.TabIndex = 20;
            this.label13.Text = Labels.SwapView_InitializeComponent_Spread_;
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(137)))), ((int)(((byte)(94)))));
            this.label5.Location = new System.Drawing.Point(201, 80);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(74, 26);
            this.label5.TabIndex = 15;
            this.label5.Text = Labels.SwapView_InitializeComponent_Coupon_;
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(137)))), ((int)(((byte)(94)))));
            this.label18.Location = new System.Drawing.Point(597, 27);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(84, 26);
            this.label18.TabIndex = 48;
            this.label18.Text = Labels.SwapView_InitializeComponent_Leg_ID;
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(137)))), ((int)(((byte)(94)))));
            this.label11.Location = new System.Drawing.Point(201, 53);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(74, 27);
            this.label11.TabIndex = 18;
            this.label11.Text = Labels.SwapView_InitializeComponent_Day_Count_;
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(137)))), ((int)(((byte)(94)))));
            this.label19.Location = new System.Drawing.Point(597, 53);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(84, 27);
            this.label19.TabIndex = 49;
            this.label19.Text = Labels.SwapView_InitializeComponent_Day_Count_;
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(137)))), ((int)(((byte)(94)))));
            this.label22.Location = new System.Drawing.Point(597, 133);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(84, 27);
            this.label22.TabIndex = 53;
            this.label22.Text = Labels.SwapView_InitializeComponent_Index_;
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(137)))), ((int)(((byte)(94)))));
            this.label21.Location = new System.Drawing.Point(597, 80);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(84, 26);
            this.label21.TabIndex = 52;
            this.label21.Text = Labels.SwapView_InitializeComponent_Latest_Index_;
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(137)))), ((int)(((byte)(94)))));
            this.label20.Location = new System.Drawing.Point(597, 106);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(84, 27);
            this.label20.TabIndex = 51;
            this.label20.Text = Labels.SwapView_InitializeComponent_Tenor_;
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBox6
            // 
            this.textBox6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(182)))), ((int)(((byte)(64)))));
            this.textBox6.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bindingSource2, "Notional", true));
            this.textBox6.Location = new System.Drawing.Point(481, 30);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(110, 20);
            this.textBox6.TabIndex = 58;
            // 
            // regexTextBox2
            // 
            this.regexTextBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(182)))), ((int)(((byte)(64)))));
            this.regexTextBox2.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bindingSource2, "LatestIndex", true, System.Windows.Forms.DataSourceUpdateMode.OnValidation, ""));
            this.regexTextBox2.Location = new System.Drawing.Point(687, 83);
            this.regexTextBox2.Name = "regexTextBox2";
            this.regexTextBox2.Regex = "^[-]?\\d{0,2}(\\.\\d{0,5})?$";
            this.regexTextBox2.Size = new System.Drawing.Size(110, 20);
            this.regexTextBox2.TabIndex = 60;
            this.regexTextBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxWithDropDown1
            // 
            this.textBoxWithDropDown1.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.bindingSource2, "Tenor", true));
            this.textBoxWithDropDown1.Location = new System.Drawing.Point(687, 109);
            this.textBoxWithDropDown1.Name = "textBoxWithDropDown1";
            this.textBoxWithDropDown1.Size = new System.Drawing.Size(110, 21);
            this.textBoxWithDropDown1.TabIndex = 62;
            this.textBoxWithDropDown1.Value = 0;
            // 
            // regexTextBox1
            // 
            this.regexTextBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(182)))), ((int)(((byte)(64)))));
            this.regexTextBox1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bindingSource2, "Spread", true, System.Windows.Forms.DataSourceUpdateMode.OnValidation, ""));
            this.regexTextBox1.Location = new System.Drawing.Point(687, 163);
            this.regexTextBox1.Name = "regexTextBox1";
            this.regexTextBox1.Regex = "^[-]?\\d{0,1}(\\.\\d{0,2})?$";
            this.regexTextBox1.Size = new System.Drawing.Size(108, 20);
            this.regexTextBox1.TabIndex = 59;
            this.regexTextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // comboBoxIndex
            // 
            this.comboBoxIndex.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.bindingSource2, "BondIndex", true));
            this.comboBoxIndex.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxIndex.FormattingEnabled = true;
            this.comboBoxIndex.Location = new System.Drawing.Point(687, 136);
            this.comboBoxIndex.Name = "comboBoxIndex";
            this.comboBoxIndex.Size = new System.Drawing.Size(110, 21);
            this.comboBoxIndex.TabIndex = 61;
            // 
            // dayCount2ComboBox
            // 
            this.dayCount2ComboBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(182)))), ((int)(((byte)(64)))));
            this.dayCount2ComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.bindingSource2, "DayCountConvention", true));
            this.dayCount2ComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.dayCount2ComboBox.FormattingEnabled = true;
            this.dayCount2ComboBox.Location = new System.Drawing.Point(687, 56);
            this.dayCount2ComboBox.Name = "dayCount2ComboBox";
            this.dayCount2ComboBox.Size = new System.Drawing.Size(110, 21);
            this.dayCount2ComboBox.TabIndex = 50;
            // 
            // dayCount1ComboBox
            // 
            this.dayCount1ComboBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(182)))), ((int)(((byte)(64)))));
            this.dayCount1ComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.bindingSource1, "DayCountConvention", true));
            this.dayCount1ComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.dayCount1ComboBox.FormattingEnabled = true;
            this.dayCount1ComboBox.Location = new System.Drawing.Point(281, 56);
            this.dayCount1ComboBox.Name = "dayCount1ComboBox";
            this.dayCount1ComboBox.Size = new System.Drawing.Size(111, 21);
            this.dayCount1ComboBox.TabIndex = 34;
            // 
            // textBox2
            // 
            this.textBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(182)))), ((int)(((byte)(64)))));
            this.textBox2.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bindingSource1, "Coupon", true));
            this.textBox2.Location = new System.Drawing.Point(281, 83);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(111, 20);
            this.textBox2.TabIndex = 25;
            // 
            // panel1
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.panel1, 8);
            this.panel1.Controls.Add(this.label23);
            this.panel1.Controls.Add(this.reofferedBookPriceTextBox);
            this.panel1.Location = new System.Drawing.Point(3, 190);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(794, 28);
            this.panel1.TabIndex = 63;
            // 
            // reofferedBookPriceTextBox
            // 
            this.reofferedBookPriceTextBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(182)))), ((int)(((byte)(64)))));
            // this.reofferedBookPriceTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bindingSource1, "BookPrice", true));
            this.reofferedBookPriceTextBox.Location = new System.Drawing.Point(400, 4);
            this.reofferedBookPriceTextBox.Name = "reofferedBookPriceTextBox";
            this.reofferedBookPriceTextBox.Size = new System.Drawing.Size(111, 20);
            this.reofferedBookPriceTextBox.TabIndex = 26;
            this.reofferedBookPriceTextBox.Click += new System.EventHandler(this.reofferedBookPriceTextBox_Click);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(137)))), ((int)(((byte)(94)))));
            this.label23.Location = new System.Drawing.Point(261, 7);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(133, 13);
            this.label23.TabIndex = 27;
            this.label23.Text = Labels.SwapView_InitializeComponent_Reoffered_Book_Price_;
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // SwapView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "SwapView";
            this.Size = new System.Drawing.Size(800, 670);
            this.xpPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource2)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox leg1ComboBox;
        private System.Windows.Forms.ComboBox currency1ComboBox;
        private System.Windows.Forms.DateTimePicker dateTimePicker4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox payFreq1ComboBox;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.ComboBox dayCount1ComboBox;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox leg2ComboBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox currency2ComboBox;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.DateTimePicker dateTimePicker3;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ComboBox reset2ComboBox;
        private System.Windows.Forms.ComboBox payFreq2Combobox;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.ComboBox dayCount2ComboBox;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private UIComponents.XPPanel xpPanel1;
        private GeneralView generalView1;
        private System.Windows.Forms.TextBox textBox6;
        private GeneralView generalView2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.BindingSource bindingSource1;
        private System.Windows.Forms.BindingSource bindingSource2;
        private AssetModelingModule.Controls.RegexTextBox regexTextBox1;
        private AssetModelingModule.Controls.RegexTextBox regexTextBox2;
        private System.Windows.Forms.ComboBox comboBoxIndex;
        private AssetModelingModule.Controls.TextBoxWithDropDown textBoxWithDropDown1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox reofferedBookPriceTextBox;

    }
}
