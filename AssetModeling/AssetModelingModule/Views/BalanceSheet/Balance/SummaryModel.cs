namespace AssetModelingModule.Views.BalanceSheet.Balance
{
    public enum SummaryType
    {
        None,
        Normal,
        Header,
        Footer,
        MasterFooter
    }

    public class SummaryModel
    {
        private string description;
        private decimal value;
        private SummaryType type;

        public SummaryModel(string description, decimal value)
        {
            this.description = description;
            this.value = value;
        }

        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        public decimal Value
        {
            get { return value; }
            set { this.value = value; }
        }

        public SummaryType Type
        {
            get { return type; }
            set { type = value; }
        }
    }
}