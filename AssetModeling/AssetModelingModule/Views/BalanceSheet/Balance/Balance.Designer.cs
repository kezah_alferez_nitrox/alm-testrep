﻿using AssetModelingModule.Controls;
using AssetModelingModule.Resources.Language;

namespace AssetModelingModule.Views.BalanceSheet.Balance
{
    partial class Balance
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.treeViewContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.addASubcategoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.renameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.balanceSheetSplitContainer = new System.Windows.Forms.SplitContainer();
            this.balanceHeaderPanel = new System.Windows.Forms.Panel();
            this.upButton = new System.Windows.Forms.Button();
            this.addNewButton = new System.Windows.Forms.Button();
            this.selectedNodeLabel = new System.Windows.Forms.Label();
            this.detailsGridContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.modelAnalysisToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.sortAscendingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sortDescendingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.applyValueToEntireColumnToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.applyValueToRowsAboveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.applyValueToRowsBelowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.applyValueToSelectedRowsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.detailsGridRowHeaderContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.modelAnalysisLineStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteRowsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.treeView = new DataTreeView();
            this.detailsGrid = new DataGridViewEx();
            this.summaryGrid = new DataGridViewEx();
            this.treeViewContextMenuStrip.SuspendLayout();
            this.balanceSheetSplitContainer.Panel1.SuspendLayout();
            this.balanceSheetSplitContainer.Panel2.SuspendLayout();
            this.balanceSheetSplitContainer.SuspendLayout();
            this.balanceHeaderPanel.SuspendLayout();
            this.detailsGridContextMenuStrip.SuspendLayout();
            this.detailsGridRowHeaderContextMenuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.detailsGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.summaryGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // treeViewContextMenuStrip
            // 
            this.treeViewContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addASubcategoryToolStripMenuItem,
            this.renameToolStripMenuItem,
            this.deleteToolStripMenuItem});
            this.treeViewContextMenuStrip.Name = "treeViewContextMenuStrip";
            this.treeViewContextMenuStrip.Size = new System.Drawing.Size(174, 70);
            this.treeViewContextMenuStrip.Opening += new System.ComponentModel.CancelEventHandler(this.treeViewContextMenuStrip_Opening);
            // 
            // addASubcategoryToolStripMenuItem
            // 
            this.addASubcategoryToolStripMenuItem.Name = "addASubcategoryToolStripMenuItem";
            this.addASubcategoryToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.addASubcategoryToolStripMenuItem.Text = Labels.Balance_InitializeComponent_Add_a_subcategory;
            this.addASubcategoryToolStripMenuItem.Click += new System.EventHandler(this.addASubcategoryToolStripMenuItem_Click);
            // 
            // renameToolStripMenuItem
            // 
            this.renameToolStripMenuItem.Name = "renameToolStripMenuItem";
            this.renameToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F2;
            this.renameToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.renameToolStripMenuItem.Text = Labels.Balance_InitializeComponent_Rename;
            this.renameToolStripMenuItem.Click += new System.EventHandler(this.renameToolStripMenuItem_Click);
            // 
            // deleteToolStripMenuItem
            // 
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Delete)));
            this.deleteToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.deleteToolStripMenuItem.Text = Labels.Balance_InitializeComponent_Delete;
            this.deleteToolStripMenuItem.Click += new System.EventHandler(this.deleteToolStripMenuItem_Click);
            // 
            // balanceSheetSplitContainer
            // 
            this.balanceSheetSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.balanceSheetSplitContainer.Location = new System.Drawing.Point(0, 0);
            this.balanceSheetSplitContainer.Name = "balanceSheetSplitContainer";
            // 
            // balanceSheetSplitContainer.Panel1
            // 
            this.balanceSheetSplitContainer.Panel1.Controls.Add(this.treeView);
            this.balanceSheetSplitContainer.Panel1MinSize = 120;
            // 
            // balanceSheetSplitContainer.Panel2
            // 
            this.balanceSheetSplitContainer.Panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.balanceSheetSplitContainer.Panel2.Controls.Add(this.detailsGrid);
            this.balanceSheetSplitContainer.Panel2.Controls.Add(this.summaryGrid);
            this.balanceSheetSplitContainer.Panel2.Controls.Add(this.balanceHeaderPanel);
            this.balanceSheetSplitContainer.Size = new System.Drawing.Size(922, 497);
            this.balanceSheetSplitContainer.SplitterDistance = 157;
            this.balanceSheetSplitContainer.TabIndex = 3;
            // 
            // balanceHeaderPanel
            // 
            this.balanceHeaderPanel.BackColor = System.Drawing.SystemColors.ControlDark;
            this.balanceHeaderPanel.Controls.Add(this.upButton);
            this.balanceHeaderPanel.Controls.Add(this.addNewButton);
            this.balanceHeaderPanel.Controls.Add(this.selectedNodeLabel);
            this.balanceHeaderPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.balanceHeaderPanel.Location = new System.Drawing.Point(0, 0);
            this.balanceHeaderPanel.Name = "balanceHeaderPanel";
            this.balanceHeaderPanel.Size = new System.Drawing.Size(761, 34);
            this.balanceHeaderPanel.TabIndex = 0;
            // 
            // upButton
            // 
            this.upButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.upButton.Location = new System.Drawing.Point(678, 6);
            this.upButton.Name = "upButton";
            this.upButton.Size = new System.Drawing.Size(75, 23);
            this.upButton.TabIndex = 5;
            this.upButton.Text = Labels.Balance_InitializeComponent_Up;
            this.upButton.UseVisualStyleBackColor = true;
            this.upButton.Click += new System.EventHandler(this.upButton_Click);
            // 
            // addNewButton
            // 
            this.addNewButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.addNewButton.Enabled = false;
            this.addNewButton.Location = new System.Drawing.Point(597, 6);
            this.addNewButton.Name = "addNewButton";
            this.addNewButton.Size = new System.Drawing.Size(75, 23);
            this.addNewButton.TabIndex = 4;
            this.addNewButton.Text = Labels.Balance_InitializeComponent_Add_New;
            this.addNewButton.UseVisualStyleBackColor = true;
            this.addNewButton.Click += new System.EventHandler(this.addNewButton_Click);
            // 
            // selectedNodeLabel
            // 
            this.selectedNodeLabel.AutoSize = true;
            this.selectedNodeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.selectedNodeLabel.ForeColor = System.Drawing.Color.White;
            this.selectedNodeLabel.Location = new System.Drawing.Point(4, 4);
            this.selectedNodeLabel.Name = "selectedNodeLabel";
            this.selectedNodeLabel.Size = new System.Drawing.Size(0, 24);
            this.selectedNodeLabel.TabIndex = 0;
            // 
            // detailsGridContextMenuStrip
            // 
            this.detailsGridContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.modelAnalysisToolStripMenuItem,
            this.toolStripSeparator1,
            this.sortAscendingToolStripMenuItem,
            this.sortDescendingToolStripMenuItem,
            this.toolStripSeparator2,
            this.applyValueToEntireColumnToolStripMenuItem,
            this.applyValueToRowsAboveToolStripMenuItem,
            this.applyValueToRowsBelowToolStripMenuItem,
            this.applyValueToSelectedRowsToolStripMenuItem});
            this.detailsGridContextMenuStrip.Name = "detailsGridContextMenuStrip";
            this.detailsGridContextMenuStrip.Size = new System.Drawing.Size(231, 170);
            this.detailsGridContextMenuStrip.Opening += new System.ComponentModel.CancelEventHandler(this.detailsGridContextMenuStrip_Opening);
            // 
            // modelAnalysisToolStripMenuItem
            // 
            this.modelAnalysisToolStripMenuItem.Name = "modelAnalysisToolStripMenuItem";
            this.modelAnalysisToolStripMenuItem.Size = new System.Drawing.Size(230, 22);
            this.modelAnalysisToolStripMenuItem.Text = Labels.Balance_InitializeComponent_Model_Analysis;
            this.modelAnalysisToolStripMenuItem.Click += new System.EventHandler(this.modelAnalysisToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(227, 6);
            // 
            // sortAscendingToolStripMenuItem
            // 
            this.sortAscendingToolStripMenuItem.Name = "sortAscendingToolStripMenuItem";
            this.sortAscendingToolStripMenuItem.Size = new System.Drawing.Size(230, 22);
            this.sortAscendingToolStripMenuItem.Text = "Sort Ascending";
            this.sortAscendingToolStripMenuItem.Click += new System.EventHandler(this.sortAscendingToolStripMenuItem_Click);
            // 
            // sortDescendingToolStripMenuItem
            // 
            this.sortDescendingToolStripMenuItem.Name = "sortDescendingToolStripMenuItem";
            this.sortDescendingToolStripMenuItem.Size = new System.Drawing.Size(230, 22);
            this.sortDescendingToolStripMenuItem.Text = "Sort Descending";
            this.sortDescendingToolStripMenuItem.Click += new System.EventHandler(this.sortDescendingToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(227, 6);
            // 
            // applyValueToEntireColumnToolStripMenuItem
            // 
            this.applyValueToEntireColumnToolStripMenuItem.Name = "applyValueToEntireColumnToolStripMenuItem";
            this.applyValueToEntireColumnToolStripMenuItem.Size = new System.Drawing.Size(230, 22);
            this.applyValueToEntireColumnToolStripMenuItem.Text = Labels.Balance_InitializeComponent_Apply_Value_to_Entire_Column;
            this.applyValueToEntireColumnToolStripMenuItem.Click += new System.EventHandler(this.applyValueToEntireColumnToolStripMenuItem_Click);
            // 
            // applyValueToRowsAboveToolStripMenuItem
            // 
            this.applyValueToRowsAboveToolStripMenuItem.Name = "applyValueToRowsAboveToolStripMenuItem";
            this.applyValueToRowsAboveToolStripMenuItem.Size = new System.Drawing.Size(230, 22);
            this.applyValueToRowsAboveToolStripMenuItem.Text = Labels.Balance_InitializeComponent_Apply_Value_to_Rows_Above;
            this.applyValueToRowsAboveToolStripMenuItem.Click += new System.EventHandler(this.applyValueToRowsAboveToolStripMenuItem_Click);
            // 
            // applyValueToRowsBelowToolStripMenuItem
            // 
            this.applyValueToRowsBelowToolStripMenuItem.Name = "applyValueToRowsBelowToolStripMenuItem";
            this.applyValueToRowsBelowToolStripMenuItem.Size = new System.Drawing.Size(230, 22);
            this.applyValueToRowsBelowToolStripMenuItem.Text = Labels.Balance_InitializeComponent_Apply_Value_to_Rows_Below;
            this.applyValueToRowsBelowToolStripMenuItem.Click += new System.EventHandler(this.applyValueToRowsBelowToolStripMenuItem_Click);
            // 
            // applyValueToSelectedRowsToolStripMenuItem
            // 
            this.applyValueToSelectedRowsToolStripMenuItem.Name = "applyValueToSelectedRowsToolStripMenuItem";
            this.applyValueToSelectedRowsToolStripMenuItem.Size = new System.Drawing.Size(230, 22);
            this.applyValueToSelectedRowsToolStripMenuItem.Text = Labels.Balance_InitializeComponent_Apply_Value_to_Selected_Rows;
            this.applyValueToSelectedRowsToolStripMenuItem.Click += new System.EventHandler(this.applyValueToSelectedRowsToolStripMenuItem_Click);
            // 
            // detailsGridRowHeaderContextMenuStrip
            // 
            this.detailsGridRowHeaderContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.modelAnalysisLineStripMenuItem1,
            this.deleteRowsToolStripMenuItem});
            this.detailsGridRowHeaderContextMenuStrip.Name = "detailsGridRowHeaderContextMenuStrip";
            this.detailsGridRowHeaderContextMenuStrip.Size = new System.Drawing.Size(155, 48);
            this.detailsGridRowHeaderContextMenuStrip.Opening += new System.ComponentModel.CancelEventHandler(this.detailsGridRowHeaderContextMenuStrip_Opening);
            // 
            // modelAnalysisLineStripMenuItem1
            // 
            this.modelAnalysisLineStripMenuItem1.Name = "modelAnalysisLineStripMenuItem1";
            this.modelAnalysisLineStripMenuItem1.Size = new System.Drawing.Size(154, 22);
            this.modelAnalysisLineStripMenuItem1.Text = Labels.Balance_InitializeComponent_Model_Analysis;
            this.modelAnalysisLineStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // deleteRowsToolStripMenuItem
            // 
            this.deleteRowsToolStripMenuItem.Name = "deleteRowsToolStripMenuItem";
            this.deleteRowsToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.deleteRowsToolStripMenuItem.Text = Labels.Balance_InitializeComponent_Delete_Row_s_;
            this.deleteRowsToolStripMenuItem.Click += new System.EventHandler(this.deleteRowsToolStripMenuItem_Click);
            // 
            // treeView
            // 
            this.treeView.ContextMenuStrip = this.treeViewContextMenuStrip;
            this.treeView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeView.HideSelection = false;
            this.treeView.Location = new System.Drawing.Point(0, 0);
            this.treeView.Name = "treeView";
            this.treeView.Size = new System.Drawing.Size(157, 497);
            this.treeView.TabIndex = 0;
            this.treeView.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView_AfterSelect);
            // 
            // detailsGrid
            // 
            this.detailsGrid.AllowUserToAddRows = false;
            this.detailsGrid.BackgroundColor = System.Drawing.SystemColors.Window;
            this.detailsGrid.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
            this.detailsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.detailsGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.detailsGrid.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.detailsGrid.Location = new System.Drawing.Point(0, 34);
            this.detailsGrid.Name = "detailsGrid";
            this.detailsGrid.Size = new System.Drawing.Size(761, 463);
            this.detailsGrid.TabIndex = 1;
            this.detailsGrid.Visible = false;
            this.detailsGrid.CellContextMenuStripNeeded += new System.Windows.Forms.DataGridViewCellContextMenuStripNeededEventHandler(this.detailsGrid_CellContextMenuStripNeeded);
            // 
            // summaryGrid
            // 
            this.summaryGrid.AllowUserToAddRows = false;
            this.summaryGrid.BackgroundColor = System.Drawing.SystemColors.Window;
            this.summaryGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.summaryGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.summaryGrid.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.summaryGrid.Location = new System.Drawing.Point(0, 34);
            this.summaryGrid.Name = "summaryGrid";
            this.summaryGrid.Size = new System.Drawing.Size(761, 463);
            this.summaryGrid.TabIndex = 1;
            this.summaryGrid.Visible = false;
            // 
            // Balance
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.balanceSheetSplitContainer);
            this.DoubleBuffered = true;
            this.Name = "Balance";
            this.Size = new System.Drawing.Size(922, 497);
            this.Load += new System.EventHandler(this.Balance_Load);
            this.treeViewContextMenuStrip.ResumeLayout(false);
            this.balanceSheetSplitContainer.Panel1.ResumeLayout(false);
            this.balanceSheetSplitContainer.Panel2.ResumeLayout(false);
            this.balanceSheetSplitContainer.ResumeLayout(false);
            this.balanceHeaderPanel.ResumeLayout(false);
            this.balanceHeaderPanel.PerformLayout();
            this.detailsGridContextMenuStrip.ResumeLayout(false);
            this.detailsGridRowHeaderContextMenuStrip.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.detailsGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.summaryGrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip treeViewContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem addASubcategoryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem renameToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
        private System.Windows.Forms.SplitContainer balanceSheetSplitContainer;
        private DataTreeView treeView;
        private DataGridViewEx detailsGrid;
        private DataGridViewEx summaryGrid;
        private System.Windows.Forms.Panel balanceHeaderPanel;
        private System.Windows.Forms.Button addNewButton;
        private System.Windows.Forms.Label selectedNodeLabel;
        private System.Windows.Forms.ContextMenuStrip detailsGridContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem sortAscendingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sortDescendingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem applyValueToEntireColumnToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem applyValueToRowsAboveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem applyValueToRowsBelowToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem applyValueToSelectedRowsToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip detailsGridRowHeaderContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem deleteRowsToolStripMenuItem;
        private System.Windows.Forms.Button upButton;
        private System.Windows.Forms.ToolStripMenuItem modelAnalysisToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem modelAnalysisLineStripMenuItem1;
    }
}
