using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using AssetModelingModule.BusinessLogic;
using AssetModelingModule.Domain;
using AssetModelingModule.Resources.Language;

namespace AssetModelingModule.Views.BalanceSheet.Balance
{
    public class SummaryGridController
    {
        private readonly DataGridView grid;

        private DataGridViewTextBoxColumn descriptionDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn valueDataGridViewTextBoxColumn;

        private DataGridViewColumn[] columns;

        private Category category;

        public SummaryGridController(DataGridView grid)
        {
            this.grid = grid;
            grid.ReadOnly = true;
            grid.AutoGenerateColumns = false;
            grid.AllowUserToAddRows = false;
            grid.AllowUserToDeleteRows = false;
            grid.AllowUserToResizeRows = false;
            grid.RowTemplate.Height = Constants.GRID_ROW_HEIGHT;
            InitColumns();

            this.grid.CellFormatting += grid_CellFormatting;
        }

        private void InitColumns()
        {
            descriptionDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            valueDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();

            // 
            // summaryDescriptionDataGridViewTextBoxColumn
            // 
            descriptionDataGridViewTextBoxColumn.DataPropertyName = "Description";
            descriptionDataGridViewTextBoxColumn.HeaderText = Labels.SummaryGridController_InitColumns_Description;
            descriptionDataGridViewTextBoxColumn.Name = "Description";
            descriptionDataGridViewTextBoxColumn.Width = 400;
            descriptionDataGridViewTextBoxColumn.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

            // 
            // summaryValueDataGridViewTextBoxColumn
            // 
            valueDataGridViewTextBoxColumn.DataPropertyName = "Value";
            valueDataGridViewTextBoxColumn.HeaderText = Labels.SummaryGridController_InitColumns_Book_Value;
            valueDataGridViewTextBoxColumn.Name = "Value";
            valueDataGridViewTextBoxColumn.Width = 100;
            valueDataGridViewTextBoxColumn.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            valueDataGridViewTextBoxColumn.DefaultCellStyle.Format = Constants.MONEY_CELL_FORMAT;
            valueDataGridViewTextBoxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            columns = new DataGridViewColumn[]
                          {
                              descriptionDataGridViewTextBoxColumn,
                              valueDataGridViewTextBoxColumn,
                          };

            grid.Columns.Clear();
            grid.Columns.AddRange(columns);
        }

        private void grid_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            SummaryModel summary = GetDataItemAtRowIndex(e.RowIndex);
            if (null == summary) return;

            string colName = grid.Columns[e.ColumnIndex].Name;

            switch (summary.Type)
            {
                case SummaryType.Header:
                    e.CellStyle.BackColor = Constants.Colors.TableHeader1;
                    e.CellStyle.Font = new Font(e.CellStyle.Font, FontStyle.Bold);
                    if ("Value" == colName) e.Value = null;
                    break;
                case SummaryType.Footer:
                    e.CellStyle.BackColor = Constants.Colors.TableFooter1;
                    e.CellStyle.Font = new Font(e.CellStyle.Font, FontStyle.Bold);
                    break;
                case SummaryType.MasterFooter:
                    e.CellStyle.BackColor = Constants.Colors.TableFooter2;
                    e.CellStyle.Font = new Font(e.CellStyle.Font, FontStyle.Bold);
                    break;
                default: // SummaryType.Normal, SummaryType.None
                    break;
            }
        }

        private SummaryModel GetDataItemAtRowIndex(int rowIndex)
        {
            if (0 > rowIndex) return null;

            return grid.Rows[rowIndex].DataBoundItem as SummaryModel;
        }

        public void Refresh(Category newCategory)
        {
            category = newCategory;

            if (category == null)
            {
                grid.DataSource = null;
                return;
            }

            BindingList<SummaryModel> gridData = new BindingList<SummaryModel>();

            //PersistenceSession.Evict(category);
            //category = Category.Find(category.Id);
            
            if (category.Parent == null)// Constants.ROOT_CATEGORY_ID == category.Id)
            {
                decimal total = 0;

                foreach (Category rootSubCategory in category.Children)
                {
                    SummaryModel headerSummary = new SummaryModel(string.Format("{0}", rootSubCategory.Label), 0);
                    headerSummary.Type = SummaryType.Header;
                    gridData.Add(headerSummary);

                    decimal subTotal = 0;
                    foreach (Category subCategory in rootSubCategory.Children)
                    {
                        decimal _totalbond = 0;

                        foreach (Bond bond in subCategory.Bonds)
                        {
                            if (bond.Exclude) continue;
                            if (bond.Attrib == BondAttrib.Notional) continue;

                            _totalbond += CurrencyBL.ConvertToValuationCurrency(bond.BookValue.HasValue ? bond.BookValue.Value : 0, bond.Currency);
                        }

                        if (subCategory.Children.Count > 0)
                        {
                            _totalbond += tBound(subCategory);
                        }
                        SummaryModel s = new SummaryModel(subCategory.Label, _totalbond);
                        subTotal += s.Value;
                        gridData.Add(s);
                    }
                    if (rootSubCategory.Label != "Asset")
                        total += subTotal;
                    SummaryModel footerSummary = new SummaryModel(string.Format(Labels.SummaryGridController_Refresh_Total__0_, rootSubCategory.Label), subTotal);
                    footerSummary.Type = SummaryType.Footer;
                    gridData.Add(footerSummary);
                }
                SummaryModel totalSummary = new SummaryModel(Labels.SummaryGridController_Refresh_Total_Liabilities___Equities, total);
                totalSummary.Type = SummaryType.MasterFooter;
                gridData.Add(totalSummary);
            }
            else
            {
                decimal total = 0;
                foreach (Category subCategory in category.Children)
                {
                    decimal _totalbond = 0;
                    foreach (Bond bond in subCategory.Bonds)
                    {
                        if (bond.Exclude) continue;
                        if (bond.Attrib == BondAttrib.Notional) continue;
                        _totalbond += CurrencyBL.ConvertToValuationCurrency(bond.BookValue.HasValue ? bond.BookValue.Value : 0, bond.Currency);
                    }

                    if (subCategory.Children.Count > 0)
                    {
                        _totalbond += tBound(subCategory);
                    }

                    SummaryModel s = new SummaryModel(subCategory.Label, _totalbond);
                    total += s.Value;
                    gridData.Add(s);
                }

                SummaryModel totalSummary = new SummaryModel(string.Format(Labels.SummaryGridController_Refresh_Total___0__, Param.ValuationCurrency.Symbol), total);
                totalSummary.Type = SummaryType.Footer;
                gridData.Add(totalSummary);
            }

            grid.DataSource = gridData;
        }

        private decimal tBound(Category subCategory)
        {
            decimal _tBound = 0;
            if (subCategory.Children.Count == 0)
                _tBound = bondPerCategory(subCategory);
            else
            {
                foreach (Category cat in subCategory.Children)
                    _tBound = _tBound + tBound(cat);
            }
            return _tBound;
        }

        private decimal bondPerCategory(Category cat)
        {
            decimal _bondPerCategory = 0;
            foreach (Bond bond in cat.Bonds)
            {
                if (bond.Exclude) continue;
                if (bond.Attrib == BondAttrib.Notional) continue;
                _bondPerCategory += CurrencyBL.ConvertToValuationCurrency(bond.BookValue.HasValue ? bond.BookValue.Value : 0, bond.Currency);
            }
            return _bondPerCategory;
        }

        public void ClipboardCopy()
        {
            if (grid.GetCellCount(DataGridViewElementStates.Selected) > 0)
            {
                DataObject data = grid.GetClipboardContent();
                if (data != null)
                {
                    Clipboard.SetDataObject(grid.GetClipboardContent());
                }
            }
        }
    }
}