using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using AssetModelingModule.BusinessLogic;
using AssetModelingModule.Domain;
using AssetModelingModule.Properties;
using System.Linq;
using NHibernate.Mapping;

namespace AssetModelingModule.Views.BalanceSheet.Balance
{
    public partial class Balance : UserControl
    {
        private Category _selectedCategory;

        private readonly DetailsGridController _detailsController;
        private readonly SummaryGridController _summaryController;

        private GridMode _gridMode = GridMode.None;

        protected enum GridMode
        {
            None,
            Detail,
            Summary
        }

        private int _menuColumnIndex;
        private int _menuRowIndex;

        public Balance()
        {
            InitializeComponent();

            detailsGrid.BackgroundImage = Properties.Resources.logo400x400;
            summaryGrid.BackgroundImage = Properties.Resources.logo400x400;

            if (!DesignMode && !Tools.InDesignMode)
            {
                _detailsController = new DetailsGridController(detailsGrid);
                _summaryController = new SummaryGridController(summaryGrid);
            }

            detailsGrid.EditingControlShowing += detailsGrid_EditingControlShowing;

            detailsGrid.RowTemplate.HeaderCell.ContextMenuStrip = detailsGridRowHeaderContextMenuStrip;
        }

        private void Balance_Load(object sender, EventArgs e)
        {
            if (!DesignMode && !Tools.InDesignMode)
            {
                treeView.LoadNodes();
                if (treeView.Nodes.Count > 0) treeView.SelectedNode = treeView.Nodes[0];
            }
        }

        private void detailsGrid_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            //todo : move to grid controller
            if (e.Control is TextBox)
            {
                _menuColumnIndex = detailsGrid.CurrentCell.ColumnIndex;
                _menuRowIndex = detailsGrid.CurrentCell.RowIndex;
                e.Control.ContextMenuStrip = detailsGridContextMenuStrip;
            }
        }

        private void addNewButton_Click(object sender, EventArgs e)
        {
            AddNewBondsPrompt addNewBondsPrompt = new AddNewBondsPrompt();
            if (addNewBondsPrompt.ShowDialog() == DialogResult.OK)
            {
                //var position = detailsGrid.RowCount;
                List<Bond> addedBonds =  _detailsController.AddNewBonds(addNewBondsPrompt.RowCount, addNewBondsPrompt.Type);
                PersistenceSession.Flush();
                _detailsController.EditBond(addedBonds[0]);
                _detailsController.Refresh(_selectedCategory, false);
            }
        }

        private void treeViewContextMenuStrip_Opening(object sender, CancelEventArgs e)
        {
            if (null == treeView.SelectedNode)
            {
                treeView.Enabled = false;
                return;
            }
            treeView.Enabled = true;

            deleteToolStripMenuItem.Enabled = CanDeleteNode(treeView.SelectedNode);

            renameToolStripMenuItem.Enabled = CanRenameNode(treeView.SelectedNode);

            addASubcategoryToolStripMenuItem.Enabled = CanAddChildNode(treeView.SelectedNode);
        }

        private static bool CanDeleteNode(TreeNode node)
        {
            Category category = node.Tag as Category;

            return (0 == node.Nodes.Count && node.Level > 1 &&
                category != null && !category.ReadOnly && category.Bonds.Count == 0);
        }

        private static bool CanRenameNode(TreeNode node)
        {
            Category category = node.Tag as Category;

            return (category != null && !category.ReadOnly);
        }

        private static bool CanAddChildNode(TreeNode node)
        {
            Category category = node.Tag as Category;

            return category != null && category.Type == CategoryType.Normal &&
                node.Level > 0 && (category.Bonds == null || category.Bonds.Count == 0);
        }

        private void treeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            PersistenceSession.Flush();

            BalanceBL.AjustBalanceIfNeeded();

            Category category = e.Node.Tag as Category;
            if (null == category) return;

            Cursor.Current = Cursors.WaitCursor;

            _selectedCategory = category;

            selectedNodeLabel.Text = e.Node.Text;

            if (category.Type == CategoryType.CashAdj || category.Type == CategoryType.Treasury || e.Node.Nodes.Count != 0)
                addNewButton.Enabled = false;
            else
                addNewButton.Enabled = true;

            GridMode newGridmode = (0 == _selectedCategory.Children.Count) ? GridMode.Detail : GridMode.Summary;

            if (_gridMode != newGridmode)
            {
                detailsGrid.Visible = (newGridmode == GridMode.Detail);
                summaryGrid.Visible = (newGridmode == GridMode.Summary);
                _gridMode = newGridmode;
            }

            switch (_gridMode)
            {
                case GridMode.Detail:
                    _detailsController.Refresh(_selectedCategory, false);
                    break;
                default: //case GridMode.Summary:
                    _summaryController.Refresh(_selectedCategory);
                    break;
            }

            Cursor.Current = Cursors.Default;
        }

        #region "TreeView modification"

        private void addASubcategoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(!CanAddChildNode(treeView.SelectedNode)) return;

            treeView.AddNode();
        }

        private void renameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!CanRenameNode(treeView.SelectedNode)) return;
            treeView.RenameNode();
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!CanDeleteNode(treeView.SelectedNode)) return;

            treeView.DeleteNode();
        }

        #endregion

        private void detailsGrid_CellContextMenuStripNeeded(object sender,
                                                            DataGridViewCellContextMenuStripNeededEventArgs e)
        {
            _menuColumnIndex = e.ColumnIndex;
            _menuRowIndex = e.RowIndex;
            if (_menuColumnIndex != -1)
                e.ContextMenuStrip = detailsGridContextMenuStrip;
        }

        private void sortAscendingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            detailsGrid.ClearSelection();
            detailsGrid.Sort(detailsGrid.Columns[_menuColumnIndex], ListSortDirection.Ascending);
        }

        private void sortDescendingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            detailsGrid.ClearSelection();
            detailsGrid.Sort(detailsGrid.Columns[_menuColumnIndex], ListSortDirection.Descending);
        }

        private void applyValueToEntireColumnToolStripMenuItem_Click(object sender, EventArgs e)
        {
            applyValueToRowsAboveToolStripMenuItem_Click(null, null);
            applyValueToRowsBelowToolStripMenuItem_Click(null, null);
        }

        private void applyValueToRowsAboveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            object value = detailsGrid[_menuColumnIndex, _menuRowIndex].Value;
            for (int i = 0; i < _menuRowIndex; i++)
            {
                detailsGrid[_menuColumnIndex, i].Value = value;
            }
        }

        private void applyValueToRowsBelowToolStripMenuItem_Click(object sender, EventArgs e)
        {
            object value = detailsGrid[_menuColumnIndex, _menuRowIndex].Value;
            for (int i = _menuRowIndex + 1; i < detailsGrid.Rows.Count - 1; i++)
            {
                detailsGrid[_menuColumnIndex, i].Value = value;
            }
        }

        private void applyValueToSelectedRowsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            object value = detailsGrid[_menuColumnIndex, _menuRowIndex].Value;
            for (int i = 0; i < detailsGrid.Rows.Count - 1; i++)
            {
                if (i != _menuRowIndex && detailsGrid[_menuColumnIndex, i].Selected)
                {
                    detailsGrid[_menuColumnIndex, i].Value = value;
                }
            }
        }

        private void detailsGridContextMenuStrip_Opening(object sender, CancelEventArgs e)
        {
            applyValueToSelectedRowsToolStripMenuItem.Visible =
                applyValueToRowsAboveToolStripMenuItem.Visible =
                applyValueToRowsBelowToolStripMenuItem.Visible =
                applyValueToEntireColumnToolStripMenuItem.Visible =
                (_menuRowIndex != -1 && _detailsController.IsColumnCopyAble(_menuColumnIndex));

            modelAnalysisToolStripMenuItem.Enabled = _detailsController.IsModelRow(_menuRowIndex);

            detailsGrid.EndEdit();
            if (null != detailsGrid.EditingControl) detailsGrid.EditingControl.Visible = false;
        }

        public void Reload()
        {
            treeView.LoadNodes();
            _detailsController.Refresh(null, true);
            _summaryController.Refresh(null);

            if (treeView.Nodes.Count > 0)
            {
                treeView.SelectedNode = treeView.Nodes[0];
            }
            else
            {
                _selectedCategory = null;
                addNewButton.Enabled = false;
                upButton.Enabled = false;
            }
        }

        public void RefreshSelectedCategory()
        {
            if(_selectedCategory !=null)
            {
                // _selectedCategory.Refresh();
                _detailsController.Refresh(_selectedCategory, true);
            }            
        }

        public void ClipboardCopy()
        {
            if (_gridMode == GridMode.Detail)
            {
                _detailsController.ClipboardCopy();
            }
            else
            {
                _summaryController.ClipboardCopy();
            }
        }

        public void ClipboardPaste()
        {
            if (_gridMode == GridMode.Detail)
            {
                _detailsController.ClipboadPaste();
            }
        }

        private void deleteRowsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _detailsController.DeleteRows(_menuRowIndex);
        }

        private void upButton_Click(object sender, EventArgs e)
        {
            if (treeView.SelectedNode != null && treeView.SelectedNode.Parent != null)
            {
                treeView.SelectedNode = treeView.SelectedNode.Parent;
            }
        }

        public void Parameters_ValuationCurrencyChanged(object sender, EventArgs e)
        {
            _detailsController.CurrenciesChanged();
            _summaryController.Refresh(_selectedCategory);
        }

        public void OnFxRatesChange()
        {
            _detailsController.CurrenciesChanged();
            _summaryController.Refresh(_selectedCategory);
        }

        public void FindAndSelectBond(Bond bond, string property)
        {
            FindAndSelectBategory(bond.Category);
            _detailsController.SelectCellFor(bond, property);
        }

        private void FindAndSelectBategory(Category category)
        {
            FindCategoryInTreeView(treeView.Nodes[0], category);
        }

        private void FindCategoryInTreeView(TreeNode node, Category category)
        {
            Category nodeCategory = (Category)node.Tag;
            if (nodeCategory == category)
            {
                node.EnsureVisible();
                treeView.SelectedNode = node;
                return;
            }

            foreach (TreeNode childNode in node.Nodes)
            {
                FindCategoryInTreeView(childNode, category);
            }
        }

        private void modelAnalysisToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Bond bond = _detailsController.GetBondAtIndex(_menuRowIndex);
            if (bond == null) return;

            ModelAnalysis.Display(this.Font, bond);
        }

        public void BeforeRunSimulation()
        {
            _detailsController.EndEdit();
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            modelAnalysisToolStripMenuItem_Click(sender, e);
        }

        private void detailsGridRowHeaderContextMenuStrip_Opening(object sender, CancelEventArgs e)
        {
            modelAnalysisLineStripMenuItem1.Enabled = _detailsController.IsModelRow(_menuRowIndex);
        }
    }
}