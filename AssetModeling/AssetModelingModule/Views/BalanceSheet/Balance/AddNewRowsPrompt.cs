using System;
using System.Windows.Forms;

namespace AssetModelingModule.Views.BalanceSheet.Balance
{
    public partial class AddNewRowsPrompt : Form
    {
        public AddNewRowsPrompt()
        {
            InitializeComponent();
        }

        public int RowCount
        {
            get
            {
                return (int)rowCountNumericUpDown.Value;
            }
        }

        private void AddNewRowsPrompt_Load(object sender, EventArgs e)
        {
            rowCountNumericUpDown.Focus();
            rowCountNumericUpDown.Select(0, rowCountNumericUpDown.Text.Length);
        }

    }
}