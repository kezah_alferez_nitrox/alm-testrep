using System;
using System.Collections.Generic;
using System.Windows.Forms;
using AssetModelingModule.Calculus;

namespace AssetModelingModule.Views.BalanceSheet.Result
{
    public partial class ScenarioValidationResult : Form
    {
        private readonly Form parent;

        public ScenarioValidationResult(Form parent)
        {
            this.parent = parent;
            InitializeComponent();
        }

        public void DisplayMessages(List<ValidationMessage> messages)
        {
            messagesListBox.DisplayMember = "Message";
            messagesListBox.DataSource = messages;
            if(!this.Visible) this.Show(parent);
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void messagesListBox_DoubleClick(object sender, EventArgs e)
        {
            ValidationMessage validationMessage = messagesListBox.SelectedItem as ValidationMessage;
            if (validationMessage == null) return;

            switch (validationMessage.Type)
            {
                case ValidationMessageType.Bond:
                    Constants.BalanceSheetForm.FindAndSelectBond(validationMessage.Bond, validationMessage.Property);
                    break;
                case ValidationMessageType.VariableValue:
                    Constants.BalanceSheetForm.FindAndSelectVariableValue(validationMessage.VariableValue);
                    break;
                default:
                    // nothing
                    break;
            }
        }
    }
}