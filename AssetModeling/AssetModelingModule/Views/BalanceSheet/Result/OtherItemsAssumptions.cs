using System.Windows.Forms;
using ALMSCommon;
using AssetModelingModule.BusinessLogic;
using AssetModelingModule.Controls;
using AssetModelingModule.Core;
using AssetModelingModule.Domain;
using AssetModelingModule.Resources.Language;

namespace AssetModelingModule.Views.BalanceSheet.Result
{
    public partial class OtherItemsAssumptions : Form
    {
        public OtherItemsAssumptions()
        {
            InitializeComponent();
        }

        private DataGridView _otherItemsGrid;

        private string[] variableAndVectorNames;

        private DataGridViewTextBoxColumn descriptionDataGridViewTextBoxColumn;
        private AutoCompleteColumn vectorAutoCompleteColumn;
        private DataGridViewTextBoxColumn SUBTOTALonGroupDataGridViewTextBoxColumn;

        private DataGridViewColumn[] columns;

        private void InitVariableAndVector()
        {
            variableAndVectorNames = DomainTools.GetAllvariablesAndVectorNames();
        }

        private void InitColumns()
        {
            InitVariableAndVector();

            descriptionDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            vectorAutoCompleteColumn = new AutoCompleteColumn();
            SUBTOTALonGroupDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();

            //descriptionDataGridViewTextBoxColumn 
            descriptionDataGridViewTextBoxColumn.DataPropertyName = "Descrip";
            descriptionDataGridViewTextBoxColumn.HeaderText = Labels.OtherItemsAssumptions_InitColumns_Description;
            descriptionDataGridViewTextBoxColumn.Name = "Description";
            descriptionDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            descriptionDataGridViewTextBoxColumn.Width = 150;

            //vectorAutoCompleteColumn
            vectorAutoCompleteColumn.DataPropertyName = "Vect";
            vectorAutoCompleteColumn.HeaderText = Labels.OtherItemsAssumptions_InitColumns_Vector;
            vectorAutoCompleteColumn.Name = "Vector";
            vectorAutoCompleteColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            vectorAutoCompleteColumn.Width = 100;
            vectorAutoCompleteColumn.ValueList = variableAndVectorNames;

            //SUBTOTALonGroupDataGridViewTextBoxColumn
            SUBTOTALonGroupDataGridViewTextBoxColumn.DataPropertyName = "SubtotalGroup";
            SUBTOTALonGroupDataGridViewTextBoxColumn.HeaderText = Labels.OtherItemsAssumptions_InitColumns_SUBTOTAL_on_Group;
            SUBTOTALonGroupDataGridViewTextBoxColumn.Name = "SUBTOTAL on Group";
            SUBTOTALonGroupDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            SUBTOTALonGroupDataGridViewTextBoxColumn.Width = 150;



            columns = new DataGridViewColumn[]
            {
                descriptionDataGridViewTextBoxColumn,
                vectorAutoCompleteColumn,
                SUBTOTALonGroupDataGridViewTextBoxColumn
            };

            _otherItemsGrid.Columns.Clear();
            _otherItemsGrid.Columns.AddRange(columns);
        }

        private void OtherItemsAssumptions_Load(object sender, System.EventArgs e)
        {
            if (!DesignMode && !Tools.InDesignMode)
            {
                _otherItemsGrid = OtherItemsAssumption;

                _otherItemsGrid.AutoGenerateColumns = false;
                _otherItemsGrid.AllowUserToAddRows = false;
                _otherItemsGrid.AllowUserToDeleteRows = false;
                _otherItemsGrid.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
                _otherItemsGrid.RowTemplate.Height = Constants.GRID_ROW_HEIGHT;

                InitColumns();
                Refresh(false);

            }
        }

        private void addNewItems(int itemsCount)
        {
            for (int i = 0; i < itemsCount; i++)
            {
                OtherItemAssum oia = new OtherItemAssum();
                oia.Descrip = string.Empty;
                oia.Status = Constants.Status.NEW;
                oia.Create();
                OIAList.Add(oia);
            }
        }

        private SortableBindingList<OtherItemAssum> OIAList
        {
            get { return _otherItemsGrid.DataSource as SortableBindingList<OtherItemAssum>; }
            set { _otherItemsGrid.DataSource = value; }
        }

        private void Refresh(bool resetColumns)
        {
            if (resetColumns)
            {
                InitColumns();
            }

            SortableBindingList<OtherItemAssum> gridData = new SortableBindingList<OtherItemAssum>();

            foreach (OtherItemAssum OIA in OtherItemAssum.FindAll())
            {
                gridData.Add(OIA);
            }
            OIAList = gridData;
        }

        private void addButton_Click(object sender, System.EventArgs e)
        {
            Balance.AddNewRowsPrompt addNewRowsPrompt = new Balance.AddNewRowsPrompt();
            addNewRowsPrompt.Text = Labels.OtherItemsAssumptions_addButton_Click_Other_Items_Assumption;
            if (addNewRowsPrompt.ShowDialog() == DialogResult.OK)
            {

                addNewItems(addNewRowsPrompt.RowCount);
            }
        }

        private void okButton_Click(object sender, System.EventArgs e)
        {
            foreach (OtherItemAssum _oia in OtherItemAssum.findDataByStatus(Constants.Status.NEW))
            {
                _oia.Status = Constants.Status.SAVED;
            }

            if (_otherItemsGrid.Rows.Count > 0)
            {
                OtherItemAssum[] oias = OtherItemAssum.FindAll();
                foreach (DataGridViewRow _row in _otherItemsGrid.Rows)
                {
                    if (_row.Cells[descriptionDataGridViewTextBoxColumn.Index].Value != null)
                        oias[_row.Index].Descrip = _row.Cells[descriptionDataGridViewTextBoxColumn.Index].Value.ToString();
                    if (_row.Cells[vectorAutoCompleteColumn.Index].Value != null)
                        oias[_row.Index].Vect = _row.Cells[vectorAutoCompleteColumn.Index].Value.ToString();
                    if (_row.Cells[SUBTOTALonGroupDataGridViewTextBoxColumn.Index].Value != null)
                        oias[_row.Index].SubtotalGroup = _row.Cells[SUBTOTALonGroupDataGridViewTextBoxColumn.Index].Value.ToString();
                    oias[_row.Index].IsVisibleChecked = true;
                }
            }

            foreach (OtherItemAssum _oia in OtherItemAssum.findDataByStatus(Constants.Status.DELETED))
            {
                _oia.DeleteAndFlush();
            }
        }

        private void cancelButton_Click(object sender, System.EventArgs e)
        {
            foreach (OtherItemAssum _oia in OtherItemAssum.findDataByStatus(Constants.Status.NEW))
            {
                _oia.Delete();
            }
            foreach (OtherItemAssum _oia in OtherItemAssum.findDataByStatus(Constants.Status.DELETED))
            {
                _oia.Status = Constants.Status.SAVED;
            }
        }

        private void deleteToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            OtherItemAssum[] oias = OtherItemAssum.FindAll();

            if (_otherItemsGrid.SelectedCells.Count > 0)
            {
                foreach (DataGridViewCell _cell in _otherItemsGrid.SelectedCells)
                {
                    if (_cell.RowIndex >= 0)
                    {
                        CurrencyManager cm = (CurrencyManager)BindingContext[_otherItemsGrid.DataSource];
                        if (oias[_cell.RowIndex].Status == Constants.Status.NEW)
                        {
                            oias[_cell.RowIndex].Delete();
                            OIAList.Remove(oias[_cell.RowIndex]);
                        }
                        else
                        {
                            oias[_cell.RowIndex].Status = Constants.Status.DELETED;
                            cm.ResumeBinding();
                            cm.SuspendBinding();
                            _cell.OwningRow.Visible = false;
                        }
                    }

                }
            }
        }

        private void OtherItemsAssumption_MouseDown(object sender, MouseEventArgs e)
        {
            DataGridView.HitTestInfo hti = this.OtherItemsAssumption.HitTest(e.X, e.Y);
            if (hti.Type == DataGridViewHitTestType.Cell && OtherItemsAssumption.SelectedCells.Count <= 1)
            {
                this.OtherItemsAssumption.ClearSelection();

                this.OtherItemsAssumption.Rows[hti.RowIndex].Cells[hti.ColumnIndex].Selected =
                true;
            }
        }

        private static void OtherItemsAssumption_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            e.ThrowException = false;
            e.Cancel = true;
        }

    }
}