﻿using AssetModelingModule.Resources.Language;

namespace AssetModelingModule.Views.BalanceSheet.Result
{
    partial class TaxAssumptions
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.okButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.StandardRatetextBox = new System.Windows.Forms.NumericUpDown();
            this.LongTermtextBox = new System.Windows.Forms.NumericUpDown();
            this.SpecialtextBox = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.StandardRatetextBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LongTermtextBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SpecialtextBox)).BeginInit();
            this.SuspendLayout();
            // 
            // okButton
            // 
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okButton.Location = new System.Drawing.Point(43, 104);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(75, 23);
            this.okButton.TabIndex = 2;
            this.okButton.Text = Labels.TaxAssumptions_InitializeComponent_OK;
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Click += new System.EventHandler(this.okButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point(124, 104);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 1;
            this.cancelButton.Text = Labels.TaxAssumptions_InitializeComponent_Cancel;
            this.cancelButton.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = Labels.TaxAssumptions_InitializeComponent_Standard_Rate;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = Labels.TaxAssumptions_InitializeComponent_Long_Term;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = Labels.TaxAssumptions_InitializeComponent_Special;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(211, 15);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(15, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "%";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(211, 41);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(15, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "%";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(211, 67);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(15, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "%";
            // 
            // StandardRatetextBox
            // 
            this.StandardRatetextBox.DecimalPlaces = 2;
            this.StandardRatetextBox.Location = new System.Drawing.Point(105, 12);
            this.StandardRatetextBox.Name = "StandardRatetextBox";
            this.StandardRatetextBox.Size = new System.Drawing.Size(100, 20);
            this.StandardRatetextBox.TabIndex = 12;
            // 
            // LongTermtextBox
            // 
            this.LongTermtextBox.DecimalPlaces = 2;
            this.LongTermtextBox.Location = new System.Drawing.Point(105, 38);
            this.LongTermtextBox.Name = "LongTermtextBox";
            this.LongTermtextBox.Size = new System.Drawing.Size(100, 20);
            this.LongTermtextBox.TabIndex = 13;
            // 
            // SpecialtextBox
            // 
            this.SpecialtextBox.DecimalPlaces = 2;
            this.SpecialtextBox.Location = new System.Drawing.Point(105, 64);
            this.SpecialtextBox.Name = "SpecialtextBox";
            this.SpecialtextBox.Size = new System.Drawing.Size(100, 20);
            this.SpecialtextBox.TabIndex = 14;
            // 
            // TaxAssumptions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(243, 139);
            this.ControlBox = false;
            this.Controls.Add(this.SpecialtextBox);
            this.Controls.Add(this.LongTermtextBox);
            this.Controls.Add(this.StandardRatetextBox);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.okButton);
            this.Controls.Add(this.cancelButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "TaxAssumptions";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = Labels.TaxAssumptions_InitializeComponent_Tax_Assumptions;
            this.Load += new System.EventHandler(this.TaxAssumptions_Load);
            ((System.ComponentModel.ISupportInitialize)(this.StandardRatetextBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LongTermtextBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SpecialtextBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown StandardRatetextBox;
        private System.Windows.Forms.NumericUpDown LongTermtextBox;
        private System.Windows.Forms.NumericUpDown SpecialtextBox;
    }
}