using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using AssetModelingModule.Calculus;
using AssetModelingModule.Core;
using AssetModelingModule.Domain;
using AssetModelingModule.Resources.Language;

namespace AssetModelingModule.Views.BalanceSheet.Result
{
    public class ResultGridController
    {
        private readonly DataGridView grid;
        private readonly List<DataGridViewColumn> columns = new List<DataGridViewColumn>();

        public ResultGridController(DataGridView grid)
        {
            this.grid = grid;
            grid.ReadOnly = true;
            grid.AutoGenerateColumns = false;
            grid.AllowUserToAddRows = false;
            grid.AllowUserToDeleteRows = false;
            grid.AllowUserToResizeRows = false;
            grid.RowTemplate.Height = Constants.GRID_ROW_HEIGHT;
            grid.ColumnHeadersVisible = false;
        }

        private void CreateColumns(IList<DateInterval> dateIntervals)
        {
            this.columns.Clear();

            DataGridViewTextBoxColumn valueDataGridViewTextBoxColumn;

            DataGridViewTextBoxColumn modelTypeColumn = new DataGridViewTextBoxColumn();
            modelTypeColumn.Visible = false;
            modelTypeColumn.Frozen = true;
            this.columns.Add(modelTypeColumn);

            // 
            // summaryDescriptionDataGridViewTextBoxColumn
            // 
            DataGridViewTextBoxColumn descriptionDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            descriptionDataGridViewTextBoxColumn.HeaderText = Labels.ResultGridController_CreateColumns_Description;
            descriptionDataGridViewTextBoxColumn.Name = "Description";
            descriptionDataGridViewTextBoxColumn.Width = 200;
            descriptionDataGridViewTextBoxColumn.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            descriptionDataGridViewTextBoxColumn.Frozen = true;

            this.columns.Add(descriptionDataGridViewTextBoxColumn);

            for (int i = 0; i < dateIntervals.Count; i++)
            {
                DateInterval interval = dateIntervals[i];

                // 
                // summaryValueDataGridViewTextBoxColumn
                // 
                valueDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
                valueDataGridViewTextBoxColumn.Name = "Value" + interval.Start + "-" + interval.End;
                valueDataGridViewTextBoxColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                valueDataGridViewTextBoxColumn.Width = 100;
                valueDataGridViewTextBoxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                valueDataGridViewTextBoxColumn.DefaultCellStyle.Format = "N2";
                if (i == 0) valueDataGridViewTextBoxColumn.Frozen = true;
                this.columns.Add(valueDataGridViewTextBoxColumn);
            }

            this.grid.Columns.Clear();
            this.grid.Columns.AddRange(this.columns.ToArray());
        }

        private void FormatRows()
        {
            Font boldFont = new Font(this.grid.Font, FontStyle.Bold);

            for (int rowIndex = 0; rowIndex < this.grid.Rows.Count; rowIndex++)
            {
                ResultRowType type = (ResultRowType) this.grid[0, rowIndex].Value;
                DataGridViewRow row = this.grid.Rows[rowIndex];

                switch (type)
                {
                    case ResultRowType.MasterHeader:
                        row.DefaultCellStyle.BackColor = Constants.Colors.TableHeader2;
                        row.DefaultCellStyle.Font = boldFont;
                        break;
                    case ResultRowType.PeriodHeader:
                        row.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                        row.DefaultCellStyle.BackColor = Constants.Colors.TableHeader2;
                        row.DefaultCellStyle.Font = boldFont;
                        break;
                    case ResultRowType.Header:
                        row.DefaultCellStyle.BackColor = Constants.Colors.TableHeader1;
                        row.DefaultCellStyle.Font = boldFont;
                        break;
                    case ResultRowType.WhiteHeader:
                        row.DefaultCellStyle.Font = boldFont;
                        break;
                    case ResultRowType.Footer:
                        row.DefaultCellStyle.BackColor = Constants.Colors.TableFooter1;
                        row.DefaultCellStyle.Font = boldFont;
                        break;
                    case ResultRowType.MasterFooter:
                        row.DefaultCellStyle.BackColor = Constants.Colors.TableFooter2;
                        row.DefaultCellStyle.Font = boldFont;
                        break;
                    case ResultRowType.Percentage:

                        row.DefaultCellStyle.BackColor = Constants.Colors.TableHeader1;
                        row.DefaultCellStyle.Font = boldFont;
                        row.DefaultCellStyle.Format = "P";
                        break;
                    default: // SummaryType.Normal, SummaryType.None
                        break;
                }
            }
        }

        public void Refresh(ScenarioData data, ReportType type)
        {
            DateTime valuationDate = Param.ValuationDate ?? DateTime.Today;
            ResultModel resultModel = new ResultModel(data, type, valuationDate);

            this.CreateColumns(resultModel.DateIntervals);

            IList<ResultRowModel> gridData = resultModel.GetRows();

            this.grid.Rows.Clear();
            foreach (ResultRowModel model in gridData)
            {
                this.grid.Rows.Add(model.RowValues);
            }

            if (this.grid.RowCount >= 1)
            {
                this.grid.Rows[0].Frozen = true;
                this.grid.Rows[0].Height *= 2;
            }

            this.FormatRows();
        }

        public void ClipboardCopy()
        {
            if (this.grid.GetCellCount(DataGridViewElementStates.Selected) > 0)
            {
                DataObject data = this.grid.GetClipboardContent();
                if (data != null)
                {
                    Clipboard.SetDataObject(this.grid.GetClipboardContent());
                }
            }
        }

        public void InvalidateResults()
        {
            this.grid.Rows.Clear();
        }
    }
}