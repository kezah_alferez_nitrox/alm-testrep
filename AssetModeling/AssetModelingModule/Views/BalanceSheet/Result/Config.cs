using System;
using System.Windows.Forms;
using System.Collections.Generic;
using AssetModelingModule.Domain;
using AssetModelingModule.Resources.Language;

namespace AssetModelingModule.Views.BalanceSheet.Result
{
    public partial class Config : Form
    {
        public Config()
        {
            InitializeComponent();
        }

        List<int> _ids = new List<int>();
        Dictionary<int, int> _oiaIDs = new Dictionary<int,int>();
        Dictionary<int, int> _bondIDs = new Dictionary<int, int>();
        Dictionary<int, int> _imparmentID = new Dictionary<int, int>();
        Dictionary<int, bool> _modified = new Dictionary<int, bool>();
        Dictionary<int, bool> _modifiedOIA = new Dictionary<int, bool>();
        Dictionary<int, bool> _modifiedBond = new Dictionary<int, bool>();
        Dictionary<int, bool> _modifiedImparment = new Dictionary<int, bool>();
        private bool _refreshed = false;

        private void Config_Load(object sender, EventArgs e)
        {
            categoryTreeView.LoadVisibleNodes();
            RefreshCBL();
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            //update tree
            categoryTreeView.UpdateVisibles(categoryTreeView.Nodes[0]);

            //update profits & losses

            UpdateProfitsLosses();
        }

        private void UpdateProfitsLosses()
        {
            foreach (KeyValuePair<int, bool> _kvp in _modified)
            {
                Category cat = Category.FindNodeByID(_kvp.Key);
                cat.IsVisibleChecked = _kvp.Value;
                cat.SaveAndFlush();
            }
            foreach (KeyValuePair<int, bool> _kvp in _modifiedOIA)
            {
                OtherItemAssum oia = OtherItemAssum.findByID(_kvp.Key);
                oia.IsVisibleChecked = _kvp.Value;
                oia.SaveAndFlush();
            }
            foreach (KeyValuePair<int, bool> _kvp in _modifiedBond)
            {
                Bond bond = Bond.FindByID(_kvp.Key);
                bond.IsVisibleChecked = _kvp.Value;
                bond.SaveAndFlush();
            }
            foreach (KeyValuePair<int, bool> _kvp in _modifiedImparment)
            {
                Category cat = Category.FindNodeByID(_kvp.Key);
                cat.IsVisibleCheckedModel = _kvp.Value;
                cat.SaveAndFlush();
            }
        }

        private void RefreshCBL()
        {
            ProfitsLossesCheckBoxList.Items.Clear();
            _ids = new List<int>();
            _oiaIDs = new Dictionary<int, int>();
            _modified = new Dictionary<int, bool>();
            _modifiedOIA = new Dictionary<int, bool>();

            foreach (Category cat in Category.FindByBalanceType(BalanceType.Asset))
            {
                if (cat.isVisible == true)
                {
                    if (cat.IsVisibleChecked == true)
                    {
                        ProfitsLossesCheckBoxList.Items.Add(cat.Label, CheckState.Checked);
                        _ids.Add(cat.Id);
                    }
                    else
                    {
                        ProfitsLossesCheckBoxList.Items.Add(cat.Label);
                       _ids.Add(cat.Id);
                    }
                }
            }
            ProfitsLossesCheckBoxList.Items.Add(Labels.Config_RefreshCBL_Total_Interest_Income, CheckState.Indeterminate);
            _ids.Add(0);

            foreach (Category cat in Category.FindByBalanceType(BalanceType.Liability))
            {
                if (cat.isVisible == true)
                    if (cat.IsVisibleChecked == true)
                    {
                        ProfitsLossesCheckBoxList.Items.Add(cat.Label, CheckState.Checked);
                        _ids.Add(cat.Id);
                    }
                    else
                    {
                        ProfitsLossesCheckBoxList.Items.Add(cat.Label);
                        _ids.Add(cat.Id);
                    }
            }
            ProfitsLossesCheckBoxList.Items.Add(Labels.Config_RefreshCBL_Equity, Category.FindByBalanceType(BalanceType.Equity)[0].IsVisibleChecked == true? CheckState.Checked : CheckState.Unchecked);
            _ids.Add(Category.FindByBalanceType(BalanceType.Equity)[0].Id);

            ProfitsLossesCheckBoxList.Items.Add(Labels.Config_RefreshCBL_Total_Interest_Expense, CheckState.Indeterminate);
            _ids.Add(0);

            ProfitsLossesCheckBoxList.Items.Add(Labels.Config_RefreshCBL_Net_interest_income, CheckState.Indeterminate);
            _ids.Add(0);

            ComputeOIA();

            ProfitsLossesCheckBoxList.Items.Add(Labels.Config_RefreshCBL_Income_before_impairment_charge, CheckState.Indeterminate);
            _ids.Add(0);

            ImpartmentData();

            ProfitsLossesCheckBoxList.Items.Add(Labels.Config_RefreshCBL_Impairment_charge, CheckState.Indeterminate);
            _ids.Add(0);
            
            ProfitsLossesCheckBoxList.Items.Add(Labels.Config_RefreshCBL_Income_tax_benefit____expense_, CheckState.Indeterminate);
            _ids.Add(0);

            _refreshed = true;
        }

        private void ImpartmentData()
        {
            List<string>_auxlist = new List<string>();

            foreach (Bond _bond in Bond.FindIsModel())
            {
                if (!_auxlist.Contains(_bond.Category.Label))
                {
                    if (_bond.Category.IsVisibleCheckedModel == true)
                        ProfitsLossesCheckBoxList.Items.Add(_bond.Category.Label, CheckState.Checked);
                    else
                        ProfitsLossesCheckBoxList.Items.Add(_bond.Category.Label);
                    _auxlist.Add(_bond.Category.Label);
                    _ids.Add(0);
                    _imparmentID.Add(_ids.Count - 1, _bond.Category.Id);
                }
            }
        }

        private void ProfitsLossesCheckBoxList_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            if (_refreshed)
            {
                if (e.CurrentValue == CheckState.Indeterminate || e.NewValue == CheckState.Indeterminate)
                    e.NewValue = CheckState.Indeterminate;
                else
                {
                    if (_ids[e.Index] != 0)
                        if (_modified.ContainsKey(_ids[e.Index]))
                            _modified[_ids[e.Index]] = e.NewValue == CheckState.Checked ? true : false;
                        else
                            _modified.Add(_ids[e.Index], (e.NewValue == CheckState.Checked ? true : false));
                    else
                    {
                        if (_oiaIDs.ContainsKey(e.Index))
                        {
                            if (_modifiedOIA.ContainsKey(_oiaIDs[e.Index]))
                                _modifiedOIA[_oiaIDs[e.Index]] = e.NewValue == CheckState.Checked ? true : false;
                            else
                                _modifiedOIA.Add(_oiaIDs[e.Index], (e.NewValue == CheckState.Checked ? true : false));
                        }
                        if (_bondIDs.ContainsKey(e.Index))
                        {
                            if (_modifiedBond.ContainsKey(_bondIDs[e.Index]))
                                _modifiedBond[_bondIDs[e.Index]] = e.NewValue == CheckState.Checked ? true : false;
                            else
                                _modifiedBond.Add(_bondIDs[e.Index], (e.NewValue == CheckState.Checked ? true : false));

                        }
                        if (_imparmentID.ContainsKey(e.Index))
                        {
                            if (_modifiedImparment.ContainsKey(_imparmentID[e.Index]))
                                _modifiedImparment[_imparmentID[e.Index]] = e.NewValue == CheckState.Checked ? true : false;
                            else
                                _modifiedImparment.Add(_imparmentID[e.Index], (e.NewValue == CheckState.Checked ? true : false));
                        }
                    }
                }
            }
        }

        private void ComputeOIA()
        {
            string[] subtotalgroups = OtherItemAssum.GetAllSubgroups();

            bool swapped;
            do
            {
                swapped = false;
                for (int i = 0; i < subtotalgroups.Length - 1; i++)
                {
                    if ((subtotalgroups[i] == null ? 0 : OtherItemAssum.findBySubtotalGroup(subtotalgroups[i]).Length) < (subtotalgroups[i + 1] == null ? 0 : OtherItemAssum.findBySubtotalGroup(subtotalgroups[i + 1]).Length))
                    {
                        string aux;
                        aux = subtotalgroups[i];
                        subtotalgroups[i] = subtotalgroups[i + 1];
                        subtotalgroups[i + 1] = aux;
                        swapped = true;
                    }
                }
            }
            while (swapped);


            foreach (string subTotalGroup in subtotalgroups)
            {
                OtherItemAssum[] oias = OtherItemAssum.findBySubtotalGroup(subTotalGroup);

                foreach (OtherItemAssum oia in oias)
                {
                    string name = oia.Descrip;
                    if (oia.IsVisibleChecked == true)
                        ProfitsLossesCheckBoxList.Items.Add(name, CheckState.Checked);
                    else
                        ProfitsLossesCheckBoxList.Items.Add(name);
                    _ids.Add(0);
                    _oiaIDs.Add(_ids.Count - 1, oia.Id);
                }

                if (oias.Length > 1 && subTotalGroup != null)
                {
                    ProfitsLossesCheckBoxList.Items.Add(subTotalGroup, CheckState.Indeterminate);
                    _ids.Add(0);
                    //_oiaIDs.Add(_ids.Count - 1, oia.Id);
                }
                
            }
        }
            
    }
}