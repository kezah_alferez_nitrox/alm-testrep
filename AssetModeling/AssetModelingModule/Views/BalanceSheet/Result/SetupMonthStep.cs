using System;
using System.Windows.Forms;

namespace AssetModelingModule.Views.BalanceSheet.Result
{
    public partial class SetupMonthStep : Form
    {
        public SetupMonthStep()
        {
            InitializeComponent();

            dataGridView.AutoGenerateColumns = false;
        }

        private void SetupMonthStep_Load(object sender, System.EventArgs e)
        {
            dataGridView.Rows.Add(Constants.RESULT_MONTHS);

            DateTime now = DateTime.Now;
            DateTime startDate = new DateTime(now.Year, now.Month, DateTime.DaysInMonth(now.Year, now.Month));
            DateTime endDate = startDate.AddYears(Constants.VECTOR_YEARS);

            // todo: load from database
            DateTime date = startDate;
            int i = 0;
            while (date < endDate)
            {
                dataGridView[0, i].Value = date;
                dataGridView[1, i].Value = 1;
                i++;
                date = date.AddMonths(1);
                date = new DateTime(date.Year, date.Month, DateTime.DaysInMonth(date.Year, date.Month));
            }
        }
    }
}