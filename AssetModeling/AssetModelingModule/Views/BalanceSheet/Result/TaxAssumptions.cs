using System;
using System.Windows.Forms;
using AssetModelingModule.Domain;

namespace AssetModelingModule.Views.BalanceSheet.Result
{
    public partial class TaxAssumptions : Form
    {
        public TaxAssumptions()
        {
            InitializeComponent();
        }

        private void TaxAssumptions_Load(object sender, EventArgs e)
        {
            StandardRatetextBox.Value = Param.StandardTax;
            LongTermtextBox.Value = Param.LongTermTax;
            SpecialtextBox.Value = Param.SpecialTax;
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            Param.StandardTax = StandardRatetextBox.Value;
            Param.LongTermTax = LongTermtextBox.Value;
            Param.SpecialTax = SpecialtextBox.Value;
        }
    }
}