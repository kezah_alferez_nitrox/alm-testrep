using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using ALMSCommon.Forms;
using AssetModelingModule.Calculus;
using AssetModelingModule.Core.Export.MultiScenario;
using AssetModelingModule.Domain;
using AssetModelingModule.Resources.Language;
using Castle.ActiveRecord;
using Eu.AlterSystems.ASNetLib.Core;

namespace AssetModelingModule.Views.BalanceSheet.Result
{
    public partial class Result : UserControl
    {
        private readonly ResultGridController _resultController;

        private Simulation simulation = new Simulation(true);
        private ScenarioValidationResult scenarioValidationResultForm;

        public Result()
        {
            InitializeComponent();

            reportTypeComboBox.DisplayMember = "Value";
            reportTypeComboBox.ValueMember = "Key";

            scenarioComboBox.ValueMember = "Self";

            if (!DesignMode && !Tools.InDesignMode)
            {
                _resultController = new ResultGridController(resultDataGridView);
            }
        }

        public void BeforeRunSimulation()
        {
            if (scenarioValidationResultForm != null && !scenarioValidationResultForm.IsDisposed && scenarioValidationResultForm.Visible)
                scenarioValidationResultForm.Close();
        }

        public void RunSimulation(BackgroundWorker backgroundWorker)
        {
            try
            {
                using (new SessionScope())
                {
                    simulation.Run(backgroundWorker);
                }
            }
            catch (OverflowException)
            {
                MessageBox.Show(
                    Labels.Result_RunSimulation_, Labels.Result_RunSimulation_Error, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void RefreshResultGrid()
        {
            if (!simulation.IsSuccesfull) return;

            Scenario scenario = scenarioComboBox.SelectedItem as Scenario;
            if (scenario == null) return;

            ScenarioData categoriesData = simulation[scenario];
            if (categoriesData == null) return;

            if (reportTypeComboBox.SelectedIndex < 0) return;
            ReportType reportType = (ReportType)reportTypeComboBox.SelectedValue;
            _resultController.Refresh(categoriesData, reportType);
        }


        public bool SimulationEnd()
        {
            if (simulation.IsSuccesfull)
            {
                scenarioComboBox.DataSource = Scenario.FindSelected();
                RefreshResultGrid();
            }
            else
            {
                if (simulation.ValidationErrors != null && simulation.ValidationErrors.Count > 0)
                {
                    DisplayValidationErrors(simulation.ValidationErrors);
                }
            }

            return simulation.IsSuccesfull;
        }

        private void DisplayValidationErrors(List<ValidationMessage> errors)
        {
            if (scenarioValidationResultForm == null || scenarioValidationResultForm.IsDisposed) scenarioValidationResultForm = new ScenarioValidationResult(this.ParentForm);
            scenarioValidationResultForm.DisplayMessages(errors);
        }

        private void Result_Load(object sender, System.EventArgs e)
        {
            if (DesignMode || Tools.InDesignMode) return;

            reportTypeComboBox.DataSource = EnumHelper.ToList(typeof(ReportType));

            scenarioComboBox.DataSource = Scenario.FindSelected();
        }

        private void reportTypeComboBox_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            userDefinedButton.Visible = (reportTypeComboBox.SelectedIndex == 5);

            RefreshResultGrid();
        }

        private void scenarioComboBox_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            RefreshResultGrid();
        }

        private void userDefinedButton_Click(object sender, System.EventArgs e)
        {
            (new SetupMonthStep()).ShowDialog();
        }

        private void configButton_Click(object sender, System.EventArgs e)
        {
            if (Tools.ShowDialogWithPrivateSession(new Config()) == DialogResult.OK)
            {
                RefreshResultGrid();
            }
        }

        public void ClipboardCopy()
        {
            _resultController.ClipboardCopy();
        }

        private void printButton_Click(object sender, EventArgs e)
        {
            if (resultDataGridView.Rows.Count != 0)
            {
                PrintForm _pf = new PrintForm(resultDataGridView);
                _pf.ShowDialog();
            }
        }

        public void InvalidateResult()
        {
            simulation = new Simulation(true);
            _resultController.InvalidateResults();
        }

        private void ExportAllButtonClick(object sender, EventArgs e)
        {
            if(simulation != null && simulation.IsSuccesfull)
            {
                ReportType reportType = (ReportType)this.reportTypeComboBox.SelectedValue;
                if (reportType == ReportType.Monthly)
                {
                    reportType = ReportType.Quarterly;
                }
                DateTime valuationDate = Param.ValuationDate ?? DateTime.Today;
                MultiScenarioExporter exporter = new MultiScenarioExporter(this.simulation, reportType, valuationDate);

                TimeSpan ts = new TimeSpan(0, 0, int.Parse(System.Configuration.ConfigurationManager.AppSettings["LAST_EXPORTALL_DURATION"]));

                LoadForm.Instance.DoWork(new WorkParameters(bw=>exporter.Export(), null), ts);

                System.Configuration.ConfigurationManager.AppSettings["LAST_EXPORTALL_DURATION"] = LoadForm.Instance.WorkDurationInSeconds.ToString();
            }
        }
    }
}
