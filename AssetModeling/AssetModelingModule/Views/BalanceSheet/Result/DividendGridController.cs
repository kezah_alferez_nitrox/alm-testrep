using System;
using System.Windows.Forms;
using ALMSCommon;
using AssetModelingModule.BusinessLogic;
using AssetModelingModule.Controls;
using AssetModelingModule.Core;
using AssetModelingModule.Domain;
using AssetModelingModule.Resources.Language;

namespace AssetModelingModule.Views.BalanceSheet.Result
{
    public class DividendGridController
    {
        private const int DECIMAL_CELL_WIDTH = 70;

        private readonly DataGridView grid;
        private string[] vectorNames;
        private DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn;
        private AutoCompleteColumn amountDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn minimumDataGridViewTextBoxColumn;

        private DataGridViewColumn[] columns;

        public DividendGridController(DataGridView Grid)
        {
            this.grid = Grid;

            grid.AutoGenerateColumns = false;
            grid.AllowUserToAddRows = false;
            grid.AllowUserToDeleteRows = false;

            grid.RowTemplate.Height = Constants.GRID_ROW_HEIGHT;

            InitColumns();

        }

        private void InitColumns()
        {
            vectorNames = DomainTools.GetAllVectorNames();

            nameDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            amountDataGridViewTextBoxColumn = new AutoCompleteColumn();
            minimumDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();

            //
            //nameDataGridViewTextBoxColumn
            //

            //nameDataGridViewTextBoxColumn.DataPropertyName =  "Name";
            nameDataGridViewTextBoxColumn.Name = "Name";
            nameDataGridViewTextBoxColumn.HeaderText = Labels.DividendGridController_InitColumns_Name;
            nameDataGridViewTextBoxColumn.Width = 100;

            //
            //amountDataGridViewTextBoxColumn
            //
            //amountDataGridViewTextBoxColumn.DataPropertyName = "Amount";
            amountDataGridViewTextBoxColumn.Name = "Amount";
            amountDataGridViewTextBoxColumn.HeaderText = Labels.DividendGridController_InitColumns_Value__vector_or_percentage_;
            amountDataGridViewTextBoxColumn.Width = 200;
            amountDataGridViewTextBoxColumn.ValueList = vectorNames;

            //
            //minimumDataGridViewTextBoxColumn
            //
            //minimumDataGridViewTextBoxColumn.DataPropertyName = "Minimum";
            minimumDataGridViewTextBoxColumn.Name = "Minimum";
            minimumDataGridViewTextBoxColumn.DefaultCellStyle.Format = Constants.DURATION_CELL_FORMAT;
            minimumDataGridViewTextBoxColumn.HeaderText = Labels.DividendGridController_InitColumns_Minimum;
            minimumDataGridViewTextBoxColumn.Width = 100;

            columns = new DataGridViewColumn[]
            {
                nameDataGridViewTextBoxColumn,
                amountDataGridViewTextBoxColumn,
                minimumDataGridViewTextBoxColumn
            };
            grid.Columns.Clear();
            grid.Columns.AddRange(columns);

        }

        public void Refresh(bool resetColumns)
        {
            if (resetColumns)
                InitColumns();

            Dividend[] dividends = Dividend.FindAll();

            SortableBindingList<Dividend> gridData = new SortableBindingList<Dividend>();

            foreach (Dividend dividend in dividends)
            {
                gridData.Add(dividend);
            }

            DividendList = gridData;

            for (int i = 0; i < DividendList.Count; i++)
            {
                grid.Rows[i].Cells[nameDataGridViewTextBoxColumn.Index].Value = gridData[i].Name;
                string _amount = gridData[i].Amount;
                grid.Rows[i].Cells[amountDataGridViewTextBoxColumn.Index].Value = _amount;

                decimal _amountdec;
                if (decimal.TryParse(_amount, out _amountdec))
                {
                    grid.Rows[i].Cells[minimumDataGridViewTextBoxColumn.Index].ReadOnly = false;
                }
                else
                {
                    grid.Rows[i].Cells[minimumDataGridViewTextBoxColumn.Index].ReadOnly = true;
                }
                grid.Rows[i].Cells[minimumDataGridViewTextBoxColumn.Index].Value = gridData[i].Minimum;

            }
        }

        public SortableBindingList<Dividend> DividendList
        {
            get { return grid.DataSource as SortableBindingList<Dividend>; }
            set { grid.DataSource = value; }
        }

        public void SaveDividend()
        {

            foreach (Dividend dividend in Dividend.FindByStatus(Constants.Status.NEW))
            {
                dividend.Status = Constants.Status.SAVED;
            }

            if (grid.Rows.Count > 0)
            {
                Dividend[] dividends = Dividend.FindAll();
                foreach (DataGridViewRow _row in grid.Rows)
                {
                    if (_row.Visible)
                    {
                        if (_row.Cells[nameDataGridViewTextBoxColumn.Index].Value == null) _row.Cells[nameDataGridViewTextBoxColumn.Index].Value = string.Empty;
                        dividends[_row.Index].Name = _row.Cells[nameDataGridViewTextBoxColumn.Index].Value.ToString();
                        if (_row.Cells[amountDataGridViewTextBoxColumn.Index].Value == null) _row.Cells[amountDataGridViewTextBoxColumn.Index].Value = string.Empty;
                        string _amount = _row.Cells[amountDataGridViewTextBoxColumn.Index].Value.ToString();
                        dividends[_row.Index].Amount = _amount;
                        if (_row.Cells[minimumDataGridViewTextBoxColumn.Index].Value != null)
                            dividends[_row.Index].Minimum = Convert.ToDecimal(_row.Cells[minimumDataGridViewTextBoxColumn.Index].Value);
                        else
                            dividends[_row.Index].Minimum = null;
                    }
                }
            }

            foreach (Dividend dividend in Dividend.FindByStatus(Constants.Status.DELETED))
            {
                dividend.Delete();
            }
        }

        public int namecolIndex()
        {
            return nameDataGridViewTextBoxColumn.Index;
        }

        public int amountcolIndex()
        {
            return amountDataGridViewTextBoxColumn.Index;
        }

        public int minimumcolIndex()
        {
            return minimumDataGridViewTextBoxColumn.Index;
        }

        public void addNewItems(int itemsCount)
        {
            for (int i = 0; i < itemsCount; i++)
            {
                Dividend dividend = new Dividend();
                dividend.Status = "new";
                dividend.Create();
                DividendList.Add(dividend);
            }
        }
    }
}
