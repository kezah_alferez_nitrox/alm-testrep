﻿using AssetModelingModule.Controls;
using AssetModelingModule.Resources.Language;

namespace AssetModelingModule.Views.BalanceSheet.Result
{
    partial class Config
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.saveasbutton = new System.Windows.Forms.Button();
            this.savebutton = new System.Windows.Forms.Button();
            this.loadbutton = new System.Windows.Forms.Button();
            this.okButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.doubleformatgroupBox = new System.Windows.Forms.GroupBox();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.showblankcheckBox = new System.Windows.Forms.CheckBox();
            this.hidecheckBox = new System.Windows.Forms.CheckBox();
            this.signcheckBox = new System.Windows.Forms.CheckBox();
            this.commascheckBox = new System.Windows.Forms.CheckBox();
            this.parenschekBox = new System.Windows.Forms.CheckBox();
            this.alignmentgroupBox = new System.Windows.Forms.GroupBox();
            this.rightButton = new System.Windows.Forms.RadioButton();
            this.defaultButton = new System.Windows.Forms.RadioButton();
            this.centerradioButton = new System.Windows.Forms.RadioButton();
            this.leftButton = new System.Windows.Forms.RadioButton();
            this.widthgroupBox = new System.Windows.Forms.GroupBox();
            this.spacificwidthtextBox = new System.Windows.Forms.TextBox();
            this.bestfitradioButton = new System.Windows.Forms.RadioButton();
            this.specificwidthradioButton = new System.Windows.Forms.RadioButton();
            this.formatcomboBox = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.AttributeslistBox = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.categoryTreeView = new DataTreeView();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.ProfitsLossesCheckBoxList = new System.Windows.Forms.CheckedListBox();
            this.panel1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.doubleformatgroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.alignmentgroupBox.SuspendLayout();
            this.widthgroupBox.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.saveasbutton);
            this.panel1.Controls.Add(this.savebutton);
            this.panel1.Controls.Add(this.loadbutton);
            this.panel1.Controls.Add(this.okButton);
            this.panel1.Controls.Add(this.cancelButton);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 434);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(425, 44);
            this.panel1.TabIndex = 0;
            // 
            // saveasbutton
            // 
            this.saveasbutton.Location = new System.Drawing.Point(14, 9);
            this.saveasbutton.Name = "saveasbutton";
            this.saveasbutton.Size = new System.Drawing.Size(75, 23);
            this.saveasbutton.TabIndex = 7;
            this.saveasbutton.Text = Labels.Config_InitializeComponent_Save_As___;
            this.saveasbutton.UseVisualStyleBackColor = true;
            // 
            // savebutton
            // 
            this.savebutton.Location = new System.Drawing.Point(95, 9);
            this.savebutton.Name = "savebutton";
            this.savebutton.Size = new System.Drawing.Size(75, 23);
            this.savebutton.TabIndex = 7;
            this.savebutton.Text = Labels.Config_InitializeComponent_Save;
            this.savebutton.UseVisualStyleBackColor = true;
            // 
            // loadbutton
            // 
            this.loadbutton.Location = new System.Drawing.Point(176, 9);
            this.loadbutton.Name = "loadbutton";
            this.loadbutton.Size = new System.Drawing.Size(75, 23);
            this.loadbutton.TabIndex = 2;
            this.loadbutton.Text = Labels.Config_InitializeComponent_Load;
            this.loadbutton.UseVisualStyleBackColor = true;
            // 
            // okButton
            // 
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okButton.Location = new System.Drawing.Point(257, 9);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(75, 23);
            this.okButton.TabIndex = 1;
            this.okButton.Text = Labels.Config_InitializeComponent_OK;
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Click += new System.EventHandler(this.okButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point(338, 9);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 0;
            this.cancelButton.Text = Labels.Config_InitializeComponent_Cancel;
            this.cancelButton.UseVisualStyleBackColor = true;
            // 
            // tabPage3
            // 
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(417, 408);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = Labels.Config_InitializeComponent_Option;
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.doubleformatgroupBox);
            this.tabPage2.Controls.Add(this.alignmentgroupBox);
            this.tabPage2.Controls.Add(this.widthgroupBox);
            this.tabPage2.Controls.Add(this.formatcomboBox);
            this.tabPage2.Controls.Add(this.label2);
            this.tabPage2.Controls.Add(this.AttributeslistBox);
            this.tabPage2.Controls.Add(this.label1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(417, 408);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = Labels.Config_InitializeComponent_Formatting;
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // doubleformatgroupBox
            // 
            this.doubleformatgroupBox.Controls.Add(this.numericUpDown1);
            this.doubleformatgroupBox.Controls.Add(this.label3);
            this.doubleformatgroupBox.Controls.Add(this.showblankcheckBox);
            this.doubleformatgroupBox.Controls.Add(this.hidecheckBox);
            this.doubleformatgroupBox.Controls.Add(this.signcheckBox);
            this.doubleformatgroupBox.Controls.Add(this.commascheckBox);
            this.doubleformatgroupBox.Controls.Add(this.parenschekBox);
            this.doubleformatgroupBox.Enabled = false;
            this.doubleformatgroupBox.Location = new System.Drawing.Point(151, 263);
            this.doubleformatgroupBox.Name = "doubleformatgroupBox";
            this.doubleformatgroupBox.Size = new System.Drawing.Size(231, 109);
            this.doubleformatgroupBox.TabIndex = 6;
            this.doubleformatgroupBox.TabStop = false;
            this.doubleformatgroupBox.Text = Labels.Config_InitializeComponent_Double_Format;
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(61, 82);
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(45, 20);
            this.numericUpDown1.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 85);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = Labels.Config_InitializeComponent_Precision;
            // 
            // showblankcheckBox
            // 
            this.showblankcheckBox.AutoSize = true;
            this.showblankcheckBox.Location = new System.Drawing.Point(121, 53);
            this.showblankcheckBox.Name = "showblankcheckBox";
            this.showblankcheckBox.Size = new System.Drawing.Size(106, 17);
            this.showblankcheckBox.TabIndex = 4;
            this.showblankcheckBox.Text = Labels.Config_InitializeComponent_Show_blank_for_0;
            this.showblankcheckBox.UseVisualStyleBackColor = true;
            // 
            // hidecheckBox
            // 
            this.hidecheckBox.AutoSize = true;
            this.hidecheckBox.Location = new System.Drawing.Point(6, 53);
            this.hidecheckBox.Name = "hidecheckBox";
            this.hidecheckBox.Size = new System.Drawing.Size(100, 17);
            this.hidecheckBox.TabIndex = 3;
            this.hidecheckBox.Text = Labels.Config_InitializeComponent_Hide_M_MM_bp;
            this.hidecheckBox.UseVisualStyleBackColor = true;
            // 
            // signcheckBox
            // 
            this.signcheckBox.AutoSize = true;
            this.signcheckBox.Location = new System.Drawing.Point(162, 19);
            this.signcheckBox.Name = "signcheckBox";
            this.signcheckBox.Size = new System.Drawing.Size(56, 17);
            this.signcheckBox.TabIndex = 2;
            this.signcheckBox.Text = Labels.Config_InitializeComponent_;
            this.signcheckBox.UseVisualStyleBackColor = true;
            // 
            // commascheckBox
            // 
            this.commascheckBox.AutoSize = true;
            this.commascheckBox.Location = new System.Drawing.Point(6, 19);
            this.commascheckBox.Name = "commascheckBox";
            this.commascheckBox.Size = new System.Drawing.Size(66, 17);
            this.commascheckBox.TabIndex = 0;
            this.commascheckBox.Text = Labels.Config_InitializeComponent_Commas;
            this.commascheckBox.UseVisualStyleBackColor = true;
            // 
            // parenschekBox
            // 
            this.parenschekBox.AutoSize = true;
            this.parenschekBox.Location = new System.Drawing.Point(80, 19);
            this.parenschekBox.Name = "parenschekBox";
            this.parenschekBox.Size = new System.Drawing.Size(59, 17);
            this.parenschekBox.TabIndex = 1;
            this.parenschekBox.Text = Labels.Config_InitializeComponent_Parens;
            this.parenschekBox.UseVisualStyleBackColor = true;
            // 
            // alignmentgroupBox
            // 
            this.alignmentgroupBox.Controls.Add(this.rightButton);
            this.alignmentgroupBox.Controls.Add(this.defaultButton);
            this.alignmentgroupBox.Controls.Add(this.centerradioButton);
            this.alignmentgroupBox.Controls.Add(this.leftButton);
            this.alignmentgroupBox.Enabled = false;
            this.alignmentgroupBox.Location = new System.Drawing.Point(151, 174);
            this.alignmentgroupBox.Name = "alignmentgroupBox";
            this.alignmentgroupBox.Size = new System.Drawing.Size(231, 79);
            this.alignmentgroupBox.TabIndex = 5;
            this.alignmentgroupBox.TabStop = false;
            this.alignmentgroupBox.Text = Labels.Config_InitializeComponent_Alignment;
            // 
            // rightButton
            // 
            this.rightButton.AutoSize = true;
            this.rightButton.Location = new System.Drawing.Point(162, 19);
            this.rightButton.Name = "rightButton";
            this.rightButton.Size = new System.Drawing.Size(50, 17);
            this.rightButton.TabIndex = 3;
            this.rightButton.TabStop = true;
            this.rightButton.Text = Labels.Config_InitializeComponent_Right;
            this.rightButton.UseVisualStyleBackColor = true;
            // 
            // defaultButton
            // 
            this.defaultButton.AutoSize = true;
            this.defaultButton.Location = new System.Drawing.Point(6, 51);
            this.defaultButton.Name = "defaultButton";
            this.defaultButton.Size = new System.Drawing.Size(59, 17);
            this.defaultButton.TabIndex = 2;
            this.defaultButton.TabStop = true;
            this.defaultButton.Text = Labels.Config_InitializeComponent_Default;
            this.defaultButton.UseVisualStyleBackColor = true;
            // 
            // centerradioButton
            // 
            this.centerradioButton.AutoSize = true;
            this.centerradioButton.Location = new System.Drawing.Point(80, 19);
            this.centerradioButton.Name = "centerradioButton";
            this.centerradioButton.Size = new System.Drawing.Size(56, 17);
            this.centerradioButton.TabIndex = 1;
            this.centerradioButton.TabStop = true;
            this.centerradioButton.Text = Labels.Config_InitializeComponent_Center;
            this.centerradioButton.UseVisualStyleBackColor = true;
            // 
            // leftButton
            // 
            this.leftButton.AutoSize = true;
            this.leftButton.Location = new System.Drawing.Point(6, 19);
            this.leftButton.Name = "leftButton";
            this.leftButton.Size = new System.Drawing.Size(43, 17);
            this.leftButton.TabIndex = 0;
            this.leftButton.TabStop = true;
            this.leftButton.Text = Labels.Config_InitializeComponent_Left;
            this.leftButton.UseVisualStyleBackColor = true;
            // 
            // widthgroupBox
            // 
            this.widthgroupBox.Controls.Add(this.spacificwidthtextBox);
            this.widthgroupBox.Controls.Add(this.bestfitradioButton);
            this.widthgroupBox.Controls.Add(this.specificwidthradioButton);
            this.widthgroupBox.Enabled = false;
            this.widthgroupBox.Location = new System.Drawing.Point(151, 92);
            this.widthgroupBox.Name = "widthgroupBox";
            this.widthgroupBox.Size = new System.Drawing.Size(231, 76);
            this.widthgroupBox.TabIndex = 4;
            this.widthgroupBox.TabStop = false;
            this.widthgroupBox.Text = Labels.Config_InitializeComponent_Width;
            // 
            // spacificwidthtextBox
            // 
            this.spacificwidthtextBox.Location = new System.Drawing.Point(103, 19);
            this.spacificwidthtextBox.Name = "spacificwidthtextBox";
            this.spacificwidthtextBox.Size = new System.Drawing.Size(100, 20);
            this.spacificwidthtextBox.TabIndex = 2;
            // 
            // bestfitradioButton
            // 
            this.bestfitradioButton.AutoSize = true;
            this.bestfitradioButton.Location = new System.Drawing.Point(6, 51);
            this.bestfitradioButton.Name = "bestfitradioButton";
            this.bestfitradioButton.Size = new System.Drawing.Size(57, 17);
            this.bestfitradioButton.TabIndex = 1;
            this.bestfitradioButton.TabStop = true;
            this.bestfitradioButton.Text = Labels.Config_InitializeComponent_Best_fit;
            this.bestfitradioButton.UseVisualStyleBackColor = true;
            // 
            // specificwidthradioButton
            // 
            this.specificwidthradioButton.AutoSize = true;
            this.specificwidthradioButton.Location = new System.Drawing.Point(6, 19);
            this.specificwidthradioButton.Name = "specificwidthradioButton";
            this.specificwidthradioButton.Size = new System.Drawing.Size(91, 17);
            this.specificwidthradioButton.TabIndex = 0;
            this.specificwidthradioButton.TabStop = true;
            this.specificwidthradioButton.Text = Labels.Config_InitializeComponent_Specific_width;
            this.specificwidthradioButton.UseVisualStyleBackColor = true;
            // 
            // formatcomboBox
            // 
            this.formatcomboBox.FormattingEnabled = true;
            this.formatcomboBox.Location = new System.Drawing.Point(193, 56);
            this.formatcomboBox.Name = "formatcomboBox";
            this.formatcomboBox.Size = new System.Drawing.Size(189, 21);
            this.formatcomboBox.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(148, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = Labels.Config_InitializeComponent_Format;
            // 
            // AttributeslistBox
            // 
            this.AttributeslistBox.FormattingEnabled = true;
            this.AttributeslistBox.Items.AddRange(new object[] {
            "balance",
            "income",
            "ROA",
            "ROE",
            "yield"});
            this.AttributeslistBox.Location = new System.Drawing.Point(21, 56);
            this.AttributeslistBox.Name = "AttributeslistBox";
            this.AttributeslistBox.Size = new System.Drawing.Size(121, 316);
            this.AttributeslistBox.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = Labels.Config_InitializeComponent_Attributes;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.categoryTreeView);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(417, 408);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = Labels.Config_InitializeComponent_Tree;
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // categoryTreeView
            // 
            this.categoryTreeView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.categoryTreeView.Location = new System.Drawing.Point(3, 3);
            this.categoryTreeView.Name = "categoryTreeView";
            this.categoryTreeView.Size = new System.Drawing.Size(411, 402);
            this.categoryTreeView.TabIndex = 0;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(425, 434);
            this.tabControl1.TabIndex = 1;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.ProfitsLossesCheckBoxList);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(417, 408);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = Labels.Config_InitializeComponent_Profits___Losses;
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // ProfitsLossesCheckBoxList
            // 
            this.ProfitsLossesCheckBoxList.CheckOnClick = true;
            this.ProfitsLossesCheckBoxList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ProfitsLossesCheckBoxList.FormattingEnabled = true;
            this.ProfitsLossesCheckBoxList.Location = new System.Drawing.Point(3, 3);
            this.ProfitsLossesCheckBoxList.Name = "ProfitsLossesCheckBoxList";
            this.ProfitsLossesCheckBoxList.Size = new System.Drawing.Size(411, 394);
            this.ProfitsLossesCheckBoxList.TabIndex = 2;
            this.ProfitsLossesCheckBoxList.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.ProfitsLossesCheckBoxList_ItemCheck);
            // 
            // Config
            // 
            this.AcceptButton = this.okButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(425, 478);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "Config";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = Labels.Config_InitializeComponent_Config;
            this.Load += new System.EventHandler(this.Config_Load);
            this.panel1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.doubleformatgroupBox.ResumeLayout(false);
            this.doubleformatgroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.alignmentgroupBox.ResumeLayout(false);
            this.alignmentgroupBox.PerformLayout();
            this.widthgroupBox.ResumeLayout(false);
            this.widthgroupBox.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button loadbutton;
        private System.Windows.Forms.Button saveasbutton;
        private System.Windows.Forms.Button savebutton;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox doubleformatgroupBox;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox showblankcheckBox;
        private System.Windows.Forms.CheckBox hidecheckBox;
        private System.Windows.Forms.CheckBox signcheckBox;
        private System.Windows.Forms.CheckBox commascheckBox;
        private System.Windows.Forms.CheckBox parenschekBox;
        private System.Windows.Forms.GroupBox alignmentgroupBox;
        private System.Windows.Forms.RadioButton rightButton;
        private System.Windows.Forms.RadioButton defaultButton;
        private System.Windows.Forms.RadioButton centerradioButton;
        private System.Windows.Forms.RadioButton leftButton;
        private System.Windows.Forms.GroupBox widthgroupBox;
        private System.Windows.Forms.TextBox spacificwidthtextBox;
        private System.Windows.Forms.RadioButton bestfitradioButton;
        private System.Windows.Forms.RadioButton specificwidthradioButton;
        private System.Windows.Forms.ComboBox formatcomboBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListBox AttributeslistBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabPage1;
        private DataTreeView categoryTreeView;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.CheckedListBox ProfitsLossesCheckBoxList;
    }
}