using System.Windows.Forms;
using AssetModelingModule.Domain;
using AssetModelingModule.Resources.Language;

namespace AssetModelingModule.Views.BalanceSheet.Result
{
    public partial class DividendForm : Form
    {

        private readonly DividendGridController _dividedGridController;

        public DividendForm()
        {
            InitializeComponent();

            if (!DesignMode && !Tools.InDesignMode)
            {
                _dividedGridController = new DividendGridController(dividedGridController);
            }
        }

        private void DividendForm_Load(object sender, System.EventArgs e)
        {
            _dividedGridController.Refresh(true);
        }

        private void addButton_Click(object sender, System.EventArgs e)
        {
            Balance.AddNewRowsPrompt addNewRowsPrompt = new Balance.AddNewRowsPrompt();
            addNewRowsPrompt.Text = Labels.DividendForm_addButton_Click_Dividend;
            if (addNewRowsPrompt.ShowDialog() == DialogResult.OK)
            {
               _dividedGridController.addNewItems(addNewRowsPrompt.RowCount);
            }
        }

        private void cancelButton_Click(object sender, System.EventArgs e)
        {
            foreach (Dividend dividend in Dividend.FindByStatus(Constants.Status.NEW))
            {
                dividend.DeleteAndFlush();
            }

            foreach (Dividend dividend in Dividend.FindByStatus(Constants.Status.DELETED))
            {
                dividend.Status = Constants.Status.SAVED;
            }

            _dividedGridController.Refresh(false);
        }

        private void okButton_Click(object sender, System.EventArgs e)
        {
            _dividedGridController.SaveDividend();
        }

        private void dividedGridController_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == _dividedGridController.amountcolIndex())
            {
                string amount = (dividedGridController[e.ColumnIndex, e.RowIndex].Value == null ? string.Empty :  dividedGridController[e.ColumnIndex, e.RowIndex].Value.ToString());
                
                decimal _amountdec;
                if (decimal.TryParse(amount, out _amountdec))
                {
                    dividedGridController[_dividedGridController.minimumcolIndex(), e.RowIndex].ReadOnly = false;
                }
                else
                {
                    dividedGridController[_dividedGridController.minimumcolIndex(), e.RowIndex].Value = null;
                    dividedGridController[_dividedGridController.minimumcolIndex(), e.RowIndex].ReadOnly = true;
                }
            }
        }

        private void deleteToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            Dividend[] dividends = Dividend.FindAll();
            if (dividedGridController.SelectedCells.Count > 0)
            {
                foreach (DataGridViewCell _cell in dividedGridController.SelectedCells)
                {
                    if (_cell.RowIndex >= 0)
                    {
                        CurrencyManager cm = (CurrencyManager)BindingContext[dividedGridController.DataSource];
                        if (dividends[_cell.RowIndex].Status == Constants.Status.NEW)
                        {
                            dividends[_cell.RowIndex].DeleteAndFlush();
                            _dividedGridController.DividendList.Remove(dividends[_cell.RowIndex]);

                        }
                        else
                        {
                            dividends[_cell.RowIndex].Status = Constants.Status.DELETED;
                            cm.ResumeBinding();
                            cm.SuspendBinding();
                            _cell.OwningRow.Visible = false;
                        }
                    }

                }
            }
        }

        private void dividedGridController_MouseDown(object sender, MouseEventArgs e)
        {
            DataGridView.HitTestInfo hti = this.dividedGridController.HitTest(e.X, e.Y);
            if (hti.Type == DataGridViewHitTestType.Cell && dividedGridController.SelectedCells.Count <= 1)
            {
                this.dividedGridController.ClearSelection();

                this.dividedGridController.Rows[hti.RowIndex].Cells[hti.ColumnIndex].Selected = true;
            }
        }
    }
}