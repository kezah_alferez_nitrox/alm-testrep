﻿using AssetModelingModule.Controls;
using AssetModelingModule.Resources.Language;

namespace AssetModelingModule.Views.BalanceSheet.Result
{
    partial class Result
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.resultDataGridView = new DataGridViewEx();
            this.resultHeaderPanel = new System.Windows.Forms.Panel();
            this.exportAllButton = new System.Windows.Forms.Button();
            this.userDefinedButton = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.reportTypeComboBox = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.scenarioComboBox = new System.Windows.Forms.ComboBox();
            this.printButton = new System.Windows.Forms.Button();
            this.configButton = new System.Windows.Forms.Button();
            this.resultLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.resultDataGridView)).BeginInit();
            this.resultHeaderPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // resultDataGridView
            // 
            this.resultDataGridView.AllowUserToAddRows = false;
            this.resultDataGridView.AllowUserToDeleteRows = false;
            this.resultDataGridView.BackgroundColor = System.Drawing.SystemColors.Window;
            this.resultDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.resultDataGridView.Location = new System.Drawing.Point(0, 34);
            this.resultDataGridView.Name = "resultDataGridView";
            this.resultDataGridView.ReadOnly = true;
            this.resultDataGridView.Size = new System.Drawing.Size(824, 437);
            this.resultDataGridView.TabIndex = 1;
            // 
            // resultHeaderPanel
            // 
            this.resultHeaderPanel.BackColor = System.Drawing.SystemColors.ControlDark;
            this.resultHeaderPanel.Controls.Add(this.exportAllButton);
            this.resultHeaderPanel.Controls.Add(this.userDefinedButton);
            this.resultHeaderPanel.Controls.Add(this.label9);
            this.resultHeaderPanel.Controls.Add(this.reportTypeComboBox);
            this.resultHeaderPanel.Controls.Add(this.label8);
            this.resultHeaderPanel.Controls.Add(this.scenarioComboBox);
            this.resultHeaderPanel.Controls.Add(this.printButton);
            this.resultHeaderPanel.Controls.Add(this.configButton);
            this.resultHeaderPanel.Controls.Add(this.resultLabel);
            this.resultHeaderPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.resultHeaderPanel.Location = new System.Drawing.Point(0, 0);
            this.resultHeaderPanel.Name = "resultHeaderPanel";
            this.resultHeaderPanel.Size = new System.Drawing.Size(824, 34);
            this.resultHeaderPanel.TabIndex = 0;
            // 
            // exportAllButton
            // 
            this.exportAllButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.exportAllButton.Location = new System.Drawing.Point(665, 6);
            this.exportAllButton.Name = "exportAllButton";
            this.exportAllButton.Size = new System.Drawing.Size(75, 23);
            this.exportAllButton.TabIndex = 9;
            this.exportAllButton.Text = Labels.Result_InitializeComponent_Export_All;
            this.exportAllButton.UseVisualStyleBackColor = true;
            this.exportAllButton.Click += new System.EventHandler(this.ExportAllButtonClick);
            // 
            // userDefinedButton
            // 
            this.userDefinedButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.userDefinedButton.Location = new System.Drawing.Point(109, 6);
            this.userDefinedButton.Name = "userDefinedButton";
            this.userDefinedButton.Size = new System.Drawing.Size(84, 23);
            this.userDefinedButton.TabIndex = 8;
            this.userDefinedButton.Text = Labels.Result_InitializeComponent_User_Defined;
            this.userDefinedButton.UseVisualStyleBackColor = true;
            this.userDefinedButton.Visible = false;
            this.userDefinedButton.Click += new System.EventHandler(this.userDefinedButton_Click);
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.AutoSize = true;
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(199, 13);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(66, 13);
            this.label9.TabIndex = 7;
            this.label9.Text = Labels.Result_InitializeComponent_Report_Type;
            // 
            // reportTypeComboBox
            // 
            this.reportTypeComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.reportTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.reportTypeComboBox.Location = new System.Drawing.Point(271, 8);
            this.reportTypeComboBox.Name = "reportTypeComboBox";
            this.reportTypeComboBox.Size = new System.Drawing.Size(157, 21);
            this.reportTypeComboBox.TabIndex = 6;
            this.reportTypeComboBox.SelectedIndexChanged += new System.EventHandler(this.reportTypeComboBox_SelectedIndexChanged);
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(434, 11);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(49, 13);
            this.label8.TabIndex = 5;
            this.label8.Text = Labels.Result_InitializeComponent_Scenario;
            // 
            // scenarioComboBox
            // 
            this.scenarioComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.scenarioComboBox.DisplayMember = "Label";
            this.scenarioComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.scenarioComboBox.FormattingEnabled = true;
            this.scenarioComboBox.Location = new System.Drawing.Point(489, 7);
            this.scenarioComboBox.Name = "scenarioComboBox";
            this.scenarioComboBox.Size = new System.Drawing.Size(89, 21);
            this.scenarioComboBox.TabIndex = 4;
            this.scenarioComboBox.ValueMember = "Rate";
            this.scenarioComboBox.SelectedIndexChanged += new System.EventHandler(this.scenarioComboBox_SelectedIndexChanged);
            // 
            // printButton
            // 
            this.printButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.printButton.Location = new System.Drawing.Point(584, 6);
            this.printButton.Name = "printButton";
            this.printButton.Size = new System.Drawing.Size(75, 23);
            this.printButton.TabIndex = 3;
            this.printButton.Text = "Print";
            this.printButton.UseVisualStyleBackColor = true;
            this.printButton.Click += new System.EventHandler(this.printButton_Click);
            // 
            // configButton
            // 
            this.configButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.configButton.Location = new System.Drawing.Point(746, 6);
            this.configButton.Name = "configButton";
            this.configButton.Size = new System.Drawing.Size(75, 23);
            this.configButton.TabIndex = 2;
            this.configButton.Text = Labels.Result_InitializeComponent_Config;
            this.configButton.UseVisualStyleBackColor = true;
            this.configButton.Click += new System.EventHandler(this.configButton_Click);
            // 
            // resultLabel
            // 
            this.resultLabel.AutoSize = true;
            this.resultLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.resultLabel.ForeColor = System.Drawing.Color.White;
            this.resultLabel.Location = new System.Drawing.Point(5, 5);
            this.resultLabel.Name = "resultLabel";
            this.resultLabel.Size = new System.Drawing.Size(68, 24);
            this.resultLabel.TabIndex = 1;
            this.resultLabel.Text = Labels.Result_InitializeComponent_Result;
            // 
            // Result
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.resultDataGridView);
            this.Controls.Add(this.resultHeaderPanel);
            this.Name = "Result";
            this.Size = new System.Drawing.Size(824, 471);
            this.Load += new System.EventHandler(this.Result_Load);
            ((System.ComponentModel.ISupportInitialize)(this.resultDataGridView)).EndInit();
            this.resultHeaderPanel.ResumeLayout(false);
            this.resultHeaderPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DataGridViewEx resultDataGridView;
        private System.Windows.Forms.Panel resultHeaderPanel;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox reportTypeComboBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox scenarioComboBox;
        private System.Windows.Forms.Button printButton;
        private System.Windows.Forms.Button configButton;
        private System.Windows.Forms.Label resultLabel;
        private System.Windows.Forms.Button userDefinedButton;
        private System.Windows.Forms.Button exportAllButton;
    }
}
