using System;
using System.Windows.Forms;

namespace AssetModelingModule.Views.Vectors
{
    public partial class IncrementValues : Form
    {
        public IncrementValues()
        {
            InitializeComponent();
        }

        public string IncrementValue
        {
            get
            {
                return incrementTextBox.Text;
            }
        }

        private void IncrementValues_Load(object sender, EventArgs e)
        {
            incrementTextBox.Focus();
            incrementTextBox.Select(0, incrementTextBox.Text.Length);
        }
    }
}