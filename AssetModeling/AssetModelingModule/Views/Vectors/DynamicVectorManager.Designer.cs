﻿using AssetModelingModule.Resources.Language;

namespace AssetModelingModule.Views.Vectors
{
    partial class DynamicVectorManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("Bermudan");
            System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("American");
            System.Windows.Forms.TreeNode treeNode3 = new System.Windows.Forms.TreeNode("European");
            System.Windows.Forms.TreeNode treeNode4 = new System.Windows.Forms.TreeNode("CapFloor");
            System.Windows.Forms.TreeNode treeNode5 = new System.Windows.Forms.TreeNode("BarrierOption");
            System.Windows.Forms.TreeNode treeNode6 = new System.Windows.Forms.TreeNode("Swap");
            System.Windows.Forms.TreeNode treeNode7 = new System.Windows.Forms.TreeNode("OneAsstetOption");
            System.Windows.Forms.TreeNode treeNode8 = new System.Windows.Forms.TreeNode("MultiAssetOption");
            System.Windows.Forms.TreeNode treeNode9 = new System.Windows.Forms.TreeNode("PricingEngines", new System.Windows.Forms.TreeNode[] {
            treeNode1,
            treeNode2,
            treeNode3,
            treeNode4,
            treeNode5,
            treeNode6,
            treeNode7,
            treeNode8});
            System.Windows.Forms.TreeNode treeNode10 = new System.Windows.Forms.TreeNode("SquareRootProcess");
            System.Windows.Forms.TreeNode treeNode11 = new System.Windows.Forms.TreeNode("OrnsteinUhlenbeckProcess");
            System.Windows.Forms.TreeNode treeNode12 = new System.Windows.Forms.TreeNode("GarmanKohlagenProcess");
            System.Windows.Forms.TreeNode treeNode13 = new System.Windows.Forms.TreeNode("BlackScholesMertonProcess ");
            System.Windows.Forms.TreeNode treeNode14 = new System.Windows.Forms.TreeNode("GeometricBrownianMotionProcess ");
            System.Windows.Forms.TreeNode treeNode15 = new System.Windows.Forms.TreeNode("StochasticProcess", new System.Windows.Forms.TreeNode[] {
            treeNode10,
            treeNode11,
            treeNode12,
            treeNode13,
            treeNode14});
            System.Windows.Forms.TreeNode treeNode16 = new System.Windows.Forms.TreeNode("ConvergenceStatistics");
            System.Windows.Forms.TreeNode treeNode17 = new System.Windows.Forms.TreeNode("GaussianStatistics");
            System.Windows.Forms.TreeNode treeNode18 = new System.Windows.Forms.TreeNode("RiskStatistics");
            System.Windows.Forms.TreeNode treeNode19 = new System.Windows.Forms.TreeNode("Statistics", new System.Windows.Forms.TreeNode[] {
            treeNode16,
            treeNode17,
            treeNode18});
            System.Windows.Forms.TreeNode treeNode20 = new System.Windows.Forms.TreeNode("BackwardFlat");
            System.Windows.Forms.TreeNode treeNode21 = new System.Windows.Forms.TreeNode("ForwardFlat");
            System.Windows.Forms.TreeNode treeNode22 = new System.Windows.Forms.TreeNode("Cubic");
            System.Windows.Forms.TreeNode treeNode23 = new System.Windows.Forms.TreeNode("Linear");
            System.Windows.Forms.TreeNode treeNode24 = new System.Windows.Forms.TreeNode("LogCubic");
            System.Windows.Forms.TreeNode treeNode25 = new System.Windows.Forms.TreeNode("LogLinear");
            System.Windows.Forms.TreeNode treeNode26 = new System.Windows.Forms.TreeNode("Interpolation", new System.Windows.Forms.TreeNode[] {
            treeNode20,
            treeNode21,
            treeNode22,
            treeNode23,
            treeNode24,
            treeNode25});
            System.Windows.Forms.TreeNode treeNode27 = new System.Windows.Forms.TreeNode("QuantLib", new System.Windows.Forms.TreeNode[] {
            treeNode9,
            treeNode15,
            treeNode19,
            treeNode26});
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DynamicVectorManager));
            this.bottomPanel = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.closeButton = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.vectorListBox = new System.Windows.Forms.ListBox();
            this.deleteButton = new System.Windows.Forms.Button();
            this.copyButton = new System.Windows.Forms.Button();
            this.newButton = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.functionsTreeView = new System.Windows.Forms.TreeView();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.staticVectorsListBox = new System.Windows.Forms.ListBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.bondPropertiesListBox = new System.Windows.Forms.ListBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.formulaTextBox = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.selectedNodeLabel = new System.Windows.Forms.Label();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.bottomPanel.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // bottomPanel
            // 
            this.bottomPanel.Controls.Add(this.button1);
            this.bottomPanel.Controls.Add(this.closeButton);
            this.bottomPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bottomPanel.Location = new System.Drawing.Point(0, 501);
            this.bottomPanel.Name = "bottomPanel";
            this.bottomPanel.Size = new System.Drawing.Size(959, 44);
            this.bottomPanel.TabIndex = 1;
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Location = new System.Drawing.Point(713, 6);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(114, 32);
            this.button1.TabIndex = 1;
            this.button1.Text = Labels.DynamicVectorManager_InitializeComponent_Simulate_On_Bond___;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // closeButton
            // 
            this.closeButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.closeButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.closeButton.Location = new System.Drawing.Point(833, 6);
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size(114, 32);
            this.closeButton.TabIndex = 0;
            this.closeButton.Text = Labels.DynamicVectorManager_InitializeComponent_Close;
            this.closeButton.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.vectorListBox);
            this.panel1.Controls.Add(this.deleteButton);
            this.panel1.Controls.Add(this.copyButton);
            this.panel1.Controls.Add(this.newButton);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.MinimumSize = new System.Drawing.Size(258, 414);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(258, 501);
            this.panel1.TabIndex = 13;
            // 
            // vectorListBox
            // 
            this.vectorListBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.vectorListBox.FormattingEnabled = true;
            this.vectorListBox.IntegralHeight = false;
            this.vectorListBox.Items.AddRange(new object[] {
            "DynamicVector1",
            "DynamicVector2"});
            this.vectorListBox.Location = new System.Drawing.Point(12, 4);
            this.vectorListBox.Name = "vectorListBox";
            this.vectorListBox.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.vectorListBox.Size = new System.Drawing.Size(234, 419);
            this.vectorListBox.TabIndex = 12;
            this.vectorListBox.SelectedIndexChanged += new System.EventHandler(this.vectorListBox_SelectedIndexChanged);
            // 
            // deleteButton
            // 
            this.deleteButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.deleteButton.Image = global::AssetModelingModule.Properties.Resources.DeleteHS;
            this.deleteButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.deleteButton.Location = new System.Drawing.Point(12, 470);
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Padding = new System.Windows.Forms.Padding(5);
            this.deleteButton.Size = new System.Drawing.Size(114, 32);
            this.deleteButton.TabIndex = 6;
            this.deleteButton.Text = Labels.DynamicVectorManager_InitializeComponent_Delete;
            this.deleteButton.UseVisualStyleBackColor = true;
            // 
            // copyButton
            // 
            this.copyButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.copyButton.Image = global::AssetModelingModule.Properties.Resources.CopyHS;
            this.copyButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.copyButton.Location = new System.Drawing.Point(132, 470);
            this.copyButton.Name = "copyButton";
            this.copyButton.Padding = new System.Windows.Forms.Padding(5);
            this.copyButton.Size = new System.Drawing.Size(114, 32);
            this.copyButton.TabIndex = 7;
            this.copyButton.Text = Labels.DynamicVectorManager_InitializeComponent_Copy___;
            this.copyButton.UseVisualStyleBackColor = true;
            // 
            // newButton
            // 
            this.newButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.newButton.Image = global::AssetModelingModule.Properties.Resources.NewDocumentHS;
            this.newButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.newButton.Location = new System.Drawing.Point(12, 432);
            this.newButton.Name = "newButton";
            this.newButton.Padding = new System.Windows.Forms.Padding(5);
            this.newButton.Size = new System.Drawing.Size(234, 32);
            this.newButton.TabIndex = 11;
            this.newButton.Text = Labels.DynamicVectorManager_InitializeComponent_Add_New_Vector___;
            this.newButton.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.splitContainer1);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(258, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(701, 501);
            this.panel2.TabIndex = 14;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 38);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.tableLayoutPanel1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.groupBox1);
            this.splitContainer1.Size = new System.Drawing.Size(701, 463);
            this.splitContainer1.SplitterDistance = 298;
            this.splitContainer1.TabIndex = 1;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.Controls.Add(this.groupBox5, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.groupBox4, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.groupBox3, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(701, 298);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.functionsTreeView);
            this.groupBox5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox5.Location = new System.Drawing.Point(469, 3);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(229, 292);
            this.groupBox5.TabIndex = 2;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = Labels.DynamicVectorManager_InitializeComponent_Functions;
            // 
            // functionsTreeView
            // 
            this.functionsTreeView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.functionsTreeView.ImageIndex = 0;
            this.functionsTreeView.ImageList = this.imageList1;
            this.functionsTreeView.Location = new System.Drawing.Point(3, 16);
            this.functionsTreeView.Name = "functionsTreeView";
            treeNode1.Name = "Node4";
            treeNode1.Text = "Bermudan";
            treeNode2.Name = "Node6";
            treeNode2.Text = "American";
            treeNode3.Name = "Node7";
            treeNode3.Text = "European";
            treeNode4.Name = "Node8";
            treeNode4.Text = "CapFloor";
            treeNode5.Name = "Node9";
            treeNode5.Text = "BarrierOption";
            treeNode6.Name = "Node12";
            treeNode6.Text = "Swap";
            treeNode7.Name = "Node11";
            treeNode7.Text = "OneAsstetOption";
            treeNode8.Name = "Node10";
            treeNode8.Text = "MultiAssetOption";
            treeNode9.Name = "Node2";
            treeNode9.Text = "PricingEngines";
            treeNode10.Name = "Node1";
            treeNode10.Text = "SquareRootProcess";
            treeNode11.Name = "Node2";
            treeNode11.Text = "OrnsteinUhlenbeckProcess";
            treeNode12.Name = "Node3";
            treeNode12.Text = "GarmanKohlagenProcess";
            treeNode13.Name = "Node4";
            treeNode13.Text = "BlackScholesMertonProcess ";
            treeNode14.Name = "Node5";
            treeNode14.Text = "GeometricBrownianMotionProcess ";
            treeNode15.Name = "Node0";
            treeNode15.Text = "StochasticProcess";
            treeNode16.Name = "Node8";
            treeNode16.Text = "ConvergenceStatistics";
            treeNode17.Name = "Node11";
            treeNode17.Text = "GaussianStatistics";
            treeNode18.Name = "Node12";
            treeNode18.Text = "RiskStatistics";
            treeNode19.Name = "Node7";
            treeNode19.Text = "Statistics";
            treeNode20.Name = "Node14";
            treeNode20.Text = "BackwardFlat";
            treeNode21.Name = "Node15";
            treeNode21.Text = "ForwardFlat";
            treeNode22.Name = "Node16";
            treeNode22.Text = "Cubic";
            treeNode23.Name = "Node17";
            treeNode23.Text = "Linear";
            treeNode24.Name = "Node18";
            treeNode24.Text = "LogCubic";
            treeNode25.Name = "Node19";
            treeNode25.Text = "LogLinear";
            treeNode26.Name = "Node13";
            treeNode26.Text = "Interpolation";
            treeNode27.Name = "Node0";
            treeNode27.Text = "QuantLib";
            this.functionsTreeView.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode27});
            this.functionsTreeView.SelectedImageIndex = 0;
            this.functionsTreeView.Size = new System.Drawing.Size(223, 273);
            this.functionsTreeView.TabIndex = 0;
            this.functionsTreeView.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.functionsTreeView_MouseDoubleClick);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.staticVectorsListBox);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox4.Location = new System.Drawing.Point(236, 3);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(227, 292);
            this.groupBox4.TabIndex = 1;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = Labels.DynamicVectorManager_InitializeComponent_Static_vectors;
            // 
            // staticVectorsListBox
            // 
            this.staticVectorsListBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.staticVectorsListBox.FormattingEnabled = true;
            this.staticVectorsListBox.IntegralHeight = false;
            this.staticVectorsListBox.Location = new System.Drawing.Point(3, 16);
            this.staticVectorsListBox.Name = "staticVectorsListBox";
            this.staticVectorsListBox.Size = new System.Drawing.Size(221, 273);
            this.staticVectorsListBox.TabIndex = 1;
            this.staticVectorsListBox.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.staticVectorsListBox_MouseDoubleClick);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.bondPropertiesListBox);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(3, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(227, 292);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = Labels.DynamicVectorManager_InitializeComponent_Bond_properties;
            // 
            // bondPropertiesListBox
            // 
            this.bondPropertiesListBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bondPropertiesListBox.FormattingEnabled = true;
            this.bondPropertiesListBox.IntegralHeight = false;
            this.bondPropertiesListBox.Items.AddRange(new object[] {
            "Bond_type",
            "Bond_growth",
            "Bond_accounting",
            "Bond_origFace",
            "Bond_currFace",
            "Bond_bookPrice",
            "Bond_bookValue",
            "Bond_marketPrice",
            "Bond_rollSpec",
            "Bond_coupon",
            "Bond_period",
            "Bond_duration",
            "Bond_tax",
            "Bond_attrib",
            "Bond_cpr",
            "Bond_cdr",
            "Bond_theoreticalCurrFace",
            "Bond_lgd",
            "Bond_basel2",
            "Bond_currency",
            "Bond_tci",
            "Bond_rwa"});
            this.bondPropertiesListBox.Location = new System.Drawing.Point(3, 16);
            this.bondPropertiesListBox.Name = "bondPropertiesListBox";
            this.bondPropertiesListBox.Size = new System.Drawing.Size(221, 273);
            this.bondPropertiesListBox.TabIndex = 0;
            this.bondPropertiesListBox.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listBox1_MouseDoubleClick);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.formulaTextBox);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(701, 161);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = Labels.DynamicVectorManager_InitializeComponent_Formula___i__;
            // 
            // formulaTextBox
            // 
            this.formulaTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.formulaTextBox.Location = new System.Drawing.Point(3, 16);
            this.formulaTextBox.Multiline = true;
            this.formulaTextBox.Name = "formulaTextBox";
            this.formulaTextBox.Size = new System.Drawing.Size(695, 142);
            this.formulaTextBox.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.ControlDark;
            this.panel3.Controls.Add(this.selectedNodeLabel);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(701, 38);
            this.panel3.TabIndex = 0;
            // 
            // selectedNodeLabel
            // 
            this.selectedNodeLabel.AutoSize = true;
            this.selectedNodeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.selectedNodeLabel.ForeColor = System.Drawing.Color.White;
            this.selectedNodeLabel.Location = new System.Drawing.Point(6, 7);
            this.selectedNodeLabel.Name = "selectedNodeLabel";
            this.selectedNodeLabel.Size = new System.Drawing.Size(0, 24);
            this.selectedNodeLabel.TabIndex = 1;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "RelationshipsHS.png");
            this.imageList1.Images.SetKeyName(1, "FunctionHS.png");
            // 
            // DynamicVectorManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.closeButton;
            this.ClientSize = new System.Drawing.Size(959, 545);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.bottomPanel);
            this.Name = "DynamicVectorManager";
            this.Text = Labels.DynamicVectorManager_InitializeComponent_Dynamic_Vector_Manager;
            this.bottomPanel.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel bottomPanel;
        private System.Windows.Forms.Button closeButton;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ListBox vectorListBox;
        private System.Windows.Forms.Button deleteButton;
        private System.Windows.Forms.Button copyButton;
        private System.Windows.Forms.Button newButton;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label selectedNodeLabel;
        private System.Windows.Forms.TextBox formulaTextBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TreeView functionsTreeView;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.ListBox staticVectorsListBox;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ListBox bondPropertiesListBox;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ImageList imageList1;
    }
}