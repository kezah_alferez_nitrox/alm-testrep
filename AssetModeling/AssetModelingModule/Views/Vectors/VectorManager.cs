using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using AssetModelingModule.Domain;
using AssetModelingModule.Resources.Language;

namespace AssetModelingModule.Views.Vectors
{
    public partial class VectorManager : Form
    {
        private BindingList<Vector> vectorList;

        private Vector selectedVector = null;

        public VectorManager()
        {
            InitializeComponent();

            vectorListBox.DisplayMember = "Name";
            vectorListBox.ValueMember = "Self";

            valuesDataGridView.AutoGenerateColumns = false;
        }

        private void VectorManager_Load(object sender, EventArgs e)
        {
            groupComboBox.SelectedIndex = 0;
        }

        private void modifyButton_Click(object sender, EventArgs e)
        {
            ModifySelectedVector();
        }

        private void ModifySelectedVector()
        {
            Vector selectedVector = vectorListBox.SelectedValue as Vector;
            if (selectedVector == null) return;

            if (Tools.ShowDialogWithPrivateSession(new VectorEditor(selectedVector)) == DialogResult.OK)
            {
                RefreshVectors();
            }
        }

        private void newButton_Click(object sender, EventArgs e)
        {
            if (Tools.ShowDialogWithPrivateSession(new VectorEditor()) == DialogResult.OK)
            {
                RefreshVectors();
            }
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            //Vector vector = vectorListBox.SelectedValue as Vector;
            //if (vector == null) return;
            int countSelectedVectors = vectorListBox.SelectedItems.Count;

            if ((countSelectedVectors > 1) ? MessageBox.Show(Labels.VectorManager_deleteButton_Click_Are_you_sure_you_want_to_delete__ + vectorListBox.SelectedItems.Count + Labels.VectorManager_deleteButton_Click___vectors_, "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes : true)
            {
                object[] _items = new object[vectorListBox.SelectedItems.Count];
                int _i = 0;
                foreach (object _item in vectorListBox.SelectedItems)
                {
                    _items[_i] = _item;
                    _i++;
                }
                foreach (object _item in _items) //; .SelectedIndices)
                {
                    Vector vector = _item as Vector;
                    //Vector vector = (vectorListBox.Items[_index] as Vector);
                    string varname = IsVectorUnique(vector.Name);
                    if (varname == string.Empty)
                    {
                        if (_items.Length == 1)
                        {
                            if (MessageBox.Show(Labels.VectorManager_deleteButton_Click_Are_you_sure_you_want_to_delete_vector_ + vector.Name + " ?", Labels.VectorManager_deleteButton_Click_Delete, MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) == DialogResult.Yes)
                            {
                                vectorList.Remove(vector);
                                vector.Delete();
                            }
                        }
                        else
                        {
                            vectorList.Remove(vector);
                            vector.Delete();
                            Views.BalanceSheet.BalanceSheet.modifiedScenarioTemplate = true;
                        }
                    }
                    else
                        MessageBox.Show(Labels.VectorManager_deleteButton_Click_Cannot_delete_vector_ + vector.Name + Labels.VectorManager_deleteButton_Click__because_it_is_in_use_by_variable_ + varname, Labels.VectorManager_deleteButton_Click_Error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private string IsVectorUnique(string VectorName)
        {
            string IsVU = string.Empty;
            VariableValue[] values = VariableValue.FindAll();
            for (int i = 0; i < values.Length; i++)
            {
                if (values[i].Value != null)
                {
                    foreach (string operand in Tools.GetOperandsFromExpression(values[i].Value))
                    {
                        if (VectorName == operand)
                        {
                            IsVU = values[i].Variable.Name;
                            break;
                        }

                    }
                    if (IsVU != string.Empty)
                        break;
                }
            }
            return IsVU;
        }

        private bool IsDuplicateVectorName(string VectorName)
        {
            Vector existingVector = Vector.FindByName(VectorName);
            if (existingVector != null)
            {
                //MessageBox.Show(Properties.Resources.DuplicateVector, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return true;
            }

            return false;
        }

        private void copyButton_Click(object sender, EventArgs e)
        {
            Vector vector = vectorListBox.SelectedValue as Vector;
            if (vector == null) return;

            Vector copy = vector.Copy();
            int _count = 1;
            string _copyname = copy.Name;
            copy.Name = _copyname + string.Format(Labels.VectorManager_copyButton_Click__Copy__0__, _count); // todo: increment copy number to ensure that the name is unique

            while (IsDuplicateVectorName(copy.Name))
            {
                _count++;
                copy.Name = _copyname + string.Format(Labels.VectorManager_copyButton_Click__Copy__0__, _count);
            }

            vectorList.Add(copy);
            //vectorListBox.SelectedItem = vectorListBox.Items[vectorListBox.Items.Count - 1];
            vectorListBox.SelectionMode = SelectionMode.One;
            vectorListBox.SelectedIndex = vectorListBox.Items.Count - 1;
            vectorListBox.SelectionMode = SelectionMode.MultiExtended;
            copy.Create();
            Views.BalanceSheet.BalanceSheet.modifiedScenarioTemplate = true;
            //RefreshVectors();
        }

        private void groupComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            TMI_MoveToWorkSpace.Visible = true;
            TMI_MarketDataVectors.Visible = true;
            TMI_MarketVolVectors.Visible = true;
            RefreshVectors();
            UpdateButtons();
            if (groupComboBox.SelectedIndex == 0)
                TMI_MoveToWorkSpace.Visible = false;
            if (groupComboBox.SelectedIndex == 1)
                TMI_MarketDataVectors.Visible = false;
            if (groupComboBox.SelectedIndex == 2)
                TMI_MarketVolVectors.Visible = false;
        }

        private void UpdateButtons()
        {

            newButton.Enabled = (groupComboBox.SelectedIndex == 0);

            modifyButton.Enabled = deleteButton.Enabled = copyButton.Enabled = (vectorListBox.SelectedIndex >= 0);
            modifyButton.Enabled = copyButton.Enabled = (vectorListBox.SelectedItems.Count == 1);

        }

        private void RefreshVectors()
        {
            Vector[] vectors = Vector.FindByGroup((VectorGroup)groupComboBox.SelectedIndex);
            vectorList = new BindingList<Vector>(new List<Vector>(vectors));
            vectorListBox.DataSource = vectorList;

            if (vectorList.Count == 0)
            {
                valuesDataGridView.DataSource = null;
            }
        }

        private void vectorsListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateButtons();

            
            Vector vector = vectorListBox.SelectedValue as Vector;

            if(selectedVector != null && selectedVector != vector)
            {
                PersistenceSession.Evict(selectedVector);
            }

            selectedVector = vector;

            if (vector == null)
            {
                valuesDataGridView.DataSource = null;
                return;
            }

            BindingList<VectorPoint> vectorPointList = new BindingList<VectorPoint>(new List<VectorPoint>(vector.VectorPoints));


            valuesDataGridView.DataSource = vectorPointList;

        }

        private void vectorListBox_DoubleClick(object sender, EventArgs e)
        {
            ModifySelectedVector();
        }

        private void TMI_MarketDataVectors_Click(object sender, EventArgs e)
        {
            ((Vector) vectorListBox.SelectedItem).Group = VectorGroup.MarketData;
            RefreshVectors();
        }

        private void TMI_MarketVolVectors_Click(object sender, EventArgs e)
        {
            ((Vector) vectorListBox.SelectedItem).Group = VectorGroup.MarketVol;
            RefreshVectors();
        }

        private void TMI_MoveToWorkSpace_Click(object sender, EventArgs e)
        {
            ((Vector) vectorListBox.SelectedItem).Group = VectorGroup.Workspace;
            RefreshVectors();
        }
    }
}