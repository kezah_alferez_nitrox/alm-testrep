﻿using AssetModelingModule.Resources.Language;

namespace AssetModelingModule.Views.Vectors
{
    partial class VectorManager
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VectorManager));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.bottomPanel = new System.Windows.Forms.Panel();
            this.closeButton = new System.Windows.Forms.Button();
            this.groupComboBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.newButton = new System.Windows.Forms.Button();
            this.modifyButton = new System.Windows.Forms.Button();
            this.copyButton = new System.Windows.Forms.Button();
            this.deleteButton = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.vectorListBox = new System.Windows.Forms.ListBox();
            this.CMS_VectorListBox = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.TMI_MoveToWorkSpace = new System.Windows.Forms.ToolStripMenuItem();
            this.TMI_MarketDataVectors = new System.Windows.Forms.ToolStripMenuItem();
            this.TMI_MarketVolVectors = new System.Windows.Forms.ToolStripMenuItem();
            this.valuesDataGridView = new System.Windows.Forms.DataGridView();
            this.periodColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.valueColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bottomPanel.SuspendLayout();
            this.panel1.SuspendLayout();
            this.CMS_VectorListBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.valuesDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // bottomPanel
            // 
            this.bottomPanel.Controls.Add(this.closeButton);
            this.bottomPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bottomPanel.Location = new System.Drawing.Point(0, 440);
            this.bottomPanel.Name = "bottomPanel";
            this.bottomPanel.Size = new System.Drawing.Size(492, 44);
            this.bottomPanel.TabIndex = 0;
            // 
            // closeButton
            // 
            this.closeButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.closeButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.closeButton.Location = new System.Drawing.Point(366, 6);
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size(114, 32);
            this.closeButton.TabIndex = 0;
            this.closeButton.Text = Labels.VectorManager_InitializeComponent_Close;
            this.closeButton.UseVisualStyleBackColor = true;
            // 
            // groupComboBox
            // 
            this.groupComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.groupComboBox.FormattingEnabled = true;
            this.groupComboBox.Items.AddRange(new object[] {
            Labels.VectorManager_InitializeComponent_Workspace,
            Labels.VectorManager_InitializeComponent_Market_Data_Vectors,
            Labels.VectorManager_InitializeComponent_Market_Vol_Vectors});
            this.groupComboBox.Location = new System.Drawing.Point(12, 29);
            this.groupComboBox.Name = "groupComboBox";
            this.groupComboBox.Size = new System.Drawing.Size(234, 21);
            this.groupComboBox.TabIndex = 8;
            this.groupComboBox.SelectedIndexChanged += new System.EventHandler(this.groupComboBox_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = Labels.VectorManager_InitializeComponent_Vector_groups__;
            // 
            // newButton
            // 
            this.newButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.newButton.Image = global::AssetModelingModule.Properties.Resources.NewDocumentHS;
            this.newButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.newButton.Location = new System.Drawing.Point(132, 371);
            this.newButton.Name = "newButton";
            this.newButton.Padding = new System.Windows.Forms.Padding(5);
            this.newButton.Size = new System.Drawing.Size(114, 32);
            this.newButton.TabIndex = 11;
            this.newButton.Text = Labels.VectorManager_InitializeComponent_New___;
            this.newButton.UseVisualStyleBackColor = true;
            this.newButton.Click += new System.EventHandler(this.newButton_Click);
            // 
            // modifyButton
            // 
            this.modifyButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.modifyButton.Image = ((System.Drawing.Image)(resources.GetObject("modifyButton.Image")));
            this.modifyButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.modifyButton.Location = new System.Drawing.Point(12, 371);
            this.modifyButton.Name = "modifyButton";
            this.modifyButton.Padding = new System.Windows.Forms.Padding(5);
            this.modifyButton.Size = new System.Drawing.Size(114, 32);
            this.modifyButton.TabIndex = 10;
            this.modifyButton.Text = Labels.VectorManager_InitializeComponent_Modify___;
            this.modifyButton.UseVisualStyleBackColor = true;
            this.modifyButton.Click += new System.EventHandler(this.modifyButton_Click);
            // 
            // copyButton
            // 
            this.copyButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.copyButton.Image = global::AssetModelingModule.Properties.Resources.CopyHS;
            this.copyButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.copyButton.Location = new System.Drawing.Point(132, 409);
            this.copyButton.Name = "copyButton";
            this.copyButton.Padding = new System.Windows.Forms.Padding(5);
            this.copyButton.Size = new System.Drawing.Size(114, 32);
            this.copyButton.TabIndex = 7;
            this.copyButton.Text = Labels.VectorManager_InitializeComponent_Copy___;
            this.copyButton.UseVisualStyleBackColor = true;
            this.copyButton.Click += new System.EventHandler(this.copyButton_Click);
            // 
            // deleteButton
            // 
            this.deleteButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.deleteButton.Image = global::AssetModelingModule.Properties.Resources.DeleteHS;
            this.deleteButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.deleteButton.Location = new System.Drawing.Point(12, 409);
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Padding = new System.Windows.Forms.Padding(5);
            this.deleteButton.Size = new System.Drawing.Size(114, 32);
            this.deleteButton.TabIndex = 6;
            this.deleteButton.Text = Labels.VectorManager_InitializeComponent_Delete;
            this.deleteButton.UseVisualStyleBackColor = true;
            this.deleteButton.Click += new System.EventHandler(this.deleteButton_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.vectorListBox);
            this.panel1.Controls.Add(this.groupComboBox);
            this.panel1.Controls.Add(this.deleteButton);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.copyButton);
            this.panel1.Controls.Add(this.newButton);
            this.panel1.Controls.Add(this.modifyButton);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.MinimumSize = new System.Drawing.Size(258, 414);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(258, 440);
            this.panel1.TabIndex = 12;
            // 
            // vectorListBox
            // 
            this.vectorListBox.ContextMenuStrip = this.CMS_VectorListBox;
            this.vectorListBox.FormattingEnabled = true;
            this.vectorListBox.Location = new System.Drawing.Point(12, 56);
            this.vectorListBox.Name = "vectorListBox";
            this.vectorListBox.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.vectorListBox.Size = new System.Drawing.Size(234, 277);
            this.vectorListBox.TabIndex = 12;
            this.vectorListBox.DoubleClick += new System.EventHandler(this.vectorListBox_DoubleClick);
            this.vectorListBox.SelectedIndexChanged += new System.EventHandler(this.vectorsListBox_SelectedIndexChanged);
            // 
            // CMS_VectorListBox
            // 
            this.CMS_VectorListBox.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TMI_MoveToWorkSpace,
            this.TMI_MarketDataVectors,
            this.TMI_MarketVolVectors});
            this.CMS_VectorListBox.Name = "CMS_VectorListBox";
            this.CMS_VectorListBox.Size = new System.Drawing.Size(260, 70);
            // 
            // TMI_MoveToWorkSpace
            // 
            this.TMI_MoveToWorkSpace.Name = "TMI_MoveToWorkSpace";
            this.TMI_MoveToWorkSpace.Size = new System.Drawing.Size(259, 22);
            this.TMI_MoveToWorkSpace.Text = Labels.VectorManager_InitializeComponent_Move_Vector_to_Workspace;
            this.TMI_MoveToWorkSpace.Click += new System.EventHandler(this.TMI_MoveToWorkSpace_Click);
            // 
            // TMI_MarketDataVectors
            // 
            this.TMI_MarketDataVectors.Name = "TMI_MarketDataVectors";
            this.TMI_MarketDataVectors.Size = new System.Drawing.Size(259, 22);
            this.TMI_MarketDataVectors.Text = Labels.VectorManager_InitializeComponent_Move_Vector_to_Market_Data_Vectors;
            this.TMI_MarketDataVectors.Click += new System.EventHandler(this.TMI_MarketDataVectors_Click);
            // 
            // TMI_MarketVolVectors
            // 
            this.TMI_MarketVolVectors.Name = "TMI_MarketVolVectors";
            this.TMI_MarketVolVectors.Size = new System.Drawing.Size(259, 22);
            this.TMI_MarketVolVectors.Text = Labels.VectorManager_InitializeComponent_Move_Vector_to_Market_Vol_Vectors_;
            this.TMI_MarketVolVectors.Click += new System.EventHandler(this.TMI_MarketVolVectors_Click);
            // 
            // valuesDataGridView
            // 
            this.valuesDataGridView.AllowUserToAddRows = false;
            this.valuesDataGridView.AllowUserToDeleteRows = false;
            this.valuesDataGridView.BackgroundColor = System.Drawing.SystemColors.Window;
            this.valuesDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.valuesDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.periodColumn,
            this.valueColumn});
            this.valuesDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.valuesDataGridView.Location = new System.Drawing.Point(258, 0);
            this.valuesDataGridView.Name = "valuesDataGridView";
            this.valuesDataGridView.RowHeadersWidth = 23;
            this.valuesDataGridView.Size = new System.Drawing.Size(234, 440);
            this.valuesDataGridView.TabIndex = 13;
            // 
            // periodColumn
            // 
            this.periodColumn.DataPropertyName = "Date";
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle3.Format = "MM/yyyy";
            dataGridViewCellStyle3.NullValue = null;
            this.periodColumn.DefaultCellStyle = dataGridViewCellStyle3;
            this.periodColumn.HeaderText = Labels.VectorManager_InitializeComponent_Period;
            this.periodColumn.Name = "periodColumn";
            this.periodColumn.ReadOnly = true;
            this.periodColumn.Width = 80;
            // 
            // valueColumn
            // 
            this.valueColumn.DataPropertyName = "Value";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.Format = "N5";
            this.valueColumn.DefaultCellStyle = dataGridViewCellStyle4;
            this.valueColumn.HeaderText = Labels.VectorManager_InitializeComponent_Value;
            this.valueColumn.Name = "valueColumn";
            this.valueColumn.ReadOnly = true;
            // 
            // VectorManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.closeButton;
            this.ClientSize = new System.Drawing.Size(492, 484);
            this.ControlBox = false;
            this.Controls.Add(this.valuesDataGridView);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.bottomPanel);
            this.MinimumSize = new System.Drawing.Size(16, 492);
            this.Name = "VectorManager";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = Labels.VectorManager_InitializeComponent_Vector_Manager;
            this.Load += new System.EventHandler(this.VectorManager_Load);
            this.bottomPanel.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.CMS_VectorListBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.valuesDataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel bottomPanel;
        private System.Windows.Forms.Button closeButton;
        private System.Windows.Forms.ComboBox groupComboBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button newButton;
        private System.Windows.Forms.Button modifyButton;
        private System.Windows.Forms.Button copyButton;
        private System.Windows.Forms.Button deleteButton;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ListBox vectorListBox;
        private System.Windows.Forms.DataGridView valuesDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn periodColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn valueColumn;
        private System.Windows.Forms.ContextMenuStrip CMS_VectorListBox;
        private System.Windows.Forms.ToolStripMenuItem TMI_MarketDataVectors;
        private System.Windows.Forms.ToolStripMenuItem TMI_MarketVolVectors;
        private System.Windows.Forms.ToolStripMenuItem TMI_MoveToWorkSpace;
    }
}