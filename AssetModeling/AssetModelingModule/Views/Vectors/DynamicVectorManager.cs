﻿using System;
using System.Windows.Forms;
using AssetModelingModule.BusinessLogic;

namespace AssetModelingModule.Views.Vectors
{
    public partial class DynamicVectorManager : Form
    {
        public DynamicVectorManager()
        {
            this.InitializeComponent();

            this.Load += this.DynamicVectorManager_Load;

            vectorListBox.SelectedIndex = 0;

            AddExcelFunction();

            functionsTreeView.ExpandAll();

            SetIncons(functionsTreeView.Nodes);
        }

        private void SetIncons(TreeNodeCollection collection)
        {
            foreach (TreeNode node in collection)
            {
                if(node.Nodes.Count == 0)
                {
                    node.ImageIndex = 1;
                }

                this.SetIncons(node.Nodes);
            }
        }

        private void AddExcelFunction()
        {
            TreeNode node = this.functionsTreeView.Nodes.Add("Basic");

            node.Nodes.Add("ACCRINT");
            node.Nodes.Add("ACCRINTM");
            node.Nodes.Add("AMORDEGRC");
            node.Nodes.Add("AMORLINC");
            node.Nodes.Add("COUPDAYBS");
            node.Nodes.Add("COUPDAYS");
            node.Nodes.Add("COUPDAYSNC");
            node.Nodes.Add("COUPNCD");
            node.Nodes.Add("COUPNUM");
            node.Nodes.Add("COUPPCD");
            node.Nodes.Add("CUMIPMT");
            node.Nodes.Add("CUMPRINC");
            node.Nodes.Add("DB");
            node.Nodes.Add("DDB");
            node.Nodes.Add("DISC");
            node.Nodes.Add("DOLLARDE");
            node.Nodes.Add("DOLLARFR");
            node.Nodes.Add("DURATION");
            node.Nodes.Add("EFFECT");
            node.Nodes.Add("FV");
            node.Nodes.Add("FVSCHEDULE");
            node.Nodes.Add("INTRATE");
            node.Nodes.Add("IPMT");
            node.Nodes.Add("IRR");
            node.Nodes.Add("ISPMT");
            node.Nodes.Add("MDURATION");
            node.Nodes.Add("MIRR");
            node.Nodes.Add("NOMINAL");
            node.Nodes.Add("NPER");
            node.Nodes.Add("NPV");
            node.Nodes.Add("ODDFPRICE");
            node.Nodes.Add("ODDFYIELD");
            node.Nodes.Add("ODDLPRICE");
            node.Nodes.Add("ODDLYIELD");
            node.Nodes.Add("PMT");
            node.Nodes.Add("PPMT");
            node.Nodes.Add("PRICE");
            node.Nodes.Add("PRICEDISC");
            node.Nodes.Add("PRICEMAT");
            node.Nodes.Add("PV");
            node.Nodes.Add("RATE");
            node.Nodes.Add("RECEIVED");
            node.Nodes.Add("SLN");
            node.Nodes.Add("SYD");
            node.Nodes.Add("TBILLEQ");
            node.Nodes.Add("TBILLPRICE");
            node.Nodes.Add("TBILLYIELD");
            node.Nodes.Add("VDB");
            node.Nodes.Add("XIRR");
            node.Nodes.Add("XNPV");
            node.Nodes.Add("YIELD");
            node.Nodes.Add("YIELDDISC");
            node.Nodes.Add("YIELDMAT");
        }

        private void DynamicVectorManager_Load(object sender, EventArgs e)
        {
            string[] vectors = DomainTools.GetAllVectorNames();
            this.staticVectorsListBox.DataSource = vectors;
        }

        private void vectorListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.selectedNodeLabel.Text = Convert.ToString(this.vectorListBox.SelectedItem);
            this.selectedNodeLabel.Focus();
        }

        private void listBox1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (this.bondPropertiesListBox.SelectedItem == null) return;

            this.formulaTextBox.Text += this.bondPropertiesListBox.SelectedItem;
            this.selectedNodeLabel.Focus();
        }

        private void staticVectorsListBox_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if(staticVectorsListBox.SelectedItem == null) return;

            this.formulaTextBox.Text += staticVectorsListBox.SelectedItem + "[i]";
            this.selectedNodeLabel.Focus();
        }

        private void functionsTreeView_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (functionsTreeView.SelectedNode == null || functionsTreeView.SelectedNode.Nodes.Count != 0) return;

            this.formulaTextBox.Text += functionsTreeView.SelectedNode.Text;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DynamicVectorSimulation simulation = new DynamicVectorSimulation();
            simulation.ShowDialog(this);
        }
    }
}