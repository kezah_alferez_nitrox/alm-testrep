using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using AssetModelingModule.Domain;
using AssetModelingModule.Resources.Language;
using ZedGraph;

namespace AssetModelingModule.Views.Vectors
{
    public partial class VectorEditor : Form
    {
        private bool valid = true;

        private bool isNewVector;
        private Vector vector;
        private BindingList<VectorPoint> vectorPointList = new BindingList<VectorPoint>();
        private LineItem graphCurve;

        public VectorEditor()
        {
            InitializeComponent();

            frequencyComboBox.SelectedIndex = 0;
            valuesDataGridView.AutoGenerateColumns = false;

            zedGraphControl.GraphPane.Title.IsVisible = false;
            zedGraphControl.GraphPane.Legend.IsVisible = false;
            zedGraphControl.GraphPane.XAxis.Type = AxisType.Date;
            zedGraphControl.GraphPane.XAxis.Title.IsVisible = false;
            zedGraphControl.GraphPane.YAxis.Title.IsVisible = false;
            zedGraphControl.GraphPane.YAxis.MajorGrid.IsVisible = true;
        }

        public VectorEditor(Vector vector)
            : this()
        {
            this.vector = Vector.Find(vector.Id);
        }

        private void VectorEditor_Load(object sender, EventArgs e)
        {
            CB_Currency.DataSource = Currency.FindAll();
            CB_Currency.DisplayMember = "Symbol";
            CB_Currency.ValueMember = "Self";
            CB_Currency.SelectedIndex = 0;

            if (vector == null)
            {
                isNewVector = true;
                vector = new Vector();
                vector.Group = VectorGroup.Workspace;
                vectorPointList = new BindingList<VectorPoint>();
                frequencyComboBox.SelectedIndex = (int)VectorFrequency.Quaterly;
            }
            else
            {
                isNewVector = false;
                nameTextBox.Text = vector.Name;
                importantCheckBox.Checked = vector.ImportantMarketData;
                vectorPointList = GetAdjustedPointList();
                if (vector.Currency != null)
                    CB_Currency.SelectedValue = vector.Currency;

                frequencyComboBox.SelectedIndex = (int)vector.Frequency;
            }

            valuesDataGridView.DataSource = vectorPointList;
        }

        private BindingList<VectorPoint> GetAdjustedPointList()
        {
            List<VectorPoint> pointList = new List<VectorPoint>(vector.VectorPoints);



            return new BindingList<VectorPoint>(pointList);
        }

        private void frequencyComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            DateTime now = Param.ValuationDate ?? DateTime.Today;
            DateTime startDate = new DateTime(now.Year, now.Month, DateTime.DaysInMonth(now.Year, now.Month));
            DateTime endDate = startDate.AddYears(Constants.VECTOR_YEARS);

            vectorPointList.Clear();

            BindingList<VectorPoint> originalvectorPointList = null;

            if (vector != null)
            {
                originalvectorPointList = new BindingList<VectorPoint>(new List<VectorPoint>(vector.VectorPoints));
                if (vector.Id != 0 && vector.VectorPoints.Count > 0)
                {
                    endDate = vector.VectorPoints[vector.VectorPoints.Count - 1].Date;
                    endDate = endDate > startDate.AddYears(Constants.VECTOR_YEARS) ? endDate : startDate.AddYears(Constants.VECTOR_YEARS);
                }
            }

            int increment;
            switch ((VectorFrequency)frequencyComboBox.SelectedIndex)
            {
                case VectorFrequency.None:
                    return;
                case VectorFrequency.Monthly:
                    increment = 1;
                    break;
                case VectorFrequency.Quaterly:
                    increment = 3;
                    break;
                case VectorFrequency.SemiAnnual:
                    increment = 6;
                    break;
                case VectorFrequency.Annual:
                    increment = 12;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            DateTime date = startDate.Date;
            int _j = 0;

            if (originalvectorPointList.Count != 0)
            {
                VectorPoint _auxvp = originalvectorPointList[0];

                while ((date.Year * 12 + date.Month) <= (endDate.Year * 12 + endDate.Month)) //&& vectorPointList.Count < vector.VectorPoints.Count)
                {
                    VectorPoint newPoint = vectorPointList.AddNew();
                    newPoint.Date = date;
                    foreach (VectorPoint vp in originalvectorPointList)
                    {
                        if (newPoint.Date.Month == vp.Date.Month && newPoint.Date.Year == vp.Date.Year)
                        {
                            newPoint = vp;
                            _auxvp = vp;
                        }
                        else
                        {
                            if (newPoint.Date > _auxvp.Date)
                                newPoint.Value = _auxvp.Value;
                        }

                    }

                    vectorPointList[_j] = newPoint;
                    _j++;
                    date = date.AddMonths(increment);
                    date = new DateTime(date.Year, date.Month, DateTime.DaysInMonth(date.Year, date.Month));

                }
                vectorPointList.ResetBindings();
            }
            else
            {
                while ((date.Year * 12 + date.Month) <= (endDate.Year * 12 + date.Month) && vectorPointList.Count < vector.VectorPoints.Count)
                {
                    VectorPoint newPoint = vectorPointList.AddNew();
                    newPoint.Date = date;
                    vectorPointList[_j] = newPoint;
                    _j++;
                    date = date.AddMonths(increment);
                    date = new DateTime(date.Year, date.Month, DateTime.DaysInMonth(date.Year, date.Month));
                }
                if (vector.VectorPoints.Count == 0)
                {
                    int i = 0;
                    while ((date.Year * 12 + date.Month) <= (endDate.Year * 12 + date.Month))
                    {
                        VectorPoint newPoint = vectorPointList.AddNew();
                        newPoint.Date = date;
                        vectorPointList[i] = newPoint;
                        i++;
                        date = date.AddMonths(increment);
                        date = new DateTime(date.Year, date.Month, DateTime.DaysInMonth(date.Year, date.Month));
                    }
                }
                vectorPointList.ResetBindings();
            }

            InitializeGraph();
        }

        private void InitializeGraph()
        {
            GraphPane zedPane = zedGraphControl.GraphPane;

            zedPane.CurveList.Clear();

            PointPairList list = new PointPairList();
            foreach (VectorPoint vectorPoint in vectorPointList)
            {

                double x = new XDate(vectorPoint.Date);
                double y = (double)vectorPoint.Value;
                list.Add(x, y);
            }

            graphCurve = zedPane.AddCurve(Labels.VectorEditor_InitializeGraph_Values, list, Color.DarkGreen);
            graphCurve.Symbol.Type = SymbolType.Circle;
            graphCurve.Symbol.Fill = new Fill(Color.White);
            graphCurve.Symbol.Size = 8;

            zedGraphControl.AxisChange();
            zedGraphControl.Invalidate();
        }

        private string zedGraphControl_PointEditEvent(ZedGraphControl sender, GraphPane pane, CurveItem curve, int iPt)
        {
            PointPair pt = curve[iPt];
            foreach (VectorPoint point in vectorPointList)
            {
                if (XDate.XLDateToDateTime(pt.X) == point.Date)
                {
                    point.Value = Convert.ToDecimal(Math.Round(pt.Y, 5));
                    break;
                }
            }
            vectorPointList.ResetBindings();
            return null;
        }

        private void valuesDataGridView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0) return;

            decimal value = (decimal)valuesDataGridView[e.ColumnIndex, e.RowIndex].Value;
            vectorPointList[e.RowIndex].Value = value;

            graphCurve.Points[e.RowIndex].Y = (double)value;

            zedGraphControl.AxisChange();
            zedGraphControl.Invalidate();
        }

        private void nameTextBox_TextChanged(object sender, EventArgs e)
        {
            okButton.Enabled = !string.IsNullOrEmpty(nameTextBox.Text);
            // TODO ?! (tooltips unavailable for disabled controls)
            //if(!okButton.Enabled)
            //{
            //    toolTip.SetToolTip(okButton, "A Name is required");
            //}else
            //{
            //    toolTip.SetToolTip(okButton, null);
            //}
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            valid = !IsValidVectorName();
            if (!valid) return;

            vector.Name = nameTextBox.Text;
            vector.Frequency = (VectorFrequency)frequencyComboBox.SelectedIndex;
            vector.ImportantMarketData = importantCheckBox.Checked;

            if (!isNewVector)
            {
                vector.VectorPoints.Clear();
            }

            foreach (VectorPoint point in vectorPointList)
            {
                point.Vector = vector;
                vector.VectorPoints.Add(point);
            }

            if (isNewVector)
            {
                vector.CreateAndFlush();
            }
            else
            {
                vector.UpdateAndFlush();
            }

            vector.Currency = Currency.FindFirstBySymbol(CB_Currency.Text);
            Views.BalanceSheet.BalanceSheet.modifiedScenarioTemplate = true;
        }

        private void VectorEditor_Activated(object sender, EventArgs e)
        {
            nameTextBox.Focus();
        }

        private bool IsValidVectorName()
        {
            if (!Constants.VectorOrVariableRegex.IsMatch(nameTextBox.Text))
            {
                MessageBox.Show(string.Format(Properties.Resources.InvalidVectorOrVariableName, nameTextBox.Text), "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return true;
            }

            if (Tools.IsFunctionName(nameTextBox.Text))
            {
                MessageBox.Show(string.Format(Properties.Resources.ReservedFunctionName, nameTextBox.Text), "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return true;
            }

            Vector existingVector = Vector.FindByName(nameTextBox.Text);
            if (existingVector != null && existingVector != vector)
            {
                MessageBox.Show(Properties.Resources.DuplicateVector, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return true;
            }

            return false;
        }

        private void VectorEditor_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = !valid;
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            valid = true;
        }

        private void valuesDataGridView_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            e.Cancel = false;
            e.ThrowException = false;
        }

        private void CopyValuetoAllCellsBelowtoolStripMenuItem_Click(object sender, EventArgs e)
        {
            int _sel = valuesDataGridView.SelectedCells[0].RowIndex;
            string _val = valuesDataGridView.SelectedCells[0].Value.ToString();
            for (int i = _sel; i < valuesDataGridView.Rows.Count; i++)
            {
                valuesDataGridView.Rows[i].Cells[1].Value = _val;
            }
        }

        private void CopyValuetoAllCellsBelowwithIncrementtoolStripMenuItem_Click(object sender, EventArgs e)
        {
            IncrementValues _incv = new IncrementValues();
            DialogResult dialogResult = _incv.ShowDialog();
            if (dialogResult == DialogResult.Cancel) return;

            if (dialogResult == DialogResult.OK)
            {
                decimal _incremetValue = decimal.Parse(_incv.IncrementValue); //TODO: if the entered values is not decimal
                int _sel = valuesDataGridView.SelectedCells[0].RowIndex;
                string _val = valuesDataGridView[valueColumn.Index, _sel].Value.ToString(); //.SelectedCells[0].Value.ToString();
                for (int i = _sel + 1; i < valuesDataGridView.Rows.Count; i++)
                {
                    valuesDataGridView.Rows[i].Cells[1].Value = decimal.Parse(_val) + _incremetValue;
                    _val = valuesDataGridView.Rows[i].Cells[1].Value.ToString();
                }
            }
        }

        private void pasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string clipboardtext = Clipboard.GetText();
            if (string.IsNullOrEmpty(clipboardtext)) return;
            if (valuesDataGridView.CurrentCell == null || valuesDataGridView.CurrentCell.RowIndex < 0) return;
            string[] clipboardrows = clipboardtext.Split(new[] { Environment.NewLine, "\t" }, StringSplitOptions.RemoveEmptyEntries);

            if (clipboardrows.Length == 0) return;

            int startIndex = valuesDataGridView.CurrentCell.RowIndex;

            for (int i = 0; i < clipboardrows.Length; i++)
            {
                if (startIndex >= valuesDataGridView.Rows.Count) break;
                decimal? decimalValue = Tools.ClipboardValueToDecimal(clipboardrows[i]);
                if (decimalValue != null)
                {
                    valuesDataGridView.Rows[startIndex].Cells[1].Value = decimalValue.Value;
                    startIndex++;
                }
            }
        }

        private void valuesDataGridView_MouseDown(object sender, MouseEventArgs e)
        {
            DataGridView.HitTestInfo hti = this.valuesDataGridView.HitTest(e.X, e.Y);
            if (hti.Type == DataGridViewHitTestType.Cell)
            {
                this.valuesDataGridView.ClearSelection();

                this.valuesDataGridView.Rows[hti.RowIndex].Cells[hti.ColumnIndex].Selected =
                true;
            }
        }

        private void nameTextBox_Leave(object sender, EventArgs e)
        {
            nameTextBox.Text = Tools.EscapeIdentifier(nameTextBox.Text);
        }
    }
}