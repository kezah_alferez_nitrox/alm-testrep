using AssetModelingModule.Resources.Language;

namespace AssetModelingModule.Views.Vectors
{
	partial class IncrementValues
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.okButton = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.incrementTextBox = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.incrementTextBox)).BeginInit();
            this.SuspendLayout();
            // 
            // okButton
            // 
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okButton.Location = new System.Drawing.Point(60, 63);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(84, 20);
            this.okButton.TabIndex = 8;
            this.okButton.Text = Labels.IncrementValues_InitializeComponent_OK;
            this.okButton.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button1.Location = new System.Drawing.Point(184, 63);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(84, 20);
            this.button1.TabIndex = 9;
            this.button1.Text = Labels.IncrementValues_InitializeComponent_Cancel;
            this.button1.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(57, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(110, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = Labels.IncrementValues_InitializeComponent_Enter_increment_value;
            // 
            // incrementTextBox
            // 
            this.incrementTextBox.DecimalPlaces = 5;
            this.incrementTextBox.Location = new System.Drawing.Point(184, 21);
            this.incrementTextBox.Maximum = new decimal(new int[] {
            1874919423,
            2328306,
            0,
            0});
            this.incrementTextBox.Minimum = new decimal(new int[] {
            1874919423,
            2328306,
            0,
            -2147483648});
            this.incrementTextBox.Name = "incrementTextBox";
            this.incrementTextBox.Size = new System.Drawing.Size(84, 20);
            this.incrementTextBox.TabIndex = 1;
            // 
            // IncrementValues
            // 
            this.AcceptButton = this.okButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.button1;
            this.ClientSize = new System.Drawing.Size(324, 105);
            this.Controls.Add(this.incrementTextBox);
            this.Controls.Add(this.okButton);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label1);
            this.Name = "IncrementValues";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = Labels.IncrementValues_InitializeComponent_Increment_Values;
            this.Load += new System.EventHandler(this.IncrementValues_Load);
            ((System.ComponentModel.ISupportInitialize)(this.incrementTextBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown incrementTextBox;

    }
}