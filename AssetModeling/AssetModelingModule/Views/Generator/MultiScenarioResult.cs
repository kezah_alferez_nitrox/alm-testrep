﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using ALMSCommon.Forms;
using AssetModelingModule.Calculus;
using AssetModelingModule.Core;
using AssetModelingModule.Core.IO;
using AssetModelingModule.Domain;
using AssetModelingModule.Resources.Language;
using Castle.ActiveRecord;
using ZedGraph;

namespace AssetModelingModule.Views.Generator
{
    public partial class MultiScenarioResult : UserControl
    {
        private Simulation simulation;

        public MultiScenarioResult()
        {
            this.InitializeComponent();
        }

        public GeneratorResultNode[] GetGeneratorResult()
        {
            if(!includeResultsCheckBox.Checked) return null;

            return GetGeneratorResult(treeView.Nodes);
        }

        private static GeneratorResultNode[] GetGeneratorResult(TreeNodeCollection treeNodes)
        {
            if(treeNodes == null || treeNodes.Count == 0) return null;

            GeneratorResultNode[] result = new GeneratorResultNode[treeNodes.Count];
            for (int i = 0; i < treeNodes.Count; i++)
            {
                TreeNode treeNode = treeNodes[i];
                result[i] = new GeneratorResultNode
                    {
                        Label = treeNode.Text,
                        Data = treeNode.Tag as double[][],
                        Nodes = GetGeneratorResult(treeNode.Nodes),
                        IsExpanded = treeNode.IsExpanded,
                    };
            }

            return result;
        }

        public void SetGeneratorResult(GeneratorResultNode[] nodes)
        {
            treeView.Nodes.Clear();
            SetGeneratorResult(treeView.Nodes, nodes);
        }

        private static void SetGeneratorResult(TreeNodeCollection treeNodes, GeneratorResultNode[] generatorResultNodes)
        {
            if(generatorResultNodes == null || generatorResultNodes.Length ==0) return;

            foreach (GeneratorResultNode resultNode in generatorResultNodes)
            {
                TreeNode treeNode = treeNodes.Add(resultNode.Label);
                treeNode.Tag = resultNode.Data;

                SetGeneratorResult(treeNode.Nodes, resultNode.Nodes);

                if(resultNode.IsExpanded)
                {
                    treeNode.Expand();
                }
            }
        }

        private void treeView_DoubleClick(object sender, EventArgs e)
        {
            if (this.treeView.SelectedNode == null) return;

            var data = this.treeView.SelectedNode.Tag as double[][];
            if (data == null) return;

            this.DisplayCategoryPanel(this.treeView.SelectedNode.Text, data);
        }

        private void DisplayCategoryPanel(string label, double[][] data)
        {
            var simulationPanel = new PercentilPanel(label, data);
            simulationPanel.SelectedScenarioChanged += this.SimulationPanelSelectedScenarioChanged;
            simulationPanel.ZoomChanged += this.SimulationPanelZoomChanged;
            simulationPanel.Dock = DockStyle.Top;
            this.containerPanel.Controls.Add(simulationPanel);
        }

        private void SimulationPanelZoomChanged(object sender, EventArgs<Scale> args)
        {
            foreach (Control control in this.containerPanel.Controls)
            {
                PercentilPanel pp = control as PercentilPanel;
                if (pp == null || pp == sender) continue;

                pp.ApplyZoom(args.Value);
            }
        }

        private void SimulationPanelSelectedScenarioChanged(object sender, EventArgs<int> args)
        {
            this.selectedScenarioNumericUpDown.Value = args.Value;
        }

        public void Reload()
        {
            if(this.simulation != null)
            {
                this.LoadTreeViewFromSimulation();   
            }            

            int count = Scenario.GeneratedCount();
            this.scenarioCountLabel.Text = "of " + count;
            this.selectedScenarioNumericUpDown.Maximum = count;

            this.containerPanel.Controls.Clear();
        }

        private void LoadTreeViewFromSimulation()
        {
            this.Cursor = Cursors.WaitCursor;
            this.treeView.Nodes.Clear();

            this.CreateCategoryNodes(ResultSection.BookValue);
            this.CreateCategoryNodes(ResultSection.Income);

            this.CreateCustomNode(this.treeView.Nodes, Labels.MultiScenarioResult_LoadTreeViewFromSimulation_Net_Interest_Income, sd => sd.GetNetInterestIncome());
            this.CreateCustomNode(this.treeView.Nodes, Labels.MultiScenarioResult_LoadTreeViewFromSimulation_Gain_Losses_on_Financial_Instrument,
                                  sd => sd.GetGainLosesOnFinancialInstrument());
            this.CreateOIANodes();
            this.CreateCustomNode(this.treeView.Nodes, Labels.MultiScenarioResult_LoadTreeViewFromSimulation_Income_before_impairment_charge,
                                  sd => sd.GetInterestIncomeBeforeImpairment());
            this.CreateImpairmentNodes();
            this.CreateCustomNode(this.treeView.Nodes, Labels.MultiScenarioResult_LoadTreeViewFromSimulation_Impairment_Charge, sd => sd.GetImpairment());
            this.CreateCustomNode(this.treeView.Nodes, Labels.MultiScenarioResult_LoadTreeViewFromSimulation_Income_Pre_Tax, sd => sd.GetIncomePreTax());
            this.CreateCustomNode(this.treeView.Nodes, Labels.MultiScenarioResult_LoadTreeViewFromSimulation_Income_tax_benefit____expense_, sd => sd.GetTaxes());
            this.CreateCustomNode(this.treeView.Nodes, Labels.MultiScenarioResult_LoadTreeViewFromSimulation_Net_Income, sd => sd.GetNetIncome());
            this.CreateDividendNodes();
            this.CreateCustomNode(this.treeView.Nodes, Labels.MultiScenarioResult_LoadTreeViewFromSimulation_ROE, ROEFunc);
            this.CreateCustomNode(this.treeView.Nodes, Labels.MultiScenarioResult_LoadTreeViewFromSimulation_Periodic_ROA, PeriodicROAFunc);

            this.CreateCategoryNodes(ResultSection.Yield);

            foreach (TreeNode node in this.treeView.Nodes)
            {
                node.Expand();
            }

            this.Cursor = Cursors.Default;
        }

        private void CreateImpairmentNodes()
        {
            TreeNode node = this.treeView.Nodes.Add(Labels.MultiScenarioResult_CreateImpairmentNodes_Impairments);

            foreach (Category category in Category.FindAll())
            {
                if (category.IsVisibleCheckedModel == true)
                {
                    Category key = category;
                    this.CreateCustomNode(node.Nodes, category.Label, sd => sd.GetImpairementData()[key]);
                }
            }
        }

        private void CreateDividendNodes()
        {
            TreeNode node = this.treeView.Nodes.Add(Labels.MultiScenarioResult_CreateDividendNodes_Dividends);

            IDictionary<Dividend, double[][]> data = new Dictionary<Dividend, double[][]>();

            int scenarioIndex = 0;
            foreach (Scenario scenario in this.simulation.Scenarios)
            {
                ScenarioData scenarioData = this.simulation[scenario];

                decimal[] netIncome = scenarioData.GetNetIncome();
                IDictionary<Dividend, decimal[]> dividendAmounts = scenarioData.GetDividendAmounts();

                if (scenarioIndex == 0)
                {
                    foreach (Dividend dividend in dividendAmounts.Keys)
                    {
                        data.Add(dividend, new double[this.simulation.Count][]);
                    }
                }

                foreach (Dividend dividend in dividendAmounts.Keys)
                {
                    data[dividend][scenarioIndex] = new double[Constants.RESULT_MONTHS];
                    for (int i = 0; i < Constants.RESULT_MONTHS; i++)
                    {
                        decimal value = dividendAmounts[dividend][i] * netIncome[i];
                        if (dividend.IsValueAmount && value < dividend.Minimum.GetValueOrDefault())
                        {
                            value = dividend.Minimum.GetValueOrDefault();
                        }
                        data[dividend][scenarioIndex][i] = (double)value;
                    }
                }
                scenarioIndex++;
            }

            foreach (Dividend dividend in data.Keys)
            {
                TreeNode subNode = node.Nodes.Add(dividend.Name);
                subNode.Tag = data[dividend];
            }
        }

        private void CreateOIANodes()
        {
            TreeNode node = this.treeView.Nodes.Add(Labels.MultiScenarioResult_CreateOIANodes_Other_Item_Assumptions);

            IDictionary<string, double[][]> oiaData = new Dictionary<string, double[][]>();

            int scenarioIndex = 0;
            foreach (Scenario scenario in this.simulation.Scenarios)
            {
                ScenarioData scenarioData = this.simulation[scenario];

                IDictionary<string, decimal[]> oiaValues = scenarioData.GetOtherItemAssumptions();

                if (scenarioIndex == 0)
                {
                    foreach (string oiaName in oiaValues.Keys)
                    {
                        oiaData.Add(oiaName, new double[this.simulation.Count][]);
                    }
                }

                foreach (string oiaName in oiaValues.Keys)
                {
                    oiaData[oiaName][scenarioIndex] = new double[Constants.RESULT_MONTHS];
                    for (int i = 0; i < Constants.RESULT_MONTHS; i++)
                    {
                        oiaData[oiaName][scenarioIndex][i] = (double)oiaValues[oiaName][i];
                    }
                }
                scenarioIndex++;
            }

            foreach (string oiaName in oiaData.Keys)
            {
                TreeNode subNode = node.Nodes.Add(oiaName);
                subNode.Tag = oiaData[oiaName];
            }
        }

        private static decimal[] PeriodicROAFunc(ScenarioData scenarioData)
        {
            decimal[] netIncome = scenarioData.GetNetIncome();
            decimal[] totalAssets = scenarioData.GetTotalAssets();

            decimal[] result = new decimal[Constants.RESULT_MONTHS];
            for (int i = 0; i < netIncome.Length; i++)
            {
                if (totalAssets[i] != 0)
                {
                    result[i] = netIncome[i] / totalAssets[i] * 12;
                }
            }

            return result;
        }

        private static decimal[] ROEFunc(ScenarioData scenarioData)
        {
            decimal[] netIncome = scenarioData.GetNetIncome();
            decimal[] totalEquity = scenarioData.GetTotalEquity();

            decimal[] result = new decimal[Constants.RESULT_MONTHS];
            for (int i = 0; i < netIncome.Length; i++)
            {
                if (totalEquity[i] != 0)
                {
                    result[i] = netIncome[i] / totalEquity[i];
                }
            }

            return result;
        }

        private void CreateCustomNode(TreeNodeCollection nodes, string title, Func<ScenarioData, decimal[]> func)
        {
            TreeNode node = nodes.Add(title);

            if (this.simulation == null || this.simulation.Count == 0) return;

            double[][] data = new double[this.simulation.Count][];

            int scenarioIndex = 0;
            foreach (Scenario scenario in this.simulation.Scenarios)
            {
                ScenarioData scenarioData = this.simulation[scenario];
                decimal[] values = func(scenarioData);

                data[scenarioIndex] = new double[Constants.RESULT_MONTHS];
                for (int i = 0; i < Constants.RESULT_MONTHS; i++)
                {
                    data[scenarioIndex][i] = (double)values[i];
                }
                scenarioIndex++;
            }

            node.Tag = data;
        }

        private void CreateCategoryNodes(ResultSection section)
        {
            string label = GetSectionLabel(section, null);
            TreeNode node = this.treeView.Nodes.Add(label);

            Category root = Category.FindRoot();

            this.CreateSubCategoryNodes(root, node, section);
        }

        private void CreateSubCategoryNodes(Category parentCategory, TreeNode parentNode, ResultSection section)
        {
            foreach (Category cat in parentCategory.Children)
            {
                TreeNode node = parentNode.Nodes.Add(cat.Label);
                node.Tag = this.GetCategoryValuesBySection(cat, section);

                this.CreateSubCategoryNodes(cat, node, section);
            }
        }

        private double[][] GetCategoryValuesBySection(Category category, ResultSection section)
        {
            if (this.simulation == null || this.simulation.Count == 0) return null;

            double[][] result = new double[this.simulation.Count][];

            int scenarioIndex = 0;
            foreach (Scenario scenario in this.simulation.Scenarios)
            {
                ScenarioData scenarioData = this.simulation[scenario];
                decimal[] values = new decimal[Constants.RESULT_MONTHS];
                switch (section)
                {
                    case ResultSection.BookValue:
                        values = scenarioData.GetBookValue(category);
                        break;
                    case ResultSection.Income:
                        values = scenarioData.GetIncome(category);
                        break;
                    case ResultSection.Yield:
                        decimal[] income = scenarioData.GetIncome(category);
                        decimal[] bookValues = scenarioData.GetBookValue(category);
                        for (int i = 1; i < income.Length; i++)
                        {
                            if (bookValues[i] != 0) values[i] = income[i] / bookValues[i] * 100;
                        }
                        break;
                    default:
                        throw new ArgumentOutOfRangeException("section");
                }

                result[scenarioIndex] = new double[Constants.RESULT_MONTHS];
                for (int i = 0; i < Constants.RESULT_MONTHS; i++)
                {
                    result[scenarioIndex][i] = (double)values[i];
                }
                scenarioIndex++;
            }

            return result;
        }

        private static string GetSectionLabel(ResultSection section, Category category)
        {
            string type;
            switch (section)
            {
                case ResultSection.BookValue:
                    type = Labels.MultiScenarioResult_GetSectionLabel_Book_Values;
                    break;
                case ResultSection.Income:
                    if (category == null)
                    {
                        type = Labels.MultiScenarioResult_GetSectionLabel_Interest_Income___Expenses;
                    }
                    else if (category.BalanceType == BalanceType.Asset)
                    {
                        type = Labels.MultiScenarioResult_GetSectionLabel_Interest_Income;
                    }
                    else
                    {
                        type = Labels.MultiScenarioResult_GetSectionLabel_Interest_Expenses;
                    }
                    break;
                case ResultSection.Yield:
                    type = Labels.MultiScenarioResult_GetSectionLabel_Yield;
                    break;
                default:
                    throw new ArgumentOutOfRangeException("section");
            }
            return type;
        }

        public void Reload(Simulation sim)
        {
            if (this.simulation == sim) return;

            this.simulation = sim;

            this.Reload();
        }

        private void selectedScenarioNumericUpDown_ValueChanged(object sender, EventArgs e)
        {
            int index = (int)this.selectedScenarioNumericUpDown.Value;
            this.exportButton.Enabled = index > 0;
            this.SelectScenario(index);
        }

        private void SelectScenario(int index)
        {
            foreach (Control control in this.containerPanel.Controls)
            {
                PercentilPanel panel = control as PercentilPanel;
                if (panel == null) continue;

                panel.SelectCurve(index);
            }
        }

        private void exportButton_Click(object sender, EventArgs e)
        {
            if (this.simulation == null) return;
            int index = (int)this.selectedScenarioNumericUpDown.Value;
            if (index <= 0 || index > this.simulation.Scenarios.Count) return;

            ScenarioNamePrompt prompt = new ScenarioNamePrompt();
            if (prompt.ShowDialog(this.ParentForm) != DialogResult.OK)
            {
                return;
            }

            Scenario[] simulationScenarios = new Scenario[this.simulation.Scenarios.Count];
            this.simulation.Scenarios.CopyTo(simulationScenarios, 0);

            Scenario selectedScenario = simulationScenarios[index - 1];

            LoadForm.Instance.DoWork(new WorkParameters(
                                         bw => ExportScenario(selectedScenario, prompt.ScenarioName), null));
        }

        private static void ExportScenario(Scenario oldScenario, string scenarioName)
        {
            using (new SessionScope())
            {
                Scenario scenario = new Scenario();
                scenario.Name = scenarioName;
                scenario.Generated = false;
                scenario.Description = Labels.MultiScenarioResult_ExportScenario_export_of_a_generated_scenario;
                scenario.Selected = true;
                scenario.VariableValues = new List<VariableValue>(oldScenario.VariableValues.Count);

                foreach (VariableValue oldValue in oldScenario.VariableValues)
                {
                    VariableValue value = new VariableValue();
                    value.Scenario = scenario;
                    value.Variable = oldValue.Variable;

                    scenario.VariableValues.Add(value);
                    oldValue.Variable.VariableValues.Add(value);

                    if (oldValue.ValueIsArray())
                    {
                        string vectorName = CreateVector(oldValue.GetArrayValue(),
                                                         scenarioName + "_" + value.Variable.Name);
                        value.Value = vectorName;
                    }
                    else
                    {
                        value.Value = oldValue.Value;
                    }
                }

                scenario.CreateAndFlush();
            }
        }

        private static string CreateVector(decimal[] values, string name)
        {
            Vector vector = new Vector();
            vector.Frequency = VectorFrequency.Monthly;
            vector.Group = VectorGroup.Workspace;
            vector.ImportantMarketData = false;
            vector.Name = name;
            vector.VectorPoints = new List<VectorPoint>();

            DateTime valuationDate = Param.ValuationDate ?? DateTime.Now;

            for (int i = 0; i < values.Length; i++)
            {
                VectorPoint vectorPoint = new VectorPoint();
                vectorPoint.Vector = vector;
                vectorPoint.Date = valuationDate.AddMonths(i);
                vectorPoint.Value = values[i];

                vector.VectorPoints.Add(vectorPoint);
            }

            vector.CreateAndFlush();

            return name;
        }
    }
}