﻿using AssetModelingModule.Resources.Language;

namespace AssetModelingModule.Views.Generator
{
    partial class MultiScenarioParameters
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.scenariosToGenerateNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.scenariosComboBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.cancelButton = new System.Windows.Forms.Button();
            this.saveButton = new System.Windows.Forms.Button();
            this.generateButton = new System.Windows.Forms.Button();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.volatilitiesDataGridView = new System.Windows.Forms.DataGridView();
            this.correlationsDataGridView = new System.Windows.Forms.DataGridView();
            this.correlationsLabel = new System.Windows.Forms.Label();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scenariosToGenerateNumericUpDown)).BeginInit();
            this.panel2.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.volatilitiesDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.correlationsDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.scenariosToGenerateNumericUpDown);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.scenariosComboBox);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(964, 48);
            this.panel1.TabIndex = 0;
            // 
            // scenariosToGenerateNumericUpDown
            // 
            this.scenariosToGenerateNumericUpDown.Location = new System.Drawing.Point(464, 13);
            this.scenariosToGenerateNumericUpDown.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.scenariosToGenerateNumericUpDown.Name = "scenariosToGenerateNumericUpDown";
            this.scenariosToGenerateNumericUpDown.Size = new System.Drawing.Size(101, 20);
            this.scenariosToGenerateNumericUpDown.TabIndex = 3;
            this.scenariosToGenerateNumericUpDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.scenariosToGenerateNumericUpDown.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(343, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(114, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = Labels.MultiScenarioParameters_InitializeComponent_Scenarios_to_generate_;
            // 
            // scenariosComboBox
            // 
            this.scenariosComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.scenariosComboBox.FormattingEnabled = true;
            this.scenariosComboBox.Location = new System.Drawing.Point(115, 13);
            this.scenariosComboBox.Name = "scenariosComboBox";
            this.scenariosComboBox.Size = new System.Drawing.Size(171, 21);
            this.scenariosComboBox.TabIndex = 1;
            this.scenariosComboBox.SelectedIndexChanged += new System.EventHandler(this.scenariosComboBox_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = Labels.MultiScenarioParameters_InitializeComponent_Based_on_scenario;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.cancelButton);
            this.panel2.Controls.Add(this.saveButton);
            this.panel2.Controls.Add(this.generateButton);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 487);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(964, 48);
            this.panel2.TabIndex = 1;
            // 
            // cancelButton
            // 
            this.cancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Image = global::AssetModelingModule.Properties.Resources.GoRtlHS;
            this.cancelButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cancelButton.Location = new System.Drawing.Point(507, 13);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.cancelButton.Size = new System.Drawing.Size(120, 23);
            this.cancelButton.TabIndex = 2;
            this.cancelButton.Text = Labels.MultiScenarioParameters_InitializeComponent_Cancel;
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // saveButton
            // 
            this.saveButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.saveButton.Image = global::AssetModelingModule.Properties.Resources.saveHS;
            this.saveButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.saveButton.Location = new System.Drawing.Point(633, 13);
            this.saveButton.Name = "saveButton";
            this.saveButton.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.saveButton.Size = new System.Drawing.Size(120, 23);
            this.saveButton.TabIndex = 1;
            this.saveButton.Text = Labels.MultiScenarioParameters_InitializeComponent_Save;
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // generateButton
            // 
            this.generateButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.generateButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.generateButton.Image = global::AssetModelingModule.Properties.Resources.percentile;
            this.generateButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.generateButton.Location = new System.Drawing.Point(792, 13);
            this.generateButton.Name = "generateButton";
            this.generateButton.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.generateButton.Size = new System.Drawing.Size(160, 23);
            this.generateButton.TabIndex = 0;
            this.generateButton.Text = Labels.MultiScenarioParameters_InitializeComponent_Launch_Simulation;
            this.generateButton.UseVisualStyleBackColor = true;
            this.generateButton.Click += new System.EventHandler(this.GenerateButtonClick);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 48);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.volatilitiesDataGridView);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.correlationsDataGridView);
            this.splitContainer1.Panel2.Controls.Add(this.correlationsLabel);
            this.splitContainer1.Size = new System.Drawing.Size(964, 439);
            this.splitContainer1.SplitterDistance = 291;
            this.splitContainer1.TabIndex = 2;
            // 
            // volatilitiesDataGridView
            // 
            this.volatilitiesDataGridView.AllowUserToAddRows = false;
            this.volatilitiesDataGridView.AllowUserToDeleteRows = false;
            this.volatilitiesDataGridView.AllowUserToResizeRows = false;
            this.volatilitiesDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.volatilitiesDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.volatilitiesDataGridView.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.volatilitiesDataGridView.Location = new System.Drawing.Point(0, 0);
            this.volatilitiesDataGridView.Name = "volatilitiesDataGridView";
            this.volatilitiesDataGridView.RowHeadersVisible = false;
            this.volatilitiesDataGridView.Size = new System.Drawing.Size(291, 439);
            this.volatilitiesDataGridView.TabIndex = 0;
            // 
            // correlationsDataGridView
            // 
            this.correlationsDataGridView.AllowUserToAddRows = false;
            this.correlationsDataGridView.AllowUserToDeleteRows = false;
            this.correlationsDataGridView.AllowUserToResizeRows = false;
            this.correlationsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.correlationsDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.correlationsDataGridView.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.correlationsDataGridView.Location = new System.Drawing.Point(0, 24);
            this.correlationsDataGridView.Name = "correlationsDataGridView";
            this.correlationsDataGridView.RowHeadersVisible = false;
            this.correlationsDataGridView.Size = new System.Drawing.Size(669, 415);
            this.correlationsDataGridView.TabIndex = 1;
            // 
            // correlationsLabel
            // 
            this.correlationsLabel.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.correlationsLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.correlationsLabel.Location = new System.Drawing.Point(0, 0);
            this.correlationsLabel.Name = "correlationsLabel";
            this.correlationsLabel.Padding = new System.Windows.Forms.Padding(5);
            this.correlationsLabel.Size = new System.Drawing.Size(669, 24);
            this.correlationsLabel.TabIndex = 0;
            this.correlationsLabel.Text = Labels.MultiScenarioParameters_InitializeComponent_Correlations;
            this.correlationsLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Volatility";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle1.Format = "N5";
            this.dataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewTextBoxColumn1.FillWeight = 70F;
            this.dataGridViewTextBoxColumn1.HeaderText = Labels.MultiScenarioParameters_InitializeComponent_Volatility_factor;
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn2.DataPropertyName = "VariableName";
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.dataGridViewTextBoxColumn2.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridViewTextBoxColumn2.HeaderText = Labels.MultiScenarioParameters_InitializeComponent_Variable;
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // MultiScenarioParameters
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(964, 535);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "MultiScenarioParameters";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = Labels.MultiScenarioParameters_InitializeComponent_Scenario_Generator_Parameters;
            this.Load += new System.EventHandler(this.ScenarioGenerator_Load);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ScenarioGenerator_FormClosed);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scenariosToGenerateNumericUpDown)).EndInit();
            this.panel2.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.volatilitiesDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.correlationsDataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.NumericUpDown scenariosToGenerateNumericUpDown;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox scenariosComboBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView volatilitiesDataGridView;
        private System.Windows.Forms.DataGridView correlationsDataGridView;
        private System.Windows.Forms.Label correlationsLabel;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.Button generateButton;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
    }
}