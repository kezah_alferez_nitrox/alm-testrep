using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using ALMSCommon;
using ZedGraph;

namespace AssetModelingModule.Views.Generator
{
    public delegate void SelectedScenarioHandler(object sender, EventArgs<int> args);
    public delegate void ZoomHandler(object sender, EventArgs<Scale> args);

    public partial class PercentilPanel : UserControl
    {
        private const int BottomPadding = 5;

        private readonly double[][] data;

        private static readonly int[] Percents = new[] { 1, 5, 10, 33, 50, 67, 90, 95, 100 };
        private static readonly string[] PercentLabels = new[] { "1", "5", "10", "33", "MEAN", "67", "90", "95", "99" };
        private static readonly int FirstYear = DateTime.Today.Year;
        private int[] xValues;

        private readonly Color[] _colors = new[]
                                               {
                                                   Color.White,
                                                   Color.FromArgb(152, 51, 0), Color.FromArgb(255, 153, 0),
                                                   Color.FromArgb(255, 204, 153),
                                                   Color.FromArgb(255, 255, 204), Color.FromArgb(255, 255, 204),
                                                   Color.FromArgb(204, 255, 204), Color.FromArgb(153, 204, 0),
                                                   Color.FromArgb(0, 100, 17),
                                                   Color.FromArgb(0, 60, 10)
                                               };

        private readonly double[][] percentils = new double[Percents.Length][];
        private readonly double[][] relativePercentils = new double[Percents.Length][];

        private LineItem selectedCurve = null;

        private int selectedIndex;
        private int count;
        private bool resizeing;

        public event SelectedScenarioHandler SelectedScenarioChanged = delegate { };
        public event ZoomHandler ZoomChanged = delegate { };

        public PercentilPanel(string title, double[][] data)
        {
            this.data = data;
            this.InitializeComponent();

            this.titleLabel.Text = title;

            this.InitPercentils();
            this.InitGrid();
            this.UpdatePercentilesGraph();
        }

        private void InitPercentils()
        {
            var sortedValues = new List<double[]>();

            this.count = data[0].Length;
            this.xValues = new int[this.count];
            for (int i = 0; i < this.count; i++)
            {
                this.xValues[i] = i;
            }

            for (int i = 0; i < this.count; i++)
            {
                double[] column = new double[data.Length];
                for (int j = 0; j < data.Length; j++)
                {
                    column[j] = data[j][i];
                }
                Array.Sort(column);
                sortedValues.Add(column);
            }

            for (int j = 0; j < Percents.Length; j++)
            {
                var percentile = new List<double>();
                for (int i = 0; i < this.count; i++)
                {
                    percentile.Add(Percentile.Calculate(sortedValues[i], Percents[j]));
                }
                this.percentils[j] = percentile.ToArray();
            }
        }

        private void InitRelativePercentils()
        {
            for (int i = 0; i < this.percentils.Length; i++)
            {
                this.relativePercentils[i] = new double[this.percentils[i].Length];
                for (int j = 0; j < this.percentils[i].Length; j++)
                {
                    this.relativePercentils[i][j] = i == 0
                                                         ? this.percentils[i][j]
                                                         : this.percentils[i][j] - this.percentils[i - 1][j];
                }
            }
        }

        private void InitGrid()
        {
            this.CreateGridColumns();

            for (int j = 0; j < Percents.Length; j++)
            {
                var line = new List<object> { PercentLabels[j] };
                for (int i = 0; i < this.percentils[j].Length; i++)
                    line.Add(this.percentils[j][i]);
                this.percentileGrid.Rows.Add(line.ToArray());
            }
        }

        private void CreateGridColumns()
        {
            DataGridViewColumn[] columns = new DataGridViewColumn[this.count + 1];

            DataGridViewCellStyle numericStyle = new DataGridViewCellStyle();
            numericStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            numericStyle.Format = "N3";

            columns[0] = new DataGridViewTextBoxColumn();
            columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            columns[0].Frozen = true;
            columns[0].HeaderText = "";
            columns[0].Name = "labelColumn";
            columns[0].ReadOnly = true;
            columns[0].Width = 40;
            
            for (int i = 0; i < this.count; i++)
            {
                columns[i + 1] = new DataGridViewTextBoxColumn();
                columns[i + 1].DefaultCellStyle = numericStyle;
                columns[i + 1].HeaderText = IndexToLabel(this.xValues[i]);
                columns[i + 1].Name = "year" + i + "Column";
                columns[i + 1].ReadOnly = true;
                columns[i + 1].Width = 80;
            }

            ((ISupportInitialize)this.percentileGrid).BeginInit();
            this.percentileGrid.Columns.Clear();
            this.percentileGrid.Columns.AddRange(columns);
            ((ISupportInitialize)this.percentileGrid).EndInit();
        }

        private void BCloseClick(object sender, EventArgs e)
        {
            this.Parent.Controls.Remove(this);
        }

        private void UpdatePercentilesGraph()
        {
            this.zedGraphControl.IsAntiAlias = true;

            GraphPane graphPane = this.zedGraphControl.GraphPane;

            graphPane.Title.IsVisible = false;
            graphPane.Legend.IsVisible = false;

            graphPane.XAxis.Title.IsVisible = false;
            graphPane.XAxis.Type = AxisType.Text;
            graphPane.XAxis.Scale.TextLabels = this.xValues.Select(x => IndexToLabel(x)).ToArray();

            graphPane.YAxis.Title.Text = "";
            graphPane.YAxis.Title.IsVisible = true;

            double maxY;
            double minY;
            GetArrayMinMax(this.percentils, out minY, out maxY);
            graphPane.YAxis.Scale.Min = minY;
            graphPane.YAxis.Scale.Max = maxY;

            graphPane.LineType = LineType.Stack;

            graphPane.CurveList.Clear();

            this.InitRelativePercentils();

            double[] selectedScenarioYValues = this.DrawSelectedScenarioCurve();

            if (selectedScenarioYValues != null)
            {
                for (int i = 0; i < selectedScenarioYValues.Length; i++)
                {
                    this.relativePercentils[0][i] -= selectedScenarioYValues[i];
                }
            }

            //draw percentils
            for (int i = 0; i < this.relativePercentils.Length; i++)
            {
                LineItem percentilCurve = graphPane.AddCurve(
                    PercentLabels[i],
                    null,
                    this.relativePercentils[i],
                    this._colors[i]);

                if (i != 0)
                {
                    percentilCurve.Line.Fill = new Fill(this._colors[i]);
                }

                if (i == 0)
                {
                    percentilCurve.Line.Width = 3;
                    percentilCurve.Line.Color = Color.Black;
                }
                else if (i == this.relativePercentils.Length - 1)
                {
                    percentilCurve.Line.Width = 3;
                    percentilCurve.Line.Color = Color.Black;
                }
                else if (i == this.relativePercentils.Length / 2)
                {
                    percentilCurve.Line.Width = 3;
                    percentilCurve.Line.Color = Color.DarkSlateBlue;
                }
                else
                {
                    percentilCurve.Line.Width = 0;
                }

                percentilCurve.Symbol.IsVisible = false;
            }

            this.zedGraphControl.AxisChange();
            this.zedGraphControl.Invalidate();
        }

        private static string IndexToLabel(int x)
        {
            if(x==0)
            {
                Console.WriteLine("coco");
            }
            return x == 0 ? "OB" : string.Format("{0:00}/{1:0000}", ((x - 1) % 12 + 1), (FirstYear + x / 12));
        }

        private static void GetArrayMinMax(double[][] array, out double minY, out double maxY)
        {
            minY = double.MaxValue;
            maxY = double.MinValue;
            foreach (double[] t in array)
            {
                foreach (double percentil in t)
                {
                    minY = Math.Min(minY, percentil);
                    maxY = Math.Max(maxY, percentil);
                }
            }
        }

        public void SelectCurve(int index)
        {
            this.selectedIndex = index;

            this.UpdatePercentilesGraph();
        }

        private double[] DrawSelectedScenarioCurve()
        {
            GraphPane graphPane = this.zedGraphControl.GraphPane;

            if (this.selectedIndex != 0 && this.selectedIndex <= this.data.Length)
            {
                double[] yValues = data[selectedIndex - 1];

                this.selectedCurve = graphPane.AddCurve("", null, yValues, Color.Red);
                this.selectedCurve.Line.Width = 3;
                this.selectedCurve.Symbol.IsVisible = false;

                return yValues;
            }

            return null;
        }

        protected override void OnMouseDown(MouseEventArgs e)
        {
            base.OnMouseDown(e);

            if (this.InsideResizeZone(e.Location))
            {
                this.resizeing = true;
                this.Capture = true;
            }
        }

        protected override void OnMouseLeave(EventArgs e)
        {
            base.OnMouseLeave(e);

            this.Cursor = Cursors.Default;
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);

            if (this.resizeing)
            {
                this.Height = e.Location.Y;
            }
            else
            {
                this.Cursor = this.InsideResizeZone(e.Location) ? Cursors.SizeNS : Cursors.Default;
            }
        }

        protected override void OnMouseUp(MouseEventArgs e)
        {
            base.OnMouseUp(e);

            if (this.resizeing)
            {
                this.resizeing = false;
                this.Capture = false;
            }
        }

        private bool InsideResizeZone(Point location)
        {
            return location.Y >= this.Height - BottomPadding && location.Y <= this.Height;
        }

        private bool zedGraphControl_MouseDownEvent(ZedGraphControl sender, MouseEventArgs e)
        {
            if (ModifierKeys != Keys.Alt) return false;

            PointF mousePt = new PointF(e.X, e.Y);
            GraphPane pane = this.zedGraphControl.GraphPane;

            double x;
            double y;
            pane.ReverseTransform(mousePt, out x, out y);

            int clickIndex = (int)Math.Round(x);
            double clickValue = y;

            var deltas = this.data.Select((v, i) => new { Index = i, Delta = Math.Abs(v[clickIndex] - clickValue) });
            double min = deltas.Min(d => d.Delta);

            int scenarioIndex = deltas.First(d => d.Delta == min).Index;
            this.SelectedScenarioChanged(this, new EventArgs<int>(scenarioIndex + 1));

            return true;
        }

        public void ApplyZoom(Scale scale)
        {
            zedGraphControl.GraphPane.XAxis.Scale.Min = scale.Min;
            zedGraphControl.GraphPane.XAxis.Scale.Max = scale.Max;

            this.zedGraphControl.AxisChange();
            this.zedGraphControl.Invalidate();
        }

        private void zedGraphControl_ZoomEvent(ZedGraphControl sender, ZoomState oldState, ZoomState newState)
        {                        
            this.ZoomChanged(this, new EventArgs<Scale>(zedGraphControl.GraphPane.XAxis.Scale));
        }
    }
}