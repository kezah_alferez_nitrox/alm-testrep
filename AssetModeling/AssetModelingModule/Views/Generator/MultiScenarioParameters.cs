﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using ALMSCommon.Forms;
using AssetModelingModule.BusinessLogic;
using AssetModelingModule.Calculus;
using AssetModelingModule.Controls;
using AssetModelingModule.Domain;
using AssetModelingModule.Generator;
using AssetModelingModule.Resources.Language;
using Castle.ActiveRecord;

namespace AssetModelingModule.Views.Generator
{
    public partial class MultiScenarioParameters : Form
    {
        private DataGridViewCheckBoxColumn generatedColumn;
        private DataGridViewTextBoxColumn volatitlityFactorColumn;
        private AutoCompleteColumn meanColumn;
        private DataGridViewTextBoxColumn variableColumn;

        private SessionScope scope;
        private double[,] variableValues;
        private BindingList<VolatilityConfig> volatilities;
        private readonly IList<int> ignoredVariableindexes = new List<int>();
        private double[,] correlationMatrix;
        private Scenario selectedScenario;
        private Variable[] variables;
        private Simulation simulation;
        private DateTime? valuationDate;
        private bool loaded = false;

        public Simulation Simulation
        {
            get { return this.simulation; }
        }

        public MultiScenarioParameters()
        {
            this.InitializeComponent();

            this.correlationsDataGridView.ColumnHeadersHeightSizeMode =
                this.volatilitiesDataGridView.ColumnHeadersHeightSizeMode =
                DataGridViewColumnHeadersHeightSizeMode.DisableResizing;

            this.correlationsDataGridView.RowHeightChanged += this.CorrelationsDataGridViewRowHeightChanged;
            this.volatilitiesDataGridView.RowHeightChanged += this.VolatilitiesDataGridViewRowHeightChanged;

            this.volatilitiesDataGridView.ColumnHeadersHeight += this.correlationsLabel.Height;

            this.correlationsDataGridView.CellValidating += CorrelationsDataGridViewCellValidating;
            this.volatilitiesDataGridView.CellValidating += VolatilitiesDataGridViewCellValidating;

            this.volatilitiesDataGridView.CellMouseUp += this.VolatilitiesDataGridViewCellMouseUp;
            this.volatilitiesDataGridView.CellValueChanged += this.VolatilitiesDataGridViewCellValueChanged;
        }

        private void VolatilitiesDataGridViewCellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex == 0)
            {
                this.volatilitiesDataGridView.EndEdit();
            }
        }

        private static void VolatilitiesDataGridViewCellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (e.ColumnIndex == 1)
            {
                double tmp;
                if (!double.TryParse((string)e.FormattedValue, out tmp))
                {
                    e.Cancel = true;
                }
            }
        }

        private static void CorrelationsDataGridViewCellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            double tmp;
            if (!double.TryParse((string)e.FormattedValue, out tmp))
            {
                e.Cancel = true;
            }
        }

        private void VolatilitiesDataGridViewRowHeightChanged(object sender, DataGridViewRowEventArgs e)
        {
            this.correlationsDataGridView.Rows[e.Row.Index].Height = e.Row.Height;
        }

        private void CorrelationsDataGridViewRowHeightChanged(object sender, DataGridViewRowEventArgs e)
        {
            this.volatilitiesDataGridView.Rows[e.Row.Index].Height = e.Row.Height;
        }

        private void VolatilitiesDataGridViewCellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0) return;

            if (e.ColumnIndex == 0)
            {
                this.ignoredVariableindexes.Clear();
                for (int i = 0; i < this.volatilitiesDataGridView.Rows.Count; i++)
                {
                    bool selected = (bool)this.volatilitiesDataGridView[0, i].Value;
                    if (selected)
                    {
                        EnableCell(this.volatilitiesDataGridView[1, i], true);
                        EnableCell(this.volatilitiesDataGridView[2, i], true);
                    }
                    else
                    {
                        this.ignoredVariableindexes.Add(i);
                        EnableCell(this.volatilitiesDataGridView[1, i], false);
                        EnableCell(this.volatilitiesDataGridView[2, i], false);
                    }
                }

                for (int i = 0; i < this.variables.Length - 1; i++)
                {
                    for (int j = i + 1; j < this.variables.Length; j++)
                    {
                        EnableCell(this.correlationsDataGridView[j, i], true);
                    }
                }

                foreach (var index in this.ignoredVariableindexes)
                {
                    for (int i = 0; i < this.variables.Length; i++)
                    {
                        EnableCell(this.correlationsDataGridView[index, i], false);
                        EnableCell(this.correlationsDataGridView[i, index], false);
                    }
                }
            }
        }

        private void ScenarioGenerator_Load(object sender, EventArgs e)
        {
            this.scope = new SessionScope();

            if (loaded) return;
            loaded = true;

            this.CreateVolatilityColumns();

            List<Scenario> scenarios = new List<Scenario>(Scenario.FindNonGenerated());

            this.scenariosComboBox.DisplayMember = "Name";
            this.scenariosComboBox.ValueMember = "Self";
            this.scenariosComboBox.DataSource = scenarios;

            this.valuationDate = Param.ValuationDate;
        }

        private void CreateVolatilityColumns()
        {
            this.volatilitiesDataGridView.AutoGenerateColumns = false;

            this.generatedColumn = new DataGridViewCheckBoxColumn();
            this.volatitlityFactorColumn = new DataGridViewTextBoxColumn();
            this.meanColumn = new AutoCompleteColumn();
            this.variableColumn = new DataGridViewTextBoxColumn();

            // 
            // generatedColumn
            // 
            this.generatedColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            this.generatedColumn.DataPropertyName = "Selected";
            this.generatedColumn.FillWeight = 70F;
            this.generatedColumn.HeaderText = Labels.MultiScenarioParameters_CreateVolatilityColumns_Generated_within_the_simulation;
            this.generatedColumn.Name = "generatedColumn";
            // 
            // volatitlityFactorColumn
            // 
            this.volatitlityFactorColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            this.volatitlityFactorColumn.DataPropertyName = "Volatility";
            DataGridViewCellStyle dataGridViewCellStyle1 = new DataGridViewCellStyle();
            dataGridViewCellStyle1.Alignment = DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle1.Format = "N3";
            this.volatitlityFactorColumn.DefaultCellStyle = dataGridViewCellStyle1;
            this.volatitlityFactorColumn.FillWeight = 70F;
            this.volatitlityFactorColumn.HeaderText = Labels.MultiScenarioParameters_CreateVolatilityColumns_Volatility_factor;
            this.volatitlityFactorColumn.Name = "volatitlityFactorColumn";
            this.volatitlityFactorColumn.SortMode = DataGridViewColumnSortMode.NotSortable;
            // 
            // meanColumn
            // 
            this.meanColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            this.meanColumn.DataPropertyName = "Mean";
            DataGridViewCellStyle dataGridViewCellStyle2 = new DataGridViewCellStyle();
            dataGridViewCellStyle2.Alignment = DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle2.Format = "N3";
            this.meanColumn.DefaultCellStyle = dataGridViewCellStyle2;
            this.meanColumn.FillWeight = 70F;
            this.meanColumn.HeaderText = Labels.MultiScenarioParameters_CreateVolatilityColumns_Mean;
            this.meanColumn.Name = "meanColumn";
            this.meanColumn.SortMode = DataGridViewColumnSortMode.NotSortable;
            this.meanColumn.ValueList = DomainTools.GetAllVectorNames();
            // 
            // variableColumn
            // 
            this.variableColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            this.variableColumn.DataPropertyName = "VariableName";
            DataGridViewCellStyle dataGridViewCellStyle3 = new DataGridViewCellStyle();
            dataGridViewCellStyle3.BackColor = SystemColors.ControlLight;
            dataGridViewCellStyle3.Font = new Font("Microsoft Sans Serif", 8.25F, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.variableColumn.DefaultCellStyle = dataGridViewCellStyle3;
            this.variableColumn.HeaderText = Labels.MultiScenarioParameters_CreateVolatilityColumns_Variable;
            this.variableColumn.Name = "variableColumn";
            this.variableColumn.ReadOnly = true;
            this.variableColumn.SortMode = DataGridViewColumnSortMode.NotSortable;

            this.volatilitiesDataGridView.Columns.AddRange(new DataGridViewColumn[] {
            this.generatedColumn,
            this.volatitlityFactorColumn,
            this.meanColumn,
            this.variableColumn});
        }

        private void ScenarioGenerator_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.scope.Flush();
            this.scope.Dispose();
        }

        private void scenariosComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.correlationsDataGridView.CellValueChanged -= this.CorrelationsDataGridViewCellValueChanged;

            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.ComputeGrids();
            }
            finally
            {
                this.Cursor = Cursors.Default;
                this.correlationsDataGridView.CellValueChanged += this.CorrelationsDataGridViewCellValueChanged;
            }
        }

        private void ComputeGrids()
        {
            this.volatilitiesDataGridView.Rows.Clear();
            this.correlationsDataGridView.Rows.Clear();

            this.selectedScenario = this.scenariosComboBox.SelectedItem as Scenario;
            if (this.selectedScenario == null) return;

            this.selectedScenario = Scenario.Find(this.selectedScenario.Id);
            ScenarioVariablesEvaluator evaluator = new ScenarioVariablesEvaluator(this.selectedScenario);
            evaluator.Run();

            if (evaluator.ValidationErrors.Count > 0)
            {
                MessageBox.Show(Labels.MultiScenarioParameters_ComputeGrids_There_are_errors_in_the_variable_formulas);
                return;
            }

            IDictionary<Variable, double[]> variableDictionary = evaluator.VariableValues;

            this.variables = new Variable[variableDictionary.Count];
            variableDictionary.Keys.CopyTo(this.variables, 0);

            this.variableValues = new double[variableDictionary.Count, Constants.RESULT_MONTHS];
            for (int i = 0; i < variableDictionary.Count; i++)
            {
                double[] values = variableDictionary[this.variables[i]];
                for (int j = 0; j < values.Length; j++)
                {
                    this.variableValues[i, j] = values[j];
                }
            }

            this.FillVolatilitiesGrid();
            this.FillCorrelationGrid();

            this.CheckPositiveDefinite();
        }

        private void FillVolatilitiesGrid()
        {
            this.volatilities = new BindingList<VolatilityConfig>();
            for (int i = 0; i < this.variables.Length; i++)
            {
                double[] v = MatrixHelper.GetRow(this.variableValues, i);
                VolatilityConfig config = new VolatilityConfig(true, Statistics.StandardDeviation(v),
                                                               v.Average().ToString("N3"),
                                                               this.variables[i].Name);
                this.volatilities.Add(config);
            }

            this.volatilitiesDataGridView.DataSource = this.volatilities;
        }

        private void FillCorrelationGrid()
        {
            this.correlationsDataGridView.Columns.Clear();
            this.correlationsDataGridView.ColumnHeadersDefaultCellStyle.Font =
                new Font(this.correlationsDataGridView.Font, FontStyle.Bold);

            foreach (Variable variable in this.variables)
            {
                DataGridViewTextBoxColumn column = new DataGridViewTextBoxColumn();
                column.HeaderText = variable.Name;
                column.DefaultCellStyle.Format = "N3";
                column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                column.Width = 60;
                column.SortMode = DataGridViewColumnSortMode.NotSortable;
                this.correlationsDataGridView.Columns.Add(column);
            }

            this.correlationMatrix = MatrixHelper.CorrelationMatrix(this.variableValues);

            for (int i = 0; i < this.correlationMatrix.GetLength(0); i++)
            {
                this.correlationMatrix[i, i] = 1;
            }

            this.CorrelationMatrixToCorrelationsDataGridView();
        }

        private void CorrelationMatrixToCorrelationsDataGridView()
        {
            this.correlationsDataGridView.Rows.Clear();
            this.correlationsDataGridView.Rows.Add(this.correlationMatrix.GetLength(0));
            for (int i = 0; i < this.correlationMatrix.GetLength(0); i++)
            {
                for (int j = 0; j < this.correlationMatrix.GetLength(1); j++)
                {
                    this.correlationsDataGridView[i, j].Value = this.correlationMatrix[i, j];

                    if (i <= j)
                    {
                        this.correlationsDataGridView[i, j].ReadOnly = true;
                        this.correlationsDataGridView[i, j].Style.BackColor = SystemColors.ControlLight;
                    }
                }
            }
        }

        public class VolatilityConfig
        {
            public bool Selected { get; set; }
            public double Volatility { get; set; }
            public string Mean { get; set; }
            public string VariableName { get; set; }

            public VolatilityConfig(bool selected, double volatility, string mean, string variableName)
            {
                this.Selected = selected;
                this.Volatility = volatility;
                this.Mean = mean;
                this.VariableName = variableName;
            }
        }

        private void CorrelationsDataGridViewCellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == e.RowIndex) return;
            this.correlationsDataGridView[e.RowIndex, e.ColumnIndex].Value =
                this.correlationsDataGridView[e.ColumnIndex, e.RowIndex].Value;

            this.CheckPositiveDefinite();
        }

        private void CheckPositiveDefinite()
        {
            this.CorrelationsDataGridViewToCorrelationMatrix();

            double[,] cholesky = MatrixHelper.Cholesky(this.correlationMatrix);

            bool containsNaN = MatrixHelper.ContainsNaN(cholesky);
            bool positiveDefinite = MatrixHelper.IsPositiveDefinite(this.correlationMatrix);

            // Debug.Assert((!containsNaN) == positiveDefinite);

            if (!containsNaN /*&& positiveDefinite*/)
            {
                this.correlationsLabel.Text = Labels.MultiScenarioParameters_CheckPositiveDefinite_Correlations;
                this.correlationsLabel.ForeColor = Color.Black;
            }
            else
            {
                this.correlationsLabel.Text = Labels.MultiScenarioParameters_CheckPositiveDefinite_Invalid_Correlations__not_a_positive_definite_matrix;
                this.correlationsLabel.ForeColor = Color.Red;
            }
        }

        private void CorrelationsDataGridViewToCorrelationMatrix()
        {
            for (int i = 0; i < this.correlationsDataGridView.Rows.Count; i++)
            {
                for (int j = 0; j < this.correlationsDataGridView.Columns.Count; j++)
                {
                    this.correlationMatrix[i, j] = Convert.ToDouble(this.correlationsDataGridView[j, i].Value);
                }
            }
        }

        private void GenerateButtonClick(object sender, EventArgs e)
        {
            if (this.variables == null || this.variables.Length == 0) return;

            LoadForm.Instance.DoWork(new WorkParameters(this.GenerateScenariosAndRunSimulation,
                                                        this.AfterGenerateScenarios));
        }

        private void AfterGenerateScenarios()
        {
            if (this.Simulation.IsSuccesfull)
            {
                this.Close();
            }
            else
            {
                MessageBox.Show(Labels.MultiScenarioParameters_AfterGenerateScenarios_Some_errors_where_encountered_);
            }
        }

        private void GenerateScenariosAndRunSimulation(BackgroundWorker backgroundWorker)
        {
            using (new SessionScope())
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo(Constants.CULTURE_NAME);

                double[][] means = this.GetMeans();
                if (means == null) return;

                VariableValue.DeleteGenerated();
                SessionScope.Current.Flush();
                Scenario.DeleteGenerated();
                SessionScope.Current.Flush();

                LoadForm.Instance.SetTitle(Labels.MultiScenarioParameters_GenerateScenariosAndRunSimulation_Generating_Scenarios____);

                double[] stdevs = this.GetStandardDeviations();
                double[,] corrMatrix = this.GetCorrelationMatrix();
                int count = (int)this.scenariosToGenerateNumericUpDown.Value;
                MultivariateRandomGenerator generator = new MultivariateRandomGenerator();
                for (int i = 0; i < count; i++)
                {
                    Scenario scenario = new Scenario();
                    scenario.Name = string.Format("G{0}", i);
                    scenario.Generated = true;

                    generator.GenerateMultivariateNormals(Constants.RESULT_MONTHS, means, stdevs, corrMatrix);

                    int j2 = 0;
                    for (int j = 0; j < this.variables.Length; j++)
                    {
                        Variable variable = this.variables[j];

                        VariableValue value = new VariableValue();
                        value.Scenario = scenario;
                        value.Variable = variable;
                        value.Generated = true;

                        if (this.ignoredVariableindexes.Contains(j))
                        {
                            VariableValue baseVariableValue =
                                this.selectedScenario.VariableValues.Single(vv => vv.Variable.Id == variable.Id);
                            value.Value = baseVariableValue.Value;
                        }
                        else
                        {
                            double[] row = MatrixHelper.GetRow(generator.Result, j2++);
                            value.Value = ArrayToString(row);
                        }

                        variable.VariableValues.Add(value);
                        scenario.VariableValues.Add(value);
                    }
                    scenario.Save();

                    backgroundWorker.ReportProgress(100 * (i + 1) / count);
                }
            }

            LoadForm.Instance.SetTitle(Labels.MultiScenarioParameters_GenerateScenariosAndRunSimulation_Running_Simulation____);

            try
            {
                this.simulation = new Simulation(false);
                this.simulation.Run(backgroundWorker);
            }
            catch (OverflowException)
            {
                MessageBox.Show(
                    Labels.MultiScenarioParameters_GenerateScenariosAndRunSimulation_, Labels.MultiScenarioParameters_GenerateScenariosAndRunSimulation_Error,
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private static string ArrayToString(double[] doubles)
        {
            return VariableValue.ArrayPrefix + doubles.Aggregate("", (a, b) => a + ";" + b);
        }

        private double[][] GetMeans()
        {
            int count = this.variables.Length - this.ignoredVariableindexes.Count;
            double[][] result = new double[count][];
            int j = 0;
            for (int i = 0; i < this.volatilities.Count; i++)
            {
                if (this.ignoredVariableindexes.Contains(i)) continue;
                double[] mean = this.GetMean(this.volatilities[i].Mean);
                if (mean == null)
                {
                    return null;
                }
                result[j++] = mean;
            }
            return result;
        }

        private double[] GetMean(string stringMean)
        {
            double[] result = new double[Constants.RESULT_MONTHS];
            double mean;
            if (double.TryParse(stringMean, out mean))
            {
                for (int i = 0; i < result.Length; i++)
                {
                    result[i] = mean;
                }
            }
            else
            {
                Vector vector = Vector.FindByName(stringMean);
                if (vector == null)
                {
                    MessageBox.Show(Labels.MultiScenarioParameters_GetMean_Vector_not_found___ + stringMean);
                    return null;
                }

                IList<VectorPoint> points = vector.VectorPoints;
                if (points == null || points.Count == 0) return result;
                decimal[] decimals = Util.GetVectorValues(this.valuationDate ?? points[0].Date, points);
                for (int i = 0; i < decimals.Length; i++)
                {
                    result[i] = (double)decimals[i];
                }
            }

            return result;
        }

        private double[] GetStandardDeviations()
        {
            int count = this.variables.Length - this.ignoredVariableindexes.Count;
            double[] result = new double[count];
            int j = 0;
            for (int i = 0; i < this.volatilities.Count; i++)
            {
                if (this.ignoredVariableindexes.Contains(i)) continue;
                result[j++] = this.volatilities[i].Volatility;
            }
            return result;
        }

        private double[,] GetCorrelationMatrix()
        {
            int count = this.variables.Length - this.ignoredVariableindexes.Count;
            double[,] result = new double[count, count];

            int i2 = 0;
            for (int i = 0; i < this.correlationMatrix.GetLength(0); i++)
            {
                if (this.ignoredVariableindexes.Contains(i)) continue;
                int j2 = 0;
                for (int j = 0; j < this.correlationMatrix.GetLength(1); j++)
                {
                    if (this.ignoredVariableindexes.Contains(j)) continue;
                    result[i2, j2] = this.correlationMatrix[i, j];
                    j2++;
                }
                i2++;
            }

            return result;
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private static void EnableCell(DataGridViewCell cell, bool enabled)
        {
            cell.ReadOnly = !enabled;
            cell.Style.BackColor = enabled ? Color.White : SystemColors.ControlLight;
        }
    }
}