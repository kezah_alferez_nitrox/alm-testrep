﻿using System;
using System.Windows.Forms;
using AssetModelingModule.Domain;
using Castle.ActiveRecord;

namespace AssetModelingModule.Views.Generator
{
    public partial class ScenarioNamePrompt : Form
    {
        private SessionScope sessionScope;

        public ScenarioNamePrompt()
        {
            InitializeComponent();
        }

        public string ScenarioName
        {
            get { return scenarioTextBox.Text; }
        }

        private void ScenarioTextBoxTextChanged(object sender, EventArgs e)
        {
            if(scenarioTextBox.Text.Length == 0)
            {
                okButton.Enabled = false;
                return;
            }
            Scenario findByName = Scenario.FindByName(this.scenarioTextBox.Text);

            okButton.Enabled = findByName == null;
        }

        private void ScenarioNamePrompt_Load(object sender, EventArgs e)
        {
            this.sessionScope = new SessionScope();
        }

        private void ScenarioNamePrompt_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.sessionScope.Dispose(true);
            this.sessionScope = null;
        }
    }
}