using System;
using System.Collections.Generic;
using System.Windows.Forms;
using AssetModelingModule.Controls;
using AssetModelingModule.Domain;
using AssetModelingModule.Resources.Language;
using AssetModelingModule.Views.BalanceSheet.Balance;
using Castle.ActiveRecord;

namespace AssetModelingModule.Views.Options.FxRates
{
    public partial class FxRatesEditor : Form
    {
        private const string NameColumnName = "Name";
        private const string SymbolColumnName = "Symbol";
        private const string ValueColumnName = "Value";

        private Currency valuationCurrency;

        public FxRatesEditor()
        {
            InitializeComponent();

            currenciesGrid.AutoGenerateColumns = false;
            currenciesGrid.EditMode = DataGridViewEditMode.EditOnEnter;
        }

        private void InitColumns()
        {
            Variable[] allVariables = ActiveRecordBase<Variable>.FindAll();
            List<string> allVariableNames = new List<string>();
            foreach (Variable variable in allVariables)
            {
                allVariableNames.Add(variable.Name);
            }

            DataGridViewTextBoxColumn nameColumn = new DataGridViewTextBoxColumn();
            nameColumn.Name = NameColumnName;
            nameColumn.HeaderText = Labels.FxRatesEditor_InitColumns_Name__used_for_import_;
            nameColumn.Width = 140;

            DataGridViewTextBoxColumn symbolColumn = new DataGridViewTextBoxColumn();
            symbolColumn.Name = SymbolColumnName;
            symbolColumn.HeaderText = SymbolColumnName;
            symbolColumn.Width = 70;

            AutoCompleteColumn valueColumn = new AutoCompleteColumn();
            valueColumn.Name = ValueColumnName;
            valueColumn.HeaderText = Labels.FxRatesEditor_InitColumns_Rate_ + valuationCurrency.Symbol;
            valueColumn.Width = 180;
            valueColumn.ValueList = allVariableNames.ToArray();

            currenciesGrid.Columns.Clear();
            currenciesGrid.Columns.AddRange(new DataGridViewColumn[] { nameColumn, symbolColumn, valueColumn });
        }

        private void Currencies_Load(object sender, EventArgs e)
        {
            Reload();
        }

        private void Reload()
        {
            valuationCurrency = Param.ValuationCurrency;

            InitColumns();

            Currency[] allCurrencies = Currency.FindAllNotEmpty();

            currenciesGrid.Rows.Add(allCurrencies.Length - 1);

            int i = 0;
            foreach (Currency currency in allCurrencies)
            {
                if (this.valuationCurrency != currency)
                {
                    FillNewRow(currency, i);
                    i++;
                }
            }
        }

        private void FillNewRow(Currency currency, int index) 
        {
            FXRate fxRate = null;
            if(currency == null)
            {
                currency = new Currency();
            }else
            {
                fxRate = FXRate.GetRate(currency, this.valuationCurrency);
            }
            if(fxRate == null)
            {
                fxRate = new FXRate();
                fxRate.From = currency;
                fxRate.To = this.valuationCurrency;
            }

            currenciesGrid[NameColumnName, index].Value = currency.ShortName;
            currenciesGrid[SymbolColumnName, index].Value = currency.Symbol;
            currenciesGrid[ValueColumnName, index].Value = fxRate.Value;
            currenciesGrid.Rows[index].Tag = fxRate;
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in currenciesGrid.Rows)
            {
                FXRate rate = row.Tag as FXRate;
                if(rate == null) return;
                string name = (string) row.Cells[NameColumnName].Value;
                string symbol = (string)row.Cells[SymbolColumnName].Value;

                if (string.IsNullOrEmpty(symbol) && !string.IsNullOrEmpty(name)) symbol = name;
                if (string.IsNullOrEmpty(name) && !string.IsNullOrEmpty(symbol)) name = symbol;

                if(!string.IsNullOrEmpty(symbol))
                {
                    rate.From.ShortName = name;
                    rate.From.Symbol = symbol;
                    rate.From.Save();
                    rate.Value = (string)row.Cells[ValueColumnName].Value;
                    rate.Save();
                }
            }
            PersistenceSession.Flush();
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            AddNewRowsPrompt addNewRowsPrompt = new AddNewRowsPrompt();
            addNewRowsPrompt.Text = Labels.FxRatesEditor_addButton_Click_Add_currencies;
            if (addNewRowsPrompt.ShowDialog() == DialogResult.OK)
            {
                AddRows(addNewRowsPrompt.RowCount);
            }
        }

        private void AddRows(int count)
        {
            for (int i = 0; i < count; i++)
            {
                int index = currenciesGrid.Rows.Add();
                FillNewRow(null, index);
            }
        }
    }
}