using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using AssetModelingModule.Domain;

namespace AssetModelingModule.Views.Variables
{
    public partial class VariableDefinition : Form
    {
        private string _c_value = string.Empty;

        private BindingList<Variable> variableList;

        public VariableDefinition()
        {
            InitializeComponent();

            valuesDataGridView.AutoGenerateColumns = false;
        }

        private void VariableDefinition_Load(object sender, System.EventArgs e)
        {
            variablesListBox.DisplayMember = "Name";
            variablesListBox.ValueMember = "Self";
            variablesListBox.DataSource = variableList = new BindingList<Variable>(new List<Variable>(Variable.FindAll()));

            RefreshVector(string.Empty);

            if (vectorListBox.SelectedItem == null || valuesDataGridView.CurrentCell == null)
                asociateButton.Enabled = false;
            else
                asociateButton.Enabled = true;

        }

        private void RefreshVector(string val)
        {
            vectorListBox.DisplayMember = "Name";
            vectorListBox.ValueMember = "Self";
            vectorListBox.DataSource = new BindingList<Vector>(new List<Vector>(Vector.FindLikeName(val)));
        }

        private void addButton_Click(object sender, System.EventArgs e)
        {
            string variableName = newVariableTextBox.Text;
            variableName = Tools.EscapeIdentifier(variableName);

            if (!Constants.VectorOrVariableRegex.IsMatch(variableName))
            {
                MessageBox.Show(string.Format(Properties.Resources.InvalidVectorOrVariableName, variableName), "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            if (Tools.IsFunctionName(variableName))
            {
                MessageBox.Show(string.Format(Properties.Resources.ReservedFunctionName, variableName), "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            if (Variable.ExistsByName(variableName))
            {
                MessageBox.Show(string.Format(Properties.Resources.VariableAlreadyExists, variableName),
                    "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            Variable variable = variableList.AddNew();
            variable.Name = variableName;
            variable.Create();
            variableList.ResetBindings();

            newVariableTextBox.Text = "";
            valuesDataGridResetBindings();

            Views.BalanceSheet.BalanceSheet.modifiedScenarioTemplate = true;

        }

        private void removeButton_Click(object sender, System.EventArgs e)
        {
            Variable variable = variablesListBox.SelectedValue as Variable;
            if (variable == null) return;

            if (variable.Selected)
            {
                MessageBox.Show(string.Format(Properties.Resources.ImpossibleToRemoveVariable, variable.Name),
                    "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            variableList.Remove(variable);
            variableList.ResetBindings();
            DeleteVariable(variable);

            valuesDataGridResetBindings();
        }

        private static void DeleteVariable(Variable variable)
        {
            foreach (VariableValue value in variable.VariableValues)
            {
                value.Scenario.VariableValues.Remove(value);
            }
            variable.Delete();
            Views.BalanceSheet.BalanceSheet.modifiedScenarioTemplate = true;
        }

        private void newVariableTextBox_TextChanged(object sender, System.EventArgs e)
        {
            addButton.Enabled = !string.IsNullOrEmpty(newVariableTextBox.Text);
        }

        private void variablesListBox_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if (variablesListBox.SelectedItem == null)
            {
                asociateButton.Enabled = false;
                valuesDataGridView.Rows.Clear();
            }
            else
                asociateButton.Enabled = true;

            removeBbutton.Enabled = (variablesListBox.SelectedIndex >= 0);

            valuesDataGridResetBindings();
        }

        private void valuesDataGridResetBindings()
        {
            valuesDataGridView.DataBindings.Clear();
            Variable variable = variablesListBox.SelectedValue as Variable;
            if (variable == null) return;

            Scenario[] selectedScenarios = Scenario.FindSelected();
            if (variable.VariableValues.Count != selectedScenarios.Length)
            {
                AddValuesForAllSelectedScenarios(variable, selectedScenarios);
            }

            VariableValue[] values = VariableValue.FindByVariable(variable);
            valuesDataGridView.DataSource = new BindingList<VariableValue>(new List<VariableValue>(values));
        }

        private static void AddValuesForAllSelectedScenarios(Variable variable, Scenario[] selectedScenarios)
        {
            foreach (Scenario scenario in selectedScenarios)
            {
                if (!HasValueForScenario(variable, scenario))
                {
                    VariableValue value = new VariableValue();
                    value.Scenario = scenario;
                    value.Variable = variable;
                    variable.VariableValues.Add(value);
                    scenario.VariableValues.Add(value);
                }
            }
        }

        private static bool HasValueForScenario(Variable variable, Scenario scenario)
        {
            foreach (VariableValue value in variable.VariableValues)
            {
                if (value.Scenario == scenario)
                {
                    return true;
                }
            }

            return false;
        }

        private void VariableDefinition_Activated(object sender, System.EventArgs e)
        {
            newVariableTextBox.Focus();
        }

        private void okButton_Click(object sender, System.EventArgs e)
        {
            PersistenceSession.Flush();
        }

        private void vectorFilterTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            RefreshVector((sender as TextBox).Text);
        }

        private void asociateButton_Click(object sender, System.EventArgs e)
        {
            DataGridViewCell currentcell = valuesDataGridView[valueColumn.Index, valuesDataGridView.CurrentCell.RowIndex];
            currentcell.Value = currentcell.Value + (vectorListBox.SelectedItem as Vector).Name.ToString();
        }

        private void valuesDataGridView_SelectionChanged(object sender, System.EventArgs e)
        {
            if (valuesDataGridView.CurrentCell == null)
                asociateButton.Enabled = false;
            else
                asociateButton.Enabled = true;
        }

        private void valuesDataGridView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                Scenario[] scenarios = Scenario.FindNonGenerated();
                Variable[] variables = Variable.FindAll();
                Scenario scenario = scenarios[e.RowIndex];
                Variable variable = variables[e.ColumnIndex - 1];

                VariableValue value = VariableValue.Find(variable, scenario);

                string _val = _c_value;
                if (_val != value.Value)
                    Views.BalanceSheet.BalanceSheet.modifiedScenarioTemplate = true;
            }
        }

        private void valuesDataGridView_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            _c_value = Convert.ToString(valuesDataGridView[e.ColumnIndex, e.RowIndex].Value);
        }

    }
}