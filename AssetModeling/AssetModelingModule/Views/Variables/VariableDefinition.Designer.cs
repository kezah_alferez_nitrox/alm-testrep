﻿using AssetModelingModule.Resources.Language;

namespace AssetModelingModule.Views.Variables
{
    partial class VariableDefinition
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.okButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.variablesGroupBox = new System.Windows.Forms.GroupBox();
            this.variablesListBox = new System.Windows.Forms.ListBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.removeBbutton = new System.Windows.Forms.Button();
            this.addButton = new System.Windows.Forms.Button();
            this.newVariableTextBox = new System.Windows.Forms.TextBox();
            this.valuesGroupBox = new System.Windows.Forms.GroupBox();
            this.valuesDataGridView = new System.Windows.Forms.DataGridView();
            this.scenarioColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.valueColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vectorsGroupBox = new System.Windows.Forms.GroupBox();
            this.vectorListBox = new System.Windows.Forms.ListBox();
            this.topVectorsPanel = new System.Windows.Forms.Panel();
            this.vectorFilterTextBox = new System.Windows.Forms.TextBox();
            this.asociateButton = new System.Windows.Forms.Button();
            this.centerPanel = new System.Windows.Forms.Panel();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1.SuspendLayout();
            this.variablesGroupBox.SuspendLayout();
            this.panel2.SuspendLayout();
            this.valuesGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.valuesDataGridView)).BeginInit();
            this.vectorsGroupBox.SuspendLayout();
            this.topVectorsPanel.SuspendLayout();
            this.centerPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.okButton);
            this.panel1.Controls.Add(this.cancelButton);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 399);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(742, 57);
            this.panel1.TabIndex = 1;
            // 
            // okButton
            // 
            this.okButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.okButton.Location = new System.Drawing.Point(488, 13);
            this.okButton.Name = "okButton";
            this.okButton.Padding = new System.Windows.Forms.Padding(5);
            this.okButton.Size = new System.Drawing.Size(116, 32);
            this.okButton.TabIndex = 9;
            this.okButton.Text = Labels.VariableDefinition_InitializeComponent_OK;
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Click += new System.EventHandler(this.okButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cancelButton.Location = new System.Drawing.Point(614, 13);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Padding = new System.Windows.Forms.Padding(5);
            this.cancelButton.Size = new System.Drawing.Size(116, 32);
            this.cancelButton.TabIndex = 8;
            this.cancelButton.Text = Labels.VariableDefinition_InitializeComponent_Cancel;
            this.cancelButton.UseVisualStyleBackColor = true;
            // 
            // variablesGroupBox
            // 
            this.variablesGroupBox.Controls.Add(this.variablesListBox);
            this.variablesGroupBox.Controls.Add(this.panel2);
            this.variablesGroupBox.Dock = System.Windows.Forms.DockStyle.Left;
            this.variablesGroupBox.Location = new System.Drawing.Point(0, 0);
            this.variablesGroupBox.Name = "variablesGroupBox";
            this.variablesGroupBox.Size = new System.Drawing.Size(211, 399);
            this.variablesGroupBox.TabIndex = 2;
            this.variablesGroupBox.TabStop = false;
            this.variablesGroupBox.Text = Labels.VariableDefinition_InitializeComponent_Variables;
            // 
            // variablesListBox
            // 
            this.variablesListBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.variablesListBox.FormattingEnabled = true;
            this.variablesListBox.IntegralHeight = false;
            this.variablesListBox.Location = new System.Drawing.Point(3, 16);
            this.variablesListBox.Name = "variablesListBox";
            this.variablesListBox.Size = new System.Drawing.Size(205, 305);
            this.variablesListBox.TabIndex = 1;
            this.variablesListBox.SelectedIndexChanged += new System.EventHandler(this.variablesListBox_SelectedIndexChanged);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.removeBbutton);
            this.panel2.Controls.Add(this.addButton);
            this.panel2.Controls.Add(this.newVariableTextBox);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(3, 321);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(205, 75);
            this.panel2.TabIndex = 0;
            // 
            // removeBbutton
            // 
            this.removeBbutton.Enabled = false;
            this.removeBbutton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.removeBbutton.Location = new System.Drawing.Point(114, 37);
            this.removeBbutton.Name = "removeBbutton";
            this.removeBbutton.Padding = new System.Windows.Forms.Padding(5);
            this.removeBbutton.Size = new System.Drawing.Size(72, 32);
            this.removeBbutton.TabIndex = 11;
            this.removeBbutton.Text = Labels.VariableDefinition_InitializeComponent_Delete;
            this.removeBbutton.UseVisualStyleBackColor = true;
            this.removeBbutton.Click += new System.EventHandler(this.removeButton_Click);
            // 
            // addButton
            // 
            this.addButton.Enabled = false;
            this.addButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.addButton.Location = new System.Drawing.Point(21, 37);
            this.addButton.Name = "addButton";
            this.addButton.Padding = new System.Windows.Forms.Padding(5);
            this.addButton.Size = new System.Drawing.Size(72, 32);
            this.addButton.TabIndex = 10;
            this.addButton.Text = Labels.VariableDefinition_InitializeComponent_Add;
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // newVariableTextBox
            // 
            this.newVariableTextBox.Location = new System.Drawing.Point(3, 11);
            this.newVariableTextBox.Name = "newVariableTextBox";
            this.newVariableTextBox.Size = new System.Drawing.Size(199, 20);
            this.newVariableTextBox.TabIndex = 0;
            this.newVariableTextBox.TextChanged += new System.EventHandler(this.newVariableTextBox_TextChanged);
            // 
            // valuesGroupBox
            // 
            this.valuesGroupBox.Controls.Add(this.valuesDataGridView);
            this.valuesGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.valuesGroupBox.Location = new System.Drawing.Point(0, 0);
            this.valuesGroupBox.Name = "valuesGroupBox";
            this.valuesGroupBox.Size = new System.Drawing.Size(357, 399);
            this.valuesGroupBox.TabIndex = 3;
            this.valuesGroupBox.TabStop = false;
            this.valuesGroupBox.Text = Labels.VariableDefinition_InitializeComponent_Values;
            // 
            // valuesDataGridView
            // 
            this.valuesDataGridView.AllowUserToAddRows = false;
            this.valuesDataGridView.AllowUserToDeleteRows = false;
            this.valuesDataGridView.BackgroundColor = System.Drawing.SystemColors.Window;
            this.valuesDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.valuesDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.scenarioColumn,
            this.valueColumn});
            this.valuesDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.valuesDataGridView.Location = new System.Drawing.Point(3, 16);
            this.valuesDataGridView.Name = "valuesDataGridView";
            this.valuesDataGridView.Size = new System.Drawing.Size(351, 380);
            this.valuesDataGridView.TabIndex = 0;
            this.valuesDataGridView.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.valuesDataGridView_CellValueChanged);
            this.valuesDataGridView.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.valuesDataGridView_CellEnter);
            this.valuesDataGridView.SelectionChanged += new System.EventHandler(this.valuesDataGridView_SelectionChanged);
            // 
            // scenarioColumn
            // 
            this.scenarioColumn.DataPropertyName = "Scenario";
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.LightGray;
            this.scenarioColumn.DefaultCellStyle = dataGridViewCellStyle3;
            this.scenarioColumn.HeaderText = Labels.VariableDefinition_InitializeComponent_Scenario;
            this.scenarioColumn.Name = "scenarioColumn";
            this.scenarioColumn.ReadOnly = true;
            // 
            // valueColumn
            // 
            this.valueColumn.DataPropertyName = "Value";
            this.valueColumn.HeaderText = Labels.VariableDefinition_InitializeComponent_Expression;
            this.valueColumn.Name = "valueColumn";
            this.valueColumn.Width = 200;
            // 
            // vectorsGroupBox
            // 
            this.vectorsGroupBox.Controls.Add(this.vectorListBox);
            this.vectorsGroupBox.Controls.Add(this.topVectorsPanel);
            this.vectorsGroupBox.Dock = System.Windows.Forms.DockStyle.Right;
            this.vectorsGroupBox.Location = new System.Drawing.Point(357, 0);
            this.vectorsGroupBox.Name = "vectorsGroupBox";
            this.vectorsGroupBox.Size = new System.Drawing.Size(174, 399);
            this.vectorsGroupBox.TabIndex = 4;
            this.vectorsGroupBox.TabStop = false;
            this.vectorsGroupBox.Text = Labels.VariableDefinition_InitializeComponent_Vectors;
            // 
            // vectorListBox
            // 
            this.vectorListBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.vectorListBox.FormattingEnabled = true;
            this.vectorListBox.IntegralHeight = false;
            this.vectorListBox.Location = new System.Drawing.Point(3, 86);
            this.vectorListBox.Name = "vectorListBox";
            this.vectorListBox.Size = new System.Drawing.Size(168, 310);
            this.vectorListBox.TabIndex = 1;
            // 
            // topVectorsPanel
            // 
            this.topVectorsPanel.Controls.Add(this.vectorFilterTextBox);
            this.topVectorsPanel.Controls.Add(this.asociateButton);
            this.topVectorsPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.topVectorsPanel.Location = new System.Drawing.Point(3, 16);
            this.topVectorsPanel.Name = "topVectorsPanel";
            this.topVectorsPanel.Size = new System.Drawing.Size(168, 70);
            this.topVectorsPanel.TabIndex = 0;
            // 
            // vectorFilterTextBox
            // 
            this.vectorFilterTextBox.Location = new System.Drawing.Point(3, 42);
            this.vectorFilterTextBox.Name = "vectorFilterTextBox";
            this.vectorFilterTextBox.Size = new System.Drawing.Size(162, 20);
            this.vectorFilterTextBox.TabIndex = 1;
            this.vectorFilterTextBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.vectorFilterTextBox_KeyUp);
            // 
            // asociateButton
            // 
            this.asociateButton.Location = new System.Drawing.Point(3, 4);
            this.asociateButton.Name = "asociateButton";
            this.asociateButton.Size = new System.Drawing.Size(162, 32);
            this.asociateButton.TabIndex = 0;
            this.asociateButton.Text = Labels.VariableDefinition_InitializeComponent_Associate;
            this.asociateButton.UseVisualStyleBackColor = true;
            this.asociateButton.Click += new System.EventHandler(this.asociateButton_Click);
            // 
            // centerPanel
            // 
            this.centerPanel.Controls.Add(this.valuesGroupBox);
            this.centerPanel.Controls.Add(this.vectorsGroupBox);
            this.centerPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.centerPanel.Location = new System.Drawing.Point(211, 0);
            this.centerPanel.Name = "centerPanel";
            this.centerPanel.Size = new System.Drawing.Size(531, 399);
            this.centerPanel.TabIndex = 5;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Scenario";
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.LightGray;
            this.dataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridViewTextBoxColumn1.HeaderText = Labels.VariableDefinition_InitializeComponent_Scenario;
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Value";
            this.dataGridViewTextBoxColumn2.HeaderText = Labels.VariableDefinition_InitializeComponent_Expression;
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Width = 200;
            // 
            // VariableDefinition
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(742, 456);
            this.ControlBox = false;
            this.Controls.Add(this.centerPanel);
            this.Controls.Add(this.variablesGroupBox);
            this.Controls.Add(this.panel1);
            this.Name = "VariableDefinition";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = Labels.VariableDefinition_InitializeComponent_Variable_Definition;
            this.Load += new System.EventHandler(this.VariableDefinition_Load);
            this.Activated += new System.EventHandler(this.VariableDefinition_Activated);
            this.panel1.ResumeLayout(false);
            this.variablesGroupBox.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.valuesGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.valuesDataGridView)).EndInit();
            this.vectorsGroupBox.ResumeLayout(false);
            this.topVectorsPanel.ResumeLayout(false);
            this.topVectorsPanel.PerformLayout();
            this.centerPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.GroupBox variablesGroupBox;
        private System.Windows.Forms.GroupBox valuesGroupBox;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button removeBbutton;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.TextBox newVariableTextBox;
        private System.Windows.Forms.ListBox variablesListBox;
        private System.Windows.Forms.DataGridView valuesDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn scenarioColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn valueColumn;
        private System.Windows.Forms.GroupBox vectorsGroupBox;
        private System.Windows.Forms.ListBox vectorListBox;
        private System.Windows.Forms.Panel topVectorsPanel;
        private System.Windows.Forms.Panel centerPanel;
        private System.Windows.Forms.TextBox vectorFilterTextBox;
        private System.Windows.Forms.Button asociateButton;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
    }
}