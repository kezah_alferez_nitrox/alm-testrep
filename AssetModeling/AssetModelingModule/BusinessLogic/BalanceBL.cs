using System;
using AssetModelingModule.Domain;
using Castle.ActiveRecord;
using Eu.AlterSystems.ASNetLib.Core;

namespace AssetModelingModule.BusinessLogic
{
    public class BalanceBL
    {
        private readonly static int[] cashAdjBondIdCache = new int[4];

        public static bool BalanceAdjustmentNeeded = true;

        public static void AjustBalanceIfNeeded()
        {
            if (BalanceAdjustmentNeeded)
            {
                AjustBalance();
                BalanceAdjustmentNeeded = false;
            }
        }

        /// <summary>
        /// Il faut que l'une des lignes serve � ajuster la position cash �quilibrant la balance comptable. Cette ligne a l'attribut "cash adj". 
        /// Il doit donc �tre cr�� syst�matiquement et automatiquement deux lignes, une � l'actif et l'autre au passif. 
        /// -	Si Sum(Asset) > Sum(Liabilities) + Sum(Equity), la ligne en liabilities cr��e avec l'attribut "cash adj" 
        ///     vera sa "book value" �tre �gale � la diff�rence (de fa�on � ajuster actif et passif)
        /// -	Si Sum(Asset) < Sum(Liabilities) + Sum(Equity), la ligne en Assets cr��e avec l'attribut "cash adj"
        ///     vera sa "book value" �tre �gale � la diff�rence (de fa�on � ajuster actif et passif)
        /// </summary>
        public static void AjustBalance()
        {
            PersistenceSession.Flush();

            decimal totalAssets;
            decimal totalLiabilities;
            decimal totalEquity;

            bool isNewBondLiability;
            bool isNewBondAsset;

            if (Category.FindAll().Length == 0) return;

            Bond cashAdjBondLiability = GetCashAdjBondByBalanceType(BalanceType.Liability, out isNewBondLiability);
            Bond cashAdjBondAsset = GetCashAdjBondByBalanceType(BalanceType.Asset, out isNewBondAsset);

            Currency valuationCurrency = Param.ValuationCurrency;
            if (valuationCurrency != null)
            {
                cashAdjBondAsset.Currency = valuationCurrency;
                cashAdjBondLiability.Currency = valuationCurrency;
                PersistenceSession.Flush();
            }

            ComputeTotalsForALM(out totalAssets, out totalLiabilities, out totalEquity);

            decimal delta = totalAssets - (totalLiabilities + totalEquity);

            decimal cashAdjAssetValue = cashAdjBondAsset.BookValue.GetValueOrDefault();
            decimal cashAdjLiabilitiesValue = cashAdjBondLiability.BookValue.GetValueOrDefault();

            AjustCashAdjBonds(ref cashAdjAssetValue, ref cashAdjLiabilitiesValue, delta);

            cashAdjBondAsset.BookValue = cashAdjAssetValue;
            cashAdjBondLiability.BookValue = cashAdjLiabilitiesValue;

            PersistenceSession.Flush();

            if (isNewBondAsset)
                cashAdjBondAsset.Category.Refresh();
            if (isNewBondLiability)
                cashAdjBondLiability.Category.Refresh();
        }

        public static void ClearCache()
        {
            //for (int i = 0; i < cashAdjBondIdCache.Length; i++)
            //{
            //    cashAdjBondIdCache[i] = 0;
            //}
        }

        public static void AjustCashAdjBonds(ref decimal cashAdjBondAsset, ref decimal cashAdjBondLiability, decimal balance)
        {
            if (balance > 0)
            {
                cashAdjBondLiability += balance;
            }
            else if (balance < 0)
            {
                cashAdjBondAsset -= balance;
            }

            if (cashAdjBondAsset >= cashAdjBondLiability)
            {
                cashAdjBondAsset = cashAdjBondAsset - cashAdjBondLiability;
                cashAdjBondLiability = 0;
            }
            else
            {
                cashAdjBondLiability = cashAdjBondLiability - cashAdjBondAsset;
                cashAdjBondAsset = 0;
            }
        }

        private static void ComputeTotalsForALM(out decimal assets, out decimal liabilities, out decimal equity)
        {
            assets = liabilities = equity = 0;

            Bond[] allBonds = ActiveRecordBase<Bond>.FindAll();

            foreach (Bond bond in allBonds)
            {
                if (bond.Exclude) continue;
                if (bond.Attrib == BondAttrib.Notional) continue;

                decimal bondBookValue = CurrencyBL.ConvertToValuationCurrency(bond.BookValue ?? 0m, bond.Currency);

                switch (bond.Category.BalanceType)
                {
                    case BalanceType.None:
                        break;
                    case BalanceType.Asset:
                        assets += bondBookValue;
                        break;
                    case BalanceType.Liability:
                        liabilities += bondBookValue;
                        break;
                    case BalanceType.Equity:
                        equity += bondBookValue;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        private static Bond GetCashAdjBondByBalanceType(BalanceType balanceType, out bool isNewBond)
        {
            isNewBond = false;
            int intBlanceType = (int)balanceType;

            //// try cache
            //Bond bond = Bond.TryFind(cashAdjBondIdCache[intBlanceType]);

            //if (bond == null || bond.Attrib != BondAttrib.CashAdj || bond.Category.BalanceType != balanceType)
            //{
            // try existent Cash Adj bond
            Bond bond = Bond.FindFirstCashAdjByBalanceTypeAndAttrib(balanceType, BondAttrib.CashAdj);
            //}

            if (bond == null || bond.Attrib != BondAttrib.CashAdj)
            {
                // Create new Cash Adj bond
                bond = new Bond();
                bond.Category = Category.CashAdjCategory(balanceType);
                bond.Description = Constants.CASH_ADJ_BOND_DESCRIPTION;
                bond.Attrib = BondAttrib.CashAdj;
                bond.Currency = Param.ValuationCurrency;
                bond.GenerateALMID();
                bond.Duration = Constants.DEFAULT_BOND_DURATION;
                bond.BookPrice = 100;
                bond.CreateAndFlush();

                isNewBond = true;
            }
            else
            {
                bond.Refresh();
            }

            // cashAdjBondIdCache[intBlanceType] = bond.Id;

            return bond;
        }

        public static void EnsureSpecialCategoriesAndBondsExist()
        {
            PersistenceSession.Flush();

            EnsureSpecialCategoryAndBondExist(CategoryType.CashAdj, true, true, false);
            EnsureSpecialCategoryAndBondExist(CategoryType.Treasury, true, false, false);
            EnsureSpecialCategoryAndBondExist(CategoryType.Impairments, true, false, false);
            EnsureSpecialCategoryAndBondExist(CategoryType.Receivable, true, false, false);
            EnsureSpecialCategoryAndBondExist(CategoryType.Reserves, false, false, true);
            EnsureSpecialCategoryAndBondExist(CategoryType.OtherComprehensiveIncome, false, false, true);
        }

        private static void EnsureSpecialCategoryAndBondExist(CategoryType type,
            bool inAssets, bool inLiabilities, bool inEquity)
        {
            Category rootAssets = null;
            Category rootLiabilities = null;
            Category rootEquity = null;

            Category root = Category.FindRoot();
            if (root == null) return;

            root.Refresh();

            foreach (Category child in root.Children)
            {
                if (child.BalanceType == BalanceType.Asset) rootAssets = child;
                if (child.BalanceType == BalanceType.Liability) rootLiabilities = child;
                if (child.BalanceType == BalanceType.Equity) rootEquity = child;
            }

            if (rootAssets != null && inAssets)
            {
                Category assetsSpecial = EnsureSpecialCategoryExists(rootAssets, type);
                EnsureSpecialBondExists(assetsSpecial);
            }
            if (rootLiabilities != null && inLiabilities)
            {
                Category liabilitiesSpecial = EnsureSpecialCategoryExists(rootLiabilities, type);
                EnsureSpecialBondExists(liabilitiesSpecial);
            }
            if (rootEquity != null && inEquity)
            {
                Category equitySpecial = EnsureSpecialCategoryExists(rootEquity, type);
                EnsureSpecialBondExists(equitySpecial);
            }
        }

        private static void EnsureSpecialBondExists(Category category)
        {
            Bond[] bonds = Bond.FindByCategory(category);
            if (bonds != null && bonds.Length >= 1) return;

            Bond bond = new Bond();
            bond.Category = category;
            bond.Description = EnumHelper.GetDescription(category.Type);
            bond.Attrib = Category.MapCategoryTypeToBondAttrib(category.Type);
            bond.Duration = Constants.DEFAULT_BOND_DURATION;
            bond.MarketPrice = "0";
            bond.Coupon = "0";
            bond.GenerateALMID();
            category.Bonds.Add(bond);
            bond.CreateAndFlush();
        }

        private static Category EnsureSpecialCategoryExists(Category parent, CategoryType type)
        {
            foreach (Category child in parent.Children)
            {
                if (child.Type == type) return child;
            }

            Category category = new Category();
            category.Label = EnumHelper.GetDescription(type);
            category.ReadOnly = true;
            category.Type = type;
            category.Parent = parent;
            category.BalanceType = parent.BalanceType;
            category.isVisible = parent.isVisible;
            parent.Children.Add(category);
            category.Create();

            return category;
        }
    }
}