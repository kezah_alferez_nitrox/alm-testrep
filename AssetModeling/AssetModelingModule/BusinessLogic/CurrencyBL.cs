using System;
using System.Collections.Generic;
using AssetModelingModule.Calculus;
using AssetModelingModule.Domain;

namespace AssetModelingModule.BusinessLogic
{
    public class CurrencyBL
    {
        private const decimal IdentityFxRate = 1m;

        public static decimal Convert(decimal value, Currency from, Currency to)
        {
            return FxRate(from, to) * value;
        }

        public static decimal FxRate(Currency from, Currency to)
        {
            if (from == null || to == null) return IdentityFxRate;
            return FxRatesCache[from][to];
        }

        public static void ResetValuationDateFxRates()
        {
            fxRatesCache.Clear();
        }

        private static readonly Dictionary<Currency, Dictionary<Currency, decimal>> fxRatesCache = new Dictionary<Currency, Dictionary<Currency, decimal>>();
        private static Dictionary<Currency, Dictionary<Currency, decimal>> FxRatesCache
        {
            get
            {
                if (fxRatesCache.Count == 0)
                {
                    Currency[] currencies = Currency.FindAll();
                    foreach (Currency fromCurrency in currencies)
                    {
                        fxRatesCache[fromCurrency] = new Dictionary<Currency, decimal>();
                        foreach (Currency toCurrency in currencies)
                        {
                            fxRatesCache[fromCurrency][toCurrency] = IdentityFxRate;
                        }
                    }

                    Scenario[] selected = Scenario.FindSelected();
                    Scenario firstScenario = selected.Length > 0 ? selected[0] : null;

                    DateTime valuationDate = Param.ValuationDate ?? DateTime.Today;
                    FXRate[] allFXRates = FXRate.FindAll();

                    foreach (Currency fromCurrency in currencies)
                    {
                        foreach (Currency toCurrency in currencies)
                        {
                            if (fromCurrency == toCurrency) continue;

                            foreach (FXRate rate in allFXRates)
                            {
                                if (rate.From == fromCurrency && rate.To == toCurrency && rate.Value != null)
                                {
                                    decimal fxRate = ComputeFxRate(rate.Value, firstScenario, valuationDate);

                                    fxRatesCache[fromCurrency][toCurrency] = fxRate;
                                    fxRatesCache[toCurrency][fromCurrency] = IdentityFxRate / fxRate;
                                }
                            }
                        }
                    }
                }
                return fxRatesCache;
            }
        }

        private static decimal ComputeFxRate(string variableName, Scenario scenario, DateTime date)
        {
            decimal fxRate;

            if (decimal.TryParse(variableName, out fxRate)) return fxRate;

            Variable variable = Variable.FindByName(variableName);
            if (variable == null) return IdentityFxRate;

            VariableValue variableValue = VariableValue.Find(variable, scenario);
            if (variableValue == null || string.IsNullOrEmpty(variableValue.Value)) return IdentityFxRate;

            fxRate = ComputeVariableValue(variableValue.Value, date);
            return fxRate;
        }

        private static decimal ComputeVariableValue(string value, DateTime date)
        {            
            decimal decimalValue;
            if (decimal.TryParse(value, out decimalValue)) return decimalValue;

            IDictionary<string, object> operands = ComputeOperands(value, date);
            ExpressionParser parser = new ExpressionParser();            
            try
            {
                value = Tools.PreProcessExpression(value);
                decimalValue = (decimal)parser.Evaluate(value, operands);
            }
            catch(Exception ex)
            {
                Log.Exception("ComputeVariableValue", ex);
                decimalValue = IdentityFxRate;
            }

            return decimalValue;
        }

        private static IDictionary<string, object> ComputeOperands(string value, DateTime date)
        {
            string[] operands = Tools.GetOperandsFromExpression(value);
            IDictionary<string, object> vectorValues = new Dictionary<string, object>(32, StringComparer.CurrentCultureIgnoreCase);

            foreach (string operand in operands)
            {
                decimal unused;
                if (decimal.TryParse(operand, out unused)) continue;

                Vector vector = Vector.FindByName(operand);
                if (vector == null) continue;

                VectorPoint point = vector.GetCurrentValue(date);
                if (point == null)
                {
                    if (vector.VectorPoints.Count > 0)
                    {
                        point = vector.VectorPoints[0];
                    }
                    else
                    {
                        continue;
                    }
                }

                vectorValues[operand] = point.Value;
            }

            return vectorValues;
        }

        public static decimal ConvertToValuationCurrency(decimal value, Currency currency)
        {
            return Convert(value, currency, Param.ValuationCurrency);
        }

        public static void SetSpecialBondsToValuationCurrency()
        {
            Currency valueationCurrency = Param.ValuationCurrency;

            SetBondsCurrency(Bond.FindByAttrib(BondAttrib.CashAdj), valueationCurrency);
            SetBondsCurrency(Bond.FindByAttrib(BondAttrib.Roll), valueationCurrency);
            SetBondsCurrency(Bond.FindByAttrib(BondAttrib.Impairments), valueationCurrency);
            SetBondsCurrency(Bond.FindByAttrib(BondAttrib.Treasury), valueationCurrency);

            PersistenceSession.Flush();
        }

        private static void SetBondsCurrency(Bond[] bonds, Currency currency)
        {
            foreach (Bond bond in bonds)
            {
                bond.Currency = currency;
                bond.Update();
            }
        }
    }
}