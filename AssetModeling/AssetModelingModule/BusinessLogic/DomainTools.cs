using System.Collections.Generic;
using AssetModelingModule.Domain;

namespace AssetModelingModule.BusinessLogic
{
    public class DomainTools
    {
        public static string[] GetAllVectorNames()
        {
            List<string> vectorNames = new List<string>();
            foreach (Vector vector in Vector.FindAll())
            {
                vectorNames.Add(vector.Name);
                PersistenceSession.Evict(vector);
            }

            return vectorNames.ToArray();
        }

        public static string[] GetAllvariablesAndVectorNames()
        {
            List<string> variableAndVectorNames = new List<string>();

            Variable[] allVariables = Variable.FindAll();
            Vector[] allVectors = Vector.FindAll();

            foreach (Variable var in allVariables)
            {
                variableAndVectorNames.Add(var.Name);
            }

            foreach (Vector vect in allVectors)
            {
                variableAndVectorNames.Add(vect.Name);
                PersistenceSession.Evict(vect);
            }

            return variableAndVectorNames.ToArray();
        }
    }
}
