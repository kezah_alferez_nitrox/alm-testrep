using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Threading;
using AssetModelingModule.Domain;
using AssetModelingModule.Resources.Language;
using Castle.ActiveRecord;
using Iesi.Collections.Generic;

namespace AssetModelingModule.Calculus
{
    public class Simulation
    {
        private readonly bool ignoreGenerated;
        private readonly IAlgorithmConfiguration algorithmConfiguration = new AlgorithmConfiguration();
        private readonly ExpressionParser parser = new ExpressionParser();

        private IDictionary<Scenario, ScenarioData> scenarioData = new Dictionary<Scenario, ScenarioData>();

        private readonly IDictionary<string, Vector> allVectors = new Dictionary<string, Vector>(30, StringComparer.CurrentCultureIgnoreCase);
        private readonly IDictionary<string, Variable> allVariables = new Dictionary<string, Variable>(30, StringComparer.CurrentCultureIgnoreCase);

        private readonly ISet<Vector> usedVectors = new HashedSet<Vector>();
        private readonly ISet<Variable> usedVariables = new HashedSet<Variable>();
        private readonly ISet<Currency> usedCurrencies = new HashedSet<Currency>();

        private readonly IDictionary<string, object>[] vectorValues =
            new IDictionary<string, object>[Constants.RESULT_MONTHS];

        private readonly IDictionary<string, decimal>[] variableValues =
            new IDictionary<string, decimal>[Constants.RESULT_MONTHS];

        private readonly IDictionary<string, object>[] variableAndVectorValues =
            new IDictionary<string, object>[Constants.RESULT_MONTHS];

        private readonly IDictionary<Currency, decimal[]> fxRateValues =
            new Dictionary<Currency, decimal[]>();

        private OtherItemAssum[] otherItemAssumptions;
        private Dividend[] dividends;
        private Category[] categories;
        private FXRate[] fxRates;
        private List<Scenario> selectedScenarios;
        private Currency valuationCurrency;
        private DateTime valuationDate;

        private readonly List<ValidationMessage> validationErrors = new List<ValidationMessage>();

        private bool isSuccesfull;
        private readonly IList<BondData> bondDatas = new List<BondData>();

        public bool IsSuccesfull
        {
            get { return isSuccesfull; }
        }

        public List<ValidationMessage> ValidationErrors
        {
            get { return validationErrors; }
        }

        public Simulation()
            : this(true)
        {
        }

        public Simulation(bool ignoreGenerated)
        {
            this.ignoreGenerated = ignoreGenerated;
        }

        private void Initialise()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo(Constants.CULTURE_NAME);

            isSuccesfull = false;

            scenarioData.Clear();
            allVectors.Clear();
            allVariables.Clear();
            usedVectors.Clear();
            usedVariables.Clear();
            usedCurrencies.Clear();
            validationErrors.Clear();
            bondDatas.Clear();

            Vector[] vectors = Vector.FindAll();
            foreach (Vector vector in vectors)
            {
                allVectors.Add(vector.Name, vector);
            }

            Variable[] variables = Variable.FindSelected();
            foreach (Variable variable in variables)
            {
                allVariables.Add(variable.Name, variable);
            }

            Bond[] bonds = Bond.FindAllNotExcluded();
            foreach (Bond bond in bonds)
            {
                BondData bondData = new BondData(bond, parser, algorithmConfiguration);
                bondDatas.Add(bondData);
            }

            otherItemAssumptions = ActiveRecordBase<OtherItemAssum>.FindAll();
            dividends = ActiveRecordBase<Dividend>.FindAll();
            categories = Category.FindAll();
            fxRates = FXRate.FindAll();
            valuationCurrency = Param.ValuationCurrency;
            valuationDate = Param.ValuationDate ?? DateTime.Today;

            selectedScenarios = new List<Scenario>(ignoreGenerated ?
                Scenario.FindSelected() : Scenario.FindGenerated());
        }

        public void Run(BackgroundWorker backgroundWorker)
        {
            TraceListener[] listeners = new TraceListener[Debug.Listeners.Count];
            Debug.Listeners.CopyTo(listeners, 0);
            Debug.Listeners.Clear();
            using (new SessionScope(FlushAction.Never))
            {
                Initialise();

                ValidateBonds();
                ValidateOIA();
                ValidateDividends();
                ValidateFxRates();
                if (validationErrors.Count > 0) return;

                backgroundWorker.ReportProgress(1);

                for (int i = 0; i < selectedScenarios.Count; i++)
                {
                    backgroundWorker.ReportProgress(1 + 99 * i / selectedScenarios.Count);

                    Scenario scenario = selectedScenarios[i];

                    Util.ValidateVariables(selectedScenarios, usedVariables, allVectors, usedVectors, validationErrors);
                    if (validationErrors.Count > 0) return;

                    Util.ComputeVectorValues(valuationDate, vectorValues, usedVectors);
                    Util.ComputeVariableValues(scenario, parser, vectorValues, variableValues, usedVariables, validationErrors);
                    if (validationErrors.Count > 0) return;

                    ComputeFxRates();
                    if (validationErrors.Count > 0) return;

                    InitVariableAndVectorValues();

                    ValuateBonds();
                    if (validationErrors.Count > 0) return;

                    ValuateOtherItemAssumptions();
                    if (validationErrors.Count > 0) return;

                    ScenarioData categoriesData = new ScenarioData(scenario, vectorValues);
                    categoriesData.Compute(bondDatas, otherItemAssumptions, dividends, fxRateValues, Param.StandardTax);
                    categoriesData.AggregateCategories(categories);

                    scenarioData.Add(scenario, categoriesData);
                }

                isSuccesfull = true;
            }
            Debug.Listeners.AddRange(listeners);
        }

        private void InitVariableAndVectorValues()
        {
            for (int i = 0; i < Constants.RESULT_MONTHS; i++)
            {
                variableAndVectorValues[i] = new Dictionary<string, object>(32, StringComparer.CurrentCultureIgnoreCase);

                foreach (KeyValuePair<string, object> vectorValue in vectorValues[i])
                {
                    variableAndVectorValues[i][vectorValue.Key] = vectorValue.Value;
                }

                foreach (KeyValuePair<string, decimal> variableValue in variableValues[i])
                {
                    variableAndVectorValues[i][variableValue.Key] = variableValue.Value;
                }
            }
        }

        private void ValidateBonds()
        {
            foreach (BondData bondData in bondDatas)
            {
                if (bondData.Bond.Attrib == BondAttrib.CashAdj) continue;

                bondData.ValidateBond(allVectors, allVariables);
                usedVariables.AddAll(bondData.UsedVariables);
                usedVectors.AddAll(bondData.UsedVectors);
                if (bondData.Bond.Currency != null) usedCurrencies.Add(bondData.Bond.Currency);
                validationErrors.AddRange(bondData.ValidationErrors);
            }
        }

        private void ValuateBonds()
        {
            foreach (BondData bondData in bondDatas)
            {
                if (bondData.Bond.Attrib == BondAttrib.CashAdj) continue;
                bondData.ValuateBond(variableAndVectorValues);
                validationErrors.AddRange(bondData.ValidationErrors);
            }
        }

        private void ValuateOtherItemAssumptions()
        {
            foreach (OtherItemAssum oia in otherItemAssumptions)
            {
                oia.Values = new decimal[Constants.RESULT_MONTHS];

                for (int i = 0; i < Constants.RESULT_MONTHS; i++)
                {
                    oia.Values[i] = GetComputedValue(oia.Vect, variableAndVectorValues[i]);
                }
            }
        }

        private decimal GetComputedValue(string value, IDictionary<string, object> variables)
        {
            decimal tempDecimal;
            if (!decimal.TryParse(value, out tempDecimal))
            {

                tempDecimal = ComputeExpression(value, variables);
            }
            return tempDecimal;
        }

        private decimal ComputeExpression(string value, IDictionary<string, object> variables)
        {
            decimal expressionValue = 0;
            try
            {
                string expression = Tools.PreProcessExpression(value);
                expressionValue = (decimal)parser.Evaluate(expression, variables);
            }
            catch (Exception ex)
            {
                Log.Warn(ex);
                AddValidationMessage(new ValidationMessage(string.Format("{0} in expression '{1}'", ex.Message, value)));
            }

            return expressionValue;
        }

        private void ComputeFxRates()
        {
            fxRateValues.Clear();

            fxRateValues[valuationCurrency] = new decimal[Constants.RESULT_MONTHS];
            for (int i = 0; i < Constants.RESULT_MONTHS; i++)
            {
                fxRateValues[valuationCurrency][i] = 1m;
            }

            foreach (FXRate rate in fxRates)
            {
                if (this.IsRateNeeded(rate))
                {
                    fxRateValues[rate.From] = new decimal[Constants.RESULT_MONTHS];

                    decimal value;
                    if (decimal.TryParse(rate.Value, out value))
                    {
                        for (int i = 0; i < Constants.RESULT_MONTHS; i++)
                        {
                            fxRateValues[rate.From][i] = value;
                        }
                    }
                    else
                    {
                        for (int i = 0; i < Constants.RESULT_MONTHS; i++)
                        {
                            fxRateValues[rate.From][i] = variableValues[i][rate.Value];
                        }
                    }
                }
            }
        }

        private void ValidateOIA()
        {
            foreach (OtherItemAssum assum in otherItemAssumptions)
            {
                if (Tools.IsDecimalString(assum.Vect)) continue;

                if (string.IsNullOrEmpty(assum.Vect))
                {
                    AddValidationMessage(new ValidationMessage(assum));
                }
                else
                {
                    string[] operands = Tools.GetOperandsFromExpression(assum.Vect);
                    foreach (string operand in operands)
                    {
                        if (Tools.IsDecimalString(operand))
                        {
                            continue;
                        }
                        if (allVectors.ContainsKey(operand))
                        {
                            usedVectors.Add(allVectors[operand]);
                            continue;
                        }
                        if (allVariables.ContainsKey(operand))
                        {
                            usedVariables.Add(allVariables[operand]);
                            continue;
                        }

                        AddValidationMessage(new ValidationMessage(assum));
                    }
                }
            }
        }

        private void ValidateDividends()
        {
            foreach (Dividend dividend in dividends)
            {
                decimal temp;
                if (!decimal.TryParse(dividend.Amount, out temp))
                {
                    if (allVectors.ContainsKey(dividend.Amount))
                    {
                        usedVectors.Add(allVectors[dividend.Amount]);
                    }
                    else
                    {
                        AddValidationMessage(new ValidationMessage(dividend));
                    }
                }
            }
        }

        private void ValidateFxRates()
        {
            foreach (FXRate rate in fxRates)
            {
                if (this.IsRateNeeded(rate))
                {
                    decimal temp;
                    string value = rate.Value;
                    if (!decimal.TryParse(value, out temp))
                    {
                        if (value != null && allVariables.ContainsKey(value))
                        {
                            usedVariables.Add(allVariables[value]);
                        }
                        else
                        {
                            AddValidationMessage(new ValidationMessage(rate));
                        }
                    }
                }
            }

            foreach (Currency usedCurrency in usedCurrencies)
            {
                if(usedCurrency == valuationCurrency) continue;

                if (!ExistsFXRateFrom(usedCurrency))
                {
                    AddValidationMessage(new ValidationMessage(Labels.Simulation_ValidateFxRates_No_exchange_rate_for_ + usedCurrency.Symbol + "/" + valuationCurrency.Symbol));
                }
            }
        }

        private void AddValidationMessage(ValidationMessage message)
        {
            Debugger.Break();
            validationErrors.Add(message);
        }

        private bool ExistsFXRateFrom(Currency currency)
        {
            foreach (FXRate rate in fxRates)
            {
                if(rate.From == currency) return true;
            }
            return false;
        }

        private bool IsRateNeeded(FXRate rate)
        {
            return this.usedCurrencies.Contains(rate.From) && rate.To == this.valuationCurrency;
        }

        public ScenarioData this[Scenario scenario]
        {
            get { return scenarioData.ContainsKey(scenario) ? scenarioData[scenario] : null; }
        }

        public int Count
        {
            get
            {
                return scenarioData.Count;
            }
        }

        public ICollection<Scenario> Scenarios
        {
            get
            {
                return scenarioData.Keys;
            }
        }

        //public XmlSchema GetSchema()
        //{
        //    return null;
        //}

        //public void ReadXml(XmlReader reader)
        //{
        //    reader.Read(); // Skip <Simulation>
        //    DictionarySerializer<Scenario, ScenarioData> serializer = new DictionarySerializer<Scenario, ScenarioData>();
        //    this.scenarioData = serializer.Deserialize(reader);
        //}

        //public void WriteXml(XmlWriter writer)
        //{
        //    DictionarySerializer<Scenario, ScenarioData> serializer = new DictionarySerializer<Scenario, ScenarioData>();
        //    serializer.Serialize(this.scenarioData, writer);
        //}
    }
}