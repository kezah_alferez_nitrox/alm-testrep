using System;
using System.Collections.Generic;
using AssetModelingModule.Domain;
using Iesi.Collections.Generic;

namespace AssetModelingModule.Calculus
{
    public class BondData
    {
        private readonly Bond bond;
        private readonly ExpressionParser parser;

        private readonly CalculusFunction currFaceFunction;
        private readonly CalculusFunction bookPriceFunction;
        private readonly CalculusFunction mtmFunction;
        private readonly CalculusFunction incomeFunction;

        private readonly decimal[] currFace = new decimal[Constants.RESULT_MONTHS];
        private readonly decimal[] growthes = new decimal[Constants.RESULT_MONTHS];

        private readonly decimal[] bookPrice = new decimal[Constants.RESULT_MONTHS];
        private readonly decimal[] income = new decimal[Constants.RESULT_MONTHS];
        private readonly decimal[] gainLoses = new decimal[Constants.RESULT_MONTHS];
        private readonly decimal[] otherComprehensiveIncome = new decimal[Constants.RESULT_MONTHS];
        private readonly decimal[] payment = new decimal[Constants.RESULT_MONTHS];

        private readonly decimal[] marketPriceValues = new decimal[Constants.RESULT_MONTHS];
        private readonly decimal[] couponValues = new decimal[Constants.RESULT_MONTHS];
        private readonly decimal[] thCurrFaceValues = new decimal[Constants.RESULT_MONTHS];
        private readonly decimal[] cprValues = new decimal[Constants.RESULT_MONTHS];
        private readonly decimal[] cdrValues = new decimal[Constants.RESULT_MONTHS];
        private readonly decimal[] lgdValues = new decimal[Constants.RESULT_MONTHS];

        public ModelData modelData;

        private readonly ISet<Vector> usedVectors = new HashedSet<Vector>();
        private readonly ISet<Variable> usedVariables = new HashedSet<Variable>();
        private readonly List<ValidationMessage> validationErrors = new List<ValidationMessage>();

        // required form XML Serialization
        private BondData()
        {
        }

        public BondData(Bond bond, ExpressionParser parser, IAlgorithmConfiguration algorithms)
        {
            this.bond = bond;
            this.parser = parser;

            currFaceFunction = algorithms.GetCurrFaceFunction(bond);
            bookPriceFunction = algorithms.GetBookPriceFunction(bond);
            mtmFunction = algorithms.GetMtMFunction(bond);
            incomeFunction = algorithms.GetIncomeFunction(bond);

            if (bond.IsModel)
            {
                modelData = new ModelData(this);
            }
        }

        public ISet<Vector> UsedVectors
        {
            get { return usedVectors; }
        }

        public ISet<Variable> UsedVariables
        {
            get { return usedVariables; }
        }

        public List<ValidationMessage> ValidationErrors
        {
            get { return validationErrors; }
        }

        public void ValidateBond(
            IDictionary<string, Vector> allVectors,
            IDictionary<string, Variable> allVariables)
        {
            if (string.IsNullOrEmpty(bond.MarketPrice)) bond.MarketPrice = "0";

            ValidateBondFormula("Coupon", bond.Coupon, allVectors, allVariables);
            ValidateBondFormula("MarketPrice", bond.MarketPrice, allVectors, allVariables);
            if (bond.Type == BondType.GROWTH) ValidateBondFormula("Growth", bond.Growth, allVectors, allVariables);

            if (bond.IsModel)
            {
                if (bond.Type == BondType.VAR)
                {
                    ValidateBondFormula("TheoreticalCurrFace", bond.TheoreticalCurrFace, allVectors, allVariables);
                }
                ValidateBondFormula("CDR", bond.CDR, allVectors, allVariables);
                ValidateBondFormula("CPR", bond.CPR, allVectors, allVariables);
                ValidateBondFormula("LGD", bond.LGD, allVectors, allVariables);

                if (bond.Type != BondType.VAR && bond.Attrib != BondAttrib.Roll && bond.OrigFace == null)
                {
                    validationErrors.Add(new ValidationMessage(bond, "OrigFace", ""));
                }
            }

            if (bond.Type == BondType.VAR)
            {
                ValidateBondFormula("CurrFace", bond.CurrFace, allVectors, allVariables);
            }
            else
            {
                if (string.IsNullOrEmpty(bond.CurrFace)) bond.CurrFace = "0";

                if (!Tools.IsDecimalString(bond.CurrFace))
                {
                    validationErrors.Add(new ValidationMessage(bond, "CurrFace", bond.CurrFace));
                }
            }
        }

        private void ValidateBondFormula(string property, string value,
            IDictionary<string, Vector> allVectors,
            IDictionary<string, Variable> allVariables)
        {
            if (Tools.IsDecimalString(value)) return;

            if (string.IsNullOrEmpty(value))
            {
                validationErrors.Add(new ValidationMessage(bond, property, value));
            }
            else
            {
                string[] operands = Tools.GetOperandsFromExpression(value);
                foreach (string operand in operands)
                {
                    if (Tools.IsDecimalString(operand))
                    {
                        continue;
                    }
                    if (allVectors.ContainsKey(operand))
                    {
                        usedVectors.Add(allVectors[operand]);
                        continue;
                    }
                    if (allVariables.ContainsKey(operand))
                    {
                        usedVariables.Add(allVariables[operand]);
                        continue;
                    }

                    validationErrors.Add(new ValidationMessage(bond, property, operand));
                }
            }
        }

        public void ValuateBond(IDictionary<string, object>[] variables)
        {
            for (int i = 0; i < Constants.RESULT_MONTHS; i++)
            {
                this.MarketPriceValues[i] = GetComputedValue(bond.MarketPrice, variables[i]);
                this.CouponValues[i] = GetComputedPercent(bond.Coupon, variables[i]);
                if (bond.Type == BondType.GROWTH)
                    this.Growthes[i] = GetComputedPercent(bond.Growth, variables[i]);

                if (bond.IsModel)
                {
                    if (bond.Type == BondType.VAR)
                    {
                        this.ThCurrFaceValues[i] = GetComputedValue(bond.TheoreticalCurrFace, variables[i]);
                    }
                    else
                    {
                        this.ThCurrFaceValues[i] = bond.OrigFace ?? 0;
                    }
                    this.CdrValues[i] = GetComputedPercent(bond.CDR, variables[i]);
                    this.CprValues[i] = GetComputedPercent(bond.CPR, variables[i]);
                    this.LgdValues[i] = GetComputedPercent(bond.LGD, variables[i]);
                }

                this.CurrFace[i] = GetComputedValue(bond.CurrFace, variables[i]);

                if (validationErrors.Count > 0) return;
            }
        }

        private decimal GetComputedPercent(string value, IDictionary<string, object> variables)
        {
            decimal tempDecimal;
            if (decimal.TryParse(value, out tempDecimal))
            {
                tempDecimal = tempDecimal / 100;
            }
            else
            {
                tempDecimal = ComputeExpression(value, variables);
            }
            return tempDecimal;
        }

        private decimal GetComputedValue(string value, IDictionary<string, object> variables)
        {
            decimal tempDecimal;
            if (!decimal.TryParse(value, out tempDecimal))
            {

                tempDecimal = ComputeExpression(value, variables);
            }
            return tempDecimal;
        }

        private decimal ComputeExpression(string value, IDictionary<string, object> variables)
        {
            decimal expressionValue = 0;
            try
            {
                string expression = Tools.PreProcessExpression(value);
                expressionValue = (decimal)parser.Evaluate(expression, variables);
            }
            catch (Exception ex)
            {
                Log.Warn(ex);
                validationErrors.Add(new ValidationMessage(string.Format("{0} in expression '{1}'", ex.Message, value)));
            }

            return expressionValue;
        }

        public Bond Bond
        {
            get { return bond; }
        }

        public ModelData ModelData
        {
            get { return modelData; }
        }

        //// todo: check usages
        //public virtual decimal[] CurrFace
        //{
        //    get { return CurrFace; }
        //}

        public virtual decimal[] MarketPriceValues
        {
            get { return marketPriceValues; }
        }

        public virtual decimal[] Growthes
        {
            get { return growthes; }
        }

        public virtual decimal[] CouponValues
        {
            get { return couponValues; }
        }

        public decimal[] ThCurrFaceValues
        {
            get { return thCurrFaceValues; }
        }

        public decimal[] CprValues
        {
            get { return cprValues; }
        }

        public decimal[] CdrValues
        {
            get { return cdrValues; }
        }

        public decimal[] LgdValues
        {
            get { return lgdValues; }
        }

        public decimal[] CurrFace
        {
            get { return currFace; }
        }

        public decimal[] BookPrice
        {
            get { return bookPrice; }
        }

        public decimal[] Income
        {
            get { return income; }
        }

        public decimal[] GainLoses
        {
            get { return this.gainLoses; }
        }

        public decimal[] Payment
        {
            get { return this.payment; }
        }

        public decimal[] OtherComprehensiveIncome
        {
            get { return this.otherComprehensiveIncome; }
        }

        public void Run(int step)
        {
            currFaceFunction(this, step);
            if (this.bond.IsModel) this.ModelData.Iteration(step);
            bookPriceFunction(this, step);
            mtmFunction(this, step);
            incomeFunction(this, step);
        }

        //public XmlSchema GetSchema()
        //{
        //    return null;
        //}

        //public void ReadXml(XmlReader reader)
        //{
        //    reader.Read(); // Skip <BondData>
        //}

        //public void WriteXml(XmlWriter writer)
        //{            
        //}
    }
}