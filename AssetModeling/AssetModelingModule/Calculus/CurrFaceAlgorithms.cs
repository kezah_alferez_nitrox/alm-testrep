using System;

namespace AssetModelingModule.Calculus
{
    public class CurrFaceAlgorithms
    {
        public static void InBalance_Bullet(BondData data, int i)
        {
            decimal[] currFace = data.Bond.IsModel ? data.ThCurrFaceValues : data.CurrFace;

            decimal duration = CommonRules.GetDuration(data.Bond);
            currFace[i] = i < duration + 1 ? currFace[0] : 0;
        }

        public static void InBalance_Growth(BondData data, int i)
        {
            decimal[] currFace = data.Bond.IsModel ? data.ThCurrFaceValues : data.CurrFace;

            if (i > 0)
            {
                decimal duration = CommonRules.GetDuration(data.Bond);

                currFace[i] = i < duration + 1
                                       ? (currFace[i - 1] * (1 + data.Growthes[i] / 12))
                                       : 0;
            }
        }

        public static void InBalance_LinearAmort(BondData data, int i)
        {
            decimal[] currFace = data.Bond.IsModel ? data.ThCurrFaceValues : data.CurrFace;

            if (i > 0)
            {
                decimal duration = CommonRules.GetDuration(data.Bond);
                currFace[i] = i < duration + 1
                               ? currFace[0] * (duration + 1 - i) / (duration + 1)
                               : 0;
            }
        }

        public static void InBalance_Var(BondData data, int i)
        {
            decimal[] currFace = data.Bond.IsModel ? data.ThCurrFaceValues : data.CurrFace;

            decimal duration = CommonRules.GetDuration(data.Bond);
            currFace[i] = i < duration + 1 ? currFace[i] : 0;
        }

        public static void InBalance_CstPay(BondData data, int i)
        {
            decimal[] currFace = data.Bond.IsModel ? data.ThCurrFaceValues : data.CurrFace;

            if (i > 0)
            {
                decimal duration = CommonRules.GetDuration(data.Bond);
                if (i < duration + 1)
                {
                    decimal crd = GetCrd(data, i);
                    currFace[i] = currFace[0] * crd;
                }
                else
                {
                    currFace[i] = 0;
                }
            }
        }

        private static decimal GetCrd(BondData data, int i)
        {
            decimal r = data.CouponValues[i];
            int d = (int)CommonRules.GetDuration(data.Bond);

            // v2
            //double rp1 = (double)(r /12 / p + 1);
            //decimal factor = (decimal)((Math.Pow(rp1, d) - Math.Pow(rp1, i)) / (Math.Pow(rp1, d) - 1));

            // v3
            //double r1 = (double)(r + 1);
            //double up = 1 - Math.Pow(r1, (i - d) / 12);
            //double down = 1 - Math.Pow(r1, -d / 12);
            //decimal factor = (decimal)Math.Max((up / down), 0);

            // v4
            //double rp1 = (double)(1 + r / p);
            //double dp = 1.0 * d / p;
            //double ip = 1.0 * i / p;
            //decimal powd = (decimal) Math.Pow(rp1, dp);
            //decimal powi = (decimal) Math.Pow(rp1, ip);
            //decimal crd = (powd - powi) / (powd - 1);
            //data.CurrFace[i] = data.CurrFace[i-1] * crd;

            // Serge: MAX(0;(1-(1+r)^((t-d)/12))/(1-(1+r)^(-d/12)))
            // Pascale: ((1+r/p)^(d/p)-(1+r/p)^(t/p))/((1+r/p)^(d/p)-1)

            //v5
            double r1 = (double)(1 + r);
            double up = 1 - Math.Pow(r1, (i - d) / 12.0);
            double down = 1 - Math.Pow(r1, -d / 12.0);
            return (down == 0) ? 1 : (decimal)Math.Max((up / down), 0);
        }

        public static void CashAdj(BondData data, int i)
        {
            // data.CurrFace[i] = data.CurrFace[i];
        }

        public static void Treasury(BondData data, int i)
        {
            // data.CurrFace[i] = i == 0 ? 0 : data.CurrFace[i];
        }

        public static void Roll(BondData data, int i)
        {
            // data.CurrFace[i] = i == 0 ? 0 : data.CurrFace[i];
        }

        public static void Impairments(BondData data, int i)
        {
            // nothing
        }

        public static void Receivable(BondData data, int i)
        {
            // nothing
        }

        public static void Reserves(BondData data, int i)
        {
            // nothing    
        }

        public static void OtherComprehensiveIncome(BondData data, int i)
        {
            // nothing 
        }
    }
}