using AssetModelingModule.Domain;

namespace AssetModelingModule.Calculus
{
    public delegate void CalculusFunction(BondData data, int i);

    public interface IAlgorithmConfiguration
    {
        CalculusFunction GetCurrFaceFunction(Bond bond);
        CalculusFunction GetBookPriceFunction(Bond bond);
        CalculusFunction GetMtMFunction(Bond bond);
        CalculusFunction GetIncomeFunction(Bond bond);
    }
}