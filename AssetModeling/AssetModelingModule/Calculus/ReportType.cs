using Eu.AlterSystems.ASNetLib.Core;

namespace AssetModelingModule.Calculus
{
    public enum ReportType
    {
        [EnumDescription("Annually")] Annually,
        [EnumDescription("Annually as calendar year")] AnnuallyAsCalendarYear,
        [EnumDescription("Quarterly")] Quarterly,
        [EnumDescription("Quarterly as calendar year")] QuarterlyAsCalendarYear,
        [EnumDescription("Monthly")] Monthly,
        // todo [EnumDescription("User Defined")] UserDefined,
        // todo [EnumDescription("Summary")] Summary,
    }
}