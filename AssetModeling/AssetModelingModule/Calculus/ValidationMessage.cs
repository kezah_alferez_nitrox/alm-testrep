using System;
using AssetModelingModule.Domain;
using AssetModelingModule.Resources.Language;

namespace AssetModelingModule.Calculus
{
    public class ValidationMessage
    {
        private readonly string customMessage;
        private readonly FXRate rate;
        private readonly Bond bond;
        private readonly VariableValue variableValue;
        private readonly Dividend dividend;
        private readonly OtherItemAssum otherItemAssum;
        private readonly string property;
        private readonly string value;
        private readonly ValidationMessageType type;

        public ValidationMessage(Bond target, string property, string value)
        {
            this.bond = target;
            this.property = property;
            this.value = value;
            type = ValidationMessageType.Bond;
        }

        public ValidationMessage(VariableValue target, string property)
        {
            this.variableValue = target;
            this.property = property;
            type = ValidationMessageType.VariableValue;
        }

        public ValidationMessage(Dividend target)
        {
            this.dividend = target;
            type = ValidationMessageType.Dividend;
        }

        public ValidationMessage(OtherItemAssum target)
        {
            this.otherItemAssum = target;
            type = ValidationMessageType.OtherItemAssumption;
        }

        public ValidationMessage(FXRate rate)
        {
            this.rate = rate;
            type = ValidationMessageType.FxRate;
        }

        public ValidationMessage(string message)
        {
            customMessage = message;
            type = ValidationMessageType.Custom;
        }

        public ValidationMessageType Type
        {
            get { return type; }
        }

        public string Property
        {
            get { return property; }
        }

        public Bond Bond
        {
            get { return bond; }
        }

        public VariableValue VariableValue
        {
            get { return variableValue; }
        }

        public Dividend Dividend
        {
            get { return dividend; }
        }

        public OtherItemAssum OtherItemAssum
        {
            get { return otherItemAssum; }
        }

        public string Message
        {
            get
            {
                switch (type)
                {
                    case ValidationMessageType.Bond:
                        return string.Format(Labels.ValidationMessage_Message_, property, value,
                                             bond.ALMIDDisplayed);
                    case ValidationMessageType.VariableValue:
                        return string.Format(Labels.ValidationMessage_Operand_Message_,
                                             property, variableValue.Variable.Name, variableValue.Value,
                                             variableValue.Scenario.Name);
                    case ValidationMessageType.Dividend:
                        return string.Format(Labels.ValidationMessage_Message_Unknown_vector___0___used_by_Dividend___1__,
                                             dividend.Amount, dividend.Name);
                    case ValidationMessageType.OtherItemAssumption:
                        return string.Format(Labels.ValidationMessage_Message_Invalid___0___used_by_Other_Item_Assumption___1__,
                                             otherItemAssum.Vect, otherItemAssum.Descrip);
                    case ValidationMessageType.FxRate:
                        return string.Format(Labels.ValidationMessage_Message_Invalid_value_for_exchage_rate___0___1__,
                            rate.From == null ? "null" : rate.From.Symbol,
                            rate.To == null ? "null" : rate.To.Symbol);
                    case ValidationMessageType.Custom:
                        return customMessage;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }
    }

    public enum ValidationMessageType
    {
        Bond,
        VariableValue,
        Dividend,
        OtherItemAssumption,
        FxRate,
        Custom
    }
}