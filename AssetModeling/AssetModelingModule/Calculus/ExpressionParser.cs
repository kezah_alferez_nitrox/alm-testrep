using System;
using System.Collections.Generic;
using NCalc;

// using NCalc;

namespace AssetModelingModule.Calculus
{
    public class ExpressionParser
    {
        public decimal Evaluate(string formula, IDictionary<string, object> parameters)
        {
            //string[] keyArray = new string[parameters.Count];
            //parameters.Keys.CopyTo(keyArray, 0);
            //foreach (var key in keyArray)
            //{
            //    object value = parameters[key];
            //    if(value != null && value.GetType() == typeof(decimal))
            //    {
            //        parameters[key] = Convert.ToDouble(value);
            //    }
            //}

            // using http://ncalc.codeplex.com/
            Expression expression = new Expression(formula);
            expression.Parameters = (Dictionary<string, object>)parameters;
            object result = expression.Evaluate();

            return Convert.ToDecimal(result);
        }
    }
}