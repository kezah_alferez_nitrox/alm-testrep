using System;
using System.Collections.Generic;
using AssetModelingModule.Domain;

namespace AssetModelingModule.Calculus
{
    public class AlgorithmConfiguration : IAlgorithmConfiguration
    {
        private readonly Dictionary<BondAttrib, Dictionary<BondType, CalculusFunction>> currFaceFunctions = new Dictionary<BondAttrib, Dictionary<BondType, CalculusFunction>>();
        private readonly Dictionary<BondAttrib, Dictionary<BondType, CalculusFunction>> bookPriceFunctions = new Dictionary<BondAttrib, Dictionary<BondType, CalculusFunction>>();
        private readonly Dictionary<BondAttrib, Dictionary<BondType, CalculusFunction>> mtmFunctions = new Dictionary<BondAttrib, Dictionary<BondType, CalculusFunction>>();
        private readonly Dictionary<BondAttrib, Dictionary<BondType, CalculusFunction>> incomeFunctions = new Dictionary<BondAttrib, Dictionary<BondType, CalculusFunction>>();

        public AlgorithmConfiguration()
        {
            foreach (BondAttrib bondAttrib in Enum.GetValues(typeof(BondAttrib)))
            {
                currFaceFunctions[bondAttrib] = new Dictionary<BondType, CalculusFunction>();
                bookPriceFunctions[bondAttrib] = new Dictionary<BondType, CalculusFunction>();
                incomeFunctions[bondAttrib] = new Dictionary<BondType, CalculusFunction>();
            }
            InBalanceConfiguration();
            CashAdjConfiguration();
            RollConfiguration();
            NotionalConfiguration();
            TreasuryConfiguration();
            ImpairmentsConfiguration();
            ReceivableConfiguration();
            ReservesConfiguration();
            OtherComprehensiveIncomeConfiguration();
        }

        private void InBalanceConfiguration()
        {
            currFaceFunctions[BondAttrib.InBalance][BondType.BULLET] = CurrFaceAlgorithms.InBalance_Bullet;
            currFaceFunctions[BondAttrib.InBalance][BondType.GROWTH] = CurrFaceAlgorithms.InBalance_Growth;
            currFaceFunctions[BondAttrib.InBalance][BondType.LINEAR_AMORT] = CurrFaceAlgorithms.InBalance_LinearAmort;
            currFaceFunctions[BondAttrib.InBalance][BondType.VAR] = CurrFaceAlgorithms.InBalance_Var;
            currFaceFunctions[BondAttrib.InBalance][BondType.CST_PAY] = CurrFaceAlgorithms.InBalance_CstPay;

            bookPriceFunctions[BondAttrib.InBalance][BondType.BULLET] = BookPriceAlgorithms.InBalance_Bullet;
            bookPriceFunctions[BondAttrib.InBalance][BondType.GROWTH] = BookPriceAlgorithms.InBalance_Growth;
            bookPriceFunctions[BondAttrib.InBalance][BondType.LINEAR_AMORT] = BookPriceAlgorithms.InBalance_LinearAmort;
            bookPriceFunctions[BondAttrib.InBalance][BondType.VAR] = BookPriceAlgorithms.InBalance_Var;
            bookPriceFunctions[BondAttrib.InBalance][BondType.CST_PAY] = BookPriceAlgorithms.InBalance_CstPay;

            incomeFunctions[BondAttrib.InBalance][BondType.BULLET] = IncomeAlgorithms.InBalance_Bullet;
            incomeFunctions[BondAttrib.InBalance][BondType.GROWTH] = IncomeAlgorithms.InBalance_Growth;
            incomeFunctions[BondAttrib.InBalance][BondType.LINEAR_AMORT] = IncomeAlgorithms.InBalance_LinearAmort;
            incomeFunctions[BondAttrib.InBalance][BondType.VAR] = IncomeAlgorithms.InBalance_Var;
            incomeFunctions[BondAttrib.InBalance][BondType.CST_PAY] = IncomeAlgorithms.InBalance_CstPay;
        }

        private void CashAdjConfiguration()
        {
            foreach (BondType type in Enum.GetValues(typeof(BondType)))
            {
                currFaceFunctions[BondAttrib.CashAdj][type] = CurrFaceAlgorithms.CashAdj;
                bookPriceFunctions[BondAttrib.CashAdj][type] = BookPriceAlgorithms.CashAdj;
                incomeFunctions[BondAttrib.CashAdj][type] = IncomeAlgorithms.CashAdj;
            }
        }

        private void RollConfiguration()
        {
            currFaceFunctions[BondAttrib.Roll][BondType.BULLET] = CurrFaceAlgorithms.InBalance_Bullet;
            currFaceFunctions[BondAttrib.Roll][BondType.GROWTH] = CurrFaceAlgorithms.InBalance_Growth;
            currFaceFunctions[BondAttrib.Roll][BondType.LINEAR_AMORT] = CurrFaceAlgorithms.InBalance_LinearAmort;
            currFaceFunctions[BondAttrib.Roll][BondType.VAR] = CurrFaceAlgorithms.InBalance_Var;
            currFaceFunctions[BondAttrib.Roll][BondType.CST_PAY] = CurrFaceAlgorithms.InBalance_CstPay;

            bookPriceFunctions[BondAttrib.Roll][BondType.BULLET] = BookPriceAlgorithms.InBalance_Bullet;
            bookPriceFunctions[BondAttrib.Roll][BondType.GROWTH] = BookPriceAlgorithms.InBalance_Growth;
            bookPriceFunctions[BondAttrib.Roll][BondType.LINEAR_AMORT] = BookPriceAlgorithms.InBalance_LinearAmort;
            bookPriceFunctions[BondAttrib.Roll][BondType.VAR] = BookPriceAlgorithms.InBalance_Var;
            bookPriceFunctions[BondAttrib.Roll][BondType.CST_PAY] = BookPriceAlgorithms.InBalance_CstPay;

            incomeFunctions[BondAttrib.Roll][BondType.BULLET] = IncomeAlgorithms.InBalance_Bullet;
            incomeFunctions[BondAttrib.Roll][BondType.GROWTH] = IncomeAlgorithms.InBalance_Growth;
            incomeFunctions[BondAttrib.Roll][BondType.LINEAR_AMORT] = IncomeAlgorithms.InBalance_LinearAmort;
            incomeFunctions[BondAttrib.Roll][BondType.VAR] = IncomeAlgorithms.InBalance_Var;
            incomeFunctions[BondAttrib.Roll][BondType.CST_PAY] = IncomeAlgorithms.InBalance_CstPay;
            //foreach (BondType type in Enum.GetValues(typeof(BondType)))
            //{
            //    currFaceFunctions[BondAttrib.Roll][type] = CurrFaceAlgorithms.Roll;
            //    bookPriceFunctions[BondAttrib.Roll][type] = bookPriceFunctions[BondAttrib.InBalance][type];
            //    incomeFunctions[BondAttrib.Roll][type] = incomeFunctions[BondAttrib.InBalance][type];
            //}
        }

        private void NotionalConfiguration()
        {
            incomeFunctions[BondAttrib.Notional][BondType.BULLET] = IncomeAlgorithms.InBalance_Bullet;
            incomeFunctions[BondAttrib.Notional][BondType.GROWTH] = IncomeAlgorithms.InBalance_Growth;
            incomeFunctions[BondAttrib.Notional][BondType.LINEAR_AMORT] = IncomeAlgorithms.InBalance_LinearAmort;
            incomeFunctions[BondAttrib.Notional][BondType.VAR] = IncomeAlgorithms.InBalance_Var;
            incomeFunctions[BondAttrib.Notional][BondType.CST_PAY] = IncomeAlgorithms.InBalance_CstPay;
        }

        private void TreasuryConfiguration()
        {
            foreach (BondType type in Enum.GetValues(typeof(BondType)))
            {
                currFaceFunctions[BondAttrib.Treasury][type] = CurrFaceAlgorithms.Treasury;
                bookPriceFunctions[BondAttrib.Treasury][type] = BookPriceAlgorithms.Treasury;
                incomeFunctions[BondAttrib.Treasury][type] = incomeFunctions[BondAttrib.InBalance][type];
            }
        }

        private void ImpairmentsConfiguration()
        {
            foreach (BondType type in Enum.GetValues(typeof(BondType)))
            {
                currFaceFunctions[BondAttrib.Impairments][type] = CurrFaceAlgorithms.Impairments;
                bookPriceFunctions[BondAttrib.Impairments][type] = BookPriceAlgorithms.Impairments;
                incomeFunctions[BondAttrib.Impairments][type] = incomeFunctions[BondAttrib.InBalance][type];
            }
        }

        private void ReceivableConfiguration()
        {
            foreach (BondType type in Enum.GetValues(typeof(BondType)))
            {
                currFaceFunctions[BondAttrib.Receivable][type] = CurrFaceAlgorithms.Receivable;
                bookPriceFunctions[BondAttrib.Receivable][type] = BookPriceAlgorithms.Receivable;
                incomeFunctions[BondAttrib.Receivable][type] = incomeFunctions[BondAttrib.InBalance][type];
            }
        }

        private void ReservesConfiguration()
        {
            foreach (BondType type in Enum.GetValues(typeof(BondType)))
            {
                currFaceFunctions[BondAttrib.Reserves][type] = CurrFaceAlgorithms.Reserves;
                bookPriceFunctions[BondAttrib.Reserves][type] = BookPriceAlgorithms.Reserves;
                incomeFunctions[BondAttrib.Reserves][type] = incomeFunctions[BondAttrib.InBalance][type];
            }
        }

        private void OtherComprehensiveIncomeConfiguration()
        {
            foreach (BondType type in Enum.GetValues(typeof(BondType)))
            {
                currFaceFunctions[BondAttrib.OtherComprehensiveIncome][type] = CurrFaceAlgorithms.OtherComprehensiveIncome;
                bookPriceFunctions[BondAttrib.OtherComprehensiveIncome][type] = BookPriceAlgorithms.OtherComprehensiveIncome;
                incomeFunctions[BondAttrib.OtherComprehensiveIncome][type] = incomeFunctions[BondAttrib.InBalance][type];
            }
        }

        public CalculusFunction GetCurrFaceFunction(Bond bond)
        {
            return TryGetCalculusFunction(currFaceFunctions, bond);
        }

        public CalculusFunction GetBookPriceFunction(Bond bond)
        {
            return TryGetCalculusFunction(bookPriceFunctions, bond);
        }

        public CalculusFunction GetMtMFunction(Bond bond)
        {
            return TryGetCalculusFunction(mtmFunctions, bond);
        }

        public CalculusFunction GetIncomeFunction(Bond bond)
        {
            return TryGetCalculusFunction(incomeFunctions, bond);
        }

        private static CalculusFunction TryGetCalculusFunction(Dictionary<BondAttrib, Dictionary<BondType, CalculusFunction>> configuration, Bond bond)
        {
            if (bond == null) return Noop;

            if (configuration.ContainsKey(bond.Attrib) && configuration[bond.Attrib].ContainsKey(bond.Type))
            {
                return configuration[bond.Attrib][bond.Type];
            }

            // todo for demo proposes only
            return Noop;
        }

        private static void Noop(BondData data, int i)
        {
        }
    }
}