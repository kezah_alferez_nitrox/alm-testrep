using System;
using System.Collections.Generic;
using AssetModelingModule.Domain;
using AssetModelingModule.Resources.Language;
using Iesi.Collections.Generic;

namespace AssetModelingModule.Calculus
{
    public class Util
    {
        public static void ComputeVariableValues(Scenario scenario,
                                                  ExpressionParser parser,
                                                  IDictionary<string, object>[] vectorValues,
                                                  IDictionary<string, decimal>[] variableValues,
                                                  IEnumerable<Variable> variablesToCompute,
                                                  ICollection<ValidationMessage> validationErrors)
        {
            List<VariableValue> scenarioVariableValues = new List<VariableValue>(scenario.VariableValues);

            for (int i = 0; i < Constants.RESULT_MONTHS; i++)
            {
                variableValues[i] = new Dictionary<string, decimal>(30, StringComparer.CurrentCultureIgnoreCase);
            }

            foreach (Variable variable in variablesToCompute)
            {
                Variable variableCopy = variable;
                VariableValue variableValue =
                    scenarioVariableValues.Find(v => v.Variable == variableCopy);

                if (variableValue != null && variableValue.ValueIsArray())
                {
                    decimal[] arrayValue = variableValue.GetArrayValue();
                    for (int i = 0; i < arrayValue.Length; i++)
                    {
                        variableValues[i][variable.Name] = arrayValue[i];
                    }
                    continue;
                }

                for (int i = 0; i < Constants.RESULT_MONTHS; i++)
                {
                    decimal expressionValue;
                    try
                    {
                        string expression = Tools.PreProcessExpression(variableValue.Value);
                        expressionValue = (decimal)parser.Evaluate(expression, vectorValues[i]);
                    }
                    catch (Exception ex)
                    {
                        Log.Warn(ex);
                        if (variableValue == null)
                            validationErrors.Add(new ValidationMessage(Labels.Util_ComputeVariableValues_The_ + variable.Name + Labels.Util_ComputeVariableValues__variable_does_not_have_a_formula_in_ + scenario.Name + Labels.Util_ComputeVariableValues__scenario_));
                        else
                            validationErrors.Add(new ValidationMessage(variableValue.Variable.Name + " : " + ex.Message));
                        break;
                    }
                    variableValues[i][variable.Name] = expressionValue;
                }
            }
        }

        public static void ComputeVectorValues(DateTime valuationDate,
                                                IDictionary<string, object>[] vectorValues,
                                                IEnumerable<Vector> vectorsToCompute)
        {
            for (int i = 0; i < Constants.RESULT_MONTHS; i++)
            {
                vectorValues[i] = new Dictionary<string, object>(32, StringComparer.CurrentCultureIgnoreCase);
            }

            foreach (Vector vector in vectorsToCompute)
            {
                List<VectorPoint> points = new List<VectorPoint>(vector.VectorPoints);
                points.Sort(delegate(VectorPoint vp1, VectorPoint vp2) { return vp1.Date.CompareTo(vp2.Date); });

                decimal[] values = GetVectorValues(valuationDate, points);
                for (int i = 0; i < values.Length; i++)
                {
                    vectorValues[i][vector.Name] = values[i];
                }
            }
        }

        public static decimal[] GetVectorValues(DateTime valuationDate, IList<VectorPoint> points)
        {
            decimal[] values = new decimal[Constants.RESULT_MONTHS];

            if (points == null || points.Count <= 1)
            {
                return values;
            }

            int monthStart = Tools.GetMonthNumber(valuationDate);
            int monthEnd = monthStart + Constants.RESULT_MONTHS;

            FillVectorValuesBeforeStartMonth(points, values, monthStart);
            FillVectorValuesBetweenStartAndEndMonths(points, values, monthStart, monthEnd);
            FillVectorValesAfterEndMonth(points, values, monthStart, monthEnd);

            return values;
        }

        public static void FillVectorValuesBeforeStartMonth(IList<VectorPoint> points, decimal[] values, int monthStart)
        {
            int monthPoint0 = Tools.GetMonthNumber(points[0].Date);
            if (monthStart < monthPoint0)
            {
                for (int i = monthStart; i < monthPoint0; i++)
                {
                    values[i - monthStart] = points[0].Value;
                }
            }
        }

        public static void FillVectorValuesBetweenStartAndEndMonths(IList<VectorPoint> points, decimal[] values,
                                                                    int monthStart, int monthEnd)
        {
            for (int pointIndex = 0; pointIndex < points.Count - 1; pointIndex++)
            {
                int monthPoint1 = Tools.GetMonthNumber(points[pointIndex].Date);
                int monthPoint2 = Tools.GetMonthNumber(points[pointIndex + 1].Date);

                monthPoint1 = Math.Max(monthStart, monthPoint1);
                monthPoint2 = Math.Min(monthEnd, monthPoint2);

                for (int i = monthPoint1; i < monthPoint2; i++)
                {
                    values[i - monthStart] = points[pointIndex].Value;
                }
            }
        }

        public static void FillVectorValesAfterEndMonth(IList<VectorPoint> points, decimal[] values, int monthStart,
                                                        int monthEnd)
        {
            int monthPointLast = Tools.GetMonthNumber(points[points.Count - 1].Date);
            if (monthPointLast <= monthEnd)
            {
                for (int i = monthPointLast; i < monthEnd; i++)
                {
                    values[i - monthStart] = points[points.Count - 1].Value;
                }
            }
        }

        public static void ValidateVariables(
            ICollection<Scenario> selectedScenarios,
            IEnumerable<Variable> variablesToValidate,
            IDictionary<string, Vector> allVectors,
            ISet<Vector> usedVectors,
            ICollection<ValidationMessage> validationErrors)
        {
            foreach (Variable variable in variablesToValidate)
            {
                foreach (VariableValue value in variable.VariableValues)
                {
                    if (!selectedScenarios.Contains(value.Scenario)) continue;

                    if (String.IsNullOrEmpty(value.Value))
                    {
                        validationErrors.Add(new ValidationMessage(value, null));
                        continue;
                    }

                    if (value.ValueIsArray())
                    {
                        continue;
                    }

                    string[] operands = Tools.GetOperandsFromExpression(value.Value);
                    foreach (string rawOperand in operands)
                    {
                        string operand = rawOperand.Trim();
                        decimal tempDecimal;
                        if (Decimal.TryParse(operand, out tempDecimal)) continue;

                        if (allVectors.ContainsKey(operand))
                        {
                            usedVectors.Add(allVectors[operand]);
                        }
                        else
                        {
                            validationErrors.Add(new ValidationMessage(value, operand));
                        }
                    }
                }
            }
        }
    }
}