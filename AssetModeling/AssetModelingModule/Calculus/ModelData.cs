namespace AssetModelingModule.Calculus
{
    public class ModelData
    {
        private readonly BondData productData;

        private readonly decimal[] nrr = new decimal[Constants.RESULT_MONTHS];
        private readonly decimal[] default_ = new decimal[Constants.RESULT_MONTHS];
        private readonly decimal[] paidInterest = new decimal[Constants.RESULT_MONTHS];
        private readonly decimal[] normalReinbursement = new decimal[Constants.RESULT_MONTHS];
        private readonly decimal[] prepayments = new decimal[Constants.RESULT_MONTHS];
        private readonly decimal[] losesImpairement = new decimal[Constants.RESULT_MONTHS];
        private readonly decimal[] recovery = new decimal[Constants.RESULT_MONTHS];

        private readonly decimal[] cumulativeDelinquencies = new decimal[Constants.RESULT_MONTHS];
        private readonly decimal[] cumulativeLoses = new decimal[Constants.RESULT_MONTHS];
        private readonly decimal[] cumulativeCPR = new decimal[Constants.RESULT_MONTHS];

        private readonly decimal[] cumulativeDelinquenciesOnInitialNominal = new decimal[Constants.RESULT_MONTHS];
        private readonly decimal[] cumulativeLosesOnInitialNominal = new decimal[Constants.RESULT_MONTHS];
        private readonly decimal[] cumulativeCPROnInitialNominal = new decimal[Constants.RESULT_MONTHS];

        public ModelData(BondData productData)
        {
            this.productData = productData;
        }

        public void Iteration(int i)
        {
            if (i > 0)
            {
                nrr[i] = this.productData.ThCurrFaceValues[i - 1] == 0
                             ? 0
                             : 1 - this.productData.ThCurrFaceValues[i] / this.productData.ThCurrFaceValues[i - 1];
                default_[i] = this.productData.CurrFace[i - 1] * this.productData.CdrValues[i] / 12;
                paidInterest[i] = (this.productData.CurrFace[i - 1] - default_[i]) * this.productData.CouponValues[i - 1] / 12;
                normalReinbursement[i] = nrr[i] * (this.productData.CurrFace[i - 1] - default_[i]);
                prepayments[i] = (this.productData.CurrFace[i - 1] - default_[i] - normalReinbursement[i]) * this.productData.CprValues[i] /
                                 12;
                losesImpairement[i] = -1 * default_[i] * this.productData.LgdValues[i];
                recovery[i] = default_[i] + losesImpairement[i];
                if (this.productData.CurrFace[i] != 0)
                {
                    
                }
                this.productData.CurrFace[i] = this.productData.CurrFace[i - 1] - normalReinbursement[i] - prepayments[i] - default_[i];

                cumulativeDelinquencies[i] = cumulativeDelinquencies[i - 1] + default_[i];
                cumulativeLoses[i] = cumulativeLoses[i - 1] + losesImpairement[i];
                cumulativeCPR[i] = cumulativeCPR[i - 1] + prepayments[i];

                if (this.productData.CurrFace[0] != 0)
                {
                    cumulativeDelinquenciesOnInitialNominal[i] = cumulativeDelinquencies[i] / this.productData.CurrFace[0];
                    cumulativeLosesOnInitialNominal[i] = -cumulativeLoses[i] / this.productData.CurrFace[0];
                    cumulativeCPROnInitialNominal[i] = cumulativeCPR[i] / this.productData.CurrFace[0];
                }
            }

            // Console.WriteLine("{0}: CF={1:N2} TCF={2:N2}", i, bondData.CurrFace[i], bondData.ThCurrFaceValues[i]);
        }

        public decimal[] LosesImpairement
        {
            get { return losesImpairement; }
        }

        public decimal[] TheoreticalCurrentFace
        {
            get { return this.productData.ThCurrFaceValues; }
        }

        public decimal[] InterestRate
        {
            get { return this.productData.CouponValues; }
        }

        public decimal[] CurrentNominal
        {
            get { return this.productData.CurrFace; }
        }

        public decimal[] Default
        {
            get { return default_; }
        }

        public decimal[] InterestIncomes
        {
            get { return paidInterest; }
        }

        public decimal[] NormalReimbursement
        {
            get { return normalReinbursement; }
        }

        public decimal[] Prepayments
        {
            get { return prepayments; }
        }

        public decimal[] Recovery
        {
            get { return recovery; }
        }

        public decimal[] LossesImpairments
        {
            get { return losesImpairement; }
        }

        public decimal[] TotalReimbursementsAndRecovery
        {
            get { return losesImpairement; } // todo
        }

        public decimal[] CPR
        {
            get
            {
                return this.productData.CprValues;
            }
        }

        public decimal[] CDR
        {
            get { return this.productData.CdrValues; }
        }

        public decimal[] LGD
        {
            get { return this.productData.LgdValues; }
        }

        public decimal[] NRR
        {
            get { return nrr; }
        }

        public decimal[] CumulativeDelinquenciesOnInitialNominal
        {
            get { return cumulativeDelinquenciesOnInitialNominal; }
        }

        public decimal[] CumulativeLosesOnInitialNominal
        {
            get { return cumulativeLosesOnInitialNominal; }
        }

        public decimal[] CumulativeCprOnInitialNominal
        {
            get { return cumulativeCPROnInitialNominal; }
        }
    }
}