using System;
using System.Collections.Generic;
using AssetModelingModule.Domain;

namespace AssetModelingModule.Calculus
{
    public class ModelAnalysisData
    {
        private readonly BondData productData;
        private readonly ExpressionParser parser;
        private readonly AlgorithmConfiguration algorithmConfiguration;

        private readonly IDictionary<string, Vector> allVectors = new Dictionary<string, Vector>(30, StringComparer.CurrentCultureIgnoreCase);
        private readonly IDictionary<string, Variable> allVariables = new Dictionary<string, Variable>(30, StringComparer.CurrentCultureIgnoreCase);

        private readonly IDictionary<string, object>[] vectorValues =
            new IDictionary<string, object>[Constants.RESULT_MONTHS];
        private readonly IDictionary<string, decimal>[] variableValues =
            new IDictionary<string, decimal>[Constants.RESULT_MONTHS];
        private readonly IDictionary<string, object>[] variableAndVectorValues =
            new IDictionary<string, object>[Constants.RESULT_MONTHS];

        private readonly List<ValidationMessage> validationErrors = new List<ValidationMessage>();
        private List<Scenario> selectedScenarios;
        private DateTime valuationDate;

        public ModelAnalysisData(Bond bond)
        {
            parser = new ExpressionParser();
            algorithmConfiguration = new AlgorithmConfiguration();
            this.productData = new BondData(bond, parser, algorithmConfiguration);

            Initialise();
        }

        private void Initialise()
        {
            Vector[] vectors = Vector.FindAll();
            foreach (Vector vector in vectors)
            {
                allVectors.Add(vector.Name, vector);
            }

            Variable[] variables = Variable.FindSelected();
            foreach (Variable variable in variables)
            {
                allVariables.Add(variable.Name, variable);
            }

            selectedScenarios = new List<Scenario>(Scenario.FindSelected());

            valuationDate = Param.ValuationDate ?? DateTime.Today;
        }

        public List<ValidationMessage> RunSimulation(Scenario scenario)
        {
            this.productData.ValidateBond(allVectors, allVariables);
            if (this.productData.ValidationErrors.Count > 0) return this.productData.ValidationErrors;

            Util.ValidateVariables(selectedScenarios, this.productData.UsedVariables, allVectors, this.productData.UsedVectors, validationErrors);
            if (validationErrors.Count > 0) return validationErrors;

            Util.ComputeVectorValues(valuationDate, vectorValues, this.productData.UsedVectors);
            Util.ComputeVariableValues(scenario, parser, vectorValues, variableValues, this.productData.UsedVariables, validationErrors);
            if (validationErrors.Count > 0) return validationErrors;

            InitVariableAndVectorValues();

            this.productData.ValuateBond(variableAndVectorValues);

            for (int i = 0; i < Constants.RESULT_MONTHS; i++)
            {
                this.productData.Run(i);
            }

            return this.productData.ValidationErrors;
        }

        private void InitVariableAndVectorValues()
        {
            for (int i = 0; i < Constants.RESULT_MONTHS; i++)
            {
                variableAndVectorValues[i] = new Dictionary<string, object>(32, StringComparer.CurrentCultureIgnoreCase);

                foreach (KeyValuePair<string, object> vectorValue in vectorValues[i])
                {
                    variableAndVectorValues[i][vectorValue.Key] = vectorValue.Value;
                }

                foreach (KeyValuePair<string, decimal> variableValue in variableValues[i])
                {
                    variableAndVectorValues[i][variableValue.Key] = variableValue.Value;
                }
            }
        }

        public ModelData ModelData
        {
            get { return this.productData.ModelData; }
        }
    }
}