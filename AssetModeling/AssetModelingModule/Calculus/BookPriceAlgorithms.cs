using AssetModelingModule.Domain;

namespace AssetModelingModule.Calculus
{
    public class BookPriceAlgorithms
    {


        public static void InBalance_Bullet(BondData data, int i)
        {
            if (data.Bond.Accounting == BondAccounting.HTM ||
                data.Bond.Accounting == BondAccounting.LAndR)
            {
                if (data.Bond.Duration != 0)
                    data.MarketPriceValues[i] = data.Bond.BookPrice.GetValueOrDefault() + (decimal)(i / data.Bond.Duration.GetValueOrDefault() * (100 - data.Bond.BookPrice.GetValueOrDefault()));
                else
                    data.MarketPriceValues[i] = data.Bond.BookPrice.GetValueOrDefault();
            }
            data.BookPrice[i] = data.MarketPriceValues[i];
        }
        //{
        //    computeMarketPrice(data, i);
        //    if (i == 0)
        //    {
        //        data.BookPrice[0] = data.Bond.BookPrice.GetValueOrDefault();
        //    }
        //    else
        //    {
        //        if (data.Bond.Accounting == BondAccounting.HFT)
        //        {
        //            data.BookPrice[i] = data.MarketPriceValues[i];
        //        }
        //        else
        //        {
        //            decimal duration = CommonRules.GetDuration(data.Bond);

        //            if (i < duration + 1)
        //            {
        //                data.BookPrice[i] = data.BookPrice[0] +
        //                                    (duration == 0 ? 0 : (100 - data.BookPrice[0]) / duration * i);
        //            }
        //            else
        //            {
        //                data.BookPrice[i] = 0;
        //            }
        //        }
        //    }

        //}

        public static void InBalance_Growth(BondData data, int i)
        {
            InBalance_Bullet(data, i);
        }
        //{
        //    computeMarketPrice(data, i);
        //    if (i == 0)
        //    {
        //        data.BookPrice[0] = data.Bond.BookPrice.GetValueOrDefault();
        //    }
        //    else
        //    {
        //        decimal duration = CommonRules.GetDuration(data.Bond);

        //        if (data.Bond.Accounting == BondAccounting.HFT && !string.IsNullOrEmpty(data.Bond.MarketPrice))
        //        {
        //            data.BookPrice[i] = (i < duration + 1) ? data.MarketPriceValues[i] : 0;
        //        }
        //        else
        //        {
        //            InBalance_Var(data, i);
        //        }
        //    }
        //}

        public static void InBalance_LinearAmort(BondData data, int i)
        {
            InBalance_Bullet(data, i);
        }
        //{
        //    computeMarketPrice(data, i);
        //    if (i == 0)
        //    {
        //        data.BookPrice[0] = data.Bond.BookPrice.GetValueOrDefault();
        //    }
        //    else
        //    {
        //        decimal duration = CommonRules.GetDuration(data.Bond);
        //        if (i >= duration + 1)
        //        {
        //            data.BookPrice[i] = 0;
        //            return;
        //        }

        //        if (data.Bond.Accounting == BondAccounting.HFT)
        //        {
        //            data.BookPrice[i] = data.MarketPriceValues[i];
        //        }
        //        else
        //        {
        //            InBalance_Var(data, i);
        //        }
        //    }
        //}
        public static void InBalance_Var(BondData data, int i)
        {
            InBalance_Bullet(data, i);
        }

        //public static void InBalance_Var(BondData data, int i)
        //{
        //    computeMarketPrice(data, i);
        //    if (i == 0)
        //    {
        //        data.BookPrice[0] = data.Bond.BookPrice.GetValueOrDefault();
        //    }
        //    else
        //    {
        //        decimal bookPrice_1 = data.BookPrice[i - 1];
        //        decimal currFace = data.CurrFace[i];
        //        decimal currFace_1 = data.CurrFace[i - 1];

        //        if (currFace_1 == 0) return; // todo

        //        decimal duration = CommonRules.GetDuration(data.Bond);

        //        data.BookPrice[i] = (i < duration + 1) ? bookPrice_1 + (100 - bookPrice_1) * (currFace_1 - currFace) / currFace_1 : 0;
        //    }
        //}

        public static void InBalance_CstPay(BondData data, int i)
        {
            InBalance_Var(data, i);
        }

        public static void CashAdj(BondData data, int i)
        {
            data.BookPrice[i] = 100m;
        }

        public static void Treasury(BondData data, int i)
        {
            data.BookPrice[i] = 100m;
        }

        public static void Impairments(BondData data, int i)
        {
            data.BookPrice[i] = 100m;
        }

        public static void Receivable(BondData data, int i)
        {
            data.BookPrice[i] = 100m;
        }

        public static void Reserves(BondData data, int i)
        {
            data.BookPrice[i] = 100m;
        }

        public static void OtherComprehensiveIncome(BondData data, int i)
        {
            data.BookPrice[i] = 100m;
        }
    }
}