using AssetModelingModule.Domain;

namespace AssetModelingModule.Calculus
{
    public class IncomeAlgorithms
    {
        public static void InBalance_Bullet(BondData data, int i)
        {
            decimal duration = CommonRules.GetDuration(data.Bond);
            //MTU
            //if (data.Bond.Attrib == BondAttrib.Roll && i == 14)
            //{
            //    int iii = 0;
            //}

            data.OtherComprehensiveIncome[i] = 0;
            if ((data.Bond.Category.Type == CategoryType.Normal) ||
                (data.Bond.Category.Type == CategoryType.Roll))
            {

                if ((i > 0) && (i <= duration))
                {
                    data.OtherComprehensiveIncome[i] = (data.Bond.Category.BalanceType == BalanceType.Asset ? -1 : 1) * (data.BookPrice[i] - data.BookPrice[i - 1]) * data.CurrFace[i - 1] / 100;
                }
                else
                    data.OtherComprehensiveIncome[i] = 0;

                if (i == duration+1)
                {
                    //on solde la r�serve d'OCI
                    data.OtherComprehensiveIncome[i] = 0;
                    for (int x = 0; x <= duration; x++)
                    {
                        data.OtherComprehensiveIncome[i] += (data.Bond.Category.BalanceType == BalanceType.Asset ? -1 : 1) * data.OtherComprehensiveIncome[x];
                    }
                }

               
            }
            //MTU-Fin
            if (data.Bond.Accounting == BondAccounting.AFS)
            {
                //MTU 20091029 data.OtherComprehensiveIncome[i] = data.MarketPriceValues[i] - data.MarketPriceValues[i - 1];
                data.GainLoses[i] = 0;
                if (i == duration)
                {
                    for (int x = 0; x <= duration; x++)
                    {
                        data.GainLoses[i] += (data.Bond.Category.BalanceType == BalanceType.Asset ? -1 : 1) * data.OtherComprehensiveIncome[x];
                    }
                }
            }
            else
            {
                data.GainLoses[i] = 0;
                if (i > 0 && i <= duration)
                {
                    //MTU data.GainLoses[i] = data.MarketPriceValues[i] - data.MarketPriceValues[i - 1];
                    data.GainLoses[i] = (data.Bond.Category.BalanceType == BalanceType.Asset ? -1 : 1) * data.OtherComprehensiveIncome[i];
                }

            }

            if (data.Bond.Accounting == BondAccounting.HFT)
            {
                if (i == 0)
                {
                    data.Income[0] = data.CouponValues[0] * data.CurrFace[0] / 12;
                }
                else
                {
                    data.Income[i] = i < duration + 1
                                         ? data.CouponValues[i] * data.CurrFace[i] / 12
                                         : 0;

                }
            }
            else if (data.Bond.Accounting == BondAccounting.AFS)
            {
                if (i == 0)
                {
                    data.Income[0] = data.CouponValues[0] * data.CurrFace[0] / 12;
                }
                else
                {
                    data.Income[i] = i < duration + 1
                                         ? data.CouponValues[i] * data.CurrFace[i] / 12
                                         : 0;



                }
            }
            else
            {
                if (i == 0)
                {
                    data.Income[0] = 0;
                }
                else
                {
                    data.Income[i] = i < duration + 1 ? data.CouponValues[i - 1] * data.CurrFace[i - 1] / 12 : 0;
                }
            }
        }

        public static void InBalance_Growth(BondData data, int i)
        {
            InBalance_Bullet(data, i);
        }

        public static void InBalance_LinearAmort(BondData data, int i)
        {
            InBalance_Bullet(data, i);
        }

        public static void InBalance_Var(BondData data, int i)
        {
            InBalance_Bullet(data, i);
        }

        public static void InBalance_CstPay(BondData data, int i)
        {
            InBalance_Bullet(data, i);
        }

        public static void CashAdj(BondData data, int i)
        {
            data.Income[i] = 0;
        }
    }
}