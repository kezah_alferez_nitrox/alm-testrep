using System;
using System.Collections.Generic;
using AssetModelingModule.BusinessLogic;
using AssetModelingModule.Domain;

namespace AssetModelingModule.Calculus
{
    public class ScenarioData
    {
        private IDictionary<Category, decimal[]> categoryBookValue = new Dictionary<Category, decimal[]>();
        private IDictionary<Category, decimal[]> categoryIncome = new Dictionary<Category, decimal[]>();
        private IDictionary<Category, decimal[]> categoryYield = new Dictionary<Category, decimal[]>();

        private IDictionary<string, decimal[]> otherItemAssumptions = new Dictionary<string, decimal[]>();
        private IDictionary<Category, decimal[]> impairementData = new Dictionary<Category, decimal[]>();
        private decimal[] otherItemAssumptionsSum = new decimal[Constants.RESULT_MONTHS];
        private IDictionary<Dividend, decimal[]> dividendsAmount = new Dictionary<Dividend, decimal[]>();

        private decimal[] totalEquity;
        private decimal[] totalAssets;
        private decimal[] impairments;
        private decimal[] incomeBeforeImpairmentCharge;
        private decimal[] netInterestIncome;
        private decimal[] gainLosesOnFinancialInstrument;
        private decimal[] incomePreTax;
        private decimal[] taxes;
        private decimal[] netIncome;

        private IList<BondData> bondSimulationData = new List<BondData>();

        private readonly Scenario scenario;
        private readonly IDictionary<string, object>[] vectorValues;

        private ScenarioData()
        {
        }

        public ScenarioData(Scenario scenario,
                            IDictionary<string, object>[] vectorValues)
        {
            this.scenario = scenario;
            this.vectorValues = vectorValues;
        }


        public void Compute(IList<BondData> bondDatas, OtherItemAssum[] oia, Dividend[] div, IDictionary<Currency, decimal[]> fxRates, decimal standardTax)
        {
            totalEquity = new decimal[Constants.RESULT_MONTHS];
            totalAssets = new decimal[Constants.RESULT_MONTHS];
            impairments = new decimal[Constants.RESULT_MONTHS];
            incomeBeforeImpairmentCharge = new decimal[Constants.RESULT_MONTHS];
            netInterestIncome = new decimal[Constants.RESULT_MONTHS];
            gainLosesOnFinancialInstrument = new decimal[Constants.RESULT_MONTHS];
            incomePreTax = new decimal[Constants.RESULT_MONTHS];
            taxes = new decimal[Constants.RESULT_MONTHS];
            netIncome = new decimal[Constants.RESULT_MONTHS];

            this.bondSimulationData = bondDatas;

            this.ComputeOtherItemsAssumption(oia);

            this.ComputeBonds(fxRates);

            this.ComputeNetInterestIncome();
            this.ComputeGainLosesOnFinancialInstrument();
            this.ComputeIncomeBeforeImpairmentCharge();
            this.ComputeIncomePreTax();
            this.ComputeTaxes(standardTax);
            this.ComputeNetIncome();
            this.ComputeDividends(div);
            this.ComputeImpairmentData();
        }

        private void ComputeDividends(Dividend[] div)
        {
            foreach (Dividend dividend in div)
            {
                decimal amount;
                bool isAmount = false;
                if (decimal.TryParse(dividend.Amount, out amount))
                {
                    amount = amount / 100;
                    isAmount = true;
                }

                dividendsAmount[dividend] = new decimal[Constants.RESULT_MONTHS];
                for (int i = 0; i < Constants.RESULT_MONTHS; i++)
                {
                    decimal value;
                    if (isAmount)
                    {
                        value = amount;
                    }
                    else
                    {
                        value = (decimal)vectorValues[i][dividend.Amount];
                    }
                    dividendsAmount[dividend][i] = value;
                }
            }
        }

        public void AggregateCategories(Category[] categories)
        {
            foreach (Category category in categoryBookValue.Keys)
            {
                ComputeCategoryYeld(category);
            }

            foreach (Category category in categories)
            {
                if (category.Parent == null)
                {
                    AggregateCategory(category);
                }
            }
        }

        private void ComputeCategoryYeld(Category category)
        {
            categoryYield[category] = new decimal[Constants.RESULT_MONTHS];
            for (int i = 0; i < Constants.RESULT_MONTHS; i++)
            {
                if (categoryBookValue[category][i] != 0)
                {
                    categoryYield[category][i] = categoryIncome[category][i] / categoryBookValue[category][i];
                }
            }
        }

        private void AggregateCategory(Category category)
        {
            if (categoryBookValue.ContainsKey(category)) return; // already aggregated

            categoryBookValue[category] = new decimal[Constants.RESULT_MONTHS];
            categoryIncome[category] = new decimal[Constants.RESULT_MONTHS];

            foreach (Category child in category.Children)
            {
                AggregateCategory(child);
                for (int i = 0; i < Constants.RESULT_MONTHS; i++)
                {
                    categoryBookValue[category][i] += categoryBookValue[child][i];
                    categoryIncome[category][i] += categoryIncome[child][i];
                }

                ComputeCategoryYeld(child);
            }
        }

        private void ComputeBonds(IDictionary<Currency, decimal[]> fxRates)
        {
            if (scenario == null) return;

            RunScenarioForEachBond(fxRates);
            ConvertByFxRates(fxRates);
            AggregateBonds(fxRates);
        }

        private void RunScenarioForEachBond(IDictionary<Currency, decimal[]> fxRates)
        {
            BondData treasuryBondData = null;
            BondData impairmentsBondData = null;
            BondData receivableBondData = null;
            BondData reservesBondData = null;
            BondData otherComprehensiveIncomeBondData = null;
            BondData assetCashAdjBondData = null;
            BondData liabilitiesCashAdjBondData = null;
            IList<BondData> rollBondDatas = new List<BondData>();

            foreach (BondData bondData in bondSimulationData)
            {
                Bond bond = bondData.Bond;
                if (bond.Attrib == BondAttrib.Roll)
                {
                    rollBondDatas.Add(bondData);
                }
                if (bond.Attrib == BondAttrib.Treasury) treasuryBondData = bondData;
                if (bond.Attrib == BondAttrib.Impairments) impairmentsBondData = bondData;
                if (bond.Attrib == BondAttrib.Receivable) receivableBondData = bondData;
                if (bond.Attrib == BondAttrib.Reserves) reservesBondData = bondData;
                if (bond.Attrib == BondAttrib.OtherComprehensiveIncome) otherComprehensiveIncomeBondData = bondData;
                if (bond.Attrib == BondAttrib.CashAdj)
                {
                    if (bond.Category.BalanceType == BalanceType.Asset) assetCashAdjBondData = bondData;
                    if (bond.Category.BalanceType == BalanceType.Liability) liabilitiesCashAdjBondData = bondData;
                }
            }

            for (int i = 0; i < Constants.RESULT_MONTHS; i++)
            {

                RunScenarioForBondsByAttrib(i, BondAttrib.InBalance);

                RunScenarioForBondsByAttrib(i, BondAttrib.Notional);

                RunScenarioForBondsByAttrib(i, BondAttrib.Roll);
                UpdateRollBonds(rollBondDatas, i);

                if (treasuryBondData != null)
                {
                    UpdateTreasuryBonds(treasuryBondData, i);
                    RunScenarioForBondsByAttrib(i, BondAttrib.Treasury);
                }

                if (impairmentsBondData != null)
                {
                    UpdateImparmentsBonds(impairmentsBondData, i);
                    RunScenarioForBondsByAttrib(i, BondAttrib.Impairments);
                }

                if (receivableBondData != null && treasuryBondData != null)
                {
                    UpdateReceivableBonds(receivableBondData, i);
                    RunScenarioForBondsByAttrib(i, BondAttrib.Receivable);
                }

                if (reservesBondData != null)
                {
                    UpdateReservesBonds(reservesBondData, i);
                    RunScenarioForBondsByAttrib(i, BondAttrib.Reserves);
                }

                if (otherComprehensiveIncomeBondData != null)
                {
                    UpdateOtherComprehensiveIncomeBonds(otherComprehensiveIncomeBondData, i);
                    RunScenarioForBondsByAttrib(i, BondAttrib.OtherComprehensiveIncome);
                }
                


                if (assetCashAdjBondData != null && liabilitiesCashAdjBondData != null)
                {
                    UpdateCashAdjBonds(assetCashAdjBondData, liabilitiesCashAdjBondData, fxRates, i);
                    RunScenarioForBondsByAttrib(i, BondAttrib.CashAdj);
                }
            }
        }

        private void UpdateReceivableBonds(BondData receivableBondData, int i)
        {
            decimal income = GetIncome(i);

            if (i == 0)
            {
                receivableBondData.CurrFace[0] = 0;
                return;
            }

            decimal totalPayments = 0;
            foreach (BondData bondData in bondSimulationData)
            {
                totalPayments += bondData.Payment[i];
            }

            receivableBondData.CurrFace[i] = receivableBondData.CurrFace[i - 1] + income - totalPayments;
        }

        private void UpdateReservesBonds(BondData reservesBondData, int i)
        {
            decimal income = GetIncome(i);

            if (i == 0)
            {
                reservesBondData.CurrFace[0] = 0;
                return;
            }

            reservesBondData.CurrFace[i] = reservesBondData.CurrFace[i - 1] + income + impairments[i] + otherItemAssumptionsSum[i]; ;
        }

        private void UpdateOtherComprehensiveIncomeBonds(BondData otherComprehensiveIncomeBondData, int i)
        {
            //MTU : todo : pkoi bullet trait� diff�rement
            if (i == 0)
            {
                otherComprehensiveIncomeBondData.CurrFace[0] = 0;
                return;
            }

            decimal delta = 0;
            foreach (BondData bondData in bondSimulationData)
            {
                delta += bondData.OtherComprehensiveIncome[i];
            }
            otherComprehensiveIncomeBondData.CurrFace[i] = otherComprehensiveIncomeBondData.CurrFace[i - 1] - delta;
        }

        private decimal GetIncome(int i)
        {
            decimal income = 0;
            foreach (BondData bondData in bondSimulationData)
            {
                decimal sign = bondData.Bond.Category.BalanceType == BalanceType.Asset ? 1 : -1;
                income += sign * bondData.Income[i];
            }
            return income;
        }

        private void UpdateImparmentsBonds(BondData impairmentsBondData, int i)
        {
            if (i == 0)
            {
                impairments[0] = 0;
                return;
            }

            decimal deltaImpairments = 0;
            foreach (BondData bondData in bondSimulationData)
            {
                if (bondData.Bond.IsModel)
                {
                    deltaImpairments += bondData.ModelData.LosesImpairement[i];
                }
            }
            impairments[i] = deltaImpairments;
            impairmentsBondData.CurrFace[i] = impairmentsBondData.CurrFace[i - 1] + deltaImpairments;
        }

        private void UpdateRollBonds(IList<BondData> rollBondDatas, int step)
        {
            foreach (BondData rollBondData in rollBondDatas)
            {
                if (step == 0)
                {
                    rollBondData.CurrFace[0] = 0;
                    continue;
                }

                decimal sumDeltaCF = 0;
                foreach (BondData bondData in bondSimulationData)
                {
                    Bond bond = bondData.Bond;
                    if (bond.RollSpec == rollBondData.Bond.ALMIDDisplayed)
                    {
                        decimal val = bondData.CurrFace[step - 1] - bondData.CurrFace[step];
                        sumDeltaCF += val;
                    }
                }

                rollBondData.CurrFace[step] += sumDeltaCF;
            }
            
        }

        private void UpdateCashAdjBonds(BondData assetCashAdjBondData, BondData liabilitiesCashAdjBondData, IDictionary<Currency, decimal[]> fxRates, int step)
        {
            decimal balance = 0;
            foreach (BondData bondData in bondSimulationData)
            {
                if (bondData.Bond.Exclude || bondData.Bond.Attrib == BondAttrib.Notional) continue;

                int sign = bondData.Bond.Category.BalanceType == BalanceType.Asset ? 1 : -1;

                decimal rate = GetBondRate(bondData, step, fxRates);
                decimal convertedValue = bondData.BookPrice[step] * rate;
                decimal bookValue = bondData.CurrFace[step] / 100 * convertedValue;
                balance += sign * bookValue;
            }

            decimal cashAdjAssetValue = 0;
            decimal cashAdjLiabilitiesValue = 0;

            BalanceBL.AjustCashAdjBonds(ref cashAdjAssetValue, ref cashAdjLiabilitiesValue, balance);

            assetCashAdjBondData.CurrFace[step] = cashAdjAssetValue;
            liabilitiesCashAdjBondData.CurrFace[step] = cashAdjLiabilitiesValue;
        }

        private void UpdateTreasuryBonds(BondData treasuryBondData, int step)
        {
            if (step == 0) return;

            decimal treasury = 0;
            foreach (BondData bondData in bondSimulationData)
            {
                treasury += bondData.Payment[step];

                if ((step == CommonRules.GetDuration(bondData.Bond) + 1) &&
                    String.IsNullOrEmpty(bondData.Bond.RollSpec))
                {
                    treasury += (bondData.Bond.Category.BalanceType == BalanceType.Asset ? 1 : -1) * (bondData.BookPrice[step - 1] * bondData.CurrFace[step - 1] / 100);
                }
            }
            treasury += otherItemAssumptionsSum[step];

            treasuryBondData.CurrFace[step] = treasuryBondData.CurrFace[step - 1] + treasury;
            //MTU : la tr�so ne peut pas �tre n�gative, le Cash Adj ajustera automatiquement
            if (treasuryBondData.CurrFace[step] < 0) treasuryBondData.CurrFace[step] = 0;
        }

        private void RunScenarioForBondsByAttrib(int step, BondAttrib attrib)
        {
            foreach (BondData bondData in bondSimulationData)
            {
                if (bondData.Bond.Attrib == attrib)
                {
                    bondData.Run(step);
                }
            }

            foreach (BondData bondData in bondSimulationData)
            {
                int period = 12 / (int)bondData.Bond.Period;
                int sign = bondData.Bond.Category.BalanceType == BalanceType.Asset ? 1 : -1;

                if (step != 0 && step % period == 0) // payement date
                {
                    decimal periodPayment = GetPeriodPayment(bondData, step, period);
                    bondData.Payment[step] = sign * periodPayment;
                }
            }
        }

        private static decimal GetPeriodPayment(BondData data, int end, int period)
        {
            decimal sum = 0;
            for (int i = end - period + 1; i <= end; i++)
            {
                sum += data.Income[i];
            }
            return sum;
        }

        private void ConvertByFxRates(IDictionary<Currency, decimal[]> fxRates)
        {
            foreach (BondData bondData in bondSimulationData)
            {
                for (int i = 0; i < Constants.RESULT_MONTHS; i++)
                {
                    decimal fxRate = GetBondRate(bondData, i, fxRates);

                    bondData.CurrFace[i] = bondData.CurrFace[i] * fxRate;
                    bondData.Income[i] = bondData.Income[i] * fxRate;
                }
            }
        }

        private void AggregateBonds(IDictionary<Currency, decimal[]> fxRates)
        {
            foreach (BondData bondData in bondSimulationData)
            {
                decimal[] categoryBookValueValue;
                decimal[] categoryIncomeValue;

                GetValueArrays(bondData, out categoryBookValueValue, out categoryIncomeValue);



                for (int i = 0; i < Constants.RESULT_MONTHS; i++)
                {
                    //MTU 20091029 : int�gration de la conversion de BookValue dans la currency courante
                    decimal bookValue;
                    if (i == 0 && bondData.Bond.Attrib == BondAttrib.Notional)
                        bookValue = 0;
                    else
                        bookValue = i == 0 ? bondData.Bond.BookValue.GetValueOrDefault() * GetBondRate(bondData, 0, fxRates) : bondData.CurrFace[i] * bondData.BookPrice[i] / 100;

                    decimal income = bondData.Income[i];

                    categoryBookValueValue[i] += bookValue;
                    categoryIncomeValue[i] += income;

                    if (bondData.Bond.Category.BalanceType == BalanceType.Asset)
                    {
                        totalAssets[i] += bookValue;
                    }
                    else if (bondData.Bond.Category.BalanceType == BalanceType.Equity)
                    {
                        totalEquity[i] += bookValue;
                    }
                }
            }
        }

        private static decimal GetBondRate(BondData bondData, int i, IDictionary<Currency, decimal[]> fxRates)
        {
            if (bondData.Bond.Currency == null || !fxRates.ContainsKey(bondData.Bond.Currency))
            {
                return 1;
            }
            return fxRates[bondData.Bond.Currency][i];
        }

        private void GetValueArrays(BondData bondData,
                            out decimal[] categoryBookValueValue,
                            out decimal[] categoryIncomeValue)
        {
            if (!categoryBookValue.TryGetValue(bondData.Bond.Category, out categoryBookValueValue))
            {
                categoryBookValueValue = new decimal[Constants.RESULT_MONTHS];
                categoryBookValue[bondData.Bond.Category] = categoryBookValueValue;
            }

            if (!categoryIncome.TryGetValue(bondData.Bond.Category, out categoryIncomeValue))
            {
                categoryIncomeValue = new decimal[Constants.RESULT_MONTHS];
                categoryIncome[bondData.Bond.Category] = categoryIncomeValue;
            }
        }

        private void ComputeIncomePreTax()
        {
            for (int i = 0; i < Constants.RESULT_MONTHS; i++)
            {
                incomePreTax[i] = incomeBeforeImpairmentCharge[i] + impairments[i];
            }
        }

        private void ComputeTaxes(decimal standardTax)
        {
            for (int i = 0; i < Constants.RESULT_MONTHS; i++)
            {
                decimal tax = incomePreTax[i] * standardTax / 100;
                taxes[i] = (tax < 0) ? 0 : tax;
                taxes[i] = -taxes[i];
            }
        }

        private void ComputeImpairmentData()
        {
            for (int i = 0; i < Constants.RESULT_MONTHS; i++)
            {
                foreach (BondData bondData in bondSimulationData)
                {
                    if (bondData.Bond.IsModel)
                    {
                        if (i == 0)
                        {
                            if (!impairementData.ContainsKey(bondData.Bond.Category))
                            {
                                impairementData[bondData.Bond.Category] = new decimal[Constants.RESULT_MONTHS];
                            }
                        }
                        else
                        {
                            impairementData[bondData.Bond.Category][i] = bondData.ModelData.LosesImpairement[i - 1];
                        }
                    }
                }
            }
        }

        private void ComputeNetIncome()
        {
            for (int i = 0; i < Constants.RESULT_MONTHS; i++)
            {
                netIncome[i] = incomePreTax[i] + taxes[i];
            }
        }

        private void ComputeNetInterestIncome()
        {
            foreach (KeyValuePair<Category, decimal[]> pair in categoryIncome)
            {
                for (int i = 0; i < Constants.RESULT_MONTHS; i++)
                {
                    int sign = (pair.Key.BalanceType == BalanceType.Asset) ? 1 : -1;

                    netInterestIncome[i] += sign * pair.Value[i];
                }
            }
        }

        private void ComputeIncomeBeforeImpairmentCharge()
        {
            for (int i = 0; i < Constants.RESULT_MONTHS; i++)
            {
                incomeBeforeImpairmentCharge[i] = netInterestIncome[i] + otherItemAssumptionsSum[i] + gainLosesOnFinancialInstrument[i];
            }
        }

        private void ComputeGainLosesOnFinancialInstrument()
        {
            for (int i = 0; i < Constants.RESULT_MONTHS; i++)
            {
                foreach (BondData bondData in bondSimulationData)
                {
                    gainLosesOnFinancialInstrument[i] += bondData.GainLoses[i];
                }
            }
        }

        private void ComputeOtherItemsAssumption(OtherItemAssum[] oia)
        {
            foreach (OtherItemAssum assum in oia)
            {
                otherItemAssumptions[assum.Descrip] = new decimal[Constants.RESULT_MONTHS];

                for (int i = 0; i < Constants.RESULT_MONTHS; i++)
                {
                    decimal value = assum.Values[i];

                    otherItemAssumptions[assum.Descrip][i] = value;
                    otherItemAssumptionsSum[i] += value;
                }
            }
        }

        public decimal[] GetBookValue(Category category)
        {
            decimal[] bookPrice;
            return categoryBookValue.TryGetValue(category, out bookPrice)
                       ? bookPrice
                       : new decimal[Constants.RESULT_MONTHS];
        }

        public decimal[] GetIncome(Category category)
        {
            decimal[] income;
            return categoryIncome.TryGetValue(category, out income)
                       ? income
                       : new decimal[Constants.RESULT_MONTHS];
        }

        public decimal[] GetInterestIncomeBeforeImpairment()
        {
            return incomeBeforeImpairmentCharge;
        }

        public decimal[] GetImpairment()
        {
            return impairments;
        }

        public decimal[] GetNetInterestIncome()
        {
            return netInterestIncome;
        }

        public decimal[] GetGainLosesOnFinancialInstrument()
        {
            return gainLosesOnFinancialInstrument;
        }

        public decimal[] GetIncomePreTax()
        {
            return incomePreTax;
        }

        public decimal[] GetTaxes()
        {
            return taxes;
        }

        public decimal[] GetNetIncome()
        {
            return netIncome;
        }

        public decimal[] GetTotalEquity()
        {
            return totalEquity;
        }

        public decimal[] GetTotalAssets()
        {
            return totalAssets;
        }

        public IDictionary<string, decimal[]> GetOtherItemAssumptions()
        {
            return otherItemAssumptions;
        }

        public IDictionary<Dividend, decimal[]> GetDividendAmounts()
        {
            return dividendsAmount;
        }

        public IDictionary<Category, decimal[]> GetImpairementData()
        {
            return impairementData;
        }

        //public XmlSchema GetSchema()
        //{
        //    return null;
        //}

        //public void ReadXml(XmlReader reader)
        //{
        //    reader.Read(); // Skip parent tag (<Value>)
        //    categoryBookValue = DeSerializeDictionary<Category, decimal[]>(reader);
        //    categoryIncome = DeSerializeDictionary<Category, decimal[]>(reader);
        //    categoryYield = DeSerializeDictionary<Category, decimal[]>(reader);
        //    otherItemAssumptions = DeSerializeDictionary<string, decimal[]>(reader);
        //    impairementData = DeSerializeDictionary<Category, decimal[]>(reader);
        //    dividendsAmount = DeSerializeDictionary<Dividend, decimal[]>(reader);
        //    categoryBookValue = DeSerializeDictionary<Category, decimal[]>(reader);
        //    categoryBookValue = DeSerializeDictionary<Category, decimal[]>(reader);

        //    otherItemAssumptionsSum = DeSerializeDecimalArray(reader);
        //    totalEquity = DeSerializeDecimalArray(reader);
        //    totalAssets = DeSerializeDecimalArray(reader);
        //    impairments = DeSerializeDecimalArray(reader);
        //    incomeBeforeImpairmentCharge =DeSerializeDecimalArray( reader);
        //    netInterestIncome =DeSerializeDecimalArray(reader);
        //    gainLosesOnFinancialInstrument =DeSerializeDecimalArray( reader);
        //    incomePreTax =DeSerializeDecimalArray( reader);
        //    taxes =DeSerializeDecimalArray( reader);
        //    netIncome =DeSerializeDecimalArray( reader);

        //    bondSimulationData = DeSerializeList<BondData>(reader);
        //}

        //private static IList<T> DeSerializeList<T>(XmlReader reader)
        //{
        //    XmlSerializer serializer = new XmlSerializer(typeof(List<T>));
        //    return (IList<T>) serializer.Deserialize(reader);
        //}

        //private static decimal[] DeSerializeDecimalArray(XmlReader reader)
        //{
        //    XmlSerializer serializer = new XmlSerializer(typeof(decimal[]));
        //    return (decimal[]) serializer.Deserialize(reader);
        //}

        //private static IDictionary<TK, TV> DeSerializeDictionary<TK, TV>(XmlReader reader)
        //{
        //    DictionarySerializer<TK, TV> serializer = new DictionarySerializer<TK, TV>();
        //    return serializer.Deserialize(reader);
        //}

        //public void WriteXml(XmlWriter writer)
        //{
        //    SerializeDictionary(categoryBookValue, writer);
        //    SerializeDictionary(categoryIncome, writer);
        //    SerializeDictionary(categoryYield, writer);
        //    SerializeDictionary(otherItemAssumptions, writer);
        //    SerializeDictionary(impairementData, writer);
        //    SerializeDictionary(dividendsAmount, writer);
        //    SerializeDictionary(categoryBookValue, writer);
        //    SerializeDictionary(categoryBookValue, writer);

        //    SerializeDecimalArray(otherItemAssumptionsSum, writer);
        //    SerializeDecimalArray(totalEquity, writer);
        //    SerializeDecimalArray(totalAssets, writer);
        //    SerializeDecimalArray(impairments, writer);
        //    SerializeDecimalArray(incomeBeforeImpairmentCharge, writer);
        //    SerializeDecimalArray(netInterestIncome, writer);
        //    SerializeDecimalArray(gainLosesOnFinancialInstrument, writer);
        //    SerializeDecimalArray(incomePreTax, writer);
        //    SerializeDecimalArray(taxes, writer);
        //    SerializeDecimalArray(netIncome, writer);

        //    SerializeList((List<BondData>)bondSimulationData, writer);
        //}

        //private static void SerializeDictionary<TK, TV>(IDictionary<TK, TV> dictionary, XmlWriter writer)
        //{
        //    DictionarySerializer<TK, TV> serializer = new DictionarySerializer<TK, TV>();
        //    serializer.Serialize(dictionary, writer);
        //}

        //private static void SerializeDecimalArray(decimal[] array, XmlWriter writer)
        //{
        //    XmlSerializer serializer = new XmlSerializer(typeof(decimal[]));
        //    serializer.Serialize(writer, array);
        //}

        //private static void SerializeList<T>(List<T> list, XmlWriter writer)
        //{
        //    XmlSerializer serializer = new XmlSerializer(typeof(List<T>));
        //    serializer.Serialize(writer, list);
        //}
    }
}