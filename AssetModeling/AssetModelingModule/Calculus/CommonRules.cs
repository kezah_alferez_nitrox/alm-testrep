using AssetModelingModule.Domain;

namespace AssetModelingModule.Calculus
{
    public class CommonRules
    {
        public static decimal GetDuration(Bond bond)
        {
            if (bond == null || !bond.Duration.HasValue)
            {
                return Constants.RESULT_MONTHS - 1;
            }
            else
            {
                return bond.Duration.Value;
            }
        }

        //public static decimal Pmt()
        //{
        //    Financial.Pmt()
        //}
    }
}