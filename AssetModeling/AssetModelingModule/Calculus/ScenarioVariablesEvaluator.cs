using System;
using System.Collections.Generic;
using AssetModelingModule.Domain;
using Iesi.Collections.Generic;

namespace AssetModelingModule.Calculus
{
    public class ScenarioVariablesEvaluator
    {
        private readonly Scenario scenario;

        private readonly ExpressionParser parser = new ExpressionParser();

        private readonly IDictionary<string, Vector> allVectors = new Dictionary<string, Vector>(30, StringComparer.CurrentCultureIgnoreCase);
        private readonly IDictionary<string, Variable> allVariables = new Dictionary<string, Variable>(30, StringComparer.CurrentCultureIgnoreCase);

        private readonly ISet<Vector> usedVectors = new HashedSet<Vector>();

        private readonly IList<BondData> bondDatas = new List<BondData>();

        private readonly IDictionary<string, object>[] vectorValues =
            new IDictionary<string, object>[Constants.RESULT_MONTHS];

        private readonly IDictionary<string, decimal>[] variableValues =
            new IDictionary<string, decimal>[Constants.RESULT_MONTHS];

        private DateTime valuationDate;

        private readonly List<ValidationMessage> validationErrors = new List<ValidationMessage>();

        public ScenarioVariablesEvaluator(Scenario scenario)
        {
            this.scenario = scenario;
        }

        public List<ValidationMessage> ValidationErrors
        {
            get
            {
                return validationErrors;
            }
        }

        public IDictionary<Variable, double[]> VariableValues
        {
            get
            {
                IDictionary<Variable, double[]> values = new Dictionary<Variable, double[]>();

                foreach (Variable variable in allVariables.Values)
                {
                    values.Add(variable, new double[Constants.RESULT_MONTHS]);
                }

                for (int i = 0; i < Constants.RESULT_MONTHS; i++)
                {
                    foreach (KeyValuePair<string, decimal> pair in variableValues[i])
                    {
                        values[allVariables[pair.Key]][i] = (double)pair.Value;
                    }
                }

                return values;
            }
        }

        private void Initialize()
        {
            allVectors.Clear();
            allVariables.Clear();
            usedVectors.Clear();
            validationErrors.Clear();

            Vector[] vectors = Vector.FindAll();
            foreach (Vector vector in vectors)
            {
                allVectors.Add(vector.Name, vector);
            }

            Variable[] variables = Variable.FindSelected();
            foreach (Variable variable in variables)
            {
                allVariables.Add(variable.Name, variable);
            }

            valuationDate = Param.ValuationDate ?? DateTime.Today;
        }

        public void Run()
        {
            Initialize();

            if (validationErrors.Count > 0) return;
            
            Util.ValidateVariables(new[] { scenario }, allVariables.Values, allVectors, usedVectors, validationErrors);
            if (validationErrors.Count > 0) return;

            Util.ComputeVectorValues(valuationDate, vectorValues, usedVectors);
            Util.ComputeVariableValues(scenario, parser, vectorValues, variableValues, allVariables.Values, validationErrors);
            if (validationErrors.Count > 0) return;
        }
    }
}