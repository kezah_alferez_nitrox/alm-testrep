﻿using System.Linq;


namespace Sensitivity_Analysis_Calculator.Calculators
{
    public class Overview
    {
        public double ActivesNPV { get; set; }
        public double PensionersDeferredNPV { get; set; }
        public double ActivesDuration { get; set; }
        public double PensionersDeferredDuration { get; set; }
        public double NPVTotal { get; set; }


        public double[] CurrentInterestRateRiskLiabilitiesActives { get; set; }
        public double[] CurrentInterestRateRiskLiabilitiesPensionersDeferreds { get; set; }
        public double[] CurrentInterestRateRiskLiabilitiesPensionersDeferredsPlusActivities { get; set; }
        public double[] CurrentInterestRateRiskLiabilitiesTotal { get; set; }

        public double CurrentInterestRateRiskLiabilitiesActivesTotal { get; set; }
        public double CurrentInterestRateRiskLiabilitiesPensionersDeferredsTotal { get; set; }
        public double CurrentInterestRateRiskLiabilitiesTotalTotal { get; set; }


        public double[] CurrentInflationRiskLiabilitiesActives { get; set; }
        public double[] CurrentInflationRiskLiabilitiesPensionersDeferreds { get; set; }
        public double[] CurrentInflationRiskLiabilitiesPensionersDeferredsPlusActivities { get; set; }
        public double[] CurrentInflationRiskLiabilitiesTotal { get; set; }

        public double CurrentInflationRiskLiabilitiesActivesTotal { get; set; }
        public double CurrentInflationRiskLiabilitiesPensionersDeferredsTotal { get; set; }
        public double CurrentInflationRiskLiabilitiesTotalTotal { get; set; }


        public double[] CurrentRiskAssetsInflation { get; set; }
        public double[] CurrentRiskAssetsInterestRate { get; set; }
        public double CurrentRiskAssetsInflationTotal { get; set; }
        public double CurrentRiskAssetsInterestRateTotal { get; set; }

        public double[] CurrentSwapPortfolioSensitivitiesInflation { get; set; }
        public double[] CurrentSwapPortfolioSensitivitiesInflationPlusActivities { get; set; }
        public double[] CurrentSwapPortfolioSensitivitiesInterestRate { get; set; }
        public double[] CurrentSwapPortfolioSensitivitiesInterestRatePlusAssets { get; set; }
        public double CurrentSwapPortfolioSensitivitiesInflationTotal { get; set; }
        public double CurrentSwapPortfolioSensitivitiesInterestRateTotal { get; set; }

        public double[] SensitivitiesRequiredSwapsInflation { get; set; }
        public double[] SensitivitiesRequiredSwapsInterestRate { get; set; }
        public double SensitivitiesRequiredSwapsInflationTotal { get; set; }
        public double SensitivitiesRequiredSwapsInterestRateTotal { get; set; }

        public double[] NotionalsRequiredSwapsInflation { get; set; }
        public double[] NotionalsRequiredSwapsInterestRate { get; set; }

        public double[] ResultingTotalPortfolioSensitivityAssetsInflation { get; set; }
        public double[] ResultingTotalPortfolioSensitivityAssetsRates { get; set; }
        public double ResultingTotalPortfolioSensitivityAssetsInflationTotal { get; set; }
        public double ResultingTotalPortfolioSensitivityAssetsRatesTotal { get; set; }

        public double[] PureInflationEffect { get; set; }
        public double[] YrKr
        {
            get
            {
                return new[]
                           {
                               5.0,
                               10,
                               15,
                               20,
                               25,
                               30,
                               35,
                               40,
                               45,
                               50
                           };
            }
            set { YrKr = value; }
        }


        public void Calculate(CalculContext context)
        {
            #region NPV

            ActivesNPV = MathTools.Round(context.IrSensAct.NPV, -6);
            PensionersDeferredNPV = MathTools.Round(context.IrSensPeDe.PensionersDeferred, -6);

            ActivesDuration = context.IrSensAct.PvCfTotalPerCent * 10000;
            PensionersDeferredDuration = context.IrSensPeDe.PvCfTotalPerCent * 10000;

            NPVTotal = ActivesNPV + PensionersDeferredNPV;

            #endregion NPV


            #region Current Interest Rate Risk Liabilities

            {
                var round1 = new double[CurrentInterestRateRiskLiabilitiesActives.Length];
                var round2 = new double[CurrentInterestRateRiskLiabilitiesPensionersDeferreds.Length];

                for (var i = 0; i < round1.Length; i++)
                {
                    round1[i] = MathTools.Round(CurrentInterestRateRiskLiabilitiesActives[i], -3);
                    round2[i] = MathTools.Round(CurrentInterestRateRiskLiabilitiesPensionersDeferreds[i], -3);
                }

                CurrentInterestRateRiskLiabilitiesTotal = ArrayTools.Add(round1, round2);
            }
            
            CurrentInterestRateRiskLiabilitiesActivesTotal =
                CurrentInterestRateRiskLiabilitiesActives.Sum(d => MathTools.Round(d, -3));

            CurrentInterestRateRiskLiabilitiesPensionersDeferredsTotal =
                CurrentInterestRateRiskLiabilitiesPensionersDeferreds.Sum(d => MathTools.Round(d, -3));

            CurrentInterestRateRiskLiabilitiesTotalTotal =
                CurrentInterestRateRiskLiabilitiesTotal.Sum(d => MathTools.Round(d, -3));

            #endregion Current Interest Rate Risk Liabilities


            #region Current Inflation Risk Liabilities

            {
                var round1 = new double[CurrentInflationRiskLiabilitiesActives.Length];
                var round2 = new double[CurrentInflationRiskLiabilitiesPensionersDeferreds.Length];

                for (var i = 0; i < round1.Length; i++)
                {
                    round1[i] = MathTools.Round(CurrentInflationRiskLiabilitiesActives[i], -3);
                    round2[i] = MathTools.Round(CurrentInflationRiskLiabilitiesPensionersDeferreds[i], -3);
                }

                CurrentInflationRiskLiabilitiesTotal = ArrayTools.Add(round1,round2);
            }

            CurrentInflationRiskLiabilitiesActivesTotal =
                CurrentInflationRiskLiabilitiesActives.Sum();

            CurrentInflationRiskLiabilitiesPensionersDeferredsTotal =
                CurrentInflationRiskLiabilitiesPensionersDeferreds.Sum();

            CurrentInflationRiskLiabilitiesTotalTotal =
                CurrentInflationRiskLiabilitiesTotal.Sum(d => MathTools.Round(d, -3));

            #endregion Current Inflation Risk Liabilities


            #region Current Risk Assets Inflation

            CurrentRiskAssetsInflationTotal = CurrentRiskAssetsInflation.Sum();
            CurrentRiskAssetsInterestRateTotal = CurrentRiskAssetsInterestRate.Sum();

            #endregion Current Risk Assets Inflation


            #region Include Current IR Swap Portfolio

            CurrentSwapPortfolioSensitivitiesInterestRateTotal =
                CurrentSwapPortfolioSensitivitiesInterestRate.Sum();
            CurrentSwapPortfolioSensitivitiesInflationTotal =
                CurrentSwapPortfolioSensitivitiesInflation.Sum();

            #endregion Include Current IR Swap Portfolio


            #region Sensitivities Required Swaps

            SensitivitiesRequiredSwapsInflation = new double[CurrentInflationRiskLiabilitiesTotal.Length];
            SensitivitiesRequiredSwapsInterestRate = new double[CurrentInflationRiskLiabilitiesTotal.Length];

            for(var i = 0; i < SensitivitiesRequiredSwapsInflation.Length; i++)
            {
                SensitivitiesRequiredSwapsInflation[i] = 
                    context.Parameters.HedgeRatioLiabilityInflationRisk *
                    CurrentInflationRiskLiabilitiesTotal[i] -
                    CurrentRiskAssetsInflation[i] -
                    CurrentSwapPortfolioSensitivitiesInflation[i];

                SensitivitiesRequiredSwapsInterestRate[i] =
                    context.Parameters.HedgeRatioLiabilityInterestRateRisk *
                    CurrentInterestRateRiskLiabilitiesTotal[i] -
                    CurrentRiskAssetsInterestRate[i] -
                    CurrentSwapPortfolioSensitivitiesInterestRate[i];
            }

            SensitivitiesRequiredSwapsInflationTotal = SensitivitiesRequiredSwapsInflation.Sum();
            SensitivitiesRequiredSwapsInterestRateTotal = SensitivitiesRequiredSwapsInterestRate.Sum();

            #endregion Sensitivities Required Swaps


            #region Notionals Required Swaps

            NotionalsRequiredSwapsInterestRate =
                new double[SensitivitiesRequiredSwapsInflation.Length];
            NotionalsRequiredSwapsInflation = 
                new double[SensitivitiesRequiredSwapsInflation.Length];

            var countFromFiveToFive = 5;

            for(var i = 0; i < NotionalsRequiredSwapsInflation.Length; i++)
            {
                NotionalsRequiredSwapsInflation[i] =
                    SensitivitiesRequiredSwapsInflation[i] /
                                    ((double)10000 * context.TotalLiabScenario.DiscountCurve[countFromFiveToFive] *
                                     ((context.TotalLiabScenario.InflationShiftUp3[countFromFiveToFive] * 100 -
                                       context.TotalLiabScenario.InflationShiftDown3[countFromFiveToFive] * 100) /
                                      context.TotalLiabScenario.BaseIndex) / (double)2) * (double)10000;

                NotionalsRequiredSwapsInterestRate[i] =
                    SensitivitiesRequiredSwapsInterestRate[i] /
                                    ((double)10000 * (double)0.5 *
                                     (context.TotalLiabScenario.InterestRateShiftDown1[countFromFiveToFive] -
                                      context.TotalLiabScenario.InterestRateShiftUp1[countFromFiveToFive]) /
                                     context.TotalLiabScenario.DiscountCurve[countFromFiveToFive]) * (double)10000;

                countFromFiveToFive += 5;
            }
            
            #endregion Notionals Required Swaps


            #region Resulting Total Portfolio Sensitivity

            ResultingTotalPortfolioSensitivityAssetsRates =
                new double[CurrentInflationRiskLiabilitiesActives.Length];
            ResultingTotalPortfolioSensitivityAssetsInflation =
                new double[CurrentInflationRiskLiabilitiesActives.Length];

            for(var i = 0; i < ResultingTotalPortfolioSensitivityAssetsInflation.Length; i++)
            {
                ResultingTotalPortfolioSensitivityAssetsInflation[i] =
                    CurrentInflationRiskLiabilitiesTotal[i] *
                     context.Parameters.HedgeRatioLiabilityInflationRisk;

                ResultingTotalPortfolioSensitivityAssetsRates[i] =
                    CurrentInterestRateRiskLiabilitiesTotal[i] *
                     context.Parameters.HedgeRatioLiabilityInterestRateRisk;
            }

            ResultingTotalPortfolioSensitivityAssetsInflationTotal =
                CurrentInflationRiskLiabilitiesTotalTotal *
                context.Parameters.HedgeRatioLiabilityInflationRisk;

            ResultingTotalPortfolioSensitivityAssetsRatesTotal =
                ResultingTotalPortfolioSensitivityAssetsRates.Sum();

            #endregion Resulting Total Portfolio Sensitivity


            #region Pure Inflation Effect
                    
            PureInflationEffect = new double[context.Projections.UpliftCashflow.Length];

            for(var i = 0; i < PureInflationEffect.Length; i++)
            {
                PureInflationEffect[i] = context.Projections.UpliftCashflow[i] - context.INFSensAct.SalaryEffect[i];
            }

            #endregion Pure Inflation Effect

            /*#region pure salary

            context.TotalLiabScenario.PureInflationEffect = new double[context.INFSensAct.SalaryEffect.Length];

            for (var i = 0; i < context.TotalLiabScenario.PureInflationEffect.Length; i++)
            {
                context.TotalLiabScenario.PureInflationEffect[i] =
                    context.Projections.UpliftCashflow[i] - context.INFSensAct.SalaryEffect[i];
            }

            #endregion pure salary*/
        }
    }
}
