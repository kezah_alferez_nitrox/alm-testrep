﻿using System.Linq;

namespace Sensitivity_Analysis_Calculator.Calculators
{
    public class FixedAssets
    {
        public double[] AssetCF { get; set; }
        public double[] DiscountCurve { get; set; }
        public double[] DiscountCurveBpUp { get; set; }
        public double[] DiscountCurveBpDown { get; set; }

        public double[] PvCf1 { get; set; }
        public double[] PvCf2 { get; set; }
        public double PvCfTotal { get; set; }
        public double PvCfTotalPerCent { get; set; }
        public double[,][] CalculatedYrKr { get; set; }

        public void Calculate(CalculContext context)
        {
            //vars
            AssetCF = new double[context.Projections.EurSovereignBonds.Length];
            DiscountCurveBpDown = new double[AssetCF.Length]; 
            DiscountCurveBpUp = new double[AssetCF.Length]; 
            DiscountCurve = new double[AssetCF.Length];
            PvCf1 = new double[DiscountCurve.Length];
            PvCf2 = new double[DiscountCurve.Length];

            for(var i = 0; i < AssetCF.Length; i++)
            {
                //Asset CF
                AssetCF[i] = 
                    context.Projections.EurSovereignBonds[i] + context.Projections.InflationLinkedBondsEur[i];

                //Discount Curve
                DiscountCurve[i] = context.TotalLiabScenario.FixedInterestRateDF[i];
                DiscountCurveBpUp[i] = context.TotalLiabScenario.FixedInterestRateUp1[i];
                DiscountCurveBpDown[i] = context.TotalLiabScenario.FixedInterestRateDown1[i];

                //PV CF
                PvCf1[i] = DiscountCurveBpDown[i] * AssetCF[i];
                PvCf2[i] = DiscountCurveBpUp[i] * AssetCF[i];
            }

            PvCfTotal = (PvCf1.Sum() - PvCf2.Sum()) / 2;
            PvCfTotalPerCent = PvCfTotal / PvCf2.Sum();

            //Calc
            var calculator = new YrKrCalculator { PvFc1 = PvCf1, PvFc2 = PvCf2 };
            calculator.Calculate();

            CalculatedYrKr = calculator.YrKr;
            context.Overview.CurrentRiskAssetsInterestRate = calculator.YrKrSummary;
        }
    }
}
