﻿using System;

namespace Sensitivity_Analysis_Calculator.Calculators
{
    public class InflationSwapExposure
    {
        public double[] AssetCF;
        public double[] ActualCF;
        
        public double[] PvCf1 { get; set; }
        public double[] PvCf2 { get; set; }
        public double PvCfTotal { get; set; }
        public double PvCfTotalPerCent { get; set; }
        public double[] InflationIndex { get; set; }
        public double[] IndexPlus1Bp { get; set; }
        public double[] IndexMinus1Bp { get; set; }
        public double[,][] CalculatedYrKr { get; set; }

        public void Calculate(CalculContext context)
        {
            PvCf1 = new double[context.TotalLiabScenario.YearFraction.Length]; 
            PvCf2 = new double[context.TotalLiabScenario.YearFraction.Length];
            AssetCF = new double[context.TotalLiabScenario.YearFraction.Length];

            for(var i = 0; i < AssetCF.Length; i++)
            {
                AssetCF[i] = context.Projections.InflationSwapsInflationLeg[i]/
                             Math.Pow((1 + context.TotalLiabScenario.FixedInflationUpliftInflationShift[i]),
                                      context.TotalLiabScenario.YearFraction[i]);

                PvCf1[i] = context.TotalLiabScenario.FixedInterestRateDF[i] * AssetCF[i] *
                           Math.Pow((1 + context.TotalLiabScenario.FixedInflationUpliftInflationShiftDown[i]),
                                    context.TotalLiabScenario.YearFraction[i]);
                PvCf2[i] = context.TotalLiabScenario.FixedInterestRateDF[i]*AssetCF[i]*
                           Math.Pow((1 + context.TotalLiabScenario.FixedInflationUpliftInflationShiftUp[i]),
                                    context.TotalLiabScenario.YearFraction[i]);
            }

            //Calc
            var calculator = new YrKrCalculator { PvFc1 = PvCf1, PvFc2 = PvCf2 };
            calculator.Calculate();
            context.Overview.CurrentSwapPortfolioSensitivitiesInflation = calculator.YrKrSummary;
            CalculatedYrKr = calculator.YrKr;

            context.Overview.CurrentSwapPortfolioSensitivitiesInflationPlusActivities =
                new double[context.Overview.CurrentSwapPortfolioSensitivitiesInflation.Length];

            if(!context.Parameters.IncludeCurrentIRSwapPortfolio)
            {
                context.Overview.CurrentSwapPortfolioSensitivitiesInflationPlusActivities =
                    context.Overview.CurrentRiskAssetsInflation;
            }
            else
            {
                for (var i = 0; i < context.Overview.CurrentSwapPortfolioSensitivitiesInflation.Length; i++)
                {
                    context.Overview.CurrentSwapPortfolioSensitivitiesInflationPlusActivities[i] =
                        context.Overview.CurrentSwapPortfolioSensitivitiesInflation[i] +
                        context.Overview.CurrentRiskAssetsInflation[i];
                }
            }

            InflationIndex = new double[AssetCF.Length];
            IndexPlus1Bp = new double[AssetCF.Length];
            IndexMinus1Bp = new double[AssetCF.Length];

            InflationIndex[0] = context.TotalLiabScenario.BaseIndex *
                                ((double)1 + context.TotalLiabScenario.InflationAssetsShiftPerCent[0]);
            IndexPlus1Bp[0] = context.TotalLiabScenario.BaseIndex *
                              ((double)1 + context.TotalLiabScenario.InflationAssetsShiftDown[0]);
            IndexMinus1Bp[0] = context.TotalLiabScenario.BaseIndex *
                              ((double)1 + context.TotalLiabScenario.InflationAssetsShiftUp[0]);

            for (var i = 1; i < InflationIndex.Length; i++)
            {
                InflationIndex[i] = InflationIndex[0] *
                                    Math.Pow(1 + context.TotalLiabScenario.InflationAssetsShiftPerCent[i],
                                             context.TotalLiabScenario.YearFraction[i]);
                IndexPlus1Bp[i] = IndexPlus1Bp[i - 1] *
                                  Math.Pow((double)1 + context.TotalLiabScenario.InflationAssetsShiftDown[i],
                                           context.TotalLiabScenario.YearFraction[i] -
                                           context.TotalLiabScenario.YearFraction[i - 1]);
                IndexMinus1Bp[i] = IndexMinus1Bp[i - 1] *
                                  Math.Pow((double)1 + context.TotalLiabScenario.InflationAssetsShiftUp[i],
                                           context.TotalLiabScenario.YearFraction[i] -
                                           context.TotalLiabScenario.YearFraction[i - 1]);
            }

            ActualCF = new double[AssetCF.Length];

            for(var i = 0; i < ActualCF.Length; i++)
            {
                ActualCF[i] = AssetCF[i] *
                           (Math.Pow((double)1 + context.TotalLiabScenario.InflationAssetsShiftPerCent[i],
                                     context.TotalLiabScenario.YearFraction[i]));
            }
        }
    }
}
