﻿using System;

namespace Sensitivity_Analysis_Calculator.Calculators
{
    public class TotalLiabScenario
    {
        public int Period { get; set; }
        public int Year { get; set; }
        public const double YearFraction0 = 0.25;
        public double[] YearFraction { get; set; }
        public double BaseIndex { get { return 126.856528189; } }
        public double[] Rate { get; set; }        
        public double[] InflationIndex { get; set; }
        public double[] InflationShiftPerCent { get; set; }
        public double[] InflationAssetsShiftPerCent { get; set; }
        public double[] DF;

        public double[] InterestRateShiftUp { get; set; }
        public double[] InterestRateShiftDown { get; set; }
        public double[] InterestRateShiftUp1 { get; set; }
        public double[] InterestRateShiftDown1 { get; set; }

        public double[] InflationShift { get; set; }
        public double[] InflationAssetsShift { get; set; }
        public double[] InflationShiftBaseIndex { get; set; }

        public double InflationShiftBpUp { get { return 1; } }
        public double InflationShiftBpDown { get { return -1; } }

        public double[] InflationShiftUp { get; set; }
        public double[] InflationShiftDown { get; set; }

        public double[] InflationAssetsShiftUp { get; set; }
        public double[] InflationAssetsShiftDown { get; set; }

        public double[] InflationShiftUp2 { get; set; }
        public double[] InflationShiftDown2 { get; set; }

        public double[] InflationShiftUp3 { get; set; }
        public double[] InflationShiftDown3 { get; set; }

        public double[] EscalationFactor { get; set; }
        public double EscalationFactorConstant { get { return (double) 30; } }

        public double[] FixedInflationUpliftInflationShift { get; set; }
        public double[] FixedInflationUpliftInflationShiftIndex { get; set; }
        public double[] FixedInflationUpliftInflationShiftUp { get; set; }
        public double[] FixedInflationUpliftInflationShiftDown { get; set; }
        public double[] FixedInflationUpliftInflationShiftUp2 { get; set; }
        public double[] FixedInflationUpliftInflationShiftDown2 { get; set; }
        public double[] FixedInflationUpliftInflationShiftUp3 { get; set; }
        public double[] FixedInflationUpliftInflationShiftDown3 { get; set; }

        public double[] FixedInterestRateRate { get; set; }
        public double[] FixedInterestRateDF { get; set; }
        public double[] FixedInterestRateUp1 { get; set; }
        public double[] FixedInterestRateUp2 { get; set; }
        public double[] FixedInterestRateDown1 { get; set; }
        public double[] FixedInterestRateDown2 { get; set; }

        public double[] InflationIndexPrivate { get; set; }
        public double[] IndexPlus1BpPrivate { get; set; }
        public double[] IndexMinus1BpPrivate { get; set; }

        public double[] PureInflationEffect { get; set; }
        
            
        public void Calculate(CalculContext context)
        {
            if (context.Parameters.LiabilitiesInterestRateMarketRatesIsSet)
                context.Parameters.LiabilitiesInterestRateMarketRates = -1;

            if (context.Parameters.LiabilitiesInflationMarketRatesIsSet)
                context.Parameters.LiabilitiesInflationMarketRates = -1;

            if (context.Parameters.AssetsInterestRateMarketRatesIsSet)
                context.Parameters.AssetsInterestRateMarketRates = -1;

            if (context.Parameters.AssetsInflationMarketRatesIsSet)
                context.Parameters.AssetsInflationMarketRates = -1;

            #region Year

            Year = DateTime.Now.Year;
            
            context.Projections.Year = new double[63];
            context.Projections.YearMinusOne = new double[63];

            for (var i = 0; i < context.Projections.Year.Length; i++)
            {
                // ReSharper disable RedundantCast
                context.Projections.Year[i] = (double) Year + (double) i;
                // ReSharper restore RedundantCast
                context.Projections.YearMinusOne[i] = context.Projections.Year[i] - 1;
            }

            #endregion Year

                #region YearFraction

                // calculeaza year fraction
                YearFraction = new double[63];
            YearFraction[0] = 0; //DateTime.Now.Subtract(DateTime.Now.AddMonths(-3)).TotalMilliseconds / 1000 / 60 / 60 / 24 / 365.25;
            for (var i = 1; i < 63; i++)
                YearFraction[i] = i;

            #endregion YearFraction


            #region InflationIndex

            // calculeaza InflationIndex
            var result = new double[Inflation.Length];

            result[0] = BaseIndex * (1 + Inflation[0]);
            
            for (var i = 1; i < result.Length; i++)
                result[i] = result[0] * (Math.Pow((1 + Inflation[i]), YearFraction[i]));

            context.TotalLiabScenario.InflationIndex = result;

            #endregion InflationIndex


            #region InflationShift

            InflationShiftPerCent = new double[YearFraction.Length];
            InflationAssetsShiftPerCent = new double[YearFraction.Length];

            if (!context.Parameters.LiabilitiesInflationMarketRatesIsSet &&
                 context.Parameters.LiabilitiesInflationMarketRates > 0)
                InflationShiftPerCent[0] = context.Parameters.IndexationLevelPercentofCpi *
                                            (context.Parameters.LiabilitiesInflationMarketRates *
                                             YearFraction[0]);
            else
                InflationShiftPerCent[0] = Inflation[0];

            if (!context.Parameters.AssetsInflationMarketRatesIsSet &&
                 context.Parameters.AssetsInflationMarketRates > 0)
                InflationAssetsShiftPerCent[0] = context.Parameters.IndexationLevelPercentofCpi *
                                            (context.Parameters.AssetsInflationMarketRates *
                                             YearFraction[0]);
            else
                InflationAssetsShiftPerCent[0] = Inflation[0];

            for (var i = 1; i < YearFraction.Length; i++)
            {
                if (!context.Parameters.LiabilitiesInflationMarketRatesIsSet &&
                     context.Parameters.LiabilitiesInflationMarketRates > 0)
                    InflationShiftPerCent[i] = context.Parameters.LiabilitiesInflationMarketRates; 
                else
                {
                    InflationShiftPerCent[i] = Inflation[i];
                    //break;
                }
            }

            for (var i = 1; i < YearFraction.Length; i++)
            {
                if (!context.Parameters.AssetsInflationMarketRatesIsSet &&
                     context.Parameters.AssetsInflationMarketRates > 0)
                    InflationAssetsShiftPerCent[i] = context.Parameters.AssetsInflationMarketRates;
                else
                {
                    InflationAssetsShiftPerCent[i] = Inflation[i];
                    //break;
                }
            }

            #endregion InflationShift


            #region RATE

            Rate = new double[DiscountCurve.Length];
            for (var i = 1; i < Rate.Length; i++)
            {
                Rate[i] = 
                    context.Parameters.LiabilitiesInterestRateMarketRates < 0 ? 
                    -Math.Log(DiscountCurve[i]) / YearFraction[i] : 
                    context.Parameters.LiabilitiesInterestRateMarketRates;
            }


            #endregion RATE


            #region DF

            DF = new double[YearFraction.Length];
            DF[0] = 1;

            for(var i = 1; i < YearFraction.Length - 1; i++)
            {
                DF[i] = Math.Exp(-Rate[i] * YearFraction[i]);
            }

            #endregion DF


            #region Interest Rate Shift

            InterestRateShiftUp = new double[YearFraction.Length];
            InterestRateShiftUp1 = new double[YearFraction.Length];
            InterestRateShiftDown = new double[YearFraction.Length];
            InterestRateShiftDown1 = new double[YearFraction.Length];

            InterestRateShiftUp[0] = 1;
            InterestRateShiftDown[0] = 1;
            InterestRateShiftUp1[0] = 1;
            InterestRateShiftDown1[0] = 1;

            for(var i = 1; i < Rate.Length - 1; i++)
            {
                InterestRateShiftDown[i] = Math.Exp(-(Rate[i] +
                                                      context.Parameters.IntrestRateShiftDownConstant /
                                                      10000) *
                                                      YearFraction[i]);
                InterestRateShiftUp[i] = Math.Exp(-(Rate[i] +
                                                   context.Parameters.IntrestRateShiftUpConstant /
                                                   10000) *
                                                   YearFraction[i]);

                InterestRateShiftDown1[i] =
                    Math.Exp(-(-Math.Log(DiscountCurve[i]) / YearFraction[i] + (double)-1 / (double)10000) * YearFraction[i]);
                InterestRateShiftUp1[i] =
                    Math.Exp(-(-Math.Log(DiscountCurve[i]) / YearFraction[i] + (double)1 / (double)10000) * YearFraction[i]);
            }

            #endregion Interest Rate Shift


            #region Inflation Shift

            InflationShift = new double[YearFraction.Length];
            InflationAssetsShift = new double[YearFraction.Length];
            InflationShiftBaseIndex = new double[YearFraction.Length];
            InflationShiftUp = new double[YearFraction.Length];
            InflationShiftDown = new double[YearFraction.Length];
            InflationAssetsShiftUp = new double[YearFraction.Length];
            InflationAssetsShiftDown = new double[YearFraction.Length];
            InflationShiftUp2 = new double[YearFraction.Length];
            InflationShiftDown2 = new double[YearFraction.Length];
            InflationShiftUp3 = new double[YearFraction.Length];
            InflationShiftDown3 = new double[YearFraction.Length];

            if (context.Parameters.LiabilitiesInflationMarketRates >= 0)
                InflationShift[0] = context.Parameters.IndexationLevelPercentofCpi *
                    (context.Parameters.LiabilitiesInflationMarketRates * YearFraction[0]);
            else
                InflationShift[0] = context.Parameters.IndexationLevelPercentofCpi * Inflation[0];

            if (context.Parameters.AssetsInflationMarketRates >= 0)
                InflationAssetsShift[0] = context.Parameters.IndexationLevelPercentofCpi *
                    (context.Parameters.AssetsInflationMarketRates * YearFraction[0]);
            else
                InflationAssetsShift[0] = context.Parameters.IndexationLevelPercentofCpi * Inflation[0];

            InflationShiftBaseIndex[0] = context.TotalLiabScenario.BaseIndex * ((double)1 + InflationShift[0]);

            for (var i = 0; i < Inflation.Length; i++)
            {
                //Inflation Shift
                if (i + 1 < 63)
                {
                    if (context.Parameters.LiabilitiesInflationMarketRates >= 0)
                        InflationShift[i + 1] = context.Parameters.IndexationLevelPercentofCpi*
                                                context.Parameters.LiabilitiesInflationMarketRates;
                    else
                        InflationShift[i + 1] =
                            (-1)*context.Parameters.LiabilitiesInflationMarketRates*Inflation[i + 1];

                    if (context.Parameters.AssetsInflationMarketRates >= 0)
                        InflationAssetsShift[i + 1] = context.Parameters.IndexationLevelPercentofCpi *
                                                context.Parameters.AssetsInflationMarketRates;
                    else
                        InflationAssetsShift[i + 1] =
                            (-1)*context.Parameters.AssetsInflationMarketRates * Inflation[i + 1];

                    InflationShiftBaseIndex[i + 1] = InflationShiftBaseIndex[i] * ((double)1 + InflationShift[i + 1]);
                }

                //Inflation Shift UP
                InflationShiftUp[i] = InflationShift[i] + context.Parameters.IndexationLevelPercentofCpi *
                                                          InflationShiftBpUp / (double)10000;
                //Inflation Shift DOWN
                InflationShiftDown[i] = InflationShift[i] + context.Parameters.IndexationLevelPercentofCpi *
                                                          InflationShiftBpDown / (double)10000;

                //Inflation Shift UP
                InflationAssetsShiftUp[i] = InflationAssetsShift[i] + context.Parameters.IndexationLevelPercentofCpi *
                                                          InflationShiftBpUp / (double)10000;
                //Inflation Shift DOWN
                InflationAssetsShiftDown[i] = InflationAssetsShift[i] + context.Parameters.IndexationLevelPercentofCpi *
                                                          InflationShiftBpDown / (double)10000;

                //Inflation Shift Up / Down 2
                InflationShiftUp2[i] = Inflation[i] + InflationShiftBpUp / (double)10000;
                InflationShiftDown2[i] = Inflation[i] + InflationShiftBpDown / (double)10000;
            }

            //Inflation Shift Up / Down 3
            InflationShiftUp3[0] = (BaseIndex * (1 + InflationShiftUp2[0])) / (double)100;
            InflationShiftDown3[0] = (BaseIndex * (1 + InflationShiftDown2[0])) / (double)100;

            for(var i = 1; i < InflationShiftUp2.Length; i++)
            {
                InflationShiftUp3[i] = InflationShiftUp3[0] *
                                       (Math.Pow((1 + InflationShiftUp2[i]), YearFraction[i]));
                InflationShiftDown3[i] = InflationShiftDown3[0] *
                                       (Math.Pow((1 + InflationShiftDown2[i]), YearFraction[i]));
            }
            
            #endregion Inflation Shift

            #region Fixed Inflation Uplift

            if (context.Parameters.AssetsInflationMarketRatesIsSet)
                context.Parameters.AssetsInflationMarketRates = -1;

            FixedInflationUpliftInflationShiftDown = new double[Inflation.Length];
            FixedInflationUpliftInflationShiftUp = new double[Inflation.Length];
            FixedInflationUpliftInflationShiftDown2 = new double[Inflation.Length];
            FixedInflationUpliftInflationShiftUp2 = new double[Inflation.Length];
            FixedInflationUpliftInflationShiftDown3 = new double[Inflation.Length];
            FixedInflationUpliftInflationShiftUp3 = new double[Inflation.Length];
            FixedInflationUpliftInflationShift = new double[Inflation.Length];
            FixedInflationUpliftInflationShiftIndex = new double[Inflation.Length];

            if (context.Parameters.AssetsInflationMarketRates >= 0)
//FixedInflationUpliftInflationShift[0] = context.Parameters.AssetsInflationMarketRates*YearFraction[0];
                FixedInflationUpliftInflationShift[0] =
                    context.Parameters.IndexationLevelPercentofCpi *
                    context.Parameters.AssetsInflationMarketRates * YearFraction[0];
            else
                //FixedInflationUpliftInflationShift[0] = Inflation[0];
                FixedInflationUpliftInflationShift[0] =
                    context.Parameters.IndexationLevelPercentofCpi *
                    Inflation[0];


            for(var i = 0; i < FixedInflationUpliftInflationShift.Length; i++)
            {
                if (i + 1 != FixedInflationUpliftInflationShift.Length)
                {
                    if (context.Parameters.AssetsInflationMarketRates >= 0)
                    {
                        FixedInflationUpliftInflationShift[i + 1] =
                            context.Parameters.IndexationLevelPercentofCpi*
                            context.Parameters.AssetsInflationMarketRates; //* YearFraction[i];
                    }
                    else
                    {
                        FixedInflationUpliftInflationShift[i + 1] =
                            context.Parameters.IndexationLevelPercentofCpi*
                            Inflation[i + 1];
                    }
                }

                FixedInflationUpliftInflationShiftDown[i] = FixedInflationUpliftInflationShift[i] + (double)1 / (double)10000;
                FixedInflationUpliftInflationShiftUp[i] = FixedInflationUpliftInflationShift[i] + (double)-1 / (double)10000;
                FixedInflationUpliftInflationShiftDown2[i] = Inflation[i] + (double)-1 / (double)10000;
                FixedInflationUpliftInflationShiftUp2[i] = Inflation[i] + (double)1 / (double)10000;
            }

            FixedInflationUpliftInflationShiftDown3[0] = ((BaseIndex*
                                                          ((double) 1 + FixedInflationUpliftInflationShiftDown2[0]))) / (double)100;
            FixedInflationUpliftInflationShiftUp3[0] = ((BaseIndex*
                                                        ((double)1 + FixedInflationUpliftInflationShiftUp2[0]))) / (double)100;

            for (var i = 1; i < FixedInflationUpliftInflationShiftUp3.Length; i++)
            {
                FixedInflationUpliftInflationShiftDown3[i] = (FixedInflationUpliftInflationShiftDown3[0] *
                                                           (Math.Pow(
                                                               (double)1 + FixedInflationUpliftInflationShiftDown2[i],
                                                               YearFraction[i])));

                FixedInflationUpliftInflationShiftUp3[i] = (FixedInflationUpliftInflationShiftUp3[0]*
                                                           (Math.Pow(
                                                               (double) 1 + FixedInflationUpliftInflationShiftUp2[i],
                                                               YearFraction[i])));
            }


            FixedInflationUpliftInflationShiftIndex[0] = BaseIndex * ((double)1 + FixedInflationUpliftInflationShift[0]);

            for (var i = 1; i < FixedInflationUpliftInflationShiftIndex.Length; i++)
            {
                FixedInflationUpliftInflationShiftIndex[i] = FixedInflationUpliftInflationShiftIndex[i - 1]*
                                                             ((double)1 + FixedInflationUpliftInflationShift[i]);
            }

            #endregion Fixed Inflation Uplift


                #region Fixed Interest Rate

                #region RATE

            FixedInterestRateRate = new double[YearFraction.Length];

            //FixedInterestRateRate[0] = 0;

            for(var i = 0; i < YearFraction.Length; i++)
            {
                if (!(context.Parameters.AssetsInterestRateMarketRates >= 0))
                    FixedInterestRateRate[i] = -Math.Log(DiscountCurve[i])/YearFraction[i];
                else
                    FixedInterestRateRate[i] = context.Parameters.AssetsInterestRateMarketRates;
            }

            #endregion RATE

            #region DF

            FixedInterestRateDF = new double[YearFraction.Length];

            for(var i = 0; i < FixedInterestRateDF.Length; i++)
            {
                FixedInterestRateDF[i] = Math.Exp(-FixedInterestRateRate[i] * YearFraction[i]);
            }

            #endregion DF

            #region bp up/down

            FixedInterestRateUp1 = new double[YearFraction.Length];
            FixedInterestRateUp2 = new double[YearFraction.Length];
            FixedInterestRateDown1 = new double[YearFraction.Length];
            FixedInterestRateDown2 = new double[YearFraction.Length];

            //FixedInterestRateUp1[0] = 1;
            //FixedInterestRateDown1[0] = 1;
            FixedInterestRateUp2[0] = 1;
            FixedInterestRateDown2[0] = 1;
            
            for(var i = 0; i < FixedInterestRateUp1.Length; i++)
            {
                FixedInterestRateUp1[i] =
// ReSharper disable RedundantCast
            Math.Exp(-(FixedInterestRateRate[i] + (double)1 / (double)10000) * YearFraction[i]);
// ReSharper restore RedundantCast

                FixedInterestRateDown1[i] =
// ReSharper disable RedundantCast
            Math.Exp(-(FixedInterestRateRate[i] + (double)-1 / (double)10000) * YearFraction[i]);
// ReSharper restore RedundantCast

                if (i + 1 != FixedInterestRateUp1.Length)
                {
                    FixedInterestRateUp2[i + 1] =
                        // ReSharper disable RedundantCast
                        Math.Exp(-(-Math.Log(DiscountCurve[i + 1])/YearFraction[i + 1] + (double) 1/(double) 10000)*
                                 YearFraction[i + 1]);
                    // ReSharper restore RedundantCast

                    FixedInterestRateDown2[i + 1] =
                        // ReSharper disable RedundantCast
                        Math.Exp(-(-Math.Log(DiscountCurve[i + 1])/YearFraction[i + 1] + (double) -1/(double) 10000)*
                                 YearFraction[i + 1]);
                    // ReSharper restore RedundantCast
                }
            }

            #endregion  bp up/down


            #endregion Fixed Interest Rate

            #region Inflation Indexes Private

            InflationIndexPrivate = new double[InflationIndex.Length];
            IndexPlus1BpPrivate = new double[InflationIndex.Length];
            IndexMinus1BpPrivate = new double[InflationIndex.Length];

            InflationIndexPrivate[0] = context.TotalLiabScenario.BaseIndex *
                                ((double)1 + context.TotalLiabScenario.InflationShiftPerCent[0]);
            IndexPlus1BpPrivate[0] = context.TotalLiabScenario.BaseIndex *
                              ((double)1 + context.TotalLiabScenario.InflationShiftDown[0]);
            IndexMinus1BpPrivate[0] = context.TotalLiabScenario.BaseIndex *
                              ((double)1 + context.TotalLiabScenario.InflationShiftUp[0]);

            for (var i = 1; i < InflationIndex.Length; i++)
            {
                InflationIndexPrivate[i] = InflationIndexPrivate[0] *
                                    Math.Pow(1 + context.TotalLiabScenario.InflationShiftPerCent[i],
                                             context.TotalLiabScenario.YearFraction[i]);
                IndexPlus1BpPrivate[i] = IndexPlus1BpPrivate[i - 1] *
                                  Math.Pow((double)1 + context.TotalLiabScenario.InflationShiftDown[i],
                                           context.TotalLiabScenario.YearFraction[i] -
                                           context.TotalLiabScenario.YearFraction[i - 1]);
                IndexMinus1BpPrivate[i] = IndexMinus1BpPrivate[i - 1] *
                                  Math.Pow((double)1 + context.TotalLiabScenario.InflationShiftUp[i],
                                           context.TotalLiabScenario.YearFraction[i] -
                                           context.TotalLiabScenario.YearFraction[i - 1]);
            }

            #endregion Inflation Indexes Private


            #region Escalation Factor

            EscalationFactor = new double[YearFraction.Length];

            for (int i = 0; i < EscalationFactor.Length; i++)
            {
                if (YearFraction[i] > EscalationFactorConstant)
                    EscalationFactor[i] = EscalationFactorConstant;
                else
                {
                    EscalationFactor[i] = YearFraction[i];
                }
            }

            #endregion Escalation Factor
        } 

        public double[] DiscountCurve = new[]
                                            {
                                                1,
                                                0.989463617692083,
                                                0.967764948925124,
                                                0.935577215238085,
                                                0.903201806497073,
                                                0.869596369382014,
                                                0.835409672197094,
                                                0.801044039399524,
                                                0.767414369909004,
                                                0.734508579355385,
                                                0.702198663339461,
                                                0.670200896390971,
                                                0.638895989775549,
                                                0.608584048853351,
                                                0.579453355269589,
                                                0.551777997991335,
                                                0.525824061456855,
                                                0.501727338385696,
                                                0.479349000070668,
                                                0.458764133184375,
                                                0.439898535875924,
                                                0.42265399156167,
                                                0.406912146819437,
                                                0.392245860478897,
                                                0.378794457082352,
                                                0.366091371815512,
                                                0.353957615912151,
                                                0.342367321868955,
                                                0.331458972640573,
                                                0.321212261960745,
                                                0.311607400597269,
                                                0.302707408436067,
                                                0.294437059791085,
                                                0.28672000395417,
                                                0.279479049110412,
                                                0.272636392191215,
                                                0.266113808665248,
                                                0.259832807472262,
                                                0.25371476627234,
                                                0.247681053623769,
                                                0.241653148429747,
                                                0.235552765445969,
                                                0.229404092655136,
                                                0.223232635965714,
                                                0.217065082434019,
                                                0.210929163178123,
                                                0.204853517274468,
                                                0.198867558025414,
                                                0.193001343059716,
                                                0.187285449780444,
                                                0.181750857704535,
                                                0.175644752877794,
                                                0.169759638525677,
                                                0.164071709517103,
                                                0.158574359003437,
                                                0.153246892211052,
                                                0.14811223563867,
                                                0.143149619736971,
                                                0.138353280149189,
                                                0.13370516105702,
                                                0.129225265418827,
                                                0.124895472175863,
                                                0.120710752030377
                                            };

        public double[] Inflation = new[]
                                        {
                                            0.00720201335198631,
                                            0.0100173240633694,
                                            0.0148067016951063,
                                            0.0166408254520015,
                                            0.0182320303907635,
                                            0.0195770030003395,
                                            0.0206597877570241,
                                            0.0215205888410167,
                                            0.0221834717412053,
                                            0.022675242875145,
                                            0.0230190122530792,
                                            0.0232433359253693,
                                            0.0233745608532637,
                                            0.0234435554458612,
                                            0.0234792268863343,
                                            0.0235132208576838,
                                            0.0235653401911317,
                                            0.0236292093635876,
                                            0.0236971691417503,
                                            0.023764031870236,
                                            0.0238209659043287,
                                            0.0238639291767123,
                                            0.0238970747133822,
                                            0.0239265021440319,
                                            0.0239553172040673,
                                            0.0239886020112137,
                                            0.0240293246984741,
                                            0.0240771553946958,
                                            0.0241291545624871,
                                            0.0241839329027637,
                                            0.0242314119369819,
                                            0.0242451444049879,
                                            0.0242588334853663,
                                            0.0242725171085099,
                                            0.0242861957281805,
                                            0.0242999070750938,
                                            0.0243135769095583,
                                            0.024327242907088,
                                            0.024340905355488,
                                            0.0243546018173349,
                                            0.0243680356838105,
                                            0.0243735955772824,
                                            0.0243791531189913,
                                            0.0243847235827989,
                                            0.0243902768951703,
                                            0.0243958283078333,
                                            0.0244013779419646,
                                            0.024406941016027,
                                            0.0244124874251445,
                                            0.0244180323677696,
                                            0.0244180323677696,
                                            0.0244180323677696,
                                            0.0244180323677696,
                                            0.0244180323677696,
                                            0.0244180323677696,
                                            0.0244180323677696,
                                            0.0244180323677696,
                                            0.0244180323677696,
                                            0.0244180323677696,
                                            0.0244180323677696,
                                            0.0244180323677696,
                                            0.0244180323677696,
                                            0.0244180323677696
                                        };
    }
}