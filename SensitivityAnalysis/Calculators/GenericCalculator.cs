﻿using System;
using System.Linq;

namespace Sensitivity_Analysis_Calculator.Calculators
{
    public class MathTools
    {
        public static double Round(double number, int decimals)
        {
            return Math.Round(number / Math.Pow(10, -decimals)) * Math.Pow(10, -decimals);
        }
    }

    public class ArrayTools
    {
        public static double[] Add(params double[][] arrays)
        {
            var length = arrays.Max(a => a.Length);
            var result = new double[length];

            for (var i = 0; i < length; i++)
            {
                var sum = (double)0;
                foreach (var array in arrays)
                {
                    if (array.Length > i)
                        sum += array[i];
                }
                result[i] = sum;
            }
            return result;
        }

        public static double[] Substract(params double[][] arrays)
        {
            var length = arrays.Max(a => a.Length);
            var result = new double[length];

            for (var i = 0; i < length; i++)
            {
                var sum = (double)0;
                foreach (var array in arrays)
                {
                    if (array.Length > i)
                        sum -= array[i];
                }
                result[i] = sum;
            }
            return result;
        }


        public static double SumProduct(params double[][] arrays)
        {
            var length = arrays.Max(a => a.Length);
            var result = new double();
            var buffer = new double();

            for (var i = 0; i < length; i++)
            {
                //returns 140, or (1*10)+(2*20)+(3*30)=10+40+90=140.
                foreach (var array in arrays)
                {
                    if(array.Length > i)
                    {
                        if(buffer != 0)
                            buffer = array[i] * buffer;
                        else
                            buffer = array[i];
                    }
                }
                result += buffer;
                buffer = 0;
            }
            return result;
        }
    }
}