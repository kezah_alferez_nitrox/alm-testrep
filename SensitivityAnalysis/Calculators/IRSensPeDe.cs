﻿
namespace Sensitivity_Analysis_Calculator.Calculators
{
    public class IRSensPeDe
    {
        public double[] LiabilityCF { get; set; }
        public double[] LiabilityCF2 { get; set; }

        public double[] PvCf1 { get; set; }

        public double[] PvCf2 { get; set; }

        public double PvCf1Sum;
        public double PvCf2Sum;

        public double PvCfTotal;
        public double PvCfTotalPerCent;

        public double PensionersDeferred;

        public double[] YrKrPlusActivities { get; set; }
        public double[,][] CalculatedYrKr { get; set; }

        public void Calculate(CalculContext context)
        {
            //Calculate Liability CF
            LiabilityCF = ArrayTools.Add(context.INFSensPeDe.ActualCF, context.Projections.DeferredMembersFixed,
                                         context.Projections.PensionersFixed);
            LiabilityCF2 = ArrayTools.Add(context.Projections.DeferredMembersIndexed,
                                          context.Projections.PensionersIndexed);

            //Calculate PV CF 1 & 2

            PvCf1 = new double[LiabilityCF.Length];
            PvCf2 = new double[LiabilityCF.Length];

            for(var i = 0; i < context.TotalLiabScenario.InterestRateShiftDown.Length; i++)
            {
                PvCf1[i] = context.TotalLiabScenario.InterestRateShiftDown[i] * LiabilityCF[i];
                PvCf1Sum += PvCf1[i];

                PvCf2[i] = context.TotalLiabScenario.InterestRateShiftUp[i] * LiabilityCF[i];
                PvCf2Sum += PvCf2[i];
            }

            PvCfTotal = (PvCf1Sum - PvCf2Sum) / 2;
            PvCfTotalPerCent = PvCfTotal / PvCf2Sum;

            var calculator = new YrKrCalculator { PvFc1 = PvCf1, PvFc2 = PvCf2 };
            calculator.Calculate();
            context.Overview.CurrentInterestRateRiskLiabilitiesPensionersDeferreds = calculator.YrKrSummary;

            PensionersDeferred = ArrayTools.SumProduct(context.TotalLiabScenario.DF, LiabilityCF);

            YrKrPlusActivities = new double[calculator.YrKrSummary.Length];

            YrKrPlusActivities = ArrayTools.Add(calculator.YrKrSummary,
                                                context.Overview.CurrentInterestRateRiskLiabilitiesActives);

            context.Overview.CurrentInterestRateRiskLiabilitiesPensionersDeferredsPlusActivities =
                new double[YrKrPlusActivities.Length];
            context.Overview.CurrentInterestRateRiskLiabilitiesPensionersDeferredsPlusActivities =
                YrKrPlusActivities;

            CalculatedYrKr = calculator.YrKr;
        }
    }
}
