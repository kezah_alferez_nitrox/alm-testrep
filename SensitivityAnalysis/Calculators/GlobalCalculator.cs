﻿
namespace Sensitivity_Analysis_Calculator.Calculators
{
    public class GlobalCalculator
    {
        public CalculContext Context { get; set; }

        public bool HasCalculated { get; set; }

        public void CreateContext()
        {
            Context = new CalculContext
            {
                Parameters = new Parameters(),
                Projections = new Projections(),
                TotalLiabScenario = new TotalLiabScenario(),
                INFSensPeDe = new InfSensPeDe(),
                INFSensAct = new InfSensAct(),
                IrSensAct = new IRSensAct(),
                IrSensPeDe = new IRSensPeDe(),
                Overview = new Overview(),
                FixedAssets = new FixedAssets(),
                ILAssets = new ILAssets(),
                IRSwapExposure = new IRSwapExposure(),
                InflationSwapExposure = new InflationSwapExposure()
            };

            const int calcLenght = 63;

            Context.Projections.Year = new double[calcLenght];
            Context.Projections.ActiveMembersIndexed = new double[calcLenght];
            Context.Projections.DeferredMembersIndexed = new double[calcLenght];
            Context.Projections.PensionersIndexed = new double[calcLenght];
            Context.Projections.ActiveMembersFixed = new double[calcLenght];
            Context.Projections.DeferredMembersFixed = new double[calcLenght];
            Context.Projections.PensionersFixed = new double[calcLenght];

            Context.Projections.EurSovereignBonds = new double[calcLenght];
            Context.Projections.InflationLinkedBondsEur = new double[calcLenght];
            Context.Projections.InterestRateSwaps = new double[calcLenght];
            Context.Projections.InflationSwapsFixedLeg = new double[calcLenght];
            Context.Projections.InflationSwapsInflationLeg = new double[calcLenght];

            HasCalculated = false;
        }

        public void Calculate()
        {
            Context.TotalLiabScenario.Calculate(Context);
            Context.INFSensAct.Calculate(Context);
            Context.INFSensPeDe.Calculate(Context);
            Context.IrSensAct.Calculate(Context);
            Context.IrSensPeDe.Calculate(Context);
            Context.Projections.Calculate(Context);
            Context.FixedAssets.Calculate(Context);
            Context.ILAssets.Calculate(Context);
            Context.IRSwapExposure.Calculate(Context);
            Context.InflationSwapExposure.Calculate(Context);
            Context.Overview.Calculate(Context);

            HasCalculated = true;
        }
    }
}
