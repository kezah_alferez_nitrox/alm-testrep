﻿
namespace Sensitivity_Analysis_Calculator.Calculators
{
    public class IRSensAct
    {
        public double[] LiabilityCF { get; set; }

        public double NPV { get; set; }

        public double[] PvCf1 { get; set; }
        public double[] PvCf2 { get; set; }

        public double PvCf1Sum;
        public double PvCf2Sum;

        public double PvCfTotal;
        public double PvCfTotalPerCent;

        public double[,][] CalculatedYrKr { get; set; }


        public void Calculate(CalculContext context)
        {
            //Calculate Libility CF
            LiabilityCF = ArrayTools.Add(context.INFSensAct.Indexed, context.Projections.ActiveMembersFixed);

            PvCf1 = new double[context.TotalLiabScenario.InterestRateShiftDown.Length];
            PvCf2 = new double[context.TotalLiabScenario.InterestRateShiftDown.Length];

            //Calculate PV CF 1 & 2
            for(var i = 0; i < context.TotalLiabScenario.InterestRateShiftDown.Length; i++)
            {
                PvCf1[i] = context.TotalLiabScenario.InterestRateShiftDown[i] *
                           LiabilityCF[i];

                PvCf1Sum += PvCf1[i];

                PvCf2[i] = context.TotalLiabScenario.InterestRateShiftUp[i] *
                           LiabilityCF[i];

                PvCf2Sum += PvCf2[i];
            }

            PvCfTotal = (PvCf1Sum - PvCf2Sum) / 2;
            PvCfTotalPerCent = PvCfTotal / PvCf2Sum;

            var calculator = new YrKrCalculator {PvFc1 = PvCf1, PvFc2 = PvCf2};
            calculator.Calculate();
            context.Overview.CurrentInterestRateRiskLiabilitiesActives = calculator.YrKrSummary;

            NPV = ArrayTools.SumProduct(LiabilityCF, context.TotalLiabScenario.DF);

            CalculatedYrKr = calculator.YrKr;
        }
    }
}
