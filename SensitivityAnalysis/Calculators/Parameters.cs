﻿using System;

namespace Sensitivity_Analysis_Calculator.Calculators
{
    [Serializable]
    public class Parameters
    {

        public double SalaryEscalation
        {
            get; set;
        }

        public double Decay
        { get { return (double) 30; } }

        public double SalaryDecayFactor
        {
            get; set;
        }

        public double IntrestRateShiftUpConstant = (double)1;

        public double IntrestRateShiftDownConstant = (double)-1;

        public bool LiabilitiesInterestRateMarketRatesIsSet
        {
            get; set;
        }

        public double LiabilitiesInterestRateMarketRates
        {
            get; set;
        }

        public bool LiabilitiesInflationMarketRatesIsSet
        {
            get; set;
        }

        public double LiabilitiesInflationMarketRates
        {
            get; set;
        }

        public bool IndexationLevelPercentofCpiIsSet
        {
            get; set;
        }

        public double IndexationLevelPercentofCpi
        {
            get; set;
        }

        public bool IncludeCurrentIRSwapPortfolio
        {
            get; set;
        }

        public bool IncludeCurrentInflationSwapPortfolio
        {
            get; set;
        }

        public double HedgeRatioLiabilityInterestRateRisk
        {
            get; set;
        }

        public double HedgeRatioLiabilityInflationRisk
        {
            get; set;
        }

        public bool AssetsInterestRateMarketRatesIsSet
        {
            get; set;
        }

        public double AssetsInterestRateMarketRates
        {
            get; set;
        }

        public bool AssetsInflationMarketRatesIsSet
        {
            get; set;
        }

        public double AssetsInflationMarketRates
        {
            get; set;
        }
    }
}
