﻿using System;

namespace Sensitivity_Analysis_Calculator.Calculators
{
    [Serializable]
    public class Projections
    {
        #region Variables

        public double[] RealCashflow
        {
            get; set;
        }

        public double[] IndexedCashflow
        {
            get; set;
        }

        public double[] UpliftCashflow
        {
            get; set;
        }

        public double[] FixedCashflow
        {
            get; set;
        }

        public double[] EurSovereignBonds
        {
            get; set;
        }

        public double[] InflationLinkedBondsEur
        {
            get; set;
        }

        public double[] InterestRateSwaps
        {
            get; set;
        }

        public double[] InflationSwapsFixedLeg
        {
            get; set;
        }

        public double[] InflationSwapsInflationLeg
        {
            get; set;
        }

        public double[] PensionLdiAssetsTotal
        {
            get; set;
        }

        public double Inflation = (double)0.0248;

        public double SalaryEscalation = (double)0.01;

        public double[] LiabilitiesTotal
        {
            get; set;
        }

        public double[] AssetsTotal
        {
            get; set;
        }

        public double[] Year
        {
            get; set;
        }

        public double[] YearMinusOne
        {
            get;
            set;
        }

        public double[] ActiveMembersIndexed
        {
            get; set;
        }


        public double[] DeferredMembersIndexed
        {
            get; set;
        }

        public double[] PensionersIndexed
        {
            get; set;
        }

        public double[] ActiveMembersFixed
        {
            get; set;
        }

        public double[] DeferredMembersFixed
        {
            get; set;
        }

        public double[] PensionersFixed
        {
            get; set;
        }

        #endregion Variables


        public void Calculate(CalculContext context)
        {
            // Calculate Real Cashflow
            RealCashflow = ArrayTools.Add(context.INFSensPeDe.NominalCfFixed, context.INFSensAct.NominalCfFixed);

            //Calculate Indexed Cashflow
            var cfSum = ArrayTools.Add(context.IrSensPeDe.LiabilityCF, 
                                       context.IrSensAct.LiabilityCF);
            FixedCashflow = ArrayTools.Add(context.Projections.ActiveMembersFixed,
                                                          context.Projections.DeferredMembersFixed,
                                                          context.Projections.PensionersFixed);

            IndexedCashflow = new double[cfSum.Length];
            UpliftCashflow = new double[IndexedCashflow.Length];

            /*DEBUG*/
            /*var rnd = new Random();
            EurSovereignBonds = new double[cfSum.Length];
            InflationLinkedBondsEur = new double[cfSum.Length];
            InterestRateSwaps = new double[cfSum.Length];
            InflationSwapsFixedLeg = new double[cfSum.Length];
            InflationSwapsInflationLeg = new double[cfSum.Length];
            PensionLdiAssetsTotal = new double[cfSum.Length];*/

            for (var i = 0; i < cfSum.Length; i++)
            {
                //Indexed Cashflow
                IndexedCashflow[i] = cfSum[i] - FixedCashflow[i];
                //Uplift Cashflow
                UpliftCashflow[i] = IndexedCashflow[i] - RealCashflow[i];

                /*DEBUG*/
                /*EurSovereignBonds[i] = rnd.Next(100000000);
                InflationLinkedBondsEur[i] = rnd.Next(100000000);
                InterestRateSwaps[i] = rnd.Next(100000000);
                InflationSwapsFixedLeg[i] = rnd.Next(100000000);
                InflationSwapsInflationLeg[i] = rnd.Next(100000000);
                PensionLdiAssetsTotal[i] = rnd.Next(100000000);*/
            }

            //Calculate Total
            LiabilitiesTotal = new double[Year.Length];
            AssetsTotal = new double[Year.Length];

            LiabilitiesTotal = ArrayTools.Add(ActiveMembersIndexed, DeferredMembersIndexed, PensionersIndexed, ActiveMembersFixed,
                                   DeferredMembersFixed, PensionersFixed);
            AssetsTotal = ArrayTools.Add(EurSovereignBonds, InflationLinkedBondsEur, InterestRateSwaps,
                                         InflationSwapsFixedLeg, InflationSwapsInflationLeg);
        }
    }
}