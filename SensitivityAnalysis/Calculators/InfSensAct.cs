﻿using System;
using System.Linq;

namespace Sensitivity_Analysis_Calculator.Calculators
{
    public class InfSensAct
    {
        public double[] NominalCfFixed { get; set; }
        public double[] Indexed { get; set; }

        public double[] PvCf1 { get; set; }
        public double[] PvCf2 { get; set; }

        public double PvCfTotal;
        public double PvCfTotalPerCent;

        public double[,][] CalculatedYrKr { get; set; }

        public double[] SalaryIndexed { get; set; }
        public double[] SalaryNone { get; set; }
        public double[] SalaryEffect { get; set; }
        public double[] SalaryEscalation { get; set; }

        public double[] InflationIndex { get; set; }
        public double[] IndexPlus1Bp { get; set; }
        public double[] IndexMinus1Bp { get; set; }

        public double[] Decay { get; set; }

        public void Calculate(CalculContext context)
        {
            NominalCfFixed = new double[context.Projections.ActiveMembersIndexed.Length];
            Indexed = new double[NominalCfFixed.Length];

            //Calculate Nominal CF Fixed
            NominalCfFixed[0] = context.Projections.ActiveMembersIndexed[0] /
                                (1 +
                                 (context.Projections.Inflation + context.Projections.SalaryEscalation) *
                                 context.TotalLiabScenario.YearFraction[0]);

            Indexed[0] = NominalCfFixed[0] *
             (1 +
             context.Parameters.SalaryEscalation *
             context.TotalLiabScenario.YearFraction[0] +
             context.TotalLiabScenario.InflationShiftPerCent[0]);

            for (var i = 1; i < NominalCfFixed.Length; i++)
            {
                //Calculate Nominal CF Fixed
                    NominalCfFixed[i] = 
                        context.Projections.ActiveMembersIndexed[i] /
                        (Math.Pow(
                        (1 + context.Projections.Inflation + context.Projections.SalaryEscalation),
                        context.TotalLiabScenario.YearFraction[i]));
                        
                    //Calculate Indexed
                    Indexed[i] = NominalCfFixed[i] *
                                 Math.Pow((1 + context.Parameters.SalaryEscalation +
                                               context.TotalLiabScenario.InflationShiftPerCent[i]),
                                            context.TotalLiabScenario.YearFraction[i]);
            }

            PvCf1 = new double[context.TotalLiabScenario.InterestRateShiftDown.Length];
            PvCf2 = new double[context.TotalLiabScenario.InterestRateShiftDown.Length];

            PvCf1[0] = context.TotalLiabScenario.DF[0] * NominalCfFixed[0] *
                       (1 + context.TotalLiabScenario.InflationShiftUp[0] +
                        context.Parameters.SalaryEscalation * context.TotalLiabScenario.YearFraction[0]);

            PvCf2[0] = context.TotalLiabScenario.DF[0] * NominalCfFixed[0] *
                       (1 + context.TotalLiabScenario.InflationShiftDown[0] +
                        context.Parameters.SalaryEscalation * context.TotalLiabScenario.YearFraction[0]);

            //Calculate PV CF 1 & 2
            for (var i = 1; i < context.TotalLiabScenario.InterestRateShiftDown.Length; i++)
            {
                PvCf1[i] = context.TotalLiabScenario.DF[i]*
                           NominalCfFixed[i]*
                           Math.Pow(
                               (1 + 
                               context.TotalLiabScenario.InflationShiftUp[i] + 
                               context.Parameters.SalaryEscalation),
                               context.TotalLiabScenario.YearFraction[i]);

                PvCf2[i] = context.TotalLiabScenario.DF[i] *
                           NominalCfFixed[i] *
                           Math.Pow(
                               (1 +
                               context.TotalLiabScenario.InflationShiftDown[i] +
                               context.Parameters.SalaryEscalation),
                               context.TotalLiabScenario.YearFraction[i]);
            }

            PvCfTotal = (PvCf1.Sum() - PvCf2.Sum()) / 2;
            PvCfTotalPerCent = PvCfTotal / PvCf2.Sum();

            var calculator = new YrKrCalculator { PvFc1 = PvCf1, PvFc2 = PvCf2 };
            calculator.Calculate();
            context.Overview.CurrentInflationRiskLiabilitiesActives = calculator.YrKrSummary;

            CalculatedYrKr = calculator.YrKr;

            //Salary
            SalaryEffect = new double[NominalCfFixed.Length];
            SalaryIndexed = new double[NominalCfFixed.Length];
            SalaryNone = new double[NominalCfFixed.Length];

            SalaryIndexed[0] = NominalCfFixed[0] *
                               ((double)1 +
                               context.Parameters.SalaryEscalation *
                               context.TotalLiabScenario.YearFraction[0] +
                               context.TotalLiabScenario.InflationShift[0]);

            for(var i = 0; i < SalaryEffect.Length; i++)
            {
                if (i + 1 < SalaryIndexed.Length)
                    SalaryIndexed[i + 1] = NominalCfFixed[i + 1]*
                                           Math.Pow(
                                               ((double) 1 +
                                                context.Parameters.SalaryEscalation +
                                                context.TotalLiabScenario.InflationShift[i + 1]),
                                               context.TotalLiabScenario.YearFraction[i + 1]);

                SalaryNone[i] = NominalCfFixed[i]*
                                Math.Pow((double)1 + context.TotalLiabScenario.InflationShift[i],
                                         context.TotalLiabScenario.YearFraction[i]);

                SalaryEffect[i] = SalaryIndexed[i] - SalaryNone[i];
            }

            InflationIndex = new double[NominalCfFixed.Length];
            IndexPlus1Bp = new double[NominalCfFixed.Length];
            IndexMinus1Bp = new double[NominalCfFixed.Length];

            InflationIndex[0] = context.TotalLiabScenario.BaseIndex *
                                ((double)1 + context.TotalLiabScenario.InflationShiftPerCent[0]);
            IndexPlus1Bp[0] = context.TotalLiabScenario.BaseIndex *
                              ((double)1 + context.TotalLiabScenario.InflationShiftDown[0]);
            IndexMinus1Bp[0] = context.TotalLiabScenario.BaseIndex *
                              ((double)1 + context.TotalLiabScenario.InflationShiftUp[0]);

            for (var i = 1; i < InflationIndex.Length; i++)
            {
                InflationIndex[i] = InflationIndex[0] *
                                    Math.Pow(1 + context.TotalLiabScenario.InflationShiftPerCent[i],
                                             context.TotalLiabScenario.YearFraction[i]);
                IndexPlus1Bp[i] = IndexPlus1Bp[i - 1] *
                                  Math.Pow((double)1 + context.TotalLiabScenario.InflationShiftDown[i],
                                           context.TotalLiabScenario.YearFraction[i] -
                                           context.TotalLiabScenario.YearFraction[i - 1]);
                IndexMinus1Bp[i] = IndexMinus1Bp[i - 1] *
                                  Math.Pow((double)1 + context.TotalLiabScenario.InflationShiftUp[i],
                                           context.TotalLiabScenario.YearFraction[i] -
                                           context.TotalLiabScenario.YearFraction[i - 1]);
            }

            SalaryEscalation = new double[SalaryEffect.Length];
            Decay = new double[SalaryEffect.Length];

            Decay[0] = context.TotalLiabScenario.YearFraction[0];

            for(var i = 1; i < Decay.Length; i++)
            {
                Decay[i] = 
                    context.TotalLiabScenario.YearFraction[i] > 
                    context.Parameters.Decay ? context.Parameters.Decay : 
                    context.TotalLiabScenario.YearFraction[i];
            }

            SalaryEscalation[0] = NominalCfFixed[0] * ((double)1 + context.Parameters.SalaryEscalation * Decay[0]);

            for (var i = 1; i < SalaryEscalation.Length; i++)
            {
                SalaryEscalation[i] = NominalCfFixed[i] * Math.Pow((double)1 + context.Parameters.SalaryEscalation, Decay[i]);
            }
        }
    }
}
