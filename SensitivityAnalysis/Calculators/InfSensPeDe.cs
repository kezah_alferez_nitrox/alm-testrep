﻿using System;

namespace Sensitivity_Analysis_Calculator.Calculators
{
    public class InfSensPeDe
    {
        public double[] DeferredMembersFixed { get; set; }
        public double[] PensionersFixed { get; set; }
        public double[] NominalCfFixed { get; set; }
        public double[] LiabilityCF { get; set; }
        public double[] ActualCF { get; set; }
        public double[] Inflation { get; set; }

        public double FixedLiabilities { get { return 0; } }

        public double[] PvCf1 { get; set; }
        public double[] PvCf2 { get; set; }

        public double PvCf1Sum;
        public double PvCf2Sum;

        public double PvCfTotal;
        public double PvCfTotalPerCent;

        public double[] YrKrPlusActivities { get; set; }
        public double[,][] CalculatedYrKr { get; set; }


        public void Calculate(CalculContext context)
        {
            Inflation = context.TotalLiabScenario.InflationShiftPerCent;

            //Calculate NominalCfFixed
            var addArrays =
                ArrayTools.Add(context.Projections.DeferredMembersIndexed, context.Projections.PensionersIndexed);

            context.INFSensPeDe.NominalCfFixed = new double[addArrays.Length];

            context.INFSensPeDe.NominalCfFixed[0] = addArrays[0] /
                                        (1 +
                                         context.Projections.Inflation *
                                         context.TotalLiabScenario.YearFraction[0]);

            for (var i = 1; i < addArrays.Length; i++)
                context.INFSensPeDe.NominalCfFixed[i] = addArrays[i] /
                                                        Math.Pow((1 + context.Projections.Inflation),
                                                         context.TotalLiabScenario.YearFraction[i]);

            //Calculate Actual CF
            ActualCF = new double[NominalCfFixed.Length];

            ActualCF[0] = NominalCfFixed[0]*(1 + Inflation[0]);

            for (var i = 1; i < NominalCfFixed.Length; i++)
                ActualCF[i] = NominalCfFixed[i] * Math.Pow((1 + Inflation[i]), context.TotalLiabScenario.YearFraction[i]);


            // PV CF
            PvCf1 = new double[NominalCfFixed.Length];
            PvCf2 = new double[NominalCfFixed.Length];

            PvCf1[0] = 
                context.TotalLiabScenario.DF[0] * 
                NominalCfFixed[0] * 
                (1 + context.TotalLiabScenario.InflationShiftUp[0]);
            PvCf2[0] = context.TotalLiabScenario.DF[0] * 
                NominalCfFixed[0] * 
                (1 + context.TotalLiabScenario.InflationShiftDown[0]);

            for(var i = 1; i < NominalCfFixed.Length; i++)
            {
                PvCf1[i] = context.TotalLiabScenario.DF[i] *
                           NominalCfFixed[i] *
                           Math.Pow(
                               (1 + context.TotalLiabScenario.InflationShiftUp[i] * (1 - FixedLiabilities) +
                                context.Projections.Inflation * FixedLiabilities),
                               context.TotalLiabScenario.YearFraction[i]);
                PvCf2[i] = context.TotalLiabScenario.DF[i] *
                           NominalCfFixed[i] *
                           Math.Pow(
                               (1 + context.TotalLiabScenario.InflationShiftDown[i] * (1 - FixedLiabilities) +
                                context.Projections.Inflation * FixedLiabilities),
                               context.TotalLiabScenario.YearFraction[i]);
            }

            //Calculator
            var calculator = new YrKrCalculator { PvFc1 = PvCf1, PvFc2 = PvCf2 };
            calculator.Calculate();
            context.Overview.CurrentInflationRiskLiabilitiesPensionersDeferreds = calculator.YrKrSummary;

            YrKrPlusActivities = new double[calculator.YrKrSummary.Length];

            YrKrPlusActivities = ArrayTools.Add(calculator.YrKrSummary,
                                                context.Overview.CurrentInflationRiskLiabilitiesActives);

            context.Overview.CurrentInflationRiskLiabilitiesPensionersDeferredsPlusActivities =
                new double[YrKrPlusActivities.Length];
            context.Overview.CurrentInflationRiskLiabilitiesPensionersDeferredsPlusActivities = 
                YrKrPlusActivities;

            CalculatedYrKr = calculator.YrKr;
        }
    }
}