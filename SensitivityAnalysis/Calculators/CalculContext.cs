﻿
namespace Sensitivity_Analysis_Calculator.Calculators
{
    public class CalculContext
    {
        public Parameters Parameters { get; set; }

        public Projections Projections { get; set; }

        public InfSensPeDe INFSensPeDe { get; set; }

        public TotalLiabScenario TotalLiabScenario { get; set; }

        public InfSensAct INFSensAct { get; set; }

        public IRSensPeDe IrSensPeDe { get; set; }

        public IRSensAct IrSensAct { get; set; }

        public Overview Overview { get; set; }

        public FixedAssets FixedAssets { get; set; }

        public ILAssets ILAssets { get; set; }

        public IRSwapExposure IRSwapExposure { get; set; }

        public InflationSwapExposure InflationSwapExposure { get; set; }
    }
}
