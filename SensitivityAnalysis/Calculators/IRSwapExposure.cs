﻿using System.Linq;

namespace Sensitivity_Analysis_Calculator.Calculators
{
    public class IRSwapExposure
    {
        public double[] AssetCF;
        public double[] AssetCF2 { get; set; }

        public double[] PvCf1 { get; set; }
        public double[] PvCf2 { get; set; }
        public double PvCfTotal { get; set; }
        public double PvCfTotalPerCent { get; set; }
        public double[,][] CalculatedYrKr { get; set; }


        public void Calculate(CalculContext context)
        {
            if (context.Overview.CurrentRiskAssetsInflation == null)
                context.Overview.CurrentRiskAssetsInflation = new double[11];
            PvCf1 = new double[context.Projections.InterestRateSwaps.Length];
            PvCf2 = new double[context.Projections.InterestRateSwaps.Length];
            AssetCF = new double[context.Projections.InterestRateSwaps.Length];
            context.Overview.CurrentSwapPortfolioSensitivitiesInterestRate = new double[10];
            context.Overview.CurrentSwapPortfolioSensitivitiesInterestRatePlusAssets =
                new double[10];




            //vars
            for (var i = 0; i < AssetCF.Length; i++)
            {
                //Asset CF
                AssetCF[i] = context.Projections.InterestRateSwaps[i] +
                             context.Projections.InflationSwapsFixedLeg[i];

                //PvCf
                PvCf1[i] = context.TotalLiabScenario.FixedInterestRateDown1[i] *
                           AssetCF[i];
                PvCf2[i] = context.TotalLiabScenario.FixedInterestRateUp1[i] *
                           AssetCF[i];
            }

            PvCfTotal = (PvCf1.Sum() - PvCf2.Sum()) / 2;
            PvCfTotalPerCent = PvCfTotal / PvCf2.Sum();

            //Calc
            var calculator = new YrKrCalculator { PvFc1 = PvCf1, PvFc2 = PvCf2 };
            calculator.Calculate();
            context.Overview.CurrentSwapPortfolioSensitivitiesInterestRate = calculator.YrKrSummary;

            CalculatedYrKr = calculator.YrKr;


            if (!context.Parameters.IncludeCurrentInflationSwapPortfolio)
            {
                for (var i = 0; i < context.Overview.CurrentRiskAssetsInflation.Length; i++)
                    context.Overview.CurrentRiskAssetsInflation[i] = 0;

                for (var i = 0; i < context.Overview.CurrentSwapPortfolioSensitivitiesInterestRate.Length; i++)
                    context.Overview.CurrentSwapPortfolioSensitivitiesInterestRate[i] = 0;

                context.Overview.CurrentSwapPortfolioSensitivitiesInterestRatePlusAssets =
                    context.Overview.CurrentRiskAssetsInterestRate;
            }
            else
            {
                for (var i = 0; i < context.Overview.CurrentSwapPortfolioSensitivitiesInterestRatePlusAssets.Length; i++)
                {
                    context.Overview.CurrentSwapPortfolioSensitivitiesInterestRatePlusAssets[i] =
                        context.Overview.CurrentSwapPortfolioSensitivitiesInterestRate[i] +
                        context.Overview.CurrentRiskAssetsInterestRate[i];
                }
            }

        AssetCF2 = ArrayTools.Add(context.Projections.InterestRateSwaps,
                      context.Projections.InflationSwapsFixedLeg);
        }
    }
}
