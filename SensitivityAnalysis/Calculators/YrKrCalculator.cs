﻿using System.Linq;

namespace Sensitivity_Analysis_Calculator.Calculators
{
    public class YrKrCalculator
    {
        public double[,][] YrKr { get; set; }

        public double[] YrKrSummary { get; set; }

        public double[] PvFc1 { get; set; }
        public double[] PvFc2 { get; set; }


        void CalculateYrKrColumn(int yrkrIndex, int pvfcIndex, double[] pvcf)
        {
            switch (yrkrIndex)
            {
                case 0:
                    YrKr[yrkrIndex, pvfcIndex] = new double[11];
                    //formula0
                    for (var i = 0; i <= 5; i++)
                        YrKr[yrkrIndex, pvfcIndex][i] = pvcf[yrkrIndex*5 + i];

                    for (var i = 6; i <= 10; i++)
                        YrKr[yrkrIndex, pvfcIndex][i] = pvcf[yrkrIndex*5 + i]*(10 - (double)i)/5;
                    break;
                case 9:
                    //formula9
                    var length = pvcf.Length - yrkrIndex * 5;
                    YrKr[yrkrIndex, pvfcIndex] = new double[length];
                    for (var i = 0; i <= 5; i++)
                        YrKr[yrkrIndex, pvfcIndex][i] = pvcf[yrkrIndex * 5 + i] -
                                                        YrKr[yrkrIndex - 1, pvfcIndex][5 + i];

                    for (var i = 6; i < length; i++)
                        YrKr[yrkrIndex, pvfcIndex][i] = pvcf[yrkrIndex*5 + i];
                    break;
                default:
                    YrKr[yrkrIndex, pvfcIndex] = new double[11];
                    //formula generala
                    YrKr[yrkrIndex, pvfcIndex][0] = 0;

                    for (var i = 1; i <= 5; i++)
                        YrKr[yrkrIndex, pvfcIndex][i] = pvcf[yrkrIndex*5 + i] -
                                                        YrKr[yrkrIndex - 1, pvfcIndex][5 + i];

                    for (var i = 6; i <= 10; i++)
                        YrKr[yrkrIndex, pvfcIndex][i] = pvcf[yrkrIndex*5 + i]*(10 - (double)i)/5;
                    break;
            }
        }

        public void Calculate()
        {
            YrKr = new double[10, 2][];
            YrKrSummary = new double[10];

            for (var i = 0; i < 10; i++)
            {
                // calculate (i+1)*5YR KR
                
                // calculate left column YrKr(i)(0)
                CalculateYrKrColumn(i, 0, PvFc1);

                // calculate right column YrKr(i)(1)
                CalculateYrKrColumn(i, 1, PvFc2);

                YrKrSummary[i] = (YrKr[i, 0].Sum() - YrKr[i, 1].Sum()) / 2;
            }
        }
    }
}
