﻿using System.Linq;
using System.Windows.Forms;

namespace Sensitivity_Analysis_Calculator.Sensitivity
{
    public partial class YrKrHeader : UserControl
    {
        public YrKrHeader()
        {
            InitializeComponent();
        }

        public void Init()
        {
            var yrKrNames = Util.YrKrNames;

            _grid.Columns.Add("yrkr", string.Empty);
            _grid.Columns.Add("PV01", "PV 01");

            _grid.Columns["yrkr"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            _grid.Columns["yrkr"].MinimumWidth = 100;
            _grid.Columns["yrkr"].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            _grid.Columns["PV01"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            _grid.Columns["PV01"].MinimumWidth = 100;
            _grid.Columns["PV01"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            _grid.Columns["PV01"].DefaultCellStyle.Format = "N0";

            _grid.AllowUserToAddRows = false;
            _grid.AllowUserToDeleteRows = false;
            _grid.AllowUserToOrderColumns = false;
            _grid.AllowUserToResizeColumns = true;
            _grid.AllowUserToResizeRows = true;
            _grid.ReadOnly = true;

            for (var i = 0; i < yrKrNames.Length; i++)
            {
                _grid.Rows.Add(yrKrNames[i], (double) 0);
            }
            _grid.Rows.Add("Total", (double)0);

            foreach (DataGridViewColumn column in _grid.Columns)
                column.SortMode = DataGridViewColumnSortMode.NotSortable;
        }

        public void PopulateYrKrGrid(double[] yrKr)
        {
            if (yrKr == null) return;

            for (var i = 0; i < yrKr.Length; i++)
            {
                _grid.Rows[i].Cells[1].Value = yrKr[i];
            }

            _grid.Rows[_grid.Rows.Count- 1].Cells[1].Value = yrKr.Sum();
        }

        public void PopulateYrKrGrid(double[] yrKr, string customTitle)
        {
            if (yrKr == null) return;

            _grid.Columns[1].HeaderText = customTitle;

            for (var i = 0; i < yrKr.Length; i++)
            {
                _grid.Rows[i].Cells[1].Value = yrKr[i];
            }

            _grid.Rows[_grid.Rows.Count - 1].Cells[1].Value = yrKr.Sum();
        }
    }
}
