﻿using System.Linq;
using Sensitivity_Analysis_Calculator.Calculators;

namespace Sensitivity_Analysis_Calculator.Sensitivity
{
    class IRSwapExposureTab : AdvancedCalculationsUserControl
    {
        private readonly YrKrHeader _yrKrHeader = new YrKrHeader();
        private readonly YrKrHeader _yrKrHeader2 = new YrKrHeader();

        public override void Init()
        {
            GridInit(null, 63);

            _yrKrHeader.Init();
            _yrKrHeader2.Init();

            AddUserControlTopToPanel(Graph(), _yrKrHeader, _yrKrHeader2);
        }

        public override void Populate(CalculContext context)
        {
            _yrKrHeader.PopulateYrKrGrid(context.Overview.CurrentSwapPortfolioSensitivitiesInterestRatePlusAssets, "PV1 + assets");
            _yrKrHeader2.PopulateYrKrGrid(context.Overview.CurrentSwapPortfolioSensitivitiesInterestRate);
            PopulateGraph(context.Overview.CurrentSwapPortfolioSensitivitiesInterestRate, context.Overview.YrKr);

            AddColumnToGrid(context.TotalLiabScenario.YearFraction, "Year Fraction", 0,"0.00");
            AddColumnToGrid(context.IRSwapExposure.AssetCF2, "Asset CF", 0, "0");
            
            
            AddColumnToGrid(context.TotalLiabScenario.FixedInterestRateDF, "Discount Curve", 0, "0.00%");
            AddColumnToGrid(context.TotalLiabScenario.FixedInterestRateRate, "Underlying IR", 0, "0.00%");
            AddColumnToGrid(context.TotalLiabScenario.FixedInterestRateDown1, "Discount Curve - 1bp", 0, "0.00%");
            AddColumnToGrid(context.TotalLiabScenario.FixedInterestRateUp1, "Discount Curve + 1bp", 0, "0.00%");
            AddColumnToGrid(context.IRSwapExposure.PvCf1, "PV CF1", 0,"0");
            AddColumnToGrid(context.IRSwapExposure.PvCf2, "PV CF2", 0,"0");

            AddYrKrColumns(context.IRSwapExposure.CalculatedYrKr);

            double[] values = new double[]
                                  {
                                      context.IRSwapExposure.CalculatedYrKr[0, 0].Sum(),
                                      context.IRSwapExposure.CalculatedYrKr[0, 1].Sum(),
                                      context.IRSwapExposure.CalculatedYrKr[1, 0].Sum(),
                                      context.IRSwapExposure.CalculatedYrKr[1, 1].Sum(),
                                      context.IRSwapExposure.CalculatedYrKr[2, 0].Sum(),
                                      context.IRSwapExposure.CalculatedYrKr[2, 1].Sum(),
                                      context.IRSwapExposure.CalculatedYrKr[3, 0].Sum(),
                                      context.IRSwapExposure.CalculatedYrKr[3, 1].Sum(),
                                      context.IRSwapExposure.CalculatedYrKr[4, 0].Sum(),
                                      context.IRSwapExposure.CalculatedYrKr[4, 1].Sum(),
                                      context.IRSwapExposure.CalculatedYrKr[5, 0].Sum(),
                                      context.IRSwapExposure.CalculatedYrKr[5, 1].Sum(),
                                      context.IRSwapExposure.CalculatedYrKr[6, 0].Sum(),
                                      context.IRSwapExposure.CalculatedYrKr[6, 1].Sum(),
                                      context.IRSwapExposure.CalculatedYrKr[7, 0].Sum(),
                                      context.IRSwapExposure.CalculatedYrKr[7, 1].Sum(),
                                      context.IRSwapExposure.CalculatedYrKr[8, 0].Sum(),
                                      context.IRSwapExposure.CalculatedYrKr[8, 1].Sum(),
                                      context.IRSwapExposure.CalculatedYrKr[9, 0].Sum(),
                                      context.IRSwapExposure.CalculatedYrKr[9, 1].Sum()
                                  };
            double[] valuesCalc = new double[]
                                      {
                                          CalculateTotal(context.IRSwapExposure.CalculatedYrKr[0, 0].Sum(), context.IRSwapExposure.CalculatedYrKr[0, 1].Sum()),
                                          CalculateTotal(context.IRSwapExposure.CalculatedYrKr[1, 0].Sum(), context.IRSwapExposure.CalculatedYrKr[1, 1].Sum()),
                                          CalculateTotal(context.IRSwapExposure.CalculatedYrKr[2, 0].Sum(), context.IRSwapExposure.CalculatedYrKr[2, 1].Sum()),
                                          CalculateTotal(context.IRSwapExposure.CalculatedYrKr[3, 0].Sum(), context.IRSwapExposure.CalculatedYrKr[3, 1].Sum()),
                                          CalculateTotal(context.IRSwapExposure.CalculatedYrKr[4, 0].Sum(), context.IRSwapExposure.CalculatedYrKr[4, 1].Sum()),
                                          CalculateTotal(context.IRSwapExposure.CalculatedYrKr[5, 0].Sum(), context.IRSwapExposure.CalculatedYrKr[5, 1].Sum()),
                                          CalculateTotal(context.IRSwapExposure.CalculatedYrKr[6, 0].Sum(), context.IRSwapExposure.CalculatedYrKr[6, 1].Sum()),
                                          CalculateTotal(context.IRSwapExposure.CalculatedYrKr[7, 0].Sum(), context.IRSwapExposure.CalculatedYrKr[7, 1].Sum()),
                                          CalculateTotal(context.IRSwapExposure.CalculatedYrKr[8, 0].Sum(), context.IRSwapExposure.CalculatedYrKr[8, 1].Sum()),
                                          CalculateTotal(context.IRSwapExposure.CalculatedYrKr[9, 0].Sum(), context.IRSwapExposure.CalculatedYrKr[9, 1].Sum())
                                      };

            AddSums("5YR KR - 1", values, valuesCalc);
            AddPvCfSums("PV CF1", context.IRSwapExposure.PvCf1.Sum(), context.IRSwapExposure.PvCf2.Sum());
        }
    }
}
