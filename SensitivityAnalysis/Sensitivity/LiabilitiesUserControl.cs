﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using ZedGraph;

namespace Sensitivity_Analysis_Calculator.Sensitivity
{
    public partial class LiabilitiesUserControl : UserControl
    {
        public LiabilitiesUserControl()
        {
            InitializeComponent();

            SetupGrid();

            SetupGraphs();
        }

        private void SetupGrid()
        {
            _grid.AutoGenerateColumns = false;
            _grid.AllowUserToAddRows = false;

            Util.AddColumnToGrid(_grid, "Year");
            Util.AddColumnToGrid(_grid, "Active Members INDEXED");
            Util.AddColumnToGrid(_grid, "Deferred Members INDEXED");
            Util.AddColumnToGrid(_grid, "Pensioners INDEXED");

            Util.AddColumnToGrid(_grid, "Active Members FIXED");
            Util.AddColumnToGrid(_grid, "Deferred Members FIXED");
            Util.AddColumnToGrid(_grid, "Pensioners FIXED");
            Util.AddColumnToGrid(_grid, "Total");
            Util.AddColumnToGrid(_grid, "Year");

            Util.AddColumnToGrid(_grid, "Real Cashflow");
            Util.AddColumnToGrid(_grid, "Indexed Cashflow");
            Util.AddColumnToGrid(_grid, "Uplift Cashflow");
            Util.AddColumnToGrid(_grid, "Fixed Cashflow");


            foreach (DataGridViewColumn column in _grid.Columns)
                if (column.HeaderText == "Year")
                {
                    column.DefaultCellStyle.Format = "0";
                    column.DefaultCellStyle.BackColor = Color.Yellow;
                }
                else
                    column.DefaultCellStyle.Format = "N0";


            _grid.Columns[0].ReadOnly = true;

            for (var i = 7; i <= _grid.Columns.Count - 1; i++)
                _grid.Columns[i].ReadOnly = true;
          
            _grid.AllowUserToAddRows = false;
            _grid.AllowUserToDeleteRows = false;
            _grid.AllowUserToOrderColumns = false;
            _grid.AllowUserToResizeColumns = true;
            _grid.AllowUserToResizeRows = true;

            foreach (DataGridViewColumn column in _grid.Columns)
                column.SortMode = DataGridViewColumnSortMode.NotSortable;
        }

        private void SetupGraphs()
        {
            _zedGraphControl1.GraphPane.BarSettings.Type = BarType.Stack;
            _zedGraphControl1.GraphPane.BarSettings.MinClusterGap = 0.3f;

            _zedGraphControl1.GraphPane.Title.Text = "Annual Liability Cash Flow (in EURm)";
            _zedGraphControl2.GraphPane.Title.Text = "Annual Liability Cash Flow (in EURm)";

            _zedGraphControl1.GraphPane.XAxis.Title.Text = "Years";
            _zedGraphControl2.GraphPane.XAxis.Title.Text = "Years";

            _zedGraphControl1.GraphPane.YAxis.Title.IsVisible = false;
            _zedGraphControl2.GraphPane.YAxis.Title.IsVisible = false;

            _zedGraphControl1.DoubleClick+=new EventHandler(_zedGraphControl_DoubleClick);
            _zedGraphControl2.DoubleClick+=new EventHandler(_zedGraphControl_DoubleClick);
        }

        private void LiabilitiesUserControl_Load(object sender, EventArgs e)
        {
            if (DesignMode || (LicenseManager.UsageMode == LicenseUsageMode.Designtime)) return;
        }

        private void _grid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void _zedGraphControl1_Load(object sender, EventArgs e)
        {

        }

        private void _zedGraphControl_DoubleClick(object sender, EventArgs e)
        {
            new ZedGraphPopUp().ShowZedGraphInPopUp((ZedGraphControl)sender);
        }

        private void _grid_KeyDown(object sender, KeyEventArgs e)
        {
            if (_grid.Rows.Count == 0)
                _grid.Rows.Add(1);

            if (e.Control && e.KeyCode == Keys.C)
            {
                DataObject d = _grid.GetClipboardContent();
                Clipboard.SetDataObject(d);
                e.Handled = true;
            }
            else if(e.Control && e.KeyCode == Keys.V &&
                _grid.CurrentCell.ColumnIndex >= 1 &&
                _grid.CurrentCell.ColumnIndex <= 6)
            {
                var s = Clipboard.GetText();
                string[] lines = s.Split('\n');
                var row = _grid.CurrentCell.RowIndex;
                var col = _grid.CurrentCell.ColumnIndex;

                if (lines.Length > _grid.Rows.Count)
                    _grid.Rows.Add(lines.Length - _grid.Rows.Count);

                foreach (var line in lines)
                {
                    if (row < _grid.RowCount && line.Length > 0)
                    {
                        string[] cells = line.Split('\t');
                        for (var i = 0; i < cells.GetLength(0); ++i)
                        {
                            if (col + i < _grid.ColumnCount)
                            {
                                double tmp;
                                if (double.TryParse(cells[i], out tmp) &&
                                    col + i <= 6)
                                    _grid[col + i, row].Value = tmp;
                            }
                            else
                            {
                                break;
                            }
                        }
                        row++;
                    }
                    else
                    {
                        break;
                    }
                }

                if (_grid.Rows.Count > 63)
                    for (var k = 0; k < _grid.Rows.Count - 63; k++)
                        _grid.Rows.Remove(_grid.Rows[_grid.Rows.Count - 1]);
            }
            else if (e.KeyData == Keys.Delete)
            {
                foreach (DataGridViewTextBoxCell cell in _grid.SelectedCells)
                {
                    cell.Value = 0;
                }
            }
        }

        private void _grid_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            // Clear the row error in case the user presses ESC.
            _grid.Rows[e.RowIndex].ErrorText = String.Empty;
        }

        private void _grid_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
                double tmp;
                if (double.TryParse(e.FormattedValue.ToString(), out tmp))
                {
                    _grid.Rows[e.RowIndex].Cells[e.ColumnIndex].ValueType = typeof(double);
                    _grid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = tmp;
                }
                else
                {
                    _grid.Rows[e.RowIndex].ErrorText = "Numeric value expected!";
                    e.Cancel = true;
                }
        }
    }
}