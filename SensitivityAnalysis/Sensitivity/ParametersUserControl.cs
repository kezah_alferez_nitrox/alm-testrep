﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Windows.Forms;

namespace Sensitivity_Analysis_Calculator.Sensitivity
{
    public partial class ParametersUserControl : UserControl
    {
        public class ParameterObj : INotifyPropertyChanged
        {
            private string _title;
            public string Title
            {
                get { return _title; }
                set { _title = value; this.NotifyPropertyChanged("Title"); }
            }

            private bool _enabled;
            public bool Enabled
            {
                get { return _enabled; }
                set { _enabled = value; this.NotifyPropertyChanged("Enabled"); }
            }

            private double _value;
            public double Value
            {
                get { return _value; }
                set { _value = value; this.NotifyPropertyChanged("Value"); }
            }

            public ParameterObj(string title, bool enabled, double value)
            {
                Title = title;
                Enabled = enabled;
                Value = value;
            }
            
            public event PropertyChangedEventHandler PropertyChanged;
            
            private void NotifyPropertyChanged(string name)
            {
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
            
        }

        private readonly BindingList<ParameterObj> _parameterList = new BindingList<ParameterObj>();
        public BindingList<ParameterObj> ParameterList
        {
            get { return _parameterList; }
        }

        public ParametersUserControl()
        {
            InitializeComponent();
            InitGrid();
        }

        public void SetupGrid(Calculators.CalculContext context)
        {
             ParameterList.Clear();

             ParameterList.Add(new ParameterObj("ASSETS Inflation (Market Rates) (%)",
                                                  context.Parameters.AssetsInflationMarketRatesIsSet,
                                                  context.Parameters.AssetsInflationMarketRates));
             ParameterList.Add(new ParameterObj("ASSETS Interest Rate (Market Rates) (%)",
                                                  context.Parameters.AssetsInterestRateMarketRatesIsSet,
                                                  context.Parameters.AssetsInterestRateMarketRates));
             ParameterList.Add(new ParameterObj("LIABILITIES Inflation (Market Rates) (%)",
                                                  context.Parameters.LiabilitiesInflationMarketRatesIsSet,
                                                  context.Parameters.LiabilitiesInflationMarketRates));
             ParameterList.Add(new ParameterObj("LIABILITIES Interest Rate (Market Rates) (%)",
                                                  context.Parameters.
                                                      LiabilitiesInterestRateMarketRatesIsSet,
                                                  context.Parameters.LiabilitiesInterestRateMarketRates));
             ParameterList.Add(new ParameterObj("Salary Escalation (%)", false,
                                                  context.Parameters.SalaryEscalation));
             ParameterList.Add(new ParameterObj("Salary Decay Factor", false,
                                                  context.Parameters.SalaryDecayFactor));
             ParameterList.Add(new ParameterObj("Indexation Level (% of CPI)",
                                                  context.Parameters.IndexationLevelPercentofCpiIsSet,
                                                  context.Parameters.IndexationLevelPercentofCpi));
             ParameterList.Add(new ParameterObj("Hedge Ratio - Liability Interest Rate Risk (%)", false,
                                                  context.Parameters.
                                                      HedgeRatioLiabilityInterestRateRisk));
             ParameterList.Add(new ParameterObj("Hedge Ratio - Liability Inflation Risk (%)", false,
                                                  context.Parameters.HedgeRatioLiabilityInflationRisk));
             ParameterList.Add(new ParameterObj("Include Current IR Swap Portfolio",
                                                  context.Parameters.IncludeCurrentIRSwapPortfolio,
                                                  (double)0));
             ParameterList.Add(new ParameterObj("Include Current Inflation Swap Portfolio",
                                                  context.Parameters.
                                                      IncludeCurrentInflationSwapPortfolio, (double)0));
            _grid.DataSource = ParameterList;
        }

        void InitGrid()
        {
            var column = Util.AddColumnToGrid(_grid, "Parameter");
            column.MinimumWidth = 298;
            column.DataPropertyName = "Title";
            column.ReadOnly = true;
            
            column = Util.AddColumnToGrid(_grid, "Include", () => new DataGridViewCheckBoxColumn());
            column.Width = 50;
            column.MinimumWidth = 50;
            column.DataPropertyName = "Enabled";

            column = Util.AddColumnToGrid(_grid, "Value");
            column.Width = 100;
            column.MinimumWidth = 100;
            column.DefaultCellStyle.Format = "0.00%";
            column.DataPropertyName = "Value";

            _grid.AllowUserToAddRows = false;
            _grid.AllowUserToDeleteRows = false;
            _grid.AllowUserToOrderColumns = false;
            _grid.AllowUserToResizeColumns = true;
            _grid.AllowUserToResizeRows = true;

            foreach (DataGridViewColumn col in _grid.Columns)
            {
                col.SortMode = DataGridViewColumnSortMode.NotSortable;
                column.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            }
        }

        private void ParametersUserControl_Load(object sender, EventArgs e)
        {

        }

        private void _grid_KeyDown(object sender, KeyEventArgs e)
        {
            return;
        }

        private void _grid_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            // Clear the row error in case the user presses ESC.
            _grid.Rows[e.RowIndex].ErrorText = String.Empty;
        }

        private void _grid_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {

            if (e.ColumnIndex == 2 && _grid.Rows[e.RowIndex].Cells[e.ColumnIndex].IsInEditMode)
            {
                double tmp;
                if (double.TryParse(e.FormattedValue.ToString().Replace("%", ""), out tmp))
                {
                    _grid.Rows[e.RowIndex].Cells[e.ColumnIndex].ValueType = typeof (double);
                    _grid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = e.RowIndex == 5 ? tmp : tmp / (double)100;
                }
                else
                {
                    _grid.Rows[e.RowIndex].ErrorText = "Numeric value expected!";
                    e.Cancel = true;
                }
            }
        }

        private void _grid_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if ((e.RowIndex == 5 || e.RowIndex == 9 || e.RowIndex == 10) && e.ColumnIndex == 2)
            {
                e.Value = e.Value.ToString();
                e.FormattingApplied = true;
            }
        }

    }
}