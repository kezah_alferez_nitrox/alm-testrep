﻿using System.Linq;
using Sensitivity_Analysis_Calculator.Calculators;

namespace Sensitivity_Analysis_Calculator.Sensitivity
{
    class FixedAssetsTab : AdvancedCalculationsUserControl
    {
        readonly YrKrHeader _yrKrHeader = new YrKrHeader();

        public override void Init()
        {
            GridInit(null, 63);

            _yrKrHeader.Init();

            AddUserControlTopToPanel(Graph(), _yrKrHeader);
        }

        public override void Populate(CalculContext context)
        {
            _yrKrHeader.PopulateYrKrGrid(context.Overview.CurrentRiskAssetsInterestRate);
            PopulateGraph(context.Overview.CurrentRiskAssetsInterestRate, context.Overview.YrKr);

            AddColumnToGrid(context.Projections.Year, "Year", 0, "0");
            AddColumnToGrid(context.FixedAssets.AssetCF, "Asset CF", 0, "N0");
            AddColumnToGrid(context.TotalLiabScenario.FixedInterestRateDF, "Discount Curve", 0, "0.00%");
            AddColumnToGrid(context.TotalLiabScenario.FixedInterestRateRate, "Underlying IR", 0, "0.00%");
            AddColumnToGrid(context.TotalLiabScenario.FixedInterestRateUp1, "Discount Curve -1bp", 0, "0.00%");
            AddColumnToGrid(context.TotalLiabScenario.FixedInterestRateDown1, "Discount Curve +1bp", 0, "0.00%");
            AddColumnToGrid(context.FixedAssets.PvCf1, "PV CF1", 0, "N0");
            AddColumnToGrid(context.FixedAssets.PvCf2, "PV CF2", 0, "N0");

            AddYrKrColumns(context.FixedAssets.CalculatedYrKr);

            //double[] values = new double[]
            //                      {
            //                          context.FixedAssets.CalculatedYrKr[0, 0].Sum(),
            //                          context.FixedAssets.CalculatedYrKr[0, 1].Sum(),
            //                          context.FixedAssets.CalculatedYrKr[1, 0].Sum(),
            //                          context.FixedAssets.CalculatedYrKr[1, 1].Sum(),
            //                          context.FixedAssets.CalculatedYrKr[2, 0].Sum(),
            //                          context.FixedAssets.CalculatedYrKr[2, 1].Sum(),
            //                          context.FixedAssets.CalculatedYrKr[3, 0].Sum(),
            //                          context.FixedAssets.CalculatedYrKr[3, 1].Sum(),
            //                          context.FixedAssets.CalculatedYrKr[4, 0].Sum(),
            //                          context.FixedAssets.CalculatedYrKr[4, 1].Sum(),
            //                          context.FixedAssets.CalculatedYrKr[5, 0].Sum(),
            //                          context.FixedAssets.CalculatedYrKr[5, 1].Sum(),
            //                          context.FixedAssets.CalculatedYrKr[6, 0].Sum(),
            //                          context.FixedAssets.CalculatedYrKr[6, 1].Sum(),
            //                          context.FixedAssets.CalculatedYrKr[7, 0].Sum(),
            //                          context.FixedAssets.CalculatedYrKr[7, 1].Sum(),
            //                          context.FixedAssets.CalculatedYrKr[8, 0].Sum(),
            //                          context.FixedAssets.CalculatedYrKr[8, 1].Sum(),
            //                          context.FixedAssets.CalculatedYrKr[9, 0].Sum(),
            //                          context.FixedAssets.CalculatedYrKr[9, 1].Sum()
            //                      };
            //double[] valuesCalc = new double[]
            //                          {
            //                              CalculateTotal(context.FixedAssets.CalculatedYrKr[0, 0].Sum(), context.FixedAssets.CalculatedYrKr[0, 1].Sum()),
            //                              CalculateTotal(context.FixedAssets.CalculatedYrKr[1, 0].Sum(), context.FixedAssets.CalculatedYrKr[1, 1].Sum()),
            //                              CalculateTotal(context.FixedAssets.CalculatedYrKr[2, 0].Sum(), context.FixedAssets.CalculatedYrKr[2, 1].Sum()),
            //                              CalculateTotal(context.FixedAssets.CalculatedYrKr[3, 0].Sum(), context.FixedAssets.CalculatedYrKr[3, 1].Sum()),
            //                              CalculateTotal(context.FixedAssets.CalculatedYrKr[4, 0].Sum(), context.FixedAssets.CalculatedYrKr[4, 1].Sum()),
            //                              CalculateTotal(context.FixedAssets.CalculatedYrKr[5, 0].Sum(), context.FixedAssets.CalculatedYrKr[5, 1].Sum()),
            //                              CalculateTotal(context.FixedAssets.CalculatedYrKr[6, 0].Sum(), context.FixedAssets.CalculatedYrKr[6, 1].Sum()),
            //                              CalculateTotal(context.FixedAssets.CalculatedYrKr[7, 0].Sum(), context.FixedAssets.CalculatedYrKr[7, 1].Sum()),
            //                              CalculateTotal(context.FixedAssets.CalculatedYrKr[8, 0].Sum(), context.FixedAssets.CalculatedYrKr[8, 1].Sum()),
            //                              CalculateTotal(context.FixedAssets.CalculatedYrKr[9, 0].Sum(), context.FixedAssets.CalculatedYrKr[9, 1].Sum())
            //                          };

            //AddSums("5YR KR - 1", values, valuesCalc);
            //AddPvCfSums("PV CF1", context.FixedAssets.PvCf1.Sum(), context.FixedAssets.PvCf2.Sum());
        }
    }
}
