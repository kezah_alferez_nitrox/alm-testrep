﻿namespace Sensitivity_Analysis_Calculator.Sensitivity
{
    partial class SensitivityForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private readonly System.ComponentModel.IContainer _components;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (_components != null))
            {
                _components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SensitivityForm));
            this._mainTabControl = new System.Windows.Forms.TabControl();
            this._liabilitiesTabPage = new System.Windows.Forms.TabPage();
            this._liabilitiesUserControl = new Sensitivity_Analysis_Calculator.Sensitivity.LiabilitiesUserControl();
            this._assetsTabPage = new System.Windows.Forms.TabPage();
            this._assetsUserControl = new Sensitivity_Analysis_Calculator.Sensitivity.AssetsUserControl();
            this._overviewTabPage = new System.Windows.Forms.TabPage();
            this._overviewUserControl1 = new Sensitivity_Analysis_Calculator.Sensitivity.OverviewUserControl();
            this._mainMenuStrip = new System.Windows.Forms.MenuStrip();
            this._fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveModelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadModelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.saveModelDefinitionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadModelDefinitionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this._exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.calculateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._panel1 = new System.Windows.Forms.Panel();
            this.toolStrip = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonCalculate = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonShowAdvanced = new System.Windows.Forms.ToolStripButton();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this._mainTabControl.SuspendLayout();
            this._liabilitiesTabPage.SuspendLayout();
            this._assetsTabPage.SuspendLayout();
            this._overviewTabPage.SuspendLayout();
            this._mainMenuStrip.SuspendLayout();
            this._panel1.SuspendLayout();
            this.toolStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // _mainTabControl
            // 
            this._mainTabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this._mainTabControl.Controls.Add(this._liabilitiesTabPage);
            this._mainTabControl.Controls.Add(this._assetsTabPage);
            this._mainTabControl.Controls.Add(this._overviewTabPage);
            this._mainTabControl.Location = new System.Drawing.Point(3, 41);
            this._mainTabControl.Name = "_mainTabControl";
            this._mainTabControl.SelectedIndex = 0;
            this._mainTabControl.Size = new System.Drawing.Size(1060, 524);
            this._mainTabControl.TabIndex = 0;
            // 
            // _liabilitiesTabPage
            // 
            this._liabilitiesTabPage.Controls.Add(this._liabilitiesUserControl);
            this._liabilitiesTabPage.Location = new System.Drawing.Point(4, 22);
            this._liabilitiesTabPage.Name = "_liabilitiesTabPage";
            this._liabilitiesTabPage.Padding = new System.Windows.Forms.Padding(3);
            this._liabilitiesTabPage.Size = new System.Drawing.Size(1052, 498);
            this._liabilitiesTabPage.TabIndex = 0;
            this._liabilitiesTabPage.Text = "Projections Liabilities";
            this._liabilitiesTabPage.UseVisualStyleBackColor = true;
            // 
            // _liabilitiesUserControl
            // 
            this._liabilitiesUserControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._liabilitiesUserControl.Location = new System.Drawing.Point(3, 3);
            this._liabilitiesUserControl.Name = "_liabilitiesUserControl";
            this._liabilitiesUserControl.Size = new System.Drawing.Size(1046, 492);
            this._liabilitiesUserControl.TabIndex = 0;
            // 
            // _assetsTabPage
            // 
            this._assetsTabPage.Controls.Add(this._assetsUserControl);
            this._assetsTabPage.Location = new System.Drawing.Point(4, 22);
            this._assetsTabPage.Name = "_assetsTabPage";
            this._assetsTabPage.Padding = new System.Windows.Forms.Padding(3);
            this._assetsTabPage.Size = new System.Drawing.Size(1052, 498);
            this._assetsTabPage.TabIndex = 1;
            this._assetsTabPage.Text = "Projections Assets";
            this._assetsTabPage.UseVisualStyleBackColor = true;
            // 
            // _assetsUserControl
            // 
            this._assetsUserControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._assetsUserControl.Location = new System.Drawing.Point(3, 3);
            this._assetsUserControl.Name = "_assetsUserControl";
            this._assetsUserControl.Size = new System.Drawing.Size(1046, 492);
            this._assetsUserControl.TabIndex = 0;
            // 
            // _overviewTabPage
            // 
            this._overviewTabPage.Controls.Add(this._overviewUserControl1);
            this._overviewTabPage.Location = new System.Drawing.Point(4, 22);
            this._overviewTabPage.Name = "_overviewTabPage";
            this._overviewTabPage.Size = new System.Drawing.Size(1052, 498);
            this._overviewTabPage.TabIndex = 3;
            this._overviewTabPage.Text = "Overview";
            this._overviewTabPage.UseVisualStyleBackColor = true;
            // 
            // _overviewUserControl1
            // 
            this._overviewUserControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this._overviewUserControl1.Location = new System.Drawing.Point(0, 0);
            this._overviewUserControl1.Name = "_overviewUserControl1";
            this._overviewUserControl1.Size = new System.Drawing.Size(1052, 498);
            this._overviewUserControl1.TabIndex = 0;
            // 
            // _mainMenuStrip
            // 
            this._mainMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._fileToolStripMenuItem,
            this._editToolStripMenuItem,
            this._toolsToolStripMenuItem,
            this._helpToolStripMenuItem});
            this._mainMenuStrip.Location = new System.Drawing.Point(0, 0);
            this._mainMenuStrip.Name = "_mainMenuStrip";
            this._mainMenuStrip.Size = new System.Drawing.Size(1060, 24);
            this._mainMenuStrip.TabIndex = 2;
            this._mainMenuStrip.Text = "menuStrip1";
            // 
            // _fileToolStripMenuItem
            // 
            this._fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveModelToolStripMenuItem,
            this.loadModelToolStripMenuItem,
            this.toolStripMenuItem1,
            this.saveModelDefinitionToolStripMenuItem,
            this.loadModelDefinitionToolStripMenuItem,
            this.toolStripMenuItem2,
            this._exitToolStripMenuItem});
            this._fileToolStripMenuItem.Name = "_fileToolStripMenuItem";
            this._fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this._fileToolStripMenuItem.Text = "&File";
            // 
            // saveModelToolStripMenuItem
            // 
            this.saveModelToolStripMenuItem.Name = "saveModelToolStripMenuItem";
            this.saveModelToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.saveModelToolStripMenuItem.Text = "&Save Model";
            this.saveModelToolStripMenuItem.Click += new System.EventHandler(this.saveModelToolStripMenuItem_Click);
            // 
            // loadModelToolStripMenuItem
            // 
            this.loadModelToolStripMenuItem.Name = "loadModelToolStripMenuItem";
            this.loadModelToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.loadModelToolStripMenuItem.Text = "&Load Model";
            this.loadModelToolStripMenuItem.Click += new System.EventHandler(this.loadModelToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(189, 6);
            // 
            // saveModelDefinitionToolStripMenuItem
            // 
            this.saveModelDefinitionToolStripMenuItem.Name = "saveModelDefinitionToolStripMenuItem";
            this.saveModelDefinitionToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.saveModelDefinitionToolStripMenuItem.Text = "Save Model &Definition";
            this.saveModelDefinitionToolStripMenuItem.Click += new System.EventHandler(this.saveModelDefinitionToolStripMenuItem_Click);
            // 
            // loadModelDefinitionToolStripMenuItem
            // 
            this.loadModelDefinitionToolStripMenuItem.Name = "loadModelDefinitionToolStripMenuItem";
            this.loadModelDefinitionToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.loadModelDefinitionToolStripMenuItem.Text = "Load &Model Definition";
            this.loadModelDefinitionToolStripMenuItem.Click += new System.EventHandler(this.loadModelDefinitionToolStripMenuItem_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(189, 6);
            // 
            // _exitToolStripMenuItem
            // 
            this._exitToolStripMenuItem.Name = "_exitToolStripMenuItem";
            this._exitToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this._exitToolStripMenuItem.Text = "E&xit";
            this._exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // _editToolStripMenuItem
            // 
            this._editToolStripMenuItem.Name = "_editToolStripMenuItem";
            this._editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this._editToolStripMenuItem.Text = "&Edit";
            // 
            // _toolsToolStripMenuItem
            // 
            this._toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.calculateToolStripMenuItem});
            this._toolsToolStripMenuItem.Name = "_toolsToolStripMenuItem";
            this._toolsToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this._toolsToolStripMenuItem.Text = "&Tools";
            // 
            // calculateToolStripMenuItem
            // 
            this.calculateToolStripMenuItem.Name = "calculateToolStripMenuItem";
            this.calculateToolStripMenuItem.Size = new System.Drawing.Size(123, 22);
            this.calculateToolStripMenuItem.Text = "&Calculate";
            this.calculateToolStripMenuItem.Click += new System.EventHandler(this.calculateToolStripMenuItem_Click);
            // 
            // _helpToolStripMenuItem
            // 
            this._helpToolStripMenuItem.Name = "_helpToolStripMenuItem";
            this._helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this._helpToolStripMenuItem.Text = "&Help";
            // 
            // _panel1
            // 
            this._panel1.Controls.Add(this.toolStrip);
            this._panel1.Controls.Add(this._mainTabControl);
            this._panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this._panel1.Location = new System.Drawing.Point(0, 24);
            this._panel1.Name = "_panel1";
            this._panel1.Size = new System.Drawing.Size(1060, 561);
            this._panel1.TabIndex = 7;
            // 
            // toolStrip
            // 
            this.toolStrip.AllowItemReorder = true;
            this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonCalculate,
            this.toolStripSeparator1,
            this.toolStripButtonShowAdvanced});
            this.toolStrip.Location = new System.Drawing.Point(0, 0);
            this.toolStrip.Name = "toolStrip";
            this.toolStrip.Size = new System.Drawing.Size(1060, 25);
            this.toolStrip.TabIndex = 1;
            this.toolStrip.Text = "toolStrip1";
            // 
            // toolStripButtonCalculate
            // 
            this.toolStripButtonCalculate.AccessibleDescription = "Calculate";
            this.toolStripButtonCalculate.AccessibleName = "Calculate";
            this.toolStripButtonCalculate.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonCalculate.Image")));
            this.toolStripButtonCalculate.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonCalculate.Name = "toolStripButtonCalculate";
            this.toolStripButtonCalculate.Size = new System.Drawing.Size(76, 22);
            this.toolStripButtonCalculate.Text = "Calculate";
            this.toolStripButtonCalculate.Click += new System.EventHandler(this.toolStripButtonCalculate_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButtonShowAdvanced
            // 
            this.toolStripButtonShowAdvanced.CheckOnClick = true;
            this.toolStripButtonShowAdvanced.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonShowAdvanced.Image")));
            this.toolStripButtonShowAdvanced.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonShowAdvanced.Name = "toolStripButtonShowAdvanced";
            this.toolStripButtonShowAdvanced.Size = new System.Drawing.Size(148, 22);
            this.toolStripButtonShowAdvanced.Text = "Advanced Calculations";
            this.toolStripButtonShowAdvanced.CheckedChanged += new System.EventHandler(this.toolStripButtonShowAdvanced_CheckedChanged);
            this.toolStripButtonShowAdvanced.CheckStateChanged += new System.EventHandler(this.toolStripButtonShowAdvanced_CheckStateChanged);
            this.toolStripButtonShowAdvanced.Click += new System.EventHandler(this.toolStripButtonShowAdvanced_Click);
            // 
            // SensitivityForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1060, 585);
            this.Controls.Add(this._panel1);
            this.Controls.Add(this._mainMenuStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this._mainMenuStrip;
            this.Name = "SensitivityForm";
            this.Text = "Sensitivity";
            this.Load += new System.EventHandler(this.SensitivityForm_Load);
            this._mainTabControl.ResumeLayout(false);
            this._liabilitiesTabPage.ResumeLayout(false);
            this._assetsTabPage.ResumeLayout(false);
            this._overviewTabPage.ResumeLayout(false);
            this._mainMenuStrip.ResumeLayout(false);
            this._mainMenuStrip.PerformLayout();
            this._panel1.ResumeLayout(false);
            this._panel1.PerformLayout();
            this.toolStrip.ResumeLayout(false);
            this.toolStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl _mainTabControl;
        private System.Windows.Forms.TabPage _liabilitiesTabPage;
        private System.Windows.Forms.TabPage _assetsTabPage;
        private System.Windows.Forms.TabPage _overviewTabPage;
        private AssetsUserControl _assetsUserControl;
        //private OverviewUserControl _overviewUserControl;
        private LiabilitiesUserControl _liabilitiesUserControl;
        private System.Windows.Forms.MenuStrip _mainMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem _fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _helpToolStripMenuItem;
        private System.Windows.Forms.Panel _panel1;
        private System.Windows.Forms.ToolStrip toolStrip;
        private System.Windows.Forms.ToolStripButton toolStripButtonCalculate;
        public OverviewUserControl _overviewUserControl1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.ToolStripMenuItem saveModelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveModelDefinitionToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem loadModelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadModelDefinitionToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem calculateToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton toolStripButtonShowAdvanced;
    }
}