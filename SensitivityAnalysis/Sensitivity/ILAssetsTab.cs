﻿using System.Linq;
using Sensitivity_Analysis_Calculator.Calculators;

namespace Sensitivity_Analysis_Calculator.Sensitivity
{
    class ILAssetsTab : AdvancedCalculationsUserControl
    {
        private readonly YrKrHeader _yrKrHeader = new YrKrHeader();

        public override void Init()
        {
            GridInit(null, 63);

            _yrKrHeader.Init();

            AddUserControlTopToPanel(Graph(), _yrKrHeader);
        }

        public override void Populate(CalculContext context)
        {
            _yrKrHeader.PopulateYrKrGrid(context.Overview.CurrentRiskAssetsInflation);
            PopulateGraph(context.Overview.CurrentRiskAssetsInflation, context.Overview.YrKr);

            AddColumnToGrid(context.Projections.Year, "Year", 0,"0");
            AddColumnToGrid(context.TotalLiabScenario.YearFraction, "Year Fraction", 0,"N0");
            AddColumnToGrid(context.TotalLiabScenario.FixedInterestRateDF, "Discount Curve", 0, "0.00%");
            AddColumnToGrid(context.TotalLiabScenario.InflationIndexPrivate, "Inflation Index", 0,"N0");
            AddColumnToGrid(context.TotalLiabScenario.InflationAssetsShiftPerCent, "Inflation", 0, "0.00%");
            AddColumnToGrid(context.TotalLiabScenario.IndexPlus1BpPrivate, "Index + 1bp (1)", 0,"N0");
            AddColumnToGrid(context.TotalLiabScenario.InflationAssetsShiftUp, "Inflation + 1bp (1)", 0, "0.00%");
            AddColumnToGrid(context.TotalLiabScenario.IndexMinus1BpPrivate, "Index + 1bp (2)", 0,"N0");
            AddColumnToGrid(context.TotalLiabScenario.InflationAssetsShiftDown, "Inflation - 1bp (2)", 0, "0.00%");
            AddColumnToGrid(context.ILAssets.AssetCF, "Asset CF", 0,"N0");
            AddColumnToGrid(context.ILAssets.PvCf1, "PV CF1", 0,"N0");
            AddColumnToGrid(context.ILAssets.PvCf2, "PV CF2", 0,"N0");

            AddYrKrColumns(context.ILAssets.CalculatedYrKr);

            double[] values = new double[]
                                  {
                                      context.ILAssets.CalculatedYrKr[0, 0].Sum(),
                                      context.ILAssets.CalculatedYrKr[0, 1].Sum(),
                                      context.ILAssets.CalculatedYrKr[1, 0].Sum(),
                                      context.ILAssets.CalculatedYrKr[1, 1].Sum(),
                                      context.ILAssets.CalculatedYrKr[2, 0].Sum(),
                                      context.ILAssets.CalculatedYrKr[2, 1].Sum(),
                                      context.ILAssets.CalculatedYrKr[3, 0].Sum(),
                                      context.ILAssets.CalculatedYrKr[3, 1].Sum(),
                                      context.ILAssets.CalculatedYrKr[4, 0].Sum(),
                                      context.ILAssets.CalculatedYrKr[4, 1].Sum(),
                                      context.ILAssets.CalculatedYrKr[5, 0].Sum(),
                                      context.ILAssets.CalculatedYrKr[5, 1].Sum(),
                                      context.ILAssets.CalculatedYrKr[6, 0].Sum(),
                                      context.ILAssets.CalculatedYrKr[6, 1].Sum(),
                                      context.ILAssets.CalculatedYrKr[7, 0].Sum(),
                                      context.ILAssets.CalculatedYrKr[7, 1].Sum(),
                                      context.ILAssets.CalculatedYrKr[8, 0].Sum(),
                                      context.ILAssets.CalculatedYrKr[8, 1].Sum(),
                                      context.ILAssets.CalculatedYrKr[9, 0].Sum(),
                                      context.ILAssets.CalculatedYrKr[9, 1].Sum()
                                  };
            double[] valuesCalc = new double[]
                                      {
                                          CalculateTotal(context.ILAssets.CalculatedYrKr[0, 0].Sum(), context.ILAssets.CalculatedYrKr[0, 1].Sum()),
                                          CalculateTotal(context.ILAssets.CalculatedYrKr[1, 0].Sum(), context.ILAssets.CalculatedYrKr[1, 1].Sum()),
                                          CalculateTotal(context.ILAssets.CalculatedYrKr[2, 0].Sum(), context.ILAssets.CalculatedYrKr[2, 1].Sum()),
                                          CalculateTotal(context.ILAssets.CalculatedYrKr[3, 0].Sum(), context.ILAssets.CalculatedYrKr[3, 1].Sum()),
                                          CalculateTotal(context.ILAssets.CalculatedYrKr[4, 0].Sum(), context.ILAssets.CalculatedYrKr[4, 1].Sum()),
                                          CalculateTotal(context.ILAssets.CalculatedYrKr[5, 0].Sum(), context.ILAssets.CalculatedYrKr[5, 1].Sum()),
                                          CalculateTotal(context.ILAssets.CalculatedYrKr[6, 0].Sum(), context.ILAssets.CalculatedYrKr[6, 1].Sum()),
                                          CalculateTotal(context.ILAssets.CalculatedYrKr[7, 0].Sum(), context.ILAssets.CalculatedYrKr[7, 1].Sum()),
                                          CalculateTotal(context.ILAssets.CalculatedYrKr[8, 0].Sum(), context.ILAssets.CalculatedYrKr[8, 1].Sum()),
                                          CalculateTotal(context.ILAssets.CalculatedYrKr[9, 0].Sum(), context.ILAssets.CalculatedYrKr[9, 1].Sum())
                                      };

            AddSums("5YR KR - 1", values, valuesCalc);
        }
    }
}
