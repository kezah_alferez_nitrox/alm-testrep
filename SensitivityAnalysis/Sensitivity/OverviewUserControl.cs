﻿using System;
//using System.Drawing;
using System.Drawing;
using System.Windows.Forms;
using ZedGraph;

namespace Sensitivity_Analysis_Calculator.Sensitivity
{
    public partial class OverviewUserControl : UserControl
    {
        public string[] GridNames
        {
            get
            {
                return new[]
                           {
                               "CURRENT INTEREST RATE RISK - LIABILITIES",
                               "CURRENT INFLATION RISK - LIABILITIES",
                               "CURRENT RISK - ASSETS",
                               "CURRENT SWAP PORTFOLIO SENSITIVITIES",
                               "SENSITIVITIES - REQUIRED SWAPS",
                               "NOTIONALS - REQUIRED SWAPS",
                               "RESULTING TOTAL PORTFOLIO SENSITIVITY"
                           };
            }
        }

        #region Columns
        public string[] CurrentInterestRateRiskLiabilitiesColumns
        {
            get
            {
                return new[]
                           {
                               "Liabilities - Interest Rate PV01",
                               "Total (EUR)", 
                               "Actives (EUR thou)", 
                               "Pensioners / Deferreds (EUR)"
                           };
            }
        }

        public string[] CurrentInflationRiskLiabilitiesColumns
        {
            get
            {
                return new[]
                           {
                               "Liabilities - Interest Rate PV01",
                               "Total (EUR)", 
                               "Actives (EUR)", 
                               "Pensioners / Deferreds (EUR)"
                           };
            }
        }

        public string[] CurrentRiskAssetsColumns
        {
            get
            {
                return new[]
                           {
                               "Assets - Inflation PV01",
                               "Total (EUR)", 
                               "Assets - Interest Rate PV01", 
                               "Total (EUR)"
                           };
            }
        }

        public string[] CurrentSwapPortfolioSensitivitiesColumns
        {
            get
            {
                return new[]
                           {
                               "Inflation",
                               "Swap PV01  (EUR)", 
                               "Interest Rate", 
                               "Swap PV01  (EUR))"
                           };
            }
        }

        public string[] SensitivitiesRequiredSwapsColumns
        {
            get
            {
                return new[]
                           {
                               "Inflation Swap",
                               "Swap PV01  (EUR)", 
                               "Interest Rate Swap", 
                               "Swap PV01  (EUR))"
                           };
            }
        }

        public string[] NotionalsRequiredSwapsColumns
        {
            get
            {
                return new[]
                           {
                               "Inflation Swap",
                               "Total (EUR)", 
                               "Interest Rate Swap", 
                               "Total (EUR)"
                           };
            }
        }

        public string[] ResultingTotalPortfolioSensitivityColumns
        {
            get
            {
                return new[]
                           {
                               "Assets - Inflation KR",
                               "Total (EUR)", 
                               "Assets - Rates KR", 
                               "Total (EUR)"
                           };
            }
        }
        #endregion Columns

        public string[] YrKrNames
        {
            get
            {
                return new[]
                           {
                               "5YR KR",
                               "10YR KR",
                               "15YR KR",
                               "20YR KR",
                               "25YR KR",
                               "30YR KR",
                               "35YR KR",
                               "40YR KR",
                               "45YR KR",
                               "50YR KR"

                           };
            }
        }


        public OverviewUserControl()
        {
            InitializeComponent();

            SetupGrid();
            SetupGraphs();
        }

        private void SetupGrid()
        {
            _grid.AllowUserToAddRows = false;
            _grid.AllowUserToDeleteRows = false;
            _grid.AllowUserToOrderColumns = false;
            _grid.AllowUserToResizeColumns = true;
            _grid.AllowUserToResizeRows = true;
            _grid.ReadOnly = true;

            foreach (DataGridViewColumn column in _grid.Columns)
                column.SortMode = DataGridViewColumnSortMode.NotSortable;
        }

        private void SetupGraphs()
        {
            _zedGraphControl1.GraphPane.Title.IsVisible = false;
            _zedGraphControl1.GraphPane.YAxis.Title.IsVisible = false;
            _zedGraphControl1.GraphPane.XAxis.Title.Text = "Years";
            _zedGraphControl1.GraphPane.BarSettings.Type = BarType.Stack;
            _zedGraphControl1.DoubleClick+=new EventHandler(_zedGraphControl_DoubleClick);

            _zedGraphControl2.GraphPane.Title.IsVisible = false;
            _zedGraphControl2.GraphPane.YAxis.Title.IsVisible = false;
            _zedGraphControl2.GraphPane.BarSettings.Base = BarBase.Y;
            _zedGraphControl2.GraphPane.BarSettings.MinClusterGap = 0.3f;
            _zedGraphControl2.GraphPane.YAxis.ScaleFormatEvent += YAxis_ScaleFormatEvent;
            _zedGraphControl2.GraphPane.XAxis.Scale.Min = 0;
            _zedGraphControl2.DoubleClick += new EventHandler(_zedGraphControl_DoubleClick);

            _zedGraphControl3.GraphPane.Title.IsVisible = false;
            _zedGraphControl3.GraphPane.YAxis.Title.IsVisible = false;
            _zedGraphControl3.GraphPane.BarSettings.Base = BarBase.Y;
            _zedGraphControl3.GraphPane.BarSettings.MinClusterGap = 0.3f;
            _zedGraphControl3.GraphPane.YAxis.ScaleFormatEvent += YAxis_ScaleFormatEvent;
            _zedGraphControl3.GraphPane.XAxis.Scale.Min = 0;
            _zedGraphControl3.DoubleClick += new EventHandler(_zedGraphControl_DoubleClick);

            _zedGraphControl4.GraphPane.Title.IsVisible = false;
            _zedGraphControl4.GraphPane.YAxis.Title.IsVisible = false;
            _zedGraphControl4.GraphPane.BarSettings.Base = BarBase.Y;
            _zedGraphControl4.GraphPane.BarSettings.MinClusterGap = 0.3f;
            _zedGraphControl4.GraphPane.YAxis.ScaleFormatEvent += YAxis_ScaleFormatEvent;
            _zedGraphControl4.DoubleClick += new EventHandler(_zedGraphControl_DoubleClick);

            _zedGraphControl5.GraphPane.Title.IsVisible = false;
            _zedGraphControl5.GraphPane.YAxis.Title.IsVisible = false;
            _zedGraphControl5.GraphPane.BarSettings.Base = BarBase.Y;
            _zedGraphControl5.GraphPane.BarSettings.MinClusterGap = 0.3f;
            _zedGraphControl5.GraphPane.YAxis.ScaleFormatEvent += YAxis_ScaleFormatEvent;
            _zedGraphControl5.GraphPane.XAxis.Scale.Min = 0;
            _zedGraphControl5.DoubleClick += new EventHandler(_zedGraphControl_DoubleClick);
        }

        static string YAxis_ScaleFormatEvent(GraphPane pane, Axis axis, double val, int index)
        {
            return val.ToString("0") + "YR KR";
        }

        private void OverviewUserControl_Load(object sender, EventArgs e)
        {

        }

        private void _zedGraphControl_DoubleClick(object sender, EventArgs e)
        {
            new ZedGraphPopUp().ShowZedGraphInPopUp((ZedGraphControl)sender);
        }
    }
}