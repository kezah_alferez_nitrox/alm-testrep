﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ZedGraph;

namespace Sensitivity_Analysis_Calculator.Sensitivity
{
    public class ZedGraphPopUp : Form
    {
        private ZedGraphControl _graph;

        public ZedGraphPopUp()
        {
            Init();
        }

        private void Init()
        {
            _graph = new ZedGraphControl();
            _graph.Dock = DockStyle.Fill;

            this.Size = new Size(900,600);
            this.Controls.Add(_graph);
        }

        public void ShowZedGraphInPopUp(ZedGraphControl zedGraph)
        {
            this.Text = zedGraph.GraphPane.Title.Text;
            _graph.GraphPane.CurveList = zedGraph.GraphPane.CurveList;
            _graph.GraphPane.Title.IsVisible = zedGraph.GraphPane.Title.IsVisible;
            _graph.GraphPane.Title.Text = zedGraph.GraphPane.Title.Text;
            _graph.GraphPane.YAxis.Title.IsVisible = zedGraph.GraphPane.YAxis.Title.IsVisible;
            _graph.GraphPane.YAxis.Title.Text = zedGraph.GraphPane.YAxis.Title.Text;
            _graph.GraphPane.YAxis.Scale.IsSkipFirstLabel = zedGraph.GraphPane.YAxis.Scale.IsSkipFirstLabel;
            _graph.GraphPane.XAxis.Title.IsVisible = zedGraph.GraphPane.XAxis.Title.IsVisible;
            _graph.GraphPane.XAxis.Title.Text = zedGraph.GraphPane.XAxis.Title.Text;
            _graph.GraphPane.XAxis.Scale.Max = zedGraph.GraphPane.XAxis.Scale.Max;
            _graph.GraphPane.XAxis.Scale.Min = zedGraph.GraphPane.XAxis.Scale.Min;
            _graph.GraphPane.YAxis.Scale.Max = zedGraph.GraphPane.YAxis.Scale.Max;
            _graph.GraphPane.YAxis.Scale.Min = zedGraph.GraphPane.YAxis.Scale.Min;
            _graph.GraphPane.BarSettings.Base = zedGraph.GraphPane.BarSettings.Base;
            _graph.GraphPane.BarSettings.MinClusterGap = zedGraph.GraphPane.BarSettings.MinClusterGap;
            _graph.GraphPane.BarSettings.Type = zedGraph.GraphPane.BarSettings.Type;
            _graph.GraphPane.LineType = zedGraph.GraphPane.LineType;

            _graph.ScrollGrace = zedGraph.ScrollGrace;
            _graph.ScrollMaxX = zedGraph.ScrollMaxX;
            _graph.ScrollMaxY = zedGraph.ScrollMaxY;
            _graph.ScrollMaxY2 = zedGraph.ScrollMaxY2;
            _graph.ScrollMinX = zedGraph.ScrollMinX;
            _graph.ScrollMinY = zedGraph.ScrollMinY;
            _graph.ScrollMinY2 = zedGraph.ScrollMinY2;

            _graph.AxisChange();
            _graph.Refresh();
            Show();
        }
    }
}
