﻿using System.Linq;
using Sensitivity_Analysis_Calculator.Calculators;

namespace Sensitivity_Analysis_Calculator.Sensitivity
{
    class IRSensPeDeTab : AdvancedCalculationsUserControl
    {
        private readonly YrKrHeader _yrKrHeader = new YrKrHeader();
        private readonly YrKrHeader _yrKrHeader2 = new YrKrHeader();

        public override void Init()
        {
            GridInit(null, 63);

            _yrKrHeader.Init();
            _yrKrHeader2.Init();

            AddUserControlTopToPanel(Graph(), _yrKrHeader, _yrKrHeader2);
        }

        public override void Populate(CalculContext context)
        {
            _yrKrHeader.PopulateYrKrGrid(
                context.Overview.CurrentInterestRateRiskLiabilitiesPensionersDeferredsPlusActivities, "PV1 + actives");
            _yrKrHeader2.PopulateYrKrGrid(context.Overview.CurrentInterestRateRiskLiabilitiesPensionersDeferreds);
            PopulateGraph(context.Overview.CurrentInterestRateRiskLiabilitiesActives, context.Overview.YrKr);

            AddColumnToGrid(context.Projections.Year, "Year", 0, "0");
            AddColumnToGrid(context.IrSensPeDe.LiabilityCF2, "Liability CF (1)", 0, "N0");
            AddColumnToGrid(context.TotalLiabScenario.DF, "Discount Curve", 0, "0.00%");
            AddColumnToGrid(context.TotalLiabScenario.Rate, "Underlying IR", 0, "0.00%");
            AddColumnToGrid(context.TotalLiabScenario.InterestRateShiftDown, "Discount Curve - 1bp", 0, "0.00%");
            AddColumnToGrid(context.TotalLiabScenario.InterestRateShiftUp, "Discount Curve + 1bp", 0, "0.00%");
            AddColumnToGrid(context.IrSensPeDe.LiabilityCF, "Liability CF (2)", 0, "N0");
            AddColumnToGrid(context.IrSensPeDe.PvCf1, "PV CF1", 0, "N0");
            AddColumnToGrid(context.IrSensPeDe.PvCf2, "PV CF2", 0, "N0");


            AddYrKrColumns(context.IrSensPeDe.CalculatedYrKr);

            double[] values = new double[]
                                  {
                                      context.IrSensPeDe.CalculatedYrKr[0, 0].Sum(),
                                      context.IrSensPeDe.CalculatedYrKr[0, 1].Sum(),
                                      context.IrSensPeDe.CalculatedYrKr[1, 0].Sum(),
                                      context.IrSensPeDe.CalculatedYrKr[1, 1].Sum(),
                                      context.IrSensPeDe.CalculatedYrKr[2, 0].Sum(),
                                      context.IrSensPeDe.CalculatedYrKr[2, 1].Sum(),
                                      context.IrSensPeDe.CalculatedYrKr[3, 0].Sum(),
                                      context.IrSensPeDe.CalculatedYrKr[3, 1].Sum(),
                                      context.IrSensPeDe.CalculatedYrKr[4, 0].Sum(),
                                      context.IrSensPeDe.CalculatedYrKr[4, 1].Sum(),
                                      context.IrSensPeDe.CalculatedYrKr[5, 0].Sum(),
                                      context.IrSensPeDe.CalculatedYrKr[5, 1].Sum(),
                                      context.IrSensPeDe.CalculatedYrKr[6, 0].Sum(),
                                      context.IrSensPeDe.CalculatedYrKr[6, 1].Sum(),
                                      context.IrSensPeDe.CalculatedYrKr[7, 0].Sum(),
                                      context.IrSensPeDe.CalculatedYrKr[7, 1].Sum(),
                                      context.IrSensPeDe.CalculatedYrKr[8, 0].Sum(),
                                      context.IrSensPeDe.CalculatedYrKr[8, 1].Sum(),
                                      context.IrSensPeDe.CalculatedYrKr[9, 0].Sum(),
                                      context.IrSensPeDe.CalculatedYrKr[9, 1].Sum()
                                  };
            double[] valuesCalc = new double[]
                                      {
                                          CalculateTotal(context.IrSensPeDe.CalculatedYrKr[0, 0].Sum(), context.IrSensPeDe.CalculatedYrKr[0, 1].Sum()),
                                          CalculateTotal(context.IrSensPeDe.CalculatedYrKr[1, 0].Sum(), context.IrSensPeDe.CalculatedYrKr[1, 1].Sum()),
                                          CalculateTotal(context.IrSensPeDe.CalculatedYrKr[2, 0].Sum(), context.IrSensPeDe.CalculatedYrKr[2, 1].Sum()),
                                          CalculateTotal(context.IrSensPeDe.CalculatedYrKr[3, 0].Sum(), context.IrSensPeDe.CalculatedYrKr[3, 1].Sum()),
                                          CalculateTotal(context.IrSensPeDe.CalculatedYrKr[4, 0].Sum(), context.IrSensPeDe.CalculatedYrKr[4, 1].Sum()),
                                          CalculateTotal(context.IrSensPeDe.CalculatedYrKr[5, 0].Sum(), context.IrSensPeDe.CalculatedYrKr[5, 1].Sum()),
                                          CalculateTotal(context.IrSensPeDe.CalculatedYrKr[6, 0].Sum(), context.IrSensPeDe.CalculatedYrKr[6, 1].Sum()),
                                          CalculateTotal(context.IrSensPeDe.CalculatedYrKr[7, 0].Sum(), context.IrSensPeDe.CalculatedYrKr[7, 1].Sum()),
                                          CalculateTotal(context.IrSensPeDe.CalculatedYrKr[8, 0].Sum(), context.IrSensPeDe.CalculatedYrKr[8, 1].Sum()),
                                          CalculateTotal(context.IrSensPeDe.CalculatedYrKr[9, 0].Sum(), context.IrSensPeDe.CalculatedYrKr[9, 1].Sum())
                                      };

            AddSums("5YR KR - 1", values, valuesCalc);
            AddPvCfSums("PV CF1", context.IrSensPeDe.PvCf1.Sum(), context.IrSensPeDe.PvCf2.Sum());
        }
    }
}
