﻿using System.Linq;
using Sensitivity_Analysis_Calculator.Calculators;

namespace Sensitivity_Analysis_Calculator.Sensitivity
{
    class InfSensActTab : AdvancedCalculationsUserControl
    {
        private readonly YrKrHeader _yrKrHeader = new YrKrHeader();

        public override void Init()
        {
            GridInit(null, 63);

            _yrKrHeader.Init();

            AddUserControlTopToPanel(Graph(),_yrKrHeader);
        }

        public override void Populate(CalculContext context)
        {
            _yrKrHeader.PopulateYrKrGrid(context.Overview.CurrentInflationRiskLiabilitiesActives);
            PopulateGraph(context.Overview.CurrentInflationRiskLiabilitiesActives, context.Overview.YrKr);

            AddColumnToGrid(context.Projections.Year, "Year", 0,"0");
            AddColumnToGrid(context.TotalLiabScenario.YearFraction, "Year Fraction", 0,"N0");
            AddColumnToGrid(context.TotalLiabScenario.DF, "Discount Curve", 0, "0.00%");
            AddColumnToGrid(context.INFSensAct.InflationIndex, "Inflation Index", 0,"N0");
            AddColumnToGrid(context.TotalLiabScenario.InflationShift, "Inflation", 0, "0.00%");
            AddColumnToGrid(context.INFSensAct.IndexPlus1Bp, "Index +1bp (1)", 0,"N0");
            AddColumnToGrid(context.TotalLiabScenario.InflationShiftUp, "Inflation + 1bp (1)", 0, "0.00%");
            AddColumnToGrid(context.INFSensAct.IndexMinus1Bp, "Index +1bp (2)", 0,"N0");
            AddColumnToGrid(context.TotalLiabScenario.InflationShiftDown, "Inflation - 1bp (2)", 0, "0.00%");
            AddColumnToGrid(context.INFSensAct.NominalCfFixed, "Nominal CF Fixed", 0,"N0");
            AddColumnToGrid(context.INFSensAct.SalaryEscalation, "+ salary Escalation", 0,"N0");
            AddColumnToGrid(context.INFSensAct.SalaryIndexed, "Indexed", 0,"N0");
            AddColumnToGrid(context.INFSensAct.SalaryNone, "no salary", 0,"N0");
            AddColumnToGrid(context.INFSensAct.Decay, "Decay", 0,"N0");
            AddColumnToGrid(context.INFSensAct.SalaryEffect, "salary effect", 0,"N0");
            AddColumnToGrid(context.INFSensAct.PvCf1, "PV CF1", 0,"N0");
            AddColumnToGrid(context.INFSensAct.PvCf2, "PV CF2", 0,"N0");

            AddYrKrColumns(context.INFSensAct.CalculatedYrKr);

            double[] values = new double[]
                                  {
                                      context.INFSensAct.CalculatedYrKr[0, 0].Sum(),
                                      context.INFSensAct.CalculatedYrKr[0, 1].Sum(),
                                      context.INFSensAct.CalculatedYrKr[1, 0].Sum(),
                                      context.INFSensAct.CalculatedYrKr[1, 1].Sum(),
                                      context.INFSensAct.CalculatedYrKr[2, 0].Sum(),
                                      context.INFSensAct.CalculatedYrKr[2, 1].Sum(),
                                      context.INFSensAct.CalculatedYrKr[3, 0].Sum(),
                                      context.INFSensAct.CalculatedYrKr[3, 1].Sum(),
                                      context.INFSensAct.CalculatedYrKr[4, 0].Sum(),
                                      context.INFSensAct.CalculatedYrKr[4, 1].Sum(),
                                      context.INFSensAct.CalculatedYrKr[5, 0].Sum(),
                                      context.INFSensAct.CalculatedYrKr[5, 1].Sum(),
                                      context.INFSensAct.CalculatedYrKr[6, 0].Sum(),
                                      context.INFSensAct.CalculatedYrKr[6, 1].Sum(),
                                      context.INFSensAct.CalculatedYrKr[7, 0].Sum(),
                                      context.INFSensAct.CalculatedYrKr[7, 1].Sum(),
                                      context.INFSensAct.CalculatedYrKr[8, 0].Sum(),
                                      context.INFSensAct.CalculatedYrKr[8, 1].Sum(),
                                      context.INFSensAct.CalculatedYrKr[9, 0].Sum(),
                                      context.INFSensAct.CalculatedYrKr[9, 1].Sum()
                                  };
            double[] valuesCalc = new double[]
                                      {
                                          CalculateTotal(context.INFSensAct.CalculatedYrKr[0, 0].Sum(), context.INFSensAct.CalculatedYrKr[0, 1].Sum()),
                                          CalculateTotal(context.INFSensAct.CalculatedYrKr[1, 0].Sum(), context.INFSensAct.CalculatedYrKr[1, 1].Sum()),
                                          CalculateTotal(context.INFSensAct.CalculatedYrKr[2, 0].Sum(), context.INFSensAct.CalculatedYrKr[2, 1].Sum()),
                                          CalculateTotal(context.INFSensAct.CalculatedYrKr[3, 0].Sum(), context.INFSensAct.CalculatedYrKr[3, 1].Sum()),
                                          CalculateTotal(context.INFSensAct.CalculatedYrKr[4, 0].Sum(), context.INFSensAct.CalculatedYrKr[4, 1].Sum()),
                                          CalculateTotal(context.INFSensAct.CalculatedYrKr[5, 0].Sum(), context.INFSensAct.CalculatedYrKr[5, 1].Sum()),
                                          CalculateTotal(context.INFSensAct.CalculatedYrKr[6, 0].Sum(), context.INFSensAct.CalculatedYrKr[6, 1].Sum()),
                                          CalculateTotal(context.INFSensAct.CalculatedYrKr[7, 0].Sum(), context.INFSensAct.CalculatedYrKr[7, 1].Sum()),
                                          CalculateTotal(context.INFSensAct.CalculatedYrKr[8, 0].Sum(), context.INFSensAct.CalculatedYrKr[8, 1].Sum()),
                                          CalculateTotal(context.INFSensAct.CalculatedYrKr[9, 0].Sum(), context.INFSensAct.CalculatedYrKr[9, 1].Sum())
                                      };

            AddSums("5YR KR - 1", values, valuesCalc);
        }
    }
}
