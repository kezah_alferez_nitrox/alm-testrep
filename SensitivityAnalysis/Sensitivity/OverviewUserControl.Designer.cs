﻿namespace Sensitivity_Analysis_Calculator.Sensitivity
{
    partial class OverviewUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer _components;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (_components != null))
            {
                _components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this._panel1 = new System.Windows.Forms.Panel();
            this._zedGraphControl5 = new ZedGraph.ZedGraphControl();
            this._zedGraphControl4 = new ZedGraph.ZedGraphControl();
            this._zedGraphControl3 = new ZedGraph.ZedGraphControl();
            this._zedGraphControl2 = new ZedGraph.ZedGraphControl();
            this._zedGraphControl1 = new ZedGraph.ZedGraphControl();
            this._splitContainer = new System.Windows.Forms.SplitContainer();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this._grid = new System.Windows.Forms.DataGridView();
            this._gridPanel = new System.Windows.Forms.Panel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this._parametersUserControl = new Sensitivity_Analysis_Calculator.Sensitivity.ParametersUserControl();
            this._panel1.SuspendLayout();
            this._splitContainer.Panel1.SuspendLayout();
            this._splitContainer.Panel2.SuspendLayout();
            this._splitContainer.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._grid)).BeginInit();
            this._gridPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // _panel1
            // 
            this._panel1.AutoScroll = true;
            this._panel1.Controls.Add(this._zedGraphControl5);
            this._panel1.Controls.Add(this._zedGraphControl4);
            this._panel1.Controls.Add(this._zedGraphControl3);
            this._panel1.Controls.Add(this._zedGraphControl2);
            this._panel1.Controls.Add(this._zedGraphControl1);
            this._panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this._panel1.Location = new System.Drawing.Point(0, 0);
            this._panel1.Name = "_panel1";
            this._panel1.Size = new System.Drawing.Size(304, 500);
            this._panel1.TabIndex = 0;
            // 
            // _zedGraphControl5
            // 
            this._zedGraphControl5.Dock = System.Windows.Forms.DockStyle.Top;
            this._zedGraphControl5.Location = new System.Drawing.Point(0, 1120);
            this._zedGraphControl5.Name = "_zedGraphControl5";
            this._zedGraphControl5.ScrollGrace = 0;
            this._zedGraphControl5.ScrollMaxX = 0;
            this._zedGraphControl5.ScrollMaxY = 0;
            this._zedGraphControl5.ScrollMaxY2 = 0;
            this._zedGraphControl5.ScrollMinX = 0;
            this._zedGraphControl5.ScrollMinY = 0;
            this._zedGraphControl5.ScrollMinY2 = 0;
            this._zedGraphControl5.Size = new System.Drawing.Size(287, 280);
            this._zedGraphControl5.TabIndex = 4;
            // 
            // _zedGraphControl4
            // 
            this._zedGraphControl4.Dock = System.Windows.Forms.DockStyle.Top;
            this._zedGraphControl4.Location = new System.Drawing.Point(0, 840);
            this._zedGraphControl4.Name = "_zedGraphControl4";
            this._zedGraphControl4.ScrollGrace = 0;
            this._zedGraphControl4.ScrollMaxX = 0;
            this._zedGraphControl4.ScrollMaxY = 0;
            this._zedGraphControl4.ScrollMaxY2 = 0;
            this._zedGraphControl4.ScrollMinX = 0;
            this._zedGraphControl4.ScrollMinY = 0;
            this._zedGraphControl4.ScrollMinY2 = 0;
            this._zedGraphControl4.Size = new System.Drawing.Size(287, 280);
            this._zedGraphControl4.TabIndex = 3;
            // 
            // _zedGraphControl3
            // 
            this._zedGraphControl3.Dock = System.Windows.Forms.DockStyle.Top;
            this._zedGraphControl3.Location = new System.Drawing.Point(0, 560);
            this._zedGraphControl3.Name = "_zedGraphControl3";
            this._zedGraphControl3.ScrollGrace = 0;
            this._zedGraphControl3.ScrollMaxX = 0;
            this._zedGraphControl3.ScrollMaxY = 0;
            this._zedGraphControl3.ScrollMaxY2 = 0;
            this._zedGraphControl3.ScrollMinX = 0;
            this._zedGraphControl3.ScrollMinY = 0;
            this._zedGraphControl3.ScrollMinY2 = 0;
            this._zedGraphControl3.Size = new System.Drawing.Size(287, 280);
            this._zedGraphControl3.TabIndex = 2;
            // 
            // _zedGraphControl2
            // 
            this._zedGraphControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this._zedGraphControl2.Location = new System.Drawing.Point(0, 280);
            this._zedGraphControl2.Name = "_zedGraphControl2";
            this._zedGraphControl2.ScrollGrace = 0;
            this._zedGraphControl2.ScrollMaxX = 0;
            this._zedGraphControl2.ScrollMaxY = 0;
            this._zedGraphControl2.ScrollMaxY2 = 0;
            this._zedGraphControl2.ScrollMinX = 0;
            this._zedGraphControl2.ScrollMinY = 0;
            this._zedGraphControl2.ScrollMinY2 = 0;
            this._zedGraphControl2.Size = new System.Drawing.Size(287, 280);
            this._zedGraphControl2.TabIndex = 1;
            // 
            // _zedGraphControl1
            // 
            this._zedGraphControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this._zedGraphControl1.Location = new System.Drawing.Point(0, 0);
            this._zedGraphControl1.Name = "_zedGraphControl1";
            this._zedGraphControl1.ScrollGrace = 0;
            this._zedGraphControl1.ScrollMaxX = 0;
            this._zedGraphControl1.ScrollMaxY = 0;
            this._zedGraphControl1.ScrollMaxY2 = 0;
            this._zedGraphControl1.ScrollMinX = 0;
            this._zedGraphControl1.ScrollMinY = 0;
            this._zedGraphControl1.ScrollMinY2 = 0;
            this._zedGraphControl1.Size = new System.Drawing.Size(287, 280);
            this._zedGraphControl1.TabIndex = 0;
            // 
            // _splitContainer
            // 
            this._splitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this._splitContainer.Location = new System.Drawing.Point(0, 0);
            this._splitContainer.Name = "_splitContainer";
            // 
            // _splitContainer.Panel1
            // 
            this._splitContainer.Panel1.Controls.Add(this.tableLayoutPanel1);
            // 
            // _splitContainer.Panel2
            // 
            this._splitContainer.Panel2.Controls.Add(this._panel1);
            this._splitContainer.Size = new System.Drawing.Size(890, 500);
            this._splitContainer.SplitterDistance = 582;
            this._splitContainer.TabIndex = 1;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel1.Controls.Add(this._grid, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this._gridPanel, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this._parametersUserControl, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 272F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 96F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(582, 500);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // _grid
            // 
            this._grid.AllowUserToAddRows = false;
            this._grid.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._grid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this._grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this._grid.DefaultCellStyle = dataGridViewCellStyle2;
            this._grid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._grid.Location = new System.Drawing.Point(352, 3);
            this._grid.Name = "_grid";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._grid.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this._grid.Size = new System.Drawing.Size(227, 266);
            this._grid.TabIndex = 0;
            // 
            // _gridPanel
            // 
            this._gridPanel.AutoScroll = true;
            this.tableLayoutPanel1.SetColumnSpan(this._gridPanel, 2);
            this._gridPanel.Controls.Add(this.tableLayoutPanel2);
            this._gridPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this._gridPanel.Location = new System.Drawing.Point(3, 275);
            this._gridPanel.Name = "_gridPanel";
            this._gridPanel.Size = new System.Drawing.Size(576, 222);
            this._gridPanel.TabIndex = 1;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 4;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 340F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 320F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 320F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 320F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(559, 1300);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // _parametersUserControl
            // 
            this._parametersUserControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._parametersUserControl.Location = new System.Drawing.Point(3, 3);
            this._parametersUserControl.Name = "_parametersUserControl";
            this._parametersUserControl.Size = new System.Drawing.Size(343, 266);
            this._parametersUserControl.TabIndex = 1;
            // 
            // OverviewUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._splitContainer);
            this.Name = "OverviewUserControl";
            this.Size = new System.Drawing.Size(890, 500);
            this.Load += new System.EventHandler(this.OverviewUserControl_Load);
            this._panel1.ResumeLayout(false);
            this._splitContainer.Panel1.ResumeLayout(false);
            this._splitContainer.Panel2.ResumeLayout(false);
            this._splitContainer.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._grid)).EndInit();
            this._gridPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel _panel1;
        private System.Windows.Forms.SplitContainer _splitContainer;
        private System.ComponentModel.IContainer components;
        public ZedGraph.ZedGraphControl _zedGraphControl3;
        public ZedGraph.ZedGraphControl _zedGraphControl2;
        public ZedGraph.ZedGraphControl _zedGraphControl1;
        public System.Windows.Forms.Panel _gridPanel;
        public System.Windows.Forms.DataGridView _grid;
        public ZedGraph.ZedGraphControl _zedGraphControl5;
        public ZedGraph.ZedGraphControl _zedGraphControl4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        public ParametersUserControl _parametersUserControl;
        public System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
    }
}