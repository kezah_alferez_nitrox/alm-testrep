﻿using System.Linq;
using Sensitivity_Analysis_Calculator.Calculators;

namespace Sensitivity_Analysis_Calculator.Sensitivity
{
    class InflationSwapExposureTab : AdvancedCalculationsUserControl
    {
        private readonly YrKrHeader _yrKrHeader = new YrKrHeader();
        private readonly YrKrHeader _yrKrHeader2 = new YrKrHeader();

        public override void Init()
        {
            GridInit(null, 63);

            _yrKrHeader.Init();
            _yrKrHeader2.Init();

            AddUserControlTopToPanel(Graph(), _yrKrHeader, _yrKrHeader2);
        }

        public override void Populate(CalculContext context)
        {
            _yrKrHeader.PopulateYrKrGrid(context.Overview.CurrentSwapPortfolioSensitivitiesInflationPlusActivities, "PV1 + actives");
            _yrKrHeader2.PopulateYrKrGrid(context.Overview.CurrentSwapPortfolioSensitivitiesInflation);
            PopulateGraph(context.Overview.CurrentSwapPortfolioSensitivitiesInflation, context.Overview.YrKr);

            AddColumnToGrid(context.Projections.Year, "Year", 0,"0");
            AddColumnToGrid(context.TotalLiabScenario.YearFraction, "Year Fraction", 0,"N0");
            AddColumnToGrid(context.TotalLiabScenario.FixedInterestRateDF, "Discount Curve", 0, "0.00%");
            AddColumnToGrid(context.InflationSwapExposure.InflationIndex, "Inflation Index", 0, "N0");
            AddColumnToGrid(context.TotalLiabScenario.InflationAssetsShiftPerCent, "Inflation", 0, "0.00%");
            AddColumnToGrid(context.InflationSwapExposure.IndexPlus1Bp, "Index + 1bp (1)", 0,"N0");
            AddColumnToGrid(context.TotalLiabScenario.InflationAssetsShiftUp, "Inflation + 1bp (1)", 0, "0.00%");
            AddColumnToGrid(context.InflationSwapExposure.IndexMinus1Bp, "Index + 1bp (2)", 0,"N0");
            AddColumnToGrid(context.TotalLiabScenario.InflationAssetsShiftDown, "Inflation - 1bp (2)", 0, "0.00%");
            AddColumnToGrid(context.InflationSwapExposure.AssetCF, "Asset CF", 0,"N0");
            AddColumnToGrid(context.InflationSwapExposure.ActualCF, "Actual CF", 0,"N0");
            AddColumnToGrid(context.InflationSwapExposure.PvCf1, "PV CF1",0,"N0");
            AddColumnToGrid(context.InflationSwapExposure.PvCf2, "PV CF2", 0,"N0");

            AddYrKrColumns(context.InflationSwapExposure.CalculatedYrKr);

            double[] values = new double[]
                                  {
                                      context.InflationSwapExposure.CalculatedYrKr[0, 0].Sum(),
                                      context.InflationSwapExposure.CalculatedYrKr[0, 1].Sum(),
                                      context.InflationSwapExposure.CalculatedYrKr[1, 0].Sum(),
                                      context.InflationSwapExposure.CalculatedYrKr[1, 1].Sum(),
                                      context.InflationSwapExposure.CalculatedYrKr[2, 0].Sum(),
                                      context.InflationSwapExposure.CalculatedYrKr[2, 1].Sum(),
                                      context.InflationSwapExposure.CalculatedYrKr[3, 0].Sum(),
                                      context.InflationSwapExposure.CalculatedYrKr[3, 1].Sum(),
                                      context.InflationSwapExposure.CalculatedYrKr[4, 0].Sum(),
                                      context.InflationSwapExposure.CalculatedYrKr[4, 1].Sum(),
                                      context.InflationSwapExposure.CalculatedYrKr[5, 0].Sum(),
                                      context.InflationSwapExposure.CalculatedYrKr[5, 1].Sum(),
                                      context.InflationSwapExposure.CalculatedYrKr[6, 0].Sum(),
                                      context.InflationSwapExposure.CalculatedYrKr[6, 1].Sum(),
                                      context.InflationSwapExposure.CalculatedYrKr[7, 0].Sum(),
                                      context.InflationSwapExposure.CalculatedYrKr[7, 1].Sum(),
                                      context.InflationSwapExposure.CalculatedYrKr[8, 0].Sum(),
                                      context.InflationSwapExposure.CalculatedYrKr[8, 1].Sum(),
                                      context.InflationSwapExposure.CalculatedYrKr[9, 0].Sum(),
                                      context.InflationSwapExposure.CalculatedYrKr[9, 1].Sum()
                                  };
            double[] valuesCalc = new double[]
                                      {
                                          CalculateTotal(context.InflationSwapExposure.CalculatedYrKr[0, 0].Sum(), context.InflationSwapExposure.CalculatedYrKr[0, 1].Sum()),
                                          CalculateTotal(context.InflationSwapExposure.CalculatedYrKr[1, 0].Sum(), context.InflationSwapExposure.CalculatedYrKr[1, 1].Sum()),
                                          CalculateTotal(context.InflationSwapExposure.CalculatedYrKr[2, 0].Sum(), context.InflationSwapExposure.CalculatedYrKr[2, 1].Sum()),
                                          CalculateTotal(context.InflationSwapExposure.CalculatedYrKr[3, 0].Sum(), context.InflationSwapExposure.CalculatedYrKr[3, 1].Sum()),
                                          CalculateTotal(context.InflationSwapExposure.CalculatedYrKr[4, 0].Sum(), context.InflationSwapExposure.CalculatedYrKr[4, 1].Sum()),
                                          CalculateTotal(context.InflationSwapExposure.CalculatedYrKr[5, 0].Sum(), context.InflationSwapExposure.CalculatedYrKr[5, 1].Sum()),
                                          CalculateTotal(context.InflationSwapExposure.CalculatedYrKr[6, 0].Sum(), context.InflationSwapExposure.CalculatedYrKr[6, 1].Sum()),
                                          CalculateTotal(context.InflationSwapExposure.CalculatedYrKr[7, 0].Sum(), context.InflationSwapExposure.CalculatedYrKr[7, 1].Sum()),
                                          CalculateTotal(context.InflationSwapExposure.CalculatedYrKr[8, 0].Sum(), context.InflationSwapExposure.CalculatedYrKr[8, 1].Sum()),
                                          CalculateTotal(context.InflationSwapExposure.CalculatedYrKr[9, 0].Sum(), context.InflationSwapExposure.CalculatedYrKr[9, 1].Sum())
                                      };

            AddSums("5YR KR - 1", values, valuesCalc);
        }
    }
}
