﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Sensitivity_Analysis_Calculator.Calculators;

namespace Sensitivity_Analysis_Calculator.Sensitivity
{
    public partial class TotalLiabScenarioTab : AdvancedCalculationsUserControl
    {
        public TotalLiabScenarioTab()
        {
            InitializeComponent();
            splitContainer1.Panel1.Hide();
            splitContainer1.SplitterDistance = 0;
        }

        public override void Init()
        {
            GridInit(null, 63);
        }

        public override void Populate(CalculContext context)
        {
            //AddColumnToGrid(context.Projections.Year, "Year", 0, "0");

            var dates = new string[63];

            for (var i = 0; i < dates.Length; i++)
            {
                dates[i] = DateTime.Now.AddYears(i).ToShortDateString();
            }

            AddColumnToGrid(dates, "Year", 0, "d");
            AddColumnToGrid(context.TotalLiabScenario.YearFraction, "Year Fraction", 0, "0.00");
            AddColumnToGrid(context.TotalLiabScenario.DiscountCurve, "Discount Curve", 0, "0.00%");
            AddColumnToGrid(context.TotalLiabScenario.InflationIndex, "Inflation Index", 0, "0.00");
            AddColumnToGrid(context.TotalLiabScenario.Inflation, "Inflation", 0, "0.00%");
            AddColumnToGrid(context.TotalLiabScenario.Rate, "Rate", 0, "0.00%");
            AddColumnToGrid(context.TotalLiabScenario.DF, "DF (1)", 0, "0.00%");
            AddColumnToGrid(context.TotalLiabScenario.InterestRateShiftDown, "Interest Shift Down - bp -1", 0, "0.00%");
            AddColumnToGrid(context.TotalLiabScenario.InterestRateShiftUp, "Interest Shift Up - bp +1", 0, "0.00%");
            AddColumnToGrid(context.TotalLiabScenario.InterestRateShiftDown1, "Interest Shift Down bp -1 (1)", 0, "0.00%");
            AddColumnToGrid(context.TotalLiabScenario.InterestRateShiftUp1, "Interest Shift Up - bp +1 (2)", 0, "0.00%");
            AddColumnToGrid(context.TotalLiabScenario.InflationShiftBaseIndex, "Inflation Shift Index", 0, "0.00");
            AddColumnToGrid(context.TotalLiabScenario.InflationShift, "Inflation Shift", 0, "0.00");
            AddColumnToGrid(context.TotalLiabScenario.InflationShiftUp, "Inflation Shift Up - bp +1 (3)", 0, "0.00%");
            AddColumnToGrid(context.TotalLiabScenario.InflationShiftDown, "Inflation Shift Down - bp -1 (4)", 0, "0.00%");
            AddColumnToGrid(context.TotalLiabScenario.InflationShiftUp2, "Inflation Shift Up - bp +1 (5)", 0, "0.00%");
            AddColumnToGrid(context.TotalLiabScenario.InflationShiftDown2, "Inflation Shift Down - bp -1 (6)", 0, "0.00%");
            AddColumnToGrid(context.TotalLiabScenario.InflationShiftUp3, "Inflation Shift Up - bp +1 (7)", 0, "0.00%");
            AddColumnToGrid(context.TotalLiabScenario.InflationShiftDown3, "Inflation Shift Down - bp -1 (8)", 0, "0.00%");
            AddColumnToGrid(context.TotalLiabScenario.EscalationFactor, "Inflation Escalation Factor", 0, "0.00");
            AddColumnToGrid(context.TotalLiabScenario.FixedInflationUpliftInflationShiftIndex, "Assets Inflation Uplift Index", 0, "0.00");
            AddColumnToGrid(context.TotalLiabScenario.FixedInflationUpliftInflationShift, "Assets Inflation", 0, "0.00%");
            AddColumnToGrid(context.TotalLiabScenario.FixedInflationUpliftInflationShiftDown, "Assets Inflation - Down bp1 (1)", 0, "0.00%");
            AddColumnToGrid(context.TotalLiabScenario.FixedInflationUpliftInflationShiftUp, "Assets Inflation - Up bp-1 (2)", 0, "0.00%");
            AddColumnToGrid(context.TotalLiabScenario.FixedInflationUpliftInflationShiftUp2, "Assets Inflation - Up bp+1 (3)", 0, "0.00%");
            AddColumnToGrid(context.TotalLiabScenario.FixedInflationUpliftInflationShiftDown2, "Assets Inflation - Down bp-1 (4)", 0, "0.00%");
            AddColumnToGrid(context.TotalLiabScenario.FixedInflationUpliftInflationShiftUp3, "Assets Inflation - Up bp+1 (5)", 0, "#.##%");
            AddColumnToGrid(context.TotalLiabScenario.FixedInflationUpliftInflationShiftDown3, "Assets Inflation - Down bp-1 (6)", 0, "#.##%");
            AddColumnToGrid(context.TotalLiabScenario.FixedInterestRateRate, "Assets Fixed Interest Rate", 0, "0.00%");
            AddColumnToGrid(context.TotalLiabScenario.FixedInterestRateDF, "DF (2)", 0, "0.00%");
            AddColumnToGrid(context.TotalLiabScenario.FixedInterestRateDown1, "Assets Fixed Interest Rate Down - bp-1 (1)", 0, "0.00%");
            AddColumnToGrid(context.TotalLiabScenario.FixedInterestRateUp1, "Assets Fixed Interest Rate Up - bp1 (2)", 0, "0.00%");
            AddColumnToGrid(context.TotalLiabScenario.FixedInterestRateDown2, "Assets Fixed Interest Rate Down - bp-1 (3)", 0, "0.00%");
            AddColumnToGrid(context.TotalLiabScenario.FixedInterestRateUp2, "Assets Fixed Interest Rate Up - bp1 (4)", 0, "0.00%");
            AddColumnToGrid(context.INFSensAct.SalaryEffect, "Pure Salary Effect", 0, "N0");
            AddColumnToGrid(context.Overview.PureInflationEffect, "Pure Inflation Effect", 0, "N0");
        }
    }
}
