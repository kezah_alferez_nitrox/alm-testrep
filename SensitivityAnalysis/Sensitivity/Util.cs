using System;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Windows.Forms;
using ZedGraph;

namespace Sensitivity_Analysis_Calculator.Sensitivity
{
    public static class Util
    {
        public static string[] YrKrNames
        {
            get
            {
                return new[]
                           {
                               "5YR KR",
                               "10YR KR",
                               "15YR KR",
                               "20YR KR",
                               "25YR KR",
                               "30YR KR",
                               "35YR KR",
                               "40YR KR",
                               "45YR KR",
                               "50YR KR"
                           };
            }
        }

        public static void AddLine(double[][] data, ZedGraphControl zgc, double[] xValues, int colNo, Color color)
        {
            GraphPane zedPane = zgc.GraphPane;

            var yValues = GetColumnArray(data, colNo);
            LineItem curve = zedPane.AddCurve(
                "",
                xValues,
                yValues,
                color);

            curve.Line.Width = 2;
            curve.Symbol.IsVisible = false;
            curve.Line.Fill.Color = color;
        }

        public static void AddBar(double[][] data, ZedGraphControl zgc, double[] xValues, int colNo, string label, Color color)
        {
            GraphPane zedPane = zgc.GraphPane;

            var yValues = GetColumnArray(data, colNo);

            BarItem bar = zedPane.AddBar(
                label,
                xValues,
                yValues,
                color);

            bar.Bar.Border.IsVisible = false;
            bar.Bar.Fill.Type = FillType.Solid;
            bar.Bar.Fill.Color = color;
        }

        public static double[] GetColumnArray(object[][] data, int columnNo)
        {
            if (data == null) return null;

            int length = data.Length;
            var column = new double[length];

            for (int i = 0; i < length; i++)
            {
                column[i] = ConvertToDouble(data[i][columnNo]);
            }

            return column;
        }

        public static double[] GetColumnArray(double[][] data, int columnNo)
        {
            if (data == null) return null;

            int length = data.Length;
            var column = new double[length];

            for (int i = 0; i < length; i++)
            {
                column[i] = data[i][columnNo];
            }

            return column;
        }

        public static T[][] LoadCsvData<T>(string file, Func<string, T> converter)
        {
            string[] allLines = File.ReadAllLines(file);

            var data = new T[allLines.Length][];

            for (int i = 0; i < allLines.Length; i++)
            {
                string line = allLines[i];
                string[] cells = line.Split(';');

                data[i] = new T[cells.Length];
                for (int j = 0; j < cells.Length; j++)
                {
                    data[i][j] = converter(cells[j]);
                }
            }
            return data;
        }

        public static double ConvertToDouble(object obj)
        {
            return ConvertToDouble(Convert.ToString(obj));
        }

        public static double ConvertToDouble(string text)
        {
            double value;
            var cultureInfo = new CultureInfo("en-US");
            Double.TryParse(text, NumberStyles.Number, cultureInfo, out value);

            return value;
        }

        public static void FillGrid(DataGridView grid, object[][] data)
        {
            foreach (object[] row in data)
            {
                grid.Rows.Add(row);
            }
            grid.AutoResizeColumns();
        }

        public static void FillGrid(DataGridView grid, double[][] data)
        {
            foreach (double[] row in data)
            {
                var objectRow = new object[row.Length];
                for (int i = 0; i < row.Length; i++)
                {
                    objectRow[i] = row[i];
                }

                grid.Rows.Add(objectRow);
            }

            grid.AutoResizeColumns();
        }

        public static DataGridViewColumn AddColumnToGrid(DataGridView grid, string name, Func<DataGridViewColumn> creator)
        {
            DataGridViewColumn column = creator();
            column.HeaderText = name;
            column.DefaultCellStyle.Format = "N2";
            column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            grid.Columns.Add(column);

            return column;
        }

        public static DataGridViewColumn AddColumnToGrid(DataGridView grid, string name)
        {
            return AddColumnToGrid(grid, name, () => new DataGridViewTextBoxColumn());
        }

        public static object[][] GetObjectContent(object[][] data)
        {
            var content = new object[data.Length - 2][];

            for (int i = 1; i < data.Length - 1; i++)
            {
                content[i - 1] = new object[data[i].Length];
                for (int j = 0; j < data[i].Length; j++)
                {
                    content[i - 1][j] = data[i][j];
                }
            }

            return content;
        }

        public static string[] GetHeaders(object[][] data)
        {
            var headers = new string[data[0].Length];
            for (int i = 0; i < data[0].Length; i++)
            {
                headers[i] = Convert.ToString(data[0][i]);
            }
            return headers;
        }

        public static void AddHorizontalBar(object[][] data, ZedGraphControl zgc, double[] xValues, int colNo, string label, Color color)
        {
            // double[][] doubleData = ConvertToDouble(data);

            GraphPane zedPane = zgc.GraphPane;

            var yValues = GetColumnArray(data, colNo);

            BarItem bar = zedPane.AddBar(
                label,
                yValues,
                xValues,
                color);

            bar.Bar.Border.IsVisible = false;
            bar.Bar.Fill.Type = FillType.Solid;
            bar.Bar.Fill.Color = color;
        }

        public static void AddHorizontalBar(double[][] data, ZedGraphControl zgc, double[] xValues, int colNo, string label, Color color)
        {
            GraphPane zedPane = zgc.GraphPane;

            var yValues = GetColumnArray(data, colNo);

            BarItem bar = zedPane.AddBar(
                label,
                yValues,
                xValues,
                color);

            bar.Bar.Border.IsVisible = false;
            bar.Bar.Fill.Type = FillType.Solid;
            bar.Bar.Fill.Color = color;
        }

        public static double[][] ConvertToDouble(object[][] objArray)
        {
            var doubleData = new double[objArray.Length][];

            for (int i = 0; i < objArray.Length; i++)
            {
                doubleData[i] = new double[objArray[i].Length];
                for (int j = 0; j < objArray[i].Length; j++)
                {
                    doubleData[i][j] = ConvertToDouble(objArray[i][j]);
                }
            }

            return doubleData;
        }
    }
}