﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using ALMSCommon;
using Sensitivity_Analysis_Calculator.Calculators;
using ZedGraph;

namespace Sensitivity_Analysis_Calculator.Sensitivity
{
    public partial class SensitivityForm : Form, IModuleMainForm 
    {
        private readonly GlobalCalculator _globalCalculator = new GlobalCalculator();
        private OverviewGridUserControl _overviewGridUserControl = new OverviewGridUserControl();
        private string[] args;

        private object _loadedObject;

        public SensitivityForm()
        {
            InitializeComponent();
            this.Shown += new EventHandler(SensitivityForm_Shown);
        }

        public bool IsForcedClose { get; set; }

        public ILicenceInfo LicenceInfo { set; get; }

        public SensitivityForm(string[] args):this()
        {
            this.args = args;
        }

        void SensitivityForm_Shown(object sender, EventArgs e)
        {
            OpenPassedWorkSpace();
        }

        private void OpenPassedWorkSpace()
        {
            if (args != null && args.Length > 0)
            {
                string fileName = args[0];
                OpenFile(fileName);
            }
        }

        public void OpenFile(string fileName)
        {
            if (File.Exists(fileName))
            {
                string extension = Path.GetExtension(fileName);
                if (extension == ".sad")
                {
                    this.LoadObject(fileName);

                    this.LoadParameters(((object[])this._loadedObject)[0]);

                    this.SetDataToGrids(true);
                    this.Execute();
                }
                else if (extension == ".sam")
                {
                    this.LoadObject(fileName);
                    this.LoadParameters(((object[])this._loadedObject)[1]);
                    this.LoadProjections(((object[])this._loadedObject)[0]);

                    this.SetDataToGrids(false);
                    this.Execute();
                }
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Execute()
        {
            GetDataFromGrids();

            if (_liabilitiesUserControl._grid.Rows.Count > 0)
                _liabilitiesUserControl._grid.Rows.Clear();
            if (_assetsUserControl._grid.Rows.Count > 0)
                _assetsUserControl._grid.Rows.Clear();
            if (_overviewUserControl1.tableLayoutPanel2.Controls.Count > 0)
                _overviewUserControl1.tableLayoutPanel2.Controls.Clear();
            if (_overviewUserControl1._grid.Rows.Count > 0)
                _overviewUserControl1._grid.Rows.Clear();

            _globalCalculator.Calculate();

            ProjectionsLiabilities();
            ProjectionsAssets();

            Overview();

            _mainTabControl.SelectTab("_overviewTabPage");

            foreach (var page in _tabPages)
            {
                ((AdvancedCalculationsUserControl)page.Controls[0]).Populate(_globalCalculator.Context);
            }
        }

        private void SensitivityForm_Load(object sender, EventArgs e)
        {
            _liabilitiesUserControl._grid.CellEndEdit += GridCellEndEdit;

            CreateRows();
            
            _globalCalculator.CreateContext();
            _overviewUserControl1._parametersUserControl.SetupGrid(_globalCalculator.Context);

            AdvancedCalculationTabsInit();
        }

        public void CreateRows()
        {
            _liabilitiesUserControl._grid.Rows.Clear();
            _assetsUserControl._grid.Rows.Clear();

            _liabilitiesUserControl._grid.Rows.Add(63);
            _assetsUserControl._grid.Rows.Add(63);

            var currentYear = (double) DateTime.Now.Year;

            for(var i = 0; i < 63; i++)
            {
                foreach (DataGridViewCell cell in _liabilitiesUserControl._grid.Rows[i].Cells)
                    cell.Value = (double) 0;
                foreach (DataGridViewCell cell in _assetsUserControl._grid.Rows[i].Cells)
                    cell.Value = (double) 0;

                _liabilitiesUserControl._grid.Rows[i].Cells[0].Value = currentYear + i;
                _liabilitiesUserControl._grid.Rows[i].Cells[8].Value = currentYear + i - 1;

                _assetsUserControl._grid.Rows[i].Cells[0].Value = currentYear + i;
                _assetsUserControl._grid.Rows[i].Cells[7].Value = currentYear + i;
            }
        }

        void GridCellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if(_liabilitiesUserControl._grid.Rows.Count == 0)
                return;
        }

        private void GetDataFromGrids()
        {
                _liabilitiesUserControl._grid.EndEdit();
                if (_liabilitiesUserControl._grid.Rows.Count > 0)
                {
                    for (var i = 0; i < _liabilitiesUserControl._grid.Rows.Count; i++)
                    {
                        if (_liabilitiesUserControl._grid.Rows[i].Cells[1].Value == null) break;

                        _globalCalculator.Context.Projections.Year[i] =
                            (double)(DateTime.Now.Year + i);
                        _globalCalculator.Context.Projections.ActiveMembersIndexed[i] =
                            (double)_liabilitiesUserControl._grid.Rows[i].Cells[1].Value;
                        _globalCalculator.Context.Projections.DeferredMembersIndexed[i] =
                            (double)_liabilitiesUserControl._grid.Rows[i].Cells[2].Value;
                        _globalCalculator.Context.Projections.PensionersIndexed[i] =
                            (double)_liabilitiesUserControl._grid.Rows[i].Cells[3].Value;
                        _globalCalculator.Context.Projections.ActiveMembersFixed[i] =
                            (double)_liabilitiesUserControl._grid.Rows[i].Cells[4].Value;
                        _globalCalculator.Context.Projections.DeferredMembersFixed[i] =
                            (double)_liabilitiesUserControl._grid.Rows[i].Cells[5].Value;
                        _globalCalculator.Context.Projections.PensionersFixed[i] =
                            (double)_liabilitiesUserControl._grid.Rows[i].Cells[6].Value;
                    }
                }

            _assetsUserControl._grid.EndEdit();
                if (_assetsUserControl._grid.Rows.Count != 0)
                { 
                    for (var i = 0; i < _assetsUserControl._grid.Rows.Count; i++)
                    {
                        if (_assetsUserControl._grid.Rows[i].Cells[1].Value == null) break;

                        _globalCalculator.Context.Projections.EurSovereignBonds[i] =
                            (double)_assetsUserControl._grid.Rows[i].Cells[1].Value;
                        _globalCalculator.Context.Projections.InflationLinkedBondsEur[i] =
                            (double)_assetsUserControl._grid.Rows[i].Cells[2].Value;
                        _globalCalculator.Context.Projections.InterestRateSwaps[i] =
                            (double)_assetsUserControl._grid.Rows[i].Cells[3].Value;
                        _globalCalculator.Context.Projections.InflationSwapsFixedLeg[i] =
                            (double)_assetsUserControl._grid.Rows[i].Cells[4].Value;
                        _globalCalculator.Context.Projections.InflationSwapsInflationLeg[i] =
                            (double)_assetsUserControl._grid.Rows[i].Cells[5].Value;
                    }
                }               

                if (_overviewUserControl1._parametersUserControl._grid.Rows.Count > 0 &&
                    _overviewUserControl1._parametersUserControl._grid.Rows[0].Cells[0].Value != null)
                {
                    _globalCalculator.Context.Parameters.AssetsInflationMarketRates =
                        _overviewUserControl1._parametersUserControl.ParameterList[0].Value;

                    _globalCalculator.Context.Parameters.AssetsInflationMarketRatesIsSet =
                        _overviewUserControl1._parametersUserControl.ParameterList[0].Enabled;

                    _globalCalculator.Context.Parameters.AssetsInterestRateMarketRates =
                        _overviewUserControl1._parametersUserControl.ParameterList[1].Value;
                    _globalCalculator.Context.Parameters.AssetsInterestRateMarketRatesIsSet =
                        _overviewUserControl1._parametersUserControl.ParameterList[1].Enabled;

                    _globalCalculator.Context.Parameters.LiabilitiesInflationMarketRates =
                        _overviewUserControl1._parametersUserControl.ParameterList[2].Value;
                    _globalCalculator.Context.Parameters.LiabilitiesInflationMarketRatesIsSet =
                        _overviewUserControl1._parametersUserControl.ParameterList[2].Enabled;

                    _globalCalculator.Context.Parameters.LiabilitiesInterestRateMarketRates =
                        _overviewUserControl1._parametersUserControl.ParameterList[3].Value;
                    _globalCalculator.Context.Parameters.LiabilitiesInterestRateMarketRatesIsSet =
                        _overviewUserControl1._parametersUserControl.ParameterList[3].Enabled;

                    _globalCalculator.Context.Parameters.SalaryEscalation =
                        _overviewUserControl1._parametersUserControl.ParameterList[4].Value;

                    _globalCalculator.Context.Parameters.SalaryDecayFactor =
                        _overviewUserControl1._parametersUserControl.ParameterList[5].Value;

                    _globalCalculator.Context.Parameters.IndexationLevelPercentofCpi =
                        _overviewUserControl1._parametersUserControl.ParameterList[6].Value;

                    _globalCalculator.Context.Parameters.IndexationLevelPercentofCpiIsSet = false;

                    _globalCalculator.Context.Parameters.HedgeRatioLiabilityInterestRateRisk =
                        _overviewUserControl1._parametersUserControl.ParameterList[7].Value;

                    _globalCalculator.Context.Parameters.HedgeRatioLiabilityInflationRisk =
                        _overviewUserControl1._parametersUserControl.ParameterList[8].Value;

                    _globalCalculator.Context.Parameters.IncludeCurrentIRSwapPortfolio =
                        _overviewUserControl1._parametersUserControl.ParameterList[9].Enabled;

                    _globalCalculator.Context.Parameters.IncludeCurrentInflationSwapPortfolio =
                        _overviewUserControl1._parametersUserControl.ParameterList[10].Enabled;
                }
        }

        private void Overview()
        {
            if (_overviewUserControl1._grid.Columns.Count == 0)
            {               
                _overviewUserControl1._grid.Columns.Add("FirstColumn", string.Empty);
                _overviewUserControl1._grid.Columns["FirstColumn"].MinimumWidth = 114;
                _overviewUserControl1._grid.Columns.Add("NPV", "NPV");
                _overviewUserControl1._grid.Columns["NPV"].MinimumWidth = 85;
                _overviewUserControl1._grid.Columns.Add("Duration", "Duration");
                _overviewUserControl1._grid.Columns["Duration"].MinimumWidth = 85;
                _overviewUserControl1._grid.Columns["Duration"].DefaultCellStyle.Format = "N2";
                foreach (DataGridViewColumn column in _overviewUserControl1._grid.Columns)
                {
                    column.SortMode = DataGridViewColumnSortMode.NotSortable;                    
                    column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                    column.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                }                
            }

            if (_overviewUserControl1 == null) return;
            if (_overviewUserControl1._grid != null)
            {
                _overviewUserControl1._grid.Rows.Add("Actives", _globalCalculator.Context.Overview.ActivesNPV,
                                                     _globalCalculator.Context.Overview.ActivesDuration);
                _overviewUserControl1._grid.Rows.Add("Pensioners/Deferred", _globalCalculator.Context.Overview.PensionersDeferredNPV,
                                                     _globalCalculator.Context.Overview.PensionersDeferredDuration);
                _overviewUserControl1._grid.Rows.Add("Total", _globalCalculator.Context.Overview.NPVTotal,
                                                     string.Empty);
            }
            
            CreateGridsForOverview(_overviewUserControl1.GridNames[0],
                                   _overviewUserControl1.CurrentInterestRateRiskLiabilitiesColumns, 0, 0);
            CreateGridsForOverview(_overviewUserControl1.GridNames[1],
                                   _overviewUserControl1.CurrentInflationRiskLiabilitiesColumns, 1, 0);
            CreateGridsForOverview(_overviewUserControl1.GridNames[2],
                                   _overviewUserControl1.CurrentRiskAssetsColumns, 0, 1);
            CreateGridsForOverview(_overviewUserControl1.GridNames[3],
                                  _overviewUserControl1.CurrentSwapPortfolioSensitivitiesColumns, 1, 1);
            CreateGridsForOverview(_overviewUserControl1.GridNames[4],
                                   _overviewUserControl1.SensitivitiesRequiredSwapsColumns, 0, 2);
            CreateGridsForOverview(_overviewUserControl1.GridNames[5],
                                   _overviewUserControl1.NotionalsRequiredSwapsColumns, 1, 2);
            CreateGridsForOverview(_overviewUserControl1.GridNames[6],
                                   _overviewUserControl1.ResultingTotalPortfolioSensitivityColumns, 0, 3);

            //Populate Overview Grids
            for (var i = 0; i < _overviewUserControl1.YrKrNames.Length; i++)
            {
                // CURRENT INTEREST RATE RISK - LIABILITIES
                ((OverviewGridUserControl)
                 _overviewUserControl1.tableLayoutPanel2.Controls[_overviewUserControl1.GridNames[0]])
                    ._grid.Rows.Add(
                    _overviewUserControl1.YrKrNames[i],
                    MathTools.Round(_globalCalculator.Context.Overview.CurrentInterestRateRiskLiabilitiesTotal[i], -3),
                    MathTools.Round(_globalCalculator.Context.Overview.CurrentInterestRateRiskLiabilitiesActives[i], -3),
                    MathTools.Round(_globalCalculator.Context.Overview.CurrentInterestRateRiskLiabilitiesPensionersDeferreds[i], -3)
                    );

                // CURRENT INFLATION RISK - LIABILITIES	
                ((OverviewGridUserControl)
                 _overviewUserControl1.tableLayoutPanel2.Controls[_overviewUserControl1.GridNames[1]])
                    ._grid.Rows.Add(_overviewUserControl1.YrKrNames[i],
                                    MathTools.Round(_globalCalculator.Context.Overview.CurrentInflationRiskLiabilitiesTotal[i], -3),
                                    MathTools.Round(_globalCalculator.Context.Overview.CurrentInflationRiskLiabilitiesActives[i], -3),
                                    MathTools.Round(_globalCalculator.Context.Overview.CurrentInflationRiskLiabilitiesPensionersDeferreds[i], -3)
                    );

                // CURRENT RISK - ASSETS
                ((OverviewGridUserControl)
                 _overviewUserControl1.tableLayoutPanel2.Controls[_overviewUserControl1.GridNames[2]])
                    ._grid.Rows.Add(_overviewUserControl1.YrKrNames[i],
                                    MathTools.Round(_globalCalculator.Context.Overview.CurrentRiskAssetsInflation[i], 0),
                                    _overviewUserControl1.YrKrNames[i],
                                    MathTools.Round(_globalCalculator.Context.Overview.CurrentRiskAssetsInterestRate[i], 0)
                    );

                // CURRENT SWAP PORTFOLIO SENSITIVITIES
                ((OverviewGridUserControl)
                 _overviewUserControl1.tableLayoutPanel2.Controls[_overviewUserControl1.GridNames[3]])
                    ._grid.Rows.Add(_overviewUserControl1.YrKrNames[i],
                                    MathTools.Round(_globalCalculator.Context.Overview.CurrentSwapPortfolioSensitivitiesInflation[i], 0),
                                    _overviewUserControl1.YrKrNames[i],
                                    MathTools.Round(_globalCalculator.Context.Overview.CurrentSwapPortfolioSensitivitiesInterestRate[i], 0)
                    );

                // SENSITIVITIES - REQUIRED SWAPS
                ((OverviewGridUserControl)
                 _overviewUserControl1.tableLayoutPanel2.Controls[_overviewUserControl1.GridNames[4]])
                    ._grid.Rows.Add(_overviewUserControl1.YrKrNames[i],
                                    MathTools.Round(_globalCalculator.Context.Overview.SensitivitiesRequiredSwapsInflation[i], 0),
                                    _overviewUserControl1.YrKrNames[i],
                                    MathTools.Round(_globalCalculator.Context.Overview.SensitivitiesRequiredSwapsInterestRate[i], 0)
                    );

                // NOTIONALS - REQUIRED SWAPS
                ((OverviewGridUserControl)
                 _overviewUserControl1.tableLayoutPanel2.Controls[_overviewUserControl1.GridNames[5]])
                    ._grid.Rows.Add(_overviewUserControl1.YrKrNames[i],
                                    MathTools.Round(_globalCalculator.Context.Overview.NotionalsRequiredSwapsInflation[i], -6),
                                    _overviewUserControl1.YrKrNames[i],
                                    MathTools.Round(_globalCalculator.Context.Overview.NotionalsRequiredSwapsInterestRate[i], -6)
                    );

                //RESULTING TOTAL PORTFOLIO SENSITIVITY
                ((OverviewGridUserControl)
                 _overviewUserControl1.tableLayoutPanel2.Controls[_overviewUserControl1.GridNames[6]])
                    ._grid.Rows.Add(_overviewUserControl1.YrKrNames[i],
                                    MathTools.Round(_globalCalculator.Context.Overview.ResultingTotalPortfolioSensitivityAssetsInflation[i], -3),
                                    _overviewUserControl1.YrKrNames[i],
                                    MathTools.Round(_globalCalculator.Context.Overview.ResultingTotalPortfolioSensitivityAssetsRates[i], -3)
                    );
            }

            // TOTALS
            ((OverviewGridUserControl)
             _overviewUserControl1.tableLayoutPanel2.Controls[_overviewUserControl1.GridNames[0]])
                ._grid.Rows.Add(
                "TOTAL",
                MathTools.Round(_globalCalculator.Context.Overview.CurrentInterestRateRiskLiabilitiesActivesTotal, -3) + MathTools.Round(_globalCalculator.Context.Overview.CurrentInterestRateRiskLiabilitiesPensionersDeferredsTotal, -3),
                //MathTools.Round(_globalCalculator.Context.Overview.CurrentInterestRateRiskLiabilitiesTotalTotal, -3),
                MathTools.Round(_globalCalculator.Context.Overview.CurrentInterestRateRiskLiabilitiesActivesTotal, -3),
                MathTools.Round(_globalCalculator.Context.Overview.CurrentInterestRateRiskLiabilitiesPensionersDeferredsTotal, -3)                
                );

            ((OverviewGridUserControl)
             _overviewUserControl1.tableLayoutPanel2.Controls[_overviewUserControl1.GridNames[1]])
                ._grid.Rows.Add("TOTAL",
                                MathTools.Round(_globalCalculator.Context.Overview.CurrentInflationRiskLiabilitiesActivesTotal, -3) + MathTools.Round(_globalCalculator.Context.Overview.CurrentInflationRiskLiabilitiesPensionersDeferredsTotal, -3),
                                //MathTools.Round(_globalCalculator.Context.Overview.CurrentInflationRiskLiabilitiesTotalTotal, -3),
                                MathTools.Round(_globalCalculator.Context.Overview.CurrentInflationRiskLiabilitiesActivesTotal, -3),
                                MathTools.Round(_globalCalculator.Context.Overview.CurrentInflationRiskLiabilitiesPensionersDeferredsTotal, -3)                                
                );

            ((OverviewGridUserControl)
             _overviewUserControl1.tableLayoutPanel2.Controls[_overviewUserControl1.GridNames[2]])
                ._grid.Rows.Add("TOTAL",
                                MathTools.Round(_globalCalculator.Context.Overview.CurrentRiskAssetsInflationTotal, 0),
                                "TOTAL",
                                MathTools.Round(_globalCalculator.Context.Overview.CurrentRiskAssetsInterestRateTotal, 0)
                );

            ((OverviewGridUserControl)
             _overviewUserControl1.tableLayoutPanel2.Controls[_overviewUserControl1.GridNames[3]])
                ._grid.Rows.Add("TOTAL",
                                MathTools.Round(_globalCalculator.Context.Overview.CurrentSwapPortfolioSensitivitiesInflationTotal, 0),
                                "TOTAL",
                                MathTools.Round(_globalCalculator.Context.Overview.CurrentSwapPortfolioSensitivitiesInterestRateTotal, 0)
                );

            ((OverviewGridUserControl)
             _overviewUserControl1.tableLayoutPanel2.Controls[_overviewUserControl1.GridNames[4]])
                ._grid.Rows.Add("TOTAL",
                                MathTools.Round(_globalCalculator.Context.Overview.SensitivitiesRequiredSwapsInflationTotal, 0),
                                "TOTAL",
                                MathTools.Round(_globalCalculator.Context.Overview.SensitivitiesRequiredSwapsInterestRateTotal, 0)
                );

            ((OverviewGridUserControl)
             _overviewUserControl1.tableLayoutPanel2.Controls[_overviewUserControl1.GridNames[6]])
                ._grid.Rows.Add("TOTAL",
                                MathTools.Round(_globalCalculator.Context.Overview.ResultingTotalPortfolioSensitivityAssetsInflationTotal, 0),
                                "TOTAL",
                                MathTools.Round(_globalCalculator.Context.Overview.ResultingTotalPortfolioSensitivityAssetsRatesTotal, 0)
                );

            //Disable editing for grids
            ((OverviewGridUserControl)
             _overviewUserControl1.tableLayoutPanel2.Controls[_overviewUserControl1.GridNames[6]])
                ._grid.ReadOnly = true;
            ((OverviewGridUserControl)
             _overviewUserControl1.tableLayoutPanel2.Controls[_overviewUserControl1.GridNames[5]])
                ._grid.ReadOnly = true;
            ((OverviewGridUserControl)
             _overviewUserControl1.tableLayoutPanel2.Controls[_overviewUserControl1.GridNames[4]])
                ._grid.ReadOnly = true;
            ((OverviewGridUserControl)
             _overviewUserControl1.tableLayoutPanel2.Controls[_overviewUserControl1.GridNames[3]])
                ._grid.ReadOnly = true;
            ((OverviewGridUserControl)
             _overviewUserControl1.tableLayoutPanel2.Controls[_overviewUserControl1.GridNames[2]])
                ._grid.ReadOnly = true;
            ((OverviewGridUserControl)
             _overviewUserControl1.tableLayoutPanel2.Controls[_overviewUserControl1.GridNames[1]])
                ._grid.ReadOnly = true;
            ((OverviewGridUserControl)
             _overviewUserControl1.tableLayoutPanel2.Controls[_overviewUserControl1.GridNames[0]])
                ._grid.ReadOnly = true;


            _overviewUserControl1._zedGraphControl1.GraphPane.CurveList.Clear();
            _overviewUserControl1._zedGraphControl2.GraphPane.CurveList.Clear();
            _overviewUserControl1._zedGraphControl3.GraphPane.CurveList.Clear();
            _overviewUserControl1._zedGraphControl4.GraphPane.CurveList.Clear();
            _overviewUserControl1._zedGraphControl5.GraphPane.CurveList.Clear();


            CreateGraphBar(_overviewUserControl1._zedGraphControl1, "Real CF",
                           _globalCalculator.Context.Projections.Year,
                           _globalCalculator.Context.Projections.RealCashflow, Color.FromArgb(217, 150, 148));
            CreateGraphBar(_overviewUserControl1._zedGraphControl1, "Inflation Impact",
                           _globalCalculator.Context.Projections.Year,
                           _globalCalculator.Context.Overview.PureInflationEffect, Color.FromArgb(166, 166, 166));
            CreateGraphBar(_overviewUserControl1._zedGraphControl1, "Salary Impact",
                           _globalCalculator.Context.Projections.Year,
                           _globalCalculator.Context.INFSensAct.SalaryEffect, Color.Black);
            _overviewUserControl1._zedGraphControl1.GraphPane.XAxis.Scale.Max =
                _globalCalculator.Context.Projections.Year[_globalCalculator.Context.Projections.Year.Length - 1];
            _overviewUserControl1._zedGraphControl1.GraphPane.XAxis.Scale.Min =
                _globalCalculator.Context.Projections.Year[0];

            _overviewUserControl1._zedGraphControl1.AxisChange();

            CreateGraphBar(_overviewUserControl1._zedGraphControl2, "Liabilities - Intereset Rate Sensitivity",
                           _globalCalculator.Context.IrSensPeDe.YrKrPlusActivities,
                           _globalCalculator.Context.Overview.YrKr,
                           Color.FromArgb(242, 175, 0));
            CreateGraphBar(_overviewUserControl1._zedGraphControl2, "Liabilities - Inflation Sensitivity",
                           _globalCalculator.Context.INFSensPeDe.YrKrPlusActivities,
                           _globalCalculator.Context.Overview.YrKr,
                           Color.FromArgb(192, 58, 63));


            _overviewUserControl1._zedGraphControl2.AxisChange();

            CreateGraphBar(_overviewUserControl1._zedGraphControl3, "Current Swap - IR Sensitivity",
                           _globalCalculator.Context.Overview.CurrentSwapPortfolioSensitivitiesInterestRate,
                           _globalCalculator.Context.Overview.YrKr,
                           Color.FromArgb(229, 183, 0));
            CreateGraphBar(_overviewUserControl1._zedGraphControl3, "Current Swap - Inflation Sensitivity",
                           _globalCalculator.Context.Overview.CurrentSwapPortfolioSensitivitiesInflation,
                           _globalCalculator.Context.Overview.YrKr,
                           Color.FromArgb(132, 97, 23));
            CreateGraphBar(_overviewUserControl1._zedGraphControl3, "Assets - Intereset Rate Sensitivity",
                           _globalCalculator.Context.Overview.CurrentRiskAssetsInterestRate,
                           _globalCalculator.Context.Overview.YrKr,
                           Color.FromArgb(156, 113, 180));
            CreateGraphBar(_overviewUserControl1._zedGraphControl3, "Assets - Inflation Sensitivity",
                           _globalCalculator.Context.Overview.CurrentRiskAssetsInflation,
                           _globalCalculator.Context.Overview.YrKr,
                           Color.FromArgb(97, 77, 125));

            _overviewUserControl1._zedGraphControl3.AxisChange();

            CreateGraphBar(_overviewUserControl1._zedGraphControl4, "Required IR Swap PV01",
                           _globalCalculator.Context.Overview.SensitivitiesRequiredSwapsInterestRate,
                           _globalCalculator.Context.Overview.YrKr,
                           Color.FromArgb(127, 209, 199));
            CreateGraphBar(_overviewUserControl1._zedGraphControl4, "Required Inflation Swap PV01",
                           _globalCalculator.Context.Overview.SensitivitiesRequiredSwapsInflation,
                           _globalCalculator.Context.Overview.YrKr,
                           Color.FromArgb(0, 104, 91));

            _overviewUserControl1._zedGraphControl4.AxisChange();

            CreateGraphBar(_overviewUserControl1._zedGraphControl5, "Result - Asset Portfolio IR Sensitivity",
                           _globalCalculator.Context.Overview.ResultingTotalPortfolioSensitivityAssetsRates,
                           _globalCalculator.Context.Overview.YrKr,
                           Color.FromArgb(242, 175, 0));
            CreateGraphBar(_overviewUserControl1._zedGraphControl5, "Result - Asset Portfolio Inflation Sensitivity",
                           _globalCalculator.Context.Overview.ResultingTotalPortfolioSensitivityAssetsInflation,
                           _globalCalculator.Context.Overview.YrKr,
                           Color.FromArgb(192, 58, 63));

            _overviewUserControl1._zedGraphControl5.AxisChange();

            _overviewUserControl1._zedGraphControl1.Refresh();
            _overviewUserControl1._zedGraphControl2.Refresh();
            _overviewUserControl1._zedGraphControl3.Refresh();
            _overviewUserControl1._zedGraphControl4.Refresh();
            _overviewUserControl1._zedGraphControl5.Refresh();
        }

        private void CreateGridsForOverview(string gridName, IEnumerable<string> gridColumns, int col, int row)
        {
            _overviewGridUserControl = new OverviewGridUserControl
                                           {
                                               _titleLabel = { Text = gridName },
                                               Name = gridName,                                               
                                           };    
        
            foreach (var column in gridColumns)
            {
                var i = _overviewGridUserControl._grid.Columns.Add(column, column);
                _overviewGridUserControl._grid.Columns[i].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                _overviewGridUserControl._grid.Columns[i].DefaultCellStyle.Format = "N0";
                _overviewGridUserControl._grid.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
                _overviewGridUserControl._grid.Columns[i].Width = 88;
                _overviewGridUserControl._grid.Columns[i].MinimumWidth = 88;
                _overviewGridUserControl._grid.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCellsExceptHeader;
            }

            _overviewGridUserControl.Dock = DockStyle.Top;
            
            //_overviewUserControl1._gridPanel.Controls.Add(_overviewGridUserControl);
            _overviewUserControl1.tableLayoutPanel2.Controls.Add(_overviewGridUserControl, col , row);
        }


        private void ProjectionsAssets()
        {
            for (var i = 0; i < _globalCalculator.Context.TotalLiabScenario.YearFraction.Length; i++)
            {
                _assetsUserControl._grid.Rows.Add(
                    (double)(DateTime.Now.Year + i),
                    _globalCalculator.Context.Projections.EurSovereignBonds[i],
                    _globalCalculator.Context.Projections.InflationLinkedBondsEur[i],
                    _globalCalculator.Context.Projections.InterestRateSwaps[i],
                    _globalCalculator.Context.Projections.InflationSwapsFixedLeg[i],
                    _globalCalculator.Context.Projections.InflationSwapsInflationLeg[i],
                    _globalCalculator.Context.Projections.AssetsTotal[i],
// ReSharper disable RedundantCast
                    (double)(DateTime.Now.Year + i) - (double)1
// ReSharper restore RedundantCast
                    );
            }

            _assetsUserControl._zedGraphControl1.GraphPane.CurveList.Clear();
            _assetsUserControl._zedGraphControl2.GraphPane.CurveList.Clear();
            _assetsUserControl._zedGraphControl3.GraphPane.CurveList.Clear();

            CreateGraphBar(_assetsUserControl._zedGraphControl1, string.Empty, _globalCalculator.Context.Projections.Year, _globalCalculator.Context.Projections.EurSovereignBonds, Color.FromArgb(128,0,128));
            _assetsUserControl._zedGraphControl1.GraphPane.XAxis.Scale.Max =
                _globalCalculator.Context.Projections.Year[_globalCalculator.Context.Projections.Year.Length - 1];
            _assetsUserControl._zedGraphControl1.GraphPane.XAxis.Scale.Min =
                _globalCalculator.Context.Projections.Year[0];
            _assetsUserControl._zedGraphControl1.AxisChange();

            CreateGraphBar(_assetsUserControl._zedGraphControl2, string.Empty, _globalCalculator.Context.Projections.Year, _globalCalculator.Context.Projections.InflationLinkedBondsEur, Color.FromArgb(150, 23, 46));
            _assetsUserControl._zedGraphControl2.GraphPane.XAxis.Scale.Max =
                _globalCalculator.Context.Projections.Year[_globalCalculator.Context.Projections.Year.Length - 1];
            _assetsUserControl._zedGraphControl2.GraphPane.XAxis.Scale.Min =
                _globalCalculator.Context.Projections.Year[0];
            _assetsUserControl._zedGraphControl2.AxisChange();

            CreateGraphBar(_assetsUserControl._zedGraphControl3, string.Empty, _globalCalculator.Context.Projections.Year, _globalCalculator.Context.Projections.InterestRateSwaps, Color.FromArgb(207, 106, 110));
            CreateGraphBar(_assetsUserControl._zedGraphControl3, string.Empty, _globalCalculator.Context.Projections.Year, _globalCalculator.Context.Projections.InflationSwapsFixedLeg, Color.FromArgb(153, 171, 45));
            CreateGraphBar(_assetsUserControl._zedGraphControl3, string.Empty, _globalCalculator.Context.Projections.Year, _globalCalculator.Context.Projections.InflationSwapsInflationLeg, Color.FromArgb(0, 104, 91));
            _assetsUserControl._zedGraphControl3.GraphPane.XAxis.Scale.Max =
                _globalCalculator.Context.Projections.Year[_globalCalculator.Context.Projections.Year.Length - 1];
            _assetsUserControl._zedGraphControl3.GraphPane.XAxis.Scale.Min =
                _globalCalculator.Context.Projections.Year[0];
            _assetsUserControl._zedGraphControl3.AxisChange();

            _assetsUserControl._zedGraphControl1.Refresh();
            _assetsUserControl._zedGraphControl2.Refresh();
            _assetsUserControl._zedGraphControl3.Refresh();
        }

        private void ProjectionsLiabilities()
        {
            for (var i = 0; i < _globalCalculator.Context.TotalLiabScenario.YearFraction.Length; i++)
            {
                _liabilitiesUserControl._grid.Rows.Add(
                    (double)(DateTime.Now.Year + i),
                    _globalCalculator.Context.Projections.ActiveMembersIndexed[i],
                    _globalCalculator.Context.Projections.DeferredMembersIndexed[i],
                    _globalCalculator.Context.Projections.PensionersIndexed[i],
                    _globalCalculator.Context.Projections.ActiveMembersFixed[i],
                    _globalCalculator.Context.Projections.DeferredMembersFixed[i],
                    _globalCalculator.Context.Projections.PensionersFixed[i],
                    _globalCalculator.Context.Projections.LiabilitiesTotal[i],
                    (double)(DateTime.Now.Year + i - 1),
                    _globalCalculator.Context.Projections.RealCashflow[i],
                    _globalCalculator.Context.Projections.IndexedCashflow[i],
                    _globalCalculator.Context.Projections.UpliftCashflow[i],
                    _globalCalculator.Context.Projections.FixedCashflow[i]
                    );
            }

            if (_liabilitiesUserControl._grid.Rows.Count > 63)
            {
                for (var k = 0; k < _liabilitiesUserControl._grid.Rows.Count - 63; k++)
                    _liabilitiesUserControl._grid.Rows.RemoveAt(_liabilitiesUserControl._grid.Rows.Count);
            }

            _liabilitiesUserControl._zedGraphControl1.GraphPane.CurveList.Clear();
            _liabilitiesUserControl._zedGraphControl2.GraphPane.CurveList.Clear();

            CreateGraphBar(_liabilitiesUserControl._zedGraphControl1, "Fixed Cashflow", _globalCalculator.Context.Projections.YearMinusOne, _globalCalculator.Context.Projections.FixedCashflow, Color.FromArgb(128,0,0));
            CreateGraphBar(_liabilitiesUserControl._zedGraphControl1, "Nominal Cashflow", _globalCalculator.Context.Projections.YearMinusOne, _globalCalculator.Context.Projections.RealCashflow, Color.FromArgb(192, 58, 63));
            CreateGraphBar(_liabilitiesUserControl._zedGraphControl1, "Indexation Uplift", _globalCalculator.Context.Projections.YearMinusOne, _globalCalculator.Context.Projections.UpliftCashflow, Color.FromArgb(180, 190, 100));
            _liabilitiesUserControl._zedGraphControl1.AxisChange();

            CreateGraphCurve(_liabilitiesUserControl._zedGraphControl2, _globalCalculator.Context.Projections.Year, _globalCalculator.Context.Projections.ActiveMembersFixed, Color.FromArgb(0, 0, 255), 2);
            CreateGraphCurve(_liabilitiesUserControl._zedGraphControl2, _globalCalculator.Context.Projections.Year, _globalCalculator.Context.Projections.ActiveMembersIndexed, Color.FromArgb(0, 51, 102),2);
            CreateGraphCurve(_liabilitiesUserControl._zedGraphControl2, _globalCalculator.Context.Projections.Year, _globalCalculator.Context.Projections.DeferredMembersFixed, Color.FromArgb(192, 58, 63), 2);
            CreateGraphCurve(_liabilitiesUserControl._zedGraphControl2, _globalCalculator.Context.Projections.Year, _globalCalculator.Context.Projections.DeferredMembersIndexed, Color.DarkRed, 2);
            CreateGraphCurve(_liabilitiesUserControl._zedGraphControl2, _globalCalculator.Context.Projections.Year, _globalCalculator.Context.Projections.PensionersFixed, Color.FromArgb(242, 175, 0), 2);
            CreateGraphCurve(_liabilitiesUserControl._zedGraphControl2, _globalCalculator.Context.Projections.Year, _globalCalculator.Context.Projections.PensionersIndexed, Color.FromArgb(246, 139, 30), 2);
            _liabilitiesUserControl._zedGraphControl2.GraphPane.LineType = LineType.Stack;
            _liabilitiesUserControl._zedGraphControl2.AxisChange();

            _liabilitiesUserControl._zedGraphControl1.GraphPane.XAxis.Scale.Max =
                _globalCalculator.Context.Projections.Year[_globalCalculator.Context.Projections.YearMinusOne.Length - 1];
            _liabilitiesUserControl._zedGraphControl1.GraphPane.XAxis.Scale.Min =
                _globalCalculator.Context.Projections.YearMinusOne[0];
            _liabilitiesUserControl._zedGraphControl2.GraphPane.XAxis.Scale.Max =
                _globalCalculator.Context.Projections.Year[_globalCalculator.Context.Projections.Year.Length - 1];
            _liabilitiesUserControl._zedGraphControl2.GraphPane.XAxis.Scale.Min =
                _globalCalculator.Context.Projections.Year[0];

            _liabilitiesUserControl._zedGraphControl1.Refresh();
            _liabilitiesUserControl._zedGraphControl2.Refresh();
        }

        private static void CreateGraphBar(ZedGraphControl zgc, string label, double[] xValues, double[] yValues, Color color)
        {
            var zedPane = zgc.GraphPane;

            var bar = zedPane.AddBar(
                label,
                xValues,
                yValues,
                color);

            bar.Bar.Border.IsVisible = false;
            bar.Bar.Fill.Type = FillType.Solid;
            bar.Bar.Fill.Color = color;
        }

        private static void CreateGraphCurve(ZedGraphControl zgc, double[] xValues, double[] yValues, Color color, int lineWidth)
        {
            var zedPane = zgc.GraphPane;

            var curve = zedPane.AddCurve(
                string.Empty,
                xValues,
                yValues,
                color);

            curve.Line.Width = lineWidth == 0 ? 2 : lineWidth;

            curve.Symbol.IsVisible = false;
            curve.Line.Fill.Color = color;
        }

        static bool EndEdit(DataGridView grid)
        {
            var cell = grid.CurrentCell;
            if (cell != null && cell.IsInEditMode)
            {
                try
                {
                    grid.CurrentCell = null;
                }
                catch (Exception)
                {
                    return false;
                }
                grid.EndEdit();
            }
            return true;
        }

        private void toolStripButtonCalculate_Click(object sender, EventArgs e)
        {
            // try to end unfinished data input
            if (EndEdit(_overviewUserControl1._parametersUserControl._grid) && EndEdit(_liabilitiesUserControl._grid) && EndEdit(_assetsUserControl._grid))
            {
                Execute();
            }
        }

        private void DoSave(bool modelDefinition, params object[] data)
        {
            GetDataFromGrids();

            saveFileDialog.DefaultExt = modelDefinition ? "sad" : "sam";

            saveFileDialog.Filter = modelDefinition ? "Definition File (*.sad)|*.sad" : "Model Definition File (*.sam)|*.sam";

            saveFileDialog.FileName = "Sensitivity Analysis " + DateTime.Now.ToShortDateString();
            saveFileDialog.Title = "Save File";


            switch (saveFileDialog.ShowDialog())
            {
                case DialogResult.OK:
                    if (saveFileDialog.FileName.Length > 0 && saveFileDialog.FileName != " ")
                    {
                        var save = new FileOperations.Save();
                        save.Serialize(saveFileDialog.FileName, data);
                    }
                    break;
                default:
                    //MessageBox.Show("Please input a filename.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    break;
            }
        }

        private bool DoLoad(bool modelDefinition)
        {
            openFileDialog.DefaultExt = modelDefinition ? "sad" : "sam";

            openFileDialog.Filter = modelDefinition ? "Model Definition File (*.sad)|*.sad" : "Model File (*.sam)|*.sam";

            openFileDialog.AddExtension = true;
            openFileDialog.Title = "Open File";

            switch (openFileDialog.ShowDialog())
            {
                case DialogResult.OK:
                    if (openFileDialog.FileName.Length > 0)
                    {
                        LoadObject(openFileDialog.FileName);
                    }
                    return true;
                default:
                    //MessageBox.Show("Please select a file for loading.", "Error", MessageBoxButtons.OK,
                                    //MessageBoxIcon.Stop);
                    return false;
            }
        }

        private void LoadObject(string fileName)
        {
            var load = new FileOperations.Load();
            _loadedObject = new object();
            _loadedObject = load.Deserialize(fileName);
        }

        private void LoadParameters(object data)
        {
            _globalCalculator.Context.Parameters = (Parameters)data;

            SetDataToGrids(true);
        }

        private void LoadProjections(object data)
        {
            _globalCalculator.Context.Projections = ((Projections) data);

            SetDataToGrids(false);
        }

        private void SetDataToGrids(bool paramsOnly)
        {
            try
            {
                if (!paramsOnly)
                {
                    if (_liabilitiesUserControl._grid.Rows.Count > 0)
                    {
                        for (var i = 0; i < _liabilitiesUserControl._grid.Rows.Count; i++)
                        {
                            _liabilitiesUserControl._grid.Rows[i].Cells[1].Value =
                                _globalCalculator.Context.Projections.ActiveMembersIndexed[i];
                            _liabilitiesUserControl._grid.Rows[i].Cells[2].Value =
                                _globalCalculator.Context.Projections.DeferredMembersIndexed[i];
                            _liabilitiesUserControl._grid.Rows[i].Cells[3].Value =
                                _globalCalculator.Context.Projections.PensionersIndexed[i];
                            _liabilitiesUserControl._grid.Rows[i].Cells[4].Value =
                                _globalCalculator.Context.Projections.ActiveMembersFixed[i];
                            _liabilitiesUserControl._grid.Rows[i].Cells[5].Value =
                                _globalCalculator.Context.Projections.DeferredMembersFixed[i];
                            _liabilitiesUserControl._grid.Rows[i].Cells[6].Value =
                                _globalCalculator.Context.Projections.PensionersFixed[i];
                        }
                    }

                    if (_assetsUserControl._grid.Rows.Count != 0)
                    {
                        for (var i = 0; i < _assetsUserControl._grid.Rows.Count; i++)
                        {
                            _assetsUserControl._grid.Rows[i].Cells[1].Value =
                                _globalCalculator.Context.Projections.EurSovereignBonds[i];
                            _assetsUserControl._grid.Rows[i].Cells[2].Value =
                                _globalCalculator.Context.Projections.InflationLinkedBondsEur[i];
                            _assetsUserControl._grid.Rows[i].Cells[3].Value =
                                _globalCalculator.Context.Projections.InterestRateSwaps[i];
                            _assetsUserControl._grid.Rows[i].Cells[4].Value =
                                _globalCalculator.Context.Projections.InflationSwapsFixedLeg[i];
                            _assetsUserControl._grid.Rows[i].Cells[5].Value =
                                _globalCalculator.Context.Projections.InflationSwapsInflationLeg[i];
                        }
                    }
                }

                //parameters
                _overviewUserControl1._parametersUserControl.SetupGrid(_globalCalculator.Context);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
                return;
            }
        }

        private void saveModelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DoSave(false ,_globalCalculator.Context.Projections, _globalCalculator.Context.Parameters);
        }

        private void saveModelDefinitionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DoSave(true ,_globalCalculator.Context.Parameters);
        }

        private void loadModelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (DoLoad(false))
            {
                LoadParameters(((object[]) _loadedObject)[1]);
                LoadProjections(((object[]) _loadedObject)[0]);

                SetDataToGrids(false);
                Execute();
            }
        }

        private void loadModelDefinitionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (DoLoad(true))
            {
                LoadParameters(((object[]) _loadedObject)[0]);

                SetDataToGrids(true);
                Execute();
            }
        }

        private void calculateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Execute();
        }

        private readonly TabPage[] _tabPages = new TabPage[9];

        private void AdvancedCalculationTabsInit()
        {
            if (_tabPages[0] == null)
            {
                _tabPages[0] = (NewTab("IRSensPeDe", new IRSensPeDeTab()));
                _tabPages[1] = (NewTab("IRSensAct", new IRSensActTab()));
                _tabPages[2] = (NewTab("InfSensPeDe", new InfSensPeDeTab()));
                _tabPages[3] = (NewTab("InfSensAct", new InfSensActTab()));
                _tabPages[4] = (NewTab("FixedAssets", new FixedAssetsTab()));
                _tabPages[5] = (NewTab("ILAssets", new ILAssetsTab()));
                _tabPages[6] = (NewTab("IRSwapExposure", new IRSwapExposureTab()));
                _tabPages[7] = (NewTab("InflationSwapExposure", new InflationSwapExposureTab()));
                _tabPages[8] = (NewTab("TotalLiabScenario", new TotalLiabScenarioTab()));
            }
        }

        static TabPage NewTab(string title, AdvancedCalculationsUserControl content)
        {
            var tab = new TabPage(title) {Name = title};
            tab.Controls.Add(content);
            content.Init();
            content.Dock = DockStyle.Fill;
            return tab;
        }

        private void toolStripButtonShowAdvanced_Click(object sender, EventArgs e)
        {
            

            /*if (_isAdvancedCalculationsVisible)
            {
                _mainTabControl.TabPages["IRSensPeDe"].Hide();
                _mainTabControl.TabPages["IRSensAct"].Hide();
                _mainTabControl.TabPages["InfSensPeDe"].Hide();
                _mainTabControl.TabPages["InfSensAct"].Hide();
                _mainTabControl.TabPages["FixedAssets"].Hide();
                _mainTabControl.TabPages["ILAssets"].Hide();
                _mainTabControl.TabPages["IRSwapExposure"].Hide();
                _mainTabControl.TabPages["InflationSwapExposure"].Hide();

                _isAdvancedCalculationsVisible = false;
            }

            if(_globalCalculator.HasCalculated)
                AdvancedCalculationTabs();*/
        }

        private void toolStripButtonShowAdvanced_CheckedChanged(object sender, EventArgs e)
        {
            
        }

        private void toolStripButtonShowAdvanced_CheckStateChanged(object sender, EventArgs e)
        {
            if(toolStripButtonShowAdvanced.Checked)
            {
                _mainTabControl.TabPages.AddRange(_tabPages);
            }
            else
            {
                foreach (var page in _tabPages)
                {
                    _mainTabControl.TabPages.Remove(page);
                }
            }
        }

        public string[] Args
        {
            get { return this.args; }
            set { args = value; }
        }
    }
}