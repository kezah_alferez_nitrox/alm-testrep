﻿using System.Linq;
using Sensitivity_Analysis_Calculator.Calculators;

namespace Sensitivity_Analysis_Calculator.Sensitivity
{
    public class IRSensActTab:AdvancedCalculationsUserControl
    {
        private readonly YrKrHeader _yrKrHeader = new YrKrHeader();

        public override void Init()
        {
            GridInit(null, 63);

            _yrKrHeader.Init();

            AddUserControlTopToPanel(Graph(), _yrKrHeader);
        }

        public override void Populate(CalculContext context)
        {
            _yrKrHeader.PopulateYrKrGrid(context.Overview.CurrentInterestRateRiskLiabilitiesActives);
            PopulateGraph(context.Overview.CurrentInterestRateRiskLiabilitiesActives, context.Overview.YrKr);

            AddColumnToGrid(context.Projections.Year, "Year", 0,"0");
            AddColumnToGrid(context.TotalLiabScenario.DF, "Discount Curve", 0, "0.00%");
            AddColumnToGrid(context.TotalLiabScenario.Rate, "Underlying IR", 0, "0.00%");
            AddColumnToGrid(context.TotalLiabScenario.InterestRateShiftDown, "Discount Curve - 1bp", 0, "0.00%");
            AddColumnToGrid(context.TotalLiabScenario.InterestRateShiftUp, "Discount Curve + 1bp", 0, "0.00%");
            AddColumnToGrid(context.IrSensAct.LiabilityCF, "Liability CF (1)", 0,"N0");
            AddColumnToGrid(context.IrSensAct.PvCf1, "PV CF1", 0,"N0");
            AddColumnToGrid(context.IrSensAct.PvCf2, "PV CF2", 0,"N0");
            AddColumnToGrid(context.Projections.ActiveMembersIndexed, "Liability CF (2)", 0,"N0");

            AddYrKrColumns(context.IrSensAct.CalculatedYrKr);

            double[] values = new double[]
                                  {
                                      context.IrSensAct.CalculatedYrKr[0, 0].Sum(),
                                      context.IrSensAct.CalculatedYrKr[0, 1].Sum(),
                                      context.IrSensAct.CalculatedYrKr[1, 0].Sum(),
                                      context.IrSensAct.CalculatedYrKr[1, 1].Sum(),
                                      context.IrSensAct.CalculatedYrKr[2, 0].Sum(),
                                      context.IrSensAct.CalculatedYrKr[2, 1].Sum(),
                                      context.IrSensAct.CalculatedYrKr[3, 0].Sum(),
                                      context.IrSensAct.CalculatedYrKr[3, 1].Sum(),
                                      context.IrSensAct.CalculatedYrKr[4, 0].Sum(),
                                      context.IrSensAct.CalculatedYrKr[4, 1].Sum(),
                                      context.IrSensAct.CalculatedYrKr[5, 0].Sum(),
                                      context.IrSensAct.CalculatedYrKr[5, 1].Sum(),
                                      context.IrSensAct.CalculatedYrKr[6, 0].Sum(),
                                      context.IrSensAct.CalculatedYrKr[6, 1].Sum(),
                                      context.IrSensAct.CalculatedYrKr[7, 0].Sum(),
                                      context.IrSensAct.CalculatedYrKr[7, 1].Sum(),
                                      context.IrSensAct.CalculatedYrKr[8, 0].Sum(),
                                      context.IrSensAct.CalculatedYrKr[8, 1].Sum(),
                                      context.IrSensAct.CalculatedYrKr[9, 0].Sum(),
                                      context.IrSensAct.CalculatedYrKr[9, 1].Sum()
                                  };
            double[] valuesCalc = new double[]
                                      {
                                          CalculateTotal(context.IrSensAct.CalculatedYrKr[0, 0].Sum(), context.IrSensAct.CalculatedYrKr[0, 1].Sum()),
                                          CalculateTotal(context.IrSensAct.CalculatedYrKr[1, 0].Sum(), context.IrSensAct.CalculatedYrKr[1, 1].Sum()),
                                          CalculateTotal(context.IrSensAct.CalculatedYrKr[2, 0].Sum(), context.IrSensAct.CalculatedYrKr[2, 1].Sum()),
                                          CalculateTotal(context.IrSensAct.CalculatedYrKr[3, 0].Sum(), context.IrSensAct.CalculatedYrKr[3, 1].Sum()),
                                          CalculateTotal(context.IrSensAct.CalculatedYrKr[4, 0].Sum(), context.IrSensAct.CalculatedYrKr[4, 1].Sum()),
                                          CalculateTotal(context.IrSensAct.CalculatedYrKr[5, 0].Sum(), context.IrSensAct.CalculatedYrKr[5, 1].Sum()),
                                          CalculateTotal(context.IrSensAct.CalculatedYrKr[6, 0].Sum(), context.IrSensAct.CalculatedYrKr[6, 1].Sum()),
                                          CalculateTotal(context.IrSensAct.CalculatedYrKr[7, 0].Sum(), context.IrSensAct.CalculatedYrKr[7, 1].Sum()),
                                          CalculateTotal(context.IrSensAct.CalculatedYrKr[8, 0].Sum(), context.IrSensAct.CalculatedYrKr[8, 1].Sum()),
                                          CalculateTotal(context.IrSensAct.CalculatedYrKr[9, 0].Sum(), context.IrSensAct.CalculatedYrKr[9, 1].Sum())
                                      };

            AddSums("5YR KR - 1", values, valuesCalc);
            AddPvCfSums("PV CF1", context.IrSensAct.PvCf1.Sum(), context.IrSensAct.PvCf2.Sum());
        }
    }
}
