﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Linq;
using Sensitivity_Analysis_Calculator.Calculators;
using ZedGraph;
using System.Collections;

namespace Sensitivity_Analysis_Calculator.Sensitivity
{
    public partial class AdvancedCalculationsUserControl : UserControl
    {
        private readonly ZedGraphControl _zedGraphControl = new ZedGraphControl();

        private const int RowCount = 63;

        public AdvancedCalculationsUserControl()
        {
            InitializeComponent();
        }

        public virtual void Init()
        {

        }

        protected void GraphInit()
        {
            
        }

        public virtual void Populate(CalculContext context)
        {
            
        }

        protected void GridInit(string[] columnNames, int rows)
        {
            _grid.AllowUserToAddRows = false;
            _grid.AllowUserToDeleteRows = false;
            _grid.AllowUserToOrderColumns = false;
            _grid.AllowUserToResizeColumns = true;
            _grid.AllowUserToResizeRows = true;
            _grid.ReadOnly = true;

            _grid.Columns.Add("Period", "Period");

            _grid.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            for (int i = 0; i < RowCount; i++)
                _grid.Rows.Add((double)i);

            for (int i = 5; i < RowCount; i+=5)
            {
                _grid.Rows[i].DefaultCellStyle.BackColor = Color.LightGray;
                _grid.Rows[i].DefaultCellStyle.Font = new Font(
                    _grid.DefaultCellStyle.Font.FontFamily,
                    _grid.DefaultCellStyle.Font.Size,
                    FontStyle.Bold
                    );
            }
        }

        protected void AddUserControlTopToPanel(params Control[] control)
        {
            foreach (var ctrl in control)
            {
                splitContainer1.Panel1.Controls.Add(ctrl);
                ctrl.Dock = DockStyle.Left;
            }
        }

        protected ZedGraphControl Graph()
        {
            _zedGraphControl.GraphPane.Title.IsVisible = false;
            _zedGraphControl.GraphPane.YAxis.Title.IsVisible = false;
            _zedGraphControl.GraphPane.BarSettings.Base = BarBase.Y;
            _zedGraphControl.GraphPane.BarSettings.MinClusterGap = 0.3f;
            _zedGraphControl.GraphPane.YAxis.ScaleFormatEvent += YAxis_ScaleFormatEvent;
            _zedGraphControl.DoubleClick+=new System.EventHandler(_zedGraphControl_DoubleClick);
            _zedGraphControl.Width = 478;
            _zedGraphControl.Height = 204;

            return _zedGraphControl;
        }

        private void _zedGraphControl_DoubleClick(object sender, EventArgs e)
        {
            new ZedGraphPopUp().ShowZedGraphInPopUp((ZedGraphControl)sender);
        }

        protected void PopulateGraph(double[] xValues, double[] yValues)
        {
            var zedPane = _zedGraphControl.GraphPane;
            var label = string.Empty;
            var color = Color.Red;

            zedPane.CurveList.Clear();

            if (xValues != null && yValues != null)
            {
                var bar = zedPane.AddBar(
                    label,
                    xValues,
                    yValues,
                    color);

                bar.Bar.Border.IsVisible = false;
                bar.Bar.Fill.Type = FillType.Solid;
                bar.Bar.Fill.Color = color;

                //_zedGraphControl.GraphPane.XAxis.Scale.Min = 0;
                _zedGraphControl.GraphPane.XAxis.Scale.Max = xValues.Max();
                _zedGraphControl.GraphPane.YAxis.Scale.MajorStep = 5;
                _zedGraphControl.GraphPane.YAxis.Scale.Min = 0;
                _zedGraphControl.GraphPane.YAxis.Scale.Max = 53;
                _zedGraphControl.GraphPane.YAxis.Scale.IsSkipFirstLabel = true;
            }

            _zedGraphControl.GraphPane.AxisChange();
        }

        static string YAxis_ScaleFormatEvent(GraphPane pane, Axis axis, double val, int index)
        {
            return val.ToString("0") + "YR KR";
        }

        //protected void AddColumnToGrid(ICollection values, string title, int startIndex, string format, params double?[] footer)
        //{
        //    var colIndex = _grid.Columns.Add(title, title);

        //    _grid.Columns[colIndex].DefaultCellStyle.Format = format;

        //    _grid.Columns[colIndex].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

        //    _grid.Columns[colIndex].SortMode = DataGridViewColumnSortMode.NotSortable;

        //    for (var i = 0; i < dates.Length; i++)
        //        _grid.Rows[startIndex + i].Cells[colIndex].Value = dates[i];

        //    if (footer != null && footer.Length != 0)
        //    {
        //        while (_grid.Rows.Count < RowCount + footer.Length)
        //            _grid.Rows.Add(1);

        //        for (int i = 0; i < footer.Length; i++)
        //            if (footer[i] != null)
        //                _grid.Rows[RowCount + i].Cells[colIndex].Value = footer[i];
        //    }
        //}

        protected int AddColumnToGrid(IList values, string title, int startIndex, string format, params double?[] footer)
        {
            int colIndex = !_grid.Columns.Contains(title) ? _grid.Columns.Add(title, title) : _grid.Columns[title].Index;

            _grid.Columns[colIndex].DefaultCellStyle.Format = format;
            
            _grid.Columns[colIndex].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            _grid.Columns[colIndex].SortMode = DataGridViewColumnSortMode.NotSortable;

            for(var i = 0; i< values.Count;i++)
                _grid.Rows[startIndex + i].Cells[colIndex].Value = values[i];

            if (footer != null && footer.Length != 0)
            {
                while (_grid.Rows.Count < RowCount + footer.Length)
                    _grid.Rows.Add(1);

                for (int i = 0; i < footer.Length; i++)
                    if (footer[i] != null)
                        _grid.Rows[RowCount + i].Cells[colIndex].Value = footer[i];
            }
            
            return colIndex;
        }

        public void AddYrKrColumns(double[,][] calculatedYrKr)
        {
            for (var i = 0; i <= 9; i++)
            {
                //var sum0 = calculatedYrKr[i, 0].Sum();
                //var sum1 = calculatedYrKr[i, 1].Sum();
                //var total = (sum0 - sum1)/2;
                //AddColumnToGrid(calculatedYrKr[i, 0], (i * 5 + 5) + "YR KR - 1", i * 5, "N0", sum0);
                //var colIndex1 = AddColumnToGrid(calculatedYrKr[i, 1], (i * 5 + 5) + "YR KR - 2", i * 5, "N0", sum1, total, total / sum1);
                //_grid.Rows[62 + 3].Cells[colIndex1].Style.Format = "0.000%";

                AddColumnToGrid(calculatedYrKr[i, 0], (i * 5 + 5) + "YR KR - 1", i * 5, "N0");
                AddColumnToGrid(calculatedYrKr[i, 1], (i * 5 + 5) + "YR KR - 2", i * 5, "N0");
            }
        }

        public double CalculateTotal(double value1, double value2)
        {
            return (value1 - value2)/(double) 2;
        }

        public double CalculateTotalPercent(double value1, double value2)
        {
            return value1/value2;
        }

        public void AddSums(string startColName, double[] sumValues, double[] yrKrValues)
        {
            if(_grid.Rows.Count <= 63)
                _grid.Rows.Add(3);

            for (int i = 0; i < sumValues.Length; i++)
            {
                _grid.Rows[_grid.Rows.Count - 3].Cells[_grid.Columns[startColName].Index + i].Value = sumValues[i];
            }

            for (var i = 0; i < yrKrValues.Length; i++)
            {
                _grid.Rows[_grid.Rows.Count - 2].Cells[_grid.Columns[startColName].Index + i * 2 + 1].Value = yrKrValues[i];

                //if (i * 2 + 1 < yrKrValues.Length)
                _grid.Rows[_grid.Rows.Count - 1].Cells[_grid.Columns[startColName].Index + i * 2 + 1].Value = yrKrValues[i] / sumValues[i * 2 + 1];

                _grid.Rows[_grid.Rows.Count - 1].Cells[_grid.Columns[startColName].Index + i * 2 + 1].Style.Format = "0.000%";
            }
        }

        public void AddPvCfSums(string startColName, double pvCf1Sum, double pvCf2Sum)
        {
            var total = CalculateTotal(pvCf1Sum, pvCf2Sum);
            _grid.Rows[_grid.Rows.Count - 3].Cells[_grid.Columns[startColName].Index].Value = pvCf1Sum;
            _grid.Rows[_grid.Rows.Count - 3].Cells[_grid.Columns[startColName].Index + 1].Value = pvCf2Sum;
            _grid.Rows[_grid.Rows.Count - 2].Cells[_grid.Columns[startColName].Index + 1].Value = total;
            _grid.Rows[_grid.Rows.Count - 1].Cells[_grid.Columns[startColName].Index + 1].Value = total / pvCf2Sum;
            _grid.Rows[_grid.Rows.Count - 1].Cells[_grid.Columns[startColName].Index + 1].Style.Format = "0.000%";
        }

        public void AddYrKr(string startColName, bool calcPvCfValues, params double[] values)
        {
            var count = 11;

            if (!calcPvCfValues)
                count = 12;

            for (int i = 0; i < count; i+=2)
            {
                
            }
        }
    }
}
