﻿using System.Windows.Forms;

namespace Sensitivity_Analysis_Calculator.Sensitivity
{
    public partial class OverviewGridUserControl : UserControl
    {
        public OverviewGridUserControl()
        {
            InitializeComponent();

            _grid.VisibleChanged += GridVisibleChanged;

            _grid.AllowUserToAddRows = false;
            _grid.AllowUserToDeleteRows = false;
            _grid.AllowUserToOrderColumns = false;
            _grid.AllowUserToResizeColumns = true;
            _grid.AllowUserToResizeRows = true;
            _grid.ReadOnly = true;
        }

        void GridVisibleChanged(object sender, System.EventArgs e)
        {
           if(_grid.Rows[_grid.Rows.Count - 1].Cells[0].Value == null)
                _grid.Rows.RemoveAt(_grid.Rows.Count - 1);
        }

        public void LoadData(string title, object[] data)
        {
            _titleLabel.Text = title;

            foreach (var row in data)
            {
                _grid.Rows.Add(row);
            }
        }
    }
}