﻿namespace Sensitivity_Analysis_Calculator.Sensitivity
{
    partial class AssetsUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer _components;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (_components != null))
            {
                _components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this._splitContainer = new System.Windows.Forms.SplitContainer();
            this._grid = new System.Windows.Forms.DataGridView();
            this._panel1 = new System.Windows.Forms.Panel();
            this._zedGraphControl3 = new ZedGraph.ZedGraphControl();
            this._zedGraphControl2 = new ZedGraph.ZedGraphControl();
            this._zedGraphControl1 = new ZedGraph.ZedGraphControl();
            this._splitContainer.Panel1.SuspendLayout();
            this._splitContainer.Panel2.SuspendLayout();
            this._splitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._grid)).BeginInit();
            this._panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // _splitContainer
            // 
            this._splitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this._splitContainer.Location = new System.Drawing.Point(0, 0);
            this._splitContainer.Name = "_splitContainer";
            // 
            // _splitContainer.Panel1
            // 
            this._splitContainer.Panel1.Controls.Add(this._grid);
            // 
            // _splitContainer.Panel2
            // 
            this._splitContainer.Panel2.Controls.Add(this._panel1);
            this._splitContainer.Size = new System.Drawing.Size(1013, 590);
            this._splitContainer.SplitterDistance = 753;
            this._splitContainer.TabIndex = 0;
            // 
            // _grid
            // 
            this._grid.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._grid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this._grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._grid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._grid.Location = new System.Drawing.Point(0, 0);
            this._grid.Name = "_grid";
            this._grid.Size = new System.Drawing.Size(753, 590);
            this._grid.TabIndex = 0;
            this._grid.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this._grid_CellValidating);
            this._grid.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this._grid_CellEndEdit);
            this._grid.KeyDown += new System.Windows.Forms.KeyEventHandler(this._grid_KeyDown);
            // 
            // _panel1
            // 
            this._panel1.AutoScroll = true;
            this._panel1.Controls.Add(this._zedGraphControl3);
            this._panel1.Controls.Add(this._zedGraphControl2);
            this._panel1.Controls.Add(this._zedGraphControl1);
            this._panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this._panel1.Location = new System.Drawing.Point(0, 0);
            this._panel1.Name = "_panel1";
            this._panel1.Size = new System.Drawing.Size(256, 590);
            this._panel1.TabIndex = 0;
            // 
            // _zedGraphControl3
            // 
            this._zedGraphControl3.Dock = System.Windows.Forms.DockStyle.Top;
            this._zedGraphControl3.Location = new System.Drawing.Point(0, 560);
            this._zedGraphControl3.Name = "_zedGraphControl3";
            this._zedGraphControl3.ScrollGrace = 0;
            this._zedGraphControl3.ScrollMaxX = 0;
            this._zedGraphControl3.ScrollMaxY = 0;
            this._zedGraphControl3.ScrollMaxY2 = 0;
            this._zedGraphControl3.ScrollMinX = 0;
            this._zedGraphControl3.ScrollMinY = 0;
            this._zedGraphControl3.ScrollMinY2 = 0;
            this._zedGraphControl3.Size = new System.Drawing.Size(239, 280);
            this._zedGraphControl3.TabIndex = 2;
            // 
            // _zedGraphControl2
            // 
            this._zedGraphControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this._zedGraphControl2.Location = new System.Drawing.Point(0, 280);
            this._zedGraphControl2.Name = "_zedGraphControl2";
            this._zedGraphControl2.ScrollGrace = 0;
            this._zedGraphControl2.ScrollMaxX = 0;
            this._zedGraphControl2.ScrollMaxY = 0;
            this._zedGraphControl2.ScrollMaxY2 = 0;
            this._zedGraphControl2.ScrollMinX = 0;
            this._zedGraphControl2.ScrollMinY = 0;
            this._zedGraphControl2.ScrollMinY2 = 0;
            this._zedGraphControl2.Size = new System.Drawing.Size(239, 280);
            this._zedGraphControl2.TabIndex = 1;
            // 
            // _zedGraphControl1
            // 
            this._zedGraphControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this._zedGraphControl1.Location = new System.Drawing.Point(0, 0);
            this._zedGraphControl1.Name = "_zedGraphControl1";
            this._zedGraphControl1.ScrollGrace = 0;
            this._zedGraphControl1.ScrollMaxX = 0;
            this._zedGraphControl1.ScrollMaxY = 0;
            this._zedGraphControl1.ScrollMaxY2 = 0;
            this._zedGraphControl1.ScrollMinX = 0;
            this._zedGraphControl1.ScrollMinY = 0;
            this._zedGraphControl1.ScrollMinY2 = 0;
            this._zedGraphControl1.Size = new System.Drawing.Size(239, 280);
            this._zedGraphControl1.TabIndex = 0;
            // 
            // AssetsUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._splitContainer);
            this.Name = "AssetsUserControl";
            this.Size = new System.Drawing.Size(1013, 590);
            this.Load += new System.EventHandler(this.AssetsUserControl_Load);
            this._splitContainer.Panel1.ResumeLayout(false);
            this._splitContainer.Panel2.ResumeLayout(false);
            this._splitContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._grid)).EndInit();
            this._panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer _splitContainer;
        private System.Windows.Forms.Panel _panel1;
        private System.ComponentModel.IContainer components;
        public ZedGraph.ZedGraphControl _zedGraphControl3;
        public ZedGraph.ZedGraphControl _zedGraphControl2;
        public ZedGraph.ZedGraphControl _zedGraphControl1;
        public System.Windows.Forms.DataGridView _grid;
    }
}