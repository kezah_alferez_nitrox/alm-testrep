﻿using System.Linq;
using Sensitivity_Analysis_Calculator.Calculators;

namespace Sensitivity_Analysis_Calculator.Sensitivity
{
    class InfSensPeDeTab : AdvancedCalculationsUserControl
    {
        private readonly YrKrHeader _yrKrHeader = new YrKrHeader();
        private readonly YrKrHeader _yrKrHeader2 = new YrKrHeader();

        public override void Init()
        {
            GridInit(null, 63);

            _yrKrHeader.Init();
            _yrKrHeader2.Init();

            AddUserControlTopToPanel(Graph(), _yrKrHeader, _yrKrHeader2);
        }

        public override void Populate(CalculContext context)
        {
            _yrKrHeader.PopulateYrKrGrid(context.Overview.CurrentInflationRiskLiabilitiesPensionersDeferredsPlusActivities, "PV1 + actives");
            _yrKrHeader2.PopulateYrKrGrid(context.Overview.CurrentInflationRiskLiabilitiesPensionersDeferreds);

            PopulateGraph(context.Overview.CurrentInterestRateRiskLiabilitiesActives, context.Overview.YrKr);

            AddColumnToGrid(context.Projections.Year, "Year", 0,"0");
            AddColumnToGrid(context.TotalLiabScenario.YearFraction, "Year Fraction", 0,"0.00");
            AddColumnToGrid(context.TotalLiabScenario.DF, "Discount Curve", 0, "0.00%");
            AddColumnToGrid(context.TotalLiabScenario.InflationIndexPrivate, "Inflation Index", 0,"N0");
            AddColumnToGrid(context.TotalLiabScenario.InflationShift, "Inflation", 0, "0.00%");
            AddColumnToGrid(context.TotalLiabScenario.IndexPlus1BpPrivate, "Index +1bp (1)", 0,"N0");
            AddColumnToGrid(context.TotalLiabScenario.InflationShiftUp, "Inflation +1bp (1)", 0, "0.00%");
            AddColumnToGrid(context.TotalLiabScenario.IndexMinus1BpPrivate, "Index +1bp (2)", 0,"N0");
            AddColumnToGrid(context.TotalLiabScenario.InflationShiftDown, "Inflation -1bp (2)", 0, "0.00%");
            AddColumnToGrid(context.INFSensPeDe.NominalCfFixed, "Nominal CF Fixed", 0,"N0");
            AddColumnToGrid(context.INFSensPeDe.ActualCF, "Actual CF", 0,"N0");
            AddColumnToGrid(context.INFSensPeDe.PvCf1, "PV CF1", 0,"N0");
            AddColumnToGrid(context.INFSensPeDe.PvCf2, "PV CF2", 0,"N0");

            AddYrKrColumns(context.INFSensPeDe.CalculatedYrKr);

            double[] values = new double[]
                                  {
                                      context.INFSensPeDe.CalculatedYrKr[0, 0].Sum(),
                                      context.INFSensPeDe.CalculatedYrKr[0, 1].Sum(),
                                      context.INFSensPeDe.CalculatedYrKr[1, 0].Sum(),
                                      context.INFSensPeDe.CalculatedYrKr[1, 1].Sum(),
                                      context.INFSensPeDe.CalculatedYrKr[2, 0].Sum(),
                                      context.INFSensPeDe.CalculatedYrKr[2, 1].Sum(),
                                      context.INFSensPeDe.CalculatedYrKr[3, 0].Sum(),
                                      context.INFSensPeDe.CalculatedYrKr[3, 1].Sum(),
                                      context.INFSensPeDe.CalculatedYrKr[4, 0].Sum(),
                                      context.INFSensPeDe.CalculatedYrKr[4, 1].Sum(),
                                      context.INFSensPeDe.CalculatedYrKr[5, 0].Sum(),
                                      context.INFSensPeDe.CalculatedYrKr[5, 1].Sum(),
                                      context.INFSensPeDe.CalculatedYrKr[6, 0].Sum(),
                                      context.INFSensPeDe.CalculatedYrKr[6, 1].Sum(),
                                      context.INFSensPeDe.CalculatedYrKr[7, 0].Sum(),
                                      context.INFSensPeDe.CalculatedYrKr[7, 1].Sum(),
                                      context.INFSensPeDe.CalculatedYrKr[8, 0].Sum(),
                                      context.INFSensPeDe.CalculatedYrKr[8, 1].Sum(),
                                      context.INFSensPeDe.CalculatedYrKr[9, 0].Sum(),
                                      context.INFSensPeDe.CalculatedYrKr[9, 1].Sum()
                                  };
            double[] valuesCalc = new double[]
                                      {
                                          CalculateTotal(context.INFSensPeDe.CalculatedYrKr[0, 0].Sum(), context.INFSensPeDe.CalculatedYrKr[0, 1].Sum()),
                                          CalculateTotal(context.INFSensPeDe.CalculatedYrKr[1, 0].Sum(), context.INFSensPeDe.CalculatedYrKr[1, 1].Sum()),
                                          CalculateTotal(context.INFSensPeDe.CalculatedYrKr[2, 0].Sum(), context.INFSensPeDe.CalculatedYrKr[2, 1].Sum()),
                                          CalculateTotal(context.INFSensPeDe.CalculatedYrKr[3, 0].Sum(), context.INFSensPeDe.CalculatedYrKr[3, 1].Sum()),
                                          CalculateTotal(context.INFSensPeDe.CalculatedYrKr[4, 0].Sum(), context.INFSensPeDe.CalculatedYrKr[4, 1].Sum()),
                                          CalculateTotal(context.INFSensPeDe.CalculatedYrKr[5, 0].Sum(), context.INFSensPeDe.CalculatedYrKr[5, 1].Sum()),
                                          CalculateTotal(context.INFSensPeDe.CalculatedYrKr[6, 0].Sum(), context.INFSensPeDe.CalculatedYrKr[6, 1].Sum()),
                                          CalculateTotal(context.INFSensPeDe.CalculatedYrKr[7, 0].Sum(), context.INFSensPeDe.CalculatedYrKr[7, 1].Sum()),
                                          CalculateTotal(context.INFSensPeDe.CalculatedYrKr[8, 0].Sum(), context.INFSensPeDe.CalculatedYrKr[8, 1].Sum()),
                                          CalculateTotal(context.INFSensPeDe.CalculatedYrKr[9, 0].Sum(), context.INFSensPeDe.CalculatedYrKr[9, 1].Sum())
                                      };

            AddSums("5YR KR - 1", values, valuesCalc);
        }
    }
}
