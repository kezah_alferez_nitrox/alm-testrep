﻿using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace Sensitivity_Analysis_Calculator.FileOperations
{
    public class Load
    {
        public object Deserialize(string filename)
        {
            object result;

            using (Stream stream = new FileStream(filename, FileMode.Open))
            {
                IFormatter formatter = new BinaryFormatter();

                result = formatter.Deserialize(stream);

                stream.Close();
            }

            return result;
        }
    }
}
