﻿using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace Sensitivity_Analysis_Calculator.FileOperations
{
    public class Save
    {
        public void Serialize(string filename, params object[] obj)
        {
            using (Stream stream = new FileStream(filename, FileMode.Create))
            {
                IFormatter formatter = new BinaryFormatter();

                formatter.Serialize(stream, obj);

                stream.Close();
            }
        }
    }
}
