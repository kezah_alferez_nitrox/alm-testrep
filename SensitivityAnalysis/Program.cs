﻿using System;
using System.Windows.Forms;

namespace Sensitivity_Analysis_Calculator
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            //Application.CurrentCulture = new System.Globalization.CultureInfo("en-US", false)
            //                                 {
            //                                     NumberFormat =
            //                                         {
            //                                             NumberGroupSeparator = ",",
            //                                             NumberDecimalSeparator = "."
            //                                         }
            //                                 };

            Application.Run(new Sensitivity.SensitivityForm(args));
        }
    }
}
