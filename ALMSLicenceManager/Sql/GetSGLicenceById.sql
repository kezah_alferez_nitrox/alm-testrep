USE [licence.altersystems.fr]
GO
/****** Object:  StoredProcedure [dbo].[GetSGLicenceById]    Script Date: 01/02/2011 22:45:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		CDU
-- Create date: 
-- Description:	
-- =============================================
drop PROCEDURE [dbo].[GetSGLicenceById]
go

CREATE PROCEDURE [dbo].[GetSGLicenceById] 
	-- Add the parameters for the stored procedure here
	@LICENCEID numeric(18, 0) = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   SELECT     
		L.LICENCEID,  
		L.DEBUT, L.FIN, 
		FN.METADATAVALUE as FIRSTNAME,
		LN.METADATAVALUE as LASTNAME,
		convert(varchar(max), M.METADATAVALUE) as MODULES
	FROM LICENCE AS L 
		LEFT OUTER JOIN METADATA AS M ON L.LICENCEID = M.LICENCEID AND M.METADATAKEY = 'Modules' 
		LEFT OUTER JOIN METADATA AS FN ON L.LICENCEID = FN.LICENCEID AND FN.METADATAKEY = 'firstName' 
		LEFT OUTER JOIN METADATA AS LN ON L.LICENCEID = LN.LICENCEID AND LN.METADATAKEY = 'lastName'
	WHERE L.LICENCEID = @LICENCEID
	
END
