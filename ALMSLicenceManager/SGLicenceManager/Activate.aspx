﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Activate.aspx.cs" Inherits="Activate" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div style="font-size: x-large;">       
        <b>ALM Solutions License Activation</b></div>
    <br />
    <asp:Label ID="messageLabel" Font-Size="Large" Font-Bold="true" ForeColor="DarkRed"
        runat="server"></asp:Label>
    <br />
    <br />
    <asp:Panel ID="panel" runat="server" Visible="false">
        <table border="0">
            <tbody>
                <tr>
                    <td>
                        Licence Number:
                    </td>
                    <td>
                        <asp:TextBox ID="licenceNumberTextBox" runat="server" ReadOnly="True" Width="400px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        Activation Key:
                    </td>
                    <td>
                        <asp:TextBox ID="activationKeyTextBox" runat="server" Height="200px" ReadOnly="True"
                            Width="780px" TextMode="MultiLine"></asp:TextBox>
                    </td>
                </tr>
            </tbody>
        </table>
    </asp:Panel>
    </form>
</body>
</html>
