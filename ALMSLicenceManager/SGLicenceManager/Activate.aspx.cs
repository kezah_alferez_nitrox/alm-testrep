﻿using System;
using System.Configuration;
using System.Web.UI;
using System.Data.SqlClient;
using ALMSCommon;

public partial class Activate : Page
{
    private int produitId;
    private int contratId;

    protected void Page_Load(object sender, EventArgs e)
    {
        string licenceNumber = this.Request.QueryString["l"];
        string machineId = this.Request.QueryString["m"];

        if (string.IsNullOrEmpty(licenceNumber) || string.IsNullOrEmpty(machineId))
        {
            messageLabel.Text = "Invalid operation";
            return;
        }

        licenceNumberTextBox.Text = licenceNumber;

        this.produitId = int.Parse(ConfigurationManager.AppSettings["PRODUITID"]);
        this.contratId = int.Parse(ConfigurationManager.AppSettings["CONTRATID"]);

        licenceNumber = licenceNumber.Replace("-", "").Replace(" ", "").ToUpper();

        LicenceInfo licenceInfo;
        try
        {
            licenceInfo = GetLicenceData(licenceNumber);
        }
        catch (Exception exception)
        {
            this.Context.Trace.Write(exception.ToString());
            this.messageLabel.Text = "An error has occured";
            return;
        }

        if (licenceInfo == null)
        {
            messageLabel.Text = "Invalid licence number !";
            return;
        }

        if (licenceInfo.MachineId != null && licenceInfo.MachineId != machineId)
        {
            messageLabel.Text = "This Licence Number is already in use !";
            return;
        }

        licenceInfo.MachineId = machineId;
        licenceInfo.FirstName = this.Request.QueryString["fn"];
        licenceInfo.LastName = this.Request.QueryString["ln"];
        licenceInfo.Company = this.Request.QueryString["c"];
        licenceInfo.PhoneNumber = this.Request.QueryString["p"];
        licenceInfo.Email = this.Request.QueryString["e"];

        this.UpdateLicence(licenceNumber, machineId);
        this.UpdateMetadata(licenceInfo.LicenceId, "firstName", licenceInfo.FirstName);
        this.UpdateMetadata(licenceInfo.LicenceId, "lastName", licenceInfo.LastName);
        this.UpdateMetadata(licenceInfo.LicenceId, "company", licenceInfo.Company);
        this.UpdateMetadata(licenceInfo.LicenceId, "phone", licenceInfo.PhoneNumber);
        this.UpdateMetadata(licenceInfo.LicenceId, "email", licenceInfo.Email);

        panel.Visible = true;

        this.activationKeyTextBox.Text = licenceInfo.Serialize();

        messageLabel.Text = "Activation succeded";
    }

    private LicenceInfo GetLicenceData(string licenceNumber)
    {
        const string sql = @"SELECT LICENCEID, MACHINE_ID, DEBUT, FIN
                FROM LICENCE
                WHERE PRODUITID = @PRODUITID AND CONTRATID = @CONTRATID AND NUMERO_SERIE = @NUMERO_SERIE";

        string connectionString = ConfigurationManager.ConnectionStrings["connectionString"].ConnectionString;
        using (SqlConnection connection = new SqlConnection(connectionString))
        {
            connection.Open();

            SqlCommand sqlCommand = new SqlCommand(sql, connection);
            sqlCommand.Parameters.AddWithValue("@PRODUITID", produitId);
            sqlCommand.Parameters.AddWithValue("@CONTRATID", contratId);
            sqlCommand.Parameters.AddWithValue("@NUMERO_SERIE", licenceNumber);

            using (SqlDataReader reader = sqlCommand.ExecuteReader())
            {
                if (!reader.Read())
                {
                    return null;
                }

                LicenceInfo licenceInfo = new LicenceInfo();
                licenceInfo.LicenceNumber = licenceNumber;

                object read = reader["MACHINE_ID"];
                licenceInfo.MachineId = read == DBNull.Value ? null : Convert.ToString(read);

                read = reader["LICENCEID"];
                licenceInfo.LicenceId = read == DBNull.Value ? 0 : (decimal)read;
                licenceInfo.Metadata = GetMetadata(licenceInfo.LicenceId);

                read = reader["DEBUT"];
                licenceInfo.Start = read == DBNull.Value ? DateTime.MinValue : (DateTime)(read);

                read = reader["FIN"];
                licenceInfo.End = read == DBNull.Value ? DateTime.MaxValue : (DateTime)(read);

                return licenceInfo;
            }
        }
    }

    private static SerializableDictionary<string, string> GetMetadata(decimal licenceId)
    {
        SerializableDictionary<string, string> metadata = new SerializableDictionary<string, string>();
        if (licenceId == 0) return metadata;

        const string sql = @"SELECT METADATAKEY, METADATAVALUE
                FROM METADATA
                WHERE LICENCEID = @LICENCEID";

        string connectionString = ConfigurationManager.ConnectionStrings["connectionString"].ConnectionString;
        using (SqlConnection connection = new SqlConnection(connectionString))
        {
            connection.Open();

            SqlCommand sqlCommand = new SqlCommand(sql, connection);
            sqlCommand.Parameters.AddWithValue("@LICENCEID", licenceId);

            using (SqlDataReader reader = sqlCommand.ExecuteReader())
            {
                while (reader.Read())
                {
                    object read = reader["METADATAKEY"];
                    if (read == DBNull.Value) continue;
                    string key = Convert.ToString(read);
                    if (string.IsNullOrEmpty(key)) continue;

                    read = reader["METADATAVALUE"];
                    metadata.Add(key, read == DBNull.Value ? "" : Convert.ToString(read));
                }
            }
        }

        return metadata;
    }

    private void UpdateLicence(string licenceNumber, string machineId)
    {
        const string sql = @"UPDATE LICENCE
                SET MACHINE_ID = @MACHINE_ID
                WHERE PRODUITID = @PRODUITID AND CONTRATID = @CONTRATID AND NUMERO_SERIE = @NUMERO_SERIE";

        string connectionString = ConfigurationManager.ConnectionStrings["connectionString"].ConnectionString;
        using (SqlConnection connection = new SqlConnection(connectionString))
        {
            connection.Open();

            SqlCommand sqlCommand = new SqlCommand(sql, connection);
            sqlCommand.Parameters.AddWithValue("@PRODUITID", produitId);
            sqlCommand.Parameters.AddWithValue("@CONTRATID", contratId);
            sqlCommand.Parameters.AddWithValue("@NUMERO_SERIE", licenceNumber);
            sqlCommand.Parameters.AddWithValue("@MACHINE_ID", machineId);

            sqlCommand.ExecuteNonQuery();
        }
    }

    private void UpdateMetadata(decimal licenceId, string key, string value)
    {
        string sql;
        if (MetadataExists(licenceId, key))
        {
            sql = @"UPDATE METADATA 
                        SET METADATAVALUE = @METADATAVALUE 
                    WHERE LICENCEID = @LICENCEID AND METADATAKEY = @METADATAKEY";
        }
        else
        {
            sql = @"INSERT INTO METADATA(LICENCEID, METADATAKEY, METADATAVALUE)
                    Values(@LICENCEID, @METADATAKEY, @METADATAVALUE)";
        }

        string connectionString = ConfigurationManager.ConnectionStrings["connectionString"].ConnectionString;
        using (SqlConnection connection = new SqlConnection(connectionString))
        {
            connection.Open();

            SqlCommand sqlCommand = new SqlCommand(sql, connection);
            sqlCommand.Parameters.AddWithValue("@LICENCEID", licenceId);
            sqlCommand.Parameters.AddWithValue("@METADATAKEY", key);
            sqlCommand.Parameters.AddWithValue("@METADATAVALUE", value);

            sqlCommand.ExecuteNonQuery();
        }
    }

    private bool MetadataExists(decimal licenceId, string key)
    {
        const string sql = @"SELECT count(1)
                FROM METADATA
                WHERE LICENCEID = @LICENCEID AND METADATAKEY = @METADATAKEY";

        string connectionString = ConfigurationManager.ConnectionStrings["connectionString"].ConnectionString;
        using (SqlConnection connection = new SqlConnection(connectionString))
        {
            connection.Open();

            SqlCommand sqlCommand = new SqlCommand(sql, connection);
            sqlCommand.Parameters.AddWithValue("@LICENCEID", licenceId);
            sqlCommand.Parameters.AddWithValue("@METADATAKEY", key);

            int count = (int)sqlCommand.ExecuteScalar();
            return count > 0;
        }
    }
}