﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <script src="scripts/jquery-1.4.4.min.js" type="text/javascript"></script>
    <script src="scripts/jquery.simplemodal.1.4.1.min.js" type="text/javascript"></script>
    <style type="text/css">
        body {
            font-family: arial;
            font-size: 75%;
        }
        .hidden
        {
            display: none;
        }
        #modal-overlay {background-color:#eee;}
        td.licence {
            
        }
    </style>
    <title>ALM Solutions License Manager</title>
</head>
<body>
    <div style="font-size: x-large;">
        <b>ALM Licence Manager</b></div>
    <br />
    <form id="form1" runat="server">
    <div>
        <asp:GridView ID="gridView" runat="server" AutoGenerateColumns="False" DataKeyNames="LICENCEID"
            DataSourceID="gridSqlDataSource" BackColor="White" BorderColor="#CC9966" BorderStyle="None"
            BorderWidth="1px" CellPadding="4" OnRowCommand="gridView_RowCommand" AllowSorting="True"
            OnSelectedIndexChanged="gridView_SelectedIndexChanged" 
            OnRowCreated="gridView_RowCreated" 
            OnRowDataBound="gridView_RowDataBound">
            <RowStyle BackColor="White" ForeColor="#330099" />
            <Columns>
                <asp:CommandField ShowSelectButton="True" SelectText="Edit" />
                <asp:TemplateField ShowHeader="False">
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Delete"
                            OnClientClick='return confirm("Are you sure you want to delete this entry?");'
                            Text="Delete" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:ButtonField CommandName="Reset" InsertVisible="False" Text="Reset" />
                <asp:BoundField DataField="LICENCEID" HeaderText="Id" ReadOnly="True" SortExpression="LICENCEID" />
                <asp:TemplateField HeaderText="Serial Number" SortExpression="NUMERO_SERIE">
                    <ItemTemplate>
                        <div style="width:12em">
                        <asp:Label runat="server" Text='<%# Eval("NUMERO_SERIE") %>'>></asp:Label>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="FIRSTNAME" HeaderText="First Name" SortExpression="FIRSTNAME"
                    ReadOnly="True" />
                <asp:BoundField DataField="LASTNAME" HeaderText="Last Name" SortExpression="LASTNAME"
                    ReadOnly="True" />
                <asp:BoundField DataField="REGION" HeaderText="Region" SortExpression="REGION"
                    ReadOnly="True" />
                <asp:BoundField DataField="COMPANY" HeaderText="Company" SortExpression="COMPANY"
                    ReadOnly="True" />
                <asp:BoundField DataField="PHONE" HeaderText="Phone" SortExpression="PHONE"
                    ReadOnly="True" />
                <asp:BoundField DataField="MAIL" HeaderText="Mail" SortExpression="MAIL"
                    ReadOnly="True" />
                <asp:BoundField DataField="USED" HeaderText="In use" SortExpression="USED" ReadOnly="True" />
                <asp:BoundField DataField="DEBUT" HeaderText="Validity Start" SortExpression="DEBUT"
                    DataFormatString="{0:yyyy/MM/dd}" />
                <asp:BoundField DataField="FIN" HeaderText="Validity End" SortExpression="FIN" DataFormatString="{0:yyyy/MM/dd}" />
                <asp:BoundField DataField="LDV" HeaderText="Last Downloaded Version" SortExpression="LDV"
                                    ReadOnly="True" />
                <asp:BoundField DataField="LDD" HeaderText="Last Download Date" SortExpression="LDD"
                                    ReadOnly="True" />
            </Columns>
            <FooterStyle BackColor="#FFFFCC" ForeColor="#330099" />
            <PagerStyle BackColor="#FFFFCC" ForeColor="#330099" HorizontalAlign="Center" />
            <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="#FFFFCC" />
        </asp:GridView>
        <div id="popup" class="hidden" style="border: thin solid #808080; padding: 5px; margin: 5px; background-color: #ffffff">
            <asp:DetailsView ID="detailsView" runat="server" AutoGenerateRows="False" DataKeyNames="LICENCEID"
                DataSourceID="detailsSqlDataSource" BackColor="White" BorderColor="White" BorderStyle="None"
                BorderWidth="1px" CellPadding="4" ForeColor="Black" GridLines="Vertical" DefaultMode="Edit"
                OnItemUpdated="detailsView_ItemUpdated" 
                OnItemCommand="detailsView_ItemCommand" 
                OnDataBound="detailsView_DataBound" >
                <AlternatingRowStyle BackColor="White" />
                <Fields>
                    <asp:BoundField DataField="FIRSTNAME" HeaderText="First Name" SortExpression="FIRSTNAME" />
                    <asp:BoundField DataField="LASTNAME" HeaderText="Last Name" SortExpression="LASTNAME" />                    
                    <asp:BoundField DataField="DEBUT" HeaderText="Validity Start (yyyy/mm/dd)" DataFormatString="{0:yyyy/MM/dd}"
                        ApplyFormatInEditMode="True" />
                    <asp:BoundField DataField="FIN" HeaderText="Validity End (yyyy/mm/dd)" DataFormatString="{0:yyyy/MM/dd}"
                        ApplyFormatInEditMode="True" />
                    <asp:BoundField DataField="MODULES" />
                    <asp:CommandField ShowEditButton="True" />
                </Fields>
                <FooterStyle BackColor="#CCCC99" />
                <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                <RowStyle BackColor="#F7F7DE" />
                <CommandRowStyle BackColor="#CCCC99" />
            </asp:DetailsView>
        </div>
        <asp:SqlDataSource ID="detailsSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:connectionString %>"
            SelectCommand="GetSGLicenceById" SelectCommandType="StoredProcedure" 
            UpdateCommand="UPDATE LICENCE SET FIN = Convert(datetime,@FIN,111), DEBUT = Convert(datetime,@DEBUT,111) WHERE LICENCEID=@LICENCEID">
            <SelectParameters>
                <asp:ControlParameter ControlID="gridView" Name="LICENCEID" PropertyName="SelectedValue"
                    Type="Decimal" />
            </SelectParameters>
        </asp:SqlDataSource>
        <br />
        <asp:Button ID="ButtonInsert" runat="server" Text="Add Licence" OnClick="ButtonInsert_Click" />
        <asp:SqlDataSource ID="gridSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:connectionString %>"
            SelectCommand="GetSGLicences" SelectCommandType="StoredProcedure" DeleteCommand="DELETE FROM METADATA WHERE LICENCEID=@LICENCEID; DELETE FROM LICENCE WHERE LICENCEID=@LICENCEID">
            <SelectParameters>
                <asp:Parameter Name="PRODUITID" Type="Decimal" />
                <asp:Parameter Name="CONTRATID" />
            </SelectParameters>
        </asp:SqlDataSource>
        <script type="text/javascript">
            if ($("#popup table tbody").length > 0) {

                $("#popup input[name='detailsView$ctl05']").parents("tr").hide();
                
                $("#popup").modal({ appendTo : 'form', overlayId: 'modal-overlay' });
            }
        </script>
    </div>
    </form>
</body>
</html>
