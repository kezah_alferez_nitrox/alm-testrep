﻿using System;
using System.Net;
using System.Web.Services;
using System.Web.UI;
using ALMSCommon;

public partial class CheckVersion : Page
{


    public string Version { get; set; }
    public string License { get; set; }
    public bool IsBeta { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.link.Visible = false;

        if (this.Request.QueryString["l"] == "true")
        {
            this.message.InnerHtml = "You don't have a valid licence, please contact us for getting one: <a href=\"http://www.alm-solutions.com\">www.alm-solutions.com</a>";
            return;
        }

		IsBeta = this.Request.QueryString["b"] == "true";

        string rootUrl = IsBeta ? Constants.BetaUpdateRoot : Constants.UpdateRoot;	
		string versionUrl = rootUrl + "version.php";
	
        string requestKey = this.Request.QueryString["k"];

        string key = DateTime.Today.ToString("yyyyMMdd");
        key = Crypt.EncryptStringAES(key, key);
        if (key != requestKey)
        {
            return;
        }

        string licenseKey = this.Request.QueryString["lk"];

        string requestVersion = this.Request.QueryString["v"];
        Version version;
        try
        {
            version = new Version(requestVersion);
        }
        catch (Exception)
        {
            return;
        }

        string since;
        Version availableVersion;

        this.GetAvailableVersion(versionUrl, out availableVersion,  out since);
        if (availableVersion != null && version < availableVersion)
        {
            this.message.InnerHtml = "A new version is available (" + availableVersion + ") since "+ since + ": ";
            this.link.HRef = string.Format("Download.aspx?v={0}&lk={1}&b={2}", availableVersion, licenseKey, IsBeta);
            this.Version = availableVersion.ToString();
            this.link.Visible = true;
        }
        else
        {
            this.message.InnerHtml = "No newer version available";
        }
    }

    private void GetAvailableVersion(string versionUrl, out Version version, out string since)
    {
        version = null;
        since = "";

        try
        {
            WebClient webClient = new WebClient();
            string downloadString = webClient.DownloadString(versionUrl);

            string[] strings = downloadString.Split('|');

            version = new Version(strings[0]);
            if (strings.Length > 1) since = strings[1];
        }
        catch (Exception ex)
        {
            message.InnerHtml = ex.ToString();
        }
    }
}