﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Web.UI;
using ALMSCommon;

public partial class Download : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        bool isBeta = string.Equals(this.Request.QueryString["b"], "true", StringComparison.InvariantCultureIgnoreCase);
        string version = this.Request.QueryString["v"];
        string licence = this.Request.QueryString["lk"];

        string rootUrl = isBeta ? Constants.BetaUpdateRoot : Constants.UpdateRoot;
        string getterUrl = rootUrl + "get.php?v=";

        if (!isBeta) UpdateLastDownload(licence, version);

        Response.Redirect(getterUrl + version);
    }

    private void UpdateLastDownload(string licence, string version)
    {
        if (string.IsNullOrEmpty(version) || string.IsNullOrEmpty(licence)) return;

        try
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ConnectionString))
            {
                connection.Open();

                const string sql = @"declare @id int;
                    SELECT @id = LICENCEID FROM LICENCE where NUMERO_SERIE = @licence;
                    DELETE FROM METADATA WHERE LICENCEID = @id and METADATAKEY = 'lastDownloadedVersion';
                    DELETE FROM METADATA WHERE LICENCEID = @id and METADATAKEY = 'lastDownloadedDate';
                    INSERT INTO METADATA(LICENCEID, METADATAKEY, METADATAVALUE) VALUES (@id, 'lastDownloadedVersion', @version);
                    INSERT INTO METADATA(LICENCEID, METADATAKEY, METADATAVALUE) VALUES (@id, 'lastDownloadedDate', @date);";

                SqlCommand command = new SqlCommand(sql, connection);
                command.Parameters.Add(new SqlParameter("licence", licence));
                command.Parameters.Add(new SqlParameter("version", version));
                command.Parameters.Add(new SqlParameter("date", DateTime.Now.ToString("yyyy/MM/dd HH:mm")));
                command.ExecuteNonQuery();
            }
        }
        catch (Exception ex)
        {
            Log(ex);
        }
    }

    private void Log(Exception exception)
    {
        this.Context.Trace.Write(exception.ToString());
    }
}