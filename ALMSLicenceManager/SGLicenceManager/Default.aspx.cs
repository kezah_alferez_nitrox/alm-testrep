﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using ALMSCommon;

// ReSharper disable InconsistentNaming

public partial class _Default : Page
{
    private const int firstModuleColumnIndex = 14;
    private const int firstModuleDetailsIndex = 5;

    private static readonly Tuple<int, string>[] ModuleNames = new[]
        {
            new Tuple<int, string>((int)AlmConfiguration.Module.BankALM, "Bank ALM"),
            new Tuple<int, string>((int)AlmConfiguration.Module.LifeInsurance, "Life Insurance"),
            new Tuple<int, string>((int)AlmConfiguration.Module.PensionFund, "Pension Fund"),
            new Tuple<int, string>((int)AlmConfiguration.Module.TreasuryManagement, "Treasury Management"),
            new Tuple<int, string>((int)AlmConfiguration.Interface.ISODEV, "ISODEV"),
            new Tuple<int, string>((int)AlmConfiguration.Interface.CA_NDF, "CA NDF"),
        };

    private int contratId;
    private int produitId;

    protected void Page_Load(object sender, EventArgs e)
    {
        this.produitId = int.Parse(ConfigurationManager.AppSettings["PRODUITID"]);
        this.contratId = int.Parse(ConfigurationManager.AppSettings["CONTRATID"]);

        this.gridSqlDataSource.SelectParameters["PRODUITID"].DefaultValue = this.produitId.ToString();
        this.gridSqlDataSource.SelectParameters["CONTRATID"].DefaultValue = this.contratId.ToString();
    }

    protected override void OnPreInit(EventArgs e)
    {
        base.OnPreInit(e);

        this.AddModuleFields();
    }

    private void AddModuleFields()
    {
        for (int i = 0; i < ModuleNames.Length; i++)
        {
            BoundField gridField = new CheckBoxField();
            gridField.HeaderText = ModuleNames[i].Item2;
            this.gridView.Columns.Insert(firstModuleColumnIndex + i, gridField);

            CheckBoxField detailsField = new CheckBoxField();
            detailsField.HeaderText = ModuleNames[i].Item2;
            this.detailsView.Fields.Insert(firstModuleDetailsIndex + i, detailsField);
        }
    }

    protected void gridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Reset")
        {
            int index = Convert.ToInt32(e.CommandArgument);
            GridViewRow row = this.gridView.Rows[index];

            int licenceid = Convert.ToInt32(row.Cells[3].Text);

            try
            {
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ConnectionString))
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand("UPDATE LICENCE SET MACHINE_ID=NULL WHERE LICENCEID=" + licenceid + "; " +
                        "DELETE FROM METADATA WHERE LICENCEID=" + licenceid, connection);
                    command.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                this.Log(ex);
            }

            this.Server.Transfer(this.Request.Path);
        }
    }

    private void Log(Exception exception)
    {
        this.Context.Trace.Write(exception.ToString());
    }

    protected void ButtonInsert_Click(object sender, EventArgs e)
    {
        try
        {
            using (SqlConnection connection = new SqlConnection(GetConnectionString()))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(
                    "INSERT INTO LICENCE (PRODUITID, CONTRATID) VALUES(" + this.produitId + "," + this.contratId + "); " +
                    " DECLARE @id int; SET @id=@@Identity;" +
                    " INSERT INTO METADATA(LICENCEID, METADATAKEY, METADATAVALUE) VALUES(@id, 'Modules', '');" +
                    " INSERT INTO METADATA(LICENCEID, METADATAKEY, METADATAVALUE) VALUES(@id, 'firstName', '');" +
                    " INSERT INTO METADATA(LICENCEID, METADATAKEY, METADATAVALUE) VALUES(@id, 'lastName', '');",
                    connection);
                command.ExecuteNonQuery();
            }

            this.gridView.DataBind();
            this.gridView.SelectedIndex = this.gridView.Rows.Count - 1;
        }
        catch (Exception ex)
        {
            this.Log(ex);
        }
    }

    private static string GetConnectionString()
    {
        return ConfigurationManager.ConnectionStrings["connectionString"].ConnectionString;
    }

    protected void detailsView_ItemUpdated(object sender, DetailsViewUpdatedEventArgs e)
    {
        if (e.Exception == null)
        {
            if (e.AffectedRows <= 0) return;
            int licenceid = Convert.ToInt32(e.Keys["LICENCEID"]);

            var modulesValue = GetModulesValues(sender, e);

            try
            {
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ConnectionString))
                {
                    connection.Open();
                    string sql =
                        "IF NOT EXISTS (SELECT * FROM METADATA where LICENCEID=" + licenceid + " and METADATAKEY='Modules') " +
                        " INSERT INTO METADATA(LICENCEID, METADATAKEY, METADATAVALUE) VALUES(" + licenceid + ",'Modules','');" +

                        "IF NOT EXISTS (SELECT * FROM METADATA where LICENCEID=" + licenceid + " and METADATAKEY='firstName') " +
                        " INSERT INTO METADATA(LICENCEID, METADATAKEY, METADATAVALUE) VALUES(" + licenceid + ",'firstName','');" +

                        "IF NOT EXISTS (SELECT * FROM METADATA where LICENCEID=" + licenceid + " and METADATAKEY='lastName') " +
                        " INSERT INTO METADATA(LICENCEID, METADATAKEY, METADATAVALUE) VALUES(" + licenceid + ",'lastName','');" +

                        " UPDATE METADATA SET METADATAVALUE='" + modulesValue + "' where METADATAKEY='Modules' And LICENCEID=" + licenceid + ";" +
                        " UPDATE METADATA SET METADATAVALUE='" + e.NewValues["FIRSTNAME"] + "' where METADATAKEY='firstName' And LICENCEID=" + licenceid + ";" +
                        " UPDATE METADATA SET METADATAVALUE='" + e.NewValues["LASTNAME"] + "' where METADATAKEY='lastName' And LICENCEID=" + licenceid + ";";
                    SqlCommand command = new SqlCommand(
                        sql,
                        connection);
                    command.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                this.Log(ex);
            }

            this.gridView.SelectedIndex = -1;
            this.gridView.DataBind();

            this.ButtonInsert.Enabled = true;
        }
        else
        {
            e.ExceptionHandled = true;
            e.KeepInEditMode = true;
        }
    }

    private static string GetModulesValues(object sender, DetailsViewUpdatedEventArgs e)
    {
        DetailsView view = (DetailsView) sender;

        var checkBoxes = GetViewControls<CheckBox>(view);

        string modulesValue = "";
        for (int i = 0; i < checkBoxes.Count; i++)
        {
            if (checkBoxes[i].Checked)
            {
                modulesValue += ModuleNames[i].Item1 + ";";
            }
        }

        return modulesValue;
    }

    private static List<T> GetViewControls<T>(DetailsView view)
    {
        List<T> checkBoxes = new List<T>();

        Table table = (Table) (view.Controls[0]);
        for (int i = 0; i < table.Rows.Count; i++)
        {
            TableRow tableRow = table.Rows[i];
            for (int j = 0; j < tableRow.Cells.Count; j++)
            {
                TableCell tableCell = tableRow.Cells[j];

                IEnumerable<T> controls = tableCell.Controls.Cast<Control>().Where(c => c is T).Cast<T>();
                checkBoxes.AddRange(controls);
            }
        }
        return checkBoxes;
    }

    protected void detailsView_ItemCommand(object sender, DetailsViewCommandEventArgs e)
    {
        this.gridView.SelectedIndex = -1;
        this.ButtonInsert.Enabled = true;
    }

    protected void gridView_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (this.gridView.SelectedIndex >= 0)
        {
            this.ButtonInsert.Enabled = false;
        }
    }

    protected void detailsView_DataBound(object sender, EventArgs e)
    {
        DetailsView view = sender as DetailsView;

        List<TextBox> textBoxes = GetViewControls<TextBox>(view);
        if (textBoxes.Count < 5) return;

        string modulesString = textBoxes[4].Text;
        string[] modules = modulesString.Split(';');

        List<CheckBox> checkBoxes = GetViewControls<CheckBox>(view);

        for (int i = 0; i < checkBoxes.Count; i++)
        {
            checkBoxes[i].Checked = modules.Any(m => m == ModuleNames[i].Item1.ToString());
        }
    }

    protected void gridView_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType != DataControlRowType.DataRow) return;

        for (int i = 0; i < ModuleNames.Length; i++)
        {
            TableCell tableCell = e.Row.Cells[firstModuleColumnIndex + i];
            tableCell.Controls.Add(new CheckBox {Enabled = false});
        }
    }

    protected void gridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType != DataControlRowType.DataRow) return;

        DataRowView dataRowView = e.Row.DataItem as DataRowView;
        if (dataRowView == null) return;

        string modulesString = Convert.ToString(dataRowView["Modules"]);
        string[] modules = modulesString.Split(';');

        for (int i = 0; i < ModuleNames.Length; i++)
        {
            TableCell tableCell = e.Row.Cells[firstModuleColumnIndex + i];
            CheckBox checkBox = tableCell.Controls[0] as CheckBox;
            if (checkBox != null)
            {
                checkBox.Checked = modules.Any(m => m == ModuleNames[i].Item1.ToString());
            }
        }
    }
}