﻿using System;
using System.Security.Cryptography;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Login : Page
{
    private const string Salt = "$^A\"v&%1";

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void Login1_Authenticate(object sender, AuthenticateEventArgs e)
    {
        string hashedPassword = HashString(Salt + this.LoginControl.Password);

        e.Authenticated = (this.LoginControl.UserName == "admin" && hashedPassword == "c6062c7c85bd337e9c47336f3f522a34");
    }

    private static string HashString(string value)
    {
        MD5CryptoServiceProvider x = new MD5CryptoServiceProvider();
        byte[] data = Encoding.ASCII.GetBytes(value);
        data = x.ComputeHash(data);
        string ret = "";
        for (int i = 0; i < data.Length; i++)
            ret += data[i].ToString("x2").ToLower();
        return ret;
    }
}