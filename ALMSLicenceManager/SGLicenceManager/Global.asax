﻿<%@ Application Language="C#" %>

<script runat="server">

    protected void Application_BeginRequest(Object sender, EventArgs e)
    {
#if !DEBUG
        if (!Request.IsSecureConnection)
        {
            string path = string.Format("https{0}", Request.Url.AbsoluteUri.Substring(4));

            Response.Redirect(path);
        }
#endif        
    }
       
</script>
