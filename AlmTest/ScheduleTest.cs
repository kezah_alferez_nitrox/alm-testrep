﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ALMSolutions;


namespace AlmTest
{
    /// <summary>
    /// Summary description for ScheduleTest
    /// </summary>
    [TestClass]
    public class ScheduleTest
    {
        /*
        public ScheduleTest()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void AddDate()
        {
            TenorObj tenor = new TenorObj(Tenor.M, 1);
            int step = 3;
            DateTime start = new DateTime(2015, 10, 15);
            DateTime fy = new DateTime(2016, 3, 31);
            DateTime end = new DateTime(2017, 11, 15);


            do
            {
                DateTime stepDate = Schedule.nextStep(start, end, step, fy);
                DateTime nextdate = Schedule.addDate(start, tenor, stepDate, Rule.EOM);
                Console.WriteLine(start + " " + nextdate + " " + Schedule.prevStep(stepDate, end, fy, step) + " " + stepDate);

                if (fy <= nextdate)
                {
                    fy = fy.AddYears(1);
                }
                start = nextdate;
            } while (start < end);
        }

        */
    }
}
