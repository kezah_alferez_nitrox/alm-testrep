using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using Eu.AlterSystems.ASNetLib.Core;
using NHibernate.Mapping;
using log4net;
using log4net.Core;

namespace ALMSCommon
{
    public class Logger
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(Logger));

        public static void Exception(string message, Exception ex)
        {
            Debug.WriteLine("-------------------------- Logger ---------------------------");
            Debug.WriteLine(message + ex);
            log.Error(message, ex);
        }

        public static void Error(string message)
        {
            Debug.WriteLine("-------------------------- Logger ---------------------------");
            Debug.WriteLine(message);
            log.Error(message);
        }

        public static void Warn(string message)
        {
            Debug.WriteLine("-------------------------- Logger ---------------------------");
            Debug.WriteLine(message);
            log.Warn(message);
        }

        public static void Warn(Exception ex)
        {
            Debug.WriteLine("-------------------------- Logger ---------------------------");
            Debug.WriteLine(ex);
            log.Warn(ex);
        }

        public static void Info(string message)
        {
            Debug.WriteLine("-------------------------- Logger ---------------------------");
            Debug.WriteLine(message);
            log.Info(message);
        }

        public static void Info(string[] values)
        {            
            string message="";
            foreach (string value in values)
            {
                message += value + ";";
            }
            Info(message);
        }
        
        public static void SetLoggerLevel(Level level)
        {
            ((log4net.Repository.Hierarchy.Logger)log.Logger).Level = level;
        }

        

        public static IList GetLevels()
        {
            return new ArrayList();
        }

    }
}