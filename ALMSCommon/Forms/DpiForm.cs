﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ALMSCommon.Forms
{
    public class DpiForm : Form
    {
        public DpiForm()
        {
            this.Load += LoadDpiForm;
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
        }

        protected virtual void LoadDpiForm(object sender, EventArgs e)
        {
            if (this.Controls.OfType<Control>().Count() == 0) return;
            int maxHeight = this.Controls.OfType<Control>().Max(c => c.Top + c.Height);
            int margin = this.Controls.OfType<Control>().Min(c => c.Top);
            this.Height = maxHeight + margin + SystemInformation.CaptionHeight;
        }
    }
}
