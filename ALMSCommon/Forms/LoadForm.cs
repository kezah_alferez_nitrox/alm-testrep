using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using ALMSCommon.Resources.Language;

namespace ALMSCommon.Forms
{
    public partial class LoadForm : Form
    {
        private static LoadForm instance;

        private TimeSpan? estimatedRemainingTime;
        private TimeSpan? lastWorkDuration;

        private int workDurationInSeconds;
        private DateTime workStartTime;
        private DateTime? lastProgressUpdateTime;

        public int WorkDurationInSeconds
        {
            get { return this.workDurationInSeconds; }
            set { this.workDurationInSeconds = value; }
        }

        public static LoadForm Instance
        {
            get { return instance ?? (instance = new LoadForm()); }
        }

        public LoadForm()
        {
            this.InitializeComponent();

            int maxHeight = this.Controls.OfType<Control>().Max(c => c.Top + c.Height);
            int margin = this.Controls.OfType<Control>().Min(c => c.Top);
            this.Height = maxHeight + margin + SystemInformation.CaptionHeight + 20;
        }

        public class WorkFinishedEventArgs : EventArgs
        {
            public string Name { get; set; }
            public TimeSpan Duration { get; set; }
        }

        public event EventHandler<WorkFinishedEventArgs> WorkFinished;

        private void OnWorkFinished()
        {
            WorkFinishedEventArgs e = new WorkFinishedEventArgs();
            e.Name = this.Text;
            e.Duration = DateTime.Now - workStartTime;

            EventHandler<WorkFinishedEventArgs> handler = this.WorkFinished;
            if (handler != null) handler(this, e);
        }

        public void DoWork(WorkParameters parameters)
        {
            this.DoWork(parameters, null);
        }

        public void SetTitle(string text)
        {
            /*if (this.InvokeRequired)
            {
                this.Invoke(new Action(() => this.Text = text));
            }
            else
            {*/
            try
            {
                this.Text = text;
            }
            catch (Exception)
            {
                
               
            }
            //this.Text = text;
          /*  }*/
        }

        public void DoWork(WorkParameters parameters, TimeSpan? lastDuration)
        {
            int cycles = 0;
            while (backgroundWorker.IsBusy && cycles < 3)
            {
                Thread.Sleep(5000);
                cycles++;
            }
            if (backgroundWorker.IsBusy)
            {
                Dialogs.Error("Application is busy right now", "Please try again in a few minutes or contact support if this occured several times.");
                return;
            }
            this.Text = Labels.LoadForm_DoWork_Please_wait___;
            this.workDurationInSeconds = 0;
            this.estimatedRemainingTime = lastDuration;
            this.lastWorkDuration = lastDuration;
            if (this.estimatedRemainingTime != null)
            {
                this.workDurationInSeconds = this.workDurationInSeconds++;
            }
            this.estimationTimer.Start();
            this.backgroundWorker.RunWorkerAsync(parameters);
            this.progressBar.Value = 0;
            this.lblTime.Text = "";

            backgroundWorker.WorkerSupportsCancellation = parameters.IsCancelable;
            cancelButton.Enabled = parameters.IsCancelable;

            this.ShowDialog();
        }

        private void BackgroundWorkerDoWork(object sender, DoWorkEventArgs e)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");

            this.workStartTime = DateTime.Now;

            e.Result = e.Argument;

            WorkParameters workParameters = (WorkParameters)e.Argument;
            try
            {
                workParameters.Work(this.backgroundWorker);
            }
            catch (Exception ex)
            {
                Logger.Exception("LoadForm.BackgroundWorkerDoWork", ex);

                workParameters.Exception = ex;
            } 
        }
        public void ReinitTimer()
        { 
            this.workStartTime = DateTime.Now; 
        }

        public void BackgroundWorkerProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (this.estimatedRemainingTime != null && this.lastWorkDuration != null &&
                (((TimeSpan)this.lastWorkDuration).TotalSeconds > 0))
            {
                //linear time progression
                this.progressBar.Value =
                    ((int)(100 - 100 *
                           (((TimeSpan)this.estimatedRemainingTime).TotalSeconds /
                            ((TimeSpan)this.lastWorkDuration).TotalSeconds)));
            }
            else
            {
                //worker based progression
                this.lastProgressUpdateTime = DateTime.Now;
                this.progressBar.Value = e.ProgressPercentage;
            }
            }

        private void BackgroundWorkerRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.Close();
            this.estimationTimer.Stop();

            this.OnWorkFinished();

            WorkParameters workParameters = (WorkParameters) e.Result;
            WorkCompletedCallback completed = workParameters.Completed;
            if (completed != null)
            {
                completed(workParameters.Exception);
            }
        }

        private void EstimationTimerTick(object sender, EventArgs e)
        {
            if (this.estimatedRemainingTime != null && (((TimeSpan)this.estimatedRemainingTime).TotalSeconds > 0))
            {
                this.estimatedRemainingTime = ((TimeSpan)this.estimatedRemainingTime).Subtract(new TimeSpan(0, 0, 1));
                this.lblTime.Text = Labels.LoadForm_EstimationTimerTick_Remaining_time___ +
                                    FormatTimeSpan(this.estimatedRemainingTime.GetValueOrDefault());
                this.BackgroundWorkerProgressChanged(this, null);
            }
            else
            {
                TimeSpan remainingTime = EstimateRemainingTimeByPercentage(this.workStartTime, this.lastProgressUpdateTime ?? this.workStartTime, this.progressBar.Value);
                this.lblTime.Text = Labels.LoadForm_EstimationTimerTick_Remaining_time___ + FormatTimeSpan(remainingTime);
            }
            this.workDurationInSeconds = this.workDurationInSeconds + 1;
        }

        private static TimeSpan EstimateRemainingTimeByPercentage(DateTime startTime, DateTime lastUpdate, int percentageCompleted)
        {
            if (percentageCompleted >= 100) return new TimeSpan(0);

            if (percentageCompleted < 1) return TimeSpan.MaxValue;

            long elapsed = (lastUpdate - startTime).Ticks;

            long remaining = (100 - percentageCompleted) * elapsed / percentageCompleted - (DateTime.Now - lastUpdate).Ticks;

            return new TimeSpan(remaining);
        }

        private static string FormatTimeSpan(TimeSpan timeSpan)
        {
            if (timeSpan == TimeSpan.MaxValue)
            {
                return "Estimation pending..."; // todo labels
        }

            if (timeSpan.TotalSeconds < 1)
            {
                return Labels.LoadForm_FormatTimeSpan_a_few_seconds;
            }

            return string.Format(Labels.LoadForm_FormatTimeSpan__0__minutes__1__seconds,
                                 (int)Math.Truncate(timeSpan.TotalMinutes), timeSpan.Seconds);
        }

        private void CancelButtonClick(object sender, EventArgs e)
        {
            backgroundWorker.CancelAsync();
            cancelButton.Enabled = false;
        }
    }
}