using System;
using System.ComponentModel;

namespace ALMSCommon.Forms
{
    public delegate void WorkCallback(BackgroundWorker backgroundWorker);

    public delegate void WorkCompletedCallback(Exception exception);

    public class WorkParameters
    {
        private readonly WorkCallback work;
        private readonly WorkCompletedCallback callback;
        private readonly bool isCancelable;

        public WorkParameters(WorkCallback work, WorkCompletedCallback completed) : this(work, completed, false)
        {            
        }

        public WorkParameters(WorkCallback work, WorkCompletedCallback completed, bool isCancelable)
        {
            this.callback = completed;
            this.isCancelable = isCancelable;
            this.work = work;
        }

        public WorkCompletedCallback Completed
        {
            get { return this.callback; }
        }

        public WorkCallback Work
        {
            get { return this.work; }
        }

        public bool IsCancelable
        {
            get { return this.isCancelable; }
        }

        public Exception Exception { get; set; }
    }
}