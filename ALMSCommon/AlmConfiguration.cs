using System;
using System.Collections.Generic;
using ALMSCommon.Resources.Language;
namespace ALMSCommon
{
    public class AlmConfiguration
    {
        public enum Module
        {
            BankALM = 1,
            LifeInsurance = 2,
            PensionFund = 3,
            TreasuryManagement = 4
        }

        public enum Interface //il ne faut pas un module et une interface avec le m�me num�ro
        {
            ISODEV = 9000,
            CA_NDF = 9001,
        }

        public static readonly string[] ModuleNames = new[]
            {
                Labels.SGConfiguration_ModuleNames_Bank_ALM,
                Labels.SGConfiguration_ModuleNames_Life_Insurance,
                Labels.SGConfiguration_ModuleNames_Pension_Fund,
                "Treasury Management"
            };

        private static Module currentModule;
        public static Module CurrentModule
        {
            get { return currentModule; }
            set { currentModule = value; }
        }


        public static string GetModuleName(Module module)
        {
            if (((int)module)>0 && ((((int)module) - 1)<ModuleNames.Length))
                return ModuleNames[((int)module) - 1];
            else
                return "";
        }

        public static bool IsPensionFundModule()
        {
            return CurrentModule != null && CurrentModule == Module.PensionFund;
        }

        public static bool IsBankALMModule()
        {
            return CurrentModule == 0 || CurrentModule == Module.BankALM;
        }

        public static bool IsInsuranceALMModule()
        {
            return CurrentModule == 0 || CurrentModule == Module.LifeInsurance;
        }

        public static string GetWorkspaceExtensionForCUrrentModule()
        {
            switch (AlmConfiguration.CurrentModule)
            {
                case AlmConfiguration.Module.BankALM:
                case 0:
                    return "alw";
                    break;
                case AlmConfiguration.Module.PensionFund:
                    return "plw";
                    break;
                case AlmConfiguration.Module.LifeInsurance:
                    return "ilw";
                    break;
                case AlmConfiguration.Module.TreasuryManagement:
                    return "tlw";
                    break;
                default:
                    throw new Exception("Incorrect module loaded.");
            }
            return null;

        }

        public static bool IsTreasuryManagementModule()
        {
            return CurrentModule != null && CurrentModule == Module.TreasuryManagement;
        }

        public static bool IsComputationDaily()
        {
            return IsTreasuryManagementModule();
        }
    }
}