using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml.Serialization;

namespace ALMSCommon
{
    [XmlRoot("li")]
    public class LicenceInfo : ILicenceInfo
    {
        public const string Pass = "azToi)��'M:;')";

        [XmlIgnore]
        public decimal LicenceId { get; set; }

        [XmlAttribute("n")]
        public string LicenceNumber { get; set; }
        [XmlAttribute("m")]
        public string MachineId { get; set; }
        [XmlAttribute("s")]
        public DateTime Start { get; set; }
        [XmlAttribute("e")]
        public DateTime End { get; set; }

        [XmlElement("md")]
        public SerializableDictionary<string, string> Metadata { get; set; }

        [XmlIgnore]
        public string FirstName
        {
            get { return this.Metadata.ContainsKey("FirstName") ? this.Metadata["FirstName"] : null; }
            set { this.Metadata["FirstName"] = value; }
        }

        [XmlIgnore]
        public string LastName
        {
            get { return this.Metadata.ContainsKey("LastName") ? this.Metadata["LastName"] : null; }
            set { this.Metadata["LastName"] = value; }
        }

        [XmlIgnore]
        public string Company
        {
            get { return this.Metadata.ContainsKey("Company") ? this.Metadata["Company"] : null; }
            set { this.Metadata["Company"] = value; }
        }

        [XmlIgnore]
        public string PhoneNumber
        {
            get { return this.Metadata.ContainsKey("PhoneNumber") ? this.Metadata["PhoneNumber"] : null; }
            set { this.Metadata["PhoneNumber"] = value; }
        }

        [XmlIgnore]
        public string Email
        {
            get { return this.Metadata.ContainsKey("Email") ? this.Metadata["Email"] : null; }
            set { this.Metadata["Email"] = value; }
        }

        public int[] Interfaces
        {
            get
            {
                List<int> result=new List<int>();
                int inter;
                foreach (string module in Modules.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    inter = int.Parse(module);
                    if (inter >= 9000) result.Add(inter);
                }
                return result.ToArray();
            }
        }

        [XmlIgnore]
        public string Modules
        {
            get { return this.Metadata.ContainsKey("Modules") ? this.Metadata["Modules"] : null; }
            set { this.Metadata["Modules"] = value; }
        }

        public LicenceInfo()
        {
            this.Start = this.End = DateTime.MaxValue;
            this.Metadata = new SerializableDictionary<string, string>();
        }

        public string Serialize()
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(LicenceInfo));

            using (MemoryStream ms = new MemoryStream())
            {
                xmlSerializer.Serialize(ms, this);
                byte[] compressed = CompressionTools.CompressBytes(ms.ToArray());

                return Crypt.EncryptAES(compressed, Pass);
            }
        }

        public static LicenceInfo Deserialize(string serialized)
        {
            if (string.IsNullOrEmpty(serialized)) return null;

            XmlSerializer xmlSerializer = new XmlSerializer(typeof(LicenceInfo));

            try
            {
                byte[] decrypted = Crypt.DecryptAES(serialized, Pass);
                string decompressed = CompressionTools.Decompress(decrypted);

                using (MemoryStream ms = new MemoryStream(Encoding.Default.GetBytes(decompressed)))
                {
                    return (LicenceInfo)xmlSerializer.Deserialize(ms);
                }
            }
            catch (Exception ex)
            {
                Logger.Exception("LicenceInfo Deserialize", ex);
            }

            return null;
        }

        public static ILicenceInfo Load(string fileName)
        {
            try
            {
                string allText = File.ReadAllText(fileName);
                return Deserialize(allText);
            }
            catch (Exception ex)
            {
                Logger.Exception("LicenceInfo Load", ex);
                return null;
            }
        }

        public void Save(string fileName)
        {
            string serialized = this.Serialize();
            File.WriteAllText(fileName, serialized);
        }

        [XmlRoot("p")]
        public class Pair
        {
            public Pair() { }

            public Pair(KeyValuePair<string, string> pair)
            {
                this.Key = pair.Key;
                this.Value = pair.Value;
            }

            [XmlAttribute("k")]
            public string Key { set; get; }
            [XmlAttribute("v")]
            public string Value { set; get; }
        }

        public bool IsModuleEnabled(int moduleNumber)
        {
            return this.Modules != null && this.Modules.Contains(moduleNumber.ToString());
        }
    }
}