using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;

namespace ALMSCommon
{
    public class Mapi
    {
        public bool AddRecipientTo(string email)
        {
            return this.AddRecipient(email, HowTo.MAPI_TO);
        }

        public bool AddRecipientCc(string email)
        {
            return this.AddRecipient(email, HowTo.MAPI_TO);
        }

        public bool AddRecipientBcc(string email)
        {
            return this.AddRecipient(email, HowTo.MAPI_TO);
        }

        public void AddAttachment(string strAttachmentFileName)
        {
            this.attachments.Add(strAttachmentFileName);
        }

        public int SendMailPopup(string strSubject, string strBody)
        {
            return this.SendMail(strSubject, strBody, MAPI_LOGON_UI | MAPI_DIALOG);
        }

        public int SendMailDirect(string strSubject, string strBody)
        {
            return this.SendMail(strSubject, strBody, MAPI_LOGON_UI);
        }


        [DllImport("MAPI32.DLL")]
        private static extern int MAPISendMail(IntPtr sess, IntPtr hwnd, MapiMessage message, int flg, int rsv);

        private int SendMail(string strSubject, string strBody, int how)
        {
            MapiMessage msg = new MapiMessage();
            msg.subject = strSubject;
            msg.noteText = strBody;

            msg.recips = this.GetRecipients(out msg.recipCount);
            msg.files = this.GetAttachments(out msg.fileCount);

            this.lastError = MAPISendMail(new IntPtr(0), new IntPtr(0), msg, how, 0);

            //if (m_lastError > 1)
            //    MessageBox.Show("MAPISendMail failed! " + GetLastError(), "MAPISendMail");

            this.Cleanup(ref msg);
            return this.lastError;
        }

        private bool AddRecipient(string email, HowTo howTo)
        {
            MapiRecipDesc recipient = new MapiRecipDesc();

            recipient.recipClass = (int) howTo;
            recipient.name = email;
            this.recipients.Add(recipient);

            return true;
        }

        private IntPtr GetRecipients(out int recipCount)
        {
            recipCount = 0;
            if (this.recipients.Count == 0)
            {
                return IntPtr.Zero;
            }

            int size = Marshal.SizeOf(typeof (MapiRecipDesc));
            IntPtr intPtr = Marshal.AllocHGlobal(this.recipients.Count*size);

            int ptr = (int) intPtr;
            foreach (MapiRecipDesc mapiDesc in this.recipients)
            {
                Marshal.StructureToPtr(mapiDesc, (IntPtr) ptr, false);
                ptr += size;
            }

            recipCount = this.recipients.Count;
            return intPtr;
        }

        private IntPtr GetAttachments(out int fileCount)
        {
            fileCount = 0;
            if (this.attachments == null)
            {
                return IntPtr.Zero;
            }

            if ((this.attachments.Count <= 0) || (this.attachments.Count > maxAttachments))
            {
                return IntPtr.Zero;
            }

            int size = Marshal.SizeOf(typeof (MapiFileDesc));
            IntPtr intPtr = Marshal.AllocHGlobal(this.attachments.Count*size);

            MapiFileDesc mapiFileDesc = new MapiFileDesc();
            mapiFileDesc.position = -1;
            int ptr = (int) intPtr;

            foreach (string strAttachment in this.attachments)
            {
                mapiFileDesc.name = Path.GetFileName(strAttachment);
                mapiFileDesc.path = strAttachment;
                Marshal.StructureToPtr(mapiFileDesc, (IntPtr) ptr, false);
                ptr += size;
            }

            fileCount = this.attachments.Count;
            return intPtr;
        }

        private void Cleanup(ref MapiMessage msg)
        {
            int size = Marshal.SizeOf(typeof (MapiRecipDesc));
            int ptr = 0;

            if (msg.recips != IntPtr.Zero)
            {
                ptr = (int) msg.recips;
                for (int i = 0; i < msg.recipCount; i++)
                {
                    Marshal.DestroyStructure((IntPtr) ptr, typeof (MapiRecipDesc));
                    ptr += size;
                }
                Marshal.FreeHGlobal(msg.recips);
            }

            if (msg.files != IntPtr.Zero)
            {
                size = Marshal.SizeOf(typeof (MapiFileDesc));

                ptr = (int) msg.files;
                for (int i = 0; i < msg.fileCount; i++)
                {
                    Marshal.DestroyStructure((IntPtr) ptr, typeof (MapiFileDesc));
                    ptr += size;
                }
                Marshal.FreeHGlobal(msg.files);
            }

            this.recipients.Clear();
            this.attachments.Clear();
            this.lastError = 0;
        }

        public string GetLastError()
        {
            if (this.lastError <= 26)
            {
                return this.errors[this.lastError];
            }
            return "MAPI error [" + this.lastError + "]";
        }

        private readonly string[] errors = new[]
                                           {
                                               "OK [0]", "User abort [1]", "General MAPI failure [2]",
                                               "MAPI login failure [3]",
                                               "Disk full [4]", "Insufficient memory [5]", "Access denied [6]",
                                               "-unknown- [7]",
                                               "Too many sessions [8]", "Too many files were specified [9]",
                                               "Too many recipients were specified [10]",
                                               "A specified attachment was not found [11]",
                                               "Attachment open failure [12]", "Attachment write failure [13]",
                                               "Unknown recipient [14]", "Bad recipient type [15]",
                                               "No messages [16]", "Invalid message [17]", "Text too large [18]",
                                               "Invalid session [19]",
                                               "Type not supported [20]", "A recipient was specified ambiguously [21]",
                                               "Message in use [22]", "Network failure [23]",
                                               "Invalid edit fields [24]", "Invalid recipients [25]",
                                               "Not supported [26]"
                                           };


        private readonly List<MapiRecipDesc> recipients = new List<MapiRecipDesc>();
        private readonly List<string> attachments = new List<string>();
        private int lastError;

        private const int MAPI_LOGON_UI = 0x00000001;
        private const int MAPI_DIALOG = 0x00000008;
        private const int maxAttachments = 20;

        private enum HowTo
        {
            MAPI_ORIG = 0,
            MAPI_TO,
            MAPI_CC,
            MAPI_BCC
        } ;
    }

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public class MapiMessage
    {
        public int reserved;
        public string subject;
        public string noteText;
        public string messageType;
        public string dateReceived;
        public string conversationID;
        public int flags;
        public IntPtr originator;
        public int recipCount;
        public IntPtr recips;
        public int fileCount;
        public IntPtr files;
    }

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public class MapiFileDesc
    {
        public int reserved;
        public int flags;
        public int position;
        public string path;
        public string name;
        public IntPtr type;
    }

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public class MapiRecipDesc
    {
        public int reserved;
        public int recipClass;
        public string name;
        public string address;
        public int eIDSize;
        public IntPtr entryID;
    }
}