using System;
using System.Text;
using System.IO;
using System.Threading;
using ICSharpCode.SharpZipLib.Zip;

namespace ALMSCommon
{
    public class CompressionTools
    {
        private const string ENTRY_NAME = "data";
        private const int COMPRESSION_LEVEL = 5;
        private const int BUFFER_SIZE = 4096;

        public static byte[] CompressBytes(byte[] data)
        {
            using (MemoryStream memoryOut = new MemoryStream())
            using (ZipOutputStream zipOut = new ZipOutputStream(memoryOut))
            {
                zipOut.SetLevel(COMPRESSION_LEVEL); // Niveau de compression de 0 (faible) � 9 (fort)
                ZipEntry theEntry = new ZipEntry(ENTRY_NAME);
                zipOut.PutNextEntry(theEntry);
                zipOut.Write(data, 0, data.Length);

                zipOut.Finish();
                zipOut.Close();

                data = memoryOut.ToArray();
                memoryOut.Close();

                return data;
            }
        }

        public static byte[] UncompressBytes(byte[] data)
        {
            using (MemoryStream memoryIn = new MemoryStream(data))
            using (ZipInputStream zipIn = new ZipInputStream(memoryIn))
            using (MemoryStream memoryOut = new MemoryStream())
            {
                ZipEntry theEntry = zipIn.GetNextEntry();
                int IntSize = BUFFER_SIZE;

                byte[] buffer = new byte[BUFFER_SIZE];

                while (theEntry != null)
                {
                    IntSize = zipIn.Read(buffer, 0, IntSize);

                    if (IntSize > 0)
                    {
                        memoryOut.Write(buffer, 0, IntSize);
                    }
                    else
                    {
                        theEntry = zipIn.GetNextEntry();
                    }
                }

                data = memoryOut.ToArray();

                memoryOut.Close();
                zipIn.Close();
                memoryIn.Close();

                return data;
            }
        }

        public static byte[] Compress(string stringToCompress)
        {
            byte[] data = Encoding.Default.GetBytes(stringToCompress);
            return CompressBytes(data);
        }

        public static string Decompress(byte[] data)
        {
            data = UncompressBytes(data);

            return Encoding.Default.GetString(data, 0, data.Length);
        }

        public static void UncompressZip(string zipFile, string directory)
        {
            using (FileStream zipFielStream = File.OpenRead(zipFile))
            using (ZipInputStream zipIn = new ZipInputStream(zipFielStream))
            {
                ZipEntry entry;
                while ((entry = zipIn.GetNextEntry()) != null)
                {
                    if (entry.IsDirectory)
                    {
                        string dirName = Path.Combine(directory, entry.Name);
                        if (!Directory.Exists(dirName))
                        {
                            Directory.CreateDirectory(dirName);
                        }
                    }
                    else
                    {
                        using (FileStream streamWriter = File.Create(Path.Combine(directory, entry.Name)))
                        {
                            long size = entry.Size;
                            byte[] data = new byte[size];
                            while (true)
                            {
                                size = zipIn.Read(data, 0, data.Length);
                                if (size > 0) streamWriter.Write(data, 0, (int)size);
                                else break;
                            }
                            streamWriter.Close();
                        }
                    }
                }

                zipIn.Close();
                zipFielStream.Close();
            }
        }

        public static void CompressFolderToZip(string sourceFolder, string destinationFile)
        {
            ZipOutputStream zip = new ZipOutputStream(File.Create(destinationFile));
            zip.SetLevel(9);
            string folder = sourceFolder;
            ZipFolder(folder, folder, zip);
            zip.Finish();
            zip.Close();

        }



        private static void ZipFolder(string RootFolder, string CurrentFolder, ZipOutputStream zStream)
        {
            string[] SubFolders = Directory.GetDirectories(CurrentFolder);

            //calls the method recursively for each subfolder
            foreach (string Folder in SubFolders)
            {
                ZipFolder(RootFolder, Folder, zStream);
            }

            string relativePath = CurrentFolder.Substring(RootFolder.Length) + "/";

            //the path "/" is not added or a folder will be created
            //at the root of the file
            if (relativePath.Length > 1)
            {
                ZipEntry dirEntry;
                dirEntry = new ZipEntry(relativePath);
                dirEntry.DateTime = DateTime.Now;
            }

            //adds all the files in the folder to the zip
            int tries;
            bool done;
            foreach (string file in Directory.GetFiles(CurrentFolder))
            {
                tries = 0;
                done = false;
                while (tries < 3 && !done)
                {
                    try
                    {
                        AddFileToZip(zStream, relativePath, file);
                        done = true;
                    }
                    catch (Exception)
                    {
                        tries++;
                        Thread.Sleep(1000);
                    }
                }

            }
        }

        private static void AddFileToZip(ZipOutputStream zStream, string relativePath, string file)
        {
            byte[] buffer = new byte[4096];

            //the relative path is added to the file in order to place the file within
            //this directory in the zip
            string fileRelativePath = (relativePath.Length > 1 ? relativePath : string.Empty)
                                      + Path.GetFileName(file);

            ZipEntry entry = new ZipEntry(fileRelativePath);
            entry.DateTime = DateTime.Now;
            zStream.PutNextEntry(entry);

            using (FileStream fs = File.OpenRead(file))
            {
                int sourceBytes;
                do
                {
                    sourceBytes = fs.Read(buffer, 0, buffer.Length);
                    zStream.Write(buffer, 0, sourceBytes);
                } while (sourceBytes > 0);
            }
        }

    }
}