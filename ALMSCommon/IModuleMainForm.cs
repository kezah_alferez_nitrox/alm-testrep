﻿using System;

namespace ALMSCommon
{
    public interface IModuleMainForm
    {
        string[] Args { get; set; }
        void OpenFile(string filePath);
        bool IsForcedClose { set; }
        ILicenceInfo LicenceInfo { set; }
    }
}