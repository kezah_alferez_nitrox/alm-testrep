using System;
using System.IO;
using System.Linq;

namespace ALMSCommon
{
    public class FileHistory
    {
        private readonly string historyFolder = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) +
                                               "\\SOGALMS\\";

        private readonly string historyFilePath;

        public FileHistory(AlmConfiguration.Module module)
        {
            this.historyFilePath =  Path.Combine(historyFolder, AlmConfiguration.GetModuleName(module)) + " .fh";
        }

        public void PutFile(string filePath)
        {
            string[] lines = File.Exists(historyFilePath) ? File.ReadAllLines(this.historyFilePath) : new string[0];

            lines =
                new[] {filePath}.Union(
                    lines.Where(l =>
                        !string.IsNullOrEmpty(l) &&
                        string.Compare(l, filePath, StringComparison.InvariantCultureIgnoreCase) != 0))
                    .Take(20).ToArray();

            if (!Directory.Exists(historyFolder)) Directory.CreateDirectory(historyFolder);
            File.WriteAllLines(historyFilePath, lines);
        }

        public string[] GetFiles()
        {
            if (File.Exists(historyFilePath))
            {
                return File.ReadAllLines(this.historyFilePath);
            }

            return new string[0];
        }
    }
}