﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Threading;

namespace ALMSCommon
{
    public class ParsingTools
    {
        private static readonly CultureInfo[] AccepedCultures = new[]
                                                                    {
                                                                        new CultureInfo("fr-FR"),
                                                                        new CultureInfo("en-US"),
                                                                    };

        public static double? ClipboardStringToDouble(string str, bool usFirst = false)
        {
            if (String.IsNullOrEmpty(str)) return null;
            str = str.Replace(" ", "").Trim();
            str = str.Replace("\xA0", "");
            str = str.Replace(",", "");

            CultureInfo[] rightOrderAccepedCultures = (CultureInfo[])AccepedCultures.Clone();

            if (usFirst) Array.Reverse(rightOrderAccepedCultures);

            foreach (CultureInfo accepedCulture in rightOrderAccepedCultures)
            {
                double decimalValue;
                if (Double.TryParse(str, NumberStyles.Number, accepedCulture, out decimalValue))
                    return decimalValue;
            }

            return null;
        }



        public static string ChangeStringCulture(string str, CultureInfo culture)
        {
            if (str == null) return null;

            string result = str.Replace(" ", "").Trim();
            result = result.Replace("\xA0", "");

            bool isPercentage = false;
            if (result.EndsWith("%"))
            {
                isPercentage = true;
                result = result.Substring(0, result.Length - 1);
            }

            foreach (CultureInfo accepedCulture in AccepedCultures)
            {
                double doubleValue;
                if (Double.TryParse(result, NumberStyles.Number, accepedCulture, out doubleValue))
                {
                    return doubleValue.ToString(culture) + (isPercentage ? "%" : "");
                }
            }

            return str;
        }

        public static double? ClipBoardPercentStringToDouble(string str)
        {
            if (str == null || !str.EndsWith("%")) return null;
            else return ClipboardStringToDouble(str.TrimEnd('%', ' ')) / 100;
        }

        public static string ClipboardFormatString(string str, CultureInfo parseFormat)
        {
            double? value;
            bool percentage;
            value = ClipBoardPercentStringToDouble(str);
            percentage = value != null;
            if (!percentage) value = ClipboardStringToDouble(str);

            if (value != null) return value.Value.ToString(percentage ? "P" : "N", parseFormat.NumberFormat).Replace(" ", "").Replace("\xA0", "");
            else return str;
        }

        public static CultureInfo[] GetAcceptedCultures()
        {
            return AccepedCultures;
        }

        public static CultureInfo GetWindowsCulture()
        {
            Thread.CurrentThread.CurrentCulture.ClearCachedData();
            Thread thread = new Thread(s => { });
            return thread.CurrentCulture;
        }

        public static double? StringToDouble(string str)
        {
            if (String.IsNullOrEmpty(str)) return null;
            str = str.Replace(" ", "").Trim();
            str = str.Replace("\xA0", "");

            double factor = 1;
            if (str.EndsWith("%"))
            {
                str = str.Replace("%", "").Trim();
                factor = 0.01;
            }
            if (str.EndsWith("bp"))
            {
                str = str.Replace("bp", "").Trim();
                factor = 0.0001;
            }

            foreach (CultureInfo accepedCulture in AccepedCultures)
            {
                double decimalValue;
                if (Double.TryParse(str, NumberStyles.Any, accepedCulture, out decimalValue))
                {
                    return decimalValue * factor;
                }
            }

            return null;
        }
    }
}
