﻿using System;
using Microsoft.Office.Interop.Excel;
using Application = System.Windows.Forms.Application;
using Excel = Microsoft.Office.Interop.Excel.Application;

namespace ALMSCommon
{
    public static class ExcelClipboard
    {
        private static Excel excelInstance;

        static ExcelClipboard()
        {
            Application.ApplicationExit += OnApplicationExit;
        }

        private static void OnApplicationExit(object sender, EventArgs e)
        {
            try
            {
                if (excelInstance != null)
                {
                    excelInstance.Workbooks[1].Close(false);
                    excelInstance.Quit();
                }
            }
            catch (Exception ex)
            {
                Logger.Exception("", ex);
            }
        }

        public static void Copy(object[,] values)
        {
            if (values == null || values.GetLength(0) == 0) return;

            Excel excel = InitExcel();

            Worksheet worksheet = excel.Workbooks[1].Worksheets[1];

            Range range = worksheet.Range["A1", ExcelTools.GetExcelColumnName(values.GetLength(1)) + (values.GetLength(0))];
            range.Clear();
            range.Value2 = values;
            range.Copy();
        }

        private static Excel InitExcel()
        {
            if (excelInstance == null)
            {
                excelInstance = new Excel();
                excelInstance.Visible = false;
                excelInstance.DisplayAlerts = false;
                excelInstance.Workbooks.Add();
            }

            return excelInstance;
        }
    }
}