﻿using System.Drawing;

namespace ALMSCommon
{
    public class AlmsColors
    {
        public static readonly Color Window = Color.FromArgb(255, 255, 255);
        public static readonly Color Control = Color.FromArgb(240, 240, 240);
        public static readonly Color ControlText = Color.FromArgb(0, 0, 0);
        public static readonly Color ControlDark = Color.FromArgb(160, 160, 160);
        public static readonly Color ControlLight = Color.FromArgb(227, 227, 227);
        public static readonly Color ControlLightLight = Color.FromArgb(255, 255, 255);
    }
}