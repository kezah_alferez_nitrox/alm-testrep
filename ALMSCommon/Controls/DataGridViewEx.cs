using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ALMSCommon.Controls
{
    public class DataGridViewEx : DataGridView
    {
        private const float IMAGE_TRANSPARENCY = 0.20f;

        public DataGridViewEx()
        {
            // this.ResizeRedraw = true;
            this.DoubleBuffered = true;

            this.FontChanged += this.DataGridViewEx_FontChanged;
        }

        public void CreateCopyContextMenu()
        {
            ContextMenuStrip menu = new ContextMenuStrip();
            ToolStripMenuItem item = new ToolStripMenuItem("Copy");
            item.ShortcutKeys = Keys.C | Keys.Control;
            item.Click += this.ContextMenuClick;
            menu.Items.Add(item);
            this.ContextMenuStrip = menu;
        }

        private void ContextMenuClick(object sender, EventArgs e)
        {
            this.CopyToClipboardAsExcel();
        }

        //public override DataObject GetClipboardContent()
        //{
        //    this.Cursor = Cursors.WaitCursor;
        //    DataObject clipboardContent = base.GetClipboardContent();
        //    if (clipboardContent != null)
        //    {
        //        object[,] values = ParseClipboardContent(clipboardContent);
        //        ExcelClipboard.Copy(values);
        //        clipboardContent = (DataObject)Clipboard.GetDataObject();
        //    }
        //    this.Cursor = Cursors.Default;
        //    return clipboardContent;
        //}

        public void CopyToClipboardAsExcel()
        {
            this.Cursor = Cursors.WaitCursor;
            try
            {
                DataObject clipboardContent = base.GetClipboardContent();
                if (clipboardContent != null)
                {
                    object[,] values = ParseClipboardContent(clipboardContent);
                    ExcelClipboard.Copy(values);
                }
            }
            catch (Exception ex)
            {
                Logger.Exception("", ex);
            }

            this.Cursor = Cursors.Default;
        }

        private object[,] ParseClipboardContent(DataObject clipboardContent)
        {
            List<List<object>> resultList = new List<List<object>>();

            string content = clipboardContent.GetData("System.String") as string;
            if (content == null) return null;

            string[] lines = content.Split(new[] { "\r\n" }, StringSplitOptions.None);

            for (int i = 0; i < lines.Length; i++)
            {
                string line = lines[i];
                string[] values = line.Split('\t');

                resultList.Add(new List<object>());

                for (int j = 0; j < values.Length; j++)
                {
                    double? d = ParsingTools.ClipboardStringToDouble(values[j],true);
                    object value = d == null ? (object)values[j] : d.Value;
                    resultList[i].Add(value);
                }
            }

            object[,] result = (object[,])Array.CreateInstance(typeof(object), new[] { resultList.Count, resultList.Max(l => l.Count) },
                                                                new[] { 0, 0 });

            for (int i = 0; i < resultList.Count; i++)
            {
                List<object> list = resultList[i];
                for (int j = 0; j < list.Count; j++)
                {
                    result[i, j] = list[j];
                }
            }

            return result;
        }

        private string FormatStringCopy(DataObject clipboardContent, CultureInfo culture)
        {
            string content = clipboardContent.GetData("System.String") as string;
            if (content == null) return null;

            string[] lines = content.Split(new[] { "\r\n" }, StringSplitOptions.None);
            StringBuilder builder = new StringBuilder();

            for (int index = 0; index < lines.Length; index++)
            {
                string line = lines[index];
                string[] values = line.Split('\t');
                if (index > 0) builder.Append(Environment.NewLine);
                for (int i = 0; i < values.Length; i++)
                {
                    string cell = values[i];
                    if (i > 0) builder.Append("\t");

                    builder.Append(ParsingTools.ChangeStringCulture(cell, culture));
                }
            }

            return builder.ToString();
        }

        private string FormatCsvCopy(DataObject clipboardContent, CultureInfo culture)
        {
            string csv = clipboardContent.GetData("Csv") as string;
            if (csv == null) return null;

            const string lineSeparator = "\r\n";
            string[] lines = csv.Split(new[] { lineSeparator }, StringSplitOptions.None);

            return string.Join(lineSeparator, lines.Select(l => this.FormatCsvLine(l, culture)));
        }

        private string FormatCsvLine(string line, CultureInfo culture)
        {
            IEnumerable<string> fields = CsvTools.ParseCsvLine(line);

            return string.Join(",", fields.Select(f => string.Format("\"{0}\"", ParsingTools.ChangeStringCulture(f, culture))));
        }

        void DataGridViewEx_FontChanged(object sender, EventArgs e)
        {
            DataGridView grid = (DataGridView)sender;
            Font boldFont = new Font(grid.Font, FontStyle.Bold);
            grid.SuspendLayout();
            //foreach (DataGridViewColumn column in grid.Columns)
            //{
            //    column.HeaderCell.Style.Font = boldFont;
            //}
            grid.ResumeLayout(true);
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);

            this.PaintBackgroundImage(e.Graphics, e.ClipRectangle);
        }

        protected void PaintBackgroundImage(Graphics graphics, Rectangle clipBounds)
        {
            if (this.BackgroundImage == null) return;

            Rectangle gridBounds = new Rectangle(0, 0, this.Width, this.Height);
            gridBounds = this.AjustBoundsByHeaders(gridBounds);

            Rectangle destBounds = gridBounds;

            Rectangle srcBounds = new Rectangle();
            srcBounds.Size = this.BackgroundImage.Size;

            destBounds.X += (gridBounds.Width - srcBounds.Width) / 2 - this.HorizontalScrollingOffset;
            destBounds.Y += (gridBounds.Height - srcBounds.Height) / 2 - this.VerticalScrollingOffset;

            if (destBounds.X - gridBounds.X < 0)
            {
                if (destBounds.X - gridBounds.X + srcBounds.Height < 0) return;

                srcBounds.X = -destBounds.X + gridBounds.X;
                destBounds.X = gridBounds.X;
                srcBounds.Width -= srcBounds.X;
            }

            if (destBounds.Y - gridBounds.Y < 0)
            {
                if (destBounds.Y - gridBounds.Y + srcBounds.Height < 0) return;

                srcBounds.Y = -destBounds.Y + gridBounds.Y;
                destBounds.Y = gridBounds.Y;
                srcBounds.Height -= srcBounds.Y;
            }

            destBounds.Size = srcBounds.Size;

            float[][] ptsArray = { new float[] { 1, 0, 0, 0, 0 }, new float[] { 0, 1, 0, 0, 0 }, new float[] { 0, 0, 1, 0, 0 }, new float[] { 0, 0, 0, IMAGE_TRANSPARENCY, 0 }, new float[] { 0, 0, 0, 0, 1 } };
            ColorMatrix clrMatrix = new ColorMatrix(ptsArray);
            ImageAttributes imgAttributes = new ImageAttributes();
            imgAttributes.SetColorMatrix(clrMatrix, ColorMatrixFlag.Default, ColorAdjustType.Bitmap);

            graphics.DrawImage(this.BackgroundImage, destBounds, srcBounds.X, srcBounds.Y, srcBounds.Width, srcBounds.Height, GraphicsUnit.Pixel, imgAttributes);
        }

        private Rectangle AjustBoundsByHeaders(Rectangle rect)
        {
            if (this.ColumnHeadersVisible)
            {
                rect.Y += this.ColumnHeadersHeight;
                rect.Height -= this.ColumnHeadersHeight;
            }
            if (this.RowHeadersVisible)
            {
                rect.X += this.RowHeadersWidth;
                rect.Width -= this.RowHeadersWidth;
            }

            return rect;
        }
    }
}