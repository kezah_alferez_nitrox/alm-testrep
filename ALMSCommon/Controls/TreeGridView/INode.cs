namespace ALMSCommon.Controls.TreeGridView
{
    public interface INode
    {
        TreeGridNodeCollection Nodes { get; }
    }
}