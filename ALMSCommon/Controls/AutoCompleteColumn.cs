using System;
using System.Windows.Forms;

namespace ALMSCommon.Controls
{
    public class AutoCompleteColumn : DataGridViewTextBoxColumn
    {
        private string[] valueList;

        public AutoCompleteColumn()
        {
            CellTemplate = new AutoCompleteCell();
        }

        public string[] ValueList
        {
            get { return valueList; }
            set
            {
                if (valueList != value)
                {
                    valueList = value;

                    AutoCompleteCell cellTemplate = (AutoCompleteCell) CellTemplate;
                    cellTemplate.ValueList = value;

                    return;
                    //
                    // now set it on all cells in other rows as well.
                    //
                    if (this.DataGridView != null)
                    {
                        int rowCount = DataGridView.Rows.Count;
                        for (int x = 0; x < rowCount; x++)
                        {
                            DataGridViewCell cell = DataGridView.Rows.SharedRow(x).Cells[x];
                            if (cell is AutoCompleteCell)
                            {
                                AutoCompleteCell autoCompleteCell = (AutoCompleteCell) cell;
                                autoCompleteCell.ValueList = value;
                            }
                        }
                    }
                }
            }
        }

        public override DataGridViewCell CellTemplate
        {
            get { return base.CellTemplate; }
            set
            {
                if (!(value is AutoCompleteCell)) throw new ArgumentException("Should be AutoCompleteCell");

                base.CellTemplate = value;
            }
        }
    }
}