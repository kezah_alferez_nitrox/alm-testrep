using System;
using System.Windows.Forms;

namespace ALMSCommon.Controls
{
    public class AutoCompleteCell : DataGridViewTextBoxCell
    {
        private string[] valueList;

        public string[] ValueList
        {
            get { return valueList; }
            set { valueList = value; }
        }

        public override void InitializeEditingControl(int rowIndex,
                                                      object initialFormattedValue,
                                                      DataGridViewCellStyle dataGridViewCellStyle)
        {
            base.InitializeEditingControl(rowIndex, initialFormattedValue,
                                          dataGridViewCellStyle);

            AutoCompleteEditingControl editingControl = (AutoCompleteEditingControl) DataGridView.EditingControl;

            DataGridViewColumn dgvc = OwningColumn;
            if (dgvc is AutoCompleteColumn)
            {
                AutoCompleteColumn autoCompleteColumn = (AutoCompleteColumn) dgvc;

                editingControl.ValueList = this.valueList == null ? autoCompleteColumn.ValueList : this.ValueList;
            }
        }

        public override Type EditType
        {
            get { return typeof (AutoCompleteEditingControl); }
        }
    }
}