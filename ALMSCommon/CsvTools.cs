﻿using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace ALMSCommon
{
    public class CsvTools
    {
        public static string FormatCsvLine(IEnumerable<string> fields)
        {
            return fields.Select(f => string.Format("\"{0}\"", f)).Aggregate("", (a, b) => a + "," + b);
        }

        public static IList<string> ParseCsvLine(string csvLine)
        {
            int intCounter;
            StringBuilder strElem = new StringBuilder();
            IList<string> alParsedCsv = new List<string>();
            int intLenght = csvLine.Length;
            strElem = strElem.Append("");
            int intCurrState = 0;
            int[][] aActionDecider = new int[9][];

            //Build the state array
            aActionDecider[0] = new [] { 2, 0, 1, 5 };
            aActionDecider[1] = new [] { 6, 0, 1, 5 };
            aActionDecider[2] = new [] { 4, 3, 3, 6 };
            aActionDecider[3] = new [] { 4, 3, 3, 6 };
            aActionDecider[4] = new [] { 2, 8, 6, 7 };
            aActionDecider[5] = new [] { 5, 5, 5, 5 };
            aActionDecider[6] = new [] { 6, 6, 6, 6 };
            aActionDecider[7] = new [] { 5, 5, 5, 5 };
            aActionDecider[8] = new [] { 0, 0, 0, 0 };

            for (intCounter = 0; intCounter < intLenght; intCounter++)
            {
                intCurrState = aActionDecider[intCurrState][GetInputId(csvLine[intCounter])];
                //take the necessary action depending upon the state returned
                PerformAction(ref intCurrState, csvLine[intCounter], ref strElem, alParsedCsv);
            }
            intCurrState = aActionDecider[intCurrState][3];
            PerformAction(ref intCurrState, '\0', ref strElem, alParsedCsv);
            return alParsedCsv;
        }

        private static int GetInputId(char chrInput)
        {
            if (chrInput == '"')
            {
                return 0;
            }
            else if (chrInput == ',')
            {
                return 1;
            }
            else
            {
                return 2;
            }
        }

        private static void PerformAction(ref int intCurrState, char chrInputChar, ref StringBuilder strElem, IList<string> alParsedCsv)
        {
            string strTemp;
            switch (intCurrState)
            {
                case 0:
                    //Seperate out value to array list
                    strTemp = strElem.ToString();
                    alParsedCsv.Add(strTemp);
                    strElem = new StringBuilder();
                    break;
                case 1:
                case 3:
                case 4:
                    //accumulate the character
                    strElem.Append(chrInputChar);
                    break;
                case 5:
                    //End of line reached. Seperate out value to array list
                    strTemp = strElem.ToString();
                    alParsedCsv.Add(strTemp);
                    break;
                case 6:
                    //Erroneous input. Reject line.
                    alParsedCsv.Clear();
                    break;
                case 7:
                    //wipe ending " and Seperate out value to array list
                    strElem.Remove(strElem.Length - 1, 1);
                    strTemp = strElem.ToString();
                    alParsedCsv.Add(strTemp);
                    strElem = new StringBuilder();
                    intCurrState = 5;
                    break;
                case 8:
                    //wipe ending " and Seperate out value to array list
                    strElem.Remove(strElem.Length - 1, 1);
                    strTemp = strElem.ToString();
                    alParsedCsv.Add(strTemp);
                    strElem = new StringBuilder();
                    //goto state 0
                    intCurrState = 0;
                    break;
            }
        }
    }
}