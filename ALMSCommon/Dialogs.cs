using System.Windows.Forms;

namespace ALMSCommon
{
    public static class Dialogs
    {
        public static void Error(string title, string message)
        {
            MessageBox.Show(message, title, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public static void Warning(string title, string message)
        {
            MessageBox.Show(message, title, MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        public static void Info(string title, string message)
        {
            MessageBox.Show(message, title, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public static bool Confirm(string title, string message)
        {
            return MessageBox.Show(message, title, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes;
        }

        public static DialogResult AskForSave(string title, string message)
        {
            return MessageBox.Show(message, title, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Exclamation);
        }

        public static string InputString(string title, string message)
        {
            Form prompt = new Form();
            prompt.Width = 500;
            prompt.Height = 150;
            prompt.Text = title;
            
            Label textLabel = new Label() { Left = 50, Top = 20, Text = message, AutoSize = true};
            
            TextBox textBox = new TextBox() { Left = 50, Top = 50, Width = 400 };
            Button confirmation = new Button() { Text = "Ok", Left = 350, Width = 100, Top = 70 };
            confirmation.Click += (sender, e) => { prompt.Close(); };
            textBox.KeyUp += (sender, e) => { if(e.KeyCode==Keys.Enter) prompt.Close(); };
            
            //confirmation.NotifyDefault(true);
            prompt.Controls.Add(confirmation);
            prompt.Controls.Add(textLabel);
            prompt.Controls.Add(textBox);
            prompt.Shown += (sender, e) => { textBox.Focus(); };
            prompt.ShowDialog();
            return textBox.Text;            
        }
    }
}