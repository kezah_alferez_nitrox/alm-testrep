namespace ALMSCommon.Data
{
    public interface IHasId<TId>
    {
        TId Id { get; }
    }
}