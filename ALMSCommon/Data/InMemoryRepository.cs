using System.Collections.Generic;

namespace ALMSCommon.Data
{
    public class InMemoryRepository<TId, TModel> : IRepository<TId, TModel> where TModel : IHasId<TId>
    {
        private static readonly IDictionary<TId, TModel> Data = new Dictionary<TId, TModel>();

        public virtual void Save(TModel model)
        {
            Data[model.Id] = model;
        }

        public virtual void Update(TModel model)
        {
            this.Save(model);
        }

        public virtual void Delete(TModel model)
        {
            Data.Remove(model.Id);
        }

        public virtual TModel Load(TId id)
        {
            if (Data.ContainsKey(id))
            {
                return Data[id];
            }

            return default(TModel);
        }

        public virtual IEnumerable<TModel> GetAll()
        {
            return Data.Values;
        }

        public virtual void DeleteAll()
        {
            Data.Clear();
        }

        public virtual int Count
        {
            get { return Data.Count; }
        }
    }
}