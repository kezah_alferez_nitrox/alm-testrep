using System.Collections.Generic;

namespace ALMSCommon.Data
{
    public interface IRepository<TId, TModel>
    {
        void Save(TModel obj);
        void Update(TModel obj);
        void Delete(TModel obj);
        TModel Load(TId id);
        IEnumerable<TModel> GetAll();
        // IList<T> Find(Func<T, bool> where);
        void DeleteAll();
        // void Delete(Func<T, bool> where);
        int Count { get; }
    }

    public interface IRepository<TModel> : IRepository<int, TModel>
    {
    }
}