using System.Collections.Generic;

namespace ALMSCommon.Data
{
    public class InMemoryWithoutIdRepository<TModel> : IRepository<int, TModel>
    {
        private static readonly IList<TModel> Data = new List<TModel>();

        public virtual void Save(TModel model)
        {
            Data.Add(model);
        }

        public virtual void Update(TModel model)
        {
            // nothing
        }

        public virtual void Delete(TModel model)
        {
            if(Data.Contains(model)) Data.Remove(model);
        }

        public virtual TModel Load(int id)
        {
            if (id >= 0 && id < Data.Count)
            {
                return Data[id];
            }

            return default(TModel);
        }

        public virtual IEnumerable<TModel> GetAll()
        {
            return Data;
        }

        public virtual void DeleteAll()
        {
            Data.Clear();
        }

        public virtual int Count
        {
            get { return Data.Count; }
        }
    }
}