using System;

namespace ALMSCommon
{
    public interface ILicenceInfo
    {
        string LicenceNumber { get;}
        string MachineId { get;}
        DateTime Start { get;}
        DateTime End { get;}
        string FirstName { get;}
        string LastName { get;}
        string Company { get;}
        string PhoneNumber { get;}
        string Email { get;}
        int[] Interfaces { get; }
    }
}