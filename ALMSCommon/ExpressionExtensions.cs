﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace ALMSCommon
{
    public static class StaticReflector
    {
        public static string GetAccessor<TModel>(Expression<Func<TModel, object>> self)
        {
            MemberExpression memberExpression = GetMemberExpression(self);
            return GetAccessor(memberExpression).Name;
        }

        private static MemberExpression GetMemberExpression<TModel, T>(Expression<Func<TModel, T>> expression)
        {
            MemberExpression memberExpression = null;
            if (expression.Body.NodeType == ExpressionType.Convert)
            {
                var body = (UnaryExpression) expression.Body;
                memberExpression = body.Operand as MemberExpression;
            }
            else if (expression.Body.NodeType == ExpressionType.MemberAccess)
            {
                memberExpression = expression.Body as MemberExpression;
            }

            if (memberExpression == null) throw new ArgumentException("Not a member access", "expression");
            return memberExpression;
        }

        private static IAccessor GetAccessor(MemberExpression memberExpression)
        {
            var list = new List<PropertyInfo>();

            while (memberExpression != null)
            {
                list.Add((PropertyInfo) memberExpression.Member);
                memberExpression = memberExpression.Expression as MemberExpression;
            }

            if (list.Count == 1)
            {
                return new SingleProperty(list[0]);
            }

            list.Reverse();
            return new PropertyChain(list.ToArray());
        }

        #region Nested type: IAccessor

        public interface IAccessor
        {
            string Name { get; }
        }

        #endregion

        #region Nested type: PropertyChain

        public class PropertyChain : IAccessor
        {
            private readonly PropertyInfo[] chain;

            public PropertyChain(PropertyInfo[] properties)
            {
                this.chain = properties;
            }

            #region IAccessor Members

            public string Name
            {
                get
                {
                    string returnValue = string.Empty;
                    foreach (PropertyInfo info in this.chain)
                        returnValue += returnValue.Length > 0 ? "." + info.Name : info.Name;

                    return returnValue;
                }
            }

            #endregion
        }

        #endregion

        #region Nested type: SingleProperty

        public class SingleProperty : IAccessor
        {
            private readonly PropertyInfo propertyInfo;

            public SingleProperty(PropertyInfo property)
            {
                this.propertyInfo = property;
            }

            #region IAccessor Members

            public string Name
            {
                get { return this.propertyInfo.Name; }
            }

            #endregion
        }

        #endregion
    }
}