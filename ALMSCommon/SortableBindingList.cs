using System.Collections.Generic;
using System.ComponentModel;

namespace ALMSCommon
{
    /// <summary>
    /// See http://msdn.microsoft.com/en-us/library/ms993236.aspx
    /// </summary>
    public class SortableBindingList<T> : BindingList<T>
    {
        private readonly bool sortLastElement;
        private bool _isSorted;

        public SortableBindingList() : this(false)
        {
        }

        public SortableBindingList(bool sortLastElement)
        {
            this.sortLastElement = sortLastElement;
        }

        public SortableBindingList(IList<T> list) : base(list)
        {
            sortLastElement = true;
        }

        protected override bool SupportsSortingCore
        {
            get { return true; }
        }

        protected override void ApplySortCore(PropertyDescriptor property, ListSortDirection direction)
        {
            // Get list to sort
            List<T> items = Items as List<T>;

            // Apply and set the sort, if items to sort
            if (items != null)
            {
                PropertyComparer<T> pc = new PropertyComparer<T>(property, direction);
                if (items.Count > 1)
                {
                    // TODO : find a proper way
                    // don't sort last line
                    int count = sortLastElement ? this.Count : items.Count - 1;
                    items.Sort(0, count, pc);
                }
                _isSorted = true;
            }
            else
            {
                _isSorted = false;
            }

            // Let bound controls know they should refresh their views
            OnListChanged(new ListChangedEventArgs(ListChangedType.Reset, -1));
        }

        protected override bool IsSortedCore
        {
            get { return _isSorted; }
        }

        protected override void RemoveSortCore()
        {
            _isSorted = false;
        }
    }
}