using System;
using System.Globalization;
using System.Threading;
using System.Windows.Forms;

namespace LifeInsurance
{
    internal static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
#if !DEBUG
            return;
            Application.ThreadException += new ThreadExceptionEventHandler(Application_ThreadException);
#endif
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }

#if !DEBUG
        static void Application_ThreadException(object sender, ThreadExceptionEventArgs e)
        {
            // todo: log
        }
#endif
    }
}