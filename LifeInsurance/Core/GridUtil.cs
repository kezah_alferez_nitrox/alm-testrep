using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace LifeInsurance.Core
{
    public class GridUtil
    {
        private const char GridStyleSeparator = '�';

        public static void EnsureGridHasSpaceToPaste(DataGridView grid, int firstRow, int firstColumn, string[][] data)
        {
            int maxItemsPerLine = GetMaxItemsPerLine(data);

            int columnDelta = firstColumn + maxItemsPerLine - grid.Columns.Count;
            for (int i = 0; i < columnDelta; i++)
            {
                AddTextBoxColumn(grid);
            }

            int rowDelta = firstRow + data.Length - grid.Rows.Count;
            if (rowDelta > 0)
            {
                grid.Rows.Add(rowDelta);
            }
        }

        public static string[][] SplitFileContentTo2DArray(string text)
        {
            return SplitStringTo2DArray(text, ';');
        }

        public static string[][] SplitClipboardContentTo2DArray(string text)
        {
            return SplitStringTo2DArray(text, '\t');
        }

        private static string[][] SplitStringTo2DArray(string text, char separator)
        {
            if (String.IsNullOrEmpty(text)) return null;

            text = text.TrimEnd('\n', '\r');
            string[] clipboardLines = text.Split('\n');

            string[][] data = new string[clipboardLines.Length][];
            for (int i = 0; i < clipboardLines.Length; i++)
            {
                string line = clipboardLines[i];
                data[i] = line == null ? new string[0] : line.Trim('\r').Split(separator);
            }

            return data;
        }

        public static void DisplayFileAsGrid(string fullName, DataGridView grid)
        {
            ((ISupportInitialize)(grid)).BeginInit();
            try
            {
                grid.Columns.Clear();

                string allText = File.ReadAllText(fullName);
                string[][] data = SplitFileContentTo2DArray(allText);

                if (data == null) return;

                int maxCellsPerRow = GetMaxItemsPerLine(data);

                for (int col = 0; col < maxCellsPerRow; col++)
                {
                    AddTextBoxColumn(grid);
                }

                object[][] convertedData = TryConvertToDoubles(data);
                foreach (object[] line in convertedData)
                {
                    grid.Rows.Add(line);
                }

                LoadFormatting(fullName + ".style", grid);
            }
            finally
            {
                ((ISupportInitialize)(grid)).EndInit();
            }
        }

        private static object[][] TryConvertToDoubles(string[][] strings)
        {
            object[][] result = new object[strings.Length][];
            for (int i = 0; i < strings.Length; i++)
            {
                string[] line = strings[i];
                result[i] = new object[line.Length];
                for (int j = 0; j < line.Length; j++)
                {
                    string s = line[j];
                    double d = Util.ConvertToDouble(s);
                    result[i][j] = double.IsNaN(d) ? (object)s : d;
                }
            }

            return result;
        }

        private static void LoadFormatting(string fileName, DataGridView grid)
        {
            if (!File.Exists(fileName)) return;

            string allText = File.ReadAllText(fileName);
            string[][] data = SplitFileContentTo2DArray(allText);

            if (data == null) return;

            for (int col = 0; col < data[0].Length; col++)
            {
                if (col >= grid.Columns.Count) break;

                grid.Columns[col].Width = int.Parse(data[0][col]);
            }

            for (int row = 1; row < data.Length; row++)
            {
                int gridRow = row - 1;
                if (gridRow >= grid.Rows.Count) break;

                grid.Rows[gridRow].Height = int.Parse(data[row][0]);

                for (int col = 1; col < data[row].Length; col++)
                {
                    int gridCol = col - 1;
                    if (gridCol >= grid.Columns.Count) break;

                    ApplyStringStyle(data[row][col], grid[gridCol, gridRow]);
                }
            }
        }

        private static void ApplyStringStyle(string stringStyle, DataGridViewCell cell)
        {
            Font font;
            Color foreColor;
            Color backColor;
            string format;
            StringToStyle(stringStyle, out font, out foreColor, out backColor, out format);

            DataGridViewCellStyle style = cell.Style;
            if (font != null) style.Font = font;
            if (foreColor.ToArgb() != 0) style.ForeColor = foreColor;
            if (backColor.ToArgb() != 0) style.BackColor = backColor;
            style.Format = format;
        }

        private static void AddTextBoxColumn(DataGridView grid)
        {
            DataGridViewTextBoxColumn column = new DataGridViewTextBoxColumn();
            column.SortMode = DataGridViewColumnSortMode.NotSortable;
            column.DefaultCellStyle.Format = "N0";
            grid.Columns.Add(column);
        }

        private static int GetMaxItemsPerLine(string[][] items)
        {
            int max = 0;

            foreach (string[] line in items)
            {
                if (line != null) max = Math.Max(max, line.Length);
            }

            return max;
        }

        public static void SaveGridAsFile(string fileName, DataGridView grid)
        {
            List<string> lines = new List<string>();

            for (int rowIndex = 0; rowIndex < grid.Rows.Count; rowIndex++)
            {
                if (grid.Rows[rowIndex].IsNewRow) continue;

                StringBuilder lineBuilder = new StringBuilder();
                for (int columnIndex = 0; columnIndex < grid.Columns.Count; columnIndex++)
                {
                    if (columnIndex > 0)
                    {
                        lineBuilder.Append(TreeViewUtils.Separator);
                    }
                    lineBuilder.Append(Convert.ToString(grid[columnIndex, rowIndex].Value));
                }
                lines.Add(lineBuilder.ToString());
            }

            File.WriteAllLines(fileName, lines.ToArray());
        }

        public static void SaveGridFormattingAsFile(string fileName, DataGridView grid)
        {
            List<string> lines = new List<string>();

            StringBuilder builder = new StringBuilder();
            for (int columnIndex = 0; columnIndex < grid.Columns.Count; columnIndex++)
            {
                if (columnIndex > 0)
                {
                    builder.Append(TreeViewUtils.Separator);
                }
                builder.Append(grid.Columns[columnIndex].Width);
            }
            lines.Add(builder.ToString());

            for (int rowIndex = 0; rowIndex < grid.Rows.Count; rowIndex++)
            {
                builder = new StringBuilder();
                builder.Append(grid.Rows[rowIndex].Height);
                for (int columnIndex = 0; columnIndex < grid.Columns.Count; columnIndex++)
                {
                    builder.Append(TreeViewUtils.Separator);
                    builder.Append(StyleToString(grid[columnIndex, rowIndex].Style));
                }
                lines.Add(builder.ToString());
            }

            File.WriteAllLines(fileName, lines.ToArray());

            Debug.WriteLine("saved " + fileName);
        }

        private static string StyleToString(DataGridViewCellStyle style)
        {
            string font = style.Font == null
                              ? "" + GridStyleSeparator + GridStyleSeparator + GridStyleSeparator
                              : style.Font.Name + GridStyleSeparator + style.Font.Size + GridStyleSeparator +
                                  style.Font.Bold + GridStyleSeparator + style.Font.Italic;

            Debug.WriteLine("font : " + font);

            string foreColor = style.ForeColor.ToArgb().ToString();
            string backColor = style.BackColor.ToArgb().ToString();
            return font + GridStyleSeparator + foreColor + GridStyleSeparator + backColor + GridStyleSeparator + style.Format;
        }

        private static void StringToStyle(string text, out Font font, out Color foreColor, out Color backColor, out string format)
        {
            font = null;
            string[] strings = text.Split(GridStyleSeparator);
            if (!string.IsNullOrEmpty(strings[0]))
            {
                FontStyle fontStyle = new FontStyle();
                if (Convert.ToBoolean(strings[2])) fontStyle |= FontStyle.Bold;
                if (Convert.ToBoolean(strings[3])) fontStyle |= FontStyle.Italic;
                font = new Font(strings[0], float.Parse(strings[1]), fontStyle);
            }

            foreColor = Color.FromArgb(int.Parse(strings[4]));
            if (foreColor.ToArgb() == 0) foreColor = Color.Black;

            backColor = Color.FromArgb(int.Parse(strings[5]));
            if (backColor.ToArgb() == 0) backColor = Color.White;

            format = strings.Length >= 7 ? strings[6] : "";
        }
    }
}