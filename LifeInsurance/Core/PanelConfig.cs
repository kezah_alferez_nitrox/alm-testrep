using ALMSCommon.Data;

namespace LifeInsurance.Core
{
    public class PanelConfig : IHasId<string>
    {
        public string Id { get; set; }
        public int Height { get; set; }

        public PanelConfig(string id, int height)
        {
            this.Id = id;
            this.Height = height;
        }
    }
}