using ALMSCommon.Data;
using LifeInsurance.Data;

namespace LifeInsurance.Core
{
    public interface IUserSettingRepository : IRepository<string, UserSetting>, IRestorable
    {
    }
}