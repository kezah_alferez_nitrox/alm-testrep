using ALMSCommon.Data;

namespace LifeInsurance.Core
{
    public interface IPanelConfigRepository : IRepository<string, PanelConfig>, IRestorable
    {
    }
}