using System;
using System.Collections.Generic;

namespace LifeInsurance.Core
{
    public static class ViewsStatus
    {
        private static readonly IDictionary<ViewType, bool> ShouldReload = new Dictionary<ViewType, bool>();

        static ViewsStatus()
        {
            foreach (object type in Enum.GetValues(typeof(ViewType)))
            {
                ShouldReload[(ViewType)type] = true;
            }
        }

        public static void SetDirty(ViewType type)
        {
            ShouldReload[type] = true;
        }

        public static void SetClean(ViewType type)
        {
            ShouldReload[type] = false;
        }

        public static bool NeedsReloading(ViewType type)
        {
            return ShouldReload[type];
        }
    }

    public enum ViewType
    {
        None,
        Setup,
        Inputs,
        Result,
        UserData
    }
}