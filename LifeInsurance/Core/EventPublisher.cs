using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace LifeInsurance.Core
{
    public delegate void Action();

    public class EventPublisher
    {
        private static readonly IDictionary<Type, List<Action<object>>> Subscribers
            = new Dictionary<Type, List<Action<object>>>();

        public static void Publish<T>(T eventToPublish, Control publishedBy) where T : IEvent
        {
            List<Action<object>> actions;
            lock (Subscribers)
            {
                if (Subscribers.TryGetValue(typeof(T), out actions) == false)
                {
                    return;
                }
            }
            foreach (var action in actions)
            {
                // not working
                //if (action.Target == publishedBy)
                //    continue;
                action(eventToPublish);
            }
        }

        public static void Register<T>(Action<T> action) where T : IEvent
        {
            lock (Subscribers)
            {
                List<Action<object>> value;
                if (Subscribers.TryGetValue(typeof(T), out value) == false)
                {
                    Subscribers[typeof(T)] = value = new List<Action<object>>();
                }
                Control control = action.Target as Control;
                Action<object> item = o =>
                                          {
                                              if (control != null && control.InvokeRequired)
                                              {
                                                  control.Invoke((Action)delegate { action((T)o); });
                                              }
                                              else
                                              {
                                                  action((T)o);
                                              }
                                          };
                if (control != null) control.Disposed += (s, e) => Unregister<T>(item);
                value.Add(item);
            }
        }

        private static void Unregister<T>(Action<object> action) where T : IEvent
        {
            lock (Subscribers)
            {
                List<Action<object>> value;
                if (Subscribers.TryGetValue(typeof(T), out value) == false)
                {
                    return;
                }
                value.Remove(action);
            }
        }
    }
}