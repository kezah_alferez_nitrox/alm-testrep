namespace LifeInsurance.Core
{
    public interface IRestorable
    {
        void Backup(string fileName);
        void Restore(string fileName);
    }
}