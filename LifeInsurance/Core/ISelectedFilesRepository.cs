using ALMSCommon.Data;
using LifeInsurance.Data;

namespace LifeInsurance.Core
{
    public interface ISelectedFilesRepository : IRepository<string, InputFile>, IRestorable
    {
    }
}