using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.Windows.Forms;
using LifeInsurance.Data;

namespace LifeInsurance.Core
{
    public class TreeViewUtils
    {
        public const char Separator = ';';

        public static void AddFileToFileTree(TreeView treeView, string fileName, IList<string> filter)
        {
            FileInfo fileInfo = new FileInfo(fileName);

            DirectoryInfo directory = fileInfo.Directory;
            Stack<DirectoryInfo> parentsInfo = new Stack<DirectoryInfo>();

            while (!ArePathsTheSame(Client.Path, directory.FullName))
            {
                parentsInfo.Push(directory);
                directory = directory.Parent;

                if (directory == null) break;
            }

            if(parentsInfo.Count == 0) return;
            DirectoryInfo rootParent = parentsInfo.Peek();
            if (filter != null && !filter.Contains(rootParent.Name))
            {
                return;
            }

            TreeNode parentNode = null;
            TreeNodeCollection nodes = treeView.Nodes;
            while (parentsInfo.Count > 0)
            {
                DirectoryInfo parentInfo = parentsInfo.Pop();

                parentNode = FindNodeByName(nodes, parentInfo.Name);
                if (parentNode == null)
                {
                    parentNode = new TreeNode(parentInfo.Name);
                    parentNode.Tag = parentInfo;
                    parentNode.ImageIndex = 0;
                    parentNode.SelectedImageIndex = 0;

                    nodes.Add(parentNode);
                }
                nodes = parentNode.Nodes;
            }

            if (parentNode != null)
            {
                foreach (TreeNode sibling in parentNode.Nodes)
                {
                    InputFile inputFile = sibling.Tag as InputFile;
                    if (inputFile != null &&
                        (String.Compare(fileInfo.FullName, inputFile.FullName, StringComparison.InvariantCultureIgnoreCase) == 0))
                    {
                        return;
                    }
                }

                TreeNode fileNode = parentNode.Nodes.Add(fileInfo.Name);
                fileNode.ImageIndex = 1;
                fileNode.SelectedImageIndex = 1;
                fileNode.Tag = new InputFile(fileInfo.FullName);

                ExpandAllParents(parentNode);
            }
        }

        public static void ExpandAllParents(TreeNode node)
        {
            while (node != null)
            {
                node.Expand();
                node = node.Parent;
            }
        }

        public static TreeNode FindNodeByName(TreeNodeCollection collection, string name)
        {
            foreach (TreeNode node in collection)
            {
                if (String.Compare(node.Text, name, StringComparison.InvariantCultureIgnoreCase) == 0)
                {
                    return node;
                }
                TreeNode chidNode = FindNodeByName(node.Nodes, name);
                if (chidNode != null) return chidNode;
            }

            return null;
        }

        public static bool ArePathsTheSame(string path1, string path2)
        {
            return
                String.Compare(Path.GetFullPath(path1), Path.GetFullPath(path2),
                               StringComparison.InvariantCultureIgnoreCase) == 0;
        }

        public static void AddFilesToTreeView(DirectoryInfo directory, TreeNodeCollection collection, ICollection<string> filter, int level)
        {
            DirectoryInfo[] directories = directory.GetDirectories();
            foreach (DirectoryInfo childDirectory in directories)
            {
                if (IsVisibileDirectory(childDirectory))
                {
                    if (level > 0 || filter.Contains(childDirectory.Name))
                    {
                        TreeNode directoryNode = collection.Add(childDirectory.Name);
                        directoryNode.ImageIndex = directoryNode.SelectedImageIndex = 0;
                        directoryNode.Tag = childDirectory;

                        AddFilesToTreeView(childDirectory, directoryNode.Nodes, filter, level + 1);
                    }
                }
            }

            if (level > 0)
            {
                foreach (FileInfo childFile in directory.GetFiles())
                {
                    if(childFile.FullName.EndsWith(".style")) continue;

                    TreeNode fileNode = collection.Add(childFile.Name);
                    fileNode.ImageIndex = 1;
                    fileNode.SelectedImageIndex = 1;
                    fileNode.Tag = new InputFile(childFile.FullName);
                }
            }
        }

        public static bool IsVisibileDirectory(DirectoryInfo directory)
        {
            return (directory.Attributes & FileAttributes.Hidden) != FileAttributes.Hidden;
        }

        public static IEnumerable<TreeNode> GetAllNodes(TreeView treeView)
        {
            return GetAllNodes(treeView.Nodes);
        }

        private static IEnumerable<TreeNode> GetAllNodes(TreeNodeCollection collection)
        {
            foreach (TreeNode node in collection)
            {
                yield return node;

                foreach (TreeNode childNode in GetAllNodes(node.Nodes))
                {
                    yield return childNode;
                } 
            }
        }
    }
}