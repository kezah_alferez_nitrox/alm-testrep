using System;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Text;
using System.Threading;

namespace LifeInsurance.Core
{
    internal class Util
    {
        public static string RelativePath(string absolutePath, string relativeTo)
        {
            string[] absoluteDirectories = absolutePath.Split('\\');
            string[] relativeDirectories = relativeTo.Split('\\');

            //Get the shortest of the two paths
            int length = absoluteDirectories.Length < relativeDirectories.Length ? absoluteDirectories.Length : relativeDirectories.Length;

            //Use to determine where in the loop we exited
            int lastCommonRoot = -1;
            int index;

            //Find common root
            for (index = 0; index < length; index++)
                if (absoluteDirectories[index] == relativeDirectories[index])
                    lastCommonRoot = index;
                else
                    break;

            //If we didn't find a common prefix then throw
            if (lastCommonRoot == -1)
                throw new ArgumentException("Paths do not have a common base");

            //Build up the relative path
            StringBuilder relativePath = new StringBuilder();

            //Add on the ..
            for (index = lastCommonRoot + 1; index < relativeDirectories.Length; index++)
                if (relativeDirectories[index].Length > 0)
                    relativePath.Append("..\\");

            //Add on the folders
            for (index = lastCommonRoot + 1; index < absoluteDirectories.Length - 1; index++)
                relativePath.Append(absoluteDirectories[index] + "\\");
            relativePath.Append(absoluteDirectories[absoluteDirectories.Length - 1]);

            return relativePath.ToString();
        }

        private static readonly CultureInfo[] AcceptedCultures = new[]
                                                                     {
                                                                         new CultureInfo("fr-FR"),
                                                                         new CultureInfo("en-US"), 
                                                                     };

        public static double ConvertToDouble(object obj)
        {
            if (obj is double) return (double)obj;

            string str = Convert.ToString(obj);
            if (string.IsNullOrEmpty(str)) return double.NaN;
            str = str.Trim();
            str = ReplaceParantheses(str);

            foreach (CultureInfo accepedCulture in AcceptedCultures)
            {
                double doubleValue;
                if (double.TryParse(str,
                                    NumberStyles.AllowDecimalPoint | NumberStyles.AllowLeadingSign |
                                    NumberStyles.AllowThousands, accepedCulture, out doubleValue))
                {
                    return doubleValue;
                }
            }

            return double.NaN;
        }

        private static string ReplaceParantheses(string number)
        {
            if (string.IsNullOrEmpty(number)) return number;

            number = number.Trim();

            if (number[0] == '(' && number[number.Length - 1] == ')')
            {
                number = "-" + number.Substring(1, number.Length - 2);
            }
            return number;
        }

        public static bool InDesignMode
        {
            get
            {
                return LicenseManager.UsageMode == LicenseUsageMode.Designtime;
            }
        }

        public static object CallPrivateInstanceMethod<T>(T target, string methodName, params object[] args)
        {
            Type type = typeof(T);

            MethodInfo methodInfo = type.GetMethod(methodName, BindingFlags.Instance | BindingFlags.NonPublic);
            if (methodInfo == null) return null;

            return methodInfo.Invoke(target, args);
        }

        public static string GetApplicationDirectory()
        {
            return Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
        }

        static public void CopyFolder(string sourceFolder, string destFolder)
        {
            if (!Directory.Exists(destFolder))
                Directory.CreateDirectory(destFolder);

            string[] files = Directory.GetFiles(sourceFolder);
            foreach (string file in files)
            {
                string name = Path.GetFileName(file);
                string dest = Path.Combine(destFolder, name);
                File.Copy(file, dest);
            }
            string[] folders = Directory.GetDirectories(sourceFolder);
            foreach (string folder in folders)
            {
                string name = Path.GetFileName(folder);
                string dest = Path.Combine(destFolder, name);
                CopyFolder(folder, dest);
            }
        }
    }

    public class EventArgs<T> : EventArgs
    {
        private readonly T _value;

        public EventArgs(T value)
        {
            this._value = value;
        }

        public T Value
        {
            get { return this._value; }
        }
    }
}