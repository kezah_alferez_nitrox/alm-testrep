using System;
using System.Drawing;
using System.Windows.Forms;

namespace LifeInsurance.Core
{
    public class GridStyler
    {
        private readonly DataGridView grid;
        private static CellStyle styleClipboard;

        public GridStyler(DataGridView grid)
        {
            this.grid = grid;
        }

        public void Apply(CellStyle style)
        {
            if (style.FontStyle != null) this.ChangeFontStyle((FontStyle)style.FontStyle);
            if (style.FontSize != null) this.ChangeFontSize((float)style.FontSize);
            if (style.FontFamily != null) this.ChangeFontFamily(style.FontFamily);
            if (style.ForeColor != null) this.ChangeForegroundColor((Color)style.ForeColor);
            if (style.BackColor != null) this.ChangeBackgroundColor((Color)style.BackColor);
            if (style.DecimalsDelta != null) this.ChangeVisibleDigits((int)style.DecimalsDelta);
        }

        public void Copy()
        {
            if (this.grid == null || this.grid.CurrentCell == null || this.grid.CurrentCell.Style == null) return;

            DataGridViewCellStyle style = this.grid.CurrentCell.Style;
            Font font = style.Font ?? this.grid.Font;

            styleClipboard = new CellStyle
                                 {
                                     FontStyle = font.Style,
                                     FontFamily = font.Name,
                                     FontSize = font.Size,
                                     ForeColor = style.ForeColor,
                                     BackColor = style.BackColor
                                 };
        }

        public void Paste()
        {
            if (styleClipboard != null)
            {
                this.Apply(styleClipboard);
            }
        }

        private void ChangeFontStyle(FontStyle style)
        {
            foreach (DataGridViewCell cell in this.grid.SelectedCells)
            {
                Font font = cell.Style.Font ?? this.grid.Font;
                cell.Style.Font = new Font(font, font.Style ^ style);
            }
        }

        private void ChangeFontSize(float size)
        {
            foreach (DataGridViewCell cell in this.grid.SelectedCells)
            {
                Font font = cell.Style.Font ?? this.grid.Font;
                cell.Style.Font = new Font(font.FontFamily, size, font.Style);
            }
        }

        private void ChangeFontFamily(string name)
        {
            foreach (DataGridViewCell cell in this.grid.SelectedCells)
            {
                Font font = cell.Style.Font ?? this.grid.Font;
                cell.Style.Font = new Font(name, font.Size, font.Style);
            }
        }

        private void ChangeForegroundColor(Color color)
        {
            foreach (DataGridViewCell cell in this.grid.SelectedCells)
            {
                cell.Style.ForeColor = color;
            }
        }

        private void ChangeBackgroundColor(Color color)
        {
            foreach (DataGridViewCell cell in this.grid.SelectedCells)
            {
                cell.Style.BackColor = color;
            }
        }

        private void ChangeVisibleDigits(int digitsDelta)
        {
            foreach (DataGridViewCell cell in this.grid.SelectedCells)
            {
                string oldFormat = cell.Style.Format;

                int digits = 0;
                if (oldFormat != null && oldFormat.StartsWith("N") && oldFormat.Length == 2)
                {
                    digits = int.Parse(oldFormat[1].ToString());
                }

                digits += digitsDelta;
                if (digits < 0 || digits > 9) continue;

                cell.Style.Format = "N" + digits;
            }
        }
    }
}