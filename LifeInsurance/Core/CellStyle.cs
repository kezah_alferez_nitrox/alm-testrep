using System.Drawing;
namespace LifeInsurance.Core
{
    public class CellStyle
    {
        public string FontFamily { get; set; }
        public float? FontSize { get; set; }
        public FontStyle? FontStyle { get; set; }
        public Color? ForeColor { get; set; }
        public Color? BackColor { get; set; }
        public int? DecimalsDelta { get; set; }
    }
}