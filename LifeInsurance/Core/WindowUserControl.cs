﻿using System;
using System.Drawing;
using System.Windows.Forms;
using LifeInsurance.Data;

namespace LifeInsurance.Core
{
    public class WindowUserControl : UserControl
    {
        private bool resizeing;
        private const int BottonPadding = 5;

        private readonly IPanelConfigRepository panelConfigRepository = new PanelConfigRepository();
        private readonly PanelConfig config;

        public bool IsActive { get; set; }

        public WindowUserControl()
            : this("")
        {
        }

        public WindowUserControl(string id)
        {
            this.config = this.panelConfigRepository.Load(id);
            if (this.config == null)
            {
                this.config = this.config = new PanelConfig(id, -1);
                panelConfigRepository.Save(this.config);
            }

            this.MinimumSize = new Size(this.MinimumSize.Width, 50);
            this.Padding = new Padding(this.Padding.Left, this.Padding.Top, this.Padding.Right, BottonPadding);
        }

        protected override void OnMouseDown(MouseEventArgs e)
        {
            base.OnMouseDown(e);

            if (this.InsideResizeZone(e.Location))
            {
                this.resizeing = true;
                this.Capture = true;
            }
        }

        protected override void OnMouseLeave(EventArgs e)
        {
            base.OnMouseLeave(e);

            this.Cursor = Cursors.Default;
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);

            if (this.resizeing)
            {
                this.Height = e.Location.Y;
            }
            else
            {
                this.Cursor = this.InsideResizeZone(e.Location) ? Cursors.SizeNS : Cursors.Default;
            }
        }

        protected override void OnMouseUp(MouseEventArgs e)
        {
            base.OnMouseUp(e);

            if (this.resizeing)
            {
                this.resizeing = false;
                this.Capture = false;

                this.SaveConfig();
            }
        }

        private void SaveConfig()
        {
            this.config.Height = this.Height;
            panelConfigRepository.Backup(null);
        }

        private bool InsideResizeZone(Point location)
        {
            return location.Y >= this.Height - BottonPadding && location.Y <= this.Height;
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            if (config.Height > 0) this.Height = config.Height;
        }        
    }
}