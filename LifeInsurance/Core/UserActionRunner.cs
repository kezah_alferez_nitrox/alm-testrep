using System;
using System.Windows.Forms;
using LifeInsurance.Data;
using LifeInsurance.Events;

namespace LifeInsurance.Core
{
    public class UserActionRunner
    {
        private readonly IUserSettingRepository _userSettingsRepository = new UserSettingRepository();

        public void RunAction(string action)
        {
            UpdateViewStatus(action);

            UserSetting userSetting = this._userSettingsRepository.Load(action);
            if (userSetting == null)
            {
                return;
            }

            if (string.IsNullOrEmpty(userSetting.MacroName) || string.IsNullOrEmpty(userSetting.Address))
            {
                return;
            }

            try
            {
                ExcelUtil.RunMacro(userSetting.Address, userSetting.MacroName);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error encountered while running macro " + userSetting.MacroName + "\n\n" + ex);
                Logger.Log(ex);
            }
        }

        private static void UpdateViewStatus(string action)
        {
            if (action == UserActions.CreateClient || action == UserActions.UploadClient)
            {
                ViewsStatus.SetDirty(ViewType.Inputs);
                ViewsStatus.SetDirty(ViewType.Result);
                ViewsStatus.SetDirty(ViewType.UserData);
            }
            else if (action == UserActions.LaunchDeterministic || action == UserActions.LaunchStochastic)
            {
                ViewsStatus.SetDirty(ViewType.Result);
                EventPublisher.Publish(new ResultsChanged(), null);
            }
            else if (action == UserActions.SaveClient)
            {
                ViewsStatus.SetDirty(ViewType.Result);
            }
        }
    }
}