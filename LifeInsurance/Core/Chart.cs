using System.Drawing;

namespace LifeInsurance.Core
{
    public class Chart
    {
        public string Key { get; set; }
        public string Name { get; set; }
        public Image Image { get; set; }

        public Chart(string key, string name, Image image)
        {
            this.Key = key;
            this.Name = name;
            this.Image = image;
        }
    }
}