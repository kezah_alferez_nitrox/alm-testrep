using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using Microsoft.Office.Interop.Excel;
using Application = System.Windows.Forms.Application;
using ExcelChart = Microsoft.Office.Interop.Excel.Chart;
using System.IO;

namespace LifeInsurance.Core
{
    public class ExcelUtil
    {
        public static void RunMacro(string excelFileName, string macroName)
        {
            ApplicationClass app = null;
            Workbook workBook = null;

            try
            {
                app = new ApplicationClass();
                app.Visible = false;

                workBook = app.Workbooks.Open(excelFileName,
                                              0,
                                              false,
                                              5,
                                              "",
                                              "",
                                              true,
                                              XlPlatform.xlWindows,
                                              "\t",
                                              false,
                                              false,
                                              0,
                                              true,
                                              1,
                                              0);

                object run = app.Run(macroName, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                                     Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                                     Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                                     Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                                     Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                                     Type.Missing);
            }
            finally
            {
                if (null != workBook)
                {
                    workBook.Close(true, Type.Missing, Type.Missing);
                    Marshal.ReleaseComObject(workBook);
                }
                if (null != app)
                {
                    app.Quit();
                    Marshal.ReleaseComObject(app);
                }
            }
        }

        public static IList<Chart> GetCharts(string excelFileName)
        {
            IList<Chart> images = new List<Chart>();

            ApplicationClass app = null;
            Workbook workBook = null;

            try
            {
                app = new ApplicationClass();
                app.Visible = false;

                workBook = app.Workbooks.Open(excelFileName,
                                              0,
                                              true,
                                              5,
                                              "",
                                              "",
                                              true,
                                              XlPlatform.xlWindows,
                                              "\t",
                                              false,
                                              false,
                                              0,
                                              true,
                                              1,
                                              0);

                for (int i = 0; i < workBook.Charts.Count; i++)
                {
                    ExcelChart chart = (ExcelChart)workBook.Charts[i + 1];
                    AddChartToImageList(excelFileName, chart, images);
                }

                for (int i = 0; i < workBook.Worksheets.Count; i++)
                {
                    Worksheet sheet = null;
                    ChartObjects chartObjects = null;

                    try
                    {
                        sheet = (Worksheet)(workBook.Worksheets[i + 1]);

                        for (int j = 0; j < sheet.Shapes.Count; j++)
                        {
                            Shape shape = sheet.Shapes.Item(j + 1);
                            AddShapeToImageList(excelFileName, shape, images);
                        }

                        //chartObjects = (ChartObjects)(sheet.ChartObjects(Type.Missing));
                        //for (int j = 0; j < chartObjects.Count; j++)
                        //{
                        //    ChartObject chartObject = (ChartObject)(chartObjects.Item(j + 1));
                        //    AddChartToImageList(excelFileName, chartObject, images);
                        //}
                    }
                    finally
                    {
                        if (chartObjects != null)
                        {
                            Marshal.ReleaseComObject(chartObjects);
                        }
                        if (sheet != null)
                        {
                            Marshal.ReleaseComObject(sheet);
                        }
                    }
                }
            }
            finally
            {
                if (null != workBook)
                {
                    workBook.Close(false, Type.Missing, Type.Missing);
                    Marshal.ReleaseComObject(workBook);
                }
                if (null != app)
                {
                    app.Quit();
                    Marshal.ReleaseComObject(app);
                }
            }

            return images;
        }

        private static void AddShapeToImageList(string excelFileName, Shape shape, IList<Chart> images)
        {
            shape.CopyPicture(XlPictureAppearance.xlScreen, XlCopyPictureFormat.xlPicture);
            Image image = GetClipboardMetafileAsImage();

            images.Add(new Chart(excelFileName + shape.Name, shape.Name, image));
        }

        private static void AddChartToImageList(string excelFileName, ExcelChart chart, IList<Chart> images)
        {
            chart.ChartArea.Copy();
            Image image = GetClipboardMetafileAsImage();

            string title = GetChartTitle(chart);
            images.Add(new Chart(excelFileName + title, title, image));
        }

        private static void AddChartToImageList(string excelFileName, ChartObject chart, IList<Chart> images)
        {
            chart.CopyPicture(XlPictureAppearance.xlScreen, XlCopyPictureFormat.xlPicture);

            Image image = GetClipboardMetafileAsImage();

            if (image != null)
            {
                string title = GetChartTitle(chart);
                images.Add(new Chart(excelFileName + title, title, image));
            }
        }

        private static Image GetClipboardMetafileAsImage()
        {
            try
            {
                ClipboardHelper.OpenClipboard(Application.OpenForms[0].Handle);

                if (ClipboardHelper.IsClipboardFormatAvailable(ClipboardHelper.CF_ENHMETAFILE))
                {
                    IntPtr ptr = ClipboardHelper.GetClipboardData(ClipboardHelper.CF_ENHMETAFILE);
                    if (ptr.ToInt32() != 0)
                    {
                        using (Metafile metafile = new Metafile(ptr, true))
                        using (MemoryStream stream = new MemoryStream())
                        {
                            metafile.Save(stream, ImageFormat.Png);
                            stream.Seek(0, SeekOrigin.Begin);
                            Image image = Image.FromStream(stream);
                            return image;
                        }
                    }
                }

            }
            finally
            {
                ClipboardHelper.CloseClipboard();
            }

            return null;
        }

        private static string GetChartTitle(ChartObject chartObject)
        {
            string title = chartObject.Name;

            if (chartObject.Chart.HasTitle)
            {
                title = chartObject.Chart.ChartTitle.Text;
            }

            return title;
        }

        private static string GetChartTitle(ExcelChart chart)
        {
            string title = chart.Name;

            if (chart.HasTitle)
            {
                title = chart.ChartTitle.Text;
            }

            return title;
        }
    }
}