using ALMSCommon.Data;

namespace LifeInsurance.Core
{
    public interface IChartConfigRepository : IRepository<string>, IRestorable
    {
    }
}