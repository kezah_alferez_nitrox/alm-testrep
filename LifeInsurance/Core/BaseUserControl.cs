﻿using System.Windows.Forms;

namespace LifeInsurance.Core
{
    public partial class BaseUserControl : UserControl
    {
        private readonly ViewType _viewType;

        public BaseUserControl(ViewType viewType)
        {
            _viewType = viewType;
            this.InitializeComponent();
        }

        public BaseUserControl() : this(ViewType.None) { }

        public void Reload()
        {
            if (ViewsStatus.NeedsReloading(this._viewType))
            {
                this.ReloadInternal();
                ViewsStatus.SetClean(this._viewType);
            }
        }

        protected virtual void ReloadInternal()
        {
        }
    }
}