using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Text;
using System.Windows.Forms;
using LifeInsurance.Core;
using LifeInsurance.Data;
using LifeInsurance.Events;

namespace LifeInsurance
{
    public partial class MainForm : Form
    {
        private Color currentCellForeground = Color.Black;
        private Color currentCellBackground = Color.White;

        public MainForm()
        {
            this.InitializeComponent();

            this.Text = Constants.APPLICATION_NAME;

            this.SetupFonts();
            this.UpdateForegroundIcon();
            this.UpdateBackgroundIcon();
        }

        private void SetupFonts()
        {
            InstalledFontCollection fontCollection = new InstalledFontCollection();
            foreach (FontFamily fontFamily in fontCollection.Families)
            {
                this.fontNameToolStripComboBox.Items.Add(fontFamily.Name);
            }
        }

        private void UpdateForegroundIcon()
        {
            SetToolbarItemColor(this.cellForegroundToolStripSplitButton, this.currentCellForeground);
        }

        private void UpdateBackgroundIcon()
        {
            SetToolbarItemColor(this.cellBackgroundToolStripSplitButton, this.currentCellBackground);
        }

        private static void SetToolbarItemColor(ToolStripItem toolStripSplitButton, Color color)
        {
            Bitmap bitmap = new Bitmap(16, 16, PixelFormat.Format32bppArgb);
            using (Graphics graphics = Graphics.FromImage(bitmap))
            {
                graphics.FillRectangle(new SolidBrush(color), 0, 0, 16, 16);
            }
            toolStripSplitButton.Image = bitmap;
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void pasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.ClipboardPaste();
        }

        private void pasteToolStripButton_Click(object sender, EventArgs e)
        {
            this.ClipboardPaste();
        }

        private void ClipboardPaste()
        {
            if (this.mainTabControl.SelectedTab == this.inputsTabPage)
            {
                this.inputsUserControl.ClipboardPaste();
            }
        }

        private void mainTabControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.ReloadActiveTab(this.mainTabControl);
        }

        private void ReloadActiveTab(TabControl tabControl)
        {
            if (tabControl.SelectedTab == null || tabControl.SelectedTab.Controls.Count == 0) return;

            Control firstControl = tabControl.SelectedTab.Controls[0];

            if (firstControl is TabControl)
            {
                this.ReloadActiveTab((TabControl)firstControl);
            }
            else if (firstControl is BaseUserControl)
            {
                ((BaseUserControl)firstControl).Reload();
            }
        }

        private void launchDeterministicToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RunMacro(UserActions.LaunchDeterministic);
        }

        private static void RunMacro(string actionName)
        {
            (new UserActionRunner()).RunAction(actionName);
        }

        private void launchStochasticToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RunMacro(UserActions.LaunchStochastic);
        }

        private void fontBoldToolStripButton_Click(object sender, EventArgs e)
        {
            EventPublisher.Publish(new CellStyleChanged { FontStyle = FontStyle.Bold }, this);
        }

        private void fontItalicsToolStripButton_Click(object sender, EventArgs e)
        {
            EventPublisher.Publish(new CellStyleChanged { FontStyle = FontStyle.Italic }, this);
        }

        private void fontSizeToolStripComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            int size = Int32.Parse((string)this.fontSizeToolStripComboBox.SelectedItem);
            EventPublisher.Publish(new CellStyleChanged { FontSize = size }, this);
        }

        private void fontNameToolStripComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            EventPublisher.Publish(
                new CellStyleChanged { FontFamily = (string)this.fontNameToolStripComboBox.SelectedItem }, this);
        }

        private void cellForegroundToolStripSplitButton_ButtonClick(object sender, EventArgs e)
        {
            EventPublisher.Publish(new CellStyleChanged { ForeColor = this.currentCellForeground }, this);
        }

        private void cellBackgroundToolStripSplitButton_ButtonClick(object sender, EventArgs e)
        {
            EventPublisher.Publish(new CellStyleChanged { BackColor = this.currentCellBackground }, this);
        }

        private void copyStyleToolStripButton_Click(object sender, EventArgs e)
        {
            EventPublisher.Publish(new CopyCellStyle(), this);
        }

        private void pasteStyleToolStripButton_Click(object sender, EventArgs e)
        {
            EventPublisher.Publish(new PasteCellStyle(), this);
        }

        private void cellBackgroundToolStripSplitButton_DropDownOpening(object sender, EventArgs e)
        {
            this.colorDialog.Color = this.currentCellBackground;
            if (this.colorDialog.ShowDialog(this) == DialogResult.OK)
            {
                this.currentCellBackground = this.colorDialog.Color;
                this.UpdateBackgroundIcon();

                EventPublisher.Publish(new CellStyleChanged { BackColor = this.currentCellBackground }, this);
            }
        }

        private void cellForegroundToolStripSplitButton_DropDownOpening(object sender, EventArgs e)
        {
            this.colorDialog.Color = this.currentCellForeground;
            if (this.colorDialog.ShowDialog(this) == DialogResult.OK)
            {
                this.currentCellForeground = this.colorDialog.Color;
                this.UpdateForegroundIcon();

                EventPublisher.Publish(new CellStyleChanged { ForeColor = this.currentCellForeground }, this);
            }
        }

        private void increaseDecimalsToolStripButton_Click(object sender, EventArgs e)
        {
            EventPublisher.Publish(new CellStyleChanged { DecimalsDelta = +1 }, this);
        }

        private void decreaseDecimalsToolStripButton_Click(object sender, EventArgs e)
        {
            EventPublisher.Publish(new CellStyleChanged { DecimalsDelta = -1 }, this);
        }
    }
}