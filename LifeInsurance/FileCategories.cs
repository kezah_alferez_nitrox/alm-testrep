using System.Collections.Generic;

namespace LifeInsurance
{
    public static class FileCategories
    {
        public static readonly IList<string> Portfolios = new List<string> { "AccountingDataInForce", "Assets", "LiabilityProducts" };
        public static IList<string> PortfoliosWithSummary = new List<string> { "Summary", "AccountingDataInForce", "Assets", "LiabilityProducts" };

        public static readonly IList<string> Assumptions = new List<string> { "AssetAssumptions", "LiabilityAssumptions" };

        public static readonly IList<string> Market = new List<string> { "DeterministicMarket", "StochasticMarket" };

        public static readonly IList<string> Result = new List<string> { "Result" };    
    }
}