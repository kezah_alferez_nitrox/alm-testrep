using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using LifeInsurance.Core;
using LifeInsurance.Data;
using LifeInsurance.Events;

namespace LifeInsurance.Views.Result
{
    public partial class GridPanel : WindowUserControl
    {
        private readonly InputFile _file;
        public event EventHandler<EventArgs<InputFile>> Closed = delegate { };
        private readonly GridStyler gridStyler;
        private bool loading = false;

        public GridPanel(InputFile file)
            : base("grid" + file.FullName)
        {
            this._file = file;
            this.InitializeComponent();
            this.grid.ReadOnly = true;

            this.gridStyler = new GridStyler(this.grid);
            EventPublisher.Register<CellStyleChanged>(style =>
                                                          {
                                                              if (!this.IsActive) return;
                                                              this.gridStyler.Apply(style);
                                                              this.GridStyleChanged();
                                                          });
            EventPublisher.Register<CopyCellStyle>(style =>
                                                       {
                                                           if (!this.IsActive) return;
                                                           this.gridStyler.Copy();
                                                       });
            EventPublisher.Register<PasteCellStyle>(style =>
                                                        {
                                                            if (!this.IsActive) return;
                                                            this.gridStyler.Paste();
                                                            this.GridStyleChanged();
                                                        });
        }

        public InputFile InputFile
        {
            get { return this._file; }
        }

        private void GridPanel_Load(object sender, EventArgs e)
        {
            this.LoadFile();

            //Size preferredSize = this.grid.GetPreferredSize(this.Size);

            //if (preferredSize.Height + this.topPanel.Height < this.Height)
            //{
            //    this.Height = preferredSize.Height + this.topPanel.Height;
            //}
        }

        private void LoadFile()
        {
            this.titleLabel.Text = Path.GetFileName(this.InputFile.FullName);
            this.loading = true;
            GridUtil.DisplayFileAsGrid(this.InputFile.FullName, this.grid);
            this.loading = false;
        }

        private void bClose_Click(object sender, EventArgs e)
        {
            this.Parent.Controls.Remove(this);
            this.Closed(this, new EventArgs<InputFile>(this.InputFile));
        }

        private void GridStyleChanged()
        {
            if (loading) return;

            string fileName = this.InputFile.FullName + ".style";
            GridUtil.SaveGridFormattingAsFile(fileName, this.grid);
        }

        private void grid_ColumnWidthChanged(object sender, DataGridViewColumnEventArgs e)
        {
            this.GridStyleChanged();
        }

        private void grid_RowHeightChanged(object sender, DataGridViewRowEventArgs e)
        {
            this.GridStyleChanged();
        }

        public void Reload()
        {
            this.LoadFile();
        }
    }
}