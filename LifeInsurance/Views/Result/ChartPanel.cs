using System;
using System.Windows.Forms;
using LifeInsurance.Core;

namespace LifeInsurance.Views.Result
{
    public partial class ChartPanel : WindowUserControl
    {
        public event EventHandler<EventArgs<Chart>> Closed = delegate { };

        private Chart _chart;

        public ChartPanel(Chart chart)
            : base("chart" + chart.Key + chart.Name)
        {
            this._chart = chart;
            this.InitializeComponent();
        }

        public Chart Chart
        {
            get { return this._chart; }
        }

        private void ChartPanel_Load(object sender, EventArgs e)
        {
            this.LoadChart();
        }

        private void LoadChart()
        {
            this.titleLabel.Text = this.Chart.Name;

            this.pictureBox.Width = this.Chart.Image.Width;
            this.pictureBox.Height = this.Chart.Image.Height;
            this.pictureBox.Image = this.Chart.Image;

            this.Height = this.topPanel.Height + this.Chart.Image.Height + this.Padding.Bottom;

            this.pictureBox.BorderStyle = BorderStyle.FixedSingle;
        }

        private void bClose_Click(object sender, EventArgs e)
        {
            this.Parent.Controls.Remove(this);
            this.Closed(this, new EventArgs<Chart>(this.Chart));
        }

        private void ChartPanel_Resize(object sender, EventArgs e)
        {
            this.pictureBox.Left = (this.Width - this.pictureBox.Width)/2;
        }

        public void Reload(Chart chart)
        {
            this._chart = chart;
            this.LoadChart();
        }
    }
}