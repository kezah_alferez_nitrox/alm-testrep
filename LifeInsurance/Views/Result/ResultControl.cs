﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using LifeInsurance.Core;
using LifeInsurance.Data;
using LifeInsurance.Events;

namespace LifeInsurance.Views.Result
{
    public partial class ResultControl : BaseUserControl
    {
        private readonly IChartConfigRepository _chartConfigRepository = new ChartConfigRepository();

        private readonly IDictionary<int, ChartPanel> _chartPanels = new Dictionary<int, ChartPanel>();
        private readonly IList<InputFile> _selectedFiles = new List<InputFile>();
        private GridPanel activeGridPanel = null;

        public ResultControl()
            : base(ViewType.Result)
        {
            this.InitializeComponent();

            EventPublisher.Register<ResultsChanged>(ResultsChanged);
        }

        private void ResultsChanged(ResultsChanged obj)
        {
            this.Reload();
        }

        protected override void ReloadInternal()
        {
            this.LoadChartTreeView();
            this.LoadDataTreeView();

            this.RefreshPanels();
        }

        public void RefreshPanels()
        {
            foreach (Control control in this.containerPanel.Controls)
            {
                TryRefreshGridPanel(control as GridPanel);
                TryRefreshChartPanel(control as ChartPanel);
            }
        }

        private void TryRefreshChartPanel(ChartPanel panel)
        {
            if(panel == null) return;

            string key = panel.Chart.Key;
            Chart tag = FindTagInTreeView<Chart>(this.chartTreeView, i => i.Key == key);

            if (tag == null)
            {
                this.ClosePanel(panel);
            }
            else
            {
                panel.Reload(tag);
            }
        }

        private void TryRefreshGridPanel(GridPanel panel)
        {
            if (panel == null) return;

            string fileName = panel.InputFile.FullName;
            InputFile tag = FindTagInTreeView<InputFile>(this.dataTreeView, i => i.FullName == fileName);

            if (tag == null)
            {
                this.ClosePanel(panel);
            }
            else
            {
                panel.Reload();
            }
        }

        private void ClosePanel(Control panel)
        {
            this.containerPanel.Controls.Remove(panel);
            panel.Dispose();
        }

        private static T FindTagInTreeView<T>(TreeView treeView, Func<T, bool> criteria) where T : class
        {
            foreach (TreeNode node in TreeViewUtils.GetAllNodes(treeView))
            {
                T tag = node.Tag as T;
                if (tag != null && criteria(tag)) return tag;
            }
            return null;
        }

        private void LoadChartTreeView()
        {
            try
            {
                this.chartTreeView.BeginUpdate();
                this.chartTreeView.Nodes.Clear();

                if (this._chartConfigRepository.Count == 0) return;

                TreeNode rootNode = this.chartTreeView.Nodes.Add("Charts");
                rootNode.ImageIndex = rootNode.SelectedImageIndex = 0;

                foreach (string fileName in this._chartConfigRepository.GetAll())
                {
                    TreeNode fileNode = rootNode.Nodes.Add(Path.GetFileName(fileName));
                    fileNode.ImageIndex = fileNode.SelectedImageIndex = 1;

                    IList<Chart> images = ExcelUtil.GetCharts(fileName);
                    foreach (Chart chart in images)
                    {
                        TreeNode chartNode = fileNode.Nodes.Add(chart.Name);
                        chartNode.ImageIndex = chartNode.SelectedImageIndex = 2;
                        chartNode.Tag = chart;
                    }
                }

                rootNode.ExpandAll();
            }
            finally
            {
                this.chartTreeView.EndUpdate();
            }
        }

        private void LoadDataTreeView()
        {
            this._selectedFiles.Clear();

            this.dataTreeView.Nodes.Clear();

            if (string.IsNullOrEmpty(Client.Path)) return;

            DirectoryInfo info = new DirectoryInfo(Client.Path);
            TreeViewUtils.AddFilesToTreeView(info, this.dataTreeView.Nodes, FileCategories.Result, 0);

            this.dataTreeView.ExpandAll();
            if (this.dataTreeView.Nodes.Count > 0)
            {
                this.dataTreeView.Nodes[0].EnsureVisible();
            }
        }

        private void GridPanelClosed(object sender, EventArgs<InputFile> e)
        {
            this._selectedFiles.Remove(e.Value);
        }

        private void ChartPanelClosed(object sender, EventArgs<Chart> e)
        {
            int key = e.Value.Image.GetHashCode();
            this._chartPanels.Remove(key);
        }

        private void DataTreeViewMouseDoubleClick(object sender, MouseEventArgs e)
        {
            TreeNode selectedNode = this.dataTreeView.SelectedNode;

            if (selectedNode != null && selectedNode.Tag is InputFile)
            {
                InputFile selectedFile = selectedNode.Tag as InputFile;

                if (!this._selectedFiles.Contains(selectedFile))
                {
                    GridPanel panel = new GridPanel(selectedFile);
                    panel.Enter += this.GridPanelEnter;
                    panel.Dock = DockStyle.Top;
                    panel.Closed += this.GridPanelClosed;
                    this.containerPanel.Controls.Add(panel);

                    this._selectedFiles.Add(selectedFile);
                }
            }
        }

        private void GridPanelEnter(object sender, EventArgs e)
        {
            this.activeGridPanel = (GridPanel)sender;

            foreach (Control control in this.containerPanel.Controls)
            {
                WindowUserControl userControl = control as WindowUserControl;
                if (userControl == null) continue;

                userControl.IsActive = false;
            }

            this.activeGridPanel.IsActive = true;
        }

        private void ChartTreeViewMouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (this.chartTreeView.SelectedNode == null) return;

            Chart chart = this.chartTreeView.SelectedNode.Tag as Chart;
            if (chart == null || chart.Image == null) return;

            int key = chart.Image.GetHashCode();

            if (!this._chartPanels.ContainsKey(key))
            {
                ChartPanel chartPanel = new ChartPanel(chart);
                chartPanel.Dock = DockStyle.Top;
                chartPanel.Closed += this.ChartPanelClosed;
                this.containerPanel.Controls.Add(chartPanel);

                this._chartPanels.Add(key, chartPanel);
            }
        }
    }
}