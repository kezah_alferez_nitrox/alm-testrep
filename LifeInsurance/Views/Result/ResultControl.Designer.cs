﻿namespace LifeInsurance.Views.Result
{
    partial class ResultControl
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ResultControl));
            this.dataTreeView = new System.Windows.Forms.TreeView();
            this.dataTreeIconsImageList = new System.Windows.Forms.ImageList(this.components);
            this.containerPanel = new System.Windows.Forms.Panel();
            this.mainSplitContainer = new System.Windows.Forms.SplitContainer();
            this.leftSplitContainer = new System.Windows.Forms.SplitContainer();
            this.chartTreeView = new System.Windows.Forms.TreeView();
            this.chartTreeImageList = new System.Windows.Forms.ImageList(this.components);
            this.mainSplitContainer.Panel1.SuspendLayout();
            this.mainSplitContainer.Panel2.SuspendLayout();
            this.mainSplitContainer.SuspendLayout();
            this.leftSplitContainer.Panel1.SuspendLayout();
            this.leftSplitContainer.Panel2.SuspendLayout();
            this.leftSplitContainer.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataTreeView
            // 
            this.dataTreeView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataTreeView.ImageIndex = 0;
            this.dataTreeView.ImageList = this.dataTreeIconsImageList;
            this.dataTreeView.Location = new System.Drawing.Point(0, 0);
            this.dataTreeView.Name = "dataTreeView";
            this.dataTreeView.SelectedImageIndex = 0;
            this.dataTreeView.Size = new System.Drawing.Size(271, 245);
            this.dataTreeView.TabIndex = 0;
            this.dataTreeView.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.DataTreeViewMouseDoubleClick);
            // 
            // dataTreeIconsImageList
            // 
            this.dataTreeIconsImageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("dataTreeIconsImageList.ImageStream")));
            this.dataTreeIconsImageList.TransparentColor = System.Drawing.Color.Black;
            this.dataTreeIconsImageList.Images.SetKeyName(0, "openfolderHS.bmp");
            this.dataTreeIconsImageList.Images.SetKeyName(1, "TableHS.bmp");
            // 
            // containerPanel
            // 
            this.containerPanel.AutoScroll = true;
            this.containerPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.containerPanel.Location = new System.Drawing.Point(0, 0);
            this.containerPanel.Name = "containerPanel";
            this.containerPanel.Size = new System.Drawing.Size(791, 515);
            this.containerPanel.TabIndex = 0;
            // 
            // mainSplitContainer
            // 
            this.mainSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainSplitContainer.Location = new System.Drawing.Point(0, 0);
            this.mainSplitContainer.Name = "mainSplitContainer";
            // 
            // mainSplitContainer.Panel1
            // 
            this.mainSplitContainer.Panel1.Controls.Add(this.leftSplitContainer);
            // 
            // mainSplitContainer.Panel2
            // 
            this.mainSplitContainer.Panel2.Controls.Add(this.containerPanel);
            this.mainSplitContainer.Size = new System.Drawing.Size(1066, 515);
            this.mainSplitContainer.SplitterDistance = 271;
            this.mainSplitContainer.TabIndex = 2;
            // 
            // leftSplitContainer
            // 
            this.leftSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.leftSplitContainer.Location = new System.Drawing.Point(0, 0);
            this.leftSplitContainer.Name = "leftSplitContainer";
            this.leftSplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // leftSplitContainer.Panel1
            // 
            this.leftSplitContainer.Panel1.Controls.Add(this.dataTreeView);
            // 
            // leftSplitContainer.Panel2
            // 
            this.leftSplitContainer.Panel2.Controls.Add(this.chartTreeView);
            this.leftSplitContainer.Size = new System.Drawing.Size(271, 515);
            this.leftSplitContainer.SplitterDistance = 245;
            this.leftSplitContainer.TabIndex = 0;
            // 
            // chartTreeView
            // 
            this.chartTreeView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartTreeView.ImageIndex = 0;
            this.chartTreeView.ImageList = this.chartTreeImageList;
            this.chartTreeView.Location = new System.Drawing.Point(0, 0);
            this.chartTreeView.Name = "chartTreeView";
            this.chartTreeView.SelectedImageIndex = 0;
            this.chartTreeView.Size = new System.Drawing.Size(271, 266);
            this.chartTreeView.TabIndex = 1;
            this.chartTreeView.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.ChartTreeViewMouseDoubleClick);
            // 
            // chartTreeImageList
            // 
            this.chartTreeImageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("chartTreeImageList.ImageStream")));
            this.chartTreeImageList.TransparentColor = System.Drawing.Color.Black;
            this.chartTreeImageList.Images.SetKeyName(0, "Book_openHS.png");
            this.chartTreeImageList.Images.SetKeyName(1, "DocumentHS.png");
            this.chartTreeImageList.Images.SetKeyName(2, "graphhs.png");
            // 
            // ResultControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.mainSplitContainer);
            this.Name = "ResultControl";
            this.Size = new System.Drawing.Size(1066, 515);
            this.mainSplitContainer.Panel1.ResumeLayout(false);
            this.mainSplitContainer.Panel2.ResumeLayout(false);
            this.mainSplitContainer.ResumeLayout(false);
            this.leftSplitContainer.Panel1.ResumeLayout(false);
            this.leftSplitContainer.Panel2.ResumeLayout(false);
            this.leftSplitContainer.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TreeView dataTreeView;
        private System.Windows.Forms.ImageList dataTreeIconsImageList;
        private System.Windows.Forms.Panel containerPanel;
        private System.Windows.Forms.SplitContainer mainSplitContainer;
        private System.Windows.Forms.SplitContainer leftSplitContainer;
        private System.Windows.Forms.TreeView chartTreeView;
        private System.Windows.Forms.ImageList chartTreeImageList;
    }
}
