﻿using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using LifeInsurance.Core;
using LifeInsurance.Data;

namespace LifeInsurance.Views.Setup
{
    public partial class FileSelectionUserControl : UserControl
    {
        private readonly SelectedFilesRepository _selectedFilesRepository = new SelectedFilesRepository();

        public IList<string> Filter { get; set; }

        public FileSelectionUserControl()
        {
            InitializeComponent();

            selectedFilesTreeView.Font = new Font(this.Font.FontFamily, 8.25f, FontStyle.Regular);
            allFilesTreeView.Font = new Font(this.Font.FontFamily, 8.25f, FontStyle.Regular);

            Filter = new List<string>();
        }

        public void LoadWithFilter(IList<string> filter)
        {
            Filter = filter;

            try
            {
                this.allFilesTreeView.BeginUpdate();
                this.selectedFilesTreeView.EndUpdate();

                this.allFilesTreeView.Nodes.Clear();

                DirectoryInfo info = new DirectoryInfo(Client.Path);
                TreeViewUtils.AddFilesToTreeView(info, this.allFilesTreeView.Nodes, this.Filter, 0);

                this.allFilesTreeView.ExpandAll();
                if (this.allFilesTreeView.Nodes.Count > 0)
                {
                    this.allFilesTreeView.Nodes[0].EnsureVisible();
                }


                this.selectedFilesTreeView.Nodes.Clear();
                foreach (InputFile selectedFile in _selectedFilesRepository.GetAll())
                {
                    TreeViewUtils.AddFileToFileTree(this.selectedFilesTreeView, selectedFile.FullName, this.Filter);
                }

                this.selectedFilesTreeView.ExpandAll();
            }
            finally
            {
                this.allFilesTreeView.EndUpdate();
                this.selectedFilesTreeView.EndUpdate();
            }

            ViewsStatus.SetDirty(ViewType.Inputs);
        }

        private void allFilesTreeView_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (this.allFilesTreeView.SelectedNode != null && this.allFilesTreeView.SelectedNode.Tag is InputFile)
            {
                InputFile inputFile = this.allFilesTreeView.SelectedNode.Tag as InputFile;

                TreeViewUtils.AddFileToFileTree(this.selectedFilesTreeView, inputFile.FullName, this.Filter);
                this._selectedFilesRepository.Save(inputFile);

                ViewsStatus.SetDirty(ViewType.Inputs);
            }
        }

        private void selectedFilesTreeView_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (this.selectedFilesTreeView.SelectedNode != null && this.selectedFilesTreeView.SelectedNode.Tag is InputFile)
            {
                this._selectedFilesRepository.Delete(this.selectedFilesTreeView.SelectedNode.Tag as InputFile);
                this.selectedFilesTreeView.SelectedNode.Remove();

                ViewsStatus.SetDirty(ViewType.Inputs);
            }
        }

        private void allFilesTreeView_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Right) return;

            TreeNode node = this.allFilesTreeView.GetNodeAt(e.Location);
            if (null != node)
            {
                this.allFilesTreeView.SelectedNode = node;
            }
        }

        private void deleteFileToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            if (this.allFilesTreeView.SelectedNode == null || this.allFilesTreeView.SelectedNode.Tag == null) return;

            InputFile inputFile = this.allFilesTreeView.SelectedNode.Tag as InputFile;
            if (inputFile == null) return;

            File.Delete(inputFile.FullName);
            this.allFilesTreeView.SelectedNode.Remove();
        }

        private void addFileToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            if (this.allFilesTreeView.SelectedNode == null || this.allFilesTreeView.SelectedNode.Tag == null) return;

            DirectoryInfo directoryInfo = this.allFilesTreeView.SelectedNode.Tag as DirectoryInfo;
            if (directoryInfo == null) return;

            TreeNode node = this.allFilesTreeView.SelectedNode.Nodes.Add("newFile.txt");
            node.ImageIndex = node.SelectedImageIndex = 1;
            node.BeginEdit();
        }

        private void treeContextMenuStrip_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (this.allFilesTreeView.SelectedNode == null || this.allFilesTreeView.SelectedNode.Tag == null) return;

            InputFile inputFile = this.allFilesTreeView.SelectedNode.Tag as InputFile;
            renameFileToolStripMenuItem.Enabled = deleteFileToolStripMenuItem.Enabled = inputFile != null && _selectedFilesRepository.Load(inputFile.Id) == null;

            addFileToolStripMenuItem.Enabled = this.allFilesTreeView.SelectedNode.Tag is DirectoryInfo;
        }

        private void allFilesTreeView_AfterLabelEdit(object sender, NodeLabelEditEventArgs e)
        {
            if (e.Node.Parent == null) return;
            DirectoryInfo directoryInfo = e.Node.Parent.Tag as DirectoryInfo;
            if (directoryInfo == null) return;

            string oldFileName = Path.Combine(directoryInfo.FullName, e.Node.Text);
            string newFileName = oldFileName;
            if (e.Label == null)
            {
                if (File.Exists(oldFileName))
                {
                    CancelEdit(e);
                    return;
                }
            }
            else
            {
                newFileName = Path.Combine(directoryInfo.FullName, e.Label);
                if (File.Exists(newFileName))
                {
                    CancelEdit(e);
                    return;
                }
            }

            try
            {
                if (File.Exists(oldFileName))
                {
                    File.Move(oldFileName, newFileName);
                }
                else
                {
                    File.Create(newFileName).Dispose();
                }
                e.Node.Tag = new InputFile(newFileName);
            }catch
            {
                CancelEdit(e);
            }
        }

        private static void CancelEdit(NodeLabelEditEventArgs e)
        {
            if(e.Node.Tag == null)
            {
                e.Node.Remove();
            }else
            {
                e.CancelEdit = true;
            }
        }

        private void renameFileToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            if (this.allFilesTreeView.SelectedNode == null || this.allFilesTreeView.SelectedNode.Tag == null) return;
            if(!(this.allFilesTreeView.SelectedNode.Tag is InputFile)) return;
            
            this.allFilesTreeView.SelectedNode.BeginEdit();
        }
    }
}
