﻿namespace LifeInsurance.Views.Setup
{
    partial class FileSelectionUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FileSelectionUserControl));
            this.mainSplitContainer = new System.Windows.Forms.SplitContainer();
            this.allFilesTreeView = new System.Windows.Forms.TreeView();
            this.treeContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.deleteFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.renameFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.treeIconsImageList = new System.Windows.Forms.ImageList(this.components);
            this.selectedFilesTreeView = new System.Windows.Forms.TreeView();
            this.mainSplitContainer.Panel1.SuspendLayout();
            this.mainSplitContainer.Panel2.SuspendLayout();
            this.mainSplitContainer.SuspendLayout();
            this.treeContextMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainSplitContainer
            // 
            this.mainSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainSplitContainer.Location = new System.Drawing.Point(0, 0);
            this.mainSplitContainer.Name = "mainSplitContainer";
            // 
            // mainSplitContainer.Panel1
            // 
            this.mainSplitContainer.Panel1.Controls.Add(this.allFilesTreeView);
            // 
            // mainSplitContainer.Panel2
            // 
            this.mainSplitContainer.Panel2.Controls.Add(this.selectedFilesTreeView);
            this.mainSplitContainer.Size = new System.Drawing.Size(1033, 241);
            this.mainSplitContainer.SplitterDistance = 492;
            this.mainSplitContainer.TabIndex = 1;
            // 
            // allFilesTreeView
            // 
            this.allFilesTreeView.ContextMenuStrip = this.treeContextMenuStrip;
            this.allFilesTreeView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.allFilesTreeView.ImageIndex = 0;
            this.allFilesTreeView.ImageList = this.treeIconsImageList;
            this.allFilesTreeView.LabelEdit = true;
            this.allFilesTreeView.Location = new System.Drawing.Point(0, 0);
            this.allFilesTreeView.Name = "allFilesTreeView";
            this.allFilesTreeView.SelectedImageIndex = 0;
            this.allFilesTreeView.Size = new System.Drawing.Size(492, 241);
            this.allFilesTreeView.TabIndex = 1;
            this.allFilesTreeView.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.allFilesTreeView_MouseDoubleClick);
            this.allFilesTreeView.MouseClick += new System.Windows.Forms.MouseEventHandler(this.allFilesTreeView_MouseClick);
            this.allFilesTreeView.AfterLabelEdit += new System.Windows.Forms.NodeLabelEditEventHandler(this.allFilesTreeView_AfterLabelEdit);
            // 
            // treeContextMenuStrip
            // 
            this.treeContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deleteFileToolStripMenuItem,
            this.addFileToolStripMenuItem,
            this.renameFileToolStripMenuItem});
            this.treeContextMenuStrip.Name = "fileContextMenuStrip";
            this.treeContextMenuStrip.Size = new System.Drawing.Size(139, 70);
            this.treeContextMenuStrip.Opening += new System.ComponentModel.CancelEventHandler(this.treeContextMenuStrip_Opening);
            // 
            // deleteFileToolStripMenuItem
            // 
            this.deleteFileToolStripMenuItem.Name = "deleteFileToolStripMenuItem";
            this.deleteFileToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.deleteFileToolStripMenuItem.Text = "&Delete File";
            this.deleteFileToolStripMenuItem.Click += new System.EventHandler(this.deleteFileToolStripMenuItem_Click);
            // 
            // addFileToolStripMenuItem
            // 
            this.addFileToolStripMenuItem.Name = "addFileToolStripMenuItem";
            this.addFileToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.addFileToolStripMenuItem.Text = "&Add File";
            this.addFileToolStripMenuItem.Click += new System.EventHandler(this.addFileToolStripMenuItem_Click);
            // 
            // renameFileToolStripMenuItem
            // 
            this.renameFileToolStripMenuItem.Name = "renameFileToolStripMenuItem";
            this.renameFileToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.renameFileToolStripMenuItem.Text = "&Rename File";
            this.renameFileToolStripMenuItem.Click += new System.EventHandler(this.renameFileToolStripMenuItem_Click);
            // 
            // treeIconsImageList
            // 
            this.treeIconsImageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("treeIconsImageList.ImageStream")));
            this.treeIconsImageList.TransparentColor = System.Drawing.Color.Black;
            this.treeIconsImageList.Images.SetKeyName(0, "openfolderHS.bmp");
            this.treeIconsImageList.Images.SetKeyName(1, "TableHS.bmp");
            // 
            // selectedFilesTreeView
            // 
            this.selectedFilesTreeView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.selectedFilesTreeView.ImageIndex = 0;
            this.selectedFilesTreeView.ImageList = this.treeIconsImageList;
            this.selectedFilesTreeView.Location = new System.Drawing.Point(0, 0);
            this.selectedFilesTreeView.Name = "selectedFilesTreeView";
            this.selectedFilesTreeView.SelectedImageIndex = 0;
            this.selectedFilesTreeView.Size = new System.Drawing.Size(537, 241);
            this.selectedFilesTreeView.TabIndex = 1;
            this.selectedFilesTreeView.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.selectedFilesTreeView_MouseDoubleClick);
            // 
            // FileSelectionUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.mainSplitContainer);
            this.Name = "FileSelectionUserControl";
            this.Size = new System.Drawing.Size(1033, 241);
            this.mainSplitContainer.Panel1.ResumeLayout(false);
            this.mainSplitContainer.Panel2.ResumeLayout(false);
            this.mainSplitContainer.ResumeLayout(false);
            this.treeContextMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer mainSplitContainer;
        private System.Windows.Forms.TreeView allFilesTreeView;
        private System.Windows.Forms.TreeView selectedFilesTreeView;
        private System.Windows.Forms.ImageList treeIconsImageList;
        private System.Windows.Forms.ContextMenuStrip treeContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem deleteFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem renameFileToolStripMenuItem;
    }
}
