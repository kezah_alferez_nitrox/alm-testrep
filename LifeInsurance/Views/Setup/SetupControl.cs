using System;
using System.IO;
using System.Windows.Forms;
using ALMSCommon;
using LifeInsurance.Controls;
using LifeInsurance.Core;
using LifeInsurance.Data;

namespace LifeInsurance.Views.Setup
{
    public partial class SetupControl : UserControl
    {
        private readonly ISelectedFilesRepository _selectedFilesRepository = new SelectedFilesRepository();
        private readonly IUserSettingRepository _userSettingRepository = new UserSettingRepository();
        private readonly IChartConfigRepository _chartConfigRepository = new ChartConfigRepository();
        private readonly IPanelConfigRepository _panelConfigRepository = new PanelConfigRepository();

        private const string ExistingRuns = "existingruns";

        private string _selectedRunFileName;

        public SetupControl()
        {
            this.InitializeComponent();

#if DEBUG
            if (!this.DesignMode)
            {
                this.LoadClient(@"..\Client1");
            }
#endif
        }

        private void LoadClient(string path)
        {
            path = Path.GetFullPath(path);

            Client.Path = path;

            this.clientNameTextBox.Text = Path.GetFileNameWithoutExtension(path);
            this.clientPathTextBox.Text = Path.GetDirectoryName(path);

            this._selectedFilesRepository.DeleteAll();
            this.selectedRunTextBox.Text = this._selectedRunFileName = "";

            try
            {
                this.LoadRuns();
                this.LoadFiles();

                _userSettingRepository.Restore(null);
                _chartConfigRepository.Restore(null);
                _panelConfigRepository.Restore(null);
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, "Error loading client: " + ex.Message);
            }
        }

        private void LoadRuns()
        {
            string runsPath = Path.Combine(Client.Path, ExistingRuns);
            FileInfo[] runFiles = (new DirectoryInfo(runsPath)).GetFiles();

            this.runsListBox.Items.Clear();
            foreach (FileInfo info in runFiles)
            {
                this.runsListBox.Items.Add(info.Name);
            }

            if (this._selectedRunFileName != null)
            {
                this.runsListBox.SelectedItem = this._selectedRunFileName;
            }
        }

        private void LoadFiles()
        {
            this.portfoliosSelection.LoadWithFilter(FileCategories.Portfolios);
            this.assumptionsSelection.LoadWithFilter(FileCategories.Assumptions);
            this.marketSelection.LoadWithFilter(FileCategories.Market);
        }

        private void runsListBox_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (this.runsListBox.SelectedItem != null)
            {
                this._selectedRunFileName = (string)this.runsListBox.SelectedItem;
                this.selectedRunTextBox.Text = this._selectedRunFileName;

                this.LoadRun();
            }
        }

        private void LoadRun()
        {
            string fullRunFilePath = GetFullRunFilePath(this._selectedRunFileName);
            if (!File.Exists(fullRunFilePath))
            {
                return;
            }

            this._selectedFilesRepository.Restore(fullRunFilePath);

            this.LoadFiles();
        }

        private void uploadClientButton_Click(object sender, EventArgs e)
        {
            using (FolderBrowserDialogEx folderBrowserDialog = new FolderBrowserDialogEx())
            {
                if (string.IsNullOrEmpty(folderBrowserDialog.SelectedPath))
                {
                    folderBrowserDialog.SelectedPath = Util.GetApplicationDirectory();
                }
                if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
                {
                    this.LoadClient(folderBrowserDialog.SelectedPath);

                    (new UserActionRunner()).RunAction(UserActions.UploadClient);
                }
            }
        }

        private void createClientButton_Click(object sender, EventArgs e)
        {
            using (FolderBrowserDialogEx folderBrowserDialog = new FolderBrowserDialogEx())
            {
                if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
                {
                    this.CreateClient(folderBrowserDialog.SelectedPath);

                    (new UserActionRunner()).RunAction(UserActions.CreateClient);
                }
            }
        }

        private void CreateClient(string path)
        {
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            CompressionTools.UncompressZip(Path.Combine(Util.GetApplicationDirectory(), @"Content\ClientTemplate.zip"),
                                           path);
            this.LoadClient(path);
        }

        private void duplicateClientButton_Click(object sender, EventArgs e)
        {
            using (FolderBrowserDialogEx folderBrowserDialog = new FolderBrowserDialogEx())
            {
                if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
                {
                    this.DuplicateClient(folderBrowserDialog.SelectedPath);
                    (new UserActionRunner()).RunAction(UserActions.UploadClient);
                }
            }
        }

        private void DuplicateClient(string path)
        {
            Util.CopyFolder(Client.Path, path);
            this.LoadClient(path);
        }

        private void saveRunButton_Click(object sender, EventArgs e)
        {
            this.SaveRun();
        }

        private void SaveRun()
        {
            string runFile = this.selectedRunTextBox.Text;

            if (string.IsNullOrEmpty(runFile))
            {
                MessageBox.Show(this, "Please choose a run file name", "Save Run", MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
                return;
            }

            runFile = GetFullRunFilePath(runFile);

            if ((this.selectedRunTextBox.Text != this._selectedRunFileName) && File.Exists(runFile))
            {
                if (
                    MessageBox.Show(this, "Override " + runFile + " ?", "Save Run", MessageBoxButtons.YesNo,
                                    MessageBoxIcon.Question) == DialogResult.No)
                {
                    return;
                }
            }

            this._selectedFilesRepository.Backup(runFile);
            this._selectedRunFileName = this.selectedRunTextBox.Text;

            this.LoadRuns();
        }


        private static string GetFullRunFilePath(string runFile)
        {
            return Path.Combine(Path.Combine(Client.Path, ExistingRuns), runFile);
        }
    }
}