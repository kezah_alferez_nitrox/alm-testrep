namespace LifeInsurance.Views.Setup
{
    partial class SetupControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SetupControl));
            this.fileNameColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.topPanel = new System.Windows.Forms.Panel();
            this.duplicateClientButton = new System.Windows.Forms.Button();
            this.saveRunButton = new System.Windows.Forms.Button();
            this.selectedRunTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.runsListBox = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.createClientButton = new System.Windows.Forms.Button();
            this.uploadClientButton = new System.Windows.Forms.Button();
            this.clientPathTextBox = new System.Windows.Forms.TextBox();
            this.clientNameTextBox = new System.Windows.Forms.TextBox();
            this.centerPanel = new System.Windows.Forms.Panel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.portfoliosSelection = new LifeInsurance.Views.Setup.FileSelectionUserControl();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.assumptionsSelection = new LifeInsurance.Views.Setup.FileSelectionUserControl();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.marketSelection = new LifeInsurance.Views.Setup.FileSelectionUserControl();
            this.bottomPanel = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.topPanel.SuspendLayout();
            this.centerPanel.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.bottomPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // fileNameColumn
            // 
            this.fileNameColumn.HeaderText = "File";
            this.fileNameColumn.Name = "fileNameColumn";
            this.fileNameColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.fileNameColumn.Width = 200;
            // 
            // topPanel
            // 
            this.topPanel.Controls.Add(this.duplicateClientButton);
            this.topPanel.Controls.Add(this.saveRunButton);
            this.topPanel.Controls.Add(this.selectedRunTextBox);
            this.topPanel.Controls.Add(this.label2);
            this.topPanel.Controls.Add(this.runsListBox);
            this.topPanel.Controls.Add(this.label1);
            this.topPanel.Controls.Add(this.createClientButton);
            this.topPanel.Controls.Add(this.uploadClientButton);
            this.topPanel.Controls.Add(this.clientPathTextBox);
            this.topPanel.Controls.Add(this.clientNameTextBox);
            this.topPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.topPanel.Location = new System.Drawing.Point(0, 0);
            this.topPanel.Name = "topPanel";
            this.topPanel.Size = new System.Drawing.Size(1024, 100);
            this.topPanel.TabIndex = 1;
            // 
            // duplicateClientButton
            // 
            this.duplicateClientButton.Location = new System.Drawing.Point(815, 3);
            this.duplicateClientButton.Name = "duplicateClientButton";
            this.duplicateClientButton.Size = new System.Drawing.Size(100, 23);
            this.duplicateClientButton.TabIndex = 9;
            this.duplicateClientButton.Text = "Duplicate Client";
            this.duplicateClientButton.UseVisualStyleBackColor = true;
            this.duplicateClientButton.Click += new System.EventHandler(this.duplicateClientButton_Click);
            // 
            // saveRunButton
            // 
            this.saveRunButton.Location = new System.Drawing.Point(603, 33);
            this.saveRunButton.Name = "saveRunButton";
            this.saveRunButton.Size = new System.Drawing.Size(100, 23);
            this.saveRunButton.TabIndex = 8;
            this.saveRunButton.Text = "Save Run File";
            this.saveRunButton.UseVisualStyleBackColor = true;
            this.saveRunButton.Click += new System.EventHandler(this.saveRunButton_Click);
            // 
            // selectedRunTextBox
            // 
            this.selectedRunTextBox.Location = new System.Drawing.Point(436, 35);
            this.selectedRunTextBox.Name = "selectedRunTextBox";
            this.selectedRunTextBox.Size = new System.Drawing.Size(161, 20);
            this.selectedRunTextBox.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(358, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Selected Run";
            // 
            // runsListBox
            // 
            this.runsListBox.FormattingEnabled = true;
            this.runsListBox.Location = new System.Drawing.Point(160, 38);
            this.runsListBox.Name = "runsListBox";
            this.runsListBox.Size = new System.Drawing.Size(161, 56);
            this.runsListBox.TabIndex = 5;
            this.runsListBox.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.runsListBox_MouseDoubleClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Existing Runs";
            // 
            // createClientButton
            // 
            this.createClientButton.Location = new System.Drawing.Point(709, 3);
            this.createClientButton.Name = "createClientButton";
            this.createClientButton.Size = new System.Drawing.Size(100, 23);
            this.createClientButton.TabIndex = 3;
            this.createClientButton.Text = "Create Client";
            this.createClientButton.UseVisualStyleBackColor = true;
            this.createClientButton.Click += new System.EventHandler(this.createClientButton_Click);
            // 
            // uploadClientButton
            // 
            this.uploadClientButton.Location = new System.Drawing.Point(603, 3);
            this.uploadClientButton.Name = "uploadClientButton";
            this.uploadClientButton.Size = new System.Drawing.Size(100, 23);
            this.uploadClientButton.TabIndex = 2;
            this.uploadClientButton.Text = "Upload Client";
            this.uploadClientButton.UseVisualStyleBackColor = true;
            this.uploadClientButton.Click += new System.EventHandler(this.uploadClientButton_Click);
            // 
            // clientPathTextBox
            // 
            this.clientPathTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clientPathTextBox.Location = new System.Drawing.Point(160, 3);
            this.clientPathTextBox.Name = "clientPathTextBox";
            this.clientPathTextBox.ReadOnly = true;
            this.clientPathTextBox.Size = new System.Drawing.Size(437, 20);
            this.clientPathTextBox.TabIndex = 1;
            // 
            // clientNameTextBox
            // 
            this.clientNameTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clientNameTextBox.Location = new System.Drawing.Point(4, 3);
            this.clientNameTextBox.Name = "clientNameTextBox";
            this.clientNameTextBox.ReadOnly = true;
            this.clientNameTextBox.Size = new System.Drawing.Size(150, 20);
            this.clientNameTextBox.TabIndex = 0;
            // 
            // centerPanel
            // 
            this.centerPanel.Controls.Add(this.splitContainer1);
            this.centerPanel.Controls.Add(this.bottomPanel);
            this.centerPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.centerPanel.Location = new System.Drawing.Point(0, 100);
            this.centerPanel.Name = "centerPanel";
            this.centerPanel.Size = new System.Drawing.Size(1024, 394);
            this.centerPanel.TabIndex = 2;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.groupBox1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(1024, 352);
            this.splitContainer1.SplitterDistance = 104;
            this.splitContainer1.TabIndex = 1;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.portfoliosSelection);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1024, 104);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Portfolios";
            // 
            // portfoliosSelection
            // 
            this.portfoliosSelection.Dock = System.Windows.Forms.DockStyle.Fill;
            this.portfoliosSelection.Filter = ((System.Collections.Generic.IList<string>)(resources.GetObject("portfoliosSelection.Filter")));
            this.portfoliosSelection.Location = new System.Drawing.Point(3, 18);
            this.portfoliosSelection.Name = "portfoliosSelection";
            this.portfoliosSelection.Size = new System.Drawing.Size(1018, 83);
            this.portfoliosSelection.TabIndex = 0;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.groupBox2);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.groupBox3);
            this.splitContainer2.Size = new System.Drawing.Size(1024, 244);
            this.splitContainer2.SplitterDistance = 124;
            this.splitContainer2.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.assumptionsSelection);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1024, 124);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Portfolio Assumptions";
            // 
            // assumptionsSelection
            // 
            this.assumptionsSelection.Dock = System.Windows.Forms.DockStyle.Fill;
            this.assumptionsSelection.Filter = null;
            this.assumptionsSelection.Location = new System.Drawing.Point(3, 18);
            this.assumptionsSelection.Name = "assumptionsSelection";
            this.assumptionsSelection.Size = new System.Drawing.Size(1018, 103);
            this.assumptionsSelection.TabIndex = 1;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.marketSelection);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(0, 0);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(1024, 116);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Market";
            // 
            // marketSelection
            // 
            this.marketSelection.Dock = System.Windows.Forms.DockStyle.Fill;
            this.marketSelection.Filter = ((System.Collections.Generic.IList<string>)(resources.GetObject("marketSelection.Filter")));
            this.marketSelection.Location = new System.Drawing.Point(3, 18);
            this.marketSelection.Name = "marketSelection";
            this.marketSelection.Size = new System.Drawing.Size(1018, 95);
            this.marketSelection.TabIndex = 1;
            // 
            // bottomPanel
            // 
            this.bottomPanel.Controls.Add(this.button1);
            this.bottomPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bottomPanel.Location = new System.Drawing.Point(0, 352);
            this.bottomPanel.Name = "bottomPanel";
            this.bottomPanel.Size = new System.Drawing.Size(1024, 42);
            this.bottomPanel.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Location = new System.Drawing.Point(917, 10);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(100, 23);
            this.button1.TabIndex = 9;
            this.button1.Text = "Save Input Files";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            // 
            // SetupControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.centerPanel);
            this.Controls.Add(this.topPanel);
            this.Name = "SetupControl";
            this.Size = new System.Drawing.Size(1024, 494);
            this.topPanel.ResumeLayout(false);
            this.topPanel.PerformLayout();
            this.centerPanel.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.bottomPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel topPanel;
        private System.Windows.Forms.Panel centerPanel;
        private System.Windows.Forms.ListBox runsListBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button createClientButton;
        private System.Windows.Forms.Button uploadClientButton;
        private System.Windows.Forms.TextBox clientPathTextBox;
        private System.Windows.Forms.TextBox clientNameTextBox;
        private System.Windows.Forms.TextBox selectedRunTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button saveRunButton;
        private System.Windows.Forms.DataGridViewTextBoxColumn fileNameColumn;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Panel bottomPanel;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private FileSelectionUserControl portfoliosSelection;
        private FileSelectionUserControl assumptionsSelection;
        private FileSelectionUserControl marketSelection;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button duplicateClientButton;

    }
}
