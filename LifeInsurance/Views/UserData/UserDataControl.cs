﻿using LifeInsurance.Core;

namespace LifeInsurance.Views.UserData
{
    public partial class UserDataControl : BaseUserControl
    {
        public UserDataControl() : base(ViewType.UserData)
        {
            this.InitializeComponent();
        }

        protected override void ReloadInternal()
        {
            actionConfigControl.Reload();
            chartConfigControl.Reload();
        }
    }
}