﻿using System.Drawing;
using System.Windows.Forms;
using LifeInsurance.Core;
using LifeInsurance.Data;

namespace LifeInsurance.Views.UserData
{
    public partial class ActionConfigControl : UserControl
    {
        private const string BrowseColumn = "browseColumn";

        private readonly IUserSettingRepository _userSettingsRepository = new UserSettingRepository();

        public ActionConfigControl()
        {
            this.InitializeComponent();

            this.grid.EditMode = DataGridViewEditMode.EditOnEnter;
        }

        private void InitNodes()
        {
            this.AddReadOnlyRow("RunSetup");
            this.AddUserSettingNode("  Upload Client", UserActions.UploadClient);
            this.AddUserSettingNode("  Create Client", UserActions.CreateClient);
            this.AddUserSettingNode("  Save Client", UserActions.SaveClient);
            this.AddUserSettingNode("  Save Input Files", UserActions.SaveInputFiles);

            this.AddReadOnlyRow("Inputs");

            this.AddReadOnlyRow("  Portfolios");
            this.AddUserSettingNode("    Save Inputs", UserActions.PortfoliosSaveInputs);

            this.AddReadOnlyRow("  Portfolio Assumptions");
            this.AddUserSettingNode("  Save Inputs",
                                    UserActions.PortfolioAssumptionsSaveInputs);

            this.AddReadOnlyRow("Markets");
            this.AddUserSettingNode("  Save Inputs", UserActions.MarketsSaveInputs);

            this.AddUserSettingNode("Launch Deterministic", UserActions.LaunchDeterministic);
            this.AddUserSettingNode("Launch Stochastic", UserActions.LaunchStochastic);
        }

        private void AddReadOnlyRow(string label)
        {
            int index = this.grid.Rows.Add(new object[] { label });

            this.grid.Rows[index].ReadOnly = true;
            this.grid.Rows[index].DefaultCellStyle.BackColor =
            this.grid.Rows[index].DefaultCellStyle.SelectionBackColor = Color.Cornsilk;
            this.grid.Rows[index].DefaultCellStyle.ForeColor =
                this.grid.Rows[index].DefaultCellStyle.SelectionForeColor = Color.Black;
        }

        private void AddUserSettingNode(string label, string key)
        {
            UserSetting userSetting = this._userSettingsRepository.Load(key);
            if (userSetting == null)
            {
                userSetting = new UserSetting(key);
                this._userSettingsRepository.Save(userSetting);
            }

            int index = this.grid.Rows.Add(new object[] { label, userSetting.MacroName, userSetting.Address });
            this.grid.Rows[index].Tag = userSetting;
        }

        private void SaveUserData()
        {
            this._userSettingsRepository.Backup(null);
        }

        public void Reload()
        {
            if (string.IsNullOrEmpty(Client.Path))
            {
                this.grid.Enabled = false;
                return;
            }

            this.grid.Enabled = true;

            this.InitNodes();
        }

        private void tree_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (this.grid.Columns[e.ColumnIndex].Name == BrowseColumn)
            {
                DataGridViewRow node = this.grid.Rows[e.RowIndex];
                if (node.ReadOnly) return;

                this.openFileDialog.FileName = (string)node.Cells[2].Value;
                if (this.openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    node.Cells[2].Value = this.openFileDialog.FileName;

                    this.SaveUserData();
                }
            }
        }

        private void tree_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (e.RowIndex == -1) return;

            DataGridViewRow node = this.grid.Rows[e.RowIndex];
            if (node.ReadOnly && this.grid.Columns[e.ColumnIndex].Name == BrowseColumn)
            {
                e.PaintBackground(e.ClipBounds, false);
                e.Handled = true;
            }
        }

        private void treeGrid_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1 || e.ColumnIndex != 1 && e.ColumnIndex != 2) return;

            DataGridViewRow node = this.grid.Rows[e.RowIndex];

            var userSetting = node.Tag as UserSetting;
            if (userSetting == null) return;

            if (e.ColumnIndex == 1)
            {
                userSetting.MacroName = (string)this.grid[e.ColumnIndex, e.RowIndex].Value;
            }
            else if (e.ColumnIndex == 2)
            {
                userSetting.Address = (string)this.grid[e.ColumnIndex, e.RowIndex].Value;
            }

            this.SaveUserData();
        }
    }
}