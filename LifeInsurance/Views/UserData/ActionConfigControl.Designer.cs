﻿namespace LifeInsurance.Views.UserData
{
    partial class ActionConfigControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grid = new System.Windows.Forms.DataGridView();
            this.buttonColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.macroColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.addressColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.browseColumn = new System.Windows.Forms.DataGridViewButtonColumn();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.grid)).BeginInit();
            this.SuspendLayout();
            // 
            // grid
            // 
            this.grid.AllowUserToAddRows = false;
            this.grid.AllowUserToDeleteRows = false;
            this.grid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.grid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.buttonColumn,
            this.macroColumn,
            this.addressColumn,
            this.browseColumn});
            this.grid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grid.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.grid.Location = new System.Drawing.Point(0, 0);
            this.grid.Name = "grid";
            this.grid.RowHeadersVisible = false;
            this.grid.Size = new System.Drawing.Size(898, 480);
            this.grid.TabIndex = 0;
            this.grid.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.treeGrid_CellValueChanged);
            this.grid.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.tree_CellPainting);
            this.grid.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.tree_CellContentClick);
            // 
            // buttonColumn
            // 
            this.buttonColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.buttonColumn.HeaderText = "Button";
            this.buttonColumn.Name = "buttonColumn";
            this.buttonColumn.ReadOnly = true;
            this.buttonColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.buttonColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.buttonColumn.Width = 200;
            // 
            // macroColumn
            // 
            this.macroColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.macroColumn.HeaderText = "External Associated Macro Name";
            this.macroColumn.Name = "macroColumn";
            this.macroColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.macroColumn.Width = 200;
            // 
            // addressColumn
            // 
            this.addressColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.addressColumn.HeaderText = "Address";
            this.addressColumn.Name = "addressColumn";
            this.addressColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // browseColumn
            // 
            this.browseColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.browseColumn.HeaderText = "";
            this.browseColumn.MinimumWidth = 28;
            this.browseColumn.Name = "browseColumn";
            this.browseColumn.ReadOnly = true;
            this.browseColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.browseColumn.Text = "...";
            this.browseColumn.UseColumnTextForButtonValue = true;
            this.browseColumn.Width = 28;
            // 
            // openFileDialog
            // 
            this.openFileDialog.Filter = "Excel files|*.xls|All files|*.*";
            // 
            // ActionConfigControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.grid);
            this.Name = "ActionConfigControl";
            this.Size = new System.Drawing.Size(898, 480);
            ((System.ComponentModel.ISupportInitialize)(this.grid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView grid;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.DataGridViewTextBoxColumn buttonColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn macroColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn addressColumn;
        private System.Windows.Forms.DataGridViewButtonColumn browseColumn;
    }
}
