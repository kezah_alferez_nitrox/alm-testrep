﻿using System.Windows.Forms;
using LifeInsurance.Core;
using LifeInsurance.Data;

namespace LifeInsurance.Views.UserData
{
    public partial class ChartConfigControl : UserControl
    {
        private readonly IChartConfigRepository _chartConfigRepository = new ChartConfigRepository();

        public ChartConfigControl()
        {
            this.InitializeComponent();
        }

        public void Reload()
        {
            if (string.IsNullOrEmpty(Client.Path))
            {
                this.grid.Enabled = false;
                return;
            }

            this.grid.Enabled = true;
            this.LoadGrid();
        }

        private void WireGridEvents()
        {
            this.grid.CellValueChanged += this.GridCellValueChanged;
            this.grid.RowsRemoved += this.GridRowsRemoved;
            this.grid.CellContentClick += this.GridCellContentClick;
        }

        private void UnWireGridEvents()
        {
            this.grid.CellValueChanged -= this.GridCellValueChanged;
            this.grid.RowsRemoved -= this.GridRowsRemoved;
            this.grid.CellContentClick -= this.GridCellContentClick;
        }

        private void LoadGrid()
        {
            this.UnWireGridEvents();

            this.grid.Rows.Clear();
            foreach (string line in this._chartConfigRepository.GetAll())
            {
                this.grid.Rows.Add(line);
            }

            this.WireGridEvents();
        }

        private void GridCellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (this.grid.Columns[e.ColumnIndex] == this.browseColumn)
            {
                this.openFileDialog.FileName = this.grid.Rows[e.RowIndex].IsNewRow ? string.Empty : (string)this.grid[e.RowIndex, 0].Value;
                if (this.openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    DataGridViewRow row = this.grid.Rows[e.RowIndex];
                    if (row.IsNewRow)
                    {
                        row = this.grid.Rows[this.grid.Rows.Add()];
                    }
                    row.Cells[0].Value = this.openFileDialog.FileName;

                    this.SaveChartConfig();
                }
            }
        }

        private void SaveChartConfig()
        {
            if (string.IsNullOrEmpty(Client.Path)) return;

            this.SaveGrid();

            this._chartConfigRepository.Backup(null);

            ViewsStatus.SetDirty(ViewType.Result);
        }

        private void SaveGrid()
        {
            this._chartConfigRepository.DeleteAll();
            foreach (DataGridViewRow row in this.grid.Rows)
            {
                string value = (string)row.Cells[0].Value;

                if (!string.IsNullOrEmpty(value)) this._chartConfigRepository.Save(value);
            }
        }

        private void GridRowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            this.SaveChartConfig();
        }

        private void GridCellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            this.SaveChartConfig();
        }
    }
}