﻿namespace LifeInsurance.Views.UserData
{
    partial class ChartConfigControl
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.grid = new System.Windows.Forms.DataGridView();
            this.fileColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.browseColumn = new System.Windows.Forms.DataGridViewButtonColumn();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.grid)).BeginInit();
            this.SuspendLayout();
            // 
            // grid
            // 
            this.grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.fileColumn,
            this.browseColumn});
            this.grid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grid.Location = new System.Drawing.Point(0, 0);
            this.grid.Name = "grid";
            this.grid.Size = new System.Drawing.Size(890, 451);
            this.grid.TabIndex = 0;
            // 
            // fileColumn
            // 
            this.fileColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.fileColumn.HeaderText = "File";
            this.fileColumn.Name = "fileColumn";
            // 
            // browseColumn
            // 
            this.browseColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.browseColumn.HeaderText = "";
            this.browseColumn.Name = "browseColumn";
            this.browseColumn.Text = "...";
            this.browseColumn.UseColumnTextForButtonValue = true;
            this.browseColumn.Width = 50;
            // 
            // openFileDialog
            // 
            this.openFileDialog.Filter = "Excel files|*.xls|All files|*.*";
            // 
            // ChartConfigControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.grid);
            this.Name = "ChartConfigControl";
            this.Size = new System.Drawing.Size(890, 451);
            ((System.ComponentModel.ISupportInitialize)(this.grid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView grid;
        private System.Windows.Forms.DataGridViewTextBoxColumn fileColumn;
        private System.Windows.Forms.DataGridViewButtonColumn browseColumn;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
    }
}
