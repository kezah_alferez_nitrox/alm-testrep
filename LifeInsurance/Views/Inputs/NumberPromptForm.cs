﻿using System.Windows.Forms;

namespace LifeInsurance.Views.Inputs
{
    public partial class NumberPromptForm : Form
    {
        public NumberPromptForm()
        {
            this.InitializeComponent();
        }

        public NumberPromptForm(int value)
            : this()
        {
            this.numberNumericUpDown.Value = value;
        }

        public int Number
        {
            set { this.numberNumericUpDown.Value = value; }
            get { return (int) this.numberNumericUpDown.Value; }
        }
    }
}