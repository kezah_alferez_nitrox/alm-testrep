﻿using LifeInsurance.Core;
using LifeInsurance.Data;
namespace LifeInsurance.Views.Inputs
{
    partial class InputsUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InputsUserControl));
            this.inputsTabControl = new System.Windows.Forms.TabControl();
            this.portfolioTabPage = new System.Windows.Forms.TabPage();
            this.assumptionsTabPage = new System.Windows.Forms.TabPage();
            this.marketTabPage = new System.Windows.Forms.TabPage();
            this.portfoliosInput = new LifeInsurance.Views.Inputs.FilteredInputControl(UserActions.PortfoliosSaveInputs);
            this.assumptionsInput = new LifeInsurance.Views.Inputs.FilteredInputControl(UserActions.PortfolioAssumptionsSaveInputs);
            this.marketInput = new LifeInsurance.Views.Inputs.FilteredInputControl(UserActions.MarketsSaveInputs);
            this.inputsTabControl.SuspendLayout();
            this.portfolioTabPage.SuspendLayout();
            this.assumptionsTabPage.SuspendLayout();
            this.marketTabPage.SuspendLayout();
            this.SuspendLayout();
            // 
            // inputsTabControl
            // 
            this.inputsTabControl.Controls.Add(this.portfolioTabPage);
            this.inputsTabControl.Controls.Add(this.assumptionsTabPage);
            this.inputsTabControl.Controls.Add(this.marketTabPage);
            this.inputsTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.inputsTabControl.Location = new System.Drawing.Point(0, 0);
            this.inputsTabControl.Name = "inputsTabControl";
            this.inputsTabControl.SelectedIndex = 0;
            this.inputsTabControl.Size = new System.Drawing.Size(1065, 497);
            this.inputsTabControl.TabIndex = 2;
            this.inputsTabControl.SelectedIndexChanged += new System.EventHandler(this.inputsTabControl_SelectedIndexChanged);
            // 
            // portfolioTabPage
            // 
            this.portfolioTabPage.Controls.Add(this.portfoliosInput);
            this.portfolioTabPage.Location = new System.Drawing.Point(4, 22);
            this.portfolioTabPage.Name = "portfolioTabPage";
            this.portfolioTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.portfolioTabPage.Size = new System.Drawing.Size(1057, 471);
            this.portfolioTabPage.TabIndex = 0;
            this.portfolioTabPage.Text = "Portfolios";
            this.portfolioTabPage.UseVisualStyleBackColor = true;
            // 
            // portfoliosInput
            // 
            this.portfoliosInput.CurrentColumnIndex = -1;
            this.portfoliosInput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.portfoliosInput.Filter = ((System.Collections.Generic.IList<string>)(resources.GetObject("portfoliosInput.Filter")));
            this.portfoliosInput.Location = new System.Drawing.Point(3, 3);
            this.portfoliosInput.Name = "portfoliosInput";
            this.portfoliosInput.Size = new System.Drawing.Size(1051, 465);
            this.portfoliosInput.TabIndex = 0;
            // 
            // assumptionsTabPage
            // 
            this.assumptionsTabPage.Controls.Add(this.assumptionsInput);
            this.assumptionsTabPage.Location = new System.Drawing.Point(4, 22);
            this.assumptionsTabPage.Name = "assumptionsTabPage";
            this.assumptionsTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.assumptionsTabPage.Size = new System.Drawing.Size(1057, 471);
            this.assumptionsTabPage.TabIndex = 2;
            this.assumptionsTabPage.Text = "Assumptions";
            this.assumptionsTabPage.UseVisualStyleBackColor = true;
            // 
            // assumptionsInput
            // 
            this.assumptionsInput.CurrentColumnIndex = -1;
            this.assumptionsInput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.assumptionsInput.Filter = ((System.Collections.Generic.IList<string>)(resources.GetObject("assumptionsInput.Filter")));
            this.assumptionsInput.Location = new System.Drawing.Point(3, 3);
            this.assumptionsInput.Name = "assumptionsInput";
            this.assumptionsInput.Size = new System.Drawing.Size(1051, 465);
            this.assumptionsInput.TabIndex = 1;
            // 
            // marketTabPage
            // 
            this.marketTabPage.Controls.Add(this.marketInput);
            this.marketTabPage.Location = new System.Drawing.Point(4, 22);
            this.marketTabPage.Name = "marketTabPage";
            this.marketTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.marketTabPage.Size = new System.Drawing.Size(1057, 471);
            this.marketTabPage.TabIndex = 3;
            this.marketTabPage.Text = "Market";
            this.marketTabPage.UseVisualStyleBackColor = true;
            // 
            // marketInput
            // 
            this.marketInput.CurrentColumnIndex = -1;
            this.marketInput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.marketInput.Filter = ((System.Collections.Generic.IList<string>)(resources.GetObject("marketInput.Filter")));
            this.marketInput.Location = new System.Drawing.Point(3, 3);
            this.marketInput.Name = "marketInput";
            this.marketInput.Size = new System.Drawing.Size(1051, 465);
            this.marketInput.TabIndex = 1;
            // 
            // InputsUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.inputsTabControl);
            this.Name = "InputsUserControl";
            this.Size = new System.Drawing.Size(1065, 497);
            this.Load += new System.EventHandler(this.InputsUserControl_Load);
            this.inputsTabControl.ResumeLayout(false);
            this.portfolioTabPage.ResumeLayout(false);
            this.assumptionsTabPage.ResumeLayout(false);
            this.marketTabPage.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl inputsTabControl;
        private System.Windows.Forms.TabPage portfolioTabPage;
        private FilteredInputControl portfoliosInput;
        private System.Windows.Forms.TabPage assumptionsTabPage;
        private System.Windows.Forms.TabPage marketTabPage;
        private FilteredInputControl assumptionsInput;
        private FilteredInputControl marketInput;
    }
}
