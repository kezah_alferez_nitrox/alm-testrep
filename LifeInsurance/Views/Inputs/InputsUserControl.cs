﻿using System;
using LifeInsurance.Core;
using System.Drawing;

namespace LifeInsurance.Views.Inputs
{
    public partial class InputsUserControl : BaseUserControl
    {
        private FilteredInputControl currentInputControl;

        public InputsUserControl() : base(ViewType.Inputs)
        {
            this.InitializeComponent();

            currentInputControl = this.portfoliosInput;
        }

        private void InputsUserControl_Load(object sender, EventArgs e)
        {
            this.portfoliosInput.LoadWithFilter(FileCategories.PortfoliosWithSummary);
            this.assumptionsInput.LoadWithFilter(FileCategories.Assumptions);
            this.marketInput.LoadWithFilter(FileCategories.Market);
        }

        protected override void ReloadInternal()
        {
            this.portfoliosInput.Reload();
            this.assumptionsInput.Reload();
            this.marketInput.Reload();
        }

        private void inputsTabControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.inputsTabControl.SelectedTab == this.portfolioTabPage)
            {
                currentInputControl = this.portfoliosInput;
            }
            else if (this.inputsTabControl.SelectedTab == this.assumptionsTabPage)
            {
                currentInputControl = this.assumptionsInput;
            }
            else if (this.inputsTabControl.SelectedTab == this.marketTabPage)
            {
                currentInputControl = this.marketInput;
            }
        }

        public void ClipboardPaste()
        {
            currentInputControl.ClipboardPaste();
        }
    }
}