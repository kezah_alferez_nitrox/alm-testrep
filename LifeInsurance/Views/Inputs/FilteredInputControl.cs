using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using LifeInsurance.Core;
using LifeInsurance.Data;

namespace LifeInsurance.Views.Inputs
{
    public partial class FilteredInputControl : UserControl
    {
        private readonly SelectedFilesRepository _selectedFilesRepository = new SelectedFilesRepository();

        private readonly IList<InputFile> _selectedFiles = new List<InputFile>();

        private InputPanelControl _activeInputPanelControl = null;

        private int _currentColumnIndex = -1;
        private readonly string _saveUserAction;

        public IList<string> Filter { get; set; }

        public int CurrentColumnIndex
        {
            get { return this._currentColumnIndex; }
            set { this._currentColumnIndex = value; }
        }

        public FilteredInputControl(string saveUserAction)
        {
            this.InitializeComponent();

            this.Filter = new List<string>();
            this._saveUserAction = saveUserAction;
        }

        public void LoadWithFilter(IList<string> filter)
        {
            this.Filter = filter;

            this.RefreshTreeView();
        }

        private void RefreshTreeView()
        {
            this.treeView.Nodes.Clear();

            if (Client.Path == null) return;

            foreach (string filter in this.Filter)
            {
                this.treeView.Nodes.Add(filter);
            }

            foreach (InputFile inputFile in this._selectedFilesRepository.GetAll())
            {
                if (this.ShouldLoad(inputFile.FullName))
                {
                    TreeViewUtils.AddFileToFileTree(this.treeView, inputFile.FullName, this.Filter);
                }
            }
        }

        private bool ShouldLoad(string name)
        {
            foreach (string filter in this.Filter)
            {
                if (name.Contains(filter)) return true;
            }

            return false;
        }

        void PanelEnter(object sender, EventArgs e)
        {
            _activeInputPanelControl = (InputPanelControl)sender;

            foreach (Control control in this.containerPanel.Controls)
            {
                WindowUserControl userControl = control as WindowUserControl;
                if (userControl == null) continue;

                userControl.IsActive = false;
            }

            _activeInputPanelControl.IsActive = true;
        }

        private void InputPanelClosed(object sender, EventArgs<InputFile> e)
        {
            if (_activeInputPanelControl == sender) _activeInputPanelControl = null;

            this._selectedFiles.Remove(e.Value);
        }

        public void Reload()
        {
            this.RefreshTreeView();

            this.containerPanel.Controls.Clear();
        }

        public void ClipboardPaste()
        {
            if (_activeInputPanelControl != null && _activeInputPanelControl.Visible)
            {
                _activeInputPanelControl.ClipboardPaste();
            }
        }

        private void treeView_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            TreeNode selectedNode = treeView.SelectedNode;

            if (selectedNode != null && selectedNode.Tag is InputFile)
            {
                InputFile file = selectedNode.Tag as InputFile;

                if (!this._selectedFiles.Contains(file))
                {
                    InputPanelControl panel = new InputPanelControl(file, _saveUserAction);
                    panel.Dock = DockStyle.Top;
                    panel.Closed += this.InputPanelClosed;
                    panel.Enter += this.PanelEnter;
                    this.containerPanel.Controls.Add(panel);

                    this._selectedFiles.Add(file);
                }
            }
        }
    }
}