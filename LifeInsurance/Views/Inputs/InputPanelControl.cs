﻿using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using LifeInsurance.Core;
using LifeInsurance.Data;
using LifeInsurance.Events;
using LifeInsurance.Properties;

namespace LifeInsurance.Views.Inputs
{
    public partial class InputPanelControl : WindowUserControl
    {
        public event EventHandler<EventArgs<InputFile>> Closed = delegate { };

        private readonly InputFile _inputFile;
        private readonly string _saveUserAction;

        private bool _isDirty;
        private int _currentColumnIndex;

        private readonly GridStyler gridStyler;

        public InputPanelControl(InputFile selectedFile, string saveUserAction)
            : base("input" + selectedFile.FullName)
        {
            this.InitializeComponent();

            this.grid.ColumnHeadersVisible = false;
            this.grid.BackgroundImage = Resources.logo400x400;
            this.grid.AllowUserToOrderColumns = false;

            this._inputFile = selectedFile;
            this._saveUserAction = saveUserAction;

            this.gridStyler = new GridStyler(this.grid);
            EventPublisher.Register<CellStyleChanged>(style =>
                                                          {
                                                              if (!this.IsActive) return;
                                                              this.gridStyler.Apply(style);
                                                              this.GridStyleChanged();
                                                          });
            EventPublisher.Register<CopyCellStyle>(style =>
                                                       {
                                                           if (!this.IsActive) return;
                                                           this.gridStyler.Copy();
                                                       });
            EventPublisher.Register<PasteCellStyle>(style =>
                                                       {
                                                           if (!this.IsActive) return;
                                                           this.gridStyler.Paste();
                                                           this.GridStyleChanged();
                                                       });
        }

        public bool IsDirty
        {
            get { return this._isDirty; }
        }

        public InputFile InputFile
        {
            get { return this._inputFile; }
        }

        public void ClipboardPaste()
        {
            if (this._inputFile == null) return;

            string clipboardText = Clipboard.GetText();

            string[][] clipboardData = GridUtil.SplitClipboardContentTo2DArray(clipboardText);
            if (clipboardData == null || clipboardData.Length == 0) return;

            this.grid.EndEdit();

            int startRow = this.grid.CurrentCell == null ? 0 : this.grid.CurrentCell.RowIndex;
            int startColumn = this.grid.CurrentCell == null ? 0 : this.grid.CurrentCell.ColumnIndex;

            GridUtil.EnsureGridHasSpaceToPaste(this.grid, startRow, startColumn, clipboardData);

            for (int row = 0; row < clipboardData.Length; row++)
            {
                for (int col = 0; col < clipboardData[row].Length; col++)
                {
                    this.grid[startColumn + col, startRow + row].Value = clipboardData[row][col];
                }
            }

            this.grid.AutoResizeColumns();

            this._isDirty = true;
        }

        private void bClose_Click(object sender, EventArgs e)
        {
            this.AskForSave();

            this.Parent.Controls.Remove(this);
            this.Closed(this, new EventArgs<InputFile>(this._inputFile));
        }

        private void AskForSave()
        {
            if (this._isDirty && MessageBox.Show(this,
                                                 string.Format("Save {0} before closing ?",
                                                               Path.GetFileName(this._inputFile.FullName)),
                                                 "Save File", MessageBoxButtons.YesNo,
                                                 MessageBoxIcon.Question) == DialogResult.Yes)
            {
                this.Save();
            }
            this._isDirty = false;
        }

        private void InputPanelControl_Load(object sender, EventArgs e)
        {
            this.titleLabel.Text = Path.GetFileName(this._inputFile.FullName);
            GridUtil.DisplayFileAsGrid(this._inputFile.FullName, this.grid);

            Size preferredSize = this.grid.GetPreferredSize(this.Size);

            if (preferredSize.Height + this.headerPanel.Height < this.Height)
            {
                this.Height = preferredSize.Height + this.headerPanel.Height;
            }
        }

        private void InsertColumnBeforeToolStripMenuItemClick(object sender, EventArgs e)
        {
            if (this._currentColumnIndex >= 0 && this.grid.Columns.Count > this._currentColumnIndex)
            {
                this.AddColumn(this._currentColumnIndex);
            }
        }

        private void InsertColumnafterToolStripMenuItemClick(object sender, EventArgs e)
        {
            if (this._currentColumnIndex >= 0 && this.grid.Columns.Count > this._currentColumnIndex)
            {
                this.AddColumn(this._currentColumnIndex + 1);
            }
        }

        private void DeleteToolStripMenuItemClick(object sender, EventArgs e)
        {
            if (this._currentColumnIndex >= 0 && this.grid.Columns.Count > this._currentColumnIndex)
            {
                if (MessageBox.Show(this,
                                    "Do you really want to delete " +
                                    this.grid.Columns[this._currentColumnIndex].HeaderText, "Delete Column",
                                    MessageBoxButtons.YesNo,
                                    MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    this.grid.Columns.RemoveAt(this._currentColumnIndex);
                    this._isDirty = true;
                }
            }
        }

        private void DataGridViewCellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            this._isDirty = true;
        }

        private void DataGridViewCellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.ColumnIndex >= 0)
                this._currentColumnIndex = e.ColumnIndex;
        }

        private void AddRowsButtonClick(object sender, EventArgs e)
        {
            if (this.grid.Columns.Count == 0) return;

            using (NumberPromptForm form = new NumberPromptForm())
            {
                form.Text = "Add Rows";

                if (form.ShowDialog(this) == DialogResult.OK)
                {
                    this.grid.Rows.Add(form.Number);
                    this._isDirty = true;
                }
            }
        }

        private void AddColumnsButtonClick(object sender, EventArgs e)
        {
            using (NumberPromptForm form = new NumberPromptForm())
            {
                form.Text = "Add Columns";

                if (form.ShowDialog(this) == DialogResult.OK)
                {
                    for (int i = 0; i < form.Number; i++)
                    {
                        this.AddColumn(this.grid.Columns.Count);
                    }
                }
            }
        }

        private void AddColumn(int index)
        {
            DataGridViewTextBoxColumn column = new DataGridViewTextBoxColumn();
            column.SortMode = DataGridViewColumnSortMode.NotSortable;
            this.grid.Columns.Insert(index, column);
            this._isDirty = true;
        }

        public void Save()
        {
            GridUtil.SaveGridAsFile(this._inputFile.FullName, this.grid);
            this._isDirty = false;
        }

        private void SaveInputsButtonClick(object sender, EventArgs e)
        {
            this.Save();

            (new UserActionRunner()).RunAction(this._saveUserAction);
        }

        private void GridStyleChanged()
        {
            string fileName = this._inputFile.FullName + ".style";
            GridUtil.SaveGridFormattingAsFile(fileName, this.grid);
        }

        private void GridUserAddedRow(object sender, DataGridViewRowEventArgs e)
        {
            this._isDirty = true;
        }

        private void GridUserDeletedRow(object sender, DataGridViewRowEventArgs e)
        {
            this._isDirty = true;
        }

        private void grid_ColumnWidthChanged(object sender, DataGridViewColumnEventArgs e)
        {
            this.GridStyleChanged();
        }

        private void grid_RowHeightChanged(object sender, DataGridViewRowEventArgs e)
        {
            this.GridStyleChanged();
        }
    }
}