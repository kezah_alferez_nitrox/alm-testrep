﻿using ALMSCommon.Controls;
using ALMSCommon.Controls.TreeGridView;

namespace LifeInsurance.Views.Inputs
{
    partial class InputPanelControl
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.columnHeaderContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.insertColumnBeforeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.insertColumnAfterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.headerPanel = new System.Windows.Forms.Panel();
            this.bClose = new System.Windows.Forms.Button();
            this.addRowsButton = new System.Windows.Forms.Button();
            this.addColumnsButton = new System.Windows.Forms.Button();
            this.saveInputsButton = new System.Windows.Forms.Button();
            this.titleLabel = new System.Windows.Forms.Label();
            this.grid = new DataGridViewEx();
            this.columnHeaderContextMenuStrip.SuspendLayout();
            this.headerPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grid)).BeginInit();
            this.SuspendLayout();
            // 
            // columnHeaderContextMenuStrip
            // 
            this.columnHeaderContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.insertColumnBeforeToolStripMenuItem,
            this.insertColumnAfterToolStripMenuItem,
            this.toolStripSeparator1,
            this.deleteToolStripMenuItem});
            this.columnHeaderContextMenuStrip.Name = "columnHeaderContextMenuStrip";
            this.columnHeaderContextMenuStrip.Size = new System.Drawing.Size(187, 76);
            // 
            // insertColumnBeforeToolStripMenuItem
            // 
            this.insertColumnBeforeToolStripMenuItem.Name = "insertColumnBeforeToolStripMenuItem";
            this.insertColumnBeforeToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.insertColumnBeforeToolStripMenuItem.Text = "Insert Column &Before";
            this.insertColumnBeforeToolStripMenuItem.Click += new System.EventHandler(this.InsertColumnBeforeToolStripMenuItemClick);
            // 
            // insertColumnAfterToolStripMenuItem
            // 
            this.insertColumnAfterToolStripMenuItem.Name = "insertColumnAfterToolStripMenuItem";
            this.insertColumnAfterToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.insertColumnAfterToolStripMenuItem.Text = "Insert Column &After";
            this.insertColumnAfterToolStripMenuItem.Click += new System.EventHandler(this.InsertColumnafterToolStripMenuItemClick);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(183, 6);
            // 
            // deleteToolStripMenuItem
            // 
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.deleteToolStripMenuItem.Text = "&Delete Column";
            this.deleteToolStripMenuItem.Click += new System.EventHandler(this.DeleteToolStripMenuItemClick);
            // 
            // headerPanel
            // 
            this.headerPanel.BackColor = System.Drawing.SystemColors.ControlDark;
            this.headerPanel.Controls.Add(this.bClose);
            this.headerPanel.Controls.Add(this.addRowsButton);
            this.headerPanel.Controls.Add(this.addColumnsButton);
            this.headerPanel.Controls.Add(this.saveInputsButton);
            this.headerPanel.Controls.Add(this.titleLabel);
            this.headerPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.headerPanel.Location = new System.Drawing.Point(0, 0);
            this.headerPanel.Name = "headerPanel";
            this.headerPanel.Size = new System.Drawing.Size(853, 36);
            this.headerPanel.TabIndex = 3;
            // 
            // bClose
            // 
            this.bClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bClose.BackColor = System.Drawing.SystemColors.Control;
            this.bClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bClose.ForeColor = System.Drawing.SystemColors.ControlText;
            this.bClose.Image = global::LifeInsurance.Properties.Resources.DeleteHS;
            this.bClose.Location = new System.Drawing.Point(823, 7);
            this.bClose.Name = "bClose";
            this.bClose.Size = new System.Drawing.Size(23, 23);
            this.bClose.TabIndex = 12;
            this.bClose.UseVisualStyleBackColor = false;
            this.bClose.Click += new System.EventHandler(this.bClose_Click);
            // 
            // addRowsButton
            // 
            this.addRowsButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.addRowsButton.Location = new System.Drawing.Point(464, 7);
            this.addRowsButton.Name = "addRowsButton";
            this.addRowsButton.Size = new System.Drawing.Size(100, 23);
            this.addRowsButton.TabIndex = 11;
            this.addRowsButton.Text = "Add Rows...";
            this.addRowsButton.UseVisualStyleBackColor = true;
            this.addRowsButton.Click += new System.EventHandler(this.AddRowsButtonClick);
            // 
            // addColumnsButton
            // 
            this.addColumnsButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.addColumnsButton.Location = new System.Drawing.Point(570, 7);
            this.addColumnsButton.Name = "addColumnsButton";
            this.addColumnsButton.Size = new System.Drawing.Size(100, 23);
            this.addColumnsButton.TabIndex = 10;
            this.addColumnsButton.Text = "Add Columns...";
            this.addColumnsButton.UseVisualStyleBackColor = true;
            this.addColumnsButton.Click += new System.EventHandler(this.AddColumnsButtonClick);
            // 
            // saveInputsButton
            // 
            this.saveInputsButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.saveInputsButton.Location = new System.Drawing.Point(702, 7);
            this.saveInputsButton.Name = "saveInputsButton";
            this.saveInputsButton.Size = new System.Drawing.Size(100, 23);
            this.saveInputsButton.TabIndex = 9;
            this.saveInputsButton.Text = "Save Inputs";
            this.saveInputsButton.UseVisualStyleBackColor = true;
            this.saveInputsButton.Click += new System.EventHandler(this.SaveInputsButtonClick);
            // 
            // titleLabel
            // 
            this.titleLabel.AutoSize = true;
            this.titleLabel.BackColor = System.Drawing.Color.Transparent;
            this.titleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold);
            this.titleLabel.ForeColor = System.Drawing.Color.White;
            this.titleLabel.Location = new System.Drawing.Point(6, 6);
            this.titleLabel.Name = "titleLabel";
            this.titleLabel.Size = new System.Drawing.Size(0, 24);
            this.titleLabel.TabIndex = 1;
            // 
            // grid
            // 
            this.grid.AllowUserToOrderColumns = true;
            this.grid.BackgroundColor = System.Drawing.SystemColors.Control;
            this.grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grid.ContextMenuStrip = this.columnHeaderContextMenuStrip;
            this.grid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grid.Location = new System.Drawing.Point(0, 36);
            this.grid.Name = "grid";
            this.grid.Size = new System.Drawing.Size(853, 338);
            this.grid.TabIndex = 4;
            this.grid.UserAddedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.GridUserAddedRow);
            this.grid.CellMouseDown += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DataGridViewCellMouseDown);
            this.grid.RowHeightChanged += new System.Windows.Forms.DataGridViewRowEventHandler(this.grid_RowHeightChanged);
            this.grid.UserDeletedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.GridUserDeletedRow);
            this.grid.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridViewCellEndEdit);
            this.grid.ColumnWidthChanged += new System.Windows.Forms.DataGridViewColumnEventHandler(this.grid_ColumnWidthChanged);
            // 
            // InputPanelControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.grid);
            this.Controls.Add(this.headerPanel);
            this.Name = "InputPanelControl";
            this.Padding = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.Size = new System.Drawing.Size(853, 379);
            this.Load += new System.EventHandler(this.InputPanelControl_Load);
            this.columnHeaderContextMenuStrip.ResumeLayout(false);
            this.headerPanel.ResumeLayout(false);
            this.headerPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DataGridViewEx grid;
        private System.Windows.Forms.Panel headerPanel;
        protected System.Windows.Forms.Button addRowsButton;
        protected System.Windows.Forms.Button addColumnsButton;
        protected System.Windows.Forms.Button saveInputsButton;
        private System.Windows.Forms.Label titleLabel;
        private System.Windows.Forms.Button bClose;
        private System.Windows.Forms.ContextMenuStrip columnHeaderContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem insertColumnBeforeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem insertColumnAfterToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
    }
}
