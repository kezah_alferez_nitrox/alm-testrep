using ALMSCommon.Data;

namespace LifeInsurance.Data
{
    public class UserSetting : IHasId<string>
    {
        private const char Separator = ';';

        public string Action { get; private set; }
        public string MacroName { get; set; }
        public string Address { get; set; }

        public UserSetting(string key)
        {
            this.Action = key;
        }

        public string Id
        {
            get { return this.Action; }
        }

        public string ToTextLine()
        {
            return string.Join(Separator.ToString(), new[] {this.Action, this.MacroName, this.Address});
        }

        public static UserSetting FromTextLine(string textLine)
        {
            if (string.IsNullOrEmpty(textLine)) return null;

            string[] fields = textLine.Split(Separator);
            if (fields.Length != 3) return null;

            UserSetting userSetting = new UserSetting(fields[0]);
            userSetting.MacroName = fields[1];
            userSetting.Address = fields[2];

            return userSetting;
        }
    }
}