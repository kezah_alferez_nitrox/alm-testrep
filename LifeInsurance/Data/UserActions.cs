namespace LifeInsurance.Data
{
    public static class UserActions
    {
        public const string UploadClient = "Upload Client";
        public const string CreateClient = "Create Client";
        public const string SaveClient = "Save Client";
        public const string SaveInputFiles = "Save Input Files";
        public const string PortfoliosSaveInputs = "Portfolios|Save Inputs";
        public const string PortfolioAssumptionsSaveInputs = "Portfolio Assumptions|Save Inputs";
        public const string MarketsSaveInputs = "Markets|Save Inputs";
        public const string LaunchDeterministic = "Launch Deterministic";
        public const string LaunchStochastic = "Launch Stochastic";
    }
}