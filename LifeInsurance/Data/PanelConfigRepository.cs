using System.Collections.Generic;
using System.IO;
using ALMSCommon.Data;
using LifeInsurance.Core;

namespace LifeInsurance.Data
{
    public class PanelConfigRepository : InMemoryRepository<string, PanelConfig>, IPanelConfigRepository
    {
        private const string PanelsFileName = "Panels.txt";
        private const char Separator = '�';

        public void Backup(string fileName)
        {
            List<string> lines = new List<string>();
            foreach (PanelConfig panelConfig in this.GetAll())
            {
                lines.Add(panelConfig.Id + Separator + panelConfig.Height);
            }

            fileName = Path.Combine(Client.Path, PanelsFileName);
            File.WriteAllLines(fileName, lines.ToArray());
        }

        public void Restore(string fileName)
        {
            fileName = Path.Combine(Client.Path, PanelsFileName);
            if (!File.Exists(fileName)) return;

            this.DeleteAll();
            foreach (string line in File.ReadAllLines(fileName))
            {
                string[] strings = line.Split(Separator);
                this.Save(new PanelConfig(strings[0], int.Parse(strings[1])));
            }
        }
    }
}