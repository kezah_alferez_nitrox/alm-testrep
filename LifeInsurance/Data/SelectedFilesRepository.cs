using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using ALMSCommon.Data;
using LifeInsurance.Core;

namespace LifeInsurance.Data
{
    public class SelectedFilesRepository : InMemoryRepository<string, InputFile>, ISelectedFilesRepository
    {
        public override void Delete(InputFile obj)
        {
            IList<InputFile> toRemove = new List<InputFile>();

            foreach (InputFile fileInfo in this.GetAll())
            {
                if (string.Compare(fileInfo.FullName, obj.FullName, StringComparison.InvariantCultureIgnoreCase) == 0)
                {
                    toRemove.Add(fileInfo);
                }
            }

            foreach (InputFile info in toRemove)
            {
                base.Delete(info);
            }
        }

        public void Backup(string fileName)
        {
            StringBuilder text = new StringBuilder();
            IEnumerable<InputFile> allSelectedFiles = this.GetAll();
            foreach (InputFile fileInfo in allSelectedFiles)
            {
                string relativePath = Util.RelativePath(fileInfo.FullName, Client.Path);
                text.Append(relativePath + Environment.NewLine);
            }

            File.WriteAllText(fileName, text.ToString());
        }

        public void Restore(string fileName)
        {
            string[] allLines = File.ReadAllLines(fileName);
            this.DeleteAll();
            foreach (string runLine in allLines)
            {
                string inputFileName = Path.Combine(Client.Path, runLine);
                if (File.Exists(inputFileName))
                {
                    this.Save(new InputFile(inputFileName));
                }
            }
        }
    }
}