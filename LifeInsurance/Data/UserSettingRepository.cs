using System.Collections.Generic;
using System.IO;
using ALMSCommon.Data;
using LifeInsurance.Core;

namespace LifeInsurance.Data
{
    public class UserSettingRepository : InMemoryRepository<string, UserSetting>, IUserSettingRepository
    {
        private const string UserDataFileName = "UserData.txt";

        public void Restore(string fileName)
        {
            fileName = Path.Combine(Client.Path, UserDataFileName);
            if (!File.Exists(fileName)) return;

            this.DeleteAll();
            foreach (string line in File.ReadAllLines(fileName))
            {
                this.Save(UserSetting.FromTextLine(line));
            }
        }

        public void Backup(string fileName)
        {
            if (string.IsNullOrEmpty(Client.Path)) return;

            List<string> lines = new List<string>();
            foreach (UserSetting userSetting in this.GetAll())
            {
                lines.Add(userSetting.ToTextLine());
            }

            fileName = Path.Combine(Client.Path, UserDataFileName);
            File.WriteAllLines(fileName, lines.ToArray());
        }
    }
}