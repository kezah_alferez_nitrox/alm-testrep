using ALMSCommon.Data;

namespace LifeInsurance.Data
{
    public class InputFile : IHasId<string>
    {
        public string Id
        {
            get { return this.FullName; }
        }

        public InputFile(string fullName)
        {
            this.FullName = fullName;
        }

        public string FullName { get; set; }
    }
}