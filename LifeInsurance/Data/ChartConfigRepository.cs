using System.Collections.Generic;
using System.IO;
using ALMSCommon.Data;
using LifeInsurance.Core;

namespace LifeInsurance.Data
{
    public class ChartConfigRepository : InMemoryWithoutIdRepository<string>, IChartConfigRepository
    {
        private const string ChartsFileName = "Charts.txt";

        public void Backup(string fileName)
        {
            List<string> lines = new List<string>();
            foreach (string line in this.GetAll())
            {
                lines.Add(line);
            }

            fileName = Path.Combine(Client.Path, ChartsFileName);
            File.WriteAllLines(fileName, lines.ToArray());
        }

        public void Restore(string fileName)
        {
            fileName = Path.Combine(Client.Path, ChartsFileName);
            if (!File.Exists(fileName)) return;

            this.DeleteAll();
            foreach (string line in File.ReadAllLines(fileName))
            {
                this.Save(line);
            }
        }
    }
}