using LifeInsurance.Views.Result;

namespace LifeInsurance
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pasteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.selectAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.launchDeterministicToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.launchStochasticToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mainTabControl = new System.Windows.Forms.TabControl();
            this.introTabPage = new System.Windows.Forms.TabPage();
            this.setupTabPage = new System.Windows.Forms.TabPage();
            this.inputsTabPage = new System.Windows.Forms.TabPage();
            this.resultTabPage = new System.Windows.Forms.TabPage();
            this.userDataTabPage = new System.Windows.Forms.TabPage();
            this.mainPanel = new System.Windows.Forms.Panel();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.mainToolStrip = new System.Windows.Forms.ToolStrip();
            this.fontNameToolStripComboBox = new System.Windows.Forms.ToolStripComboBox();
            this.fontSizeToolStripComboBox = new System.Windows.Forms.ToolStripComboBox();
            this.colorDialog = new System.Windows.Forms.ColorDialog();
            this.pasteToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.fontBoldToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.fontItalicToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.cellForegroundToolStripSplitButton = new System.Windows.Forms.ToolStripSplitButton();
            this.cellBackgroundToolStripSplitButton = new System.Windows.Forms.ToolStripSplitButton();
            this.copyStyleToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.pasteStyleToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.increaseDecimalsToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.decreaseDecimalsToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.introUserControl1 = new LifeInsurance.Views.Intro.IntroUserControl();
            this.setupControl = new LifeInsurance.Views.Setup.SetupControl();
            this.inputsUserControl = new LifeInsurance.Views.Inputs.InputsUserControl();
            this.resultControl = new LifeInsurance.Views.Result.ResultControl();
            this.userDataControl1 = new LifeInsurance.Views.UserData.UserDataControl();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.menuStrip.SuspendLayout();
            this.mainTabControl.SuspendLayout();
            this.introTabPage.SuspendLayout();
            this.setupTabPage.SuspendLayout();
            this.inputsTabPage.SuspendLayout();
            this.resultTabPage.SuspendLayout();
            this.userDataTabPage.SuspendLayout();
            this.mainPanel.SuspendLayout();
            this.mainToolStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem,
            this.helpToolStripMenuItem,
            this.launchDeterministicToolStripMenuItem,
            this.launchStochasticToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(926, 24);
            this.menuStrip.TabIndex = 0;
            this.menuStrip.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(92, 22);
            this.exitToolStripMenuItem.Text = "E&xit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cutToolStripMenuItem,
            this.copyToolStripMenuItem,
            this.pasteToolStripMenuItem,
            this.toolStripSeparator6,
            this.selectAllToolStripMenuItem});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.editToolStripMenuItem.Text = "&Edit";
            // 
            // cutToolStripMenuItem
            // 
            this.cutToolStripMenuItem.Name = "cutToolStripMenuItem";
            this.cutToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X)));
            this.cutToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.cutToolStripMenuItem.Text = "Cut";
            // 
            // copyToolStripMenuItem
            // 
            this.copyToolStripMenuItem.Name = "copyToolStripMenuItem";
            this.copyToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this.copyToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.copyToolStripMenuItem.Text = "Copy";
            // 
            // pasteToolStripMenuItem
            // 
            this.pasteToolStripMenuItem.Name = "pasteToolStripMenuItem";
            this.pasteToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V)));
            this.pasteToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.pasteToolStripMenuItem.Text = "Paste";
            this.pasteToolStripMenuItem.Click += new System.EventHandler(this.pasteToolStripMenuItem_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(161, 6);
            // 
            // selectAllToolStripMenuItem
            // 
            this.selectAllToolStripMenuItem.Enabled = false;
            this.selectAllToolStripMenuItem.Name = "selectAllToolStripMenuItem";
            this.selectAllToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A)));
            this.selectAllToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.selectAllToolStripMenuItem.Text = "Select All";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // launchDeterministicToolStripMenuItem
            // 
            this.launchDeterministicToolStripMenuItem.Name = "launchDeterministicToolStripMenuItem";
            this.launchDeterministicToolStripMenuItem.Size = new System.Drawing.Size(131, 20);
            this.launchDeterministicToolStripMenuItem.Text = "Launch &Deterministic";
            this.launchDeterministicToolStripMenuItem.Click += new System.EventHandler(this.launchDeterministicToolStripMenuItem_Click);
            // 
            // launchStochasticToolStripMenuItem
            // 
            this.launchStochasticToolStripMenuItem.Name = "launchStochasticToolStripMenuItem";
            this.launchStochasticToolStripMenuItem.Size = new System.Drawing.Size(115, 20);
            this.launchStochasticToolStripMenuItem.Text = "Launch &Stochastic";
            this.launchStochasticToolStripMenuItem.Click += new System.EventHandler(this.launchStochasticToolStripMenuItem_Click);
            // 
            // mainTabControl
            // 
            this.mainTabControl.Controls.Add(this.introTabPage);
            this.mainTabControl.Controls.Add(this.setupTabPage);
            this.mainTabControl.Controls.Add(this.inputsTabPage);
            this.mainTabControl.Controls.Add(this.resultTabPage);
            this.mainTabControl.Controls.Add(this.userDataTabPage);
            this.mainTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainTabControl.Location = new System.Drawing.Point(0, 0);
            this.mainTabControl.Name = "mainTabControl";
            this.mainTabControl.SelectedIndex = 0;
            this.mainTabControl.Size = new System.Drawing.Size(926, 520);
            this.mainTabControl.TabIndex = 2;
            this.mainTabControl.SelectedIndexChanged += new System.EventHandler(this.mainTabControl_SelectedIndexChanged);
            // 
            // introTabPage
            // 
            this.introTabPage.BackColor = System.Drawing.Color.DimGray;
            this.introTabPage.Controls.Add(this.introUserControl1);
            this.introTabPage.Location = new System.Drawing.Point(4, 22);
            this.introTabPage.Name = "introTabPage";
            this.introTabPage.Size = new System.Drawing.Size(918, 494);
            this.introTabPage.TabIndex = 3;
            this.introTabPage.Text = "Intro";
            // 
            // setupTabPage
            // 
            this.setupTabPage.Controls.Add(this.setupControl);
            this.setupTabPage.Location = new System.Drawing.Point(4, 22);
            this.setupTabPage.Name = "setupTabPage";
            this.setupTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.setupTabPage.Size = new System.Drawing.Size(918, 494);
            this.setupTabPage.TabIndex = 1;
            this.setupTabPage.Text = "Setup";
            this.setupTabPage.UseVisualStyleBackColor = true;
            // 
            // inputsTabPage
            // 
            this.inputsTabPage.Controls.Add(this.inputsUserControl);
            this.inputsTabPage.Location = new System.Drawing.Point(4, 22);
            this.inputsTabPage.Name = "inputsTabPage";
            this.inputsTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.inputsTabPage.Size = new System.Drawing.Size(918, 494);
            this.inputsTabPage.TabIndex = 0;
            this.inputsTabPage.Text = "Inputs";
            this.inputsTabPage.UseVisualStyleBackColor = true;
            // 
            // resultTabPage
            // 
            this.resultTabPage.Controls.Add(this.resultControl);
            this.resultTabPage.Location = new System.Drawing.Point(4, 22);
            this.resultTabPage.Name = "resultTabPage";
            this.resultTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.resultTabPage.Size = new System.Drawing.Size(918, 494);
            this.resultTabPage.TabIndex = 2;
            this.resultTabPage.Text = "Result";
            this.resultTabPage.UseVisualStyleBackColor = true;
            // 
            // userDataTabPage
            // 
            this.userDataTabPage.Controls.Add(this.userDataControl1);
            this.userDataTabPage.Location = new System.Drawing.Point(4, 22);
            this.userDataTabPage.Name = "userDataTabPage";
            this.userDataTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.userDataTabPage.Size = new System.Drawing.Size(918, 494);
            this.userDataTabPage.TabIndex = 4;
            this.userDataTabPage.Text = "User Data";
            this.userDataTabPage.UseVisualStyleBackColor = true;
            // 
            // mainPanel
            // 
            this.mainPanel.Controls.Add(this.mainTabControl);
            this.mainPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainPanel.Location = new System.Drawing.Point(0, 49);
            this.mainPanel.Name = "mainPanel";
            this.mainPanel.Size = new System.Drawing.Size(926, 520);
            this.mainPanel.TabIndex = 5;
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 23);
            // 
            // mainToolStrip
            // 
            this.mainToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pasteToolStripButton,
            this.toolStripSeparator2,
            this.fontBoldToolStripButton,
            this.fontItalicToolStripButton,
            this.toolStripSeparator1,
            this.fontNameToolStripComboBox,
            this.fontSizeToolStripComboBox,
            this.toolStripSeparator3,
            this.cellForegroundToolStripSplitButton,
            this.cellBackgroundToolStripSplitButton,
            this.toolStripSeparator5,
            this.copyStyleToolStripButton,
            this.pasteStyleToolStripButton,
            this.toolStripSeparator4,
            this.increaseDecimalsToolStripButton,
            this.decreaseDecimalsToolStripButton,
            this.toolStripSeparator7});
            this.mainToolStrip.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.mainToolStrip.Location = new System.Drawing.Point(0, 24);
            this.mainToolStrip.Name = "mainToolStrip";
            this.mainToolStrip.Padding = new System.Windows.Forms.Padding(0, 2, 1, 0);
            this.mainToolStrip.Size = new System.Drawing.Size(926, 25);
            this.mainToolStrip.TabIndex = 4;
            this.mainToolStrip.Text = "toolStrip1";
            // 
            // fontNameToolStripComboBox
            // 
            this.fontNameToolStripComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.fontNameToolStripComboBox.Name = "fontNameToolStripComboBox";
            this.fontNameToolStripComboBox.Size = new System.Drawing.Size(180, 23);
            this.fontNameToolStripComboBox.SelectedIndexChanged += new System.EventHandler(this.fontNameToolStripComboBox_SelectedIndexChanged);
            // 
            // fontSizeToolStripComboBox
            // 
            this.fontSizeToolStripComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.fontSizeToolStripComboBox.Items.AddRange(new object[] {
            "8",
            "9",
            "10",
            "11",
            "12",
            "14",
            "16",
            "18",
            "20",
            "22",
            "24",
            "26",
            "28",
            "36",
            "48",
            "72"});
            this.fontSizeToolStripComboBox.Name = "fontSizeToolStripComboBox";
            this.fontSizeToolStripComboBox.Size = new System.Drawing.Size(75, 23);
            this.fontSizeToolStripComboBox.SelectedIndexChanged += new System.EventHandler(this.fontSizeToolStripComboBox_SelectedIndexChanged);
            // 
            // pasteToolStripButton
            // 
            this.pasteToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.pasteToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("pasteToolStripButton.Image")));
            this.pasteToolStripButton.ImageTransparentColor = System.Drawing.Color.Black;
            this.pasteToolStripButton.Name = "pasteToolStripButton";
            this.pasteToolStripButton.Size = new System.Drawing.Size(23, 20);
            this.pasteToolStripButton.Text = "Paste";
            this.pasteToolStripButton.Click += new System.EventHandler(this.pasteToolStripButton_Click);
            // 
            // fontBoldToolStripButton
            // 
            this.fontBoldToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.fontBoldToolStripButton.Image = global::LifeInsurance.Properties.Resources.boldhs;
            this.fontBoldToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.fontBoldToolStripButton.Name = "fontBoldToolStripButton";
            this.fontBoldToolStripButton.Size = new System.Drawing.Size(23, 20);
            this.fontBoldToolStripButton.Text = "Bold";
            this.fontBoldToolStripButton.Click += new System.EventHandler(this.fontBoldToolStripButton_Click);
            // 
            // fontItalicToolStripButton
            // 
            this.fontItalicToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.fontItalicToolStripButton.Image = global::LifeInsurance.Properties.Resources.ItalicHS;
            this.fontItalicToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.fontItalicToolStripButton.Name = "fontItalicToolStripButton";
            this.fontItalicToolStripButton.Size = new System.Drawing.Size(23, 20);
            this.fontItalicToolStripButton.Text = "Italic";
            this.fontItalicToolStripButton.Click += new System.EventHandler(this.fontItalicsToolStripButton_Click);
            // 
            // cellForegroundToolStripSplitButton
            // 
            this.cellForegroundToolStripSplitButton.Image = ((System.Drawing.Image)(resources.GetObject("cellForegroundToolStripSplitButton.Image")));
            this.cellForegroundToolStripSplitButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cellForegroundToolStripSplitButton.Name = "cellForegroundToolStripSplitButton";
            this.cellForegroundToolStripSplitButton.Size = new System.Drawing.Size(69, 20);
            this.cellForegroundToolStripSplitButton.Text = "Font :";
            this.cellForegroundToolStripSplitButton.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.cellForegroundToolStripSplitButton.ButtonClick += new System.EventHandler(this.cellForegroundToolStripSplitButton_ButtonClick);
            this.cellForegroundToolStripSplitButton.DropDownOpening += new System.EventHandler(this.cellForegroundToolStripSplitButton_DropDownOpening);
            // 
            // cellBackgroundToolStripSplitButton
            // 
            this.cellBackgroundToolStripSplitButton.BackColor = System.Drawing.SystemColors.Control;
            this.cellBackgroundToolStripSplitButton.Image = ((System.Drawing.Image)(resources.GetObject("cellBackgroundToolStripSplitButton.Image")));
            this.cellBackgroundToolStripSplitButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cellBackgroundToolStripSplitButton.Name = "cellBackgroundToolStripSplitButton";
            this.cellBackgroundToolStripSplitButton.Size = new System.Drawing.Size(109, 20);
            this.cellBackgroundToolStripSplitButton.Text = "Background :";
            this.cellBackgroundToolStripSplitButton.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.cellBackgroundToolStripSplitButton.ButtonClick += new System.EventHandler(this.cellBackgroundToolStripSplitButton_ButtonClick);
            this.cellBackgroundToolStripSplitButton.DropDownOpening += new System.EventHandler(this.cellBackgroundToolStripSplitButton_DropDownOpening);
            // 
            // copyStyleToolStripButton
            // 
            this.copyStyleToolStripButton.Image = global::LifeInsurance.Properties.Resources.CopyHS;
            this.copyStyleToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.copyStyleToolStripButton.Name = "copyStyleToolStripButton";
            this.copyStyleToolStripButton.Size = new System.Drawing.Size(83, 20);
            this.copyStyleToolStripButton.Text = "Copy Style";
            this.copyStyleToolStripButton.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.copyStyleToolStripButton.Click += new System.EventHandler(this.copyStyleToolStripButton_Click);
            // 
            // pasteStyleToolStripButton
            // 
            this.pasteStyleToolStripButton.Image = global::LifeInsurance.Properties.Resources.PasteHS;
            this.pasteStyleToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.pasteStyleToolStripButton.Name = "pasteStyleToolStripButton";
            this.pasteStyleToolStripButton.Size = new System.Drawing.Size(83, 20);
            this.pasteStyleToolStripButton.Text = "Paste Style";
            this.pasteStyleToolStripButton.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.pasteStyleToolStripButton.Click += new System.EventHandler(this.pasteStyleToolStripButton_Click);
            // 
            // increaseDecimalsToolStripButton
            // 
            this.increaseDecimalsToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.increaseDecimalsToolStripButton.Image = global::LifeInsurance.Properties.Resources.FillLeftHS;
            this.increaseDecimalsToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.increaseDecimalsToolStripButton.Name = "increaseDecimalsToolStripButton";
            this.increaseDecimalsToolStripButton.Size = new System.Drawing.Size(23, 20);
            this.increaseDecimalsToolStripButton.Text = "Increase Visible Decimals";
            this.increaseDecimalsToolStripButton.Click += new System.EventHandler(this.increaseDecimalsToolStripButton_Click);
            // 
            // decreaseDecimalsToolStripButton
            // 
            this.decreaseDecimalsToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.decreaseDecimalsToolStripButton.Image = global::LifeInsurance.Properties.Resources.FillRightHS;
            this.decreaseDecimalsToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.decreaseDecimalsToolStripButton.Name = "decreaseDecimalsToolStripButton";
            this.decreaseDecimalsToolStripButton.Size = new System.Drawing.Size(23, 20);
            this.decreaseDecimalsToolStripButton.Text = "Decrease Visible Decimals";
            this.decreaseDecimalsToolStripButton.Click += new System.EventHandler(this.decreaseDecimalsToolStripButton_Click);
            // 
            // introUserControl1
            // 
            this.introUserControl1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.introUserControl1.BackColor = System.Drawing.Color.DimGray;
            this.introUserControl1.Location = new System.Drawing.Point(211, 3);
            this.introUserControl1.Name = "introUserControl1";
            this.introUserControl1.Size = new System.Drawing.Size(496, 488);
            this.introUserControl1.TabIndex = 0;
            // 
            // setupControl
            // 
            this.setupControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.setupControl.Location = new System.Drawing.Point(3, 3);
            this.setupControl.Name = "setupControl";
            this.setupControl.Size = new System.Drawing.Size(912, 488);
            this.setupControl.TabIndex = 0;
            // 
            // inputsUserControl
            // 
            this.inputsUserControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.inputsUserControl.Location = new System.Drawing.Point(3, 3);
            this.inputsUserControl.Name = "inputsUserControl";
            this.inputsUserControl.Size = new System.Drawing.Size(912, 488);
            this.inputsUserControl.TabIndex = 0;
            // 
            // resultControl
            // 
            this.resultControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.resultControl.Location = new System.Drawing.Point(3, 3);
            this.resultControl.Name = "resultControl";
            this.resultControl.Size = new System.Drawing.Size(912, 488);
            this.resultControl.TabIndex = 1;
            // 
            // userDataControl1
            // 
            this.userDataControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.userDataControl1.Location = new System.Drawing.Point(3, 3);
            this.userDataControl1.Name = "userDataControl1";
            this.userDataControl1.Size = new System.Drawing.Size(912, 488);
            this.userDataControl1.TabIndex = 0;
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 23);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 23);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 23);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 23);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(6, 23);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(926, 569);
            this.Controls.Add(this.mainPanel);
            this.Controls.Add(this.mainToolStrip);
            this.Controls.Add(this.menuStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip;
            this.Name = "MainForm";
            this.Text = "ALM Solutions";
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.mainTabControl.ResumeLayout(false);
            this.introTabPage.ResumeLayout(false);
            this.setupTabPage.ResumeLayout(false);
            this.inputsTabPage.ResumeLayout(false);
            this.resultTabPage.ResumeLayout(false);
            this.userDataTabPage.ResumeLayout(false);
            this.mainPanel.ResumeLayout(false);
            this.mainToolStrip.ResumeLayout(false);
            this.mainToolStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.TabControl mainTabControl;
        private System.Windows.Forms.TabPage inputsTabPage;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pasteToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripMenuItem selectAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.Panel mainPanel;
        private System.Windows.Forms.TabPage setupTabPage;
        private System.Windows.Forms.TabPage resultTabPage;
        private LifeInsurance.Views.Setup.SetupControl setupControl;
        private System.Windows.Forms.TabPage introTabPage;
        private LifeInsurance.Views.Intro.IntroUserControl introUserControl1;
        private LifeInsurance.Views.Inputs.InputsUserControl inputsUserControl;
        private System.Windows.Forms.TabPage userDataTabPage;
        private System.Windows.Forms.ToolStripMenuItem launchDeterministicToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem launchStochasticToolStripMenuItem;
        private ResultControl resultControl;
        private LifeInsurance.Views.UserData.UserDataControl userDataControl1;
        private System.Windows.Forms.ToolStripButton pasteToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStrip mainToolStrip;
        private System.Windows.Forms.ToolStripButton fontBoldToolStripButton;
        private System.Windows.Forms.ToolStripButton fontItalicToolStripButton;
        private System.Windows.Forms.ToolStripComboBox fontSizeToolStripComboBox;
        private System.Windows.Forms.ToolStripComboBox fontNameToolStripComboBox;
        private System.Windows.Forms.ToolStripSplitButton cellBackgroundToolStripSplitButton;
        private System.Windows.Forms.ToolStripSplitButton cellForegroundToolStripSplitButton;
        private System.Windows.Forms.ColorDialog colorDialog;
        private System.Windows.Forms.ToolStripButton copyStyleToolStripButton;
        private System.Windows.Forms.ToolStripButton pasteStyleToolStripButton;
        private System.Windows.Forms.ToolStripButton increaseDecimalsToolStripButton;
        private System.Windows.Forms.ToolStripButton decreaseDecimalsToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
    }
}

