/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.1
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */


using System;
using System.Runtime.InteropServices;

public class ARM_ROUND : IDisposable {
  private HandleRef swigCPtr;
  protected bool swigCMemOwn;

  internal ARM_ROUND(IntPtr cPtr, bool cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = new HandleRef(this, cPtr);
  }

  internal static HandleRef getCPtr(ARM_ROUND obj) {
    return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.swigCPtr;
  }

  ~ARM_ROUND() {
    Dispose();
  }

  public virtual void Dispose() {
    lock(this) {
      if (swigCPtr.Handle != IntPtr.Zero) {
        if (swigCMemOwn) {
          swigCMemOwn = false;
          icmoPINVOKE.delete_ARM_ROUND(swigCPtr);
        }
        swigCPtr = new HandleRef(null, IntPtr.Zero);
      }
      GC.SuppressFinalize(this);
    }
  }

  public double armr_unit {
    set {
      icmoPINVOKE.ARM_ROUND_armr_unit_set(swigCPtr, value);
    } 
    get {
      double ret = icmoPINVOKE.ARM_ROUND_armr_unit_get(swigCPtr);
      return ret;
    } 
  }

  public char armr_target {
    set {
      icmoPINVOKE.ARM_ROUND_armr_target_set(swigCPtr, value);
    } 
    get {
      char ret = icmoPINVOKE.ARM_ROUND_armr_target_get(swigCPtr);
      return ret;
    } 
  }

  public char armr_method {
    set {
      icmoPINVOKE.ARM_ROUND_armr_method_set(swigCPtr, value);
    } 
    get {
      char ret = icmoPINVOKE.ARM_ROUND_armr_method_get(swigCPtr);
      return ret;
    } 
  }

  public string armr_next {
    set {
      icmoPINVOKE.ARM_ROUND_armr_next_set(swigCPtr, value);
    } 
    get {
      string ret = icmoPINVOKE.ARM_ROUND_armr_next_get(swigCPtr);
      return ret;
    } 
  }

  public ARM_ROUND() : this(icmoPINVOKE.new_ARM_ROUND(), true) {
  }

}
