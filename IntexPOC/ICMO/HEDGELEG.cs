/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.1
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */


using System;
using System.Runtime.InteropServices;

public class HEDGELEG : IDisposable {
  private HandleRef swigCPtr;
  protected bool swigCMemOwn;

  internal HEDGELEG(IntPtr cPtr, bool cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = new HandleRef(this, cPtr);
  }

  internal static HandleRef getCPtr(HEDGELEG obj) {
    return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.swigCPtr;
  }

  ~HEDGELEG() {
    Dispose();
  }

  public virtual void Dispose() {
    lock(this) {
      if (swigCPtr.Handle != IntPtr.Zero) {
        if (swigCMemOwn) {
          swigCMemOwn = false;
          icmoPINVOKE.delete_HEDGELEG(swigCPtr);
        }
        swigCPtr = new HandleRef(null, IntPtr.Zero);
      }
      GC.SuppressFinalize(this);
    }
  }

  public string hleg_next {
    set {
      icmoPINVOKE.HEDGELEG_hleg_next_set(swigCPtr, value);
    } 
    get {
      string ret = icmoPINVOKE.HEDGELEG_hleg_next_get(swigCPtr);
      return ret;
    } 
  }

  public string hleg_name {
    set {
      icmoPINVOKE.HEDGELEG_hleg_name_set(swigCPtr, value);
    } 
    get {
      string ret = icmoPINVOKE.HEDGELEG_hleg_name_get(swigCPtr);
      return ret;
    } 
  }

  public int hleg_hlegno {
    set {
      icmoPINVOKE.HEDGELEG_hleg_hlegno_set(swigCPtr, value);
    } 
    get {
      int ret = icmoPINVOKE.HEDGELEG_hleg_hlegno_get(swigCPtr);
      return ret;
    } 
  }

  public int hleg_type {
    set {
      icmoPINVOKE.HEDGELEG_hleg_type_set(swigCPtr, value);
    } 
    get {
      int ret = icmoPINVOKE.HEDGELEG_hleg_type_get(swigCPtr);
      return ret;
    } 
  }

  public int hleg_cf_direction {
    set {
      icmoPINVOKE.HEDGELEG_hleg_cf_direction_set(swigCPtr, value);
    } 
    get {
      int ret = icmoPINVOKE.HEDGELEG_hleg_cf_direction_get(swigCPtr);
      return ret;
    } 
  }

  public int hleg_optpmt_method {
    set {
      icmoPINVOKE.HEDGELEG_hleg_optpmt_method_set(swigCPtr, value);
    } 
    get {
      int ret = icmoPINVOKE.HEDGELEG_hleg_optpmt_method_get(swigCPtr);
      return ret;
    } 
  }

  public int hleg_optpmt_trno {
    set {
      icmoPINVOKE.HEDGELEG_hleg_optpmt_trno_set(swigCPtr, value);
    } 
    get {
      int ret = icmoPINVOKE.HEDGELEG_hleg_optpmt_trno_get(swigCPtr);
      return ret;
    } 
  }

  public string hleg_optpmt_toptkn {
    set {
      icmoPINVOKE.HEDGELEG_hleg_optpmt_toptkn_set(swigCPtr, value);
    } 
    get {
      string ret = icmoPINVOKE.HEDGELEG_hleg_optpmt_toptkn_get(swigCPtr);
      return ret;
    } 
  }

  public int hleg_index_deps {
    set {
      icmoPINVOKE.HEDGELEG_hleg_index_deps_set(swigCPtr, value);
    } 
    get {
      int ret = icmoPINVOKE.HEDGELEG_hleg_index_deps_get(swigCPtr);
      return ret;
    } 
  }

  public int hleg_pseudo_trno {
    set {
      icmoPINVOKE.HEDGELEG_hleg_pseudo_trno_set(swigCPtr, value);
    } 
    get {
      int ret = icmoPINVOKE.HEDGELEG_hleg_pseudo_trno_get(swigCPtr);
      return ret;
    } 
  }

  public ICMO_CURRENCY hleg_optpmt_currency {
    set {
      icmoPINVOKE.HEDGELEG_hleg_optpmt_currency_set(swigCPtr, ICMO_CURRENCY.getCPtr(value));
    } 
    get {
      IntPtr cPtr = icmoPINVOKE.HEDGELEG_hleg_optpmt_currency_get(swigCPtr);
      ICMO_CURRENCY ret = (cPtr == IntPtr.Zero) ? null : new ICMO_CURRENCY(cPtr, false);
      return ret;
    } 
  }

  public double hleg_start_amt_unpaid {
    set {
      icmoPINVOKE.HEDGELEG_hleg_start_amt_unpaid_set(swigCPtr, value);
    } 
    get {
      double ret = icmoPINVOKE.HEDGELEG_hleg_start_amt_unpaid_get(swigCPtr);
      return ret;
    } 
  }

  public int hleg_attributes {
    set {
      icmoPINVOKE.HEDGELEG_hleg_attributes_set(swigCPtr, value);
    } 
    get {
      int ret = icmoPINVOKE.HEDGELEG_hleg_attributes_get(swigCPtr);
      return ret;
    } 
  }

  public ICMO_INTRATE hleg_unpaid_intratep {
    set {
      icmoPINVOKE.HEDGELEG_hleg_unpaid_intratep_set(swigCPtr, ICMO_INTRATE.getCPtr(value));
    } 
    get {
      IntPtr cPtr = icmoPINVOKE.HEDGELEG_hleg_unpaid_intratep_get(swigCPtr);
      ICMO_INTRATE ret = (cPtr == IntPtr.Zero) ? null : new ICMO_INTRATE(cPtr, false);
      return ret;
    } 
  }

  public string hleg_assoc_term_hlegp {
    set {
      icmoPINVOKE.HEDGELEG_hleg_assoc_term_hlegp_set(swigCPtr, value);
    } 
    get {
      string ret = icmoPINVOKE.HEDGELEG_hleg_assoc_term_hlegp_get(swigCPtr);
      return ret;
    } 
  }

  public int hleg_optpmt_calculated {
    set {
      icmoPINVOKE.HEDGELEG_hleg_optpmt_calculated_set(swigCPtr, value);
    } 
    get {
      int ret = icmoPINVOKE.HEDGELEG_hleg_optpmt_calculated_get(swigCPtr);
      return ret;
    } 
  }

  public double hleg_optpmt {
    set {
      icmoPINVOKE.HEDGELEG_hleg_optpmt_set(swigCPtr, value);
    } 
    get {
      double ret = icmoPINVOKE.HEDGELEG_hleg_optpmt_get(swigCPtr);
      return ret;
    } 
  }

  public double hleg_amt_topay {
    set {
      icmoPINVOKE.HEDGELEG_hleg_amt_topay_set(swigCPtr, value);
    } 
    get {
      double ret = icmoPINVOKE.HEDGELEG_hleg_amt_topay_get(swigCPtr);
      return ret;
    } 
  }

  public double hleg_amt_paid {
    set {
      icmoPINVOKE.HEDGELEG_hleg_amt_paid_set(swigCPtr, value);
    } 
    get {
      double ret = icmoPINVOKE.HEDGELEG_hleg_amt_paid_get(swigCPtr);
      return ret;
    } 
  }

  public double hleg_amt_unpaid {
    set {
      icmoPINVOKE.HEDGELEG_hleg_amt_unpaid_set(swigCPtr, value);
    } 
    get {
      double ret = icmoPINVOKE.HEDGELEG_hleg_amt_unpaid_get(swigCPtr);
      return ret;
    } 
  }

  public HEDGELEG() : this(icmoPINVOKE.new_HEDGELEG(), true) {
  }

}
