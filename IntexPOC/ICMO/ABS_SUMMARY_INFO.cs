/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.1
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */


using System;
using System.Runtime.InteropServices;

public class ABS_SUMMARY_INFO : IDisposable {
  private HandleRef swigCPtr;
  protected bool swigCMemOwn;

  internal ABS_SUMMARY_INFO(IntPtr cPtr, bool cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = new HandleRef(this, cPtr);
  }

  internal static HandleRef getCPtr(ABS_SUMMARY_INFO obj) {
    return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.swigCPtr;
  }

  ~ABS_SUMMARY_INFO() {
    Dispose();
  }

  public virtual void Dispose() {
    lock(this) {
      if (swigCPtr.Handle != IntPtr.Zero) {
        if (swigCMemOwn) {
          swigCMemOwn = false;
          icmoPINVOKE.delete_ABS_SUMMARY_INFO(swigCPtr);
        }
        swigCPtr = new HandleRef(null, IntPtr.Zero);
      }
      GC.SuppressFinalize(this);
    }
  }

  public string absi_next {
    set {
      icmoPINVOKE.ABS_SUMMARY_INFO_absi_next_set(swigCPtr, value);
    } 
    get {
      string ret = icmoPINVOKE.ABS_SUMMARY_INFO_absi_next_get(swigCPtr);
      return ret;
    } 
  }

  public string absi_name {
    set {
      icmoPINVOKE.ABS_SUMMARY_INFO_absi_name_set(swigCPtr, value);
    } 
    get {
      string ret = icmoPINVOKE.ABS_SUMMARY_INFO_absi_name_get(swigCPtr);
      return ret;
    } 
  }

  public string absi_sameas_dealname {
    set {
      icmoPINVOKE.ABS_SUMMARY_INFO_absi_sameas_dealname_set(swigCPtr, value);
    } 
    get {
      string ret = icmoPINVOKE.ABS_SUMMARY_INFO_absi_sameas_dealname_get(swigCPtr);
      return ret;
    } 
  }

  public int absi_type {
    set {
      icmoPINVOKE.ABS_SUMMARY_INFO_absi_type_set(swigCPtr, value);
    } 
    get {
      int ret = icmoPINVOKE.ABS_SUMMARY_INFO_absi_type_get(swigCPtr);
      return ret;
    } 
  }

  public int absi_cdecl {
    set {
      icmoPINVOKE.ABS_SUMMARY_INFO_absi_cdecl_set(swigCPtr, value);
    } 
    get {
      int ret = icmoPINVOKE.ABS_SUMMARY_INFO_absi_cdecl_get(swigCPtr);
      return ret;
    } 
  }

  public int absi_trancheno {
    set {
      icmoPINVOKE.ABS_SUMMARY_INFO_absi_trancheno_set(swigCPtr, value);
    } 
    get {
      int ret = icmoPINVOKE.ABS_SUMMARY_INFO_absi_trancheno_get(swigCPtr);
      return ret;
    } 
  }

  public int absi_tranchenotype {
    set {
      icmoPINVOKE.ABS_SUMMARY_INFO_absi_tranchenotype_set(swigCPtr, value);
    } 
    get {
      int ret = icmoPINVOKE.ABS_SUMMARY_INFO_absi_tranchenotype_get(swigCPtr);
      return ret;
    } 
  }

  public char absi_data_in_cdi {
    set {
      icmoPINVOKE.ABS_SUMMARY_INFO_absi_data_in_cdi_set(swigCPtr, value);
    } 
    get {
      char ret = icmoPINVOKE.ABS_SUMMARY_INFO_absi_data_in_cdi_get(swigCPtr);
      return ret;
    } 
  }

  public char absi_internal {
    set {
      icmoPINVOKE.ABS_SUMMARY_INFO_absi_internal_set(swigCPtr, value);
    } 
    get {
      char ret = icmoPINVOKE.ABS_SUMMARY_INFO_absi_internal_get(swigCPtr);
      return ret;
    } 
  }

  public char absi_mkabs_rolled {
    set {
      icmoPINVOKE.ABS_SUMMARY_INFO_absi_mkabs_rolled_set(swigCPtr, value);
    } 
    get {
      char ret = icmoPINVOKE.ABS_SUMMARY_INFO_absi_mkabs_rolled_get(swigCPtr);
      return ret;
    } 
  }

  public char absi_have_data {
    set {
      icmoPINVOKE.ABS_SUMMARY_INFO_absi_have_data_set(swigCPtr, value);
    } 
    get {
      char ret = icmoPINVOKE.ABS_SUMMARY_INFO_absi_have_data_get(swigCPtr);
      return ret;
    } 
  }

  public int absi_footnote {
    set {
      icmoPINVOKE.ABS_SUMMARY_INFO_absi_footnote_set(swigCPtr, value);
    } 
    get {
      int ret = icmoPINVOKE.ABS_SUMMARY_INFO_absi_footnote_get(swigCPtr);
      return ret;
    } 
  }

  public int absi_data_cdudate {
    set {
      icmoPINVOKE.ABS_SUMMARY_INFO_absi_data_cdudate_set(swigCPtr, value);
    } 
    get {
      int ret = icmoPINVOKE.ABS_SUMMARY_INFO_absi_data_cdudate_get(swigCPtr);
      return ret;
    } 
  }

  public string absi_eval_toptkn {
    set {
      icmoPINVOKE.ABS_SUMMARY_INFO_absi_eval_toptkn_set(swigCPtr, value);
    } 
    get {
      string ret = icmoPINVOKE.ABS_SUMMARY_INFO_absi_eval_toptkn_get(swigCPtr);
      return ret;
    } 
  }

  public ABS_SUMMARY_INFO_absi_data absi_data {
    get {
      IntPtr cPtr = icmoPINVOKE.ABS_SUMMARY_INFO_absi_data_get(swigCPtr);
      ABS_SUMMARY_INFO_absi_data ret = (cPtr == IntPtr.Zero) ? null : new ABS_SUMMARY_INFO_absi_data(cPtr, false);
      return ret;
    } 
  }

  public ABS_SUMMARY_INFO() : this(icmoPINVOKE.new_ABS_SUMMARY_INFO(), true) {
  }

}
