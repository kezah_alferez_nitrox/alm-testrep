/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.1
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */


using System;
using System.Runtime.InteropServices;

public class ICMO_SCRIPT : IDisposable {
  private HandleRef swigCPtr;
  protected bool swigCMemOwn;

  internal ICMO_SCRIPT(IntPtr cPtr, bool cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = new HandleRef(this, cPtr);
  }

  internal static HandleRef getCPtr(ICMO_SCRIPT obj) {
    return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.swigCPtr;
  }

  ~ICMO_SCRIPT() {
    Dispose();
  }

  public virtual void Dispose() {
    lock(this) {
      if (swigCPtr.Handle != IntPtr.Zero) {
        if (swigCMemOwn) {
          swigCMemOwn = false;
          icmoPINVOKE.delete_ICMO_SCRIPT(swigCPtr);
        }
        swigCPtr = new HandleRef(null, IntPtr.Zero);
      }
      GC.SuppressFinalize(this);
    }
  }

  public string script_buff {
    set {
      icmoPINVOKE.ICMO_SCRIPT_script_buff_set(swigCPtr, value);
    } 
    get {
      string ret = icmoPINVOKE.ICMO_SCRIPT_script_buff_get(swigCPtr);
      return ret;
    } 
  }

  public string script_filename {
    set {
      icmoPINVOKE.ICMO_SCRIPT_script_filename_set(swigCPtr, value);
    } 
    get {
      string ret = icmoPINVOKE.ICMO_SCRIPT_script_filename_get(swigCPtr);
      return ret;
    } 
  }

  public string script_name {
    set {
      icmoPINVOKE.ICMO_SCRIPT_script_name_set(swigCPtr, value);
    } 
    get {
      string ret = icmoPINVOKE.ICMO_SCRIPT_script_name_get(swigCPtr);
      return ret;
    } 
  }

  public int script_type {
    set {
      icmoPINVOKE.ICMO_SCRIPT_script_type_set(swigCPtr, value);
    } 
    get {
      int ret = icmoPINVOKE.ICMO_SCRIPT_script_type_get(swigCPtr);
      return ret;
    } 
  }

  public int script_loose_syntax {
    set {
      icmoPINVOKE.ICMO_SCRIPT_script_loose_syntax_set(swigCPtr, value);
    } 
    get {
      int ret = icmoPINVOKE.ICMO_SCRIPT_script_loose_syntax_get(swigCPtr);
      return ret;
    } 
  }

  public SWIGTYPE_p_p_char script_symlist {
    set {
      icmoPINVOKE.ICMO_SCRIPT_script_symlist_set(swigCPtr, SWIGTYPE_p_p_char.getCPtr(value));
    } 
    get {
      IntPtr cPtr = icmoPINVOKE.ICMO_SCRIPT_script_symlist_get(swigCPtr);
      SWIGTYPE_p_p_char ret = (cPtr == IntPtr.Zero) ? null : new SWIGTYPE_p_p_char(cPtr, false);
      return ret;
    } 
  }

  public int script_nsymlist {
    set {
      icmoPINVOKE.ICMO_SCRIPT_script_nsymlist_set(swigCPtr, value);
    } 
    get {
      int ret = icmoPINVOKE.ICMO_SCRIPT_script_nsymlist_get(swigCPtr);
      return ret;
    } 
  }

  public SWIGTYPE_p_p_char script_funclist {
    set {
      icmoPINVOKE.ICMO_SCRIPT_script_funclist_set(swigCPtr, SWIGTYPE_p_p_char.getCPtr(value));
    } 
    get {
      IntPtr cPtr = icmoPINVOKE.ICMO_SCRIPT_script_funclist_get(swigCPtr);
      SWIGTYPE_p_p_char ret = (cPtr == IntPtr.Zero) ? null : new SWIGTYPE_p_p_char(cPtr, false);
      return ret;
    } 
  }

  public int script_nfunclist {
    set {
      icmoPINVOKE.ICMO_SCRIPT_script_nfunclist_set(swigCPtr, value);
    } 
    get {
      int ret = icmoPINVOKE.ICMO_SCRIPT_script_nfunclist_get(swigCPtr);
      return ret;
    } 
  }

  public SWIGTYPE_p_f_p_struct__ICMO_p_struct_ICMO_SCRIPT_HANDLE_int_int__int script_sym_fcn {
    set {
      icmoPINVOKE.ICMO_SCRIPT_script_sym_fcn_set(swigCPtr, SWIGTYPE_p_f_p_struct__ICMO_p_struct_ICMO_SCRIPT_HANDLE_int_int__int.getCPtr(value));
    } 
    get {
      IntPtr cPtr = icmoPINVOKE.ICMO_SCRIPT_script_sym_fcn_get(swigCPtr);
      SWIGTYPE_p_f_p_struct__ICMO_p_struct_ICMO_SCRIPT_HANDLE_int_int__int ret = (cPtr == IntPtr.Zero) ? null : new SWIGTYPE_p_f_p_struct__ICMO_p_struct_ICMO_SCRIPT_HANDLE_int_int__int(cPtr, false);
      return ret;
    } 
  }

  public SWIGTYPE_p_f_p_struct__ICMO_p_struct_ICMO_SCRIPT_HANDLE_int_p_struct_ICMO_DATAVAL_int__int script_func_fcn {
    set {
      icmoPINVOKE.ICMO_SCRIPT_script_func_fcn_set(swigCPtr, SWIGTYPE_p_f_p_struct__ICMO_p_struct_ICMO_SCRIPT_HANDLE_int_p_struct_ICMO_DATAVAL_int__int.getCPtr(value));
    } 
    get {
      IntPtr cPtr = icmoPINVOKE.ICMO_SCRIPT_script_func_fcn_get(swigCPtr);
      SWIGTYPE_p_f_p_struct__ICMO_p_struct_ICMO_SCRIPT_HANDLE_int_p_struct_ICMO_DATAVAL_int__int ret = (cPtr == IntPtr.Zero) ? null : new SWIGTYPE_p_f_p_struct__ICMO_p_struct_ICMO_SCRIPT_HANDLE_int_p_struct_ICMO_DATAVAL_int__int(cPtr, false);
      return ret;
    } 
  }

  public SWIGTYPE_p_p_char script_errmsg_list {
    set {
      icmoPINVOKE.ICMO_SCRIPT_script_errmsg_list_set(swigCPtr, SWIGTYPE_p_p_char.getCPtr(value));
    } 
    get {
      IntPtr cPtr = icmoPINVOKE.ICMO_SCRIPT_script_errmsg_list_get(swigCPtr);
      SWIGTYPE_p_p_char ret = (cPtr == IntPtr.Zero) ? null : new SWIGTYPE_p_p_char(cPtr, false);
      return ret;
    } 
  }

  public SWIGTYPE_p_void script_internalp {
    set {
      icmoPINVOKE.ICMO_SCRIPT_script_internalp_set(swigCPtr, SWIGTYPE_p_void.getCPtr(value));
    } 
    get {
      IntPtr cPtr = icmoPINVOKE.ICMO_SCRIPT_script_internalp_get(swigCPtr);
      SWIGTYPE_p_void ret = (cPtr == IntPtr.Zero) ? null : new SWIGTYPE_p_void(cPtr, false);
      return ret;
    } 
  }

  public ICMO_SCRIPT() : this(icmoPINVOKE.new_ICMO_SCRIPT(), true) {
  }

}
