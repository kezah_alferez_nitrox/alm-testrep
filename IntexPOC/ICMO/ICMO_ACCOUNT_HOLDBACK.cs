/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.1
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */


using System;
using System.Runtime.InteropServices;

public class ICMO_ACCOUNT_HOLDBACK : IDisposable {
  private HandleRef swigCPtr;
  protected bool swigCMemOwn;

  internal ICMO_ACCOUNT_HOLDBACK(IntPtr cPtr, bool cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = new HandleRef(this, cPtr);
  }

  internal static HandleRef getCPtr(ICMO_ACCOUNT_HOLDBACK obj) {
    return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.swigCPtr;
  }

  ~ICMO_ACCOUNT_HOLDBACK() {
    Dispose();
  }

  public virtual void Dispose() {
    lock(this) {
      if (swigCPtr.Handle != IntPtr.Zero) {
        if (swigCMemOwn) {
          swigCMemOwn = false;
          icmoPINVOKE.delete_ICMO_ACCOUNT_HOLDBACK(swigCPtr);
        }
        swigCPtr = new HandleRef(null, IntPtr.Zero);
      }
      GC.SuppressFinalize(this);
    }
  }

  public int icmoahb_holdback_unit {
    set {
      icmoPINVOKE.ICMO_ACCOUNT_HOLDBACK_icmoahb_holdback_unit_set(swigCPtr, value);
    } 
    get {
      int ret = icmoPINVOKE.ICMO_ACCOUNT_HOLDBACK_icmoahb_holdback_unit_get(swigCPtr);
      return ret;
    } 
  }

  public int icmoahb_n_holdback {
    set {
      icmoPINVOKE.ICMO_ACCOUNT_HOLDBACK_icmoahb_n_holdback_set(swigCPtr, value);
    } 
    get {
      int ret = icmoPINVOKE.ICMO_ACCOUNT_HOLDBACK_icmoahb_n_holdback_get(swigCPtr);
      return ret;
    } 
  }

  public SWIGTYPE_p_double icmoahb_v_holdback {
    set {
      icmoPINVOKE.ICMO_ACCOUNT_HOLDBACK_icmoahb_v_holdback_set(swigCPtr, SWIGTYPE_p_double.getCPtr(value));
    } 
    get {
      IntPtr cPtr = icmoPINVOKE.ICMO_ACCOUNT_HOLDBACK_icmoahb_v_holdback_get(swigCPtr);
      SWIGTYPE_p_double ret = (cPtr == IntPtr.Zero) ? null : new SWIGTYPE_p_double(cPtr, false);
      return ret;
    } 
  }

  public int icmoahb_n_release_frac {
    set {
      icmoPINVOKE.ICMO_ACCOUNT_HOLDBACK_icmoahb_n_release_frac_set(swigCPtr, value);
    } 
    get {
      int ret = icmoPINVOKE.ICMO_ACCOUNT_HOLDBACK_icmoahb_n_release_frac_get(swigCPtr);
      return ret;
    } 
  }

  public SWIGTYPE_p_double icmoahb_v_release_frac {
    set {
      icmoPINVOKE.ICMO_ACCOUNT_HOLDBACK_icmoahb_v_release_frac_set(swigCPtr, SWIGTYPE_p_double.getCPtr(value));
    } 
    get {
      IntPtr cPtr = icmoPINVOKE.ICMO_ACCOUNT_HOLDBACK_icmoahb_v_release_frac_get(swigCPtr);
      SWIGTYPE_p_double ret = (cPtr == IntPtr.Zero) ? null : new SWIGTYPE_p_double(cPtr, false);
      return ret;
    } 
  }

  public ICMO_ACCOUNT_HOLDBACK() : this(icmoPINVOKE.new_ICMO_ACCOUNT_HOLDBACK(), true) {
  }

}
