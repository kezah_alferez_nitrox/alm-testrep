/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.1
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */


using System;
using System.Runtime.InteropServices;

public class CREDIT_SUPPORT : IDisposable {
  private HandleRef swigCPtr;
  protected bool swigCMemOwn;

  internal CREDIT_SUPPORT(IntPtr cPtr, bool cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = new HandleRef(this, cPtr);
  }

  internal static HandleRef getCPtr(CREDIT_SUPPORT obj) {
    return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.swigCPtr;
  }

  ~CREDIT_SUPPORT() {
    Dispose();
  }

  public virtual void Dispose() {
    lock(this) {
      if (swigCPtr.Handle != IntPtr.Zero) {
        if (swigCMemOwn) {
          swigCMemOwn = false;
          icmoPINVOKE.delete_CREDIT_SUPPORT(swigCPtr);
        }
        swigCPtr = new HandleRef(null, IntPtr.Zero);
      }
      GC.SuppressFinalize(this);
    }
  }

  public int credsupp_basis_type {
    set {
      icmoPINVOKE.CREDIT_SUPPORT_credsupp_basis_type_set(swigCPtr, value);
    } 
    get {
      int ret = icmoPINVOKE.CREDIT_SUPPORT_credsupp_basis_type_get(swigCPtr);
      return ret;
    } 
  }

  public int credsupp_writedown_by_rules {
    set {
      icmoPINVOKE.CREDIT_SUPPORT_credsupp_writedown_by_rules_set(swigCPtr, value);
    } 
    get {
      int ret = icmoPINVOKE.CREDIT_SUPPORT_credsupp_writedown_by_rules_get(swigCPtr);
      return ret;
    } 
  }

  public int credsupp_fully_insured {
    set {
      icmoPINVOKE.CREDIT_SUPPORT_credsupp_fully_insured_set(swigCPtr, value);
    } 
    get {
      int ret = icmoPINVOKE.CREDIT_SUPPORT_credsupp_fully_insured_get(swigCPtr);
      return ret;
    } 
  }

  public int credsupp_explicit_formula {
    set {
      icmoPINVOKE.CREDIT_SUPPORT_credsupp_explicit_formula_set(swigCPtr, value);
    } 
    get {
      int ret = icmoPINVOKE.CREDIT_SUPPORT_credsupp_explicit_formula_get(swigCPtr);
      return ret;
    } 
  }

  public double credsupp_total_amt {
    set {
      icmoPINVOKE.CREDIT_SUPPORT_credsupp_total_amt_set(swigCPtr, value);
    } 
    get {
      double ret = icmoPINVOKE.CREDIT_SUPPORT_credsupp_total_amt_get(swigCPtr);
      return ret;
    } 
  }

  public double credsupp_basis_amt {
    set {
      icmoPINVOKE.CREDIT_SUPPORT_credsupp_basis_amt_set(swigCPtr, value);
    } 
    get {
      double ret = icmoPINVOKE.CREDIT_SUPPORT_credsupp_basis_amt_get(swigCPtr);
      return ret;
    } 
  }

  public double credsupp_basis_amt_defeased {
    set {
      icmoPINVOKE.CREDIT_SUPPORT_credsupp_basis_amt_defeased_set(swigCPtr, value);
    } 
    get {
      double ret = icmoPINVOKE.CREDIT_SUPPORT_credsupp_basis_amt_defeased_get(swigCPtr);
      return ret;
    } 
  }

  public double credsupp_subordinated_amt {
    set {
      icmoPINVOKE.CREDIT_SUPPORT_credsupp_subordinated_amt_set(swigCPtr, value);
    } 
    get {
      double ret = icmoPINVOKE.CREDIT_SUPPORT_credsupp_subordinated_amt_get(swigCPtr);
      return ret;
    } 
  }

  public double credsupp_guaranty_amt {
    set {
      icmoPINVOKE.CREDIT_SUPPORT_credsupp_guaranty_amt_set(swigCPtr, value);
    } 
    get {
      double ret = icmoPINVOKE.CREDIT_SUPPORT_credsupp_guaranty_amt_get(swigCPtr);
      return ret;
    } 
  }

  public double credsupp_letter_of_credit_amt {
    set {
      icmoPINVOKE.CREDIT_SUPPORT_credsupp_letter_of_credit_amt_set(swigCPtr, value);
    } 
    get {
      double ret = icmoPINVOKE.CREDIT_SUPPORT_credsupp_letter_of_credit_amt_get(swigCPtr);
      return ret;
    } 
  }

  public double credsupp_reserve_fund_amt {
    set {
      icmoPINVOKE.CREDIT_SUPPORT_credsupp_reserve_fund_amt_set(swigCPtr, value);
    } 
    get {
      double ret = icmoPINVOKE.CREDIT_SUPPORT_credsupp_reserve_fund_amt_get(swigCPtr);
      return ret;
    } 
  }

  public double credsupp_excess_interest_amt {
    set {
      icmoPINVOKE.CREDIT_SUPPORT_credsupp_excess_interest_amt_set(swigCPtr, value);
    } 
    get {
      double ret = icmoPINVOKE.CREDIT_SUPPORT_credsupp_excess_interest_amt_get(swigCPtr);
      return ret;
    } 
  }

  public double credsupp_overcollat_amt {
    set {
      icmoPINVOKE.CREDIT_SUPPORT_credsupp_overcollat_amt_set(swigCPtr, value);
    } 
    get {
      double ret = icmoPINVOKE.CREDIT_SUPPORT_credsupp_overcollat_amt_get(swigCPtr);
      return ret;
    } 
  }

  public CREDIT_SUPPORT() : this(icmoPINVOKE.new_CREDIT_SUPPORT(), true) {
  }

}
