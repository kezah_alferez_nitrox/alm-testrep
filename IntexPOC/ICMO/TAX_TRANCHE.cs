/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.1
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */


using System;
using System.Runtime.InteropServices;

public class TAX_TRANCHE : IDisposable {
  private HandleRef swigCPtr;
  protected bool swigCMemOwn;

  internal TAX_TRANCHE(IntPtr cPtr, bool cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = new HandleRef(this, cPtr);
  }

  internal static HandleRef getCPtr(TAX_TRANCHE obj) {
    return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.swigCPtr;
  }

  ~TAX_TRANCHE() {
    Dispose();
  }

  public virtual void Dispose() {
    lock(this) {
      if (swigCPtr.Handle != IntPtr.Zero) {
        if (swigCMemOwn) {
          swigCMemOwn = false;
          icmoPINVOKE.delete_TAX_TRANCHE(swigCPtr);
        }
        swigCPtr = new HandleRef(null, IntPtr.Zero);
      }
      GC.SuppressFinalize(this);
    }
  }

  public int ttr_trno {
    set {
      icmoPINVOKE.TAX_TRANCHE_ttr_trno_set(swigCPtr, value);
    } 
    get {
      int ret = icmoPINVOKE.TAX_TRANCHE_ttr_trno_get(swigCPtr);
      return ret;
    } 
  }

  public double ttr_issue_price {
    set {
      icmoPINVOKE.TAX_TRANCHE_ttr_issue_price_set(swigCPtr, value);
    } 
    get {
      double ret = icmoPINVOKE.TAX_TRANCHE_ttr_issue_price_get(swigCPtr);
      return ret;
    } 
  }

  public double ttr_issue_price100 {
    set {
      icmoPINVOKE.TAX_TRANCHE_ttr_issue_price100_set(swigCPtr, value);
    } 
    get {
      double ret = icmoPINVOKE.TAX_TRANCHE_ttr_issue_price100_get(swigCPtr);
      return ret;
    } 
  }

  public double ttr_issue_yield {
    set {
      icmoPINVOKE.TAX_TRANCHE_ttr_issue_yield_set(swigCPtr, value);
    } 
    get {
      double ret = icmoPINVOKE.TAX_TRANCHE_ttr_issue_yield_get(swigCPtr);
      return ret;
    } 
  }

  public int ttr_taxcf_calc_type {
    set {
      icmoPINVOKE.TAX_TRANCHE_ttr_taxcf_calc_type_set(swigCPtr, value);
    } 
    get {
      int ret = icmoPINVOKE.TAX_TRANCHE_ttr_taxcf_calc_type_get(swigCPtr);
      return ret;
    } 
  }

  public TAX_TRUST ttr_residual_of {
    set {
      icmoPINVOKE.TAX_TRANCHE_ttr_residual_of_set(swigCPtr, TAX_TRUST.getCPtr(value));
    } 
    get {
      IntPtr cPtr = icmoPINVOKE.TAX_TRANCHE_ttr_residual_of_get(swigCPtr);
      TAX_TRUST ret = (cPtr == IntPtr.Zero) ? null : new TAX_TRUST(cPtr, false);
      return ret;
    } 
  }

  public TAX_TRANCHE() : this(icmoPINVOKE.new_TAX_TRANCHE(), true) {
  }

}
