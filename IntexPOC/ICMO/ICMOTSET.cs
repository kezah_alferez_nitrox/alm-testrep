/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.1
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */


using System;
using System.Runtime.InteropServices;

public class ICMOTSET : IDisposable {
  private HandleRef swigCPtr;
  protected bool swigCMemOwn;

  internal ICMOTSET(IntPtr cPtr, bool cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = new HandleRef(this, cPtr);
  }

  internal static HandleRef getCPtr(ICMOTSET obj) {
    return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.swigCPtr;
  }

  ~ICMOTSET() {
    Dispose();
  }

  public virtual void Dispose() {
    lock(this) {
      if (swigCPtr.Handle != IntPtr.Zero) {
        if (swigCMemOwn) {
          swigCMemOwn = false;
          icmoPINVOKE.delete_ICMOTSET(swigCPtr);
        }
        swigCPtr = new HandleRef(null, IntPtr.Zero);
      }
      GC.SuppressFinalize(this);
    }
  }

  public int itset_future_trans_mode {
    set {
      icmoPINVOKE.ICMOTSET_itset_future_trans_mode_set(swigCPtr, value);
    } 
    get {
      int ret = icmoPINVOKE.ICMOTSET_itset_future_trans_mode_get(swigCPtr);
      return ret;
    } 
  }

  public ICMOTSET() : this(icmoPINVOKE.new_ICMOTSET(), true) {
  }

}
