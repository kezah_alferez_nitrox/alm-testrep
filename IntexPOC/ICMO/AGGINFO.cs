/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.1
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */


using System;
using System.Runtime.InteropServices;

public class AGGINFO : IDisposable {
  private HandleRef swigCPtr;
  protected bool swigCMemOwn;

  internal AGGINFO(IntPtr cPtr, bool cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = new HandleRef(this, cPtr);
  }

  internal static HandleRef getCPtr(AGGINFO obj) {
    return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.swigCPtr;
  }

  ~AGGINFO() {
    Dispose();
  }

  public virtual void Dispose() {
    lock(this) {
      if (swigCPtr.Handle != IntPtr.Zero) {
        if (swigCMemOwn) {
          swigCMemOwn = false;
          icmoPINVOKE.delete_AGGINFO(swigCPtr);
        }
        swigCPtr = new HandleRef(null, IntPtr.Zero);
      }
      GC.SuppressFinalize(this);
    }
  }

  public string ai_next {
    set {
      icmoPINVOKE.AGGINFO_ai_next_set(swigCPtr, value);
    } 
    get {
      string ret = icmoPINVOKE.AGGINFO_ai_next_get(swigCPtr);
      return ret;
    } 
  }

  public int ai_agginfo_type {
    set {
      icmoPINVOKE.AGGINFO_ai_agginfo_type_set(swigCPtr, value);
    } 
    get {
      int ret = icmoPINVOKE.AGGINFO_ai_agginfo_type_get(swigCPtr);
      return ret;
    } 
  }

  public int ai_groupno {
    set {
      icmoPINVOKE.AGGINFO_ai_groupno_set(swigCPtr, value);
    } 
    get {
      int ret = icmoPINVOKE.AGGINFO_ai_groupno_get(swigCPtr);
      return ret;
    } 
  }

  public double ai_value {
    set {
      icmoPINVOKE.AGGINFO_ai_value_set(swigCPtr, value);
    } 
    get {
      double ret = icmoPINVOKE.AGGINFO_ai_value_get(swigCPtr);
      return ret;
    } 
  }

  public AGGINFO() : this(icmoPINVOKE.new_AGGINFO(), true) {
  }

}
