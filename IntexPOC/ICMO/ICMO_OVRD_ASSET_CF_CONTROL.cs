/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.1
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */


using System;
using System.Runtime.InteropServices;

public class ICMO_OVRD_ASSET_CF_CONTROL : IDisposable {
  private HandleRef swigCPtr;
  protected bool swigCMemOwn;

  internal ICMO_OVRD_ASSET_CF_CONTROL(IntPtr cPtr, bool cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = new HandleRef(this, cPtr);
  }

  internal static HandleRef getCPtr(ICMO_OVRD_ASSET_CF_CONTROL obj) {
    return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.swigCPtr;
  }

  ~ICMO_OVRD_ASSET_CF_CONTROL() {
    Dispose();
  }

  public virtual void Dispose() {
    lock(this) {
      if (swigCPtr.Handle != IntPtr.Zero) {
        if (swigCMemOwn) {
          swigCMemOwn = false;
          icmoPINVOKE.delete_ICMO_OVRD_ASSET_CF_CONTROL(swigCPtr);
        }
        swigCPtr = new HandleRef(null, IntPtr.Zero);
      }
      GC.SuppressFinalize(this);
    }
  }

  public SWIGTYPE_p_void ovrdcf_userp {
    set {
      icmoPINVOKE.ICMO_OVRD_ASSET_CF_CONTROL_ovrdcf_userp_set(swigCPtr, SWIGTYPE_p_void.getCPtr(value));
    } 
    get {
      IntPtr cPtr = icmoPINVOKE.ICMO_OVRD_ASSET_CF_CONTROL_ovrdcf_userp_get(swigCPtr);
      SWIGTYPE_p_void ret = (cPtr == IntPtr.Zero) ? null : new SWIGTYPE_p_void(cPtr, false);
      return ret;
    } 
  }

  public int ovrdcf_status {
    set {
      icmoPINVOKE.ICMO_OVRD_ASSET_CF_CONTROL_ovrdcf_status_set(swigCPtr, value);
    } 
    get {
      int ret = icmoPINVOKE.ICMO_OVRD_ASSET_CF_CONTROL_ovrdcf_status_get(swigCPtr);
      return ret;
    } 
  }

  public int ovrdcf_n_req_vectors {
    set {
      icmoPINVOKE.ICMO_OVRD_ASSET_CF_CONTROL_ovrdcf_n_req_vectors_set(swigCPtr, value);
    } 
    get {
      int ret = icmoPINVOKE.ICMO_OVRD_ASSET_CF_CONTROL_ovrdcf_n_req_vectors_get(swigCPtr);
      return ret;
    } 
  }

  public SWIGTYPE_p_int ovrdcf_v_req_vectors {
    set {
      icmoPINVOKE.ICMO_OVRD_ASSET_CF_CONTROL_ovrdcf_v_req_vectors_set(swigCPtr, SWIGTYPE_p_int.getCPtr(value));
    } 
    get {
      IntPtr cPtr = icmoPINVOKE.ICMO_OVRD_ASSET_CF_CONTROL_ovrdcf_v_req_vectors_get(swigCPtr);
      SWIGTYPE_p_int ret = (cPtr == IntPtr.Zero) ? null : new SWIGTYPE_p_int(cPtr, false);
      return ret;
    } 
  }

  public int ovrdcf_deal_required_base_determdate {
    set {
      icmoPINVOKE.ICMO_OVRD_ASSET_CF_CONTROL_ovrdcf_deal_required_base_determdate_set(swigCPtr, value);
    } 
    get {
      int ret = icmoPINVOKE.ICMO_OVRD_ASSET_CF_CONTROL_ovrdcf_deal_required_base_determdate_get(swigCPtr);
      return ret;
    } 
  }

  public int ovrdcf_ctrl_flag {
    set {
      icmoPINVOKE.ICMO_OVRD_ASSET_CF_CONTROL_ovrdcf_ctrl_flag_set(swigCPtr, value);
    } 
    get {
      int ret = icmoPINVOKE.ICMO_OVRD_ASSET_CF_CONTROL_ovrdcf_ctrl_flag_get(swigCPtr);
      return ret;
    } 
  }

  public int ovrdcf_asset_provided_base_paydate {
    set {
      icmoPINVOKE.ICMO_OVRD_ASSET_CF_CONTROL_ovrdcf_asset_provided_base_paydate_set(swigCPtr, value);
    } 
    get {
      int ret = icmoPINVOKE.ICMO_OVRD_ASSET_CF_CONTROL_ovrdcf_asset_provided_base_paydate_get(swigCPtr);
      return ret;
    } 
  }

  public double ovrdcf_asset_provided_face_amt {
    set {
      icmoPINVOKE.ICMO_OVRD_ASSET_CF_CONTROL_ovrdcf_asset_provided_face_amt_set(swigCPtr, value);
    } 
    get {
      double ret = icmoPINVOKE.ICMO_OVRD_ASSET_CF_CONTROL_ovrdcf_asset_provided_face_amt_get(swigCPtr);
      return ret;
    } 
  }

  public ICMO_OVRD_ASSET_CF_CONTROL() : this(icmoPINVOKE.new_ICMO_OVRD_ASSET_CF_CONTROL(), true) {
  }

}
