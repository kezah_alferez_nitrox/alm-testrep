/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.1
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */


using System;
using System.Runtime.InteropServices;

public class IOVERRIDEMISC : IDisposable {
  private HandleRef swigCPtr;
  protected bool swigCMemOwn;

  internal IOVERRIDEMISC(IntPtr cPtr, bool cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = new HandleRef(this, cPtr);
  }

  internal static HandleRef getCPtr(IOVERRIDEMISC obj) {
    return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.swigCPtr;
  }

  ~IOVERRIDEMISC() {
    Dispose();
  }

  public virtual void Dispose() {
    lock(this) {
      if (swigCPtr.Handle != IntPtr.Zero) {
        if (swigCMemOwn) {
          swigCMemOwn = false;
          icmoPINVOKE.delete_IOVERRIDEMISC(swigCPtr);
        }
        swigCPtr = new HandleRef(null, IntPtr.Zero);
      }
      GC.SuppressFinalize(this);
    }
  }

  public char iover_already_compensated {
    set {
      icmoPINVOKE.IOVERRIDEMISC_iover_already_compensated_set(swigCPtr, value);
    } 
    get {
      char ret = icmoPINVOKE.IOVERRIDEMISC_iover_already_compensated_get(swigCPtr);
      return ret;
    } 
  }

  public char iover_decr_coll_vint {
    set {
      icmoPINVOKE.IOVERRIDEMISC_iover_decr_coll_vint_set(swigCPtr, value);
    } 
    get {
      char ret = icmoPINVOKE.IOVERRIDEMISC_iover_decr_coll_vint_get(swigCPtr);
      return ret;
    } 
  }

  public char iover_shiftint_dontreduce {
    set {
      icmoPINVOKE.IOVERRIDEMISC_iover_shiftint_dontreduce_set(swigCPtr, value);
    } 
    get {
      char ret = icmoPINVOKE.IOVERRIDEMISC_iover_shiftint_dontreduce_get(swigCPtr);
      return ret;
    } 
  }

  public char iover_ignore_neg_nsints {
    set {
      icmoPINVOKE.IOVERRIDEMISC_iover_ignore_neg_nsints_set(swigCPtr, value);
    } 
    get {
      char ret = icmoPINVOKE.IOVERRIDEMISC_iover_ignore_neg_nsints_get(swigCPtr);
      return ret;
    } 
  }

  public double iover_servfee_compensated {
    set {
      icmoPINVOKE.IOVERRIDEMISC_iover_servfee_compensated_set(swigCPtr, value);
    } 
    get {
      double ret = icmoPINVOKE.IOVERRIDEMISC_iover_servfee_compensated_get(swigCPtr);
      return ret;
    } 
  }

  public double iover_xrs_curnet {
    set {
      icmoPINVOKE.IOVERRIDEMISC_iover_xrs_curnet_set(swigCPtr, value);
    } 
    get {
      double ret = icmoPINVOKE.IOVERRIDEMISC_iover_xrs_curnet_get(swigCPtr);
      return ret;
    } 
  }

  public SWIGTYPE_p_double iover_vintshort_fullprepay {
    set {
      icmoPINVOKE.IOVERRIDEMISC_iover_vintshort_fullprepay_set(swigCPtr, SWIGTYPE_p_double.getCPtr(value));
    } 
    get {
      IntPtr cPtr = icmoPINVOKE.IOVERRIDEMISC_iover_vintshort_fullprepay_get(swigCPtr);
      SWIGTYPE_p_double ret = (cPtr == IntPtr.Zero) ? null : new SWIGTYPE_p_double(cPtr, false);
      return ret;
    } 
  }

  public SWIGTYPE_p_double iover_vintshort_partprepay {
    set {
      icmoPINVOKE.IOVERRIDEMISC_iover_vintshort_partprepay_set(swigCPtr, SWIGTYPE_p_double.getCPtr(value));
    } 
    get {
      IntPtr cPtr = icmoPINVOKE.IOVERRIDEMISC_iover_vintshort_partprepay_get(swigCPtr);
      SWIGTYPE_p_double ret = (cPtr == IntPtr.Zero) ? null : new SWIGTYPE_p_double(cPtr, false);
      return ret;
    } 
  }

  public SWIGTYPE_p_double iover_vintshort_loss {
    set {
      icmoPINVOKE.IOVERRIDEMISC_iover_vintshort_loss_set(swigCPtr, SWIGTYPE_p_double.getCPtr(value));
    } 
    get {
      IntPtr cPtr = icmoPINVOKE.IOVERRIDEMISC_iover_vintshort_loss_get(swigCPtr);
      SWIGTYPE_p_double ret = (cPtr == IntPtr.Zero) ? null : new SWIGTYPE_p_double(cPtr, false);
      return ret;
    } 
  }

  public SWIGTYPE_p_double iover_vintshort_ssra {
    set {
      icmoPINVOKE.IOVERRIDEMISC_iover_vintshort_ssra_set(swigCPtr, SWIGTYPE_p_double.getCPtr(value));
    } 
    get {
      IntPtr cPtr = icmoPINVOKE.IOVERRIDEMISC_iover_vintshort_ssra_get(swigCPtr);
      SWIGTYPE_p_double ret = (cPtr == IntPtr.Zero) ? null : new SWIGTYPE_p_double(cPtr, false);
      return ret;
    } 
  }

  public SWIGTYPE_p_double iover_vcollnetrate {
    set {
      icmoPINVOKE.IOVERRIDEMISC_iover_vcollnetrate_set(swigCPtr, SWIGTYPE_p_double.getCPtr(value));
    } 
    get {
      IntPtr cPtr = icmoPINVOKE.IOVERRIDEMISC_iover_vcollnetrate_get(swigCPtr);
      SWIGTYPE_p_double ret = (cPtr == IntPtr.Zero) ? null : new SWIGTYPE_p_double(cPtr, false);
      return ret;
    } 
  }

  public SWIGTYPE_p_double iover_vcollnetmargin {
    set {
      icmoPINVOKE.IOVERRIDEMISC_iover_vcollnetmargin_set(swigCPtr, SWIGTYPE_p_double.getCPtr(value));
    } 
    get {
      IntPtr cPtr = icmoPINVOKE.IOVERRIDEMISC_iover_vcollnetmargin_get(swigCPtr);
      SWIGTYPE_p_double ret = (cPtr == IntPtr.Zero) ? null : new SWIGTYPE_p_double(cPtr, false);
      return ret;
    } 
  }

  public SWIGTYPE_p_double iover_vcollnetlifecap {
    set {
      icmoPINVOKE.IOVERRIDEMISC_iover_vcollnetlifecap_set(swigCPtr, SWIGTYPE_p_double.getCPtr(value));
    } 
    get {
      IntPtr cPtr = icmoPINVOKE.IOVERRIDEMISC_iover_vcollnetlifecap_get(swigCPtr);
      SWIGTYPE_p_double ret = (cPtr == IntPtr.Zero) ? null : new SWIGTYPE_p_double(cPtr, false);
      return ret;
    } 
  }

  public SWIGTYPE_p_double iover_vcollIOrate {
    set {
      icmoPINVOKE.IOVERRIDEMISC_iover_vcollIOrate_set(swigCPtr, SWIGTYPE_p_double.getCPtr(value));
    } 
    get {
      IntPtr cPtr = icmoPINVOKE.IOVERRIDEMISC_iover_vcollIOrate_get(swigCPtr);
      SWIGTYPE_p_double ret = (cPtr == IntPtr.Zero) ? null : new SWIGTYPE_p_double(cPtr, false);
      return ret;
    } 
  }

  public SWIGTYPE_p_double iover_vcollnetrate_um {
    set {
      icmoPINVOKE.IOVERRIDEMISC_iover_vcollnetrate_um_set(swigCPtr, SWIGTYPE_p_double.getCPtr(value));
    } 
    get {
      IntPtr cPtr = icmoPINVOKE.IOVERRIDEMISC_iover_vcollnetrate_um_get(swigCPtr);
      SWIGTYPE_p_double ret = (cPtr == IntPtr.Zero) ? null : new SWIGTYPE_p_double(cPtr, false);
      return ret;
    } 
  }

  public SWIGTYPE_p_double iover_vcollnetratecf_um {
    set {
      icmoPINVOKE.IOVERRIDEMISC_iover_vcollnetratecf_um_set(swigCPtr, SWIGTYPE_p_double.getCPtr(value));
    } 
    get {
      IntPtr cPtr = icmoPINVOKE.IOVERRIDEMISC_iover_vcollnetratecf_um_get(swigCPtr);
      SWIGTYPE_p_double ret = (cPtr == IntPtr.Zero) ? null : new SWIGTYPE_p_double(cPtr, false);
      return ret;
    } 
  }

  public SWIGTYPE_p_double iover_voptint_modify_reduction {
    set {
      icmoPINVOKE.IOVERRIDEMISC_iover_voptint_modify_reduction_set(swigCPtr, SWIGTYPE_p_double.getCPtr(value));
    } 
    get {
      IntPtr cPtr = icmoPINVOKE.IOVERRIDEMISC_iover_voptint_modify_reduction_get(swigCPtr);
      SWIGTYPE_p_double ret = (cPtr == IntPtr.Zero) ? null : new SWIGTYPE_p_double(cPtr, false);
      return ret;
    } 
  }

  public SWIGTYPE_p_double iover_vcollservfee {
    set {
      icmoPINVOKE.IOVERRIDEMISC_iover_vcollservfee_set(swigCPtr, SWIGTYPE_p_double.getCPtr(value));
    } 
    get {
      IntPtr cPtr = icmoPINVOKE.IOVERRIDEMISC_iover_vcollservfee_get(swigCPtr);
      SWIGTYPE_p_double ret = (cPtr == IntPtr.Zero) ? null : new SWIGTYPE_p_double(cPtr, false);
      return ret;
    } 
  }

  public SWIGTYPE_p_double iover_vpart_to_dd {
    set {
      icmoPINVOKE.IOVERRIDEMISC_iover_vpart_to_dd_set(swigCPtr, SWIGTYPE_p_double.getCPtr(value));
    } 
    get {
      IntPtr cPtr = icmoPINVOKE.IOVERRIDEMISC_iover_vpart_to_dd_get(swigCPtr);
      SWIGTYPE_p_double ret = (cPtr == IntPtr.Zero) ? null : new SWIGTYPE_p_double(cPtr, false);
      return ret;
    } 
  }

  public SWIGTYPE_p_double iover_vpart_from_dd {
    set {
      icmoPINVOKE.IOVERRIDEMISC_iover_vpart_from_dd_set(swigCPtr, SWIGTYPE_p_double.getCPtr(value));
    } 
    get {
      IntPtr cPtr = icmoPINVOKE.IOVERRIDEMISC_iover_vpart_from_dd_get(swigCPtr);
      SWIGTYPE_p_double ret = (cPtr == IntPtr.Zero) ? null : new SWIGTYPE_p_double(cPtr, false);
      return ret;
    } 
  }

  public SWIGTYPE_p_double iover_vlosspaccum {
    set {
      icmoPINVOKE.IOVERRIDEMISC_iover_vlosspaccum_set(swigCPtr, SWIGTYPE_p_double.getCPtr(value));
    } 
    get {
      IntPtr cPtr = icmoPINVOKE.IOVERRIDEMISC_iover_vlosspaccum_get(swigCPtr);
      SWIGTYPE_p_double ret = (cPtr == IntPtr.Zero) ? null : new SWIGTYPE_p_double(cPtr, false);
      return ret;
    } 
  }

  public SWIGTYPE_p_double iover_vunrealized_writedown_accum {
    set {
      icmoPINVOKE.IOVERRIDEMISC_iover_vunrealized_writedown_accum_set(swigCPtr, SWIGTYPE_p_double.getCPtr(value));
    } 
    get {
      IntPtr cPtr = icmoPINVOKE.IOVERRIDEMISC_iover_vunrealized_writedown_accum_get(swigCPtr);
      SWIGTYPE_p_double ret = (cPtr == IntPtr.Zero) ? null : new SWIGTYPE_p_double(cPtr, false);
      return ret;
    } 
  }

  public SWIGTYPE_p_double iover_vnotionalbal {
    set {
      icmoPINVOKE.IOVERRIDEMISC_iover_vnotionalbal_set(swigCPtr, SWIGTYPE_p_double.getCPtr(value));
    } 
    get {
      IntPtr cPtr = icmoPINVOKE.IOVERRIDEMISC_iover_vnotionalbal_get(swigCPtr);
      SWIGTYPE_p_double ret = (cPtr == IntPtr.Zero) ? null : new SWIGTYPE_p_double(cPtr, false);
      return ret;
    } 
  }

  public SWIGTYPE_p_double iover_vnegam {
    set {
      icmoPINVOKE.IOVERRIDEMISC_iover_vnegam_set(swigCPtr, SWIGTYPE_p_double.getCPtr(value));
    } 
    get {
      IntPtr cPtr = icmoPINVOKE.IOVERRIDEMISC_iover_vnegam_get(swigCPtr);
      SWIGTYPE_p_double ret = (cPtr == IntPtr.Zero) ? null : new SWIGTYPE_p_double(cPtr, false);
      return ret;
    } 
  }

  public SWIGTYPE_p_double iover_vdraws {
    set {
      icmoPINVOKE.IOVERRIDEMISC_iover_vdraws_set(swigCPtr, SWIGTYPE_p_double.getCPtr(value));
    } 
    get {
      IntPtr cPtr = icmoPINVOKE.IOVERRIDEMISC_iover_vdraws_get(swigCPtr);
      SWIGTYPE_p_double ret = (cPtr == IntPtr.Zero) ? null : new SWIGTYPE_p_double(cPtr, false);
      return ret;
    } 
  }

  public SWIGTYPE_p_double iover_vexcessintcash {
    set {
      icmoPINVOKE.IOVERRIDEMISC_iover_vexcessintcash_set(swigCPtr, SWIGTYPE_p_double.getCPtr(value));
    } 
    get {
      IntPtr cPtr = icmoPINVOKE.IOVERRIDEMISC_iover_vexcessintcash_get(swigCPtr);
      SWIGTYPE_p_double ret = (cPtr == IntPtr.Zero) ? null : new SWIGTYPE_p_double(cPtr, false);
      return ret;
    } 
  }

  public SWIGTYPE_p_double iover_vnetratecf_mtgdue {
    set {
      icmoPINVOKE.IOVERRIDEMISC_iover_vnetratecf_mtgdue_set(swigCPtr, SWIGTYPE_p_double.getCPtr(value));
    } 
    get {
      IntPtr cPtr = icmoPINVOKE.IOVERRIDEMISC_iover_vnetratecf_mtgdue_get(swigCPtr);
      SWIGTYPE_p_double ret = (cPtr == IntPtr.Zero) ? null : new SWIGTYPE_p_double(cPtr, false);
      return ret;
    } 
  }

  public SWIGTYPE_p_double iover_vym {
    set {
      icmoPINVOKE.IOVERRIDEMISC_iover_vym_set(swigCPtr, SWIGTYPE_p_double.getCPtr(value));
    } 
    get {
      IntPtr cPtr = icmoPINVOKE.IOVERRIDEMISC_iover_vym_get(swigCPtr);
      SWIGTYPE_p_double ret = (cPtr == IntPtr.Zero) ? null : new SWIGTYPE_p_double(cPtr, false);
      return ret;
    } 
  }

  public SWIGTYPE_p_double iover_vpreppen {
    set {
      icmoPINVOKE.IOVERRIDEMISC_iover_vpreppen_set(swigCPtr, SWIGTYPE_p_double.getCPtr(value));
    } 
    get {
      IntPtr cPtr = icmoPINVOKE.IOVERRIDEMISC_iover_vpreppen_get(swigCPtr);
      SWIGTYPE_p_double ret = (cPtr == IntPtr.Zero) ? null : new SWIGTYPE_p_double(cPtr, false);
      return ret;
    } 
  }

  public SWIGTYPE_p_double iover_vdeferint {
    set {
      icmoPINVOKE.IOVERRIDEMISC_iover_vdeferint_set(swigCPtr, SWIGTYPE_p_double.getCPtr(value));
    } 
    get {
      IntPtr cPtr = icmoPINVOKE.IOVERRIDEMISC_iover_vdeferint_get(swigCPtr);
      SWIGTYPE_p_double ret = (cPtr == IntPtr.Zero) ? null : new SWIGTYPE_p_double(cPtr, false);
      return ret;
    } 
  }

  public SWIGTYPE_p_p_double iover_xrs_frac {
    set {
      icmoPINVOKE.IOVERRIDEMISC_iover_xrs_frac_set(swigCPtr, SWIGTYPE_p_p_double.getCPtr(value));
    } 
    get {
      IntPtr cPtr = icmoPINVOKE.IOVERRIDEMISC_iover_xrs_frac_get(swigCPtr);
      SWIGTYPE_p_p_double ret = (cPtr == IntPtr.Zero) ? null : new SWIGTYPE_p_p_double(cPtr, false);
      return ret;
    } 
  }

  public SWIGTYPE_p_double iover_vdefeased {
    set {
      icmoPINVOKE.IOVERRIDEMISC_iover_vdefeased_set(swigCPtr, SWIGTYPE_p_double.getCPtr(value));
    } 
    get {
      IntPtr cPtr = icmoPINVOKE.IOVERRIDEMISC_iover_vdefeased_get(swigCPtr);
      SWIGTYPE_p_double ret = (cPtr == IntPtr.Zero) ? null : new SWIGTYPE_p_double(cPtr, false);
      return ret;
    } 
  }

  public AVG_INFO iover_avg_list {
    set {
      icmoPINVOKE.IOVERRIDEMISC_iover_avg_list_set(swigCPtr, AVG_INFO.getCPtr(value));
    } 
    get {
      IntPtr cPtr = icmoPINVOKE.IOVERRIDEMISC_iover_avg_list_get(swigCPtr);
      AVG_INFO ret = (cPtr == IntPtr.Zero) ? null : new AVG_INFO(cPtr, false);
      return ret;
    } 
  }

  public AGGINFO iover_agginfo_list {
    set {
      icmoPINVOKE.IOVERRIDEMISC_iover_agginfo_list_set(swigCPtr, AGGINFO.getCPtr(value));
    } 
    get {
      IntPtr cPtr = icmoPINVOKE.IOVERRIDEMISC_iover_agginfo_list_get(swigCPtr);
      AGGINFO ret = (cPtr == IntPtr.Zero) ? null : new AGGINFO(cPtr, false);
      return ret;
    } 
  }

  public _POOL_INFO iover_pip {
    set {
      icmoPINVOKE.IOVERRIDEMISC_iover_pip_set(swigCPtr, _POOL_INFO.getCPtr(value));
    } 
    get {
      IntPtr cPtr = icmoPINVOKE.IOVERRIDEMISC_iover_pip_get(swigCPtr);
      _POOL_INFO ret = (cPtr == IntPtr.Zero) ? null : new _POOL_INFO(cPtr, false);
      return ret;
    } 
  }

  public PREPHIST iover_cpr {
    set {
      icmoPINVOKE.IOVERRIDEMISC_iover_cpr_set(swigCPtr, PREPHIST.getCPtr(value));
    } 
    get {
      IntPtr cPtr = icmoPINVOKE.IOVERRIDEMISC_iover_cpr_get(swigCPtr);
      PREPHIST ret = (cPtr == IntPtr.Zero) ? null : new PREPHIST(cPtr, false);
      return ret;
    } 
  }

  public PREPHIST iover_psa {
    set {
      icmoPINVOKE.IOVERRIDEMISC_iover_psa_set(swigCPtr, PREPHIST.getCPtr(value));
    } 
    get {
      IntPtr cPtr = icmoPINVOKE.IOVERRIDEMISC_iover_psa_get(swigCPtr);
      PREPHIST ret = (cPtr == IntPtr.Zero) ? null : new PREPHIST(cPtr, false);
      return ret;
    } 
  }

  public SWIGTYPE_p_f_p_struct__ICMO_int_p_char_p_char__double iover_symvar_fcn {
    set {
      icmoPINVOKE.IOVERRIDEMISC_iover_symvar_fcn_set(swigCPtr, SWIGTYPE_p_f_p_struct__ICMO_int_p_char_p_char__double.getCPtr(value));
    } 
    get {
      IntPtr cPtr = icmoPINVOKE.IOVERRIDEMISC_iover_symvar_fcn_get(swigCPtr);
      SWIGTYPE_p_f_p_struct__ICMO_int_p_char_p_char__double ret = (cPtr == IntPtr.Zero) ? null : new SWIGTYPE_p_f_p_struct__ICMO_int_p_char_p_char__double(cPtr, false);
      return ret;
    } 
  }

  public SWIGTYPE_p_f_p_struct__ICMO_int_p_char_int__double iover_index_fcn {
    set {
      icmoPINVOKE.IOVERRIDEMISC_iover_index_fcn_set(swigCPtr, SWIGTYPE_p_f_p_struct__ICMO_int_p_char_int__double.getCPtr(value));
    } 
    get {
      IntPtr cPtr = icmoPINVOKE.IOVERRIDEMISC_iover_index_fcn_get(swigCPtr);
      SWIGTYPE_p_f_p_struct__ICMO_int_p_char_int__double ret = (cPtr == IntPtr.Zero) ? null : new SWIGTYPE_p_f_p_struct__ICMO_int_p_char_int__double(cPtr, false);
      return ret;
    } 
  }

  public SWIGTYPE_p_p_double iover_misc_coll_p {
    set {
      icmoPINVOKE.IOVERRIDEMISC_iover_misc_coll_p_set(swigCPtr, SWIGTYPE_p_p_double.getCPtr(value));
    } 
    get {
      IntPtr cPtr = icmoPINVOKE.IOVERRIDEMISC_iover_misc_coll_p_get(swigCPtr);
      SWIGTYPE_p_p_double ret = (cPtr == IntPtr.Zero) ? null : new SWIGTYPE_p_p_double(cPtr, false);
      return ret;
    } 
  }

  public SWIGTYPE_p_p_double iover_misc_coll_i {
    set {
      icmoPINVOKE.IOVERRIDEMISC_iover_misc_coll_i_set(swigCPtr, SWIGTYPE_p_p_double.getCPtr(value));
    } 
    get {
      IntPtr cPtr = icmoPINVOKE.IOVERRIDEMISC_iover_misc_coll_i_get(swigCPtr);
      SWIGTYPE_p_p_double ret = (cPtr == IntPtr.Zero) ? null : new SWIGTYPE_p_p_double(cPtr, false);
      return ret;
    } 
  }

  public SWIGTYPE_p_double iover_vnonperforming_bal {
    set {
      icmoPINVOKE.IOVERRIDEMISC_iover_vnonperforming_bal_set(swigCPtr, SWIGTYPE_p_double.getCPtr(value));
    } 
    get {
      IntPtr cPtr = icmoPINVOKE.IOVERRIDEMISC_iover_vnonperforming_bal_get(swigCPtr);
      SWIGTYPE_p_double ret = (cPtr == IntPtr.Zero) ? null : new SWIGTYPE_p_double(cPtr, false);
      return ret;
    } 
  }

  public IOVERRIDEMISC() : this(icmoPINVOKE.new_IOVERRIDEMISC(), true) {
  }

}
