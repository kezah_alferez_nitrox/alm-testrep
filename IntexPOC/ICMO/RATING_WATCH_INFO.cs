/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.1
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */


using System;
using System.Runtime.InteropServices;

public class RATING_WATCH_INFO : IDisposable {
  private HandleRef swigCPtr;
  protected bool swigCMemOwn;

  internal RATING_WATCH_INFO(IntPtr cPtr, bool cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = new HandleRef(this, cPtr);
  }

  internal static HandleRef getCPtr(RATING_WATCH_INFO obj) {
    return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.swigCPtr;
  }

  ~RATING_WATCH_INFO() {
    Dispose();
  }

  public virtual void Dispose() {
    lock(this) {
      if (swigCPtr.Handle != IntPtr.Zero) {
        if (swigCMemOwn) {
          swigCMemOwn = false;
          icmoPINVOKE.delete_RATING_WATCH_INFO(swigCPtr);
        }
        swigCPtr = new HandleRef(null, IntPtr.Zero);
      }
      GC.SuppressFinalize(this);
    }
  }

  public string rw_next {
    set {
      icmoPINVOKE.RATING_WATCH_INFO_rw_next_set(swigCPtr, value);
    } 
    get {
      string ret = icmoPINVOKE.RATING_WATCH_INFO_rw_next_get(swigCPtr);
      return ret;
    } 
  }

  public int rw_agency {
    set {
      icmoPINVOKE.RATING_WATCH_INFO_rw_agency_set(swigCPtr, value);
    } 
    get {
      int ret = icmoPINVOKE.RATING_WATCH_INFO_rw_agency_get(swigCPtr);
      return ret;
    } 
  }

  public int rw_action {
    set {
      icmoPINVOKE.RATING_WATCH_INFO_rw_action_set(swigCPtr, value);
    } 
    get {
      int ret = icmoPINVOKE.RATING_WATCH_INFO_rw_action_get(swigCPtr);
      return ret;
    } 
  }

  public int rw_date {
    set {
      icmoPINVOKE.RATING_WATCH_INFO_rw_date_set(swigCPtr, value);
    } 
    get {
      int ret = icmoPINVOKE.RATING_WATCH_INFO_rw_date_get(swigCPtr);
      return ret;
    } 
  }

  public RATING_WATCH_INFO() : this(icmoPINVOKE.new_RATING_WATCH_INFO(), true) {
  }

}
