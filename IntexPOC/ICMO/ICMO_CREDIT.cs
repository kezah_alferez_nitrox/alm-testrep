/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.1
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */


using System;
using System.Runtime.InteropServices;

public class ICMO_CREDIT : IDisposable {
  private HandleRef swigCPtr;
  protected bool swigCMemOwn;

  internal ICMO_CREDIT(IntPtr cPtr, bool cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = new HandleRef(this, cPtr);
  }

  internal static HandleRef getCPtr(ICMO_CREDIT obj) {
    return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.swigCPtr;
  }

  ~ICMO_CREDIT() {
    Dispose();
  }

  public virtual void Dispose() {
    lock(this) {
      if (swigCPtr.Handle != IntPtr.Zero) {
        if (swigCMemOwn) {
          swigCMemOwn = false;
          icmoPINVOKE.delete_ICMO_CREDIT(swigCPtr);
        }
        swigCPtr = new HandleRef(null, IntPtr.Zero);
      }
      GC.SuppressFinalize(this);
    }
  }

  public int credit_creditno {
    set {
      icmoPINVOKE.ICMO_CREDIT_credit_creditno_set(swigCPtr, value);
    } 
    get {
      int ret = icmoPINVOKE.ICMO_CREDIT_credit_creditno_get(swigCPtr);
      return ret;
    } 
  }

  public int credit_ignore {
    set {
      icmoPINVOKE.ICMO_CREDIT_credit_ignore_set(swigCPtr, value);
    } 
    get {
      int ret = icmoPINVOKE.ICMO_CREDIT_credit_ignore_get(swigCPtr);
      return ret;
    } 
  }

  public string credit_by {
    set {
      icmoPINVOKE.ICMO_CREDIT_credit_by_set(swigCPtr, value);
    } 
    get {
      string ret = icmoPINVOKE.ICMO_CREDIT_credit_by_get(swigCPtr);
      return ret;
    } 
  }

  public string credit_name {
    set {
      icmoPINVOKE.ICMO_CREDIT_credit_name_set(swigCPtr, value);
    } 
    get {
      string ret = icmoPINVOKE.ICMO_CREDIT_credit_name_get(swigCPtr);
      return ret;
    } 
  }

  public string credit_full_name {
    set {
      icmoPINVOKE.ICMO_CREDIT_credit_full_name_set(swigCPtr, value);
    } 
    get {
      string ret = icmoPINVOKE.ICMO_CREDIT_credit_full_name_get(swigCPtr);
      return ret;
    } 
  }

  public string credit_description {
    set {
      icmoPINVOKE.ICMO_CREDIT_credit_description_set(swigCPtr, value);
    } 
    get {
      string ret = icmoPINVOKE.ICMO_CREDIT_credit_description_get(swigCPtr);
      return ret;
    } 
  }

  public char credit_flavor {
    set {
      icmoPINVOKE.ICMO_CREDIT_credit_flavor_set(swigCPtr, value);
    } 
    get {
      char ret = icmoPINVOKE.ICMO_CREDIT_credit_flavor_get(swigCPtr);
      return ret;
    } 
  }

  public ICMO_CURRENCY credit_currencyp {
    set {
      icmoPINVOKE.ICMO_CREDIT_credit_currencyp_set(swigCPtr, ICMO_CURRENCY.getCPtr(value));
    } 
    get {
      IntPtr cPtr = icmoPINVOKE.ICMO_CREDIT_credit_currencyp_get(swigCPtr);
      ICMO_CURRENCY ret = (cPtr == IntPtr.Zero) ? null : new ICMO_CURRENCY(cPtr, false);
      return ret;
    } 
  }

  public char credit_type {
    set {
      icmoPINVOKE.ICMO_CREDIT_credit_type_set(swigCPtr, value);
    } 
    get {
      char ret = icmoPINVOKE.ICMO_CREDIT_credit_type_get(swigCPtr);
      return ret;
    } 
  }

  public char credit_subtype {
    set {
      icmoPINVOKE.ICMO_CREDIT_credit_subtype_set(swigCPtr, value);
    } 
    get {
      char ret = icmoPINVOKE.ICMO_CREDIT_credit_subtype_get(swigCPtr);
      return ret;
    } 
  }

  public int credit_on_n_items {
    set {
      icmoPINVOKE.ICMO_CREDIT_credit_on_n_items_set(swigCPtr, value);
    } 
    get {
      int ret = icmoPINVOKE.ICMO_CREDIT_credit_on_n_items_get(swigCPtr);
      return ret;
    } 
  }

  public string credit_on_v_item_types {
    set {
      icmoPINVOKE.ICMO_CREDIT_credit_on_v_item_types_set(swigCPtr, value);
    } 
    get {
      string ret = icmoPINVOKE.ICMO_CREDIT_credit_on_v_item_types_get(swigCPtr);
      return ret;
    } 
  }

  public SWIGTYPE_p_int credit_on_v_item_nums {
    set {
      icmoPINVOKE.ICMO_CREDIT_credit_on_v_item_nums_set(swigCPtr, SWIGTYPE_p_int.getCPtr(value));
    } 
    get {
      IntPtr cPtr = icmoPINVOKE.ICMO_CREDIT_credit_on_v_item_nums_get(swigCPtr);
      SWIGTYPE_p_int ret = (cPtr == IntPtr.Zero) ? null : new SWIGTYPE_p_int(cPtr, false);
      return ret;
    } 
  }

  public ICMO_CREDIT() : this(icmoPINVOKE.new_ICMO_CREDIT(), true) {
  }

}
