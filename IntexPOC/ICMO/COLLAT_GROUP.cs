/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.1
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */


using System;
using System.Runtime.InteropServices;

public class COLLAT_GROUP : IDisposable {
  private HandleRef swigCPtr;
  protected bool swigCMemOwn;

  internal COLLAT_GROUP(IntPtr cPtr, bool cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = new HandleRef(this, cPtr);
  }

  internal static HandleRef getCPtr(COLLAT_GROUP obj) {
    return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.swigCPtr;
  }

  ~COLLAT_GROUP() {
    Dispose();
  }

  public virtual void Dispose() {
    lock(this) {
      if (swigCPtr.Handle != IntPtr.Zero) {
        if (swigCMemOwn) {
          swigCMemOwn = false;
          icmoPINVOKE.delete_COLLAT_GROUP(swigCPtr);
        }
        swigCPtr = new HandleRef(null, IntPtr.Zero);
      }
      GC.SuppressFinalize(this);
    }
  }

  public int grp_groupno {
    set {
      icmoPINVOKE.COLLAT_GROUP_grp_groupno_set(swigCPtr, value);
    } 
    get {
      int ret = icmoPINVOKE.COLLAT_GROUP_grp_groupno_get(swigCPtr);
      return ret;
    } 
  }

  public string grp_name {
    set {
      icmoPINVOKE.COLLAT_GROUP_grp_name_set(swigCPtr, value);
    } 
    get {
      string ret = icmoPINVOKE.COLLAT_GROUP_grp_name_get(swigCPtr);
      return ret;
    } 
  }

  public char grp_is_pseudo {
    set {
      icmoPINVOKE.COLLAT_GROUP_grp_is_pseudo_set(swigCPtr, value);
    } 
    get {
      char ret = icmoPINVOKE.COLLAT_GROUP_grp_is_pseudo_get(swigCPtr);
      return ret;
    } 
  }

  public char grp_is_ground_group {
    set {
      icmoPINVOKE.COLLAT_GROUP_grp_is_ground_group_set(swigCPtr, value);
    } 
    get {
      char ret = icmoPINVOKE.COLLAT_GROUP_grp_is_ground_group_get(swigCPtr);
      return ret;
    } 
  }

  public char grp_prospectus_group {
    set {
      icmoPINVOKE.COLLAT_GROUP_grp_prospectus_group_set(swigCPtr, value);
    } 
    get {
      char ret = icmoPINVOKE.COLLAT_GROUP_grp_prospectus_group_get(swigCPtr);
      return ret;
    } 
  }

  public PREPCURVE grp_ppc_info {
    set {
      icmoPINVOKE.COLLAT_GROUP_grp_ppc_info_set(swigCPtr, PREPCURVE.getCPtr(value));
    } 
    get {
      IntPtr cPtr = icmoPINVOKE.COLLAT_GROUP_grp_ppc_info_get(swigCPtr);
      PREPCURVE ret = (cPtr == IntPtr.Zero) ? null : new PREPCURVE(cPtr, false);
      return ret;
    } 
  }

  public int grp_pricing_units {
    set {
      icmoPINVOKE.COLLAT_GROUP_grp_pricing_units_set(swigCPtr, value);
    } 
    get {
      int ret = icmoPINVOKE.COLLAT_GROUP_grp_pricing_units_get(swigCPtr);
      return ret;
    } 
  }

  public int grp_pricing_nspeed {
    set {
      icmoPINVOKE.COLLAT_GROUP_grp_pricing_nspeed_set(swigCPtr, value);
    } 
    get {
      int ret = icmoPINVOKE.COLLAT_GROUP_grp_pricing_nspeed_get(swigCPtr);
      return ret;
    } 
  }

  public int grp_pricing_draw_units {
    set {
      icmoPINVOKE.COLLAT_GROUP_grp_pricing_draw_units_set(swigCPtr, value);
    } 
    get {
      int ret = icmoPINVOKE.COLLAT_GROUP_grp_pricing_draw_units_get(swigCPtr);
      return ret;
    } 
  }

  public int grp_pricing_draw_nspeed {
    set {
      icmoPINVOKE.COLLAT_GROUP_grp_pricing_draw_nspeed_set(swigCPtr, value);
    } 
    get {
      int ret = icmoPINVOKE.COLLAT_GROUP_grp_pricing_draw_nspeed_get(swigCPtr);
      return ret;
    } 
  }

  public SWIGTYPE_p_int grp_aggunion_id {
    set {
      icmoPINVOKE.COLLAT_GROUP_grp_aggunion_id_set(swigCPtr, SWIGTYPE_p_int.getCPtr(value));
    } 
    get {
      IntPtr cPtr = icmoPINVOKE.COLLAT_GROUP_grp_aggunion_id_get(swigCPtr);
      SWIGTYPE_p_int ret = (cPtr == IntPtr.Zero) ? null : new SWIGTYPE_p_int(cPtr, false);
      return ret;
    } 
  }

  public SWIGTYPE_p_double grp_pricing_vspeed {
    set {
      icmoPINVOKE.COLLAT_GROUP_grp_pricing_vspeed_set(swigCPtr, SWIGTYPE_p_double.getCPtr(value));
    } 
    get {
      IntPtr cPtr = icmoPINVOKE.COLLAT_GROUP_grp_pricing_vspeed_get(swigCPtr);
      SWIGTYPE_p_double ret = (cPtr == IntPtr.Zero) ? null : new SWIGTYPE_p_double(cPtr, false);
      return ret;
    } 
  }

  public SWIGTYPE_p_double grp_pricing_draw_rate_vspeed {
    set {
      icmoPINVOKE.COLLAT_GROUP_grp_pricing_draw_rate_vspeed_set(swigCPtr, SWIGTYPE_p_double.getCPtr(value));
    } 
    get {
      IntPtr cPtr = icmoPINVOKE.COLLAT_GROUP_grp_pricing_draw_rate_vspeed_get(swigCPtr);
      SWIGTYPE_p_double ret = (cPtr == IntPtr.Zero) ? null : new SWIGTYPE_p_double(cPtr, false);
      return ret;
    } 
  }

  public double grp_mastserv_rate {
    set {
      icmoPINVOKE.COLLAT_GROUP_grp_mastserv_rate_set(swigCPtr, value);
    } 
    get {
      double ret = icmoPINVOKE.COLLAT_GROUP_grp_mastserv_rate_get(swigCPtr);
      return ret;
    } 
  }

  public double grp_trustee_rate {
    set {
      icmoPINVOKE.COLLAT_GROUP_grp_trustee_rate_set(swigCPtr, value);
    } 
    get {
      double ret = icmoPINVOKE.COLLAT_GROUP_grp_trustee_rate_get(swigCPtr);
      return ret;
    } 
  }

  public double grp_hazard_limit {
    set {
      icmoPINVOKE.COLLAT_GROUP_grp_hazard_limit_set(swigCPtr, value);
    } 
    get {
      double ret = icmoPINVOKE.COLLAT_GROUP_grp_hazard_limit_get(swigCPtr);
      return ret;
    } 
  }

  public double grp_bankrp_limit {
    set {
      icmoPINVOKE.COLLAT_GROUP_grp_bankrp_limit_set(swigCPtr, value);
    } 
    get {
      double ret = icmoPINVOKE.COLLAT_GROUP_grp_bankrp_limit_get(swigCPtr);
      return ret;
    } 
  }

  public double grp_fraud_limit {
    set {
      icmoPINVOKE.COLLAT_GROUP_grp_fraud_limit_set(swigCPtr, value);
    } 
    get {
      double ret = icmoPINVOKE.COLLAT_GROUP_grp_fraud_limit_get(swigCPtr);
      return ret;
    } 
  }

  public double grp_cdu_hazard_amt {
    set {
      icmoPINVOKE.COLLAT_GROUP_grp_cdu_hazard_amt_set(swigCPtr, value);
    } 
    get {
      double ret = icmoPINVOKE.COLLAT_GROUP_grp_cdu_hazard_amt_get(swigCPtr);
      return ret;
    } 
  }

  public double grp_cdu_bankrp_amt {
    set {
      icmoPINVOKE.COLLAT_GROUP_grp_cdu_bankrp_amt_set(swigCPtr, value);
    } 
    get {
      double ret = icmoPINVOKE.COLLAT_GROUP_grp_cdu_bankrp_amt_get(swigCPtr);
      return ret;
    } 
  }

  public double grp_cdu_fraud_amt {
    set {
      icmoPINVOKE.COLLAT_GROUP_grp_cdu_fraud_amt_set(swigCPtr, value);
    } 
    get {
      double ret = icmoPINVOKE.COLLAT_GROUP_grp_cdu_fraud_amt_get(swigCPtr);
      return ret;
    } 
  }

  public double grp_lastcdu_hazard_amt {
    set {
      icmoPINVOKE.COLLAT_GROUP_grp_lastcdu_hazard_amt_set(swigCPtr, value);
    } 
    get {
      double ret = icmoPINVOKE.COLLAT_GROUP_grp_lastcdu_hazard_amt_get(swigCPtr);
      return ret;
    } 
  }

  public double grp_lastcdu_bankrp_amt {
    set {
      icmoPINVOKE.COLLAT_GROUP_grp_lastcdu_bankrp_amt_set(swigCPtr, value);
    } 
    get {
      double ret = icmoPINVOKE.COLLAT_GROUP_grp_lastcdu_bankrp_amt_get(swigCPtr);
      return ret;
    } 
  }

  public double grp_lastcdu_fraud_amt {
    set {
      icmoPINVOKE.COLLAT_GROUP_grp_lastcdu_fraud_amt_set(swigCPtr, value);
    } 
    get {
      double ret = icmoPINVOKE.COLLAT_GROUP_grp_lastcdu_fraud_amt_get(swigCPtr);
      return ret;
    } 
  }

  public int grp_user_shiftint_nstepdowns {
    set {
      icmoPINVOKE.COLLAT_GROUP_grp_user_shiftint_nstepdowns_set(swigCPtr, value);
    } 
    get {
      int ret = icmoPINVOKE.COLLAT_GROUP_grp_user_shiftint_nstepdowns_get(swigCPtr);
      return ret;
    } 
  }

  public SWIGTYPE_p_double grp_user_shiftint_vstepdowns {
    set {
      icmoPINVOKE.COLLAT_GROUP_grp_user_shiftint_vstepdowns_set(swigCPtr, SWIGTYPE_p_double.getCPtr(value));
    } 
    get {
      IntPtr cPtr = icmoPINVOKE.COLLAT_GROUP_grp_user_shiftint_vstepdowns_get(swigCPtr);
      SWIGTYPE_p_double ret = (cPtr == IntPtr.Zero) ? null : new SWIGTYPE_p_double(cPtr, false);
      return ret;
    } 
  }

  public SWIGTYPE_p_p__LOANSTAT grp_cdu_loanstat_list {
    set {
      icmoPINVOKE.COLLAT_GROUP_grp_cdu_loanstat_list_set(swigCPtr, SWIGTYPE_p_p__LOANSTAT.getCPtr(value));
    } 
    get {
      IntPtr cPtr = icmoPINVOKE.COLLAT_GROUP_grp_cdu_loanstat_list_get(swigCPtr);
      SWIGTYPE_p_p__LOANSTAT ret = (cPtr == IntPtr.Zero) ? null : new SWIGTYPE_p_p__LOANSTAT(cPtr, false);
      return ret;
    } 
  }

  public SWIGTYPE_p_p__LOANSTAT grp_latest_cdu_loanstat_list {
    set {
      icmoPINVOKE.COLLAT_GROUP_grp_latest_cdu_loanstat_list_set(swigCPtr, SWIGTYPE_p_p__LOANSTAT.getCPtr(value));
    } 
    get {
      IntPtr cPtr = icmoPINVOKE.COLLAT_GROUP_grp_latest_cdu_loanstat_list_get(swigCPtr);
      SWIGTYPE_p_p__LOANSTAT ret = (cPtr == IntPtr.Zero) ? null : new SWIGTYPE_p_p__LOANSTAT(cPtr, false);
      return ret;
    } 
  }

  public double grp_pass_thru_rate {
    set {
      icmoPINVOKE.COLLAT_GROUP_grp_pass_thru_rate_set(swigCPtr, value);
    } 
    get {
      double ret = icmoPINVOKE.COLLAT_GROUP_grp_pass_thru_rate_get(swigCPtr);
      return ret;
    } 
  }

  public int grp_has_xrs {
    set {
      icmoPINVOKE.COLLAT_GROUP_grp_has_xrs_set(swigCPtr, value);
    } 
    get {
      int ret = icmoPINVOKE.COLLAT_GROUP_grp_has_xrs_get(swigCPtr);
      return ret;
    } 
  }

  public int grp_n_xrs {
    set {
      icmoPINVOKE.COLLAT_GROUP_grp_n_xrs_set(swigCPtr, value);
    } 
    get {
      int ret = icmoPINVOKE.COLLAT_GROUP_grp_n_xrs_get(swigCPtr);
      return ret;
    } 
  }

  public SWIGTYPE_p_p_ICMOXRS grp_vxrsp {
    set {
      icmoPINVOKE.COLLAT_GROUP_grp_vxrsp_set(swigCPtr, SWIGTYPE_p_p_ICMOXRS.getCPtr(value));
    } 
    get {
      IntPtr cPtr = icmoPINVOKE.COLLAT_GROUP_grp_vxrsp_get(swigCPtr);
      SWIGTYPE_p_p_ICMOXRS ret = (cPtr == IntPtr.Zero) ? null : new SWIGTYPE_p_p_ICMOXRS(cPtr, false);
      return ret;
    } 
  }

  public PREPHIST grp_psa {
    set {
      icmoPINVOKE.COLLAT_GROUP_grp_psa_set(swigCPtr, PREPHIST.getCPtr(value));
    } 
    get {
      IntPtr cPtr = icmoPINVOKE.COLLAT_GROUP_grp_psa_get(swigCPtr);
      PREPHIST ret = (cPtr == IntPtr.Zero) ? null : new PREPHIST(cPtr, false);
      return ret;
    } 
  }

  public PREPHIST grp_cpr {
    set {
      icmoPINVOKE.COLLAT_GROUP_grp_cpr_set(swigCPtr, PREPHIST.getCPtr(value));
    } 
    get {
      IntPtr cPtr = icmoPINVOKE.COLLAT_GROUP_grp_cpr_get(swigCPtr);
      PREPHIST ret = (cPtr == IntPtr.Zero) ? null : new PREPHIST(cPtr, false);
      return ret;
    } 
  }

  public PREPHIST grp_speed_in_pricing_units {
    set {
      icmoPINVOKE.COLLAT_GROUP_grp_speed_in_pricing_units_set(swigCPtr, PREPHIST.getCPtr(value));
    } 
    get {
      IntPtr cPtr = icmoPINVOKE.COLLAT_GROUP_grp_speed_in_pricing_units_get(swigCPtr);
      PREPHIST ret = (cPtr == IntPtr.Zero) ? null : new PREPHIST(cPtr, false);
      return ret;
    } 
  }

  public SWIGTYPE_p_float grp_1mo_psa_hist {
    set {
      icmoPINVOKE.COLLAT_GROUP_grp_1mo_psa_hist_set(swigCPtr, SWIGTYPE_p_float.getCPtr(value));
    } 
    get {
      IntPtr cPtr = icmoPINVOKE.COLLAT_GROUP_grp_1mo_psa_hist_get(swigCPtr);
      SWIGTYPE_p_float ret = (cPtr == IntPtr.Zero) ? null : new SWIGTYPE_p_float(cPtr, false);
      return ret;
    } 
  }

  public SWIGTYPE_p_float grp_1mo_cpr_hist {
    set {
      icmoPINVOKE.COLLAT_GROUP_grp_1mo_cpr_hist_set(swigCPtr, SWIGTYPE_p_float.getCPtr(value));
    } 
    get {
      IntPtr cPtr = icmoPINVOKE.COLLAT_GROUP_grp_1mo_cpr_hist_get(swigCPtr);
      SWIGTYPE_p_float ret = (cPtr == IntPtr.Zero) ? null : new SWIGTYPE_p_float(cPtr, false);
      return ret;
    } 
  }

  public SWIGTYPE_p_float grp_1mo_speed_in_pricing_units_hist {
    set {
      icmoPINVOKE.COLLAT_GROUP_grp_1mo_speed_in_pricing_units_hist_set(swigCPtr, SWIGTYPE_p_float.getCPtr(value));
    } 
    get {
      IntPtr cPtr = icmoPINVOKE.COLLAT_GROUP_grp_1mo_speed_in_pricing_units_hist_get(swigCPtr);
      SWIGTYPE_p_float ret = (cPtr == IntPtr.Zero) ? null : new SWIGTYPE_p_float(cPtr, false);
      return ret;
    } 
  }

  public int grp_callable_by_investor {
    set {
      icmoPINVOKE.COLLAT_GROUP_grp_callable_by_investor_set(swigCPtr, value);
    } 
    get {
      int ret = icmoPINVOKE.COLLAT_GROUP_grp_callable_by_investor_get(swigCPtr);
      return ret;
    } 
  }

  public int grp_servicer_advance {
    set {
      icmoPINVOKE.COLLAT_GROUP_grp_servicer_advance_set(swigCPtr, value);
    } 
    get {
      int ret = icmoPINVOKE.COLLAT_GROUP_grp_servicer_advance_get(swigCPtr);
      return ret;
    } 
  }

  public string grp_virtual_reremic {
    set {
      icmoPINVOKE.COLLAT_GROUP_grp_virtual_reremic_set(swigCPtr, value);
    } 
    get {
      string ret = icmoPINVOKE.COLLAT_GROUP_grp_virtual_reremic_get(swigCPtr);
      return ret;
    } 
  }

  public double grp_cdu_loss_accum {
    set {
      icmoPINVOKE.COLLAT_GROUP_grp_cdu_loss_accum_set(swigCPtr, value);
    } 
    get {
      double ret = icmoPINVOKE.COLLAT_GROUP_grp_cdu_loss_accum_get(swigCPtr);
      return ret;
    } 
  }

  public double grp_lastcdu_loss_accum {
    set {
      icmoPINVOKE.COLLAT_GROUP_grp_lastcdu_loss_accum_set(swigCPtr, value);
    } 
    get {
      double ret = icmoPINVOKE.COLLAT_GROUP_grp_lastcdu_loss_accum_get(swigCPtr);
      return ret;
    } 
  }

  public double grp_origbal {
    set {
      icmoPINVOKE.COLLAT_GROUP_grp_origbal_set(swigCPtr, value);
    } 
    get {
      double ret = icmoPINVOKE.COLLAT_GROUP_grp_origbal_get(swigCPtr);
      return ret;
    } 
  }

  public _POOL_INFO grp_collat_wavg {
    set {
      icmoPINVOKE.COLLAT_GROUP_grp_collat_wavg_set(swigCPtr, _POOL_INFO.getCPtr(value));
    } 
    get {
      IntPtr cPtr = icmoPINVOKE.COLLAT_GROUP_grp_collat_wavg_get(swigCPtr);
      _POOL_INFO ret = (cPtr == IntPtr.Zero) ? null : new _POOL_INFO(cPtr, false);
      return ret;
    } 
  }

  public string grp_pnote_tag {
    set {
      icmoPINVOKE.COLLAT_GROUP_grp_pnote_tag_set(swigCPtr, value);
    } 
    get {
      string ret = icmoPINVOKE.COLLAT_GROUP_grp_pnote_tag_get(swigCPtr);
      return ret;
    } 
  }

  public SWIGTYPE_p_p_char grp_pnote_v_tags {
    set {
      icmoPINVOKE.COLLAT_GROUP_grp_pnote_v_tags_set(swigCPtr, SWIGTYPE_p_p_char.getCPtr(value));
    } 
    get {
      IntPtr cPtr = icmoPINVOKE.COLLAT_GROUP_grp_pnote_v_tags_get(swigCPtr);
      SWIGTYPE_p_p_char ret = (cPtr == IntPtr.Zero) ? null : new SWIGTYPE_p_p_char(cPtr, false);
      return ret;
    } 
  }

  public string grp_fullname {
    set {
      icmoPINVOKE.COLLAT_GROUP_grp_fullname_set(swigCPtr, value);
    } 
    get {
      string ret = icmoPINVOKE.COLLAT_GROUP_grp_fullname_get(swigCPtr);
      return ret;
    } 
  }

  public COLLAT_GROUP() : this(icmoPINVOKE.new_COLLAT_GROUP(), true) {
  }

}
