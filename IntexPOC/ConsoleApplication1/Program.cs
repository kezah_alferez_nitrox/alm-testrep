﻿using System;

namespace IntexPOC
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("\n\n\nStart of Intex CMO Subroutines Version {0} test\n", icmo.ICMO_VERSION);

            SWIGTYPE_p_void icmo_make_struct = icmo.icmo_make_struct(null, icmo.ICMOMS_ICMOMISC, "TYPICAL", null);

            IntPtr handle = SWIGTYPE_p_void.getCPtr(icmo_make_struct).Handle;
            SWIGTYPE_p_ICMOMISC icmomiscPtr = new SWIGTYPE_p_ICMOMISC(handle, true);

            SWIGTYPE_p__ICMO icmoPtr = icmo.icmo_deal(icmomiscPtr, null, "FHL034", icmo.ICMODEAL_NEW, 0, 0, 0, null, null);

            if(icmoPtr == null)
            {
                ICMOMISC icmomisc = new ICMOMISC(handle, true);
                Console.WriteLine("Can't find deal FHL034 or \n{0}\n{1}\n{2}", icmomisc.icmomisc_pars_errmsg1, icmomisc.icmomisc_pars_errmsg2, icmomisc.icmomisc_pars_errmsg3);
            }

            _ICMO _icmo = new _ICMO(SWIGTYPE_p__ICMO.getCPtr(icmoPtr).Handle, true);

            int trno_1 = (_icmo.icmo_have_residual == 1 ? 0 : 1);
            for (int trno = trno_1; trno <= _icmo.icmo_n_tranches; trno++)
            {
                //if ( _icmo.icmo_tranche_origbals[trno] > 0. )
                //    factor = _icmo.icmo_tranche_curbals[trno] /
                //                _icmo.icmo_tranche_origbals[trno] ;
                //else
                //    factor = 0. ;

                //printf( "\n  %-2.2s %9.4f %12.0f %9.6f     %2d ",
                //        _icmo.icmo_tranche_names[trno],
                //        _icmo.icmo_tranche_coupons[trno],
                //        _icmo.icmo_tranche_origbals[trno],
                //        factor,
                //        _icmo.icmo_tranche_delays[trno] ) ;
            }
        }

        static void sample_index_fcn(_ICMO icmo, int ith_index)
        {
            //            int ith_month, n_index_months ;
            ///* double index_vect[ 1+MAX_MONTHS_IN_DEALS ] ; */
            //double[] index_vect ;
            //char user_appl_main_ptr ;


            ///* You can point to your own system-wide area, if desired */

            //            string icmoUser = icmo.icmo_user;

            //            switch (ith_index)
            //            {
            //                case cmosub32.ICMOI_1MO_LIBOR:
            //                n_index_months = 1 ;
            //    index_vect[ 0 ] = 7.5 ;
            //    break ;
            //            }
        }
    }
}
