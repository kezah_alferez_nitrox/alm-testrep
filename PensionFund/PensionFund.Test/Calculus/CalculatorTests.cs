using System;
using System.Diagnostics;
using NUnit.Framework;
using PensionFund.Calculus;

namespace PensionFund.Test.Calculus
{
    [TestFixture]
    public class CalculatorTests
    {
        [Test]
        public void TheTest()
        {
            PBOCalculator calculator = GetCalculator();

            calculator.Compute();
            double[] test = { 20194, 21265.71735, 21785.6386, 23958.92158, 25569.07045, 26229.99517 };
            for (int i = 0; i < test.Length; i++)
            {
                double delta = Math.Abs(test[i] - calculator.ExpectedPBO[i]);
                Assert.Less(delta, 0.00001);
            }
        }

        [Test]
        public void PerfTest()
        {
            Stopwatch stopwatch = Stopwatch.StartNew();
            for (int i = 0; i < 10000; i++)
            {
                PBOCalculator calculator = GetCalculator();
                calculator.Compute();
            }
            stopwatch.Stop();
            Debug.WriteLine("temps: " + stopwatch.ElapsedMilliseconds);
        }


        private static PBOCalculator GetCalculator()
        {
            PBOCalculator calculator = new PBOCalculator(6);

            double[] discountRate = { 6.2, 5.986891495, 6.123094177, 5.450633872, 5.003749887, 4.892834789 };
            double[] rateOfCompensationIncrease = { 2.9, 2.672707644, 3.327225457, 3.288873551, 3.115229091, 2.930276373 };
            double[] annualAdjustmentsToPensions = { 1.9, 1.672707644, 2.327225457, 2.288873551, 2.115229091, 1.930276373 };
            double[] actives = { 0.65, 0.65, 0.65, 0.65, 0.65, 0.65 };
            double[] deferredsPensioners = { 0.35, 0.35, 0.35, 0.35, 0.35, 0.35 };

            calculator.Actives = actives;
            calculator.AnnualAdjustmentsToPensions = annualAdjustmentsToPensions;
            calculator.DeferredsPensioners = deferredsPensioners;
            calculator.RateOfCompensationIncrease = rateOfCompensationIncrease;
            calculator.DiscountRate = discountRate;
            return calculator;
        }
    }
}