﻿using NUnit.Framework;
using PensionFund.IO;

namespace PensionFund.Test.IO
{
    [TestFixture]
    public class ModelReaderTest
    {
        [Test]
        public void Test()
        {
            var items = new ModelDefinitionReader("items.xml");
            items.ReadAll();
        }
    }
}