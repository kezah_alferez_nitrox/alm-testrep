using System;
using System.Diagnostics;
using Microsoft.VisualBasic;
using NUnit.Framework;
using PensionFund.IO;

namespace PensionFund.Test.IO
{
    [TestFixture]
    public class ImportScenarios
    {
        [Test]
        public void TheTest()
        {
            foreach (var value in Enum.GetValues(typeof (VariantType)))
            {
                Debug.WriteLine(value + " " + (int) value);
            }

            BinScenarioReader reader = new BinScenarioReader(@"c:\temp\RNEquity.bin");
            double[,] data = reader.ReadAll2D(10,50000);
        }
    }
}