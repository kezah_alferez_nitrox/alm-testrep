using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using ALMSCommon;
using PensionFund.Calculus;
using PensionFund.Data;
using PensionFund.Domain;
using ZedGraph;

namespace PensionFund.Views
{
    public partial class SimulationPanel : UserControl
    {
        private readonly IRepository<ScenarioItem> _scenarioItemRepository = new ScenarioItemRepository();

        private static readonly int[] Percents = new[] { 1, 5, 10, 33, 50, 67, 90, 95, 100 };
        private static readonly string[] PercentLabels = new[] { "1", "5", "10", "33", "MEAN", "67", "90", "95", "99" };
        private static readonly int firstYear = 2008;
        private double[] _xValues = new double[] { 2008, 2009, 2010, 2011, 2012, 2013 };

        private readonly Color[] _colors = new[]
                                               {
                                                   Color.White,
                                                   Color.FromArgb(152, 51, 0), Color.FromArgb(255, 153, 0),
                                                   Color.FromArgb(255, 204, 153),
                                                   Color.FromArgb(255, 255, 204), Color.FromArgb(255, 255, 204),
                                                   Color.FromArgb(204, 255, 204), Color.FromArgb(153, 204, 0),
                                                   Color.FromArgb(0, 100, 17),
                                                   Color.FromArgb(0, 60, 10)
                                               };

        private readonly double[][] _percentils = new double[Percents.Length][];
        private readonly double[][] _relativePercentils = new double[Percents.Length][];
        private double[] _averages;

        private LineItem _selectedCurve = null;

        private readonly ModelItem _modelItem;
        private int _selectedScenario;
        private int years;

        public ModelItem ModelItem
        {
            get { return this._modelItem; }
        }

        public SimulationPanel(ModelItem modelItem)
        {
            this.InitializeComponent();

            this._modelItem = modelItem;
            this.titleLabel.Text = modelItem.DisplayName;

            this.InitPercentils();
            this.InitGrid();
            this.UpdatePercentilesGraph();
        }

        private void InitPercentils()
        {
            var sortedValues = new List<double[]>();

            var scenarios = this._scenarioItemRepository.GetAll();

            this.years = scenarios.Min(s => s.YearCount);
            this._xValues = new double[this.years];
            for (int i = 0; i < this.years; i++)
            {
                this._xValues[i] = firstYear + i;
            }

            for (int i = 0; i < this.years; i++)
            {
                var index = i;
                sortedValues.Add((from s in scenarios
                                  where s.Calculator.Values.ContainsKey(this.ModelItem.FunctionName)
                                  orderby s.Calculator.Values[this.ModelItem.FunctionName][index]
                                  select s.Calculator.Values[this.ModelItem.FunctionName][index]).ToArray());
            }

            for (int j = 0; j < Percents.Length; j++)
            {
                var percentile = new List<double>();
                var average = new List<double>();
                for (int i = 0; i < this.years; i++)
                {
                    percentile.Add(Percentile.Calculate(sortedValues[i], Percents[j]));
                    average.Add(sortedValues[i].Average(d => d));
                }
                this._percentils[j] = percentile.ToArray();
                this._averages = average.ToArray();
            }
        }

        private void InitRelativePercentils()
        {
            for (int i = 0; i < this._percentils.Length; i++)
            {
                this._relativePercentils[i] = new double[this._percentils[i].Length];
                for (int j = 0; j < this._percentils[i].Length; j++)
                {
                    this._relativePercentils[i][j] = i == 0
                                                         ? this._percentils[i][j]
                                                         : this._percentils[i][j] - this._percentils[i - 1][j];
                }
            }
        }

        private void InitGrid()
        {
            this.CreateGridColumns();

            for (int j = 0; j < Percents.Length; j++)
            {
                var line = new List<object> { PercentLabels[j] };
                for (int i = 0; i < this._percentils[j].Length; i++)
                    line.Add(this._percentils[j][i]);
                this.percentileGrid.Rows.Add(line.ToArray());
            }
        }

        private void CreateGridColumns()
        {
            DataGridViewColumn[] columns = new DataGridViewColumn[this.years + 1];

            DataGridViewCellStyle numericStyle = new DataGridViewCellStyle();
            numericStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            numericStyle.Format = "N3";

            columns[0] = new DataGridViewTextBoxColumn();
            columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            columns[0].Frozen = true;
            columns[0].HeaderText = "";
            columns[0].Name = "labelColumn";
            columns[0].ReadOnly = true;
            columns[0].Width = 40;

            for (int i = 0; i < this.years; i++)
            {
                columns[i + 1] = new DataGridViewTextBoxColumn();
                columns[i + 1].DefaultCellStyle = numericStyle;
                columns[i + 1].HeaderText = this._xValues[i].ToString();
                columns[i + 1].Name = "year" + i + "Column";
                columns[i + 1].ReadOnly = true;
                columns[i + 1].Width = 80;
            }

            ((ISupportInitialize)this.percentileGrid).BeginInit();
            this.percentileGrid.Columns.Clear();
            this.percentileGrid.Columns.AddRange(columns);
            ((ISupportInitialize)this.percentileGrid).EndInit();
        }

        private void bClose_Click(object sender, EventArgs e)
        {
            this.ModelItem.PanelDisplayed = false;
            this.Parent.Controls.Remove(this);
        }

        private void UpdatePercentilesGraph()
        {
            this.zedGraphControl.IsAntiAlias = true;

            GraphPane graphPane = this.zedGraphControl.GraphPane;

            graphPane.Title.IsVisible = false;
            graphPane.Legend.IsVisible = false;

            graphPane.XAxis.Title.IsVisible = false;
            graphPane.XAxis.Scale.Min = this._xValues[0];
            graphPane.XAxis.Scale.Max = this._xValues[this._xValues.Length - 1];
            graphPane.XAxis.MinorTic.IsAllTics = false;

            graphPane.YAxis.Title.Text = "";
            graphPane.YAxis.Title.IsVisible = true;

            double maxY;
            double minY;
            GetArrayMinMax(_percentils, out minY, out maxY);
            graphPane.YAxis.Scale.Min = minY;
            graphPane.YAxis.Scale.Max = maxY;

            graphPane.LineType = LineType.Stack;

            graphPane.CurveList.Clear();

            this.InitRelativePercentils();

            double[] selectedScenarioYValues = this.DrawSelectedScenarioCurve();

            if (selectedScenarioYValues != null)
            {
                for (int i = 0; i < selectedScenarioYValues.Length; i++)
                {
                    this._relativePercentils[0][i] -= selectedScenarioYValues[i];
                }
            }

            //draw percentils
            for (int i = 0; i < this._relativePercentils.Length; i++)
            {
                LineItem percentilCurve = graphPane.AddCurve(
                    PercentLabels[i],
                    this._xValues,
                    this._relativePercentils[i],
                    this._colors[i]);

                if (i != 0)
                {
                    percentilCurve.Line.Fill = new Fill(this._colors[i]);
                }

                if (i == 0)
                {
                    percentilCurve.Line.Width = 3;
                    percentilCurve.Line.Color = Color.Black;
                }
                else if (i == this._relativePercentils.Length - 1)
                {
                    percentilCurve.Line.Width = 3;
                    percentilCurve.Line.Color = Color.Black;
                }
                else if (i == this._relativePercentils.Length / 2)
                {
                    percentilCurve.Line.Width = 3;
                    percentilCurve.Line.Color = Color.DarkSlateBlue;
                }
                else
                {
                    percentilCurve.Line.Width = 0;
                }

                percentilCurve.Symbol.IsVisible = false;
            }

            this.zedGraphControl.AxisChange();
            this.zedGraphControl.Invalidate();
        }

        private static void GetArrayMinMax(double[][] array, out double minY, out double maxY)
        {
            minY = double.MaxValue;
            maxY = double.MinValue;
            for (int i = 0; i < array.Length; i++)
            {
                for (int j = 0; j < array[i].Length; j++)
                {
                    double percentil = array[i][j];
                    minY = Math.Min(minY, percentil);
                    maxY = Math.Max(maxY, percentil);
                }
            }
        }

        public void SelectScenario(int scenario)
        {
            this._selectedScenario = scenario;

            this.UpdatePercentilesGraph();
        }

        private double[] DrawSelectedScenarioCurve()
        {
            GraphPane graphPane = this.zedGraphControl.GraphPane;

            if (this._selectedScenario != 0 && this._selectedScenario <= this._scenarioItemRepository.Count)
            {
                ScenarioItem scenarioItem = this._scenarioItemRepository.Load(this._selectedScenario - 1);

                double[] yValues;
                if (!scenarioItem.Calculator.Values.TryGetValue(this.ModelItem.FunctionName, out yValues))
                {
                    return null;
                }

                this._selectedCurve = graphPane.AddCurve("", this._xValues, yValues, Color.Red);
                this._selectedCurve.Line.Width = 3;
                this._selectedCurve.Symbol.IsVisible = false;

                return yValues;
            }

            return null;
        }
    }
}