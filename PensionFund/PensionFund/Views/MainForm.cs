using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using ALMSCommon;
using ALMSCommon.Forms;
using PensionFund.Data;
using PensionFund.Domain;
using PensionFund.IO;
using System.Threading;

namespace PensionFund.Views
{
    public partial class MainForm : Form, IModuleMainForm
    {
        private readonly string viewerFile;
        private const string ModelFileExtension = "Pension Fund Model|*.pfm";
        private const string ModelDefinitionFileExtension = "Pension Fund Model Definition|*.pfd";
        private readonly IRepository<ModelItem> _modelItemRepository = new ModelItemRepository();
        private readonly IRepository<ScenarioItem> _scenarioItemRepository = new ScenarioItemRepository();
        private IList<ComputeException> _evaluationErrors;
        private readonly bool viewerMode = false;
        private string[] args;

        public MainForm()
        {
            this.InitializeComponent();

            this.Shown += new EventHandler(MainForm_Shown);
            simulations.ModelChanged += SimulationsModelChanged;
        }

        public MainForm(string file)
            : this()
        {
            this.viewerMode = true;
            this.viewerFile = file;
        }

        public MainForm(string[] args):this()
        {
            this.args = args;
        }

        public bool IsForcedClose { get; set; }

        public ILicenceInfo LicenceInfo { set; get; }

        void MainForm_Shown(object sender, EventArgs e)
        {
            OpenPassedWorkSpace();
        }

        private void OpenPassedWorkSpace()
        {
            if (args != null && args.Length > 0)
            {
                string fileName = args[0];
                OpenFile(fileName);
            }
        }

        public void OpenFile(string fileName)
        {
            if (File.Exists(fileName))
            {
                string extension = Path.GetExtension(fileName);
                if (extension == ".pfm")
                    this.LoadModelFile(fileName);
                else if (extension == ".pfd")
                    this.ImportModel(fileName);
            }
        }

        private void SimulationsModelChanged(object sender, EventArgs e)
        {
            this.footNote.LoadModelDefinition();
            this.RefreshSelectedScenario();
        }

        private void ExecuteScenarios()
        {
            if (this._scenarioItemRepository.Count > 0)
            {
                LoadForm.Instance.DoWork(new WorkParameters(this.EvaluateAllScenariosCallback, this.ScenarioEvaluationEnd));
            }

            this.simulations.DeleteAll();
        }

        private void ScenarioEvaluationEnd(Exception exception)
        {
            if (this._evaluationErrors.Count > 0)
            {
                if (ComputeErrorsForm.Display(this, _evaluationErrors) == DialogResult.Retry)
                {
                    ThreadPool.QueueUserWorkItem(o => this.Invoke(new InvokeDelegate(this.ExecuteScenarios)));
                }
            }

            this.RefreshSelectedScenario();
        }

        public delegate void InvokeDelegate();

        private void EvaluateAllScenariosCallback(BackgroundWorker backgroundworker)
        {
            IList<ScenarioItem> scenarios = this._scenarioItemRepository.GetAll();

            this._evaluationErrors = new List<ComputeException>();
            for (int i = 0; i < scenarios.Count; i++)
            {
                try
                {
                    int percent = (int)Math.Round((i + 1) * 100.0 / scenarios.Count);
                    backgroundworker.ReportProgress(percent);

                    scenarios[i].Evaluate();
                }
                catch (ComputeException ex)
                {
                    ex.Scenario = i + 1;
                    Logger.Log(ex);
                    this._evaluationErrors.Add(ex);
                }
                catch (Exception ex)
                {
                    this._evaluationErrors.Add(new ComputeException(null, ex.Message, ex));
                }

                if (this._evaluationErrors.Count >= 100) break;
            }
        }

        private void InitScenarios()
        {
            this.scenarioListBox.Items.Clear();
            this.scenarioListBox.Items.AddRange(this._scenarioItemRepository.GetAll().ToArray());

            if (this.scenarioListBox.Items.Count > 0)
            {
                this.scenarioListBox.SelectedIndex = 0;
            }

            this.simulations.DeleteAll();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void MainFormLoad(object sender, EventArgs e)
        {
            if (this.viewerMode)
            {
                addScenariosButton.Enabled = false;
                if (string.IsNullOrEmpty(viewerFile))
                {
                    MessageBox.Show(this,
                                    "No .pfm file was found. Please select a .pfm file from your disk to continue.",
                                    "No .pfm file found", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    this.LoadModel();
                }
                else
                {
                    this.LoadModelFile(viewerFile);
                }
            }
            this.simulations.Reload();
        }

        private void importModelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this._modelItemRepository.Count != 0)
            {
                if (MessageBox.Show(
                        "Warning, this importation will delete all functions currently loaded wich are not saved. Wich you continue ?",
                        "Importation Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
                {
                    return;
                }
            }

            this.modelOpenFileDialog.Filter = ModelDefinitionFileExtension;
            if (this.modelOpenFileDialog.ShowDialog() == DialogResult.OK)
            {
                ImportModel(this.modelOpenFileDialog.FileName);
            }
        }

        private void ImportModel(string fileName)
        {
            try
            {
                this._modelItemRepository.DeleteAll();
                var loader = new ModelDefinitionLoader(this._modelItemRepository);
                loader.Load(fileName);

                this.InitScenarios();
                this.simulations.Reload();

                this.footNote.DisplayModelDefinition();
            }
            catch (Exception ex)
            {
                Logger.Log(ex);
            }
        }

        private void exportModelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.modelSaveFileDialog.Filter = ModelDefinitionFileExtension;
            if (this.modelSaveFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    var writer = new ModelDefinitionWriter(this._modelItemRepository);
                    writer.WriteAll(this.modelSaveFileDialog.FileName);
                }
                catch (Exception ex)
                {
                    Logger.Log(ex);
                }
            }
        }

        private void scenarioListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.RefreshSelectedScenario();
        }

        private void RefreshSelectedScenario()
        {
            if (this.scenarioListBox.SelectedIndex < 0) return;

            this.footNote.Refresh((ScenarioItem)this.scenarioListBox.Items[this.scenarioListBox.SelectedIndex]);
        }

        private void footNote_ShowModel(object sender, EventArgs e)
        {
            this.mainTabControl.SelectTab(1);
            this.simulations.AddSimulationPanel((ModelItem)sender);
        }

        private void addScenariosButton_Click(object sender, EventArgs e)
        {
            var scenariosLoadDialog = new ScenariosLoadDialog();

            if (scenariosLoadDialog.ShowDialog() == DialogResult.OK)
            {
                this.InitScenarios();
                this.simulations.Reload();
            }
        }

        private void LoadModel()
        {
            if (this._modelItemRepository.Count != 0 || this._scenarioItemRepository.Count != 0)
            {
                if (MessageBox.Show(
                        "Warning, this will delete all functions and calculated scenarios which are not saved! Do you want to continue?",
                        "Importation Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
                {
                    return;
                }
            }

            this.modelOpenFileDialog.Filter = ModelFileExtension;
            if (this.modelOpenFileDialog.ShowDialog() == DialogResult.OK)
            {
                this.LoadModelFile(this.modelOpenFileDialog.FileName);
            }
        }

        private void LoadModelFile(string fileName)
        {
            try
            {
                LoadForm.Instance.DoWork(new WorkParameters(bw => new ModelDeSerializer().DeSerialize(fileName, bw), null));

                this.InitScenarios();
                this.simulations.Reload();

                this.footNote.DisplayModelDefinition();
            }
            catch (Exception ex)
            {
                Logger.Log(ex);
            }
        }

        private void loadModelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.LoadModel();
        }

        private void saveModelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.SaveModel();
        }

        private void SaveModel()
        {
            this.modelSaveFileDialog.Filter = ModelFileExtension;
            if (this.modelSaveFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    LoadForm.Instance.DoWork(new WorkParameters(bw => new ModelSerializer().Serialize(this.modelSaveFileDialog.FileName, bw), null));
                }
                catch (Exception ex)
                {
                    Logger.Log(ex);
                }
            }
        }

        private void calculateToolStripButton_Click(object sender, EventArgs e)
        {
            this.ExecuteScenarios();
        }

        private void pasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.ClipboardPaste();
        }

        private void pasteToolStripButton_Click(object sender, EventArgs e)
        {
            this.ClipboardPaste();
        }

        private void ClipboardPaste()
        {
            if (this.mainTabControl.SelectedIndex == 0)
            {
                this.footNote.ClipboardPaste();
            }
        }

        private void openToolStripButton_Click(object sender, EventArgs e)
        {
            this.LoadModel();
        }

        private void saveToolStripButton_Click(object sender, EventArgs e)
        {
            this.SaveModel();
        }

        private void vectorManagerToolStripButton_Click(object sender, EventArgs e)
        {
            mainTabControl.SelectedIndex = 1;
        }

        public string[] Args
        {
            get { return this.args; }
            set { args = value; }
        }
    }
}