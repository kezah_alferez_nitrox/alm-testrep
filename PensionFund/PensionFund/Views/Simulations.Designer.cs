﻿namespace PensionFund.Views
{
    partial class Simulations
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.panel3 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.modelContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.editModelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeLineItemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel2 = new System.Windows.Forms.Panel();
            this.addModelItemButton = new System.Windows.Forms.Button();
            this.centerPanel = new System.Windows.Forms.Panel();
            this.simulationContainerPanel = new System.Windows.Forms.Panel();
            this.topPanel = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.inputGraphsPanel = new System.Windows.Forms.TableLayoutPanel();
            this.zedGraphControl1 = new ZedGraph.ZedGraphControl();
            this.zedGraphControl2 = new ZedGraph.ZedGraphControl();
            this.zedGraphControl3 = new ZedGraph.ZedGraphControl();
            this.buttonPanel = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.showInputGraphs = new System.Windows.Forms.Button();
            this.modelTreeview = new PensionFund.Controls.ModelTreeview();
            this.scenarioSelectionUserControl = new PensionFund.Views.ScenarioSelectionUserControl();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.modelContextMenuStrip.SuspendLayout();
            this.panel2.SuspendLayout();
            this.centerPanel.SuspendLayout();
            this.topPanel.SuspendLayout();
            this.panel4.SuspendLayout();
            this.inputGraphsPanel.SuspendLayout();
            this.buttonPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.panel3);
            this.splitContainer1.Panel1.Controls.Add(this.panel2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.centerPanel);
            this.splitContainer1.Size = new System.Drawing.Size(1012, 459);
            this.splitContainer1.SplitterDistance = 223;
            this.splitContainer1.TabIndex = 1;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.groupBox1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(223, 416);
            this.panel3.TabIndex = 2;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.modelTreeview);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(223, 416);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Models";
            // 
            // modelContextMenuStrip
            // 
            this.modelContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.editModelToolStripMenuItem,
            this.removeLineItemToolStripMenuItem});
            this.modelContextMenuStrip.Name = "modelContextMenuStrip";
            this.modelContextMenuStrip.Size = new System.Drawing.Size(170, 70);
            this.modelContextMenuStrip.Opening += new System.ComponentModel.CancelEventHandler(this.modelContextMenuStrip_Opening);
            // 
            // editModelToolStripMenuItem
            // 
            this.editModelToolStripMenuItem.Name = "editModelToolStripMenuItem";
            this.editModelToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.editModelToolStripMenuItem.Text = "&Modify Formula...";
            this.editModelToolStripMenuItem.Click += new System.EventHandler(this.editModelToolStripMenuItem_Click);
            // 
            // removeLineItemToolStripMenuItem
            // 
            this.removeLineItemToolStripMenuItem.Name = "removeLineItemToolStripMenuItem";
            this.removeLineItemToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.removeLineItemToolStripMenuItem.Text = "&Remove Line Item";
            this.removeLineItemToolStripMenuItem.Click += new System.EventHandler(this.removeLineItemToolStripMenuItem_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.addModelItemButton);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 416);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(223, 43);
            this.panel2.TabIndex = 1;
            // 
            // addModelItemButton
            // 
            this.addModelItemButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.addModelItemButton.Location = new System.Drawing.Point(5, 6);
            this.addModelItemButton.Name = "addModelItemButton";
            this.addModelItemButton.Size = new System.Drawing.Size(215, 32);
            this.addModelItemButton.TabIndex = 0;
            this.addModelItemButton.Text = "Add Line Item...";
            this.addModelItemButton.UseVisualStyleBackColor = true;
            this.addModelItemButton.Click += new System.EventHandler(this.addModelItemButton_Click);
            // 
            // centerPanel
            // 
            this.centerPanel.AutoScroll = true;
            this.centerPanel.Controls.Add(this.simulationContainerPanel);
            this.centerPanel.Controls.Add(this.topPanel);
            this.centerPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.centerPanel.Location = new System.Drawing.Point(0, 0);
            this.centerPanel.Name = "centerPanel";
            this.centerPanel.Size = new System.Drawing.Size(785, 459);
            this.centerPanel.TabIndex = 0;
            // 
            // simulationContainerPanel
            // 
            this.simulationContainerPanel.AutoScroll = true;
            this.simulationContainerPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.simulationContainerPanel.Location = new System.Drawing.Point(0, 256);
            this.simulationContainerPanel.Name = "simulationContainerPanel";
            this.simulationContainerPanel.Size = new System.Drawing.Size(785, 203);
            this.simulationContainerPanel.TabIndex = 1;
            // 
            // topPanel
            // 
            this.topPanel.Controls.Add(this.panel4);
            this.topPanel.Controls.Add(this.buttonPanel);
            this.topPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.topPanel.Location = new System.Drawing.Point(0, 0);
            this.topPanel.Name = "topPanel";
            this.topPanel.Size = new System.Drawing.Size(785, 256);
            this.topPanel.TabIndex = 0;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.inputGraphsPanel);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 36);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(785, 220);
            this.panel4.TabIndex = 2;
            // 
            // inputGraphsPanel
            // 
            this.inputGraphsPanel.ColumnCount = 3;
            this.inputGraphsPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.inputGraphsPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.inputGraphsPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.inputGraphsPanel.Controls.Add(this.zedGraphControl1, 0, 0);
            this.inputGraphsPanel.Controls.Add(this.zedGraphControl2, 1, 0);
            this.inputGraphsPanel.Controls.Add(this.zedGraphControl3, 2, 0);
            this.inputGraphsPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.inputGraphsPanel.Location = new System.Drawing.Point(0, 0);
            this.inputGraphsPanel.Name = "inputGraphsPanel";
            this.inputGraphsPanel.RowCount = 1;
            this.inputGraphsPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.inputGraphsPanel.Size = new System.Drawing.Size(785, 220);
            this.inputGraphsPanel.TabIndex = 0;
            // 
            // zedGraphControl1
            // 
            this.zedGraphControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.zedGraphControl1.Location = new System.Drawing.Point(3, 3);
            this.zedGraphControl1.Name = "zedGraphControl1";
            this.zedGraphControl1.ScrollGrace = 0;
            this.zedGraphControl1.ScrollMaxX = 0;
            this.zedGraphControl1.ScrollMaxY = 0;
            this.zedGraphControl1.ScrollMaxY2 = 0;
            this.zedGraphControl1.ScrollMinX = 0;
            this.zedGraphControl1.ScrollMinY = 0;
            this.zedGraphControl1.ScrollMinY2 = 0;
            this.zedGraphControl1.Size = new System.Drawing.Size(255, 214);
            this.zedGraphControl1.TabIndex = 0;
            // 
            // zedGraphControl2
            // 
            this.zedGraphControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.zedGraphControl2.Location = new System.Drawing.Point(264, 3);
            this.zedGraphControl2.Name = "zedGraphControl2";
            this.zedGraphControl2.ScrollGrace = 0;
            this.zedGraphControl2.ScrollMaxX = 0;
            this.zedGraphControl2.ScrollMaxY = 0;
            this.zedGraphControl2.ScrollMaxY2 = 0;
            this.zedGraphControl2.ScrollMinX = 0;
            this.zedGraphControl2.ScrollMinY = 0;
            this.zedGraphControl2.ScrollMinY2 = 0;
            this.zedGraphControl2.Size = new System.Drawing.Size(255, 214);
            this.zedGraphControl2.TabIndex = 1;
            // 
            // zedGraphControl3
            // 
            this.zedGraphControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.zedGraphControl3.Location = new System.Drawing.Point(525, 3);
            this.zedGraphControl3.Name = "zedGraphControl3";
            this.zedGraphControl3.ScrollGrace = 0;
            this.zedGraphControl3.ScrollMaxX = 0;
            this.zedGraphControl3.ScrollMaxY = 0;
            this.zedGraphControl3.ScrollMaxY2 = 0;
            this.zedGraphControl3.ScrollMinX = 0;
            this.zedGraphControl3.ScrollMinY = 0;
            this.zedGraphControl3.ScrollMinY2 = 0;
            this.zedGraphControl3.Size = new System.Drawing.Size(257, 214);
            this.zedGraphControl3.TabIndex = 2;
            // 
            // buttonPanel
            // 
            this.buttonPanel.Controls.Add(this.scenarioSelectionUserControl);
            this.buttonPanel.Controls.Add(this.label1);
            this.buttonPanel.Controls.Add(this.showInputGraphs);
            this.buttonPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonPanel.Location = new System.Drawing.Point(0, 0);
            this.buttonPanel.Name = "buttonPanel";
            this.buttonPanel.Size = new System.Drawing.Size(785, 36);
            this.buttonPanel.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(2, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Scenarios";
            // 
            // showInputGraphs
            // 
            this.showInputGraphs.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.showInputGraphs.Location = new System.Drawing.Point(648, 7);
            this.showInputGraphs.Name = "showInputGraphs";
            this.showInputGraphs.Size = new System.Drawing.Size(124, 23);
            this.showInputGraphs.TabIndex = 0;
            this.showInputGraphs.Text = "Show/Hide Graphs";
            this.showInputGraphs.UseVisualStyleBackColor = true;
            this.showInputGraphs.Click += new System.EventHandler(this.showInputGraphs_Click);
            // 
            // modelTreeview
            // 
            this.modelTreeview.ContextMenuStrip = this.modelContextMenuStrip;
            this.modelTreeview.Dock = System.Windows.Forms.DockStyle.Fill;
            this.modelTreeview.Location = new System.Drawing.Point(3, 16);
            this.modelTreeview.Name = "modelTreeview";
            this.modelTreeview.Size = new System.Drawing.Size(217, 397);
            this.modelTreeview.TabIndex = 0;
            this.modelTreeview.NodeMouseDoubleClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.modelTreeview_NodeMouseDoubleClick);
            // 
            // scenarioSelectionUserControl
            // 
            this.scenarioSelectionUserControl.Location = new System.Drawing.Point(63, 7);
            this.scenarioSelectionUserControl.Max = 0;
            this.scenarioSelectionUserControl.Name = "scenarioSelectionUserControl";
            this.scenarioSelectionUserControl.Size = new System.Drawing.Size(150, 23);
            this.scenarioSelectionUserControl.TabIndex = 2;
            this.scenarioSelectionUserControl.Value = "";
            // 
            // Simulations
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainer1);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "Simulations";
            this.Size = new System.Drawing.Size(1012, 459);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.modelContextMenuStrip.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.centerPanel.ResumeLayout(false);
            this.topPanel.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.inputGraphsPanel.ResumeLayout(false);
            this.buttonPanel.ResumeLayout(false);
            this.buttonPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button addModelItemButton;
        private System.Windows.Forms.Panel centerPanel;
        private PensionFund.Controls.ModelTreeview modelTreeview;
        private System.Windows.Forms.Panel simulationContainerPanel;
        private System.Windows.Forms.Panel topPanel;
        private System.Windows.Forms.TableLayoutPanel inputGraphsPanel;
        private ZedGraph.ZedGraphControl zedGraphControl1;
        private ZedGraph.ZedGraphControl zedGraphControl2;
        private ZedGraph.ZedGraphControl zedGraphControl3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel buttonPanel;
        private System.Windows.Forms.Button showInputGraphs;
        private System.Windows.Forms.Label label1;
        private ScenarioSelectionUserControl scenarioSelectionUserControl;
        private System.Windows.Forms.ContextMenuStrip modelContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem editModelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removeLineItemToolStripMenuItem;

    }
}
