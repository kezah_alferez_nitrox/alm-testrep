using System;
using PensionFund.Domain;

namespace PensionFund.Views
{
    public class ComputeException : Exception
    {
        public int Scenario { get; set; }
        public ModelItem ModelItem { get; private set; }
        
        public ComputeException(ModelItem modelItem, string message, Exception innerException) : base(message, innerException)
        {
            this.ModelItem = modelItem;
        }
    }
}