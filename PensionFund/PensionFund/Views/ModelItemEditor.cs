﻿using System;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using PensionFund.Calculus;
using PensionFund.Data;
using PensionFund.Domain;

namespace PensionFund.Views
{
    public partial class ModelItemEditor : Form
    {
        private readonly IRepository<ModelItem> _modelItemRepository = new ModelItemRepository();
        private ModelItem _modelItem;
        private bool _canClose = true;

        public ModelItem ModelItem
        {
            get { return this._modelItem; }
        }

        public ModelItemEditor(ModelItem modelItem)
        {
            this._modelItem = modelItem;

            this.InitializeComponent();

            this.LoadModelTreeView();
            this.DisplayModelItem();
        }

        private void LoadModelTreeView()
        {
            this.modelTreeview.Load(this._modelItemRepository.GetAll());
        }

        private void DisplayModelItem()
        {
            if (this._modelItem == null) return;

            this.displayNameTextBox.Text = this._modelItem.DisplayName;
            this.formulaNameTextBox.Text = this._modelItem.FunctionName;
            this.initialValueTextBox.Text = this._modelItem.InitalValue.ToString();
            this.formulaTextBox.Text = this._modelItem.Formula;
            this.isVectorCheckBox.Checked = this._modelItem.Type == ModelItemType.Vector;

            this.UpdateControlState();
        }

        private void ValidateEditor(object sender, KeyEventArgs e)
        {
            this.UpdateButtonStatus();
        }

        private void UpdateButtonStatus()
        {
            if (this.displayNameTextBox.Text != "" && this.formulaNameTextBox.Text != "" &&
                this.initialValueTextBox.Text != "" && (this.formulaTextBox.Text != "" || this.isVectorCheckBox.Checked))
            {
                this.okButton.Enabled = true;
            }
            else
            {
                this.okButton.Enabled = false;
            }
        }

        private void GenerateFormulaName(object sender, EventArgs e)
        {
            Regex formulaName = new Regex("[a-zA-Z0-9]*");
            string formatedFormulaName = "";

            foreach (object matche in formulaName.Matches(this.displayNameTextBox.Text))
            {
                formatedFormulaName = formatedFormulaName + matche;
            }
            this.formulaNameTextBox.Text = "O_" + formatedFormulaName;
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            string error;
            if (!this.isVectorCheckBox.Checked && Calculator.FormulaIsValid(this.formulaTextBox.Text, out error))
            {
                MessageBox.Show(error, "Formula Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this._canClose = false;
                return;
            }

            this._canClose = true;

            if (this._modelItem == null)
            {
                this._modelItem = new ModelItem();
                this._modelItem.Group = ModelItem.OtherGroupName;
            }

            this._modelItem.DisplayName = this.displayNameTextBox.Text;
            this._modelItem.FunctionName = this.formulaNameTextBox.Text;

            double initialvalue;
            double.TryParse(this.initialValueTextBox.Text, out initialvalue);
            this._modelItem.InitalValue = initialvalue;

            this._modelItem.Formula = this.formulaTextBox.Text;

            this._modelItem.Type = this.isVectorCheckBox.Checked ? ModelItemType.Vector : ModelItemType.Formula;

            this._modelItemRepository.Save(this._modelItem);
        }

        private void modelTreeview_DoubleClick(object sender, EventArgs e)
        {
            if (this.modelTreeview.SelectedNode == null || !this.formulaTextBox.Enabled) return;

            ModelItem clickedModelItem = this.modelTreeview.SelectedNode.Tag as ModelItem;
            if (clickedModelItem == null) return;

            this.formulaTextBox.SelectedText = clickedModelItem.FunctionName + "(t)";
        }

        private void isVectorCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            this.UpdateControlState();
        }

        private void UpdateControlState()
        {
            this.initialValueTextBox.Enabled = this._modelItem == null;
            this.isVectorCheckBox.Enabled = this._modelItem == null;
            this.formulaTextBox.Enabled = !this.isVectorCheckBox.Checked;

            this.UpdateButtonStatus();
        }

        private void ModelItemEditor_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = !this._canClose;
        }
    }
}