﻿using System;
using System.Diagnostics;
using System.Windows.Forms;
using PensionFund.Data;
using PensionFund.Domain;
using PensionFund.IO;

namespace PensionFund.Views
{
    public partial class ScenariosLoadDialog : Form
    {
        private readonly IRepository<ScenarioItem> _scenarioItemRepository = new ScenarioItemRepository();

        public ScenariosLoadDialog()
        {
            InitializeComponent();
            discountRateLoad.Text = "Discount Rate file :";
            annualAdjustmentsToPensionsLoad.Text = "Annual Adjustments To Pensions file :";
            this.actualReturnOnAssetsLoad.Text = "Actual Return on Assets file :";
        }

        private void loadButton_Click(object sender, EventArgs e)
        {
            var data1 = discountRateLoad.GetData((int)maxYearsNumericUpDown.Value, (int)maxScenariosNumericUpDown.Value);
            var data2 = annualAdjustmentsToPensionsLoad.GetData((int)maxYearsNumericUpDown.Value, (int)maxScenariosNumericUpDown.Value);
            var data3 = this.actualReturnOnAssetsLoad.GetData((int)maxYearsNumericUpDown.Value, (int)maxScenariosNumericUpDown.Value);

            if (data1 == null || data2 == null || data3 == null)
            {
                MessageBox.Show("All files must be specified!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            _scenarioItemRepository.DeleteAll();

            int scenarioCount = Math.Min(data1.GetLength(0), Math.Min(data2.GetLength(0), data3.GetLength(0)));
            scenarioCount = Math.Min(scenarioCount, (int)maxScenariosNumericUpDown.Value);

            int years = Math.Min(data1.GetLength(1), Math.Min(data2.GetLength(1), data3.GetLength(1)));
            years = Math.Min(years, (int)maxYearsNumericUpDown.Value);

            for (int i = 0; i < scenarioCount; i++)
            {
                var rowDr = new double[years + 1];
                var rowInf = new double[years + 1];
                var rowEr = new double[years + 1];

                const double percentageCoef = 100;
                for (int j = 0; j < years; j++)
                {
                    rowDr[j + 1] = data1[i, j] * percentageCoef;
                    rowInf[j + 1] = data2[i, j] * percentageCoef;
                    rowEr[j + 1] = data3[i, j] * percentageCoef;
                }

                var scenario = new ScenarioItem(years + 1,
                    new[]
                        {
                            rowDr,
                            rowInf,
                            rowEr
                        }
                    );

                _scenarioItemRepository.Save(scenario);
            }

            DialogResult = DialogResult.OK;
            Close();

            // multiply scenarios
            //var scenarios = _scenarioItemRepository.GetAll().ToArray();
            //for(int j=0;j<49;j++)
            //    foreach (var scenario in scenarios)
            //    {
            //        _scenarioItemRepository.Save(scenario);
            //        scenarioListBox.Items.Add(scenario);
            //    }

            // old init
            //for (int i = 0; i < 5; i++)
            //{
            //    var scenario = new ScenarioItem(
            //        new []
            //            {
            //                new [] { 6.2, 5.986891495, 6.123094177, 5.450633872, 5.003749887, 4.892834789 },
            //                new [] { 2.9, 2.672707644, 3.327225457, 3.288873551, 3.115229091, 2.930276373 },
            //                new [] { -9.07, 8.6591071508, 7.3874589246, 6.4868245845, 6.1610905391, 4.6413102163}
            //            });
            //    _scenarioItemRepository.Save(scenario);
            //    scenarioListBox.Items.Add(scenario);

            //}            
        }

    }
}
