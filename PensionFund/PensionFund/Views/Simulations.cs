﻿using System;
using System.Drawing;
using System.Windows.Forms;
using PensionFund.Data;
using PensionFund.Domain;
using ZedGraph;

namespace PensionFund.Views
{
    public partial class Simulations : UserControl
    {
        private readonly IRepository<ModelItem> _modelItemRepository = new ModelItemRepository();
        private readonly IRepository<ScenarioItem> _scenarioRepository = new ScenarioItemRepository();

        private static readonly double[] _xValues = new double[] {2008, 2009, 2010, 2011, 2012, 2013};

        public event EventHandler ModelChanged = delegate { };

        public Simulations()
        {
            this.InitializeComponent();

            SetupInputGraph(this.zedGraphControl1, "Discount Rate");
            SetupInputGraph(this.zedGraphControl2, "Annual Adjustments to Pensions");
            SetupInputGraph(this.zedGraphControl3, "Actual Return On Assets");

            this.scenarioSelectionUserControl.Changed += this.ScenarioSelectionUserControlChanged;
        }

        private void ScenarioSelectionUserControlChanged(object sender, EventArgs e)
        {
            this.UpdateSelectedScenarioCurves();
            this.ReloadInputGraphs();
        }

        private void UpdateSelectedScenarioCurves()
        {
            int value = this.GetSelectedScenario();

            foreach (var control in this.simulationContainerPanel.Controls)
            {
                ((SimulationPanel) control).SelectScenario(value);
            }
        }

        private int GetSelectedScenario()
        {
            int value;
            int.TryParse(this.scenarioSelectionUserControl.Value, out value);
            return value;
        }

        public void Reload()
        {
            this.ReloadModelTreeView();

            this.ReloadSimulationPanels();

            this.ReloadInputGraphs();

            this.scenarioSelectionUserControl.Max = this._scenarioRepository.Count;

            this.UpdateSelectedScenarioCurves();
        }

        private void ReloadModelTreeView()
        {
            this.modelTreeview.Load(this._modelItemRepository.GetAll());
        }

        private void ReloadInputGraphs()
        {
            this.LoadInputGraph(this.zedGraphControl1, ModelItem.DiscountRate, Color.Firebrick);
            this.LoadInputGraph(this.zedGraphControl2, ModelItem.AnnualAdjustmentsToPensions, Color.SlateBlue);
            this.LoadInputGraph(this.zedGraphControl3, ModelItem.ActualReturnOnAssets, Color.DarkOliveGreen);
        }

        private void LoadInputGraph(ZedGraphControl zgc, string itemName, Color color)
        {
            zgc.GraphPane.CurveList.Clear();

            int scenarioIndex = this.GetSelectedScenario();
            if (scenarioIndex > 0 && this._scenarioRepository.Count > scenarioIndex)
            {
                ScenarioItem scenario = this._scenarioRepository.Load(scenarioIndex - 1);
                double[] vector;
                if (scenario.Calculator.Values.TryGetValue(itemName, out vector))
                {
                    LineItem curve = zgc.GraphPane.AddCurve("", _xValues, vector, color);
                    curve.Line.Width = 2;
                    curve.Symbol.IsVisible = false;
                }
            }

            zgc.AxisChange();
            zgc.Invalidate();
        }

        private static void SetupInputGraph(ZedGraphControl zgc, string title)
        {
            zgc.IsAntiAlias = true;
            zgc.GraphPane.Title.Text = title;
            zgc.GraphPane.XAxis.Title.IsVisible = false;
            zgc.GraphPane.YAxis.Title.IsVisible = false;
            zgc.GraphPane.XAxis.Scale.Min = _xValues[0];
            zgc.GraphPane.XAxis.Scale.Max = _xValues[_xValues.Length - 1];
        }

        private void ReloadSimulationPanels()
        {
            this.simulationContainerPanel.SuspendLayout();
            this.simulationContainerPanel.Controls.Clear();

            foreach (ModelItem modelItem in this._modelItemRepository.Find(mi => mi.PanelDisplayed))
            {
                var panel = new SimulationPanel(modelItem) {Dock = DockStyle.Top};
                this.simulationContainerPanel.Controls.Add(panel);
            }

            this.simulationContainerPanel.ResumeLayout(true);

            this.UpdateSelectedScenarioCurves();
        }

        public void AddSimulationPanel(ModelItem modelItem)
        {
            foreach (var control in this.simulationContainerPanel.Controls)
            {
                if (((SimulationPanel) control).ModelItem == modelItem)
                {
                    this.simulationContainerPanel.ScrollControlIntoView((SimulationPanel) control);
                    return;
                }
            }

            modelItem.PanelDisplayed = true;
            var panel = new SimulationPanel(modelItem) {Dock = DockStyle.Top};
            this.simulationContainerPanel.Controls.Add(panel);
            this.simulationContainerPanel.ScrollControlIntoView(panel);

            this.UpdateSelectedScenarioCurves();
        }

        private void addModelItemButton_Click(object sender, EventArgs e)
        {
            var modelItemEditor = new ModelItemEditor(null);
            if (modelItemEditor.ShowDialog(this) == DialogResult.OK)
            {
                this.RefreshModelItem();
            }
        }

        private void RefreshModelItem()
        {
            this.ReloadModelTreeView();
            this.ModelChanged(this, new EventArgs());
        }

        private void modelTreeview_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (e.Node.Tag != null)
                this.AddSimulationPanel((ModelItem) e.Node.Tag);
        }

        public void DeleteAll()
        {
            this.simulationContainerPanel.Controls.Clear();
        }

        private bool _graphsShowing = true;

        private void showInputGraphs_Click(object sender, EventArgs e)
        {
            if (this._graphsShowing)
            {
                this.inputGraphsPanel.Visible = false;
                this.topPanel.Height = this.buttonPanel.Height;
            }
            else
            {
                this.inputGraphsPanel.Visible = true;
                this.topPanel.Height = 256;
            }

            this._graphsShowing = !this._graphsShowing;
        }

        private void editModelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.modelTreeview.SelectedNode == null) return;

            ModelItem modelItem = this.modelTreeview.SelectedNode.Tag as ModelItem;
            if (modelItem == null) return;

            var modelItemEditor = new ModelItemEditor(modelItem);
            if (modelItemEditor.ShowDialog(this) == DialogResult.OK)
            {
                this.RefreshModelItem();
            }
        }

        private void removeLineItemToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.modelTreeview.SelectedNode == null) return;

            ModelItem modelItem = this.modelTreeview.SelectedNode.Tag as ModelItem;
            if (modelItem == null) return;

            if(MessageBox.Show("Are you sure you want to remove this line item? (If the line item is used in other formulas, they will no longer work)", 
                "Remove Line Item", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                _modelItemRepository.Delete(modelItem);
                this.RefreshModelItem();
            }
        }

        private void modelContextMenuStrip_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            TreeNode selectedNode = this.modelTreeview.SelectedNode;

            editModelToolStripMenuItem.Enabled =
            removeLineItemToolStripMenuItem.Enabled =
                selectedNode != null && selectedNode.Tag is ModelItem && !((ModelItem)selectedNode.Tag).IsInput();
        }
    }
}