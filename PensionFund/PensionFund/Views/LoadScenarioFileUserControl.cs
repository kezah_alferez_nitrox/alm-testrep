﻿using System;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using PensionFund.IO;

namespace PensionFund.Views
{
    public partial class LoadScenarioFileUserControl : UserControl
    {
        private Regex regex2D = new Regex(@"^2", RegexOptions.Compiled);
        private Regex regex3D = new Regex(@"^3", RegexOptions.Compiled);
        private Regex regexBIN = new Regex(@".bin$", RegexOptions.Compiled);
        private Regex regexCSV = new Regex(@".csv$", RegexOptions.Compiled);

        public override string Text
        {
            get
            {
                return label.Text;
            }
            set
            {
                label.Text = value;
            }
        }

        protected virtual double[,] Data { get; private set; }

        public string FileName { get { return openFileDialog.FileName; } }

        public LoadScenarioFileUserControl()
        {
            InitializeComponent();    
        }

        protected virtual void showFileDialog_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    SetFileType(openFileDialog.SafeFileName);
                    fileTextBox.Text = openFileDialog.FileName;
                }
                catch(Exception ex)
                {
                    Logger.Log(ex);
                    MessageBox.Show("Cannot read file!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        public virtual double[,] GetData(int years, int scenarious)
        {
            Data = FillData(openFileDialog.FileName, years, scenarious);
            return Data;
        }

        protected double[,] FillData(string filePath, int years, int scenarious)
        {
            double[,] data = null;

            if (string.IsNullOrEmpty(openFileDialog.FileName))
                return data;
            
            if (radioButton2D.Checked)
            {
                if (radioButtonBIN.Checked)
                    data = new BinScenarioReader(openFileDialog.FileName).ReadAll2D(years, scenarious);
                else if (radioButtonCSV.Checked)
                    data = new CsvScenarioReader(openFileDialog.FileName).ReadAll2D(years, scenarious);
            }
            else if (radioButton3D.Checked)
            {
                if (radioButtonBIN.Checked)
                    data = new BinScenarioReader(openFileDialog.FileName).ReadAll3DTo2D(years, scenarious);
                else if (radioButtonCSV.Checked)
                    data = new CsvScenarioReader(openFileDialog.FileName).ReadAll3DTo2D(years, scenarious);
            }

            return data;
        }
        
        protected void SetFileType(string text)
        {
            if (regex3D.IsMatch(text))
                radioButton3D.Checked = true;
            else
                radioButton2D.Checked = true;

            if (regexCSV.IsMatch(text))
                radioButtonCSV.Checked = true;
            else
                radioButtonBIN.Checked = true;
        }
    }
}
