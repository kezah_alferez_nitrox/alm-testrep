﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using PensionFund.Data;
using PensionFund.Domain;
using System.Diagnostics;

namespace PensionFund.Views
{
    public partial class FootNote : UserControl
    {
        private DataGridViewButtonColumn _expanderColumn;
        private DataGridViewTextBoxColumn _labelColumn;
        private DataGridViewTextBoxColumn _year0Column;
        private DataGridViewTextBoxColumn[] _yearColumns;
        private DataGridViewTextBoxColumn _commentColumn;
        private DataGridViewButtonColumn _seeModelColumn;

        private readonly ModelItemRepository _modelItemRepository = new ModelItemRepository();

        public event EventHandler ShowModel;

        private readonly DataGridViewCellStyle _numericCellStyle;
        private int _startYear = 2008;

        private ScenarioItem _scenarioItem;

        private DataGridViewCell _lastCurrentCell;

        public FootNote()
        {
            this.InitializeComponent();

            this.footnoteGrid.CurrentCellChanged += this.FootnoteGridCurrentCellChanged;

            this._numericCellStyle = new DataGridViewCellStyle();
            this._numericCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this._numericCellStyle.Format = "N2";

            this.InitGrid();
        }

        private void InitGrid()
        {
            this.CreateBaseGridColumns();

            this.footnoteGrid.CellPainting += this.FootnoteGridCellPainting;
        }

        private void CreateBaseGridColumns()
        {
            ((ISupportInitialize)(this.footnoteGrid)).BeginInit();

            this.footnoteGrid.Columns.Clear();

            var boldFont = new Font(this.footnoteGrid.Font, FontStyle.Bold);

            // 
            // _expanderColumn
            // 
            this._expanderColumn = new DataGridViewButtonColumn();
            this._expanderColumn.Frozen = true;
            this._expanderColumn.HeaderText = "";
            this._expanderColumn.Name = "_expanderColumn";
            this._expanderColumn.ReadOnly = true;
            this._expanderColumn.Text = "-";
            this._expanderColumn.Width = 24;
            this._expanderColumn.SortMode = DataGridViewColumnSortMode.NotSortable;
            this.footnoteGrid.Columns.Add(this._expanderColumn);
            // 
            // _labelColumn
            // 
            this._labelColumn = new DataGridViewTextBoxColumn();
            this._labelColumn.Frozen = true;
            this._labelColumn.HeaderText = "";
            this._labelColumn.Name = "_labelColumn";
            this._labelColumn.ReadOnly = true;
            this._labelColumn.Width = 250;
            this._labelColumn.SortMode = DataGridViewColumnSortMode.NotSortable;
            this.footnoteGrid.Columns.Add(this._labelColumn);
            // 
            // _year0Column
            // 
            this._year0Column = new DataGridViewTextBoxColumn();
            this._year0Column.DefaultCellStyle = this._numericCellStyle.Clone();
            this._year0Column.HeaderText = this._startYear.ToString();
            this._year0Column.Name = "_year0Column";
            this._year0Column.Width = 80;
            this._year0Column.HeaderCell.Style.Font = boldFont;
            this._year0Column.DefaultCellStyle.Font = boldFont;
            this._year0Column.SortMode = DataGridViewColumnSortMode.NotSortable;
            this.footnoteGrid.Columns.Add(this._year0Column);
            // 
            // _commentColumn
            // 
            this._commentColumn = new DataGridViewTextBoxColumn();
            this._commentColumn.HeaderText = "Comment";
            this._commentColumn.Name = "_commentColumn";
            this._commentColumn.Width = 200;
            this._commentColumn.SortMode = DataGridViewColumnSortMode.NotSortable;
            this.footnoteGrid.Columns.Add(this._commentColumn);
            // 
            // _seeModelColumn
            // 
            this._seeModelColumn = new DataGridViewButtonColumn();
            this._seeModelColumn.HeaderText = "See Model";
            this._seeModelColumn.Name = "_seeModelColumn";
            this._seeModelColumn.ReadOnly = true;
            this._seeModelColumn.Text = "See Model";
            this._seeModelColumn.UseColumnTextForButtonValue = true;
            this._seeModelColumn.Width = 75;
            this._seeModelColumn.SortMode = DataGridViewColumnSortMode.NotSortable;
            this.footnoteGrid.Columns.Add(this._seeModelColumn);

            ((ISupportInitialize)(this.footnoteGrid)).EndInit();
        }

        private void CreateCalculatedYearColumns(int count)
        {
            if (count <= 0) return;

            int yearColumnCount = this._commentColumn.Index - this._year0Column.Index - 1;
            if (count == yearColumnCount) return;

            for (int i = 0; i < yearColumnCount; i++)
            {
                this.footnoteGrid.Columns.RemoveAt(this._year0Column.Index + 1);
            }

            this._yearColumns = new DataGridViewTextBoxColumn[count];
            for (int i = 0; i < count; i++)
            {
                DataGridViewTextBoxColumn column = new DataGridViewTextBoxColumn();
                column.DefaultCellStyle = this._numericCellStyle;
                column.HeaderText = (this._startYear + i + 1).ToString();
                column.Name = "yearColumn" + (i + 1);
                column.ReadOnly = true;
                column.Width = 80;
                column.SortMode = DataGridViewColumnSortMode.NotSortable;
                this.footnoteGrid.Columns.Insert(this._year0Column.Index + i + 1, column);

                this._yearColumns[i] = column;
            }
        }

        private void FootnoteGridCellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                if (this.footnoteGrid.Rows[e.RowIndex].Tag == null)
                {
                    if (e.ColumnIndex == this._seeModelColumn.Index)
                    {
                        e.PaintBackground(e.ClipBounds, false);
                        e.Handled = true;
                    }
                }
                else
                {
                    if (e.ColumnIndex == 0)
                    {
                        e.PaintBackground(e.ClipBounds, false);
                        e.Handled = true;
                    }
                }
            }
        }

        public void Refresh(ScenarioItem scenarioItem)
        {
            this._scenarioItem = scenarioItem.Clone();
            this.labelFootNote.Text = "Pension Footnote - " + scenarioItem;
            this.RefreshDataForSelectedScenario();
        }

        public void LoadModelDefinition()
        {
            this.footnoteGrid.Rows.Clear();

            var groups = new List<string>();

            foreach (var item in this._modelItemRepository.GetAll())
                if (!groups.Contains(item.Group))
                    groups.Add(item.Group);

            var boldFont = new Font(this.footnoteGrid.Font, FontStyle.Bold);

            foreach (var group in groups)
            {
                int rowIndex = this.footnoteGrid.Rows.Add(new object[] { "", group });
                this.footnoteGrid.Rows[rowIndex].DefaultCellStyle.Font = boldFont;
                this.LoadGroup(rowIndex);
            }

            this.RefreshDataForSelectedScenario();
        }

        private void LoadGroup(int index)
        {
            var group = (string)this.footnoteGrid.Rows[index].Cells[1].Value;
            this.footnoteGrid.Rows[index].Cells[0].Value = " - ";
            this.footnoteGrid.Rows[index].ReadOnly = true;

            foreach (var modelItem in this._modelItemRepository.Find(m => m.Group == group))
            {
                this.footnoteGrid.Rows.Insert(++index, new object[] { null, modelItem.DisplayName, modelItem.InitalValue });
                this.footnoteGrid.Rows[index].Cells[this._commentColumn.Index].Value = modelItem.Comment;
                this.footnoteGrid.Rows[index].Tag = modelItem;

                if (modelItem.IsInput())
                {
                    this.footnoteGrid.Rows[index].DefaultCellStyle.BackColor = Color.LemonChiffon;
                }
                else if (modelItem.Type == ModelItemType.Vector)
                {
                    this.footnoteGrid.Rows[index].DefaultCellStyle.BackColor = Color.LightCyan;
                }
            }
        }

        private void UnloadGroup(int index)
        {
            this.footnoteGrid.Rows[index].Cells[0].Value = " + ";
            while (index + 1 < this.footnoteGrid.Rows.Count && this.footnoteGrid.Rows[index + 1].Tag != null)
                this.footnoteGrid.Rows.RemoveAt(index + 1);
        }

        private void footnoteGrid_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == this._seeModelColumn.Index && this.footnoteGrid.Rows[e.RowIndex].Tag != null)
                this.ShowModel(this.footnoteGrid.Rows[e.RowIndex].Tag, null);

            if (e.ColumnIndex == 0 && this.footnoteGrid.Rows[e.RowIndex].Tag == null)
                if ((string)this.footnoteGrid.Rows[e.RowIndex].Cells[0].Value == " + ")
                    this.LoadGroup(e.RowIndex);
                else
                    this.UnloadGroup(e.RowIndex);
        }

        public void RefreshDataForSelectedScenario()
        {
            if (_scenarioItem == null) return;

            this.footnoteGrid.CurrentCellChanged -= this.FootnoteGridCurrentCellChanged;

            this._scenarioItem.TryEvaluate();

            this.CreateCalculatedYearColumns(this._scenarioItem.YearCount - 1);
            foreach (DataGridViewRow row in this.footnoteGrid.Rows)
            {
                ModelItem modelItem = row.Tag as ModelItem;
                if (modelItem == null) continue;

                row.Cells[this._year0Column.Index].Value = double.IsNaN(modelItem.InitalValue) ? (object)null : modelItem.InitalValue;

                double[] values;
                if (modelItem.IsVector())
                {
                    modelItem.EnsureVectorValueCount(this._scenarioItem.YearCount);
                    values = ((List<double>)modelItem.VectorValues).ToArray();
                }
                else
                {
                    this._scenarioItem.Calculator.Values.TryGetValue(modelItem.FunctionName, out values);
                }

                this.DisplayValuesIntoRow(values, row, modelItem.IsInput() || modelItem.IsVector());
            }

            this.footnoteGrid.CurrentCellChanged += this.FootnoteGridCurrentCellChanged;

            CellEnter(footnoteGrid.CurrentCell);
        }

        private void DisplayValuesIntoRow(double[] values, DataGridViewRow row, bool writable)
        {
            if (values == null) return;

            for (int t = 1; t < values.Length; t++)
            {
                DataGridViewCell cell = row.Cells[this._yearColumns[t - 1].Index];
                cell.Value = double.IsNaN(values[t]) ? (object)null : values[t];
                cell.ReadOnly = !writable;
            }
        }

        private void footnoteGrid_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            ModelItem modelItem = this.footnoteGrid.Rows[e.RowIndex].Tag as ModelItem;
            if (modelItem == null) return;

            if (e.ColumnIndex == this._commentColumn.Index)
            {
                modelItem.Comment = (string)this.footnoteGrid[e.ColumnIndex, e.RowIndex].Value;
                return;
            }
            if (e.ColumnIndex == this._year0Column.Index)
            {
                modelItem.InitalValue = Util.ConvertToDouble(this.footnoteGrid[e.ColumnIndex, e.RowIndex].Value);
                if (this._scenarioItem != null) this._scenarioItem.ModelItems[modelItem.FunctionName].InitalValue = modelItem.InitalValue;
                this.RefreshDataForSelectedScenario();
                return;
            }
            int yearIndex = this.GetYearIndex(e.ColumnIndex);
            if (yearIndex > 0 && (modelItem.IsInput() || modelItem.IsVector()))
            {
                double value = Util.ConvertToDouble(this.footnoteGrid[e.ColumnIndex, e.RowIndex].Value);
                if (!double.IsNaN(value))
                {
                    formulaEditorTextBox.Text = value.ToString();
                    if (modelItem.IsInput())
                    {
                        this._scenarioItem.Calculator.Values[modelItem.FunctionName][yearIndex] = value;
                    }
                    else
                    {
                        modelItem.SetVectorValue(yearIndex, value);
                    }
                    this.RefreshDataForSelectedScenario();
                }
            }
        }

        private int GetYearIndex(int columnIndex)
        {
            if (this._yearColumns == null || this._yearColumns.Length == 0 || this._yearColumns[0] == null) return -1;

            if (columnIndex < this._yearColumns[0].Index ||
                columnIndex > this._yearColumns[this._yearColumns.Length - 1].Index)
                return -1;

            return columnIndex - this._yearColumns[0].Index + 1;
        }

        public void ClipboardPaste()
        {
            string clipboardTxt = Clipboard.GetText();
            if (string.IsNullOrEmpty(clipboardTxt)) return;

            string[] clipboardRows = clipboardTxt.Split(new[] { Environment.NewLine },
                                                        StringSplitOptions.RemoveEmptyEntries);
            if (clipboardRows.Length == 0) return;

            if (this.footnoteGrid.CurrentRow == null) return;

            this.footnoteGrid.EndEdit();

            int gridRow = this.footnoteGrid.CurrentRow.Index;
            foreach (string data in clipboardRows)
            {
                while (this.footnoteGrid.Rows.Count > gridRow && !(this.footnoteGrid.Rows[gridRow].Tag is ModelItem))
                    gridRow++;

                if (gridRow >= this.footnoteGrid.Rows.Count) break;

                ModelItem modelItem = this.footnoteGrid.Rows[gridRow].Tag as ModelItem;

                double doubleValue = Util.ConvertToDouble(data);
                if (double.IsNaN(doubleValue)) continue;

                modelItem.InitalValue = doubleValue;

                this.footnoteGrid[this._year0Column.Index, gridRow].Value = doubleValue.ToString();

                gridRow++;
            }
        }

        public void DisplayModelDefinition()
        {
            this.LoadModelDefinition();
        }

        private void FootnoteGridCurrentCellChanged(object sender, EventArgs e)
        {
            if (this._scenarioItem == null || footnoteGrid.IsCurrentCellInEditMode) return;

            CellLeave(_lastCurrentCell);
            CellEnter(footnoteGrid.CurrentCell);

            this._lastCurrentCell = footnoteGrid.CurrentCell;
        }

        private void CellEnter(DataGridViewCell cell)
        {
            if (cell == null) return;

            ModelItem modelItem = footnoteGrid.Rows[cell.RowIndex].Tag as ModelItem;

            int yearIndex = this.GetYearIndex(cell.ColumnIndex);

            if (modelItem == null || (modelItem.IsInput() && yearIndex < 0))
            {
                formulaEditorTextBox.Text = "";
                formulaEditorTextBox.Enabled = false;
            }
            else if (modelItem.IsInput())
            {
                formulaEditorTextBox.Text = this._scenarioItem.Calculator.Values[modelItem.FunctionName][yearIndex].ToString();
                formulaEditorTextBox.Enabled = true;
            }
            else
            {
                formulaEditorTextBox.Text = this._scenarioItem.ModelItems[modelItem.FunctionName].Formula;
                formulaEditorTextBox.Enabled = true;
            }
        }

        private void CellLeave(DataGridViewCell cell)
        {
            if (cell == null) return;

            ModelItem modelItem = footnoteGrid.Rows[cell.RowIndex].Tag as ModelItem;
            if (modelItem == null) return;

            int yearIndex = this.GetYearIndex(cell.ColumnIndex);

            if (modelItem.IsInput())
            {
                if (yearIndex > 0)
                {
                    double value = Util.ConvertToDouble(formulaEditorTextBox.Text);
                    if (!double.IsNaN(value))
                    {
                        this._scenarioItem.Calculator.Values[modelItem.FunctionName][yearIndex] = value;
                        cell.Value = value;
                        this.RefreshDataForSelectedScenario();
                    }
                }
            }
            else
            {
                this._scenarioItem.ModelItems[modelItem.FunctionName].Formula = formulaEditorTextBox.Text;
                this.RefreshDataForSelectedScenario();
            }
        }

        private void formulaEditorTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (footnoteGrid.IsCurrentCellInEditMode) return;
                e.Handled = true;
                CellLeave(_lastCurrentCell);
            }
        }
    }
}