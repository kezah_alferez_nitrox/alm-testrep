﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using PensionFund.IO;

namespace PensionFund.Views
{
    class LoadWeightedScenarioFilesUserControl : LoadScenarioFileUserControl
    {
        private DataGridView grid;

        public LoadWeightedScenarioFilesUserControl()
        {
            this.InitializeComponent();

            grid.AutoGenerateColumns = false;

            openFileDialog.Multiselect = true;
        }

        private void InitializeComponent()
        {
            this.grid = new System.Windows.Forms.DataGridView();
            this.fileColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.weightColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.removeColumn = new System.Windows.Forms.DataGridViewButtonColumn();
            ((System.ComponentModel.ISupportInitialize)(this.grid)).BeginInit();
            this.SuspendLayout();
            // 
            // grid
            // 
            this.grid.AllowUserToAddRows = false;
            this.grid.AllowUserToDeleteRows = false;
            this.grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.fileColumn,
            this.weightColumn,
            this.removeColumn});
            this.grid.Location = new System.Drawing.Point(6, 77);
            this.grid.Name = "grid";
            this.grid.RowHeadersVisible = false;
            this.grid.Size = new System.Drawing.Size(355, 131);
            this.grid.TabIndex = 8;
            this.grid.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grid_CellContentClick);
            // 
            // fileColumn
            // 
            this.fileColumn.DataPropertyName = "FileName";
            this.fileColumn.HeaderText = "File";
            this.fileColumn.Name = "fileColumn";
            this.fileColumn.ReadOnly = true;
            this.fileColumn.Width = 230;
            // 
            // weightColumn
            // 
            this.weightColumn.DataPropertyName = "Weight";
            this.weightColumn.HeaderText = "Weight";
            this.weightColumn.Name = "weightColumn";
            this.weightColumn.Width = 50;
            // 
            // removeColumn
            // 
            this.removeColumn.HeaderText = "Remove";
            this.removeColumn.Name = "removeColumn";
            this.removeColumn.Text = "X";
            this.removeColumn.UseColumnTextForButtonValue = true;
            this.removeColumn.Width = 50;
            // 
            // LoadWeightedScenarioFilesUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.Controls.Add(this.grid);
            this.Name = "LoadWeightedScenarioFilesUserControl";
            this.Size = new System.Drawing.Size(367, 216);
            this.Controls.SetChildIndex(this.fileTextBox, 0);
            this.Controls.SetChildIndex(this.grid, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private DataGridViewTextBoxColumn fileColumn;
        private DataGridViewTextBoxColumn weightColumn;
        private DataGridViewButtonColumn removeColumn;

        private readonly IList<DataFile> dataFiles = new List<DataFile>();

        protected override void showFileDialog_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                SetFileType(openFileDialog.SafeFileNames[0]);
                foreach (string fileName in openFileDialog.FileNames)
                {
                    try
                    {
                        DataFile dataFile = new DataFile
                        {
                            FullFileName = fileName,
                            FileName = Path.GetFileName(fileName),
                            Weight = 1.0
                        };
                        dataFiles.Add(dataFile);
                    }
                    catch (Exception ex)
                    {
                        Logger.Log(ex);
                        MessageBox.Show(string.Format("Cannot read file {0} !", fileName), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                DisplayFiles();
            }
        }

        private void DisplayFiles()
        {
            grid.DataSource = null;
            grid.DataSource = dataFiles;

            fileTextBox.Text = dataFiles.Count > 0 ? dataFiles.Select(df => df.FileName).Aggregate((fn1, fn2) => fn1 + ";" + fn2) : "";
        }

        public override double[,] GetData(int years, int scenarious)
        {
            foreach (DataFile dataFile in dataFiles)
            {
                dataFile.Data = FillData(dataFile.FullFileName, years, scenarious);
            }
            return Data;
        }

        protected override double[,] Data
        {
            get
            {
                if (dataFiles.Count == 0) return null;

                double[,] firstData = this.dataFiles[0].Data;
                int length0 = firstData.GetLength(0);
                int length1 = firstData.GetLength(1);

                double totalWeight = dataFiles.Sum(df => df.Weight);
                if (totalWeight == 0) return null;

                double[,] data = new double[length0, length1];
                for (int i = 0; i < length0; i++)
                {
                    for (int j = 0; j < length1; j++)
                    {
                        int index0 = i;
                        int index1 = j;
                        data[i, j] = dataFiles.Sum(df => df.Data[index0, index1] * df.Weight) / totalWeight;
                    }
                }

                return data;
            }
        }

        private void grid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (grid.Columns[e.ColumnIndex].Name == "removeColumn")
            {
                // grid.DataSource = null;
                dataFiles.RemoveAt(e.RowIndex);

                DisplayFiles();
            }
        }

        private class DataFile
        {
            public string FullFileName { get; set; }
            public string FileName { get; set; }
            public double Weight { get; set; }
            public double[,] Data { get; set; }
        }
    }
}
