﻿namespace PensionFund.Views
{
    partial class FootNote
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.footnoteGrid = new System.Windows.Forms.DataGridView();
            this.headerPanel = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.formulaEditorTextBox = new System.Windows.Forms.TextBox();
            this.labelFootNote = new System.Windows.Forms.Label();
            this.selectedNodeLabel = new System.Windows.Forms.Label();
            this.panelFootNote = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.footnoteGrid)).BeginInit();
            this.headerPanel.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panelFootNote.SuspendLayout();
            this.SuspendLayout();
            // 
            // footnoteGrid
            // 
            this.footnoteGrid.AllowUserToAddRows = false;
            this.footnoteGrid.AllowUserToDeleteRows = false;
            this.footnoteGrid.BackgroundColor = System.Drawing.SystemColors.Control;
            this.footnoteGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.footnoteGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.footnoteGrid.Location = new System.Drawing.Point(0, 58);
            this.footnoteGrid.Name = "footnoteGrid";
            this.footnoteGrid.Size = new System.Drawing.Size(812, 450);
            this.footnoteGrid.TabIndex = 5;
            this.footnoteGrid.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.footnoteGrid_CellEndEdit);
            this.footnoteGrid.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.footnoteGrid_CellClick);
            // 
            // headerPanel
            // 
            this.headerPanel.BackColor = System.Drawing.SystemColors.ControlDark;
            this.headerPanel.Controls.Add(this.panel1);
            this.headerPanel.Controls.Add(this.labelFootNote);
            this.headerPanel.Controls.Add(this.selectedNodeLabel);
            this.headerPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.headerPanel.Location = new System.Drawing.Point(0, 0);
            this.headerPanel.Name = "headerPanel";
            this.headerPanel.Size = new System.Drawing.Size(812, 58);
            this.headerPanel.TabIndex = 4;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.formulaEditorTextBox);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 32);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(812, 26);
            this.panel1.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label1.Location = new System.Drawing.Point(7, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(23, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "f(t)";
            // 
            // formulaEditorTextBox
            // 
            this.formulaEditorTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.formulaEditorTextBox.Location = new System.Drawing.Point(36, 3);
            this.formulaEditorTextBox.Name = "formulaEditorTextBox";
            this.formulaEditorTextBox.Size = new System.Drawing.Size(773, 20);
            this.formulaEditorTextBox.TabIndex = 2;
            this.formulaEditorTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.formulaEditorTextBox_KeyPress);
            // 
            // labelFootNote
            // 
            this.labelFootNote.AutoSize = true;
            this.labelFootNote.BackColor = System.Drawing.Color.Transparent;
            this.labelFootNote.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold);
            this.labelFootNote.ForeColor = System.Drawing.Color.White;
            this.labelFootNote.Location = new System.Drawing.Point(6, 5);
            this.labelFootNote.Name = "labelFootNote";
            this.labelFootNote.Size = new System.Drawing.Size(175, 24);
            this.labelFootNote.TabIndex = 1;
            this.labelFootNote.Text = "Pension Footnote";
            // 
            // selectedNodeLabel
            // 
            this.selectedNodeLabel.AutoSize = true;
            this.selectedNodeLabel.BackColor = System.Drawing.Color.Transparent;
            this.selectedNodeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold);
            this.selectedNodeLabel.ForeColor = System.Drawing.Color.White;
            this.selectedNodeLabel.Location = new System.Drawing.Point(6, 6);
            this.selectedNodeLabel.Name = "selectedNodeLabel";
            this.selectedNodeLabel.Size = new System.Drawing.Size(0, 24);
            this.selectedNodeLabel.TabIndex = 1;
            // 
            // panelFootNote
            // 
            this.panelFootNote.Controls.Add(this.footnoteGrid);
            this.panelFootNote.Controls.Add(this.headerPanel);
            this.panelFootNote.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelFootNote.Location = new System.Drawing.Point(0, 0);
            this.panelFootNote.Name = "panelFootNote";
            this.panelFootNote.Size = new System.Drawing.Size(812, 508);
            this.panelFootNote.TabIndex = 6;
            // 
            // FootNote
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelFootNote);
            this.Name = "FootNote";
            this.Size = new System.Drawing.Size(812, 508);
            ((System.ComponentModel.ISupportInitialize)(this.footnoteGrid)).EndInit();
            this.headerPanel.ResumeLayout(false);
            this.headerPanel.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panelFootNote.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView footnoteGrid;
        private System.Windows.Forms.Panel headerPanel;
        private System.Windows.Forms.Label labelFootNote;
        private System.Windows.Forms.Label selectedNodeLabel;
        private System.Windows.Forms.Panel panelFootNote;
        private System.Windows.Forms.TextBox formulaEditorTextBox;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
    }
}
