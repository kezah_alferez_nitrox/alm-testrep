﻿namespace PensionFund.Views
{
    partial class ModelItemEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.centerPanel = new System.Windows.Forms.Panel();
            this.isVectorCheckBox = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.formulaTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.initialValueTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.formulaNameTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.displayNameTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.modelTreeview = new PensionFund.Controls.ModelTreeview();
            this.bottomPanel = new System.Windows.Forms.Panel();
            this.cancelButton = new System.Windows.Forms.Button();
            this.okButton = new System.Windows.Forms.Button();
            this.mainPanel = new System.Windows.Forms.Panel();
            this.centerPanel.SuspendLayout();
            this.bottomPanel.SuspendLayout();
            this.mainPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // centerPanel
            // 
            this.centerPanel.Controls.Add(this.isVectorCheckBox);
            this.centerPanel.Controls.Add(this.label5);
            this.centerPanel.Controls.Add(this.formulaTextBox);
            this.centerPanel.Controls.Add(this.label4);
            this.centerPanel.Controls.Add(this.initialValueTextBox);
            this.centerPanel.Controls.Add(this.label3);
            this.centerPanel.Controls.Add(this.formulaNameTextBox);
            this.centerPanel.Controls.Add(this.label2);
            this.centerPanel.Controls.Add(this.displayNameTextBox);
            this.centerPanel.Controls.Add(this.label1);
            this.centerPanel.Controls.Add(this.modelTreeview);
            this.centerPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.centerPanel.Location = new System.Drawing.Point(0, 0);
            this.centerPanel.Name = "centerPanel";
            this.centerPanel.Size = new System.Drawing.Size(871, 431);
            this.centerPanel.TabIndex = 1;
            // 
            // isVectorCheckBox
            // 
            this.isVectorCheckBox.AutoSize = true;
            this.isVectorCheckBox.Location = new System.Drawing.Point(343, 65);
            this.isVectorCheckBox.Name = "isVectorCheckBox";
            this.isVectorCheckBox.Size = new System.Drawing.Size(15, 14);
            this.isVectorCheckBox.TabIndex = 10;
            this.isVectorCheckBox.UseVisualStyleBackColor = true;
            this.isVectorCheckBox.CheckedChanged += new System.EventHandler(this.isVectorCheckBox_CheckedChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(226, 66);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Is Vector";
            // 
            // formulaTextBox
            // 
            this.formulaTextBox.Location = new System.Drawing.Point(343, 114);
            this.formulaTextBox.Multiline = true;
            this.formulaTextBox.Name = "formulaTextBox";
            this.formulaTextBox.Size = new System.Drawing.Size(516, 160);
            this.formulaTextBox.TabIndex = 8;
            this.formulaTextBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ValidateEditor);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(226, 117);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(62, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Formula (t) :";
            // 
            // initialValueTextBox
            // 
            this.initialValueTextBox.Location = new System.Drawing.Point(343, 88);
            this.initialValueTextBox.Name = "initialValueTextBox";
            this.initialValueTextBox.Size = new System.Drawing.Size(516, 20);
            this.initialValueTextBox.TabIndex = 6;
            this.initialValueTextBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ValidateEditor);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(226, 91);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Initial Value :";
            // 
            // formulaNameTextBox
            // 
            this.formulaNameTextBox.Location = new System.Drawing.Point(343, 39);
            this.formulaNameTextBox.Name = "formulaNameTextBox";
            this.formulaNameTextBox.ReadOnly = true;
            this.formulaNameTextBox.Size = new System.Drawing.Size(516, 20);
            this.formulaNameTextBox.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(226, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Formula Name :";
            // 
            // displayNameTextBox
            // 
            this.displayNameTextBox.Location = new System.Drawing.Point(343, 13);
            this.displayNameTextBox.Name = "displayNameTextBox";
            this.displayNameTextBox.Size = new System.Drawing.Size(516, 20);
            this.displayNameTextBox.TabIndex = 2;
            this.displayNameTextBox.TextChanged += new System.EventHandler(this.GenerateFormulaName);
            this.displayNameTextBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ValidateEditor);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(226, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Displayed Name :";
            // 
            // modelTreeview
            // 
            this.modelTreeview.Dock = System.Windows.Forms.DockStyle.Left;
            this.modelTreeview.Location = new System.Drawing.Point(0, 0);
            this.modelTreeview.Name = "modelTreeview";
            this.modelTreeview.Size = new System.Drawing.Size(219, 431);
            this.modelTreeview.TabIndex = 0;
            this.modelTreeview.DoubleClick += new System.EventHandler(this.modelTreeview_DoubleClick);
            // 
            // bottomPanel
            // 
            this.bottomPanel.Controls.Add(this.cancelButton);
            this.bottomPanel.Controls.Add(this.okButton);
            this.bottomPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bottomPanel.Location = new System.Drawing.Point(0, 431);
            this.bottomPanel.Name = "bottomPanel";
            this.bottomPanel.Size = new System.Drawing.Size(871, 50);
            this.bottomPanel.TabIndex = 2;
            // 
            // cancelButton
            // 
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point(703, 14);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 1;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            // 
            // okButton
            // 
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okButton.Enabled = false;
            this.okButton.Location = new System.Drawing.Point(784, 14);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(75, 23);
            this.okButton.TabIndex = 0;
            this.okButton.Text = "OK";
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Click += new System.EventHandler(this.okButton_Click);
            // 
            // mainPanel
            // 
            this.mainPanel.Controls.Add(this.centerPanel);
            this.mainPanel.Controls.Add(this.bottomPanel);
            this.mainPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainPanel.Location = new System.Drawing.Point(0, 0);
            this.mainPanel.Name = "mainPanel";
            this.mainPanel.Size = new System.Drawing.Size(871, 481);
            this.mainPanel.TabIndex = 3;
            // 
            // ModelItemEditor
            // 
            this.AcceptButton = this.okButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(871, 481);
            this.Controls.Add(this.mainPanel);
            this.Name = "ModelItemEditor";
            this.Text = "Line Item Editor";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ModelItemEditor_FormClosing);
            this.centerPanel.ResumeLayout(false);
            this.centerPanel.PerformLayout();
            this.bottomPanel.ResumeLayout(false);
            this.mainPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private PensionFund.Controls.ModelTreeview modelTreeview;
        private System.Windows.Forms.Panel centerPanel;
        private System.Windows.Forms.Panel bottomPanel;
        private System.Windows.Forms.Panel mainPanel;
        private System.Windows.Forms.TextBox displayNameTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.TextBox formulaTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox initialValueTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox formulaNameTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox isVectorCheckBox;
        private System.Windows.Forms.Label label5;
    }
}