﻿using System;
using System.IO;
using System.Windows.Forms;

namespace PensionFund.Views
{
    public partial class ChooseModelForm : Form
    {
        private readonly FileInfo[] infos;

        public ChooseModelForm(FileInfo[] infos)
        {
            this.infos = infos;
            this.InitializeComponent();

            foreach (FileInfo info in infos)
            {
                this.filesListBox.Items.Add(info.Name);
            }
        }

        public string SelectedFile
        {
            get
            {
                return this.filesListBox.SelectedIndex >= 0 ? this.infos[this.filesListBox.SelectedIndex].FullName : null;
            }
        }

        private void filesListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            okButton.Enabled = this.filesListBox.SelectedIndex >= 0;
        }

        private void filesListBox_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if(this.filesListBox.SelectedIndex >= 0)
            {
                this.DialogResult = DialogResult.OK;
            }
        }
    }
}