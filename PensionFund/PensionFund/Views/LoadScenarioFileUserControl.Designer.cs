﻿namespace PensionFund.Views
{
    partial class LoadScenarioFileUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label = new System.Windows.Forms.Label();
            this.showFileDialog = new System.Windows.Forms.Button();
            this.fileTextBox = new System.Windows.Forms.TextBox();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.radioButton2D = new System.Windows.Forms.RadioButton();
            this.radioButton3D = new System.Windows.Forms.RadioButton();
            this.radioButtonCSV = new System.Windows.Forms.RadioButton();
            this.radioButtonBIN = new System.Windows.Forms.RadioButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label
            // 
            this.label.AutoSize = true;
            this.label.Location = new System.Drawing.Point(3, 0);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(0, 13);
            this.label.TabIndex = 7;
            // 
            // showFileDialog
            // 
            this.showFileDialog.Location = new System.Drawing.Point(318, 45);
            this.showFileDialog.Name = "showFileDialog";
            this.showFileDialog.Size = new System.Drawing.Size(43, 21);
            this.showFileDialog.TabIndex = 6;
            this.showFileDialog.Text = "...";
            this.showFileDialog.UseVisualStyleBackColor = true;
            this.showFileDialog.Click += new System.EventHandler(this.showFileDialog_Click);
            // 
            // fileTextBox
            // 
            this.fileTextBox.BackColor = System.Drawing.SystemColors.Window;
            this.fileTextBox.Location = new System.Drawing.Point(6, 46);
            this.fileTextBox.Name = "fileTextBox";
            this.fileTextBox.ReadOnly = true;
            this.fileTextBox.Size = new System.Drawing.Size(306, 20);
            this.fileTextBox.TabIndex = 5;
            // 
            // openFileDialog
            // 
            this.openFileDialog.Filter = "Binary file|*.bin|CSV file|*csv";
            // 
            // radioButton2D
            // 
            this.radioButton2D.AutoSize = true;
            this.radioButton2D.Location = new System.Drawing.Point(3, 3);
            this.radioButton2D.Name = "radioButton2D";
            this.radioButton2D.Size = new System.Drawing.Size(39, 17);
            this.radioButton2D.TabIndex = 8;
            this.radioButton2D.TabStop = true;
            this.radioButton2D.Text = "2D";
            this.radioButton2D.UseVisualStyleBackColor = true;
            // 
            // radioButton3D
            // 
            this.radioButton3D.AutoSize = true;
            this.radioButton3D.Location = new System.Drawing.Point(48, 3);
            this.radioButton3D.Name = "radioButton3D";
            this.radioButton3D.Size = new System.Drawing.Size(39, 17);
            this.radioButton3D.TabIndex = 9;
            this.radioButton3D.TabStop = true;
            this.radioButton3D.Text = "3D";
            this.radioButton3D.UseVisualStyleBackColor = true;
            // 
            // radioButtonCSV
            // 
            this.radioButtonCSV.AutoSize = true;
            this.radioButtonCSV.Location = new System.Drawing.Point(55, 3);
            this.radioButtonCSV.Name = "radioButtonCSV";
            this.radioButtonCSV.Size = new System.Drawing.Size(46, 17);
            this.radioButtonCSV.TabIndex = 10;
            this.radioButtonCSV.TabStop = true;
            this.radioButtonCSV.Text = "CSV";
            this.radioButtonCSV.UseVisualStyleBackColor = true;
            // 
            // radioButtonBIN
            // 
            this.radioButtonBIN.AutoSize = true;
            this.radioButtonBIN.Location = new System.Drawing.Point(3, 3);
            this.radioButtonBIN.Name = "radioButtonBIN";
            this.radioButtonBIN.Size = new System.Drawing.Size(43, 17);
            this.radioButtonBIN.TabIndex = 11;
            this.radioButtonBIN.TabStop = true;
            this.radioButtonBIN.Text = "BIN";
            this.radioButtonBIN.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.radioButtonBIN);
            this.panel1.Controls.Add(this.radioButtonCSV);
            this.panel1.Location = new System.Drawing.Point(131, 17);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(103, 21);
            this.panel1.TabIndex = 12;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.radioButton2D);
            this.panel2.Controls.Add(this.radioButton3D);
            this.panel2.Location = new System.Drawing.Point(6, 17);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(90, 21);
            this.panel2.TabIndex = 13;
            // 
            // LoadScenarioFileUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label);
            this.Controls.Add(this.showFileDialog);
            this.Controls.Add(this.fileTextBox);
            this.Name = "LoadScenarioFileUserControl";
            this.Size = new System.Drawing.Size(367, 71);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label;
        private System.Windows.Forms.Button showFileDialog;
        protected System.Windows.Forms.TextBox fileTextBox;
        protected System.Windows.Forms.OpenFileDialog openFileDialog;
        protected System.Windows.Forms.RadioButton radioButton2D;
        protected System.Windows.Forms.RadioButton radioButton3D;
        protected System.Windows.Forms.RadioButton radioButtonCSV;
        protected System.Windows.Forms.RadioButton radioButtonBIN;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
    }
}
