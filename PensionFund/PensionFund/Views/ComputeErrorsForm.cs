﻿using System.Collections.Generic;
using System.Windows.Forms;

namespace PensionFund.Views
{
    public partial class ComputeErrorsForm : Form
    {
        protected ComputeErrorsForm()
        {
            this.InitializeComponent();
            errorGrid.AutoGenerateColumns = false;
        }

        public static DialogResult Display(Form parent, IList<ComputeException> exceptions)
        {
            ComputeErrorsForm form = new ComputeErrorsForm();
            form.errorGrid.DataSource = exceptions;
            return form.ShowDialog(parent);
        }

        private void errorGrid_SelectionChanged(object sender, System.EventArgs e)
        {
            if (this.errorGrid.CurrentRow != null)
            {
                ComputeException computeException = this.errorGrid.CurrentRow.DataBoundItem as ComputeException;
                if (computeException == null)
                {
                    formulaEditorTextBox.Text = null;
                    formulaEditorTextBox.Enabled = false;
                    return;
                }

                if (computeException.ModelItem != null)
                {
                    formulaEditorTextBox.Text = computeException.ModelItem.Formula;
                    formulaEditorTextBox.Enabled = true;
                }
            }
        }

        private void retryButton_Click(object sender, System.EventArgs e)
        {
            if (this.errorGrid.CurrentRow != null)
            {
                ComputeException computeException = this.errorGrid.CurrentRow.DataBoundItem as ComputeException;
                if (computeException == null || computeException.ModelItem == null)
                {
                    return;
                }

                computeException.ModelItem.Formula = formulaEditorTextBox.Text;
            }
        }
    }
}