﻿namespace PensionFund.Views
{
    partial class ScenariosLoadDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cancelButton = new System.Windows.Forms.Button();
            this.loadButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.maxYearsNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.maxScenariosNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.actualReturnOnAssetsLoad = new PensionFund.Views.LoadWeightedScenarioFilesUserControl();
            this.annualAdjustmentsToPensionsLoad = new PensionFund.Views.LoadScenarioFileUserControl();
            this.discountRateLoad = new PensionFund.Views.LoadScenarioFileUserControl();
            ((System.ComponentModel.ISupportInitialize)(this.maxYearsNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.maxScenariosNumericUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // cancelButton
            // 
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point(311, 434);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 1;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            // 
            // loadButton
            // 
            this.loadButton.Location = new System.Drawing.Point(230, 434);
            this.loadButton.Name = "loadButton";
            this.loadButton.Size = new System.Drawing.Size(75, 23);
            this.loadButton.TabIndex = 0;
            this.loadButton.Text = "Load";
            this.loadButton.UseVisualStyleBackColor = true;
            this.loadButton.Click += new System.EventHandler(this.loadButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 360);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(123, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Maximum years to load : ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 395);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(140, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Maximum scenarios to load :";
            // 
            // maxYearsNumericUpDown
            // 
            this.maxYearsNumericUpDown.Location = new System.Drawing.Point(266, 358);
            this.maxYearsNumericUpDown.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.maxYearsNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.maxYearsNumericUpDown.Name = "maxYearsNumericUpDown";
            this.maxYearsNumericUpDown.Size = new System.Drawing.Size(120, 20);
            this.maxYearsNumericUpDown.TabIndex = 7;
            this.maxYearsNumericUpDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.maxYearsNumericUpDown.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // maxScenariosNumericUpDown
            // 
            this.maxScenariosNumericUpDown.Location = new System.Drawing.Point(266, 393);
            this.maxScenariosNumericUpDown.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.maxScenariosNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.maxScenariosNumericUpDown.Name = "maxScenariosNumericUpDown";
            this.maxScenariosNumericUpDown.Size = new System.Drawing.Size(120, 20);
            this.maxScenariosNumericUpDown.TabIndex = 8;
            this.maxScenariosNumericUpDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.maxScenariosNumericUpDown.Value = new decimal(new int[] {
            50000,
            0,
            0,
            0});
            // 
            // actualReturnOnAssetsLoad
            // 
            this.actualReturnOnAssetsLoad.Location = new System.Drawing.Point(23, 173);
            this.actualReturnOnAssetsLoad.Name = "actualReturnOnAssetsLoad";
            this.actualReturnOnAssetsLoad.Size = new System.Drawing.Size(363, 180);
            this.actualReturnOnAssetsLoad.TabIndex = 4;
            // 
            // annualAdjustmentsToPensionsLoad
            // 
            this.annualAdjustmentsToPensionsLoad.Location = new System.Drawing.Point(23, 96);
            this.annualAdjustmentsToPensionsLoad.Name = "annualAdjustmentsToPensionsLoad";
            this.annualAdjustmentsToPensionsLoad.Size = new System.Drawing.Size(363, 71);
            this.annualAdjustmentsToPensionsLoad.TabIndex = 3;
            // 
            // discountRateLoad
            // 
            this.discountRateLoad.Location = new System.Drawing.Point(23, 20);
            this.discountRateLoad.Name = "discountRateLoad";
            this.discountRateLoad.Size = new System.Drawing.Size(363, 71);
            this.discountRateLoad.TabIndex = 2;
            // 
            // ScenariosLoadDialog
            // 
            this.AcceptButton = this.loadButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(410, 466);
            this.Controls.Add(this.maxScenariosNumericUpDown);
            this.Controls.Add(this.maxYearsNumericUpDown);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.actualReturnOnAssetsLoad);
            this.Controls.Add(this.annualAdjustmentsToPensionsLoad);
            this.Controls.Add(this.discountRateLoad);
            this.Controls.Add(this.loadButton);
            this.Controls.Add(this.cancelButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "ScenariosLoadDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Load scenarios";
            ((System.ComponentModel.ISupportInitialize)(this.maxYearsNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.maxScenariosNumericUpDown)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button loadButton;
        private LoadScenarioFileUserControl discountRateLoad;
        private LoadScenarioFileUserControl annualAdjustmentsToPensionsLoad;
        private LoadWeightedScenarioFilesUserControl actualReturnOnAssetsLoad;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown maxYearsNumericUpDown;
        private System.Windows.Forms.NumericUpDown maxScenariosNumericUpDown;

    }
}