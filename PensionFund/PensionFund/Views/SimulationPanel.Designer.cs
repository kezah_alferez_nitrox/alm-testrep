using System.Windows.Forms;

namespace PensionFund.Views
{
    partial class SimulationPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            this.simulationPanel1 = new System.Windows.Forms.Panel();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.zedGraphControl = new ZedGraph.ZedGraphControl();
            this.percentileGrid = new System.Windows.Forms.DataGridView();
            this.panel5 = new System.Windows.Forms.Panel();
            this.bClose = new System.Windows.Forms.Button();
            this.titleLabel = new System.Windows.Forms.Label();
            this.labelColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.yearColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.year1Column = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.year2Column = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.year3Column = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.year4Column = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.year5Column = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.simulationPanel1.SuspendLayout();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.percentileGrid)).BeginInit();
            this.panel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // simulationPanel1
            // 
            this.simulationPanel1.Controls.Add(this.splitContainer2);
            this.simulationPanel1.Controls.Add(this.panel5);
            this.simulationPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.simulationPanel1.Location = new System.Drawing.Point(1, 1);
            this.simulationPanel1.Name = "simulationPanel1";
            this.simulationPanel1.Size = new System.Drawing.Size(883, 271);
            this.simulationPanel1.TabIndex = 1;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 30);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.zedGraphControl);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.percentileGrid);
            this.splitContainer2.Size = new System.Drawing.Size(883, 241);
            this.splitContainer2.SplitterDistance = 368;
            this.splitContainer2.TabIndex = 4;
            // 
            // zedGraphControl
            // 
            this.zedGraphControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.zedGraphControl.Location = new System.Drawing.Point(0, 0);
            this.zedGraphControl.Name = "zedGraphControl";
            this.zedGraphControl.ScrollGrace = 0;
            this.zedGraphControl.ScrollMaxX = 0;
            this.zedGraphControl.ScrollMaxY = 0;
            this.zedGraphControl.ScrollMaxY2 = 0;
            this.zedGraphControl.ScrollMinX = 0;
            this.zedGraphControl.ScrollMinY = 0;
            this.zedGraphControl.ScrollMinY2 = 0;
            this.zedGraphControl.Size = new System.Drawing.Size(368, 241);
            this.zedGraphControl.TabIndex = 0;
            // 
            // percentileGrid
            // 
            this.percentileGrid.AllowUserToAddRows = false;
            this.percentileGrid.AllowUserToDeleteRows = false;
            this.percentileGrid.BackgroundColor = System.Drawing.SystemColors.Control;
            this.percentileGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.percentileGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.labelColumn,
            this.yearColumn,
            this.year1Column,
            this.year2Column,
            this.year3Column,
            this.year4Column,
            this.year5Column});
            this.percentileGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.percentileGrid.Location = new System.Drawing.Point(0, 0);
            this.percentileGrid.Name = "percentileGrid";
            this.percentileGrid.ReadOnly = true;
            this.percentileGrid.Size = new System.Drawing.Size(511, 241);
            this.percentileGrid.TabIndex = 4;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.SystemColors.ControlDark;
            this.panel5.Controls.Add(this.bClose);
            this.panel5.Controls.Add(this.titleLabel);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(883, 30);
            this.panel5.TabIndex = 3;
            // 
            // bClose
            // 
            this.bClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bClose.BackColor = System.Drawing.SystemColors.Control;
            this.bClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bClose.ForeColor = System.Drawing.SystemColors.ControlText;
            this.bClose.Location = new System.Drawing.Point(857, 3);
            this.bClose.Name = "bClose";
            this.bClose.Size = new System.Drawing.Size(23, 23);
            this.bClose.TabIndex = 2;
            this.bClose.Text = "X";
            this.bClose.UseVisualStyleBackColor = false;
            this.bClose.Click += new System.EventHandler(this.bClose_Click);
            // 
            // titleLabel
            // 
            this.titleLabel.AutoSize = true;
            this.titleLabel.BackColor = System.Drawing.Color.Transparent;
            this.titleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold);
            this.titleLabel.ForeColor = System.Drawing.Color.White;
            this.titleLabel.Location = new System.Drawing.Point(6, 4);
            this.titleLabel.Name = "titleLabel";
            this.titleLabel.Size = new System.Drawing.Size(55, 24);
            this.titleLabel.TabIndex = 1;
            this.titleLabel.Text = "EBIT";
            // 
            // SimulationPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.simulationPanel1);
            this.Name = "SimulationPanel";
            this.Padding = new System.Windows.Forms.Padding(1);
            this.Size = new System.Drawing.Size(885, 273);
            this.simulationPanel1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.percentileGrid)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel simulationPanel1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label titleLabel;
        private System.Windows.Forms.DataGridView percentileGrid;
        private System.Windows.Forms.Button bClose;
        private ZedGraph.ZedGraphControl zedGraphControl;
        private DataGridViewTextBoxColumn labelColumn;
        private DataGridViewTextBoxColumn yearColumn;
        private DataGridViewTextBoxColumn year1Column;
        private DataGridViewTextBoxColumn year2Column;
        private DataGridViewTextBoxColumn year3Column;
        private DataGridViewTextBoxColumn year4Column;
        private DataGridViewTextBoxColumn year5Column;
    }
}