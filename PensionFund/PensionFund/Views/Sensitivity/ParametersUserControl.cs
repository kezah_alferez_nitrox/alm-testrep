﻿using System;
using System.Windows.Forms;

namespace PensionFund.Views.Sensitivity
{
    public partial class ParametersUserControl : UserControl
    {
        public ParametersUserControl()
        {
            this.InitializeComponent();

            this.SetupGrid();
        }

        private void SetupGrid()
        {
            DataGridViewColumn column = Util.AddColumnToGrid(this.grid, "Parameter");
            column.Width = 200;

            column = Util.AddColumnToGrid(this.grid, "Include", () => new DataGridViewCheckBoxColumn());
            column.Width = 50;

            column = Util.AddColumnToGrid(this.grid, "Value");
            column.Width = 300;
        }

        private void ParametersUserControl_Load(object sender, EventArgs e)
        {
            FillGrid();
        }

        private void FillGrid()
        {
            object[][] data = Util.LoadCsvData(@"Demo\parameters.csv", s => (object)s);

            Util.FillGrid(grid, data);
        }
    }
}