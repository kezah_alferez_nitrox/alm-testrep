﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Windows.Forms;
using ZedGraph;

namespace PensionFund.Views.Sensitivity
{
    public partial class LiabilitiesUserControl : UserControl
    {
        public LiabilitiesUserControl()
        {
            this.InitializeComponent();

            this.SetupGrid();

            this.SetupGraphs();
        }

        private void SetupGrid()
        {
            grid.AutoGenerateColumns = false;
            grid.AllowUserToAddRows = false;

            Util.AddColumnToGrid(grid, "Year");
            Util.AddColumnToGrid(grid, "Active Members INDEXED");
            Util.AddColumnToGrid(grid, "Deferred Members INDEXED");
            Util.AddColumnToGrid(grid, "Pensioners INDEXED");

            Util.AddColumnToGrid(grid, "Active Members FIXED");
            Util.AddColumnToGrid(grid, "Deferred Members FIXED");
            Util.AddColumnToGrid(grid, "Pensioners FIXED");

            Util.AddColumnToGrid(grid, "Real Cashflow");
            Util.AddColumnToGrid(grid, "Indexed Cashflow");
            Util.AddColumnToGrid(grid, "Uplift Cashflow");
            Util.AddColumnToGrid(grid, "Fixed Cashflow");

            Util.AddColumnToGrid(grid, "Year");
        }

        private void SetupGraphs()
        {
            this.zedGraphControl1.GraphPane.Title.IsVisible = false;
            this.zedGraphControl2.GraphPane.Title.IsVisible = false;

            this.zedGraphControl1.GraphPane.BarSettings.Type = BarType.Stack;
            this.zedGraphControl1.GraphPane.BarSettings.MinClusterGap = 0.3f;

            this.zedGraphControl1.GraphPane.YAxis.Title.Text = "Annual Liability Cash Flow (in EURm)";
            this.zedGraphControl2.GraphPane.YAxis.Title.Text = "Annual Liability Cash Flow (in EURm)";
        }

        private void LiabilitiesUserControl_Load(object sender, EventArgs e)
        {
            if (this.DesignMode || (LicenseManager.UsageMode == LicenseUsageMode.Designtime)) return;

            double[][] data = Util.LoadCsvData(@"Demo\liabilities.csv", s => Util.ConvertToDouble(s));

            Util.FillGrid(grid, data);

            this.FillGraph(data);
        }

        private void FillGraph(double[][] data)
        {
            FillGraph1(this.zedGraphControl1, data);
            FillGraph2(this.zedGraphControl2, data);
        }

        private static void FillGraph2(ZedGraphControl zgc, double[][] data)
        {
            GraphPane zedPane = zgc.GraphPane;
            zedPane.XAxis.Title.IsVisible = false;

            var xValues = Util.GetColumnArray(data, 0);
            Util.AddLine(data, zgc, xValues, 4, Color.Orange);
            Util.AddLine(data, zgc, xValues, 1, Color.Red);
            Util.AddLine(data, zgc, xValues, 5, Color.Blue);
            Util.AddLine(data, zgc, xValues, 2, Color.DarkOrange);
            Util.AddLine(data, zgc, xValues, 6, Color.DarkRed);
            Util.AddLine(data, zgc, xValues, 3, Color.DarkBlue);

            zgc.AxisChange();
        }

        private static void FillGraph1(ZedGraphControl zgc, double[][] data)
        {
            GraphPane zedPane = zgc.GraphPane;
            zedPane.XAxis.Title.IsVisible = false;

            var xValues = Util.GetColumnArray(data, 0);
            Util.AddBar(data, zgc, xValues, 10, "Fixed Cash Flow", Color.Brown);
            Util.AddBar(data, zgc, xValues, 7, "Nominal Cash Flows", Color.IndianRed);
            Util.AddBar(data, zgc, xValues, 9, "Indexation Uplift", Color.Olive);

            zgc.AxisChange();
        }
    }
}