﻿using System;
using System.Windows.Forms;

namespace PensionFund.Views.Sensitivity
{
    public partial class SensitivityForm : Form
    {
        public SensitivityForm()
        {
            this.InitializeComponent();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}