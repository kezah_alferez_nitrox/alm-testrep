﻿namespace PensionFund.Views.Sensitivity
{
    partial class SensitivityForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SensitivityForm));
            this.mainTabControl = new System.Windows.Forms.TabControl();
            this.liabilitiesTabPage = new System.Windows.Forms.TabPage();
            this.liabilitiesUserControl = new PensionFund.Views.Sensitivity.LiabilitiesUserControl();
            this.assetsTabPage = new System.Windows.Forms.TabPage();
            this.assetsUserControl = new PensionFund.Views.Sensitivity.AssetsUserControl();
            this.parametrsTabPage = new System.Windows.Forms.TabPage();
            this.parametersUserControl = new PensionFund.Views.Sensitivity.ParametersUserControl();
            this.overviewTabPage = new System.Windows.Forms.TabPage();
            this.overviewUserControl = new PensionFund.Views.Sensitivity.OverviewUserControl();
            this.mainMenuStrip = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mainToolStrip = new System.Windows.Forms.ToolStrip();
            this.newToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.openToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.saveToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.copyToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.pasteToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.vectorManagerToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.panel1 = new System.Windows.Forms.Panel();
            this.mainTabControl.SuspendLayout();
            this.liabilitiesTabPage.SuspendLayout();
            this.assetsTabPage.SuspendLayout();
            this.parametrsTabPage.SuspendLayout();
            this.overviewTabPage.SuspendLayout();
            this.mainMenuStrip.SuspendLayout();
            this.mainToolStrip.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainTabControl
            // 
            this.mainTabControl.Controls.Add(this.liabilitiesTabPage);
            this.mainTabControl.Controls.Add(this.assetsTabPage);
            this.mainTabControl.Controls.Add(this.parametrsTabPage);
            this.mainTabControl.Controls.Add(this.overviewTabPage);
            this.mainTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainTabControl.Location = new System.Drawing.Point(0, 0);
            this.mainTabControl.Name = "mainTabControl";
            this.mainTabControl.SelectedIndex = 0;
            this.mainTabControl.Size = new System.Drawing.Size(1060, 515);
            this.mainTabControl.TabIndex = 0;
            // 
            // liabilitiesTabPage
            // 
            this.liabilitiesTabPage.Controls.Add(this.liabilitiesUserControl);
            this.liabilitiesTabPage.Location = new System.Drawing.Point(4, 22);
            this.liabilitiesTabPage.Name = "liabilitiesTabPage";
            this.liabilitiesTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.liabilitiesTabPage.Size = new System.Drawing.Size(1052, 489);
            this.liabilitiesTabPage.TabIndex = 0;
            this.liabilitiesTabPage.Text = "Projections Liabilities";
            this.liabilitiesTabPage.UseVisualStyleBackColor = true;
            // 
            // liabilitiesUserControl
            // 
            this.liabilitiesUserControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.liabilitiesUserControl.Location = new System.Drawing.Point(3, 3);
            this.liabilitiesUserControl.Name = "liabilitiesUserControl";
            this.liabilitiesUserControl.Size = new System.Drawing.Size(1046, 483);
            this.liabilitiesUserControl.TabIndex = 0;
            // 
            // assetsTabPage
            // 
            this.assetsTabPage.Controls.Add(this.assetsUserControl);
            this.assetsTabPage.Location = new System.Drawing.Point(4, 22);
            this.assetsTabPage.Name = "assetsTabPage";
            this.assetsTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.assetsTabPage.Size = new System.Drawing.Size(1052, 538);
            this.assetsTabPage.TabIndex = 1;
            this.assetsTabPage.Text = "Projections Assets";
            this.assetsTabPage.UseVisualStyleBackColor = true;
            // 
            // assetsUserControl
            // 
            this.assetsUserControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.assetsUserControl.Location = new System.Drawing.Point(3, 3);
            this.assetsUserControl.Name = "assetsUserControl";
            this.assetsUserControl.Size = new System.Drawing.Size(1046, 532);
            this.assetsUserControl.TabIndex = 0;
            // 
            // parametrsTabPage
            // 
            this.parametrsTabPage.Controls.Add(this.parametersUserControl);
            this.parametrsTabPage.Location = new System.Drawing.Point(4, 22);
            this.parametrsTabPage.Name = "parametrsTabPage";
            this.parametrsTabPage.Size = new System.Drawing.Size(1052, 538);
            this.parametrsTabPage.TabIndex = 2;
            this.parametrsTabPage.Text = "Parametrs";
            this.parametrsTabPage.UseVisualStyleBackColor = true;
            // 
            // parametersUserControl
            // 
            this.parametersUserControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.parametersUserControl.Location = new System.Drawing.Point(0, 0);
            this.parametersUserControl.Name = "parametersUserControl";
            this.parametersUserControl.Size = new System.Drawing.Size(1052, 538);
            this.parametersUserControl.TabIndex = 0;
            // 
            // overviewTabPage
            // 
            this.overviewTabPage.Controls.Add(this.overviewUserControl);
            this.overviewTabPage.Location = new System.Drawing.Point(4, 22);
            this.overviewTabPage.Name = "overviewTabPage";
            this.overviewTabPage.Size = new System.Drawing.Size(1052, 538);
            this.overviewTabPage.TabIndex = 3;
            this.overviewTabPage.Text = "Overview";
            this.overviewTabPage.UseVisualStyleBackColor = true;
            // 
            // overviewUserControl
            // 
            this.overviewUserControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.overviewUserControl.Location = new System.Drawing.Point(0, 0);
            this.overviewUserControl.Name = "overviewUserControl";
            this.overviewUserControl.Size = new System.Drawing.Size(1052, 538);
            this.overviewUserControl.TabIndex = 1;
            // 
            // mainMenuStrip
            // 
            this.mainMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem,
            this.toolsToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.mainMenuStrip.Location = new System.Drawing.Point(0, 0);
            this.mainMenuStrip.Name = "mainMenuStrip";
            this.mainMenuStrip.Size = new System.Drawing.Size(1060, 24);
            this.mainMenuStrip.TabIndex = 2;
            this.mainMenuStrip.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.exitToolStripMenuItem.Text = "E&xit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.editToolStripMenuItem.Text = "&Edit";
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.toolsToolStripMenuItem.Text = "&Tools";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "&Help";
            // 
            // mainToolStrip
            // 
            this.mainToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripButton,
            this.openToolStripButton,
            this.saveToolStripButton,
            this.toolStripSeparator1,
            this.toolStripButton4,
            this.copyToolStripButton,
            this.pasteToolStripButton,
            this.toolStripSeparator2,
            this.vectorManagerToolStripButton,
            this.toolStripSeparator3});
            this.mainToolStrip.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.mainToolStrip.Location = new System.Drawing.Point(0, 24);
            this.mainToolStrip.Name = "mainToolStrip";
            this.mainToolStrip.Padding = new System.Windows.Forms.Padding(0, 2, 1, 0);
            this.mainToolStrip.Size = new System.Drawing.Size(1060, 25);
            this.mainToolStrip.TabIndex = 6;
            this.mainToolStrip.Text = "toolStrip1";
            // 
            // newToolStripButton
            // 
            this.newToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.newToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("newToolStripButton.Image")));
            this.newToolStripButton.ImageTransparentColor = System.Drawing.Color.Black;
            this.newToolStripButton.Name = "newToolStripButton";
            this.newToolStripButton.Size = new System.Drawing.Size(23, 20);
            this.newToolStripButton.Text = "New";
            // 
            // openToolStripButton
            // 
            this.openToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.openToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("openToolStripButton.Image")));
            this.openToolStripButton.ImageTransparentColor = System.Drawing.Color.Black;
            this.openToolStripButton.Name = "openToolStripButton";
            this.openToolStripButton.Size = new System.Drawing.Size(23, 20);
            this.openToolStripButton.Text = "Open";
            // 
            // saveToolStripButton
            // 
            this.saveToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.saveToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("saveToolStripButton.Image")));
            this.saveToolStripButton.ImageTransparentColor = System.Drawing.Color.Black;
            this.saveToolStripButton.Name = "saveToolStripButton";
            this.saveToolStripButton.Size = new System.Drawing.Size(23, 20);
            this.saveToolStripButton.Text = "Save";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 23);
            // 
            // toolStripButton4
            // 
            this.toolStripButton4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton4.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton4.Image")));
            this.toolStripButton4.ImageTransparentColor = System.Drawing.Color.Black;
            this.toolStripButton4.Name = "toolStripButton4";
            this.toolStripButton4.Size = new System.Drawing.Size(23, 20);
            this.toolStripButton4.Text = "Cut";
            // 
            // copyToolStripButton
            // 
            this.copyToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.copyToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("copyToolStripButton.Image")));
            this.copyToolStripButton.ImageTransparentColor = System.Drawing.Color.Black;
            this.copyToolStripButton.Name = "copyToolStripButton";
            this.copyToolStripButton.Size = new System.Drawing.Size(23, 20);
            this.copyToolStripButton.Text = "Copy";
            // 
            // pasteToolStripButton
            // 
            this.pasteToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.pasteToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("pasteToolStripButton.Image")));
            this.pasteToolStripButton.ImageTransparentColor = System.Drawing.Color.Black;
            this.pasteToolStripButton.Name = "pasteToolStripButton";
            this.pasteToolStripButton.Size = new System.Drawing.Size(23, 20);
            this.pasteToolStripButton.Text = "Paste";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 23);
            // 
            // vectorManagerToolStripButton
            // 
            this.vectorManagerToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.vectorManagerToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("vectorManagerToolStripButton.Image")));
            this.vectorManagerToolStripButton.ImageTransparentColor = System.Drawing.Color.Black;
            this.vectorManagerToolStripButton.Name = "vectorManagerToolStripButton";
            this.vectorManagerToolStripButton.Size = new System.Drawing.Size(23, 20);
            this.vectorManagerToolStripButton.Text = "Vector Manager";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 23);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.mainTabControl);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 49);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1060, 515);
            this.panel1.TabIndex = 7;
            // 
            // SensitivityForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1060, 564);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.mainToolStrip);
            this.Controls.Add(this.mainMenuStrip);
            this.Name = "SensitivityForm";
            this.Text = "Sensitivity";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.mainTabControl.ResumeLayout(false);
            this.liabilitiesTabPage.ResumeLayout(false);
            this.assetsTabPage.ResumeLayout(false);
            this.parametrsTabPage.ResumeLayout(false);
            this.overviewTabPage.ResumeLayout(false);
            this.mainMenuStrip.ResumeLayout(false);
            this.mainMenuStrip.PerformLayout();
            this.mainToolStrip.ResumeLayout(false);
            this.mainToolStrip.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl mainTabControl;
        private System.Windows.Forms.TabPage liabilitiesTabPage;
        private System.Windows.Forms.TabPage assetsTabPage;
        private System.Windows.Forms.TabPage parametrsTabPage;
        private System.Windows.Forms.TabPage overviewTabPage;
        private AssetsUserControl assetsUserControl;
        private ParametersUserControl parametersUserControl;
        private OverviewUserControl overviewUserControl;
        private LiabilitiesUserControl liabilitiesUserControl;
        private System.Windows.Forms.MenuStrip mainMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStrip mainToolStrip;
        private System.Windows.Forms.ToolStripButton newToolStripButton;
        private System.Windows.Forms.ToolStripButton openToolStripButton;
        private System.Windows.Forms.ToolStripButton saveToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton toolStripButton4;
        private System.Windows.Forms.ToolStripButton copyToolStripButton;
        private System.Windows.Forms.ToolStripButton pasteToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton vectorManagerToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.Panel panel1;
    }
}