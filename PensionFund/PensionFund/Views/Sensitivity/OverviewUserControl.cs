﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using ZedGraph;

namespace PensionFund.Views.Sensitivity
{
    public partial class OverviewUserControl : UserControl
    {
        protected static readonly string[] GridNames = new[]
        {
            "CURRENT INTEREST RATE RISK - LIABILITIES",
            "CURRENT INFLATION RISK - LIABILITIES",
            "CURRENT RISK - ASSETS",
            "CURRENT SWAP PORTFOLIO SENSITIVITIES",
            "SENSITIVITIES - REQUIRED SWAPS",
            "NOTIONALS - REQUIRED SWAPS",	
            "RESULTING TOTAL PORTFOLIO SENSITIVITY",                                                
        };

        object[][][] datas = new object[7][][];

        public OverviewUserControl()
        {
            InitializeComponent();

            this.SetupGrid();
            this.SetupGraphs();
        }

        private void SetupGrid()
        {
            Util.AddColumnToGrid(grid, "");
            Util.AddColumnToGrid(grid, "NPV");
            Util.AddColumnToGrid(grid, "Duration");

            object[][] data = Util.LoadCsvData(@"Demo\overview.csv", s => (object)s);
            Util.FillGrid(grid, data);
        }

        private void SetupGraphs()
        {
            this.zedGraphControl1.GraphPane.Title.IsVisible = false;
            this.zedGraphControl1.GraphPane.YAxis.Title.IsVisible = false;
            this.zedGraphControl1.GraphPane.BarSettings.Type = BarType.Stack;

            this.zedGraphControl2.GraphPane.Title.IsVisible = false;
            this.zedGraphControl2.GraphPane.YAxis.Title.IsVisible = false;
            this.zedGraphControl2.GraphPane.BarSettings.Base = BarBase.Y;
            this.zedGraphControl2.GraphPane.BarSettings.MinClusterGap = 0.3f;

            this.zedGraphControl3.GraphPane.Title.IsVisible = false;
            this.zedGraphControl3.GraphPane.YAxis.Title.IsVisible = false;
            this.zedGraphControl3.GraphPane.BarSettings.Base = BarBase.Y;
            this.zedGraphControl3.GraphPane.BarSettings.MinClusterGap = 0.3f;

            this.zedGraphControl4.GraphPane.Title.IsVisible = false;
            this.zedGraphControl4.GraphPane.YAxis.Title.IsVisible = false;
            this.zedGraphControl4.GraphPane.BarSettings.Base = BarBase.Y;
            this.zedGraphControl4.GraphPane.BarSettings.MinClusterGap = 0.3f;

            this.zedGraphControl5.GraphPane.Title.IsVisible = false;
            this.zedGraphControl5.GraphPane.YAxis.Title.IsVisible = false;
            this.zedGraphControl5.GraphPane.BarSettings.Base = BarBase.Y;
            this.zedGraphControl5.GraphPane.BarSettings.MinClusterGap = 0.3f;
        }

        private void FillGridPannel()
        {
            for (int i = GridNames.Length - 1; i >= 0; i--)
            {
                string name = GridNames[i];

                datas[i] = Util.LoadCsvData(@"Demo\overview" + (i + 1) + ".csv", s => (object)s);

                OverviewGridUserControl gridUserControl = new OverviewGridUserControl();
                gridUserControl.LoadData(name, datas[i]);
                gridUserControl.Dock = DockStyle.Top;
                this.gridPanel.Controls.Add(gridUserControl);
            }
        }

        private void OverviewUserControl_Load(object sender, EventArgs e)
        {
            this.FillGridPannel();
            this.FillGraphs();
        }

        private void FillGraphs()
        {
            double[][] pureEffect = Util.LoadCsvData(@"Demo\pureeffect.csv", s => Util.ConvertToDouble(s));
            FillGraph1(zedGraphControl1, pureEffect);

            double[][] graph2Data = Util.LoadCsvData(@"Demo\overviewgraph1.csv", s => Util.ConvertToDouble(s));
            FillGraph2(zedGraphControl2, graph2Data);

            FillGraph3(zedGraphControl3, graph2Data);

            FillGraph4(zedGraphControl4, graph2Data);

            FillGraph5(zedGraphControl5, graph2Data);
        }

        private void FillGraph1(ZedGraphControl zgc, double[][] data)
        {
            GraphPane zedPane = zgc.GraphPane;
            zedPane.XAxis.Title.IsVisible = false;

            var xValues = Util.GetColumnArray(data, 0);
            Util.AddBar(data, zgc, xValues, 1, "Real CF", Color.Salmon);
            Util.AddBar(data, zgc, xValues, 3, "Inflation Impact", Color.Gray);
            Util.AddBar(data, zgc, xValues, 2, "Salary Impact", Color.Black);

            zgc.AxisChange();
        }

        private static void FillGraph2(ZedGraphControl zgc, double[][] data)
        {
            GraphPane zedPane = zgc.GraphPane;
            zedPane.XAxis.Title.IsVisible = false;

            var xValues = Util.GetColumnArray(data, 0);
            Util.AddHorizontalBar(data, zgc, xValues, 1, "Liabilities - Interest Rate Sensitivity", Color.OrangeRed);
            Util.AddHorizontalBar(data, zgc, xValues, 2, "Liabilities - Inflation Sensitivity", Color.Firebrick);

            zgc.AxisChange();
        }

        private void FillGraph3(ZedGraphControl zgc, double[][] data1)
        {
            GraphPane zedPane = zgc.GraphPane;
            zedPane.XAxis.Title.IsVisible = false;

            var xValues = Util.GetColumnArray(data1, 0);
            Util.AddHorizontalBar(datas[2], zgc, xValues, 1, "Assets - Inflation Sensitivity", Color.DarkGoldenrod);
            Util.AddHorizontalBar(datas[2], zgc, xValues, 3, "Assets - Intereset Rate Sensitivity", Color.DarkGreen);
            Util.AddHorizontalBar(datas[3], zgc, xValues, 1, "Current Swap - Inflation Sensitivity", Color.Violet);
            Util.AddHorizontalBar(datas[3], zgc, xValues, 3, "Current Swap - IR Sensitivity", Color.DarkViolet);

            zgc.AxisChange();
        }

        private void FillGraph4(ZedGraphControl zgc, double[][] data1)
        {
            GraphPane zedPane = zgc.GraphPane;
            zedPane.XAxis.Title.IsVisible = false;

            var xValues = Util.GetColumnArray(data1, 0);
            Util.AddHorizontalBar(datas[4], zgc, xValues, 3, "Required Inflation Swap PV01", Color.LightGreen);
            Util.AddHorizontalBar(datas[4], zgc, xValues, 1, "Required IR Swap PV01", Color.DarkGreen);

            zgc.AxisChange();
        }

        private void FillGraph5(ZedGraphControl zgc, double[][] data1)
        {
            GraphPane zedPane = zgc.GraphPane;
            zedPane.XAxis.Title.IsVisible = false;

            var xValues = Util.GetColumnArray(data1, 0);
            Util.AddHorizontalBar(datas[6], zgc, xValues, 3, "Result - Asset Portfolio Inflation Sensitivity", Color.OrangeRed);
            Util.AddHorizontalBar(datas[6], zgc, xValues, 1, "Result - Asset Portfolio IR Sensitivity", Color.Firebrick);

            zgc.AxisChange();
        }
    }
}
