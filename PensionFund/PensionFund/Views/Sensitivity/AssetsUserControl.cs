﻿using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using ZedGraph;

namespace PensionFund.Views.Sensitivity
{
    public partial class AssetsUserControl : UserControl
    {
        public AssetsUserControl()
        {
            this.InitializeComponent();

            this.SetupGrid();

            this.SetupGraphs();
        }

        private void SetupGrid()
        {
            if (this.DesignMode || LicenseManager.UsageMode == LicenseUsageMode.Designtime) return;

            Util.AddColumnToGrid(this.grid,"Year");
            Util.AddColumnToGrid(this.grid,"EUR Sovereign Bonds");
            Util.AddColumnToGrid(this.grid,"Inflation-Linked Bonds EUR");
            Util.AddColumnToGrid(this.grid,"Interest Rate Swaps");
            Util.AddColumnToGrid(this.grid,"Inflation Swaps - Fixed Leg");
            Util.AddColumnToGrid(this.grid,"Inflation Swaps - Inflation Leg");
            Util.AddColumnToGrid(this.grid,"Total");
            Util.AddColumnToGrid(this.grid,"Year");

            foreach (DataGridViewColumn column in grid.Columns)
            {
                column.Width = 120;
            }
        }

        private void SetupGraphs()
        {
            this.zedGraphControl1.GraphPane.Title.IsVisible = false;
            this.zedGraphControl1.GraphPane.YAxis.Title.Text = "Annual Asset Cash Flow - Sovereigns (in EURm)";
            this.zedGraphControl1.GraphPane.BarSettings.MinClusterGap = 0.3f;

            this.zedGraphControl2.GraphPane.YAxis.Title.Text = "Annual Asset Cash Flow - Eurozone IL (in EURm)";
            this.zedGraphControl2.GraphPane.Title.IsVisible = false;
            this.zedGraphControl2.GraphPane.BarSettings.MinClusterGap = 0.3f;

            this.zedGraphControl3.GraphPane.YAxis.Title.Text = "Annual Swap Cash Flow";
            this.zedGraphControl3.GraphPane.Title.IsVisible = false;
            this.zedGraphControl3.GraphPane.BarSettings.MinBarGap = 0.0f;
            this.zedGraphControl3.GraphPane.BarSettings.MinClusterGap = 0.0f;
        }

        private void AssetsUserControl_Load(object sender, System.EventArgs e)
        {
            if (this.DesignMode || (LicenseManager.UsageMode == LicenseUsageMode.Designtime)) return;

            double[][] data = Util.LoadCsvData(@"Demo\assets.csv", s => Util.ConvertToDouble(s));

            Util.FillGrid(grid, data);

            this.FillGraph(data);
        }

        private void FillGraph(double[][] data)
        {
            FillGraph1(zedGraphControl1, data);
            FillGraph2(zedGraphControl2, data);
            FillGraph3(zedGraphControl3, data);
        }

        private static void FillGraph1(ZedGraphControl zgc, double[][] data)
        {
            GraphPane zedPane = zgc.GraphPane;
            zedPane.XAxis.Title.IsVisible = false;

            var xValues = Util.GetColumnArray(data, 0);
            Util.AddBar(data, zgc, xValues, 1, "", Color.DarkSlateBlue);

            zgc.AxisChange();
        }

        private static void FillGraph2(ZedGraphControl zgc, double[][] data)
        {
            GraphPane zedPane = zgc.GraphPane;
            zedPane.XAxis.Title.IsVisible = false;

            var xValues = Util.GetColumnArray(data, 0);
            Util.AddBar(data, zgc, xValues, 2, "", Color.RosyBrown);

            zgc.AxisChange();
        }

        private static void FillGraph3(ZedGraphControl zgc, double[][] data)
        {
            GraphPane zedPane = zgc.GraphPane;
            zedPane.XAxis.Title.IsVisible = false;

            var xValues = Util.GetColumnArray(data, 0);
            Util.AddBar(data, zgc, xValues, 3, "IR Swap - Fixed Leg", Color.Salmon);
            Util.AddBar(data, zgc, xValues, 4, "Inflation Swap - Fixed Leg", Color.Olive);
            Util.AddBar(data, zgc, xValues, 5, "Inflation Swap CF - IL Leg", Color.SlateBlue);

            zgc.AxisChange();
        }
    }
}