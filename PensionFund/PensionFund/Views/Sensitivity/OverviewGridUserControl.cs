﻿using System;
using System.Windows.Forms;

namespace PensionFund.Views.Sensitivity
{
    public partial class OverviewGridUserControl : UserControl
    {
        public OverviewGridUserControl()
        {
            this.InitializeComponent();

            grid.AllowUserToAddRows = false;
            grid.AutoGenerateColumns = false;
        }

        public void LoadData(string title, object[][] data)
        {
            titleLabel.Text = title;

            string[] headers = Util.GetHeaders(data);
            foreach(string header in headers)
            {
                Util.AddColumnToGrid(grid, header);
            }

            object[][] content = Util.GetObjectContent(data);

            Util.FillGrid(grid, content);
        }
    }
}