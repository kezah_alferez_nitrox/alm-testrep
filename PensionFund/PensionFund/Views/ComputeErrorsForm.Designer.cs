﻿namespace PensionFund.Views
{
    partial class ComputeErrorsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bottomPanel = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.formulaEditorTextBox = new System.Windows.Forms.TextBox();
            this.retryButton = new System.Windows.Forms.Button();
            this.closeButton = new System.Windows.Forms.Button();
            this.errorGrid = new System.Windows.Forms.DataGridView();
            this.scenarioColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.modelItemColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.errorMessageColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bottomPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // bottomPanel
            // 
            this.bottomPanel.Controls.Add(this.label1);
            this.bottomPanel.Controls.Add(this.formulaEditorTextBox);
            this.bottomPanel.Controls.Add(this.retryButton);
            this.bottomPanel.Controls.Add(this.closeButton);
            this.bottomPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bottomPanel.Location = new System.Drawing.Point(0, 279);
            this.bottomPanel.Name = "bottomPanel";
            this.bottomPanel.Size = new System.Drawing.Size(922, 69);
            this.bottomPanel.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label1.Location = new System.Drawing.Point(6, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(23, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "f(t)";
            // 
            // formulaEditorTextBox
            // 
            this.formulaEditorTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.formulaEditorTextBox.Location = new System.Drawing.Point(35, 6);
            this.formulaEditorTextBox.Name = "formulaEditorTextBox";
            this.formulaEditorTextBox.Size = new System.Drawing.Size(875, 20);
            this.formulaEditorTextBox.TabIndex = 4;
            // 
            // retryButton
            // 
            this.retryButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.retryButton.DialogResult = System.Windows.Forms.DialogResult.Retry;
            this.retryButton.Location = new System.Drawing.Point(707, 34);
            this.retryButton.Name = "retryButton";
            this.retryButton.Size = new System.Drawing.Size(122, 23);
            this.retryButton.TabIndex = 1;
            this.retryButton.Text = "Save and Retry";
            this.retryButton.UseVisualStyleBackColor = true;
            this.retryButton.Click += new System.EventHandler(this.retryButton_Click);
            // 
            // closeButton
            // 
            this.closeButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.closeButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.closeButton.Location = new System.Drawing.Point(835, 34);
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size(75, 23);
            this.closeButton.TabIndex = 0;
            this.closeButton.Text = "Close";
            this.closeButton.UseVisualStyleBackColor = true;
            // 
            // errorGrid
            // 
            this.errorGrid.AllowUserToAddRows = false;
            this.errorGrid.AllowUserToDeleteRows = false;
            this.errorGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.errorGrid.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.errorGrid.BackgroundColor = System.Drawing.SystemColors.Control;
            this.errorGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.errorGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.scenarioColumn,
            this.modelItemColumn,
            this.errorMessageColumn});
            this.errorGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.errorGrid.Location = new System.Drawing.Point(0, 0);
            this.errorGrid.Name = "errorGrid";
            this.errorGrid.ReadOnly = true;
            this.errorGrid.RowHeadersVisible = false;
            this.errorGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.errorGrid.Size = new System.Drawing.Size(922, 279);
            this.errorGrid.TabIndex = 1;
            this.errorGrid.SelectionChanged += new System.EventHandler(this.errorGrid_SelectionChanged);
            // 
            // scenarioColumn
            // 
            this.scenarioColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.scenarioColumn.DataPropertyName = "Scenario";
            this.scenarioColumn.FillWeight = 10F;
            this.scenarioColumn.HeaderText = "Scenario";
            this.scenarioColumn.MinimumWidth = 65;
            this.scenarioColumn.Name = "scenarioColumn";
            this.scenarioColumn.ReadOnly = true;
            this.scenarioColumn.Width = 65;
            // 
            // modelItemColumn
            // 
            this.modelItemColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.modelItemColumn.DataPropertyName = "ModelItem";
            this.modelItemColumn.HeaderText = "Model Item";
            this.modelItemColumn.Name = "modelItemColumn";
            this.modelItemColumn.ReadOnly = true;
            this.modelItemColumn.Width = 200;
            // 
            // errorMessageColumn
            // 
            this.errorMessageColumn.DataPropertyName = "Message";
            this.errorMessageColumn.FillWeight = 90F;
            this.errorMessageColumn.HeaderText = "Error Message";
            this.errorMessageColumn.Name = "errorMessageColumn";
            this.errorMessageColumn.ReadOnly = true;
            this.errorMessageColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // ComputeErrorsForm
            // 
            this.AcceptButton = this.retryButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.closeButton;
            this.ClientSize = new System.Drawing.Size(922, 348);
            this.Controls.Add(this.errorGrid);
            this.Controls.Add(this.bottomPanel);
            this.Name = "ComputeErrorsForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Compute Errors";
            this.bottomPanel.ResumeLayout(false);
            this.bottomPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorGrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel bottomPanel;
        private System.Windows.Forms.Button closeButton;
        private System.Windows.Forms.DataGridView errorGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn scenarioColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn modelItemColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn errorMessageColumn;
        private System.Windows.Forms.Button retryButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox formulaEditorTextBox;
    }
}