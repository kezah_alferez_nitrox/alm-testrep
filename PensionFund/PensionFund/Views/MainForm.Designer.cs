using System.Windows.Forms;

namespace PensionFund.Views
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.mainTabControl = new System.Windows.Forms.TabControl();
            this.footnoteTabPage = new System.Windows.Forms.TabPage();
            this.footnoteSplitContainer = new System.Windows.Forms.SplitContainer();
            this.panel5 = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.scenarioListBox = new System.Windows.Forms.ListBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.addScenariosButton = new System.Windows.Forms.Button();
            this.footNote = new PensionFund.Views.FootNote();
            this.simulationsTabPage = new System.Windows.Forms.TabPage();
            this.simulations = new PensionFund.Views.Simulations();
            this.mainMenuStrip = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importModelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportModelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.loadModelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveModelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pasteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mainToolStrip = new System.Windows.Forms.ToolStrip();
            this.openToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.saveToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.copyToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.pasteToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.vectorManagerToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.calculateToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.mainPanel = new System.Windows.Forms.Panel();
            this.modelOpenFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.modelSaveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.mainTabControl.SuspendLayout();
            this.footnoteTabPage.SuspendLayout();
            this.footnoteSplitContainer.Panel1.SuspendLayout();
            this.footnoteSplitContainer.Panel2.SuspendLayout();
            this.footnoteSplitContainer.SuspendLayout();
            this.panel5.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.simulationsTabPage.SuspendLayout();
            this.mainMenuStrip.SuspendLayout();
            this.mainToolStrip.SuspendLayout();
            this.mainPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainTabControl
            // 
            this.mainTabControl.Controls.Add(this.footnoteTabPage);
            this.mainTabControl.Controls.Add(this.simulationsTabPage);
            this.mainTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainTabControl.Location = new System.Drawing.Point(0, 0);
            this.mainTabControl.Name = "mainTabControl";
            this.mainTabControl.SelectedIndex = 0;
            this.mainTabControl.Size = new System.Drawing.Size(1117, 425);
            this.mainTabControl.TabIndex = 0;
            // 
            // footnoteTabPage
            // 
            this.footnoteTabPage.Controls.Add(this.footnoteSplitContainer);
            this.footnoteTabPage.Location = new System.Drawing.Point(4, 22);
            this.footnoteTabPage.Name = "footnoteTabPage";
            this.footnoteTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.footnoteTabPage.Size = new System.Drawing.Size(1109, 399);
            this.footnoteTabPage.TabIndex = 0;
            this.footnoteTabPage.Text = "Pension Footnote";
            this.footnoteTabPage.UseVisualStyleBackColor = true;
            // 
            // footnoteSplitContainer
            // 
            this.footnoteSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.footnoteSplitContainer.Location = new System.Drawing.Point(3, 3);
            this.footnoteSplitContainer.Name = "footnoteSplitContainer";
            // 
            // footnoteSplitContainer.Panel1
            // 
            this.footnoteSplitContainer.Panel1.Controls.Add(this.panel5);
            this.footnoteSplitContainer.Panel1.Controls.Add(this.panel4);
            // 
            // footnoteSplitContainer.Panel2
            // 
            this.footnoteSplitContainer.Panel2.Controls.Add(this.footNote);
            this.footnoteSplitContainer.Size = new System.Drawing.Size(1103, 393);
            this.footnoteSplitContainer.SplitterDistance = 184;
            this.footnoteSplitContainer.TabIndex = 0;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.groupBox2);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(0, 43);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(184, 350);
            this.panel5.TabIndex = 3;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.scenarioListBox);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(184, 350);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Scenarios";
            // 
            // scenarioListBox
            // 
            this.scenarioListBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scenarioListBox.FormattingEnabled = true;
            this.scenarioListBox.IntegralHeight = false;
            this.scenarioListBox.Location = new System.Drawing.Point(3, 16);
            this.scenarioListBox.Name = "scenarioListBox";
            this.scenarioListBox.Size = new System.Drawing.Size(178, 331);
            this.scenarioListBox.TabIndex = 0;
            this.scenarioListBox.SelectedIndexChanged += new System.EventHandler(this.scenarioListBox_SelectedIndexChanged);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.addScenariosButton);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(184, 43);
            this.panel4.TabIndex = 2;
            // 
            // addScenariosButton
            // 
            this.addScenariosButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.addScenariosButton.Location = new System.Drawing.Point(5, 6);
            this.addScenariosButton.Name = "addScenariosButton";
            this.addScenariosButton.Size = new System.Drawing.Size(176, 32);
            this.addScenariosButton.TabIndex = 0;
            this.addScenariosButton.Text = "Load Scenarios ...";
            this.addScenariosButton.UseVisualStyleBackColor = true;
            this.addScenariosButton.Click += new System.EventHandler(this.addScenariosButton_Click);
            // 
            // footNote
            // 
            this.footNote.Dock = System.Windows.Forms.DockStyle.Fill;
            this.footNote.Location = new System.Drawing.Point(0, 0);
            this.footNote.Name = "footNote";
            this.footNote.Size = new System.Drawing.Size(915, 393);
            this.footNote.TabIndex = 0;
            this.footNote.ShowModel += new System.EventHandler(this.footNote_ShowModel);
            // 
            // simulationsTabPage
            // 
            this.simulationsTabPage.Controls.Add(this.simulations);
            this.simulationsTabPage.Location = new System.Drawing.Point(4, 22);
            this.simulationsTabPage.Name = "simulationsTabPage";
            this.simulationsTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.simulationsTabPage.Size = new System.Drawing.Size(1109, 399);
            this.simulationsTabPage.TabIndex = 1;
            this.simulationsTabPage.Text = "Simulations";
            this.simulationsTabPage.UseVisualStyleBackColor = true;
            // 
            // simulations
            // 
            this.simulations.Dock = System.Windows.Forms.DockStyle.Fill;
            this.simulations.Location = new System.Drawing.Point(3, 3);
            this.simulations.Margin = new System.Windows.Forms.Padding(0);
            this.simulations.Name = "simulations";
            this.simulations.Size = new System.Drawing.Size(1103, 393);
            this.simulations.TabIndex = 0;
            // 
            // mainMenuStrip
            // 
            this.mainMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem,
            this.toolsToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.mainMenuStrip.Location = new System.Drawing.Point(0, 0);
            this.mainMenuStrip.Name = "mainMenuStrip";
            this.mainMenuStrip.Size = new System.Drawing.Size(1117, 24);
            this.mainMenuStrip.TabIndex = 1;
            this.mainMenuStrip.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.importModelToolStripMenuItem,
            this.exportModelToolStripMenuItem,
            this.toolStripSeparator5,
            this.loadModelToolStripMenuItem,
            this.saveModelToolStripMenuItem,
            this.toolStripSeparator6,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // importModelToolStripMenuItem
            // 
            this.importModelToolStripMenuItem.Name = "importModelToolStripMenuItem";
            this.importModelToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.importModelToolStripMenuItem.Text = "Import &Model definition...";
            this.importModelToolStripMenuItem.Click += new System.EventHandler(this.importModelToolStripMenuItem_Click);
            // 
            // exportModelToolStripMenuItem
            // 
            this.exportModelToolStripMenuItem.Name = "exportModelToolStripMenuItem";
            this.exportModelToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.exportModelToolStripMenuItem.Text = "Export M&odel definition...";
            this.exportModelToolStripMenuItem.Click += new System.EventHandler(this.exportModelToolStripMenuItem_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(207, 6);
            // 
            // loadModelToolStripMenuItem
            // 
            this.loadModelToolStripMenuItem.Name = "loadModelToolStripMenuItem";
            this.loadModelToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.loadModelToolStripMenuItem.Text = "Load Model";
            this.loadModelToolStripMenuItem.Click += new System.EventHandler(this.loadModelToolStripMenuItem_Click);
            // 
            // saveModelToolStripMenuItem
            // 
            this.saveModelToolStripMenuItem.Name = "saveModelToolStripMenuItem";
            this.saveModelToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.saveModelToolStripMenuItem.Text = "Save Model";
            this.saveModelToolStripMenuItem.Click += new System.EventHandler(this.saveModelToolStripMenuItem_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(207, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.exitToolStripMenuItem.Text = "E&xit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pasteToolStripMenuItem});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.editToolStripMenuItem.Text = "&Edit";
            // 
            // pasteToolStripMenuItem
            // 
            this.pasteToolStripMenuItem.Name = "pasteToolStripMenuItem";
            this.pasteToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V)));
            this.pasteToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
            this.pasteToolStripMenuItem.Text = "&Paste";
            this.pasteToolStripMenuItem.Click += new System.EventHandler(this.pasteToolStripMenuItem_Click);
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.toolsToolStripMenuItem.Text = "&Tools";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "&Help";
            // 
            // mainToolStrip
            // 
            this.mainToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripButton,
            this.saveToolStripButton,
            this.toolStripSeparator1,
            this.toolStripButton4,
            this.copyToolStripButton,
            this.pasteToolStripButton,
            this.toolStripSeparator2,
            this.vectorManagerToolStripButton,
            this.toolStripSeparator3,
            this.calculateToolStripButton});
            this.mainToolStrip.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.mainToolStrip.Location = new System.Drawing.Point(0, 24);
            this.mainToolStrip.Name = "mainToolStrip";
            this.mainToolStrip.Padding = new System.Windows.Forms.Padding(0, 2, 1, 0);
            this.mainToolStrip.Size = new System.Drawing.Size(1117, 25);
            this.mainToolStrip.TabIndex = 5;
            this.mainToolStrip.Text = "toolStrip1";
            // 
            // openToolStripButton
            // 
            this.openToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.openToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("openToolStripButton.Image")));
            this.openToolStripButton.ImageTransparentColor = System.Drawing.Color.Black;
            this.openToolStripButton.Name = "openToolStripButton";
            this.openToolStripButton.Size = new System.Drawing.Size(23, 20);
            this.openToolStripButton.Text = "Open";
            this.openToolStripButton.Click += new System.EventHandler(this.openToolStripButton_Click);
            // 
            // saveToolStripButton
            // 
            this.saveToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.saveToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("saveToolStripButton.Image")));
            this.saveToolStripButton.ImageTransparentColor = System.Drawing.Color.Black;
            this.saveToolStripButton.Name = "saveToolStripButton";
            this.saveToolStripButton.Size = new System.Drawing.Size(23, 20);
            this.saveToolStripButton.Text = "Save";
            this.saveToolStripButton.Click += new System.EventHandler(this.saveToolStripButton_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 23);
            // 
            // toolStripButton4
            // 
            this.toolStripButton4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton4.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton4.Image")));
            this.toolStripButton4.ImageTransparentColor = System.Drawing.Color.Black;
            this.toolStripButton4.Name = "toolStripButton4";
            this.toolStripButton4.Size = new System.Drawing.Size(23, 20);
            this.toolStripButton4.Text = "Cut";
            // 
            // copyToolStripButton
            // 
            this.copyToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.copyToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("copyToolStripButton.Image")));
            this.copyToolStripButton.ImageTransparentColor = System.Drawing.Color.Black;
            this.copyToolStripButton.Name = "copyToolStripButton";
            this.copyToolStripButton.Size = new System.Drawing.Size(23, 20);
            this.copyToolStripButton.Text = "Copy";
            // 
            // pasteToolStripButton
            // 
            this.pasteToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.pasteToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("pasteToolStripButton.Image")));
            this.pasteToolStripButton.ImageTransparentColor = System.Drawing.Color.Black;
            this.pasteToolStripButton.Name = "pasteToolStripButton";
            this.pasteToolStripButton.Size = new System.Drawing.Size(23, 20);
            this.pasteToolStripButton.Text = "Paste";
            this.pasteToolStripButton.Click += new System.EventHandler(this.pasteToolStripButton_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 23);
            // 
            // vectorManagerToolStripButton
            // 
            this.vectorManagerToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.vectorManagerToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("vectorManagerToolStripButton.Image")));
            this.vectorManagerToolStripButton.ImageTransparentColor = System.Drawing.Color.Black;
            this.vectorManagerToolStripButton.Name = "vectorManagerToolStripButton";
            this.vectorManagerToolStripButton.Size = new System.Drawing.Size(23, 20);
            this.vectorManagerToolStripButton.Text = "Vector Manager";
            this.vectorManagerToolStripButton.Click += new System.EventHandler(this.vectorManagerToolStripButton_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 23);
            // 
            // calculateToolStripButton
            // 
            this.calculateToolStripButton.Image = global::PensionFund.Properties.Resources.calculator;
            this.calculateToolStripButton.ImageTransparentColor = System.Drawing.Color.White;
            this.calculateToolStripButton.Name = "calculateToolStripButton";
            this.calculateToolStripButton.Size = new System.Drawing.Size(76, 20);
            this.calculateToolStripButton.Text = "Calculate";
            this.calculateToolStripButton.Click += new System.EventHandler(this.calculateToolStripButton_Click);
            // 
            // mainPanel
            // 
            this.mainPanel.Controls.Add(this.mainTabControl);
            this.mainPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainPanel.Location = new System.Drawing.Point(0, 49);
            this.mainPanel.Name = "mainPanel";
            this.mainPanel.Size = new System.Drawing.Size(1117, 425);
            this.mainPanel.TabIndex = 6;
            // 
            // modelOpenFileDialog
            // 
            this.modelOpenFileDialog.Filter = "Pension Fund Model Definition|*.pfd";
            // 
            // modelSaveFileDialog
            // 
            this.modelSaveFileDialog.Filter = "Pension Fund Model Definition|*.pfd";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1117, 474);
            this.Controls.Add(this.mainPanel);
            this.Controls.Add(this.mainToolStrip);
            this.Controls.Add(this.mainMenuStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.mainMenuStrip;
            this.Name = "MainForm";
            this.Text = "Pension Fund";
            this.Shown += new System.EventHandler(this.MainFormLoad);
            this.mainTabControl.ResumeLayout(false);
            this.footnoteTabPage.ResumeLayout(false);
            this.footnoteSplitContainer.Panel1.ResumeLayout(false);
            this.footnoteSplitContainer.Panel2.ResumeLayout(false);
            this.footnoteSplitContainer.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.simulationsTabPage.ResumeLayout(false);
            this.mainMenuStrip.ResumeLayout(false);
            this.mainMenuStrip.PerformLayout();
            this.mainToolStrip.ResumeLayout(false);
            this.mainToolStrip.PerformLayout();
            this.mainPanel.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl mainTabControl;
        private System.Windows.Forms.TabPage footnoteTabPage;
        private System.Windows.Forms.TabPage simulationsTabPage;
        private System.Windows.Forms.MenuStrip mainMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStrip mainToolStrip;
        private System.Windows.Forms.ToolStripButton openToolStripButton;
        private System.Windows.Forms.ToolStripButton saveToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton toolStripButton4;
        private System.Windows.Forms.ToolStripButton copyToolStripButton;
        private System.Windows.Forms.ToolStripButton pasteToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton vectorManagerToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.Panel mainPanel;
        private System.Windows.Forms.SplitContainer footnoteSplitContainer;
        private System.Windows.Forms.ListBox scenarioListBox;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button addScenariosButton;
        private ToolStripMenuItem exitToolStripMenuItem;
        private PensionFund.Views.Simulations simulations;
        private ToolStripMenuItem importModelToolStripMenuItem;
        private ToolStripMenuItem exportModelToolStripMenuItem;
        private ToolStripSeparator toolStripSeparator5;
        private OpenFileDialog modelOpenFileDialog;
        private SaveFileDialog modelSaveFileDialog;
        private FootNote footNote;
        private ToolStripMenuItem loadModelToolStripMenuItem;
        private ToolStripMenuItem saveModelToolStripMenuItem;
        private ToolStripSeparator toolStripSeparator6;
        private ToolStripButton calculateToolStripButton;
        private ToolStripMenuItem pasteToolStripMenuItem;
    }
}