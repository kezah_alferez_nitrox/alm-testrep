﻿using System;
using System.Windows.Forms;

namespace PensionFund.Views
{
    public partial class ScenarioSelectionUserControl : UserControl
    {
        public event EventHandler Changed = delegate { };

        public ScenarioSelectionUserControl()
        {
            this.InitializeComponent();
        }

        public int Max { get; set; }

        public string Value
        {
            get { return this.valueTextBox.Text; }
            set { this.valueTextBox.Text = value; }
        }

        private void minusButton_Click(object sender, EventArgs e)
        {
            this.Increment(-1);
        }

        private void plusButton_Click(object sender, EventArgs e)
        {
            this.Increment(1);
        }

        private void Increment(int increment)
        {
            int value = 0;
            int.TryParse(valueTextBox.Text, out value);
            value += increment;
            value = Math.Max(0, Math.Min(Max, value));
            valueTextBox.Text = value.ToString();
        }

        private void valueTextBox_TextChanged(object sender, EventArgs e)
        {
            this.Changed(sender, e);
        }
    }
}