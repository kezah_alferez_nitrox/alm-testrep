using System;
using System.Globalization;
using System.Threading;
using System.Windows.Forms;
using PensionFund.Views;
using System.IO;

namespace PensionFund
{
    internal static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main(string[] args)
        {
            Thread.CurrentThread.CurrentCulture = Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

#if VIEWER
            RunViewer();
#else
#if !DEBUG
            return;
#endif
            Application.Run(new MainForm(args));
#endif
        }

        private static void RunViewer()
        {
            DirectoryInfo directoryInfo = new DirectoryInfo(Application.StartupPath);
            FileInfo[] files = directoryInfo.GetFiles("*.pfm");

            string selectedFile = null;

            if (files.Length == 1)
            {
                selectedFile = files[0].FullName;
            }
            else if (files.Length > 1)
            {
                ChooseModelForm form = new ChooseModelForm(files);
                if (form.ShowDialog() == DialogResult.OK)
                {
                    selectedFile = form.SelectedFile;
                }
            }

            Application.Run(new MainForm(selectedFile));
        }
    }
}