﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PensionFund.IO
{
    public abstract class AbstractScenarioReader: IScenarioReader
    {
        protected const int MAX_YEARS = 10;

        public abstract double[,] ReadAll2D(int years, int scenarious);

        public abstract double[,,] ReadAll3D(int years, int scenarious);

        public abstract double[,] ReadAll3DTo2D(int years, int scenarious);

        protected double[,] Convert3DTo2D(double[, ,] data3D, int years, int scenarious)
        {
            int nrScenarious = Math.Min(scenarious, data3D.GetLength(0));
            int nrYears = Math.Min(MAX_YEARS, Math.Min(years, data3D.GetLength(2)));
            double[,] data2D = new double[nrScenarious, nrYears];

            if (data3D.GetLength(1) >= nrYears)
            {
                for (int i = 0; i < nrScenarious; i++)
                {
                    for (int j = 0; j < nrYears; j++)
                        data2D[i, j] = data3D[i, j, nrYears - 1];
                }
            }

            return data2D;
        }

    }
}
