using System;
using System.Collections.Generic;
using System.Xml;
using PensionFund.Domain;

namespace PensionFund.IO
{
    public class ModelDefinitionReader
    {
        private readonly string fileName;

        public ModelDefinitionReader(string fileName)
        {
            this.fileName = fileName;
        }

        public IList<ModelItem> ReadAll()
        {
            IList<ModelItem> modelItems = new List<ModelItem>();

            XmlDocument document = new XmlDocument();
            document.Load(fileName);

            XmlNodeList nodes = document.SelectNodes("items/group//item");
            if (nodes != null)
            {
                foreach (XmlNode node in nodes)
                {
                    ModelItem modelItem = new ModelItem();

                    modelItem.Group = node.ParentNode.Attributes["name"].Value;
                    modelItem.DisplayName = node.Attributes["displayName"].Value;
                    modelItem.FunctionName = node.Attributes["name"].Value;

                    XmlAttribute initialValueAttribute = node.Attributes["initialValue"];
                    double initialvalue = 0;
                    if (initialValueAttribute != null)
                    {
                        double.TryParse(node.Attributes["initialValue"].Value, out initialvalue);
                    }
                    modelItem.InitalValue = initialvalue;

                    XmlAttribute isConstantAttribute = node.Attributes["isVector"];
                    if (isConstantAttribute == null)
                    {
                        modelItem.Type = ModelItemType.Formula;
                    }
                    else
                    {
                        modelItem.Type = Convert.ToBoolean(isConstantAttribute.Value) ? ModelItemType.Vector : ModelItemType.Formula;
                    }

                    XmlNode commentNode = node.SelectSingleNode("comment");
                    if (commentNode != null) modelItem.Comment = commentNode.InnerText;

                    XmlNode vectorValuesNode = node.SelectSingleNode("vectorValues");
                    if (vectorValuesNode != null)
                    {
                        foreach (string value in vectorValuesNode.Value.Split(';'))
                        {
                            modelItem.VectorValues.Add(Convert.ToDouble(value));
                        }
                    }

                    XmlNode formulaNode = node.SelectSingleNode("formula");
                    if (formulaNode != null)
                    {
                        modelItem.Formula = formulaNode.InnerText.Trim();
                    }

                    modelItems.Add(modelItem);
                }
            }

            return modelItems;
        }
    }
}