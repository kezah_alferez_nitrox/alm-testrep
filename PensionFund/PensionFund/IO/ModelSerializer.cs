﻿using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using PensionFund.Data;
using PensionFund.Domain;

namespace PensionFund.IO
{
    public class ModelSerializer
    {
        private readonly ModelItemRepository _modelItemRepository = new ModelItemRepository();
        private readonly ScenarioItemRepository _scenarioItemRepository = new ScenarioItemRepository();

        public void Serialize(string fileName, BackgroundWorker worker)
        {
            int modelCount = this._modelItemRepository.Count;
            int scenarioCount = this._scenarioItemRepository.Count;

            using (Stream stream = new FileStream(fileName, FileMode.Create))
            {
                IFormatter formatter = new BinaryFormatter();

                formatter.Serialize(stream, modelCount);
                formatter.Serialize(stream, scenarioCount);

                int i = 0;
                foreach (ModelItem modelItem in this._modelItemRepository.GetAll())
                {
                    i++;
                    worker.ReportProgress(i * 100 / (modelCount + scenarioCount));
                    formatter.Serialize(stream, modelItem);
                }

                foreach (ScenarioItem scenarioItem in this._scenarioItemRepository.GetAll())
                {
                    i++;
                    worker.ReportProgress(i * 100 / (modelCount + scenarioCount));
                    formatter.Serialize(stream, scenarioItem);
                }

                stream.Close();
            }
        }
    }
}