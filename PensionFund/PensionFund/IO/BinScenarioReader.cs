using System;
using System.Diagnostics;
using System.IO;
using Microsoft.VisualBasic;

namespace PensionFund.IO
{
    public class BinScenarioReader : AbstractScenarioReader
    {
        private readonly string fileName;

        public BinScenarioReader(string fileName)
        {
            this.fileName = fileName;
        }

        public override double[,] ReadAll2D(int years, int scenarious)
        {
            double[,] data;
            int year = Math.Min(MAX_YEARS, years);

            using (BinaryReader stream = new BinaryReader(File.OpenRead(fileName)))
            {
                VariantType fileType = (VariantType) stream.ReadInt16();

                if((fileType & VariantType.Array) != VariantType.Array) throw new InvalidDataException("File expected to contain an array: " + fileName);

                short dimensions = stream.ReadInt16();

                int hiDim1 = stream.ReadInt32();
                int loDim1 = stream.ReadInt32();
                int hiDim2 = stream.ReadInt32();
                int loDim2 = stream.ReadInt32();

                Debug.WriteLine(fileName + " : " + fileType + " " + dimensions + "d " + (hiDim1 - loDim1 + 1) + "*" + (hiDim2 - loDim2 + 1));

                int length1 = hiDim1 - loDim1 + 1;
                int length2 = hiDim2 - loDim2 + 1;

                data = new double[length2, length1 > year ? year : length1];

                for (int i1 = 0; i1 < length1; i1++)
                {
                    for (int i2 = 0; i2 < length2; i2++)
                    {
                        if ((fileType & VariantType.Variant) == VariantType.Variant)
                        {
                            VariantType dataType = (VariantType)stream.ReadInt16();

                            // todo if((dataType & VariantType.Double) != VariantType.Double) throw ...
                        }

                        double d = stream.ReadDouble();
                        if (i1 < year)
                        {
                            data[i2, i1] = d;
                        }
                    }
                }

                stream.Close();
            }

            return data;
        }

        public override double[, ,] ReadAll3D(int years, int scenarious)
        {
            double[, ,] data;

            using (BinaryReader stream = new BinaryReader(File.OpenRead(fileName)))
            {
                /* short variantType = */
                stream.ReadInt16();
                /* short dimensions = */
                stream.ReadInt16();

                int hiDim1 = stream.ReadInt32();
                int loDim1 = stream.ReadInt32();
                int hiDim2 = stream.ReadInt32();
                int loDim2 = stream.ReadInt32();
                int hiDim3 = stream.ReadInt32();
                int loDim3 = stream.ReadInt32();

                int length1 = hiDim1 - loDim1 + 1;
                int length2 = hiDim2 - loDim2 + 1;
                int length3 = hiDim3 - loDim3 + 1;

                data = new double[length3, length2, length1];

                for (int i1 = 0; i1 < length1; i1++)
                {
                    for (int i2 = 0; i2 < length2; i2++)
                    {
                        for (int i3 = 0; i3 < length3; i3++)
                        {
                            data[i3, i2, i1] = stream.ReadDouble();
                        }
                    }
                }

                stream.Close();
            }

            return data;
        }

        public override double[,] ReadAll3DTo2D(int years, int scenarious)
        {
            double[,,] data3D = ReadAll3D(years, scenarious);
            return Convert3DTo2D(data3D, years, scenarious);
        }
    }
}