﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using PensionFund.Data;
using PensionFund.Domain;

namespace PensionFund.IO
{
    public class ModelDeSerializer
    {
        private readonly ModelItemRepository _modelItemRepository = new ModelItemRepository();
        private readonly ScenarioItemRepository _scenarioItemRepository = new ScenarioItemRepository();

        public void DeSerialize(string fileName, BackgroundWorker worker)
        {
            this._modelItemRepository.DeleteAll();
            this._scenarioItemRepository.DeleteAll();

            using (Stream stream = new FileStream(fileName, FileMode.Open))
            {
                IFormatter formatter = new BinaryFormatter();
                formatter.Binder = new ILMergeCompatibleBinder();

                int modelCount = (int) formatter.Deserialize(stream);
                int scenarioCount = (int) formatter.Deserialize(stream);

                for (int i = 0; i < modelCount; i++)
                {
                    worker.ReportProgress((i + 1)*100/(modelCount + scenarioCount));
                    ModelItem modelItem = (ModelItem) formatter.Deserialize(stream);
                    this._modelItemRepository.Save(modelItem);
                }

                for (int i = 0; i < scenarioCount; i++)
                {
                    worker.ReportProgress((i + 1 + modelCount)*100/(modelCount + scenarioCount));
                    ScenarioItem scenarioItem = (ScenarioItem) formatter.Deserialize(stream);
                    this._scenarioItemRepository.Save(scenarioItem);
                }

                stream.Close();
            }
        }
    }

    public class ILMergeCompatibleBinder : SerializationBinder
    {
        private static readonly IDictionary<string, Type> TypeCache = new Dictionary<string, Type>();

        public override Type BindToType(string assemblyName, string typeName)
        {
            string requestedTypeName = typeName + ", " + assemblyName;

            Type type;
            if (!TypeCache.TryGetValue(requestedTypeName, out type))
            {
                string providedTypeName = requestedTypeName;
                if (assemblyName.Contains("PensionFund"))
                {
                    providedTypeName = typeName + ", " + Assembly.GetExecutingAssembly().FullName;
                }
                type = Type.GetType(providedTypeName);
                TypeCache.Add(requestedTypeName, type);
            }

            return type;
        }
    }
}