using System.Collections.Generic;
using PensionFund.Data;
using PensionFund.Domain;

namespace PensionFund.IO
{
    public class ModelDefinitionLoader
    {
        private readonly IRepository<ModelItem> modelItemRepository;

        public ModelDefinitionLoader(IRepository<ModelItem> modelItemRepository)
        {
            this.modelItemRepository = modelItemRepository;
        }

        public void Load(string fileName)
        {
            modelItemRepository.DeleteAll();
            ModelDefinitionReader reader = new ModelDefinitionReader(fileName);
            IList<ModelItem> modelItems = reader.ReadAll();
            foreach (ModelItem modelItem in modelItems)
            {
                modelItemRepository.Save(modelItem);
            }
        }
    }
}