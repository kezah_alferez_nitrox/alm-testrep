﻿using System.Linq;
using System.Collections.Generic;
using System.Xml;
using PensionFund.Data;
using PensionFund.Domain;

namespace PensionFund.IO
{
    public class ModelDefinitionWriter
    {
        private readonly IRepository<ModelItem> _modelItemRepository;
        readonly XmlDocument _document = new XmlDocument();

        public ModelDefinitionWriter(IRepository<ModelItem> modelItemRepository)
        {
            _modelItemRepository = modelItemRepository;
        }

        public void WriteAll(string filename)
        {
            var groups = new List<string>();

            foreach (var item in _modelItemRepository.GetAll())
                if (!groups.Contains(item.Group))
                    groups.Add(item.Group);

            var documentElement = _document.CreateElement("items");
            _document.AppendChild(documentElement);

            foreach (var group in groups)
            {
                var localGroup = group;

                var groupElement = _document.CreateElement("group");
                documentElement.AppendChild(groupElement);
                groupElement.Attributes.Append(CreateAttribute("name", group));

                foreach (var modelItem in _modelItemRepository.Find(m => m.Group == localGroup))
                {
                    var modelItemElement = _document.CreateElement("item");
                    groupElement.AppendChild(modelItemElement);
                    modelItemElement.Attributes.Append(CreateAttribute("name", modelItem.FunctionName));
                    modelItemElement.Attributes.Append(CreateAttribute("displayName", modelItem.DisplayName));
                    modelItemElement.Attributes.Append(CreateAttribute("initialValue", modelItem.InitalValue.ToString()));
                    modelItemElement.Attributes.Append(CreateAttribute("isVector", (modelItem.Type == ModelItemType.Vector).ToString()));
                    modelItemElement.AppendChild(this.CreateElement("comment", modelItem.Comment));
                    modelItemElement.AppendChild(this.CreateElement("formula", modelItem.Formula));
                    modelItemElement.AppendChild(this.CreateElement("vectorValues",
                        string.Join(";", modelItem.VectorValues.Select(d => d.ToString()).ToArray())));
                }
            }

            _document.Save(filename);
        }

        private XmlNode CreateElement(string name, string content)
        {
            var formulaElement = this._document.CreateElement(name);
            formulaElement.AppendChild(this._document.CreateTextNode(content));
            return formulaElement;
        }

        XmlAttribute CreateAttribute(string name, string value)
        {
            var xmlAttribute = _document.CreateAttribute(name);
            xmlAttribute.Value = value;
            return xmlAttribute;
        }
    }
}
