﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace PensionFund.IO
{
    public class CsvScenarioReader : AbstractScenarioReader
    {
        private readonly string fileName;

        public CsvScenarioReader(string fileName)
        {
            this.fileName = fileName;
        }

        public override double[,] ReadAll2D(int years, int scenarious)
        {
            double[,] data;
            int rows;
            int col;

            GetFileDimension(out rows, out col);

            int columns = col > MAX_YEARS ? MAX_YEARS : col;

            using(StreamReader streamReader = new StreamReader(fileName))
            {
                data = new double[rows, columns];

                int i = 0;
                while (!streamReader.EndOfStream)
                {
                    string line = streamReader.ReadLine();
                    string[] values = line.Split(new[] {','});

                    for (int j = 0; j < columns; j++)
                        data[i, j] = double.Parse(values[j]);

                    i++;
                }

                streamReader.Close();
            }

            return data;
        }

        public override double[, ,] ReadAll3D(int years, int scenarious)
        {
            double[,,] data;
            int rows;
            int col;

            GetFileDimension(out rows, out col);

            using (StreamReader streamReader = new StreamReader(fileName))
            {
                int m = rows/years;
                data = new double[m, col, years];

                int i = 0;
                int k = 0;

                while (!streamReader.EndOfStream)
                {
                    string line = streamReader.ReadLine();
                    string[] values = line.Split(new[] { ',' });

                    for (int j = 0; j < col; j++)
                        data[i, j, k] = double.Parse(values[j]);

                    k++;

                    if (k == years)
                    {
                        i++;
                        k = 0;
                    }
                }

                streamReader.Close();
            }

            return data;
        }

        public override double[,] ReadAll3DTo2D(int years, int scenarious)
        {
            double[, ,] data3D = ReadAll3D(years, scenarious);
            return Convert3DTo2D(data3D, years, scenarious);
        }

        private void GetFileDimension(out int rows, out int columns)
        {
            rows = 0;
            columns = int.MaxValue;

            using (StreamReader streamReader = new StreamReader(fileName))
            {
                while (!streamReader.EndOfStream)
                {
                    rows++;
                    columns = Math.Min(columns, streamReader.ReadLine().Split(new[] { ',' }).Length);
                }

                streamReader.Close();
            }
        }
    }
}
