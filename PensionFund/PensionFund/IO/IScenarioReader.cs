﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PensionFund.IO
{
    public interface IScenarioReader
    {
        double[,] ReadAll2D(int years, int scenarious);
        double[, ,] ReadAll3D(int years, int scenarious);
        double[,] ReadAll3DTo2D(int years, int scenarious);
    }
}
