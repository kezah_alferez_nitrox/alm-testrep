﻿using System.Collections.Generic;
using System.Windows.Forms;
using PensionFund.Domain;

namespace PensionFund.Controls
{
    class ModelTreeview : TreeView
    {
        public void Load(IList<ModelItem> items)
        {
            Nodes.Clear();

            IDictionary<string, TreeNode> groupNodes = new Dictionary<string, TreeNode>();

            foreach (ModelItem item in items)
            {
                TreeNode groupNode;
                if(!groupNodes.TryGetValue(item.Group, out groupNode))
                {
                    groupNode = new TreeNode(item.Group);
                    groupNodes.Add(item.Group, groupNode);
                    Nodes.Add(groupNode);
                }
                var itemNode = new TreeNode(item.DisplayName) {Tag = item};
                groupNode.Nodes.Add(itemNode);
            }

            ExpandAll();
        }

        protected override void OnMouseClick(MouseEventArgs e)
        {
            base.OnMouseClick(e);

            if (e.Button != MouseButtons.Right) return;

            TreeNode node = GetNodeAt(e.Location);
            if (null != node)
            {
                SelectedNode = node;
            }
        }
    }
}
