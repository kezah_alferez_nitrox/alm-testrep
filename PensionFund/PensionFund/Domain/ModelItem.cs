using System;
using System.Collections.Generic;

namespace PensionFund.Domain
{
    [Serializable]
    public class ModelItem
    {
        public const string OtherGroupName = "Other";

        public string Group { get; set; }
        public string DisplayName { get; set; }
        public string FunctionName { get; set; }
        private ModelItemType _type = ModelItemType.Formula;
        public ModelItemType Type
        {
            get { return this._type; }
            set { this._type = value; }
        }

        public string Formula { get; set; }
        private IList<double> _vectorValues = new List<double>();
        public IList<double> VectorValues
        {
            get { return this._vectorValues; }
            set { this._vectorValues = value; }
        }

        public double InitalValue { get; set; }
        public string Comment { get; set; }
        public bool PanelDisplayed { get; set; }

        public double GetVectorValue(int index)
        {
            if (VectorValues.Count == 0) return 0;

            if (index < 0) return VectorValues[0];
            if (index >= VectorValues.Count) return VectorValues[VectorValues.Count - 1];

            return VectorValues[index];
        }

        public void SetVectorValue(int index, double value)
        {
            EnsureVectorValueCount(index + 1);

            VectorValues[index] = value;
        }

        public override string ToString()
        {
            return String.Format("{0}\\{1}", this.Group, this.DisplayName);
        }

        public const string ExpectedBenefitPayment = "ExpectedBenefitPayment";              //PBO output
        public const string ExpectedPBO = "ExpectedPBO";                                    //PBO output
        public const string DiscountRate = "AA_DiscountRate";                                  //input
        public const string AnnualAdjustmentsToPensions = "AA_AnnualAdjustmentsToPensions";    //input
        public const string ActualReturnOnAssets = "AA_ActualReturnOnAssets";                  //input

        public bool IsInput()
        {
            return IsInput(this.FunctionName);
        }

        public static bool IsInput(string itemName)
        {
            return itemName == DiscountRate ||
                   itemName == AnnualAdjustmentsToPensions ||
                   itemName == ActualReturnOnAssets;
        }

        public bool IsVector()
        {
            return this.Type == ModelItemType.Vector;
        }

        public void EnsureVectorValueCount(int count)
        {
            if (count > VectorValues.Count)
            {
                int valuesToAdd = count - this.VectorValues.Count;
                for (int i = 0; i < valuesToAdd; i++)
                {
                    VectorValues.Add(0);
                }
            }
        }

        public ModelItem Clone()
        {
            ModelItem modelItem = new ModelItem();

            modelItem.Comment = this.Comment;
            modelItem.DisplayName = this.DisplayName;
            modelItem.Formula = this.Formula;
            modelItem.FunctionName = this.FunctionName;
            modelItem.Group = this.Group;
            modelItem.InitalValue = this.InitalValue;
            modelItem.PanelDisplayed = this.PanelDisplayed;
            modelItem.Type = this.Type;
            modelItem.VectorValues = this.VectorValues;

            return modelItem;
        }
    }

    public enum ModelItemType
    {
        Vector,
        Formula
    }
}