﻿using System;
using System.Collections.Generic;
using PensionFund.Calculus;
using PensionFund.Data;

namespace PensionFund.Domain
{
    [Serializable]
    public class ScenarioItem
    {
        public ScenarioItem(int yearCount, double[][] initialValues)
        {
            this.YearCount = yearCount;
            this.InitialValues = initialValues;
            this.Calculator = new Calculator(yearCount);
            this.Calculator.Values[ModelItem.DiscountRate] = this.InitialValues[0];
            this.Calculator.Values[ModelItem.AnnualAdjustmentsToPensions] = this.InitialValues[1];
            this.Calculator.Values[ModelItem.ActualReturnOnAssets] = this.InitialValues[2];
        }

        public int Id { get; set; }
        public int YearCount { get; private set; }
        public Calculator Calculator { get; set; }
        public double[][] InitialValues { get; private set; }

        [NonSerialized]
        private IDictionary<string, ModelItem> _modelItems;

        [NonSerialized]
        private ModelItemRepository _modelItemRepository = new ModelItemRepository();

        public IDictionary<string, ModelItem> ModelItems
        {
            get
            {
                if (_modelItems == null)
                {
                    if (_modelItemRepository == null)
                    {
                        _modelItemRepository = new ModelItemRepository();
                    }
                    _modelItems = new Dictionary<string, ModelItem>();
                    foreach (ModelItem modelItem in _modelItemRepository.GetAll())
                    {
                        _modelItems.Add(modelItem.FunctionName, modelItem);
                    }
                }
                return _modelItems;
            }
        }



        public void Evaluate()
        {
            this.Calculator.EvaluateAll(ModelItems);
        }

        public bool TryEvaluate()
        {
            try
            {
                this.Evaluate();
            }
            catch(Exception)
            {
                return false;
            }
            return true;
        }

        public override string ToString()
        {
            return string.Format("Scenario {0:0000}", this.Id + 1);
        }

        public ScenarioItem Clone()
        {
            var cloneInitialValues = new double[this.InitialValues.Length][];

            for (int i = 0; i < this.InitialValues.Length; i++)
            {
                cloneInitialValues[i] = new double[this.InitialValues[i].Length];
                Array.Copy(this.InitialValues[i], cloneInitialValues[i], this.InitialValues[i].Length);
            }

            ScenarioItem scenarioItem = new ScenarioItem(this.YearCount, cloneInitialValues);

            foreach (ModelItem modelItem in ModelItems.Values)
            {
                scenarioItem.ModelItems[modelItem.FunctionName] = modelItem.Clone();
            }

            return scenarioItem;
        }
    }
}