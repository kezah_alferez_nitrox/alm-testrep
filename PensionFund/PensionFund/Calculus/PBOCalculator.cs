using System;

namespace PensionFund.Calculus
{
    [Serializable]
    public class PBOCalculator
    {
        private readonly int _yearCount;
        private double[] _discountRate;
        private double[] _rateOfCompensationIncrease;

        private double[] _annualAdjustmentsToPensions;
        private double[] _actives;
        private double[] _deferredsPensioners;

        private readonly double[] _expectedPBO;
        private readonly double[,] _expectedBenefitPayment;
        private readonly double[,] _discountRateInPercent;

        private readonly double[] _initialExpectedBenefitPayment = { 1261, 1329.58086, 1393.502734, 1464.759239, 1517.536516, 1549.543368, 1576.088409, 1596.455439, 1610.249048, 1617.011825, 1613.441195, 1603.175564, 1587.423873, 1565.103146, 1534.304862, 1496.306769, 1454.709208, 1406.69378, 1354.765935, 1300.229603, 1239.307771, 1174.999365, 1108.117167, 1037.469291, 965.4688811, 896.0111277, 827.876413, 760.5480227, 693.7461819, 625.883066, 561.0776095, 503.3932126, 450.2544695, 400.6910663, 354.7195471, 312.3842542, 272.5088654, 235.8822509, 203.2285405, 174.4055728, 149.2147682, 126.3097064, 104.9895135, 86.26893702, 69.58598104, 56.45691518, 46.53926361, 37.13234097, 30.04739394, 23.87543416, 18.31166903, 14.10126893, 10.67570058, 8.197095479, 6.334873419, 5.013302437, 4.480520249, 0.856881427, 0.611950559, 0.428781153, 0.289567308, 0.192734388, 0.121443681, 0.067219671, 0.036726951, 0.021659982, 0.007092817, 0.003482006, 0, 0, 0, 0, 0 };

        public PBOCalculator(int yearCount)
        {
            _yearCount = yearCount;

            _expectedPBO = new double[yearCount];
            _expectedBenefitPayment = new double[_initialExpectedBenefitPayment.Length, yearCount];
            _discountRateInPercent = new double[_initialExpectedBenefitPayment.Length, yearCount];

            for (int s = 0; s < _initialExpectedBenefitPayment.Length; s++)
            {
                _expectedBenefitPayment[s, 0] = _initialExpectedBenefitPayment[s];
            }

            for (int t = 0; t < yearCount; t++)
            {
                _discountRateInPercent[t, t] = 1;
            }
        }

        public double[] DiscountRate
        {
            get { return _discountRate; }
            set { _discountRate = value; }
        }

        public double[] RateOfCompensationIncrease
        {
            get { return _rateOfCompensationIncrease; }
            set { _rateOfCompensationIncrease = value; }
        }

        public double[] AnnualAdjustmentsToPensions
        {
            get { return _annualAdjustmentsToPensions; }
            set { _annualAdjustmentsToPensions = value; }
        }

        public double[] Actives
        {
            get { return _actives; }
            set { _actives = value; }
        }

        public double[] DeferredsPensioners
        {
            get { return _deferredsPensioners; }
            set { _deferredsPensioners = value; }
        }

        public double[] ExpectedPBO
        {
            get { return _expectedPBO; }
        }

        public double[,] ExpectedBenefitPayment
        {
            get { return _expectedBenefitPayment; }
        }

        public double[,] DiscountRateInPercent
        {
            get { return _discountRateInPercent; }
        }

        public void Compute()
        {
            //compute AnnualAdjustmentsToPensions
            _rateOfCompensationIncrease = new double[_yearCount];
            _rateOfCompensationIncrease[0] = 2.9;
            for (int t = 1; t < _yearCount; t++)
            {
                _rateOfCompensationIncrease[t] = _annualAdjustmentsToPensions[t] + _rateOfCompensationIncrease[0] -
                                                  _annualAdjustmentsToPensions[0];
            }

            for (int s = 1; s < _initialExpectedBenefitPayment.Length; s++)
            {
                int maxT = Math.Min(s + 1, _yearCount);

                for (int t = 1; t < maxT; t++)
                {
                    _expectedBenefitPayment[s, t] = _expectedBenefitPayment[s, t - 1] * _actives[t] *
                                                   (1 + _rateOfCompensationIncrease[t] / 100 +
                                                    _annualAdjustmentsToPensions[t] / 100) +
                                                   _expectedBenefitPayment[s, t - 1] * _deferredsPensioners[t] *
                                                   (1 + _annualAdjustmentsToPensions[t] / 100);
                }
            }

            for (int s = 1; s < _initialExpectedBenefitPayment.Length; s++)
            {
                int maxT = Math.Min(s, _yearCount);

                for (int t = 0; t < maxT; t++)
                {
                    _discountRateInPercent[s, t] = Math.Exp((-_discountRate[t]) / 100 * (s - t));
                }
            }

            for (int t = 0; t < _yearCount; t++)
            {
                for (int s = 0; s < _initialExpectedBenefitPayment.Length - t; s++)
                {
                    _expectedPBO[t] += _expectedBenefitPayment[s + t, t] * _discountRateInPercent[s + t, t];
                }
            }
        }
    }
}