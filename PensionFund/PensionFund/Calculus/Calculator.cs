using System;
using System.Collections.Generic;
using System.Diagnostics;
using NCalc;
using PensionFund.Domain;
using PensionFund.Views;

namespace PensionFund.Calculus
{
    [Serializable]
    public class Calculator
    {
        private readonly int _valueCount;
        private readonly IDictionary<string, double[]> _values = new Dictionary<string, double[]>();

        [NonSerialized]
        private PBOCalculator _pboCalculator;
        [NonSerialized]
        private IDictionary<string, Expression> _formulas;
        [NonSerialized]
        private IDictionary<string, ModelItem> _modelItems = new Dictionary<string, ModelItem>();

        public Calculator(int valueCount)
        {
            this._valueCount = valueCount;
        }

        public IDictionary<string, double[]> Values
        {
            get { return this._values; }
        }

        public void EvaluateAll(IDictionary<string, ModelItem> modelItems)
        {
            TraceListener[] listeners = new TraceListener[Debug.Listeners.Count];
            Debug.Listeners.CopyTo(listeners, 0);
            Debug.Listeners.Clear();

            this._modelItems = modelItems;

            this.InitCalculator();
            this.InitPBOCalculator();

            for (int t = 1; t < this._valueCount; t++)
            {
                foreach (KeyValuePair<string, ModelItem> item in this._modelItems)
                {
                    string name = item.Key;
                    ModelItem modelItem = item.Value;
                    try
                    {
                        this._values[name][t] = this.Evaluate(name, t);
                    }
                    catch (Exception ex)
                    {
                        throw new ComputeException(modelItem, ex.Message, ex);
                    }
                }
            }

            Debug.Listeners.AddRange(listeners);
        }

        private void InitCalculator()
        {
            this._formulas = new Dictionary<string, Expression>();

            foreach (var modelItem in this._modelItems.Values)
            {
                Expression e = null;
                if (!String.IsNullOrEmpty(modelItem.Formula))
                {
                    e = new Expression(modelItem.Formula);
                    e.EvaluateFunction += this.EvaluateFunction;
                }
                this._formulas.Add(modelItem.FunctionName, e);
            }

            // init values
            foreach (var formulaName in this._formulas.Keys)
            {
                if (!ModelItem.IsInput(formulaName))
                {
                    this.Values[formulaName] = new double[this._valueCount];
                    for (int i = 0; i < this._valueCount; i++)
                        this.Values[formulaName][i] = Double.NaN;
                }
            }

            foreach (var modelItem in this._modelItems.Values)
            {
                this.Values[modelItem.FunctionName][0] = modelItem.InitalValue;
            }
        }

        private void InitPBOCalculator()
        {
            // todo : check values
            this._pboCalculator = new PBOCalculator(this._valueCount);
            this._pboCalculator.Actives = new[] { 0.65, 0.65, 0.65, 0.65, 0.65, 0.65 };
            this._pboCalculator.DeferredsPensioners = new[] { 0.35, 0.35, 0.35, 0.35, 0.35, 0.35 };
            this._pboCalculator.DiscountRate = this._values[ModelItem.DiscountRate];
            this._pboCalculator.AnnualAdjustmentsToPensions = this._values[ModelItem.AnnualAdjustmentsToPensions];
            this._pboCalculator.Compute();
        }

        private double Evaluate(string line, int t)
        {
            ModelItem modelItem = this._modelItems[line];

            if (modelItem.IsVector())
            {
                return modelItem.GetVectorValue(t);
            }

            if (t == 0 || !Double.IsNaN(this._values[line][t]))
            {
                return this._values[line][t];
            }

            var e = this._formulas[line];
            e.Parameters["t"] = t;

            double value = Convert.ToDouble(e.Evaluate());
            return value;
        }

        private void EvaluateFunction(string name, FunctionArgs args)
        {
            if (name == ModelItem.ExpectedBenefitPayment)
            {
                int t = Convert.ToInt32(args.Parameters[0].Evaluate());
                args.Result = this._pboCalculator.ExpectedBenefitPayment[t, t];
            }
            else
                if (name == ModelItem.ExpectedPBO)
                {
                    int t = Convert.ToInt32(args.Parameters[0].Evaluate());
                    args.Result = this._pboCalculator.ExpectedPBO[t];
                }
                else
                    if (this._values.ContainsKey(name))
                    {
                        int t = Convert.ToInt32(args.Parameters[0].Evaluate());
                        args.Result = this.Evaluate(name, t);
                    }
                    else
                    {
                        foreach (var expression in args.Parameters)
                            expression.EvaluateFunction += this.EvaluateFunction;
                    }
        }

        public static bool FormulaIsValid(string text, out string error)
        {
            Expression expression = new Expression(text);

            bool hasErrors = expression.HasErrors();

            error = hasErrors ? expression.Error : null;

            return hasErrors;
        }
    }
}