using System;
using System.Collections.Generic;
using PensionFund.Domain;
using System.Linq;

namespace PensionFund.Data
{
    public class ModelItemRepository : IRepository<ModelItem>
    {
        private static readonly IDictionary<string, ModelItem> AllModelItems = new Dictionary<string, ModelItem>();

        public void Save(ModelItem modelItem)
        {
            AllModelItems[modelItem.FunctionName] = modelItem;
        }

        public void Update(ModelItem modelItem)
        {
            AllModelItems[modelItem.FunctionName] = modelItem;
        }

        public void Delete(ModelItem modelItem)
        {
            AllModelItems.Remove(modelItem.FunctionName);
        }

        public ModelItem Load(object id)
        {
            string typedId = (string) id;
            return AllModelItems.ContainsKey(typedId) ? AllModelItems[typedId] : null;
        }

        public IList<ModelItem> GetAll()
        {
            return AllModelItems.Values.ToList();
        }

        public IEnumerable<ModelItem> Find(Func<ModelItem, bool> where)
        {
            return AllModelItems.Values.Where(where).ToList();
        }

        public void DeleteAll()
        {
            AllModelItems.Clear();
        }

        public int Count
        {
            get { return AllModelItems.Values.Count; }
        }
    }
}