using System;
using System.Collections.Generic;

namespace PensionFund.Data
{
    public interface IRepository<T>
    {
        void Save(T obj);
        void Update(T obj);
        void Delete(T obj);
        T Load(object id);
        IList<T> GetAll();
        IEnumerable<T> Find(Func<T, bool> where);
        void DeleteAll();
        int Count { get; }
    }
}