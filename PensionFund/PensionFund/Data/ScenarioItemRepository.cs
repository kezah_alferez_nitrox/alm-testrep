using System;
using System.Collections.Generic;
using PensionFund.Domain;

namespace PensionFund.Data
{
    public class ScenarioItemRepository : IRepository<ScenarioItem>
    {
        private static readonly List<ScenarioItem> AllScenarioItems = new List<ScenarioItem>();

        public void Save(ScenarioItem scenarioItem)
        {
            AllScenarioItems.Add(scenarioItem);
            scenarioItem.Id = AllScenarioItems.Count - 1;
        }

        public void Update(ScenarioItem scenarioItem)
        {
            //todo
            //AllScenarioItems[modelItem.FunctionName] = modelItem;
        }

        public void Delete(ScenarioItem scenarioItem)
        {
            AllScenarioItems.Remove(scenarioItem);
        }

        public ScenarioItem Load(object id)
        {
            return AllScenarioItems[(int)id];
        }

        public IList<ScenarioItem> GetAll()
        {
            return AllScenarioItems;
        }

        public IEnumerable<ScenarioItem> Find(Func<ScenarioItem, bool> where)
        {
            //todo
            //return AllScenarioItems.Find(where).ToList();
            return null;
        }

        public void DeleteAll()
        {
            AllScenarioItems.Clear();
        }

        public int Count
        {
            get { return AllScenarioItems.Count; }
        }
    }
}