﻿using System;
using System.Collections;
using System.Linq;
using PensionFund.Domain;

namespace PensionFund.Data
{
    [Serializable]
    public class Model
    {
        public ArrayList ModelItems;
        public ArrayList ScenarioItems;
        
        public Model()
        {
            ModelItems = new ArrayList(new ModelItemRepository().GetAll().ToArray());
            ScenarioItems =  new ArrayList(new ScenarioItemRepository().GetAll().ToArray());
        }

        public void Save()
        {
            var modelItemRepository = new ModelItemRepository();
            modelItemRepository.DeleteAll();
            foreach (var modelItem in ModelItems)
                modelItemRepository.Save((ModelItem)modelItem);

            var scenarioItemRepository = new ScenarioItemRepository();
            scenarioItemRepository.DeleteAll();
            foreach (var scenarioItem in ScenarioItems)
                scenarioItemRepository.Save((ScenarioItem)scenarioItem);
        }
    }
}
