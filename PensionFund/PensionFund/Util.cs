using System;
using System.Globalization;

namespace PensionFund
{
    public class Util
    {
        private static readonly CultureInfo[] AcceptedCultures = new[]
        {
            new CultureInfo("fr-FR"),
            new CultureInfo("en-US"), 
        };

        public static double ConvertToDouble(object obj)
        {
            if(obj is double) return (double) obj;

            string str = Convert.ToString(obj);
            if (string.IsNullOrEmpty(str)) return double.NaN;
            str = str.Trim();
            str = ReplaceParantheses(str);

            foreach (CultureInfo accepedCulture in AcceptedCultures)
            {
                double doubleValue;
                if (double.TryParse(str,
                                     NumberStyles.AllowDecimalPoint | NumberStyles.AllowLeadingSign |
                                     NumberStyles.AllowThousands, accepedCulture, out doubleValue))
                {
                    return doubleValue;
                }
            }

            return double.NaN;
        }

        private static string ReplaceParantheses(string number)
        {
            if (string.IsNullOrEmpty(number)) return number;

            number = number.Trim();

            if (number[0] == '(' && number[number.Length - 1] == ')')
            {
                number = "-" + number.Substring(1, number.Length - 2);
            }
            return number;
        }
    }
}