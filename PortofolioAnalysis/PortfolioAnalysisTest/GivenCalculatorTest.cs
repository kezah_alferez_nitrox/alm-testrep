using NUnit.Framework;
using PortfolioAnalysis.Calculations;

namespace PortfolioAnalysisTest
{
    [TestFixture]
    public class GivenCalculatorTest
    {
        private readonly int[] days = new[] { 0, 92, 92, 90, 91, 92, 92, 91, 91, 92, 92, };
        private readonly double[] principal = { 0, 312500, 312500, 312500, 312500, 312500, 312500, 312500, 312500, 312500, 312500, };
        private readonly double[] totalCashFlow = new[] { 0, 317722.916667, 317696.562500, 317381.250000, 317354.913194, 317297.895833, 317153.906250, 316703.062500, 316063.218750, 315189.402778, 313997.635417, };
        private readonly double[] index = new[] { 0, 0.00404, 0.00473, 0.00531, 0.00628, 0.007513, 0.009155, 0.010802, 0.012536, 0.014338, 0.016253, };

        private const double Epsilon = 0.0001;

        [Test]
        public void TestDmFactor()
        {
            double[] expectedDmFactors = { 1.000000, 0.998331, 0.996490, 0.994548, 0.992346, 0.989813, 0.986874, 0.983567, 0.979842, 0.975644, 0.970991, };

            double[] dmFactor = new double[days.Length];
            PricingCalculator.PricingFunctions.ComputeDmFactor(days, index, 25, dmFactor);

            for (int i = 0; i < dmFactor.Length; i++)
            {
                Assert.AreEqual(expectedDmFactors[i], dmFactor[i], Epsilon);
            }
        }

        [Test]
        public void TestCashflowXirr()
        {
            double[] expectedCfXirr = { 0, 316953.3708, 316159.4623, 315097.2870, 314316.3015, 313498.6721, 312597.4368, 311405.2230, 310031.5344, 308425.5534, 306515.1586, };

            double[] cfXirr = new double[days.Length];
            PricingCalculator.PricingFunctions.ComputeCashflowXirr(days, totalCashFlow, 0.966736, cfXirr);

            for (int i = 1; i < cfXirr.Length; i++)
            {
                Assert.AreEqual(expectedCfXirr[i], cfXirr[i], 0.1);
            }
        }

        [Test]
        public void TestGivenDm()
        {
            PricingCalculator calculator = new PricingCalculator(3125000, this.days, this.totalCashFlow, this.index);

            calculator.SolveWithDm(25);

            Assert.AreEqual(25, calculator.Dm, Epsilon);
            Assert.AreEqual(1, calculator.Price, Epsilon);
            Assert.AreEqual(0.96673, calculator.Xirr, Epsilon);
        }

        [Test]
        public void TestGivenXirr()
        {
            PricingCalculator calculator = new PricingCalculator(3125000, this.days, this.totalCashFlow, this.index);

            calculator.SolveWithXirr(0.9667369);

            Assert.AreEqual(25, calculator.Dm, Epsilon);
            Assert.AreEqual(1, calculator.Price, Epsilon);
            Assert.AreEqual(0.96673, calculator.Xirr, Epsilon);
        }

        [Test]
        public void TestGivenPrice()
        {
            PricingCalculator calculator = new PricingCalculator(3125000, this.days, this.totalCashFlow, this.index);

            calculator.SolveWithPrice(1);

            Assert.AreEqual(25, calculator.Dm, Epsilon);
            Assert.AreEqual(1, calculator.Price, Epsilon);
            Assert.AreEqual(0.96673, calculator.Xirr, Epsilon);
            Assert.AreEqual(1.375, calculator.GetWal(principal), Epsilon);
            Assert.AreEqual(1.38272, calculator.GetModDurn(), Epsilon);
        }
    }
}