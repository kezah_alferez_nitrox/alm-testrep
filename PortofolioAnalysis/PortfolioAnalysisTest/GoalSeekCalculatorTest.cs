using NUnit.Framework;
using PortfolioAnalysis.Calculations;

namespace PortfolioAnalysisTest
{
    [TestFixture]
    public class GoalSeekTest
    {
        [Test]
        public void TestSimpleWithGoodGuess()
        {
            GoalFunction func = d => d;
            double result = ExcelFunctions.GoalSeek(func, 0);

            Assert.AreEqual(0, result);
        }

        [Test]
        public void TestSimpleWithWrongGuess()
        {
            GoalFunction func = d => d;
            double result = ExcelFunctions.GoalSeek(func, 100);

            Assert.AreEqual(0, result, 0.001);
        }
    }
}