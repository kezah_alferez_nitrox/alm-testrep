using System;
using System.Collections;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;
using Castle.ActiveRecord;
using Castle.ActiveRecord.Framework.Config;
using NHibernate;
using PortfolioAnalysis.Common.Entities;

namespace Infrastructure
{
    public sealed class SessionManager
    {
        private readonly ISessionFactory sessionFactory;

        public static ISession CurrentSession
        {
            get
            {
                return SessionFactory.GetCurrentSession();
            }
        }

        internal static ISessionFactory SessionFactory
        {
            get { return Instance.sessionFactory; }
        }

        private static SessionManager Instance
        {
            get { return NestedSessionManager.SessionManager; }
        }

        internal static ISession OpenSession()
        {
            return SessionFactory.OpenSession();
        }

        private SessionManager()
        {
            string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;

            Hashtable properties = new Hashtable();

            properties.Add("hibernate.connection.driver_class", "NHibernate.Driver.SqlClientDriver");
            properties.Add("hibernate.dialect", "NHibernate.Dialect.MsSql2005Dialect");
            properties.Add("hibernate.connection.provider", "NHibernate.Connection.DriverConnectionProvider");
            properties.Add("hibernate.current_session_context_class", "NHibernate.Context.ManagedWebSessionContext, NHibernate");
            properties.Add("hibernate.connection.connection_string", connectionString);
            // properties.Add("hibernate.cache.use_second_level_cache", "false");
#if DEBUG
            //properties.Add("hibernate.show_sql", "true");
#endif
            InPlaceConfigurationSource config = new InPlaceConfigurationSource();

            config.Add(typeof(ActiveRecordBase), properties);

            Type sampleEntityType = typeof(Security);
            Type[] types = sampleEntityType.Assembly.GetTypes().Where(t => t.Namespace == sampleEntityType.Namespace).ToArray();

            ActiveRecordStarter.ResetInitializationFlag();
            ActiveRecordStarter.Initialize(config, types);

            EnsureDatabaseExists(connectionString);

            sessionFactory = ActiveRecordMediator.GetSessionFactoryHolder().GetSessionFactory(sampleEntityType);
        }

        private static void EnsureDatabaseExists(string connectionString)
        {
            Regex initialCatalogRegex = new Regex(@"Initial Catalog=(\w+);");
            Match match = initialCatalogRegex.Match(connectionString);
            string databaseName = match.Groups[1].Value;
            string masterConnectionString = initialCatalogRegex.Replace(connectionString, "");

            object dbId = ExecuteScalar(string.Format("select db_id('{0}')", databaseName), masterConnectionString);

            if(dbId == DBNull.Value)
            {
                ExecuteNonQuery(string.Format("create database {0}", databaseName), masterConnectionString);
                ActiveRecordStarter.CreateSchema();
            }            
        }

        private static object ExecuteScalar(string commandText, string connectionString)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                SqlCommand sqlCommand = connection.CreateCommand();
                sqlCommand.CommandText = commandText;
                return sqlCommand.ExecuteScalar();
            }
        }

        private static void ExecuteNonQuery(string commandText, string connectionString)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                SqlCommand sqlCommand = connection.CreateCommand();
                sqlCommand.CommandText = commandText;
                sqlCommand.ExecuteNonQuery();
            }
        }

        private static class NestedSessionManager
        {
            internal static readonly SessionManager SessionManager = new SessionManager();
        }
    }
}