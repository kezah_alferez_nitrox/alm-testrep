using System;
using System.Web;
using NHibernate;
using NHibernate.Context;

namespace Infrastructure
{
    public class SessionPerRequestHttpModule : IHttpModule
    {
        public void Dispose()
        {
        }

        public void Init(HttpApplication context)
        {
            context.BeginRequest += this.ContextBeginRequest;
            context.EndRequest += this.ContextEndRequest;
        }

        public void ContextBeginRequest(Object sender, EventArgs e)
        {
            ManagedWebSessionContext.Bind(HttpContext.Current, SessionManager.SessionFactory.OpenSession());
        }

        public void ContextEndRequest(Object sender, EventArgs e)
        {
            ISession session = ManagedWebSessionContext.Unbind(
                HttpContext.Current, SessionManager.SessionFactory);

            if (session != null)
            {
                if (session.Transaction != null && session.Transaction.IsActive)
                {
                    session.Transaction.Rollback();
                }
                else
                {
                    session.Flush();
                }

                session.Close();
            }
        }
    }
}