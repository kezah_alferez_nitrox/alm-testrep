﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using Infrastructure;
using NHibernate.Expression;
using PortofolioAnalysis.Common.Entities;
using ProtoBuf;

public partial class GetSecurities : Page
{
    private static readonly DateTime MinSQLDate = new DateTime(1753,1,1);

    public override void ProcessRequest(HttpContext context)
    {
        SendSecurities(context);
    }

    private static void SendSecurities(HttpContext context)
    {
        DateTime since = GetDate(context);
        IList<Security> securities = RetreiveSecurities(since);
        WriteSecuritiesToResponse(context.Response, securities);
    }

    private static DateTime GetDate(HttpContext context)
    {
        string header = context.Request.QueryString["since"];

        if (!string.IsNullOrEmpty(header))
        {
            long ticks;
            if (long.TryParse(header, out ticks))
            {
                return new DateTime(ticks);
            }
        }

        return MinSQLDate;
    }

    private static IList<Security> RetreiveSecurities(DateTime since)
    {
        IList<Security> securities = SessionManager.CurrentSession.CreateCriteria(typeof (Security)).
            Add(Expression.Eq("ModifiedDate", since)).List<Security>();

        securities = new List<Security>
                          {
                              new Security {Name = "test 1", Cpn = 3.14},
                              new Security {Name = "test 2", Cpn = 3.17},
                          };

        return securities;
    }

    private static void WriteSecuritiesToResponse(HttpResponse response, IList<Security> securities)
    {
        response.Clear();
        Serializer.Serialize(response.OutputStream, securities);
    }
}