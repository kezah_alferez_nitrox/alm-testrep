using System;
using ALMS.Products;
using Castle.ActiveRecord;
using Eu.AlterSystems.ASNetLib.Core;

namespace PortofolioAnalysis.Common.DatabaseEntities
{
    [ActiveRecord]
    public class Security : EntityBase
    {
        [PrimaryKey(PrimaryKeyType.Assigned)]
        public string Id { get; set; }

        public string IdCussip
        {
            get { return this.Id; }
            set { this.Id = value; }
        }

        [Property]
        public DateTime ModifiedDate { get; set; }

        [Property(Index = "IDX_ISIN")]
        public string ISIN { get; set; }

        [Property]
        public string Description { get; set; }

        [Property]
        public double AmountIssued { get; set; }

        [Property]
        public double AmountOutstanding { get; set; }

        [Property]
        public double Factor { get; set; }

        [Property]
        public Rating RatingMoodys { get; set; }

        [Property]
        public Rating RatingSandP { get; set; }

        [Property]
        public Rating RatingFitch { get; set; }

        [Property]
        public double Coupon { get; set; }

        [Property]
        public int CouponFrequency { get; set; }

        [Property]
        public CouponType CouponType { get; set; }

        [Property]
        public DateTime IssueDate { get; set; }

        [Property]
        public DateTime FirstCouponDate { get; set; }

        [Property]
        public DateTime MaturityDate { get; set; }

        [Property]
        public string CollateralType { get; set; }

        [Property]
        public string IndustrySector { get; set; }

        [Property]
        public string SpreadToDiscMarginIndex { get; set; }

        [Property]
        public VariableCouponType? VariableCouponType { get; set; }

        [Property]
        public string ResetIndex { get; set; }

        [Property]
        public double Spread { get; set; }

        [Property]
        public DateTime? ResetDate { get; set; }

        [Property]
        public int? FloatingFrequency { get; set; }

        [Property]
        public DayConvention DayConvention { get; set; }

        public Security()
        {
            this.ModifiedDate =
                this.IssueDate =
                this.MaturityDate =
                this.FirstCouponDate = Constants.MinSqlDate;
        }

        public string LongDescription
        {
            get { return string.Format("{0}-{1}-{2}", this.IdCussip, this.ISIN, this.Description); }
        }

        public override string ToString()
        {
            return LongDescription;
        }

        public CouponBasis GetCouponBasis()
        {
            switch (DayConvention)
            {
                case DayConvention.ActAct:
                //case DayConvention.ActActNonEom:
                    return CouponBasis.ACTACT;
                case DayConvention.Act360:
                //case DayConvention.Act360NonEom:
                    return CouponBasis.ACT360;
                case DayConvention.Act365:
                //case DayConvention.Act365NonEom:
                    return CouponBasis.ACT365;
                case DayConvention.US30360:
                //case DayConvention.A30360NonEom:
                case DayConvention.EU30360:
                //case DayConvention.Isma30360NonEom:
                    return CouponBasis.ISMA30360;
                default:
                    return CouponBasis.ACT360;
            }
        }
    }

    public enum Rating
    {
        [EnumDescription("AAA")]
        AAA,
        [EnumDescription("AA+")]
        AAPlus,
        [EnumDescription("AA")]
        AA,
        [EnumDescription("AA-")]
        AAMinus,
        [EnumDescription("A+")]
        APlus,
        [EnumDescription("A")]
        A,
        [EnumDescription("A-")]
        AMinus,
        [EnumDescription("BBB+")]
        BBBPlus,
        [EnumDescription("BBB")]
        BBB,
        [EnumDescription("BBB-")]
        BBBMinus,
        [EnumDescription("BB+")]
        BBPlus,
        [EnumDescription("BB")]
        BB,
        [EnumDescription("BB-")]
        BBMinus,
        [EnumDescription("B+")]
        BPlus,
        [EnumDescription("B")]
        B,
        [EnumDescription("B-")]
        BMinus,
        [EnumDescription("CCC+")]
        CCCPlus,
        [EnumDescription("CCC")]
        CCC,
        [EnumDescription("CCC-")]
        CCCMinus,
        [EnumDescription("CC")]
        CC,
        [EnumDescription("C")]
        C,
        [EnumDescription("D")]
        D,
        [EnumDescription("NR")]
        NR,
        [EnumDescription("N/A")]
        NA,
    }

    public enum GivenType
    {
        [EnumDescription("DM")]
        Dm,
        [EnumDescription("Price")]
        Price,
        [EnumDescription("Yield")]
        Yield
    }

    public enum IndexType
    {
        [EnumDescription("3 Mo LIBOR")]
        Libor3Mo,
        [EnumDescription("3 Mo EuriBOR")]
        EuriBor3Mo,
        [EnumDescription("1 Mo LIBOR")]
        Libor1Mo,
        [EnumDescription("1 Mo EuriBOR")]
        EuriBor1Mo,
        [EnumDescription("US Prime")]
        UsPrime,
    }

    public enum CouponType
    {
        Fixed,
        Floating,
        Variable,
    }

    public enum VariableCouponType
    {
        FixedToFloat,
    }

    public enum DayConvention
    {
        [EnumDescription("ACT/ACT")]
        ActAct,
        //[EnumDescription("ACT/ACT NON-EOM")]
        //ActActNonEom,
        [EnumDescription("ACT/360")]
        Act360,
        //[EnumDescription("ACT/360 NON-EOM")]
        //Act360NonEom,
        [EnumDescription("ACT/365")]
        Act365,
        //[EnumDescription("ACT/365 NON-EOM")]
        //Act365NonEom,
        [EnumDescription("30/360")]
        US30360,
        //[EnumDescription("30/360 NON-EOM")]
        //A30360NonEom,
        [EnumDescription("ISMA-30/360")]
        EU30360,
        //[EnumDescription("ISMA-30/360 NON-EOM")]
        //Isma30360NonEom,
    }
}