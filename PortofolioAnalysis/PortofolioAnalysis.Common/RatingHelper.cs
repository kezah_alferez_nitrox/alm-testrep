using System;
using System.Collections;
using System.Collections.Generic;
using Eu.AlterSystems.ASNetLib.Core;
using PortofolioAnalysis.Common.DatabaseEntities;

namespace PortofolioAnalysis.Common
{
    public static class RatingHelper
    {
        private static readonly IDictionary<string, Rating> textToRating = new Dictionary<string, Rating>();

        private static readonly IDictionary<string, string> moodysToSandP = new Dictionary<string, string>
                                                                   {
                                                                       {"Aaa", "AAA"},
                                                                       {"Aa1", "AA+"},
                                                                       {"Aa2", "AA"},
                                                                       {"Aa3", "AA-"},
                                                                       {"A1", "A+"},
                                                                       {"A2", "A"},
                                                                       {"A3", "A-"},
                                                                       {"Baa1", "BBB+"},
                                                                       {"Baa2", "BBB"},
                                                                       {"Baa3", "BBB-"},
                                                                       {"Ba1", "BB+"},
                                                                       {"Ba2", "BB"},
                                                                       {"Ba3", "BB-"},
                                                                       {"B1", "B+"},
                                                                       {"B2", "B"},
                                                                       {"B3", "B-"},
                                                                       {"Caa1", "CCC+"},
                                                                       {"Caa2", "CCC"},
                                                                       {"Caa3", "CCC-"},
                                                                       {"Ca", "CC"},
                                                                       {"C", "D"},
                                                                   };

        static RatingHelper()
        {
            IList list = EnumHelper.ToList(typeof(Rating));
            foreach (KeyValuePair<Enum, string> pair in list)
            {
                textToRating.Add(pair.Value, (Rating) pair.Key);
            }
        }

        public static Rating ParseMoodys(string text)
        {
            string sAndP;
            if (moodysToSandP.TryGetValue(text, out sAndP))
            {
                return ParseSandP(sAndP);
            }

            return Rating.NA;
        }

        public static Rating ParseSandP(string text)
        {
            Rating rating;
            if (textToRating.TryGetValue(text, out rating))
            {
                return rating;
            }

            return Rating.NA;
        }

        public static Rating ParseFitch(string text)
        {
            if (text == "DDD" || text == "DD") return Rating.D;

            return ParseSandP(text);
        }
    }
}