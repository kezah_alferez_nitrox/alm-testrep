using System;
using System.Reflection;

namespace PortofolioAnalysis.Common
{
    public static class Util
    {
        public static void CopyProperties(object source, object destination)
        {
            if (destination == null) throw new ArgumentNullException();
            if (!destination.GetType().IsAssignableFrom(source.GetType())) throw new InvalidOperationException();

            PropertyInfo[] properties = source.GetType().GetProperties();
            foreach (PropertyInfo property in properties)
            {
                if (property.CanRead && property.CanWrite)
                {
                    object value = property.GetValue(source, null);
                    property.SetValue(destination, value, null);
                }
            }
        }
    }
}