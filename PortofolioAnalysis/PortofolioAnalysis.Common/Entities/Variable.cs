using Newtonsoft.Json;

namespace PortofolioAnalysis.Common.Entities
{
    public class Variable : EntityBase
    {
        public string Name { get; set; }
        public int Position { get; set; }
        public string Expression { get; set; }

        [JsonIgnore]
        public Variable Self
        {
            get { return this; }
        }
    }
}