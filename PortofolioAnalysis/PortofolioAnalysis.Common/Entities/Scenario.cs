
using Newtonsoft.Json;

namespace PortofolioAnalysis.Common.Entities
{
    public class Scenario : EntityBase
    {
        public string Name { get; set; }

        public int Position { get; set; }

        [JsonIgnore]
        public Scenario Self
        {
            get { return this; }
        }

        public override string ToString()
        {
            return this.Name;
        }
    }
}