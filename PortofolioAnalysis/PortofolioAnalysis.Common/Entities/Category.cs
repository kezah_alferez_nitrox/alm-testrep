using System;
using System.Collections.Generic;
using System.ComponentModel;
using Newtonsoft.Json;

namespace PortofolioAnalysis.Common.Entities
{
    public class Category : EntityBase
    {
        public string Name { get; set; }

        public int Position { get; set; }

        public Category Parent { get; set; }

        private BindingList<Category> categories;

        public BindingList<Category> Categories
        {
            get { return this.categories; }
            set
            {
                if (this.categories != value)
                {
                    if (this.categories != null) this.categories.ListChanged -= this.CategoriesChanged;
                    this.categories = value;
                    if (this.categories != null) this.categories.ListChanged += this.CategoriesChanged;
                }
            }
        }

        private void CategoriesChanged(object sender, ListChangedEventArgs e)
        {
            this.FirePropertyChanged("Categories");
        }

        private BindingList<PortfolioSecurity> securities;

        public BindingList<PortfolioSecurity> Securities
        {
            get { return this.securities; }
            set
            {
                if (this.securities != value)
                {
                    if (this.securities != null) this.securities.ListChanged -= this.SecuritiesChanged;
                    this.securities = value;
                    if (this.securities != null) this.securities.ListChanged += this.SecuritiesChanged;
                }
            }
        }

        private void SecuritiesChanged(object sender, ListChangedEventArgs e)
        {
            this.FirePropertyChanged("Securities");
        }

        [JsonIgnore]
        public Category Self
        {
            get { return this; }
        }

        public Category()
        {
            this.Categories = new BindingList<Category>();
            this.Securities = new BindingList<PortfolioSecurity>();
        }

        public bool RemoveSecurity(PortfolioSecurity portfolioSecurity)
        {
            if (this.Securities.Contains(portfolioSecurity))
            {
                if (this.Securities.Remove(portfolioSecurity))
                {
                    return true;
                }
            }

            foreach (Category category in this.Categories)
            {
                if (category.Securities.Contains(portfolioSecurity))
                {
                    category.Securities.Remove(portfolioSecurity);
                    return true;
                }
            }

            return false;
        }

        [JsonIgnore]
        public IList<PortfolioSecurity> AllSecurities
        {
            get
            {
                List<PortfolioSecurity> result = new List<PortfolioSecurity>();
                AddAllSecurities(result, this);
                return result;
            }
        }

        private static void AddAllSecurities(List<PortfolioSecurity> securities, Category category)
        {
            securities.AddRange(category.Securities);
            foreach (Category child in category.Categories)
            {
                AddAllSecurities(securities, child);
            }
        }
    }
}