using System;
using System.Collections.Generic;
using System.ComponentModel;
using ALMS.Products;
using Newtonsoft.Json;
using PortofolioAnalysis.Common.DatabaseEntities;
using CouponType = PortofolioAnalysis.Common.DatabaseEntities.CouponType;

namespace PortofolioAnalysis.Common.Entities
{
    public class PortfolioSecurity : Security, IProduct
    {
        private DateTime[] calendar;
        public string Amortization { get { return "1"; } }//dummy
        public int LagNoAmortisationPeriodInMonths { get { return 0; } }//dummy
        public bool Exclude { get; set; }
        public Status Status { get; set; }
        public GivenType GivenType { get; set; }
        public int IntDuration { get; set; }
        public double GivenAmount { get; set; }
        public int NumberOfSteps { get; set; }
        public double StepAmount { get; set; }
        public string UserNotes { get; set; }

        private BindingList<SecurityScenarioValues> scenarioValues;
        public BindingList<SecurityScenarioValues> ScenarioValues
        {
            get { return this.scenarioValues; }
            set
            {
                if (this.scenarioValues != value)
                {
                    if (this.scenarioValues != null) this.scenarioValues.ListChanged -= this.ScenarioValuesChanged;
                    this.scenarioValues = value;
                    if (this.scenarioValues != null) this.scenarioValues.ListChanged += this.ScenarioValuesChanged;
                }
            }
        }

        private double originalAmount;
        public double OriginalAmount
        {
            get { return this.originalAmount; }
            set
            {
                if (this.originalAmount != value)
                {
                    this.originalAmount = value;

                    this.currentAmount = this.Factor * this.OriginalAmount;
                    this.FirePropertyChanged("CurrentAmount");
                }
            }
        }
        private ProductAttrib attrib;
        public ProductAttrib Attrib
        {
            get { return this.attrib; }
            set { this.attrib = value; }
        }

        private double currentAmount;
        public double CurrentAmount
        {
            get { return this.currentAmount; }
            set
            {
                if (this.currentAmount != value)
                {
                    this.currentAmount = value;

                    if (this.Factor != 0)
                    {
                        this.originalAmount = this.CurrentAmount / this.Factor;
                        this.FirePropertyChanged("OriginalAmount");
                    }
                }
            }
        }

        private void ScenarioValuesChanged(object sender, ListChangedEventArgs e)
        {
            this.FirePropertyChanged("ScenarioValues");
        }

        [JsonIgnore]
        public DateTime[] Calendar
        {
            get { return this.calendar ?? (this.calendar = this.ComputeCalendar()); }
        }

        public Category PortfolioCategory { get; set; }
        public Category SetupCategory { get; set; }

        public PortfolioSecurity()
        {
            this.ScenarioValues = new BindingList<SecurityScenarioValues>();
        }

        private DateTime[] ComputeCalendar()
        {
            List<DateTime> dates = new List<DateTime>();

            dates.Add(this.IssueDate);
            if (this.FirstCouponDate != this.IssueDate)
            {
                dates.Add(this.FirstCouponDate);
            }

            DateTime date = this.GetNextCouponDate(this.FirstCouponDate);
            while (date < this.MaturityDate)
            {
                dates.Add(date);
                date = this.GetNextCouponDate(date);
            }

            dates.Add(this.MaturityDate);

            return dates.ToArray();
        }

        private DateTime GetNextCouponDate(DateTime date)
        {
            int frequency = this.CouponFrequency;
            if (this.CouponType == CouponType.Variable && date >= this.ResetDate)
            {
                frequency = this.FloatingFrequency.GetValueOrDefault();
            }

            return date.AddMonths(12 / frequency);
        }

        ProductAmortizationType IProduct.AmortizationType
        {
            get { return ProductAmortizationType.BULLET; }
        }

        public double GetFace(DateTime date)
        {
            return 1;
        }

        CouponBasis IProduct.CouponBasis
        {
            get { return CouponBasis.ACT360; }
        }

        string IProduct.Coupon
        {
            get { return this.Coupon.ToString(); } // todo
        }

        CouponCalcType IProduct.CouponCalcType
        {
            get { return CouponCalcType.InAdvance; }
        }

        double? IProduct.FirstCoupon
        {
            get { return this.Coupon; }
        }

        DateTime IProduct.GetExpiryDate(DateTime valuationDate)
        {
            return this.MaturityDate;
        }

        ICurrency IProduct.Currency
        {
            get { return null; }
        }

        double IProduct.Complement
        {
            get { return 0; }
        }

        ProductFrequency IProduct.Frequency
        {
            get { return (ProductFrequency)this.CouponFrequency; }
        }

        bool IProduct.IsReceived
        {
            get { return true; }
        }

        ALMS.Products.CouponType IProduct.CouponType
        {
            get
            {
                switch (CouponType)
                {
                    case CouponType.Fixed:
                        return ALMS.Products.CouponType.Fixed;
                    case CouponType.Floating:
                        return ALMS.Products.CouponType.Floating;
                    case CouponType.Variable:
                        return ALMS.Products.CouponType.Variable;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        DateTime? IProduct.IssueDate
        {
            get { return this.IssueDate; }
        }

        string IProduct.Spread
        {
            get { return this.Spread.ToString(); }
        }


        public bool IsDerivative
        {
            get { return false; }
        }
    }

    public enum Status
    {
        None = 0,
        Ok,
        No,
    }
}