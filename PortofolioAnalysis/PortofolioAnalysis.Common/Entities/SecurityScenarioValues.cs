﻿namespace PortofolioAnalysis.Common.Entities
{
    public class SecurityScenarioValues : EntityBase
    {
        public string Prepay { get; set; }
        public string Default { get; set; }
        public string Severity { get; set; }
        public bool PrepayValueDefined { get; set; }
        public bool DefaultValueDefined { get; set; }
        public bool SeverityValueDefined { get; set; }
    }
}