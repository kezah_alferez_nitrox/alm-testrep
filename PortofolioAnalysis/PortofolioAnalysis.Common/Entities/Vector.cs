using System;
using System.Collections.Generic;
using System.ComponentModel;
using ALMS.Products;
using Eu.AlterSystems.ASNetLib.Core;
using Newtonsoft.Json;

namespace PortofolioAnalysis.Common.Entities
{
    public class Vector : EntityBase, IFundamentalVector
    {
        public string Name { get; set; }
        public int Position { get; set; }

        private BindingList<double> values;
        public BindingList<double> Values
        {
            get { return this.values; }
            private set
            {
                if (this.values != null) this.values.ListChanged -= this.ValuesListChanged;
                this.values = value;
                if (this.values != null) this.values.ListChanged += this.ValuesListChanged;
            }
        }

        private void ValuesListChanged(object sender, ListChangedEventArgs e)
        {
            this.FirePropertyChanged("Values");
        }

        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public ProductFrequency Frequency { get; set; }

        public bool IsFundametal { get; set; }

        #region Implementation of IFundamentalVector

        public string VariableName
        {
            get { return this.Name; }
        }

        public TimeUnit Unit {get; set; }

        public int Delta { get; set; }

        public CouponBasis DateConvention
        {   get { return CouponBasis.ACT365; }
        }

        #region Implementation of IFundamentalVector

        public double[] VolatilityValues
        {
            get { return null; }
        }

        #endregion

        #endregion


        [DoNotNotify]
        [JsonIgnore]
        public DateTime[] Dates { get; private set; }

        public void GenerateDates()
        {
            if (this.Frequency == 0 || this.StartDate > this.EndDate)
            {
                Dates = new DateTime[0];
                return;
            }

            List<DateTime> dates = new List<DateTime>();

            DateTime date = this.StartDate;
            while (date <= this.EndDate)
            {
                dates.Add(date);
                date = date.AddMonths((int)this.Frequency);
            }

            this.Dates = dates.ToArray();
        }

        [JsonIgnore]
        public Vector Self
        {
            get { return this; }
        }

        public Vector()
        {
            this.Values = new BindingList<double>();
            this.StartDate = this.EndDate = Constants.MinSqlDate;
        }

        public Vector Clone()
        {
            Vector clone = new Vector();
            Util.CopyProperties(this, clone);

            double[] valueArray = new double[this.Values.Count];
            this.Values.CopyTo(valueArray, 0);
            clone.Values = new BindingList<double>(new List<double>(valueArray));

            return clone;
        }

        public double GetValueAtDate(DateTime date)
        {
            if (Values.Count == 0) return double.NaN;

            if (date <= this.StartDate) return Values[0];
            if (date >= this.EndDate) return Values[Values.Count - 1];

            double value = Values[0];
            int i = 1;
            while (i < this.Dates.Length && this.Dates[i] < date)
            {
                value = Values[i];
                i++;
            }

            return value;
        }


        public FundamentalVectorType Type { get; set; }
    }
}