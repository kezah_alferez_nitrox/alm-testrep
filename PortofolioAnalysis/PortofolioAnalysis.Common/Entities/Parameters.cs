using System.Collections.Generic;
using Eu.AlterSystems.ASNetLib.Core;

namespace PortofolioAnalysis.Common.Entities
{
    public class Parameters : EntityBase
    {
        private static readonly Dictionary<DateFormat, string> DateFormats = new Dictionary<DateFormat, string>
                                                                                 {
                                                                                     {DateFormat.Fr, "dd/MM/yyyy"},
                                                                                     {DateFormat.Us, "MM/dd/yyyy"},
                                                                                 };


        public DateFormat DateFormat { get; set; }

        public string DateFormatString
        {
            get { return DateFormats[this.DateFormat]; }
        }
    }

    public enum DateFormat
    {
        [EnumDescription("French (dd/mm/yyyy)")] Fr,
        [EnumDescription("American (mm/dd/yyyy)")] Us
    }
}