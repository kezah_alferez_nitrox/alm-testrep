using System;

namespace PortofolioAnalysis.Common
{
    public static class Constants
    {
        public static readonly DateTime MinSqlDate = new DateTime(1753, 1, 1);
    }
}