using System;

namespace PortofolioAnalysis.Common.Infrastructure
{
    public interface IGenericEntity<TIdentity> : IEquatable<IGenericEntity<TIdentity>>
    {
        TIdentity Id { get; }
    }
}