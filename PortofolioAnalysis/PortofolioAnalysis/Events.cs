﻿using PortfolioAnalysis.Infrastructure;
using PortofolioAnalysis.Common.Entities;

namespace PortfolioAnalysis
{
    public class WorkspaceLoadedEvent { }

    public class PortfolioChangedEvent { }

    public class CalculateEvent { }

    public class SecurityChangedEvent
    {
        public PortfolioSecurity Security { get; set; }
    }

    public class WorkspaceFileNameChangedEvent { }

    public class WorkspaceDirtyChangedEvent { }

    public class DateFormatChangedEvent { }
}