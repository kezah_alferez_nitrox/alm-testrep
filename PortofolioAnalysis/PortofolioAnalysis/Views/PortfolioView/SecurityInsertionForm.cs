﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ALMSCommon;
using NHibernate;
using PortofolioAnalysis.Common.DatabaseEntities;
using Expression = NHibernate.Expression.Expression;

namespace PortfolioAnalysis.Views.PortfolioView
{
    public partial class SecurityInsertionForm : Form
    {
        public SecurityInsertionForm()
        {
            InitializeComponent();
        }

        private void GridKeyDown(object sender, KeyEventArgs e)
        {
            const string cusipCaracters = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

            if (e.Control && e.KeyCode == Keys.V)
            {
                string text = Clipboard.GetText();
                List<string> listCusip = new List<string>();
                int index = 0;
                bool ok = true;
                while (ok && index < text.Length)
                {
                    char car = text[index];
                    if (cusipCaracters.Contains(car.ToString()) && index + 9 <= text.Length)
                    {
                        for (int i = 0; i < 9 && ok; i++)
                        {
                            car = text[index + i];
                            if (!cusipCaracters.Contains(car.ToString())) ok = false;
                        }
                        if (ok) listCusip.Add(text.Substring(index, 9));
                        index += 9;
                    }
                    else index++;
                }

                if (ok)
                {
                    foreach (string cusip in listCusip)
                    {
                        IList<Security> securities = this.FindSecurities(cusip);

                        if (securities.Count == 0)
                        {
                            DataGridViewRow row = new DataGridViewRow();
                            row.CreateCells(gridSecurities);
                            row.Cells[colCusip.Index].Value = cusip;
                            row.Cells[colOk.Index].Value = false;
                            row.Cells[colOk.Index].ReadOnly = true;
                            gridSecurities.Rows.Add(row);
                        }
                        else
                        {
                            foreach (Security security in securities)
                            {
                                DataGridViewRow row = new DataGridViewRow();
                                row.CreateCells(gridSecurities);
                                row.Cells[colCusip.Index].Value = security.IdCussip;
                                row.Cells[colIsin.Index].Value = security.ISIN;
                                row.Cells[colDescription.Index].Value = security.Description;
                                row.Cells[colOk.Index].Value = true;
                                row.Tag = security;

                                gridSecurities.Rows.Add(row);
                            }
                        }
                    }
                }
                else Dialogs.Warning("Paste", "You must copy a list of CUSIP from the clipboard");
            }
        }

        private IList<Security> FindSecurities(string cusip)
        {
            IList<Security> securities = new List<Security>();

            if (!string.IsNullOrEmpty(cusip))
            {
                using (ISession session = Context.Database.OpenSession())
                using (ITransaction transaction = session.BeginTransaction())
                {
                    securities = session.CreateCriteria(typeof(Security)).
                        Add(Expression.Eq("Id", cusip)).List<Security>();

                    transaction.Commit();
                }
            }

            return securities;
        }

        public List<Security> GetSelectedSecurities()
        {
            List<Security> securities = new List<Security>();
            foreach (DataGridViewRow row in gridSecurities.Rows)
            {
                object rowChecked = row.Cells[colOk.Index].Value;
                if (rowChecked is bool && (bool)rowChecked && row.Tag is Security) securities.Add((Security)row.Tag);
            }
            return securities;
        }
    }
}
