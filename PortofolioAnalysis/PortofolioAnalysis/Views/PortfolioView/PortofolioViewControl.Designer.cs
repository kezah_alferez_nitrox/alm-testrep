﻿using System.Windows.Forms;
using PortfolioAnalysis.Controls;

namespace PortfolioAnalysis.Views.PortfolioView
{
    partial class PortfolioViewControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.renamePortfolioTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.treeViewCategory = new PortfolioAnalysis.Controls.CategoryTreeView();
            this.categoryContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.addACategoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.renameCategoryToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteCategoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.portfolioListBox = new PortfolioAnalysis.Controls.PortfolioListBoxDragNDrop();
            this.portfolioContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.addAPortfolioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.renamePortfolioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deletePortfolioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addNewCategoryButton = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.addNewportfolioButton = new System.Windows.Forms.Button();
            this.dataGridView = new PortfolioAnalysis.Controls.SecurityDataGridView();
            this.securityContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.deleteSecurityToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lbCurrentCategory = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.searchSecurityButton = new System.Windows.Forms.Button();
            this.searchTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.insertMultipleLinesButton = new System.Windows.Forms.Button();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.categoryContextMenuStrip.SuspendLayout();
            this.portfolioContextMenuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            this.securityContextMenuStrip.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.renamePortfolioTextBox);
            this.splitContainer1.Panel1.Controls.Add(this.label3);
            this.splitContainer1.Panel1.Controls.Add(this.treeViewCategory);
            this.splitContainer1.Panel1.Controls.Add(this.portfolioListBox);
            this.splitContainer1.Panel1.Controls.Add(this.addNewCategoryButton);
            this.splitContainer1.Panel1.Controls.Add(this.button2);
            this.splitContainer1.Panel1.Controls.Add(this.addNewportfolioButton);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.dataGridView);
            this.splitContainer1.Panel2.Controls.Add(this.panel1);
            this.splitContainer1.Size = new System.Drawing.Size(905, 519);
            this.splitContainer1.SplitterDistance = 204;
            this.splitContainer1.TabIndex = 0;
            // 
            // renamePortfolioTextBox
            // 
            this.renamePortfolioTextBox.Location = new System.Drawing.Point(99, 14);
            this.renamePortfolioTextBox.Name = "renamePortfolioTextBox";
            this.renamePortfolioTextBox.Size = new System.Drawing.Size(10, 20);
            this.renamePortfolioTextBox.TabIndex = 10;
            this.renamePortfolioTextBox.Visible = false;
            this.renamePortfolioTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.RenamePortfolioTextBoxKeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 14);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Portfolios";
            // 
            // treeViewCategory
            // 
            this.treeViewCategory.AllowDrop = true;
            this.treeViewCategory.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.treeViewCategory.CanOrganize = true;
            this.treeViewCategory.ContextMenuStrip = this.categoryContextMenuStrip;
            this.treeViewCategory.CurrentCategory = null;
            this.treeViewCategory.Location = new System.Drawing.Point(12, 241);
            this.treeViewCategory.Name = "treeViewCategory";
            this.treeViewCategory.Portfolio = null;
            this.treeViewCategory.Size = new System.Drawing.Size(184, 210);
            this.treeViewCategory.TabIndex = 0;
            this.treeViewCategory.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.TreeViewCategoryAfterSelect);
            // 
            // categoryContextMenuStrip
            // 
            this.categoryContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addACategoryToolStripMenuItem,
            this.renameCategoryToolStripMenuItem1,
            this.deleteCategoryToolStripMenuItem});
            this.categoryContextMenuStrip.Name = "categoryContextMenuStrip";
            this.categoryContextMenuStrip.Size = new System.Drawing.Size(159, 70);
            // 
            // addACategoryToolStripMenuItem
            // 
            this.addACategoryToolStripMenuItem.Name = "addACategoryToolStripMenuItem";
            this.addACategoryToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.addACategoryToolStripMenuItem.Text = "Add a category";
            this.addACategoryToolStripMenuItem.Click += new System.EventHandler(this.AddACategoryToolStripMenuItemClick);
            // 
            // renameCategoryToolStripMenuItem1
            // 
            this.renameCategoryToolStripMenuItem1.Name = "renameCategoryToolStripMenuItem1";
            this.renameCategoryToolStripMenuItem1.ShortcutKeys = System.Windows.Forms.Keys.F2;
            this.renameCategoryToolStripMenuItem1.Size = new System.Drawing.Size(158, 22);
            this.renameCategoryToolStripMenuItem1.Text = "Rename";
            this.renameCategoryToolStripMenuItem1.Click += new System.EventHandler(this.RenameCategoryToolStripMenuItem1Click);
            // 
            // deleteCategoryToolStripMenuItem
            // 
            this.deleteCategoryToolStripMenuItem.Name = "deleteCategoryToolStripMenuItem";
            this.deleteCategoryToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Delete)));
            this.deleteCategoryToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.deleteCategoryToolStripMenuItem.Text = "Delete";
            this.deleteCategoryToolStripMenuItem.Click += new System.EventHandler(this.DeleteCategoryToolStripMenuItemClick);
            // 
            // portfolioListBox
            // 
            this.portfolioListBox.AllowDrop = true;
            this.portfolioListBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.portfolioListBox.ContextMenuStrip = this.portfolioContextMenuStrip;
            this.portfolioListBox.DisplayMember = "Name";
            this.portfolioListBox.FormattingEnabled = true;
            this.portfolioListBox.Location = new System.Drawing.Point(12, 35);
            this.portfolioListBox.Name = "portfolioListBox";
            this.portfolioListBox.Size = new System.Drawing.Size(184, 147);
            this.portfolioListBox.TabIndex = 8;
            this.portfolioListBox.ValueMember = "Self";
            this.portfolioListBox.SelectedIndexChanged += new System.EventHandler(this.PortfolioListBoxSelectedIndexChanged);
            // 
            // portfolioContextMenuStrip
            // 
            this.portfolioContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addAPortfolioToolStripMenuItem,
            this.renamePortfolioToolStripMenuItem,
            this.deletePortfolioToolStripMenuItem});
            this.portfolioContextMenuStrip.Name = "contextMenuStrip1";
            this.portfolioContextMenuStrip.Size = new System.Drawing.Size(159, 70);
            // 
            // addAPortfolioToolStripMenuItem
            // 
            this.addAPortfolioToolStripMenuItem.Name = "addAPortfolioToolStripMenuItem";
            this.addAPortfolioToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.addAPortfolioToolStripMenuItem.Text = "Add a portfolio";
            this.addAPortfolioToolStripMenuItem.Click += new System.EventHandler(this.AddAPortfolioToolStripMenuItemClick);
            // 
            // renamePortfolioToolStripMenuItem
            // 
            this.renamePortfolioToolStripMenuItem.Name = "renamePortfolioToolStripMenuItem";
            this.renamePortfolioToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F2;
            this.renamePortfolioToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.renamePortfolioToolStripMenuItem.Text = "Rename";
            this.renamePortfolioToolStripMenuItem.Click += new System.EventHandler(this.RenamePortfolioToolStripMenuItemClick);
            // 
            // deletePortfolioToolStripMenuItem
            // 
            this.deletePortfolioToolStripMenuItem.Name = "deletePortfolioToolStripMenuItem";
            this.deletePortfolioToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Delete)));
            this.deletePortfolioToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.deletePortfolioToolStripMenuItem.Text = "Delete";
            this.deletePortfolioToolStripMenuItem.Click += new System.EventHandler(this.DeletePortfolioToolStripMenuItemClick);
            // 
            // addNewCategoryButton
            // 
            this.addNewCategoryButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.addNewCategoryButton.Location = new System.Drawing.Point(12, 469);
            this.addNewCategoryButton.Name = "addNewCategoryButton";
            this.addNewCategoryButton.Size = new System.Drawing.Size(184, 23);
            this.addNewCategoryButton.TabIndex = 7;
            this.addNewCategoryButton.Text = "Add New Category";
            this.addNewCategoryButton.UseVisualStyleBackColor = true;
            this.addNewCategoryButton.Click += new System.EventHandler(this.AddNewCategoryButtonClick);
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.Location = new System.Drawing.Point(126, 199);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(70, 23);
            this.button2.TabIndex = 6;
            this.button2.Text = "Save";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // addNewportfolioButton
            // 
            this.addNewportfolioButton.Location = new System.Drawing.Point(12, 199);
            this.addNewportfolioButton.Name = "addNewportfolioButton";
            this.addNewportfolioButton.Size = new System.Drawing.Size(70, 23);
            this.addNewportfolioButton.TabIndex = 5;
            this.addNewportfolioButton.Text = "Add new";
            this.addNewportfolioButton.UseVisualStyleBackColor = true;
            this.addNewportfolioButton.Click += new System.EventHandler(this.AddNewPortfolioButtonClick);
            // 
            // dataGridView
            // 
            this.dataGridView.AllowUserToAddRows = false;
            this.dataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridView.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.ContextMenuStrip = this.securityContextMenuStrip;
            this.dataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dataGridView.Location = new System.Drawing.Point(0, 70);
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.Size = new System.Drawing.Size(695, 447);
            this.dataGridView.TabIndex = 0;
            this.dataGridView.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DataGridViewKeyDown);
            // 
            // securityContextMenuStrip
            // 
            this.securityContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deleteSecurityToolStripMenuItem});
            this.securityContextMenuStrip.Name = "securityContextMenuStrip";
            this.securityContextMenuStrip.Size = new System.Drawing.Size(203, 26);
            this.securityContextMenuStrip.Opening += new System.ComponentModel.CancelEventHandler(this.SecurityContextMenuStripOpening);
            // 
            // deleteSecurityToolStripMenuItem
            // 
            this.deleteSecurityToolStripMenuItem.Name = "deleteSecurityToolStripMenuItem";
            this.deleteSecurityToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Delete)));
            this.deleteSecurityToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
            this.deleteSecurityToolStripMenuItem.Text = "Delete security";
            this.deleteSecurityToolStripMenuItem.Click += new System.EventHandler(this.DeleteSecurityToolStripMenuItemClick);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.insertMultipleLinesButton);
            this.panel1.Controls.Add(this.lbCurrentCategory);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.searchSecurityButton);
            this.panel1.Controls.Add(this.searchTextBox);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(695, 70);
            this.panel1.TabIndex = 0;
            // 
            // lbCurrentCategory
            // 
            this.lbCurrentCategory.AutoSize = true;
            this.lbCurrentCategory.Location = new System.Drawing.Point(171, 47);
            this.lbCurrentCategory.Name = "lbCurrentCategory";
            this.lbCurrentCategory.Size = new System.Drawing.Size(125, 13);
            this.lbCurrentCategory.TabIndex = 4;
            this.lbCurrentCategory.Text = "Selected category in tree";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(109, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Category :";
            // 
            // searchSecurityButton
            // 
            this.searchSecurityButton.Location = new System.Drawing.Point(508, 10);
            this.searchSecurityButton.Name = "searchSecurityButton";
            this.searchSecurityButton.Size = new System.Drawing.Size(40, 20);
            this.searchSecurityButton.TabIndex = 2;
            this.searchSecurityButton.Text = "OK";
            this.searchSecurityButton.UseVisualStyleBackColor = true;
            this.searchSecurityButton.Click += new System.EventHandler(this.SearchSecurityButtonClick);
            // 
            // searchTextBox
            // 
            this.searchTextBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.searchTextBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.searchTextBox.Location = new System.Drawing.Point(112, 10);
            this.searchTextBox.Name = "searchTextBox";
            this.searchTextBox.Size = new System.Drawing.Size(390, 20);
            this.searchTextBox.TabIndex = 1;
            this.searchTextBox.TextChanged += new System.EventHandler(this.SearchTextBoxTextChanged);
            this.searchTextBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.SearchTextBoxKeyUp);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(93, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Search by CUSIP:";
            // 
            // insertMultipleLinesButton
            // 
            this.insertMultipleLinesButton.Location = new System.Drawing.Point(554, 10);
            this.insertMultipleLinesButton.Name = "insertMultipleLinesButton";
            this.insertMultipleLinesButton.Size = new System.Drawing.Size(48, 20);
            this.insertMultipleLinesButton.TabIndex = 5;
            this.insertMultipleLinesButton.Text = "Insert";
            this.insertMultipleLinesButton.UseVisualStyleBackColor = true;
            this.insertMultipleLinesButton.Click += new System.EventHandler(this.InsertMultipleLines);
            // 
            // PortfolioViewControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainer1);
            this.Name = "PortfolioViewControl";
            this.Size = new System.Drawing.Size(905, 519);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.categoryContextMenuStrip.ResumeLayout(false);
            this.portfolioContextMenuStrip.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            this.securityContextMenuStrip.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private CategoryTreeView treeViewCategory;
        private System.Windows.Forms.Button addNewCategoryButton;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button addNewportfolioButton;
        private SecurityDataGridView dataGridView;
        private PortfolioListBoxDragNDrop portfolioListBox;
        private System.Windows.Forms.Button searchSecurityButton;
        private System.Windows.Forms.TextBox searchTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lbCurrentCategory;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ContextMenuStrip portfolioContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem addAPortfolioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem renamePortfolioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deletePortfolioToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip categoryContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem addACategoryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem renameCategoryToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem deleteCategoryToolStripMenuItem;
        private System.Windows.Forms.TextBox renamePortfolioTextBox;
        private ContextMenuStrip securityContextMenuStrip;
        private ToolStripMenuItem deleteSecurityToolStripMenuItem;
        private Button insertMultipleLinesButton;
    }
}
