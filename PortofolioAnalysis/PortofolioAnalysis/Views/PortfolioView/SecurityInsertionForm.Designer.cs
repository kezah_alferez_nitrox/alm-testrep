﻿namespace PortfolioAnalysis.Views.PortfolioView
{
    partial class SecurityInsertionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gridSecurities = new PortfolioAnalysis.Controls.SecurityDataGridView();
            this.colCusip = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colIsin = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colOk = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.gridSecurities)).BeginInit();
            this.SuspendLayout();
            // 
            // gridSecurities
            // 
            this.gridSecurities.AllowUserToAddRows = false;
            this.gridSecurities.AllowUserToDeleteRows = false;
            this.gridSecurities.AllowUserToResizeColumns = false;
            this.gridSecurities.AllowUserToResizeRows = false;
            this.gridSecurities.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridSecurities.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.gridSecurities.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.gridSecurities.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridSecurities.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colCusip,
            this.colIsin,
            this.colDescription,
            this.colOk});
            this.gridSecurities.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.gridSecurities.Location = new System.Drawing.Point(12, 12);
            this.gridSecurities.Name = "gridSecurities";
            this.gridSecurities.RowHeadersVisible = false;
            this.gridSecurities.Size = new System.Drawing.Size(447, 155);
            this.gridSecurities.TabIndex = 1;
            this.gridSecurities.KeyDown += new System.Windows.Forms.KeyEventHandler(this.GridKeyDown);
            // 
            // colCusip
            // 
            this.colCusip.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.colCusip.HeaderText = "CUSIP";
            this.colCusip.Name = "colCusip";
            this.colCusip.ReadOnly = true;
            this.colCusip.Width = 64;
            // 
            // colIsin
            // 
            this.colIsin.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.colIsin.HeaderText = "ISIN";
            this.colIsin.Name = "colIsin";
            this.colIsin.ReadOnly = true;
            this.colIsin.Width = 53;
            // 
            // colDescription
            // 
            this.colDescription.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colDescription.HeaderText = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.ReadOnly = true;
            // 
            // colOk
            // 
            this.colOk.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.colOk.HeaderText = "Ok";
            this.colOk.Name = "colOk";
            this.colOk.Width = 27;
            // 
            // btnAdd
            // 
            this.btnAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAdd.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnAdd.Location = new System.Drawing.Point(353, 173);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(106, 23);
            this.btnAdd.TabIndex = 2;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(241, 173);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(106, 23);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // SecurityInsertionForm
            // 
            this.AcceptButton = this.btnAdd;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(471, 208);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.gridSecurities);
            this.KeyPreview = true;
            this.Name = "SecurityInsertionForm";
            this.Text = "Security insertion";
            ((System.ComponentModel.ISupportInitialize)(this.gridSecurities)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Controls.SecurityDataGridView gridSecurities;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCusip;
        private System.Windows.Forms.DataGridViewTextBoxColumn colIsin;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDescription;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colOk;

    }
}