﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using PortofolioAnalysis.Common.DatabaseEntities;

namespace PortfolioAnalysis.Views.PortfolioView
{
    public partial class SearchAutoCompleteForm : Form
    {
        private readonly TextBox textBox;
        private Form parentForm;

        public SearchAutoCompleteForm(TextBox textBox)
        {
            this.textBox = textBox;

            this.InitializeComponent();

            this.textBox.ParentChanged += this.TextBoxParentChanged;
            this.textBox.KeyDown += this.TextBoxKeyDown;
        }
        void TextBoxParentChanged(object sender, EventArgs e)
        {
            if (this.parentForm != null)
            {
                this.parentForm.LocationChanged -= this.ParentFormLocationChanged;
            }
            this.parentForm = textBox.FindForm();
            if (this.parentForm != null)
            {
                this.parentForm.LocationChanged += this.ParentFormLocationChanged;
            }

        }

        private void ParentFormLocationChanged(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                this.SetLocation();
            }
        }

        public void SetDataSource(IList<Security> dataSource)
        {
            listBox.DataSource = dataSource;

            if (dataSource.Count > 0)
            {
                SetLocation();
                if (!this.Visible)
                {
                    this.Show(parentForm);
                }
                this.Size = new Size(400, dataSource.Count * listBox.ItemHeight + 4);
                textBox.Focus();
            }
            else
            {
                if (this.Visible) this.Hide();
            }
        }

        private void SetLocation()
        {
            this.Location = this.textBox.PointToScreen(new Point(0, this.textBox.Height));
        }

        void TextBoxKeyDown(object sender, KeyEventArgs e)
        {
            if (!this.Visible) return;

            if (e.KeyCode == Keys.Up)
            {
                this.Up();
                e.Handled = true;
            }
            else if (e.KeyCode == Keys.Down)
            {
                this.Down();
                e.Handled = true;
            }
            else if (e.KeyCode == Keys.Escape)
            {
                this.Hide();
                e.Handled = true;
            }
            else if (e.KeyCode == Keys.Enter)
            {
                this.SetSelected();
                e.Handled = true;
            }
        }

        private void Down()
        {
            if (this.listBox.SelectedIndex < this.listBox.Items.Count - 1)
            {
                this.listBox.SelectedIndex++;
            }
            else if (this.listBox.Items.Count > 0)
            {
                this.listBox.SelectedIndex = 0;
            }
        }

        private void Up()
        {
            if (this.listBox.SelectedIndex > 0)
            {
                this.listBox.SelectedIndex--;
            }
            else if (this.listBox.Items.Count > 0)
            {
                this.listBox.SelectedIndex = this.listBox.Items.Count - 1;
            }
        }

        private void ListBoxClick(object sender, EventArgs e)
        {
            this.SetSelected();
        }

        private void SetSelected()
        {
            Security security = this.listBox.SelectedItem as Security;
            if (security == null) return;
            this.Hide();
            this.textBox.Tag = security;
            this.textBox.Text = security.LongDescription;
        }
    }
}