﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using ALMSCommon;
using Eu.AlterSystems.ASNetLib.Core;
using NHibernate;
using NHibernate.Expression;
using PortfolioAnalysis.Controls;
using PortfolioAnalysis.Infrastructure;
using PortofolioAnalysis.Common.DatabaseEntities;
using PortofolioAnalysis.Common.Entities;
using System.Diagnostics;
using CouponType = PortofolioAnalysis.Common.DatabaseEntities.CouponType;
using Expression = NHibernate.Expression.Expression;

namespace PortfolioAnalysis.Views.PortfolioView
{
    public partial class PortfolioViewControl : UserControl, IHandle<WorkspaceLoadedEvent>, IHandle<DateFormatChangedEvent>
    {
        #region Members
        private DataGridViewCheckBoxColumn securityDataGridViewCheckBoxColumn;
        private DataGridViewImageColumn validatedDataGridViewImageColumn;
        private DataGridViewTextBoxColumn cusipDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn isinDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn securityDescriptionDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn originalAmtDescriptionDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn factorDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn currentAmountDataGridViewTextBoxColumn;
        private DataGridViewComboBoxColumn moodysSpecDataGridViewComboBoxColumn;
        private DataGridViewComboBoxColumn spSpecDataGridViewComboBoxColumn;
        private DataGridViewComboBoxColumn fitchDataGridViewComboBoxColumn;
        private DataGridViewTextBoxColumn couponDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn couponFrequencyDataGridViewTextBoxColumn;

        private DataGridViewComboBoxColumn couponTypeDataGridViewComboBoxColumn;
        private DataGridViewTextBoxColumn indexDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn spreadDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn floatingFrequencyDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn resetDateDataGridViewTextBoxColumn;

        private DataGridViewTextBoxColumn issueDatePriceDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn maturityDatePriceDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn collateralTypeDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn industrySectorDataGridViewTextBoxColumn;
        private DataGridViewComboBoxColumn givenTypeDataGridViewComboBoxColumn;
        private DataGridViewTextBoxColumn givenAmtDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn spreadToDiscMarginIndexDataGridViewComboBoxColumn;
        private DataGridViewTextBoxColumn noOfstepsDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn stepsAmtDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn userNotesDataGridViewTextBoxColumn;

        private DataGridViewColumn[] columns;
        private Category currentPortfolio;
        private Category currentCategory;

        private readonly DelayedExecuteOnce securitySearchDelayer;
        private readonly SearchAutoCompleteForm searchAutoCompleteForm;

        #endregion

        public PortfolioViewControl()
        {
            InitializeComponent();
            InitGrid();
            dataGridView.AutoGenerateColumns = false;

            securitySearchDelayer = new DelayedExecuteOnce(this.SearchSecurities, 300);

            Context.EventAggregator.Subscribe(this);

            dataGridView.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
            dataGridView.DataError += this.DataGridViewDataError;

            searchAutoCompleteForm = new SearchAutoCompleteForm(searchTextBox) { Parent = this.FindForm() };
        }

        private void DataGridViewDataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            e.Cancel = true;
            e.ThrowException = false;
            Debug.WriteLine(dataGridView.Columns[e.ColumnIndex].HeaderText + "\n" + e.Exception);
        }

        #region Init

        public void HandleMessage(WorkspaceLoadedEvent message)
        {
            this.currentPortfolio = null;
            this.currentCategory = null;

            dataGridView.DataSource = null;
            treeViewCategory.ClearAll();
            portfolioListBox.DataSource = Context.Workspace.Portfolios;
        }

        public void HandleMessage(DateFormatChangedEvent message)
        {
            DataGridViewUtil.UpdateDateFormat(this.dataGridView);
        }

        private void InitGrid()
        {
            securityDataGridViewCheckBoxColumn = new DataGridViewCheckBoxColumn();
            validatedDataGridViewImageColumn = new DataGridViewImageColumn();
            cusipDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            isinDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            securityDescriptionDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            originalAmtDescriptionDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            factorDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            currentAmountDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            moodysSpecDataGridViewComboBoxColumn = new DataGridViewComboBoxColumn();
            spSpecDataGridViewComboBoxColumn = new DataGridViewComboBoxColumn();
            fitchDataGridViewComboBoxColumn = new DataGridViewComboBoxColumn();
            couponDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();

            couponTypeDataGridViewComboBoxColumn = new DataGridViewComboBoxColumn();
            indexDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            spreadDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            floatingFrequencyDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            resetDateDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();

            couponFrequencyDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            issueDatePriceDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            maturityDatePriceDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            collateralTypeDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            industrySectorDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            givenTypeDataGridViewComboBoxColumn = new DataGridViewComboBoxColumn();
            givenAmtDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            spreadToDiscMarginIndexDataGridViewComboBoxColumn = new DataGridViewTextBoxColumn();
            noOfstepsDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            stepsAmtDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            userNotesDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();

            // 
            // securityDataGridViewCheckBoxColumn
            // 
            securityDataGridViewCheckBoxColumn.DataPropertyName = "Exclude";
            securityDataGridViewCheckBoxColumn.HeaderText = "Exclude";
            securityDataGridViewCheckBoxColumn.Name = "Exclude";
            securityDataGridViewCheckBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            securityDataGridViewCheckBoxColumn.Frozen = true;
            // 
            // validatedDataGridViewImageColumn
            // 
            validatedDataGridViewImageColumn.DataPropertyName = "Status";
            validatedDataGridViewImageColumn.HeaderText = "Validated?";
            validatedDataGridViewImageColumn.Name = "Validated";
            validatedDataGridViewImageColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            validatedDataGridViewImageColumn.Frozen = true;
            // 
            // cusipDataGridViewTextBoxColumn
            // 
            cusipDataGridViewTextBoxColumn.DataPropertyName = "Id";
            cusipDataGridViewTextBoxColumn.HeaderText = "CUSIP";
            cusipDataGridViewTextBoxColumn.Name = "CUSIP";
            cusipDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            cusipDataGridViewTextBoxColumn.Frozen = true;
            cusipDataGridViewTextBoxColumn.DefaultCellStyle.Font = new Font("Microsoft Sans Serif", 8.25F,
                                                                      FontStyle.Bold, GraphicsUnit.Point,
                                                                      0);
            cusipDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // isinDataGridViewTextBoxColumn
            // 
            isinDataGridViewTextBoxColumn.DataPropertyName = "Isin";
            isinDataGridViewTextBoxColumn.HeaderText = "ISIN";
            isinDataGridViewTextBoxColumn.Name = "ISIN";
            isinDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            isinDataGridViewTextBoxColumn.Frozen = true;
            isinDataGridViewTextBoxColumn.DefaultCellStyle.Font = new Font("Microsoft Sans Serif", 8.25F,
                                                                      FontStyle.Bold, GraphicsUnit.Point,
                                                                      0);
            isinDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // securityDescriptionDataGridViewTextBoxColumn
            // 
            securityDescriptionDataGridViewTextBoxColumn.DataPropertyName = "Description";
            securityDescriptionDataGridViewTextBoxColumn.HeaderText = "Security Description";
            securityDescriptionDataGridViewTextBoxColumn.Name = "SecurityDescription";
            securityDescriptionDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            securityDescriptionDataGridViewTextBoxColumn.Frozen = true;
            securityDescriptionDataGridViewTextBoxColumn.DefaultCellStyle.Font = new Font("Microsoft Sans Serif", 8.25F,
                                                                      FontStyle.Bold, GraphicsUnit.Point,
                                                                      0);
            securityDescriptionDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // originalAmtDescriptionDataGridViewTextBoxColumn
            // 
            originalAmtDescriptionDataGridViewTextBoxColumn.DataPropertyName = "OriginalAmount";
            originalAmtDescriptionDataGridViewTextBoxColumn.HeaderText = "Original Amount";
            originalAmtDescriptionDataGridViewTextBoxColumn.Name = "OriginalAmount";
            originalAmtDescriptionDataGridViewTextBoxColumn.DefaultCellStyle.Format = "N2";
            originalAmtDescriptionDataGridViewTextBoxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            originalAmtDescriptionDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            // 
            // factorDataGridViewTextBoxColumn
            // 
            factorDataGridViewTextBoxColumn.DataPropertyName = "Factor";
            factorDataGridViewTextBoxColumn.HeaderText = "Factor";
            factorDataGridViewTextBoxColumn.Name = "Factor";
            factorDataGridViewTextBoxColumn.DefaultCellStyle.Format = "N3";
            factorDataGridViewTextBoxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            factorDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            factorDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // currentAmountDataGridViewTextBoxColumn
            // 
            currentAmountDataGridViewTextBoxColumn.DataPropertyName = "CurrentAmount";
            currentAmountDataGridViewTextBoxColumn.HeaderText = "Current Amount";
            currentAmountDataGridViewTextBoxColumn.Name = "CurrentAmount";
            currentAmountDataGridViewTextBoxColumn.DefaultCellStyle.Format = "N2";
            currentAmountDataGridViewTextBoxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            currentAmountDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            // 
            // moodysSpecDataGridViewComboBoxColumn
            // 
            moodysSpecDataGridViewComboBoxColumn.DataPropertyName = "RatingMoodys";
            moodysSpecDataGridViewComboBoxColumn.HeaderText = "Moody's";
            moodysSpecDataGridViewComboBoxColumn.Name = "Moodys";
            moodysSpecDataGridViewComboBoxColumn.DisplayMember = "Value";
            moodysSpecDataGridViewComboBoxColumn.ValueMember = "Key";
            moodysSpecDataGridViewComboBoxColumn.DataSource = EnumHelper.ToList(typeof(Rating));
            moodysSpecDataGridViewComboBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            moodysSpecDataGridViewComboBoxColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;
            moodysSpecDataGridViewComboBoxColumn.ReadOnly = true;
            // 
            // spSpecDataGridViewComboBoxColumn
            // 
            spSpecDataGridViewComboBoxColumn.DataPropertyName = "RatingSandP";
            spSpecDataGridViewComboBoxColumn.HeaderText = "S&P";
            spSpecDataGridViewComboBoxColumn.Name = "SP";
            spSpecDataGridViewComboBoxColumn.DisplayMember = "Value";
            spSpecDataGridViewComboBoxColumn.ValueMember = "Key";
            spSpecDataGridViewComboBoxColumn.DataSource = EnumHelper.ToList(typeof(Rating));
            spSpecDataGridViewComboBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            spSpecDataGridViewComboBoxColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;
            spSpecDataGridViewComboBoxColumn.ReadOnly = true;
            // 
            // fitchDataGridViewComboBoxColumn
            // 
            fitchDataGridViewComboBoxColumn.DataPropertyName = "RatingFitch";
            fitchDataGridViewComboBoxColumn.HeaderText = "Fitch";
            fitchDataGridViewComboBoxColumn.Name = "Fitch";
            fitchDataGridViewComboBoxColumn.DisplayMember = "Value";
            fitchDataGridViewComboBoxColumn.ValueMember = "Key";
            fitchDataGridViewComboBoxColumn.DataSource = EnumHelper.ToList(typeof(Rating));
            fitchDataGridViewComboBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            fitchDataGridViewComboBoxColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;
            fitchDataGridViewComboBoxColumn.ReadOnly = true;
            // 
            // couponDataGridViewTextBoxColumn
            // 
            couponDataGridViewTextBoxColumn.DataPropertyName = "Coupon";
            couponDataGridViewTextBoxColumn.HeaderText = "Coupon";
            couponDataGridViewTextBoxColumn.Name = "Coupon";
            couponDataGridViewTextBoxColumn.DefaultCellStyle.Format = "N3";
            couponDataGridViewTextBoxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            couponDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            couponDataGridViewTextBoxColumn.ReadOnly = true;

            // 
            // couponTypeDataGridViewComboBoxColumn
            // 
            couponTypeDataGridViewComboBoxColumn.DataPropertyName = "CouponType";
            couponTypeDataGridViewComboBoxColumn.HeaderText = "Coupon Type";
            couponTypeDataGridViewComboBoxColumn.Name = "couponType";
            couponTypeDataGridViewComboBoxColumn.DisplayMember = "Value";
            couponTypeDataGridViewComboBoxColumn.ValueMember = "Key";
            couponTypeDataGridViewComboBoxColumn.DataSource = EnumHelper.ToList(typeof(CouponType));
            couponTypeDataGridViewComboBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            couponTypeDataGridViewComboBoxColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;
            couponTypeDataGridViewComboBoxColumn.ReadOnly = true;

            // 
            // indexDataGridViewTextBoxColumn
            // 
            indexDataGridViewTextBoxColumn.DataPropertyName = "ResetIndex";
            indexDataGridViewTextBoxColumn.HeaderText = "Index";
            indexDataGridViewTextBoxColumn.Name = "index";
            indexDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            couponTypeDataGridViewComboBoxColumn.ReadOnly = true;

            // 
            // spreadDataGridViewTextBoxColumn
            // 
            spreadDataGridViewTextBoxColumn.DataPropertyName = "Spread";
            spreadDataGridViewTextBoxColumn.HeaderText = "Spread";
            spreadDataGridViewTextBoxColumn.Name = "spread";
            spreadDataGridViewTextBoxColumn.DefaultCellStyle.Format = "N2";
            spreadDataGridViewTextBoxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            spreadDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            spreadDataGridViewTextBoxColumn.ReadOnly = true;

            //
            // floatingFrequencyDataGridViewTextBoxColumn
            //
            floatingFrequencyDataGridViewTextBoxColumn.DataPropertyName = "FloatingFrequency";
            floatingFrequencyDataGridViewTextBoxColumn.HeaderText = "Floating Frequency";
            floatingFrequencyDataGridViewTextBoxColumn.Name = "floatingFrequency";
            floatingFrequencyDataGridViewTextBoxColumn.DefaultCellStyle.Format = "N0";
            floatingFrequencyDataGridViewTextBoxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            floatingFrequencyDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            floatingFrequencyDataGridViewTextBoxColumn.ReadOnly = true;

            // 
            // resetDateDataGridViewTextBoxColumn
            // 
            resetDateDataGridViewTextBoxColumn.DataPropertyName = "ResetDate";
            resetDateDataGridViewTextBoxColumn.HeaderText = "Reset Date";
            resetDateDataGridViewTextBoxColumn.Name = "resetDate";
            resetDateDataGridViewTextBoxColumn.DefaultCellStyle.Format = Context.Workspace.Parameters.DateFormatString;
            resetDateDataGridViewTextBoxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            resetDateDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            resetDateDataGridViewTextBoxColumn.ValueType = typeof(DateTime);
            resetDateDataGridViewTextBoxColumn.ReadOnly = true;

            //
            // couponFrequencyDataGridViewTextBoxColumn
            //
            couponFrequencyDataGridViewTextBoxColumn.DataPropertyName = "CouponFrequency";
            couponFrequencyDataGridViewTextBoxColumn.HeaderText = "Coupon Frequency";
            couponFrequencyDataGridViewTextBoxColumn.Name = "CouponFrequency";
            couponFrequencyDataGridViewTextBoxColumn.DefaultCellStyle.Format = "N0";
            couponFrequencyDataGridViewTextBoxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            couponFrequencyDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            couponFrequencyDataGridViewTextBoxColumn.ReadOnly = true;

            // 
            // issueDatePriceDataGridViewTextBoxColumn
            // 
            issueDatePriceDataGridViewTextBoxColumn.DataPropertyName = "IssueDate";
            issueDatePriceDataGridViewTextBoxColumn.HeaderText = "Issue Date";
            issueDatePriceDataGridViewTextBoxColumn.Name = "IssueDate";
            issueDatePriceDataGridViewTextBoxColumn.DefaultCellStyle.Format = Context.Workspace.Parameters.DateFormatString;
            issueDatePriceDataGridViewTextBoxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            issueDatePriceDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            issueDatePriceDataGridViewTextBoxColumn.ValueType = typeof(DateTime);
            issueDatePriceDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // maturityDatePriceDataGridViewTextBoxColumn
            // 
            maturityDatePriceDataGridViewTextBoxColumn.DataPropertyName = "MaturityDate";
            maturityDatePriceDataGridViewTextBoxColumn.HeaderText = "Maturity Date";
            maturityDatePriceDataGridViewTextBoxColumn.Name = "MaturityDate";
            maturityDatePriceDataGridViewTextBoxColumn.DefaultCellStyle.Format = Context.Workspace.Parameters.DateFormatString;
            maturityDatePriceDataGridViewTextBoxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            maturityDatePriceDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            maturityDatePriceDataGridViewTextBoxColumn.ValueType = typeof(DateTime);
            maturityDatePriceDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // collateralTypeDataGridViewTextBoxColumn
            // 
            collateralTypeDataGridViewTextBoxColumn.DataPropertyName = "CollateralType";
            collateralTypeDataGridViewTextBoxColumn.HeaderText = "Collateral Type";
            collateralTypeDataGridViewTextBoxColumn.Name = "CollateralType";
            collateralTypeDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            collateralTypeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // industrySectorDataGridViewTextBoxColumn
            // 
            industrySectorDataGridViewTextBoxColumn.DataPropertyName = "IndustrySector";
            industrySectorDataGridViewTextBoxColumn.HeaderText = "Industry Sector";
            industrySectorDataGridViewTextBoxColumn.Name = "IndustrySector";
            industrySectorDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            industrySectorDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // givenTypeDataGridViewComboBoxColumn
            // 
            givenTypeDataGridViewComboBoxColumn.DataPropertyName = "GivenType";
            givenTypeDataGridViewComboBoxColumn.HeaderText = "Given Type";
            givenTypeDataGridViewComboBoxColumn.Name = "GivenType";
            givenTypeDataGridViewComboBoxColumn.DisplayMember = "Value";
            givenTypeDataGridViewComboBoxColumn.ValueMember = "Key";
            givenTypeDataGridViewComboBoxColumn.DataSource = EnumHelper.ToList(typeof(GivenType));
            givenTypeDataGridViewComboBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            givenTypeDataGridViewComboBoxColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;
            // 
            // givenAmtDataGridViewTextBoxColumn
            // 
            givenAmtDataGridViewTextBoxColumn.DataPropertyName = "GivenAmount";
            givenAmtDataGridViewTextBoxColumn.HeaderText = "Given Amt";
            givenAmtDataGridViewTextBoxColumn.Name = "GivenAmount";
            givenAmtDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            // 
            // spreadToDiscMarginIndexDataGridViewComboBoxColumn
            // 
            spreadToDiscMarginIndexDataGridViewComboBoxColumn.DataPropertyName = "SpreadToDiscMarginIndex";
            spreadToDiscMarginIndexDataGridViewComboBoxColumn.HeaderText = "Spread to/Disc Margin Index";
            spreadToDiscMarginIndexDataGridViewComboBoxColumn.Name = "SpreadToDiscMarginIndex";
            spreadToDiscMarginIndexDataGridViewComboBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.spreadToDiscMarginIndexDataGridViewComboBoxColumn.ValueType = typeof(string);
            // 
            // noOfstepsDataGridViewTextBoxColumn
            // 
            noOfstepsDataGridViewTextBoxColumn.DataPropertyName = "NumberOfSteps";
            noOfstepsDataGridViewTextBoxColumn.HeaderText = "# of Steps";
            noOfstepsDataGridViewTextBoxColumn.Name = "NumberOfSteps";
            noOfstepsDataGridViewTextBoxColumn.DefaultCellStyle.Format = "N0";
            noOfstepsDataGridViewTextBoxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            noOfstepsDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            // 
            // stepsAmtDataGridViewTextBoxColumn
            // 
            stepsAmtDataGridViewTextBoxColumn.DataPropertyName = "StepAmount";
            stepsAmtDataGridViewTextBoxColumn.HeaderText = "Step Amt";
            stepsAmtDataGridViewTextBoxColumn.Name = "StepAmount";
            stepsAmtDataGridViewTextBoxColumn.DefaultCellStyle.Format = "N2";
            stepsAmtDataGridViewTextBoxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            stepsAmtDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            // 
            // userNotesDataGridViewTextBoxColumn
            // 
            userNotesDataGridViewTextBoxColumn.DataPropertyName = "UserNotes";
            userNotesDataGridViewTextBoxColumn.HeaderText = "User Notes";
            userNotesDataGridViewTextBoxColumn.Name = "UserNotes";
            userNotesDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;

            //Validated?	CUSIP	ISIN	Security Description	Original Amt	Factor	Current Amount	Moody's	S&P	Fitch	
            //Coupon	Issue Date	Maturity Date	Collateral Type	Industry Sector	Given Type	Given Amt	
            //Spread to/Disc Margin Index	# of Steps	Step Amt	User Notes

            columns = new DataGridViewColumn[]
                          {
                                securityDataGridViewCheckBoxColumn,
                                validatedDataGridViewImageColumn,
                                cusipDataGridViewTextBoxColumn,
                                isinDataGridViewTextBoxColumn,
                                securityDescriptionDataGridViewTextBoxColumn,
                                originalAmtDescriptionDataGridViewTextBoxColumn,
                                factorDataGridViewTextBoxColumn,
                                currentAmountDataGridViewTextBoxColumn,
                                moodysSpecDataGridViewComboBoxColumn,
                                spSpecDataGridViewComboBoxColumn,
                                fitchDataGridViewComboBoxColumn,
                                couponDataGridViewTextBoxColumn,                                
                                couponFrequencyDataGridViewTextBoxColumn,
                                couponTypeDataGridViewComboBoxColumn,
                                indexDataGridViewTextBoxColumn,
                                spreadDataGridViewTextBoxColumn,
                                floatingFrequencyDataGridViewTextBoxColumn,
                                resetDateDataGridViewTextBoxColumn,
                                issueDatePriceDataGridViewTextBoxColumn,
                                maturityDatePriceDataGridViewTextBoxColumn,
                                collateralTypeDataGridViewTextBoxColumn,
                                industrySectorDataGridViewTextBoxColumn,
                                givenTypeDataGridViewComboBoxColumn,
                                givenAmtDataGridViewTextBoxColumn,
                                spreadToDiscMarginIndexDataGridViewComboBoxColumn,
                                noOfstepsDataGridViewTextBoxColumn,
                                stepsAmtDataGridViewTextBoxColumn,
                                userNotesDataGridViewTextBoxColumn,
                          };


            dataGridView.Columns.Clear();
            dataGridView.Columns.AddRange(columns);

            this.dataGridView.CellFormatting += this.DataGridViewCellFormatting;
        }
        #endregion

        #region Utils

        void DataGridViewCellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.ColumnIndex == this.validatedDataGridViewImageColumn.Index && !e.FormattingApplied)
            {
                if (e.Value == null || !e.Value.GetType().IsEnum)
                {
                    return;
                }

                e.Value = Helpers.GetStatusImage((Status)e.Value);
            }
        }

        private void RefreshCategoryTreeView()
        {
            if (currentPortfolio != null)
            {
                treeViewCategory.Portfolio = currentPortfolio;
                treeViewCategory.CurrentCategory = currentCategory;
                treeViewCategory.LoadNodes();
            }
        }

        private void FillAutoCompleteCustomSource()
        {
            IList<Security> securities = new List<Security>();

            if (!string.IsNullOrEmpty(searchTextBox.Text))
            {
                using (ISession session = Context.Database.OpenSession())
                using (ITransaction transaction = session.BeginTransaction())
                {
                    securities = session.CreateCriteria(typeof(Security)).
                        Add(Expression.Or(Expression.InsensitiveLike("Id", searchTextBox.Text, MatchMode.Start),
                                          Expression.InsensitiveLike("ISIN", searchTextBox.Text, MatchMode.Start))).
                        SetMaxResults(20).List<Security>();

                    transaction.Commit();
                }
            }

            searchAutoCompleteForm.SetDataSource(securities);
        }

        private void AddNewPortfolio()
        {
            currentPortfolio = new Category { Name = "Portfolio " + (Context.Workspace.Portfolios.Count + 1) };
            Context.Workspace.Portfolios.Add(currentPortfolio);
            portfolioListBox.SelectedItem = currentPortfolio;
            RefreshCategoryTreeView();
            dataGridView.DataSource = currentPortfolio.AllSecurities;
        }

        public static void CopySecurity<TS, TT>(TS source, TT target)
        {
            var sourceType = source.GetType();
            var targetType = target.GetType();
            var sourceProperties = sourceType.GetProperties();
            var targetProperties = targetType.GetProperties();

            foreach (var tp in targetProperties)
            {
                if (!tp.CanRead || !tp.CanWrite) continue;

                foreach (var sp in sourceProperties.Where(sp => tp.Name == sp.Name))
                {
                    tp.SetValue(target, sp.GetValue(source, null), null);
                }
            }
        }
        #endregion

        #region ButtonCommands

        private void AddNewPortfolioButtonClick(object sender, EventArgs e)
        {
            this.AddNewPortfolio();
        }

        private void AddNewCategoryButtonClick(object sender, EventArgs e)
        {
            treeViewCategory.AddNode();
        }

        private void SearchSecurityButtonClick(object sender, EventArgs e)
        {
            AddSecurity();
        }

        private void InsertMultipleLines(object sender, EventArgs e)
        {
            this.ShowSecurityInsertionWindow();
        }
        #endregion

        private void ShowSecurityInsertionWindow()
        {
            SecurityInsertionForm form = new SecurityInsertionForm();

            if (CheckCurrentPortfolioExists() && form.ShowDialog() == DialogResult.OK)
            {
                foreach (Security security in form.GetSelectedSecurities()) AddSecurity(security);
            }
        }

        private void AddSecurity()
        {
            if (string.IsNullOrEmpty(searchTextBox.Text)) return;
            var security = searchTextBox.Tag as Security;

            if (CheckCurrentPortfolioExists())
            {
                this.AddSecurity(security);
                searchTextBox.Text = string.Empty;
            }
        }

        private bool CheckCurrentPortfolioExists()
        {
            if (currentPortfolio == null)
            {
                Dialogs.Warning("Add security", "Please select a portfolio first");
                return false;
            }
            return true;
        }

        private void AddSecurity(Security security)
        {
            if (security == null) return;

            if (Context.Workspace.AllSecurities.Any(s => s.IdCussip == security.IdCussip)) return;

            var ps = new PortfolioSecurity();

            CopySecurity(security, ps);
            ps.OriginalAmount = 1000000;

            if (currentCategory != null && !currentCategory.Securities.Contains(ps))
            {
                Debug.WriteLine(currentCategory.Name);

                currentCategory.Securities.Add(ps);
                dataGridView.DataSource = currentCategory.Securities;
            }
            else if (!currentPortfolio.Securities.Contains(ps))
            {
                currentPortfolio.Securities.Add(ps);
                dataGridView.DataSource = currentPortfolio.Securities;
            }

            EnsureSecurityHaveScenarioValues(ps);

            Context.EventAggregator.Publish(new WorkspaceLoadedEvent(), this);
        }

        private static void EnsureSecurityHaveScenarioValues(PortfolioSecurity security)
        {
            int delta = Context.Workspace.Scenarios.Count - security.ScenarioValues.Count;
            if (delta > 0)
            {
                for (int i = 0; i < delta; i++)
                {
                    security.ScenarioValues.Add(new SecurityScenarioValues());
                }
            }
        }

        private void SearchTextBoxTextChanged(object sender, EventArgs e)
        {
            securitySearchDelayer.TryExecute();
        }

        private void SearchSecurities()
        {
            FillAutoCompleteCustomSource();
        }

        #region SelectCommands
        private void PortfolioListBoxSelectedIndexChanged(object sender, EventArgs e)
        {
            Category portfolio = this.portfolioListBox.SelectedItem as Category;
            if (portfolio == currentPortfolio) return;

            currentPortfolio = portfolio;
            dataGridView.DataSource = currentPortfolio == null ? null : currentPortfolio.Securities;
            RefreshCategoryTreeView();
        }

        private void TreeViewCategoryAfterSelect(object sender, TreeViewEventArgs e)
        {
            var category = e.Node.Tag as Category;
            if (null == category)
            {
                lbCurrentCategory.Text = string.Empty;
                return;
            }

            currentCategory = category;
            treeViewCategory.CurrentCategory = category;
            lbCurrentCategory.Text = category.Name;
            dataGridView.DataSource = currentCategory.Securities;
        }

        private void SearchTextBoxKeyUp(object sender, KeyEventArgs e)
        {
            var t = (TextBox)sender;
            switch (e.KeyData)
            {
                case (Keys.C | Keys.Control):
                    t.Copy();
                    e.Handled = true;
                    break;
                case (Keys.X | Keys.Control):
                    t.Cut();
                    e.Handled = true;
                    break;
                case (Keys.V | Keys.Control):
                    t.Paste();
                    e.Handled = true;
                    break;
                case (Keys.A | Keys.Control):
                    t.SelectAll();
                    e.Handled = true;
                    break;
                case (Keys.Z | Keys.Control):
                    t.Undo();
                    e.Handled = true;
                    break;
                case (Keys.Enter):
                    AddSecurity();
                    e.Handled = true;
                    break;
            }
        }

        #endregion

        #region ContextMenuStripCommands

        private void AddAPortfolioToolStripMenuItemClick(object sender, EventArgs e)
        {
            this.AddNewPortfolio();
        }

        private void RenamePortfolioToolStripMenuItemClick(object sender, EventArgs e)
        {
            portfolioListBox.RenameItem(renamePortfolioTextBox);
        }

        private void DeletePortfolioToolStripMenuItemClick(object sender, EventArgs e)
        {
            Context.Workspace.Portfolios.Remove(currentPortfolio);
            if (Context.Workspace.Portfolios.Count == 0)
            {
                treeViewCategory.ClearAll();
                dataGridView.DataSource = null;
            }
        }

        private void AddACategoryToolStripMenuItemClick(object sender, EventArgs e)
        {
            treeViewCategory.AddNode();
        }

        private void RenameCategoryToolStripMenuItem1Click(object sender, EventArgs e)
        {
            treeViewCategory.RenameNode();
        }

        private void DeleteCategoryToolStripMenuItemClick(object sender, EventArgs e)
        {
            treeViewCategory.DeleteNode();
        }

        private void RenamePortfolioTextBoxKeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                currentPortfolio.Name = this.renamePortfolioTextBox.Text;
                this.renamePortfolioTextBox.Visible = false;
                ((CurrencyManager)portfolioListBox.BindingContext[Context.Workspace.Portfolios]).Refresh();

                // RefreshCategoryTreeView();
            }
            if (e.KeyChar == 27)
                this.renamePortfolioTextBox.Visible = false;
        }

        #endregion

        private void DataGridViewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == (Keys.Delete | Keys.Control))
            {
                DeleteSelectedSecurities();
                e.Handled = true;
            }
            if (e.KeyData == (Keys.Delete))
                e.Handled = true;
        }

        private void DeleteSecurityToolStripMenuItemClick(object sender, EventArgs e)
        {
            DeleteSelectedSecurities();
        }

        private void DeleteSelectedSecurities()
        {
            for (var i = 0; i < dataGridView.SelectedRows.Count; i++)
            {
                var index = dataGridView.SelectedRows[i].Index;
                if (currentCategory != null)
                {
                    currentCategory.Securities.RemoveAt(index);
                }
                else
                {
                    currentPortfolio.Securities.RemoveAt(index);
                }
            }

            dataGridView.DataSource = currentCategory != null
                              ? currentCategory.Securities
                              : currentPortfolio.Securities;
        }

        private void SecurityContextMenuStripOpening(object sender, CancelEventArgs e)
        {
            e.Cancel = this.dataGridView.SelectedRows.Count <= 0;
        }



        public void Shown()
        {

        }
    }
}
