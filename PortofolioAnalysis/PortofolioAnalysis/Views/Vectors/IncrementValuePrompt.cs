using System;
using System.Windows.Forms;

namespace PortfolioAnalysis.Views.Vectors
{
    public partial class IncrementValuePrompt : Form
    {
        public IncrementValuePrompt()
        {
            InitializeComponent();
        }

        public decimal IncrementValue
        {
            get
            {
                return this.increment.Value;
            }
        }

        private void IncrementValuesLoad(object sender, EventArgs e)
        {
            this.increment.Focus();
            this.increment.Select(0, this.increment.Text.Length);
        }
    }
}