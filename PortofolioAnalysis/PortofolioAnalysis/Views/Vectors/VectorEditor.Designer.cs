﻿namespace PortfolioAnalysis.Views.Vectors
{
    partial class VectorEditor
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.selectedVectorGroupBox = new System.Windows.Forms.GroupBox();
            this.zedGraphControl = new ZedGraph.ZedGraphControl();
            this.panel2 = new System.Windows.Forms.Panel();
            this.timeUnitComboBox = new System.Windows.Forms.ComboBox();
            this.unitsNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.isFundamentalCheckBox = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.endDateDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.frequencyComboBox = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.startDateDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.nameTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.grid = new System.Windows.Forms.DataGridView();
            this.periodColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.valueColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VectorEditorcontextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.CopyValuetoAllCellsBelowtoolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CopyValuetoAllCellsBelowwithIncrementtoolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pasteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.selectedVectorGroupBox.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.unitsNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grid)).BeginInit();
            this.VectorEditorcontextMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // selectedVectorGroupBox
            // 
            this.selectedVectorGroupBox.Controls.Add(this.zedGraphControl);
            this.selectedVectorGroupBox.Controls.Add(this.panel2);
            this.selectedVectorGroupBox.Controls.Add(this.grid);
            this.selectedVectorGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.selectedVectorGroupBox.Location = new System.Drawing.Point(0, 0);
            this.selectedVectorGroupBox.Name = "selectedVectorGroupBox";
            this.selectedVectorGroupBox.Size = new System.Drawing.Size(796, 422);
            this.selectedVectorGroupBox.TabIndex = 16;
            this.selectedVectorGroupBox.TabStop = false;
            // 
            // zedGraphControl
            // 
            this.zedGraphControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.zedGraphControl.EditButtons = System.Windows.Forms.MouseButtons.Left;
            this.zedGraphControl.EditModifierKeys = System.Windows.Forms.Keys.None;
            this.zedGraphControl.IsEnableVEdit = true;
            this.zedGraphControl.Location = new System.Drawing.Point(3, 102);
            this.zedGraphControl.Name = "zedGraphControl";
            this.zedGraphControl.ScrollGrace = 0D;
            this.zedGraphControl.ScrollMaxX = 0D;
            this.zedGraphControl.ScrollMaxY = 0D;
            this.zedGraphControl.ScrollMaxY2 = 0D;
            this.zedGraphControl.ScrollMinX = 0D;
            this.zedGraphControl.ScrollMinY = 0D;
            this.zedGraphControl.ScrollMinY2 = 0D;
            this.zedGraphControl.Size = new System.Drawing.Size(585, 317);
            this.zedGraphControl.TabIndex = 20;
            this.zedGraphControl.ZoomButtons = System.Windows.Forms.MouseButtons.None;
            this.zedGraphControl.PointEditEvent += new ZedGraph.ZedGraphControl.PointEditHandler(this.ZedGraphControlPointEditEvent);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.timeUnitComboBox);
            this.panel2.Controls.Add(this.unitsNumericUpDown);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.isFundamentalCheckBox);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.endDateDateTimePicker);
            this.panel2.Controls.Add(this.frequencyComboBox);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.startDateDateTimePicker);
            this.panel2.Controls.Add(this.nameTextBox);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(3, 16);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(585, 86);
            this.panel2.TabIndex = 19;
            // 
            // timeUnitComboBox
            // 
            this.timeUnitComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.timeUnitComboBox.Enabled = false;
            this.timeUnitComboBox.FormattingEnabled = true;
            this.timeUnitComboBox.Location = new System.Drawing.Point(178, 61);
            this.timeUnitComboBox.Name = "timeUnitComboBox";
            this.timeUnitComboBox.Size = new System.Drawing.Size(121, 21);
            this.timeUnitComboBox.TabIndex = 11;
            this.timeUnitComboBox.Visible = false;
            // 
            // unitsNumericUpDown
            // 
            this.unitsNumericUpDown.Enabled = false;
            this.unitsNumericUpDown.Location = new System.Drawing.Point(125, 62);
            this.unitsNumericUpDown.Name = "unitsNumericUpDown";
            this.unitsNumericUpDown.Size = new System.Drawing.Size(47, 20);
            this.unitsNumericUpDown.TabIndex = 10;
            this.unitsNumericUpDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.unitsNumericUpDown.Visible = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 64);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Fundamental";
            this.label5.Visible = false;
            // 
            // isFundamentalCheckBox
            // 
            this.isFundamentalCheckBox.AutoSize = true;
            this.isFundamentalCheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.isFundamentalCheckBox.Location = new System.Drawing.Point(84, 64);
            this.isFundamentalCheckBox.Name = "isFundamentalCheckBox";
            this.isFundamentalCheckBox.Size = new System.Drawing.Size(15, 14);
            this.isFundamentalCheckBox.TabIndex = 8;
            this.isFundamentalCheckBox.UseVisualStyleBackColor = true;
            this.isFundamentalCheckBox.Visible = false;
            this.isFundamentalCheckBox.CheckedChanged += new System.EventHandler(this.isFundamentalCheckBox_CheckedChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(225, 33);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "End date :";
            // 
            // endDateDateTimePicker
            // 
            this.endDateDateTimePicker.CustomFormat = "";
            this.endDateDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.endDateDateTimePicker.Location = new System.Drawing.Point(297, 27);
            this.endDateDateTimePicker.Name = "endDateDateTimePicker";
            this.endDateDateTimePicker.Size = new System.Drawing.Size(130, 20);
            this.endDateDateTimePicker.TabIndex = 6;
            this.endDateDateTimePicker.Validated += new System.EventHandler(this.EndDateDateTimePickerValidated);
            // 
            // frequencyComboBox
            // 
            this.frequencyComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.frequencyComboBox.FormattingEnabled = true;
            this.frequencyComboBox.Location = new System.Drawing.Point(297, 1);
            this.frequencyComboBox.Name = "frequencyComboBox";
            this.frequencyComboBox.Size = new System.Drawing.Size(130, 21);
            this.frequencyComboBox.TabIndex = 5;
            this.frequencyComboBox.SelectedIndexChanged += new System.EventHandler(this.FrequencyComboBoxSelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(225, 4);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Frequency  :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 33);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Start date :";
            // 
            // startDateDateTimePicker
            // 
            this.startDateDateTimePicker.CustomFormat = "yyyy/MM/dd";
            this.startDateDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.startDateDateTimePicker.Location = new System.Drawing.Point(84, 27);
            this.startDateDateTimePicker.Name = "startDateDateTimePicker";
            this.startDateDateTimePicker.Size = new System.Drawing.Size(130, 20);
            this.startDateDateTimePicker.TabIndex = 2;
            this.startDateDateTimePicker.Validated += new System.EventHandler(this.StartDateDateTimePickerValidated);
            // 
            // nameTextBox
            // 
            this.nameTextBox.Location = new System.Drawing.Point(84, 0);
            this.nameTextBox.Name = "nameTextBox";
            this.nameTextBox.Size = new System.Drawing.Size(130, 20);
            this.nameTextBox.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name :";
            // 
            // grid
            // 
            this.grid.AllowUserToAddRows = false;
            this.grid.AllowUserToDeleteRows = false;
            this.grid.BackgroundColor = System.Drawing.SystemColors.Window;
            this.grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.periodColumn,
            this.valueColumn});
            this.grid.ContextMenuStrip = this.VectorEditorcontextMenuStrip;
            this.grid.Dock = System.Windows.Forms.DockStyle.Right;
            this.grid.Location = new System.Drawing.Point(588, 16);
            this.grid.Name = "grid";
            this.grid.RowHeadersVisible = false;
            this.grid.RowHeadersWidth = 23;
            this.grid.Size = new System.Drawing.Size(205, 403);
            this.grid.TabIndex = 17;
            this.grid.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridMouseDown);
            // 
            // periodColumn
            // 
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle1.Format = "yyyy/MM/dd";
            dataGridViewCellStyle1.NullValue = null;
            this.periodColumn.DefaultCellStyle = dataGridViewCellStyle1;
            this.periodColumn.HeaderText = "Date";
            this.periodColumn.Name = "periodColumn";
            this.periodColumn.ReadOnly = true;
            this.periodColumn.Width = 80;
            // 
            // valueColumn
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle2.Format = "N5";
            this.valueColumn.DefaultCellStyle = dataGridViewCellStyle2;
            this.valueColumn.HeaderText = "Value";
            this.valueColumn.Name = "valueColumn";
            // 
            // VectorEditorcontextMenuStrip
            // 
            this.VectorEditorcontextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CopyValuetoAllCellsBelowtoolStripMenuItem,
            this.CopyValuetoAllCellsBelowwithIncrementtoolStripMenuItem,
            this.pasteToolStripMenuItem});
            this.VectorEditorcontextMenuStrip.Name = "VectorEditorcontextMenuStrip";
            this.VectorEditorcontextMenuStrip.Size = new System.Drawing.Size(312, 70);
            this.VectorEditorcontextMenuStrip.Opening += new System.ComponentModel.CancelEventHandler(this.VectorEditorcontextMenuStripOpening);
            // 
            // CopyValuetoAllCellsBelowtoolStripMenuItem
            // 
            this.CopyValuetoAllCellsBelowtoolStripMenuItem.Name = "CopyValuetoAllCellsBelowtoolStripMenuItem";
            this.CopyValuetoAllCellsBelowtoolStripMenuItem.Size = new System.Drawing.Size(311, 22);
            this.CopyValuetoAllCellsBelowtoolStripMenuItem.Text = "Copy Value to All Cells Below";
            this.CopyValuetoAllCellsBelowtoolStripMenuItem.Click += new System.EventHandler(this.CopyValuetoAllCellsBelowtoolStripMenuItemClick);
            // 
            // CopyValuetoAllCellsBelowwithIncrementtoolStripMenuItem
            // 
            this.CopyValuetoAllCellsBelowwithIncrementtoolStripMenuItem.Name = "CopyValuetoAllCellsBelowwithIncrementtoolStripMenuItem";
            this.CopyValuetoAllCellsBelowwithIncrementtoolStripMenuItem.Size = new System.Drawing.Size(311, 22);
            this.CopyValuetoAllCellsBelowwithIncrementtoolStripMenuItem.Text = "Copy Value to All Cells Below with Increment";
            this.CopyValuetoAllCellsBelowwithIncrementtoolStripMenuItem.Click += new System.EventHandler(this.CopyValuetoAllCellsBelowWithIncrementtoolStripMenuItemClick);
            // 
            // pasteToolStripMenuItem
            // 
            this.pasteToolStripMenuItem.Name = "pasteToolStripMenuItem";
            this.pasteToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V)));
            this.pasteToolStripMenuItem.Size = new System.Drawing.Size(311, 22);
            this.pasteToolStripMenuItem.Text = "Paste";
            this.pasteToolStripMenuItem.Click += new System.EventHandler(this.PasteToolStripMenuItemClick);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Date";
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle3.Format = "yyyy/MM/dd";
            dataGridViewCellStyle3.NullValue = null;
            this.dataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridViewTextBoxColumn1.HeaderText = "Date";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 80;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Value";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.Format = "N5";
            this.dataGridViewTextBoxColumn2.DefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridViewTextBoxColumn2.HeaderText = "Value";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // VectorEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.selectedVectorGroupBox);
            this.Name = "VectorEditor";
            this.Size = new System.Drawing.Size(796, 422);
            this.selectedVectorGroupBox.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.unitsNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grid)).EndInit();
            this.VectorEditorcontextMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox selectedVectorGroupBox;
        private System.Windows.Forms.DataGridView grid;
        private ZedGraph.ZedGraphControl zedGraphControl;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DateTimePicker startDateDateTimePicker;
        private System.Windows.Forms.TextBox nameTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox frequencyComboBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker endDateDateTimePicker;
        private System.Windows.Forms.DataGridViewTextBoxColumn periodColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn valueColumn;
        private System.Windows.Forms.ContextMenuStrip VectorEditorcontextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem CopyValuetoAllCellsBelowtoolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem CopyValuetoAllCellsBelowwithIncrementtoolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pasteToolStripMenuItem;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox isFundamentalCheckBox;
        private System.Windows.Forms.NumericUpDown unitsNumericUpDown;
        private System.Windows.Forms.ComboBox timeUnitComboBox;
    }
}
