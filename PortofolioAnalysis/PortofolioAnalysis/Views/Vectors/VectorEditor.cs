﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using ALMS.Products;
using Eu.AlterSystems.ASNetLib.Core;
using PortofolioAnalysis.Common.Entities;
using ZedGraph;

namespace PortfolioAnalysis.Views.Vectors
{
    public partial class VectorEditor : UserControl
    {
        private Vector vector;

        private LineItem graphCurve;

        public VectorEditor()
        {
            this.InitializeComponent();

            this.grid.AutoGenerateColumns = false;
            this.grid.Columns[1].CellTemplate.ValueType = typeof(double);

            this.zedGraphControl.GraphPane.Title.IsVisible = false;
            this.zedGraphControl.GraphPane.Legend.IsVisible = false;
            this.zedGraphControl.GraphPane.XAxis.Type = AxisType.Date;
            this.zedGraphControl.GraphPane.XAxis.Title.IsVisible = false;
            this.zedGraphControl.GraphPane.YAxis.Title.IsVisible = false;
            this.zedGraphControl.GraphPane.YAxis.MajorGrid.IsVisible = true;

            this.startDateDateTimePicker.MinDate = DateTime.MinValue;
            this.startDateDateTimePicker.MaxDate = DateTime.MaxValue;

            this.frequencyComboBox.DisplayMember = "Value";
            this.frequencyComboBox.ValueMember = "Key";
            this.frequencyComboBox.DataSource = EnumHelper.ToList(typeof(ProductFrequency));

            this.timeUnitComboBox.DisplayMember = "Value";
            this.timeUnitComboBox.ValueMember = "Key";
            this.timeUnitComboBox.DataSource = EnumHelper.ToList(typeof(TimeUnit));

            this.grid.DataError += GridDataError;
            this.grid.CellValueChanged += this.GridCellValueChanged;
        }

        private static void GridDataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            e.Cancel = true;
            e.ThrowException = false;

            Debug.WriteLine(e.Exception);
        }

        public void Edit(Vector vectorToEdit)
        {
            if (this.vector == vectorToEdit) return;

            this.Reset();

            this.vector = vectorToEdit;

            this.nameTextBox.DataBindings.Add(new Binding("Text", this.vector, "Name", false, DataSourceUpdateMode.OnPropertyChanged));

            this.startDateDateTimePicker.DataBindings.Add(new Binding("Value", this.vector, "StartDate", false, DataSourceUpdateMode.OnPropertyChanged));

            this.endDateDateTimePicker.DataBindings.Add(new Binding("Value", this.vector, "EndDate", false, DataSourceUpdateMode.OnPropertyChanged));

            this.frequencyComboBox.DataBindings.Add(new Binding("SelectedValue", this.vector, "Frequency", false, DataSourceUpdateMode.OnPropertyChanged));

            this.isFundamentalCheckBox.DataBindings.Add(new Binding("Checked", this.vector, "IsFundametal", false, DataSourceUpdateMode.OnPropertyChanged));

            this.unitsNumericUpDown.DataBindings.Add(new Binding("Value", this.vector, "Delta", false, DataSourceUpdateMode.OnPropertyChanged));

            this.timeUnitComboBox.DataBindings.Add(new Binding("SelectedValue", this.vector, "Unit", false, DataSourceUpdateMode.OnPropertyChanged));

            this.GenerateDates();

            this.Enabled = true;
        }

        public void Clear()
        {
            this.vector = null;
            this.Enabled = false;

            Reset();
        }

        private void Reset()
        {
            this.nameTextBox.DataBindings.Clear();

            this.startDateDateTimePicker.DataBindings.Clear();
            this.endDateDateTimePicker.DataBindings.Clear();
            this.frequencyComboBox.DataBindings.Clear();
            this.isFundamentalCheckBox.DataBindings.Clear();
            this.unitsNumericUpDown.DataBindings.Clear();
            this.timeUnitComboBox.DataBindings.Clear();

            this.grid.Rows.Clear();
        }

        private void FrequencyComboBoxSelectedIndexChanged(object sender, EventArgs e)
        {
            this.VectorChanged();
        }

        private void StartDateDateTimePickerValidated(object sender, EventArgs e)
        {
            this.VectorChanged();
        }

        private void EndDateDateTimePickerValidated(object sender, EventArgs e)
        {
            this.VectorChanged();
        }

        private void VectorChanged()
        {
            if (this.vector == null)
            {
                return;
            }

            this.vector.Frequency = (ProductFrequency)this.frequencyComboBox.SelectedValue;
            this.vector.StartDate = this.startDateDateTimePicker.Value;
            this.vector.EndDate = this.endDateDateTimePicker.Value;

            this.GenerateDates();
        }

        private void GenerateDates()
        {
            if (this.vector == null)
            {
                return;
            }

            this.vector.GenerateDates();

            FillData();
        }

        private void FillData()
        {
            this.FillGrid();
            this.FillGraph();
        }

        private void FillGraph()
        {
            GraphPane zedPane = zedGraphControl.GraphPane;

            zedPane.CurveList.Clear();

            PointPairList list = new PointPairList();
            for (int i = 0; i < vector.Values.Count; i++)
            {
                double x = new XDate(vector.Dates[i]);
                double y = this.vector.Values[i];
                list.Add(x, y);
            }

            graphCurve = zedPane.AddCurve("Values", list, Color.DarkGreen);
            graphCurve.Symbol.Type = SymbolType.Circle;
            graphCurve.Symbol.Fill = new Fill(Color.White);
            graphCurve.Symbol.Size = 6;

            zedGraphControl.AxisChange();
            zedGraphControl.Invalidate();
        }

        private void FillGrid()
        {
            this.AdjustGrid();
            this.AdjustVectorValues();

            this.grid.CellValueChanged -= this.GridCellValueChanged;
            for (int i = 0; i < this.vector.Dates.Length; i++)
            {
                this.grid[0, i].Value = this.vector.Dates[i];
                this.grid[1, i].Value = this.vector.Values[i];
            }
            this.grid.CellValueChanged += this.GridCellValueChanged;
        }

        private void GridCellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (this.vector == null) return;

            if (e.ColumnIndex == 1)
            {
                object value = this.grid[e.ColumnIndex, e.RowIndex].Value;
                this.vector.Values[e.RowIndex] = Convert.ToDouble(value);

                graphCurve.Points[e.RowIndex].Y = (double)value;

                zedGraphControl.AxisChange();
                zedGraphControl.Invalidate();
            }
        }

        private void AdjustVectorValues()
        {
            if (this.vector.Values.Count < this.vector.Dates.Length)
            {
                int count = this.vector.Dates.Length - this.vector.Values.Count;
                for (int i = 0; i < count; i++)
                {
                    this.vector.Values.Add(0);
                }
            }
            else if (this.vector.Values.Count > this.vector.Dates.Length)
            {
                int count = this.vector.Values.Count - this.vector.Dates.Length;
                for (int i = 0; i < count; i++)
                {
                    this.vector.Values.RemoveAt(this.grid.Rows.Count - 1);
                }
            }
        }

        private void AdjustGrid()
        {
            if (this.grid.Rows.Count < this.vector.Dates.Length)
            {
                this.grid.Rows.Add(this.vector.Dates.Length - this.grid.Rows.Count);
            }
            else if (this.grid.Rows.Count > this.vector.Dates.Length)
            {
                int count = this.grid.Rows.Count - this.vector.Dates.Length;
                for (int i = 0; i < count; i++)
                {
                    this.grid.Rows.RemoveAt(this.grid.Rows.Count - 1);
                }
            }
        }

        private void PasteToolStripMenuItemClick(object sender, EventArgs e)
        {
            string clipboardtext = Clipboard.GetText();
            if (string.IsNullOrEmpty(clipboardtext))
            {
                return;
            }
            this.grid.EndEdit();
            if (this.grid.CurrentCell == null || this.grid.CurrentCell.RowIndex < 0)
            {
                return;
            }
            string[] clipboardrows = clipboardtext.Split(new[] { Environment.NewLine, "\t" }, StringSplitOptions.None);

            if (clipboardrows.Length == 0)
            {
                return;
            }

            int startIndex = this.grid.CurrentCell.RowIndex;

            foreach (string t in clipboardrows)
            {
                if (startIndex >= this.grid.Rows.Count)
                {
                    break;
                }
                double value = Helpers.TryConvertToDouble(t).GetValueOrDefault(double.NaN);
                this.grid.Rows[startIndex].Cells[1].Value = value;
                startIndex++;
            }
        }

        private string ZedGraphControlPointEditEvent(ZedGraphControl sender, GraphPane pane, CurveItem curve, int iPt)
        {
            if (vector == null) return null;

            PointPair pt = curve[iPt];

            for (int i = 0; i < vector.Dates.Length; i++)
            {
                if (XDate.XLDateToDateTime(pt.X) == vector.Dates[i])
                {
                    vector.Values[i] = Math.Round(pt.Y, 5);
                    this.grid[1, i].Value = vector.Values[i];
                    break;
                }
            }

            return null;
        }

        private void CopyValuetoAllCellsBelowtoolStripMenuItemClick(object sender, EventArgs e)
        {
            if (this.grid.SelectedCells.Count == 0) return;

            int selectedRowIndex = grid.SelectedCells[0].RowIndex;
            object val = grid[this.valueColumn.Index, selectedRowIndex].Value;
            for (int i = selectedRowIndex; i < grid.Rows.Count; i++)
            {
                grid[this.valueColumn.Index, i].Value = val;
            }
        }

        private void CopyValuetoAllCellsBelowWithIncrementtoolStripMenuItemClick(object sender, EventArgs e)
        {
            IncrementValuePrompt incv = new IncrementValuePrompt();
            DialogResult dialogResult = incv.ShowDialog();
            if (dialogResult == DialogResult.Cancel) return;

            if (dialogResult == DialogResult.OK)
            {
                int selectedRowIndex = this.grid.SelectedCells[0].RowIndex;
                double value = (double)this.grid[this.valueColumn.Index, selectedRowIndex].Value;
                for (int i = selectedRowIndex + 1; i < grid.Rows.Count; i++)
                {
                    value += (double)incv.IncrementValue;
                    grid[this.valueColumn.Index, i].Value = value;
                }
            }
        }

        private void GridMouseDown(object sender, MouseEventArgs e)
        {
            DataGridView.HitTestInfo hitTestInfo = this.grid.HitTest(e.X, e.Y);
            if (e.Button == MouseButtons.Right)
            {
                this.grid.ClearSelection();
                this.grid[hitTestInfo.ColumnIndex, hitTestInfo.RowIndex].Selected = true;
            }
        }

        private void VectorEditorcontextMenuStripOpening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (grid.SelectedCells.Count != 1 || grid.SelectedCells[0].ColumnIndex != this.valueColumn.Index)
            {
                e.Cancel = true;
            }
        }

        private void isFundamentalCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            unitsNumericUpDown.Enabled = timeUnitComboBox.Enabled = isFundamentalCheckBox.Checked;
        }
    }
}