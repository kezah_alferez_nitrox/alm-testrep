using System;
using System.ComponentModel;
using System.Windows.Forms;
using ALMS.Products;
using PortofolioAnalysis.Common.Entities;

namespace PortfolioAnalysis.Views.Vectors
{
    public partial class VectorManager : Form
    {
        private int selectedVectorIndex = -1;
        private readonly BindingList<Vector> vectors = new BindingList<Vector>();

        public VectorManager()
        {
            this.InitializeComponent();

            this.vectorListBox.DisplayMember = "Name";
            this.vectorListBox.ValueMember = "Self";
        }

        private void VectorManagerLoad(object sender, EventArgs e)
        {
            foreach (Vector vector in Context.Workspace.Vectors)
            {
                this.vectors.Add(vector.Clone());
            }

            this.vectorListBox.DataSource = this.vectors;

            this.vectors.ListChanged += (s, ev) => this.saveButton.Enabled = true;
        }

        private void NewButtonClick(object sender, EventArgs e)
        {
            Vector vector = new Vector();
            vector.Name = "NewVector";
            vector.StartDate = DateTime.Today;
            vector.EndDate = vector.StartDate.AddYears(30);
            vector.Frequency = ProductFrequency.Monthly;
            this.vectors.Add(vector);
        }

        private void SaveButtonClick(object sender, EventArgs e)
        {
            if (VectorsAreValid())
            {
                Context.Workspace.Vectors.Clear();
                foreach (Vector vector in this.vectors)
                {
                    Context.Workspace.Vectors.Add(vector);
                }
            }
        }

        private bool VectorsAreValid()
        {
            return true;
        }

        private void VectorListBoxSelectedIndexChanged(object sender, EventArgs e)
        {
            if (selectedVectorIndex == vectorListBox.SelectedIndex) return;

            selectedVectorIndex = vectorListBox.SelectedIndex;

            Vector vector = this.vectorListBox.SelectedValue as Vector;
            if (vector != null)
            {
                this.vectorEditor.Edit(vector);
            }
        }

        private void DeleteToolStripMenuItemClick(object sender, EventArgs e)
        {
            Vector vector = this.vectorListBox.SelectedValue as Vector;
            if (vector == null) return;

            this.vectors.Remove(vector);
        }

        private void VectorListContextMenuStripOpening(object sender, CancelEventArgs e)
        {
            deleteToolStripMenuItem.Enabled = makeCopyToolStripMenuItem.Enabled = vectorListBox.SelectedItem != null;
        }

        private void MakeCopyToolStripMenuItemClick(object sender, EventArgs e)
        {
            Vector vector = this.vectorListBox.SelectedValue as Vector;
            if (vector == null) return;

            Vector clone = vector.Clone();
            clone.Name += "Copy";

            vectors.Add(clone);
        }

        private void VectorListBoxMouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                vectorListBox.SelectedIndex = vectorListBox.IndexFromPoint(e.X, e.Y);
            }
        }
    }
}