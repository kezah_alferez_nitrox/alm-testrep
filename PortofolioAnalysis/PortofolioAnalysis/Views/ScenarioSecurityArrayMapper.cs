using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using ALMSCommon;
using PortfolioAnalysis.Infrastructure;
using PortofolioAnalysis.Common.Entities;

namespace PortfolioAnalysis.Views
{
    public class ScenarioSecurityArrayMapper : IEntityArrayMapper<PortfolioSecurity>
    {
        private readonly EntityArrayMapper<PortfolioSecurity> portfolioSecurityMapper;
        private readonly EntityArrayMapper<SecurityScenarioValues> securityScenarioValuesMapper;

        public ScenarioSecurityArrayMapper(Expression<Func<PortfolioSecurity, object>>[] securityProperties,
            Expression<Func<SecurityScenarioValues, object>>[] scenarioValueProperties)
        {
            this.portfolioSecurityMapper = new EntityArrayMapper<PortfolioSecurity>(securityProperties);

            this.securityScenarioValuesMapper = new EntityArrayMapper<SecurityScenarioValues>(scenarioValueProperties);
        }

        public object[] GetArray(PortfolioSecurity portfolioSecurity)
        {
            Debug.Assert(portfolioSecurity != null);

            List<object> result = new List<object>();

            result.AddRange(this.portfolioSecurityMapper.GetArray(portfolioSecurity));
            foreach (SecurityScenarioValues scenarioValues in portfolioSecurity.ScenarioValues)
            {
                result.AddRange(this.securityScenarioValuesMapper.GetArray(scenarioValues));
            }

            return result.ToArray();
        }

        public void SetPropertyByIndex(PortfolioSecurity portfolioSecurity, int index, object value)
        {
            Debug.Assert(portfolioSecurity != null);
            Debug.Assert(index >= 0);
            Debug.Assert(index < this.portfolioSecurityMapper.Count + portfolioSecurity.ScenarioValues.Count * this.securityScenarioValuesMapper.Count);

            if (index < this.portfolioSecurityMapper.Count)
            {
                this.portfolioSecurityMapper.SetPropertyByIndex(portfolioSecurity, index, value);
            }
            else
            {
                int firstScenarioIndex = index - this.portfolioSecurityMapper.Count;
                int scenarionIndex = firstScenarioIndex / this.securityScenarioValuesMapper.Count;
                int valueIndex = firstScenarioIndex % this.securityScenarioValuesMapper.Count;

                this.securityScenarioValuesMapper.SetPropertyByIndex(portfolioSecurity.ScenarioValues[scenarionIndex],
                    valueIndex, value);
            }
        }

        object[] IEntityArrayMapper.GetArray(object entity)
        {
            return this.GetArray(entity as PortfolioSecurity);
        }

        void IEntityArrayMapper.SetPropertyByIndex(object entity, int index, object value)
        {
            this.SetPropertyByIndex(entity as PortfolioSecurity, index, value);
        }

        public object GetPropertyByIndex(PortfolioSecurity portfolioSecurity, int index)
        {
            Debug.Assert(portfolioSecurity != null);
            Debug.Assert(index >= 0);
            Debug.Assert(index < this.portfolioSecurityMapper.Count + portfolioSecurity.ScenarioValues.Count * this.securityScenarioValuesMapper.Count);

            if (index < this.portfolioSecurityMapper.Count)
            {
                return this.portfolioSecurityMapper.GetPropertyByIndex(portfolioSecurity, index);
            }

            int scenarionValueIndex = (index - this.portfolioSecurityMapper.Count) / 3;
            return this.securityScenarioValuesMapper.GetPropertyByIndex(portfolioSecurity.ScenarioValues[scenarionValueIndex],
                index - scenarionValueIndex);
        }
    }
}