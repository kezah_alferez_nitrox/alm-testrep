﻿using System;
using System.Windows.Forms;
using Eu.AlterSystems.ASNetLib.Core;
using PortofolioAnalysis.Common;
using PortofolioAnalysis.Common.Entities;

namespace PortfolioAnalysis.Views.Tools
{
    public partial class ParametersForm : Form
    {
        private readonly Parameters parameters = new Parameters();

        public ParametersForm()
        {
            this.InitializeComponent();

            this.dateFormatComboBox.DisplayMember = "Value";
            this.dateFormatComboBox.ValueMember = "Key";
            this.dateFormatComboBox.DataSource = EnumHelper.ToList(typeof(DateFormat));

            this.dateFormatComboBox.DataBindings.Add("SelectedValue", this.parameters, "DateFormat", false, DataSourceUpdateMode.OnPropertyChanged);
        }

        private void ParametersForm_Load(object sender, EventArgs e)
        {
            Util.CopyProperties(Context.Workspace.Parameters, this.parameters);

            parameters.PropertyChanged += (s, ev) => saveButton.Enabled = true;
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            Util.CopyProperties(parameters, Context.Workspace.Parameters);
        }
    }
}