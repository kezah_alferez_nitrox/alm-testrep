﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Text;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using ALMSCommon;
using PortfolioAnalysis.Infrastructure;
using PortfolioAnalysis.Views.Tools;
using PortfolioAnalysis.Views.Vectors;

namespace PortfolioAnalysis.Views
{
    public partial class MainForm : Form, IModuleMainForm,
        IHandle<WorkspaceFileNameChangedEvent>, IHandle<WorkspaceDirtyChangedEvent>
    {
        private readonly FileHistory fileHistory = new FileHistory(AlmConfiguration.Module.PortfolioAnalysis);

        public MainForm()
        {
            Context.Initialize();

            this.InitializeComponent();

            this.Font = Constants.Fonts.MainFont;

            Context.EventAggregator.Subscribe(this);

            Context.Workspace.New();
        }

        public bool IsForcedClose { get; set; }

        public ILicenceInfo LicenceInfo { set; get; }

        public void HandleMessage(WorkspaceFileNameChangedEvent message)
        {
            this.SetTitle();
        }

        public void HandleMessage(WorkspaceDirtyChangedEvent message)
        {
            this.SetTitle();
        }

        private void SetTitle()
        {
            string title = Context.Workspace.FileName;
            title = title == null ? "new file" : Path.GetFileName(title);
            if (Context.Workspace.IsDirty) title += " *";
            this.Text = title;
        }

        private void CloseToolStripMenuItemClick(object sender, EventArgs e)
        {
            if (!CanDiscadWorkspace()) return;

            this.Close();
        }

        private void OpenToolStripMenuItemClick(object sender, EventArgs e)
        {
            if (!CanDiscadWorkspace()) return;
            if (this.openFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                try
                {
                    Context.Workspace.Load(this.openFileDialog.FileName);
                    this.fileHistory.PutFile(this.openFileDialog.FileName);
                    this.RefreshOpenLast();
                }
                catch (Exception)
                {
                    Dialogs.Error("Error", "Unable to load file " + this.openFileDialog.FileName);
                }
            }
        }

        private static bool CanDiscadWorkspace()
        {
            return !Context.Workspace.IsDirty ||
                   Dialogs.Confirm("Warning",
                                   "There are unsaved changes in the current workspace, do you want to continue ?");
        }

        private void SaveToolStripMenuItemClick(object sender, EventArgs e)
        {
            this.Save();
        }

        private bool Save()
        {
            try
            {
                if (!Context.Workspace.Save())
                {
                    return this.SaveAs();
                }
            }
            catch (Exception ex)
            {
                Logger.Exception("Save", ex);

                Dialogs.Error("Error", "Unable to save file");

                return false;
            }

            return true;
        }

        private void SaveAsToolStripMenuItemClick(object sender, EventArgs e)
        {
            this.SaveAs();
        }

        private bool SaveAs()
        {
            if (this.saveFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                try
                {
                    Context.Workspace.SaveAs(this.saveFileDialog.FileName);
                    this.fileHistory.PutFile(this.saveFileDialog.FileName);
                    this.RefreshOpenLast();
                }
                catch (Exception ex)
                {
                    Logger.Exception("Save", ex);

                    Dialogs.Error("Error", "Unable to save file");

                    return false;
                }
                return true;
            }

            return false;
        }

        private void NewToolStripMenuItemClick(object sender, EventArgs e)
        {
            if (!CanDiscadWorkspace()) return;
            Context.Workspace.New();
        }

        private void FontNameToolStripComboBoxSelectedIndexChanged(object sender, EventArgs e)
        {
            this.UpdateFormFont();
        }

        private void FontSizeToolStripComboBoxSelectedIndexChanged(object sender, EventArgs e)
        {
            this.UpdateFormFont();
        }

        private void UpdateFormFont()
        {
            var fontName = this.fontNameToolStripComboBox.SelectedItem as string;
            string fontSizeString = this.fontSizeToolStripComboBox.SelectedItem as string ?? "";
            float fontSize;
            if (!string.IsNullOrEmpty(fontName) &&
                (float.TryParse(fontSizeString, out fontSize) ||
                 float.TryParse(fontSizeString.Replace('.', ','), out fontSize)))
            {
                try
                {
                    var f = new Font(fontName, fontSize);
                    this.Font = f;
                    this.Refresh();
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex);
                }
            }
        }

        private void ComputeResultsStripButtonClick(object sender, EventArgs e)
        {
            Context.EventAggregator.Publish(new CalculateEvent());

            this.tabControl.SelectedIndex = 2;
        }

        private void MainFormShown(object sender, EventArgs e)
        {
#if DEBUG
            const string file = @"C:\Users\Cata\Documents\SG\1.sgpaw";
            if (File.Exists(file))
            {
                //  Context.Workspace.Load(file);
            }
#endif
        }

        private void VectorManagerToolStripButtonClick(object sender, EventArgs e)
        {
            new VectorManager().ShowDialog();
        }

        private void SaveToolStripButtonClick(object sender, EventArgs e)
        {
            this.Save();
        }

        private void VectorManagerToolStripMenuItemClick(object sender, EventArgs e)
        {
            new VectorManager().ShowDialog();
        }

        private void MainFormFormClosing(object sender, FormClosingEventArgs e)
        {
            if (!Context.Workspace.IsDirty) return;

            DialogResult result = Dialogs.AskForSave("Workspace changed", "Do you want to save the workspace changes ?");

            if (result == DialogResult.Yes)
            {
                e.Cancel = !this.Save();
            }
            else if (result == DialogResult.Cancel)
            {
                e.Cancel = true;
            }
        }

        private void TabControlSelectedIndexChanged(object sender, EventArgs e)
        {
            switch (this.tabControl.SelectedIndex)
            {
                case 0:
                    this.portfolioView.Shown();
                    break;
                case 1:
                    this.scenarioSetUp.Shown();
                    break;
                case 2:
                    this.reports.Shown();
                    break;
            }
        }

        private void CurrenciesToolStripMenuItemClick(object sender, EventArgs e)
        {
            Dialogs.Info("Work in progress ...", "This function will be implemented in the next version");
        }

        private void ParametersToolStripMenuItemClick(object sender, EventArgs e)
        {
            new ParametersForm().ShowDialog();
        }

        private void ShowHelpToolStripMenuItemClick(object sender, EventArgs e)
        {
            Dialogs.Info("Work in progress ...", "Work in progress on the help file, coming soon...");
        }

        private void AboutToolStripMenuItemClick(object sender, EventArgs e)
        {
            new About().ShowDialog(this);
        }

        private void MainFormLoad(object sender, EventArgs e)
        {
            var fontCollection = new InstalledFontCollection();
            foreach (FontFamily fontFamily in fontCollection.Families)
            {
                this.fontNameToolStripComboBox.Items.Add(fontFamily.Name);
            }

            this.fontNameToolStripComboBox.SelectedItem = this.Font.Name;

            this.fontSizeToolStripComboBox.SelectedItem = this.Font.Size.ToString();

            this.RefreshOpenLast();
        }

        private void RefreshOpenLast()
        {
            IEnumerable<string> files =
                this.fileHistory.GetFiles().Where(f => string.Compare(f, Context.Workspace.FileName) != 0);

            this.openLastToolStripMenuItem.DropDownItems.Clear();
            foreach (string file in files)
            {
                var toolStripMenuItem = new ToolStripMenuItem();
                toolStripMenuItem.Text = file;
                toolStripMenuItem.Click += this.OpenLastToolStripMenuItemClick;
                this.openLastToolStripMenuItem.DropDownItems.Add(toolStripMenuItem);
            }

            
        }

        private void OpenLastToolStripMenuItemClick(object sender, EventArgs e)
        {
            Context.Workspace.Load(((ToolStripMenuItem)sender).Text);
            this.RefreshOpenLast();
        }

        public string[] Args
        {
            get;
            set;
        }

        public void OpenFile(string filePath)
        {
            Context.Workspace.Load(filePath);
            this.RefreshOpenLast();
        }
    }
}