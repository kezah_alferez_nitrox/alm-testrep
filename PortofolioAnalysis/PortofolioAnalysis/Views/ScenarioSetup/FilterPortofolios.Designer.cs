﻿namespace PortfolioAnalysis.Views.ScenarioSetup
{
    partial class FilterPortfolios
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.securitiesFilter = new PortfolioAnalysis.Views.ScenarioSetup.SecuritiesFilter();
            this.cancelButton = new System.Windows.Forms.Button();
            this.addAllButton = new System.Windows.Forms.Button();
            this.addSelectedButton = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.moveSecuritiesRadioButton = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.grid = new System.Windows.Forms.DataGridView();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grid)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.securitiesFilter);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(204, 569);
            this.panel1.TabIndex = 11;
            // 
            // securitiesFilter
            // 
            this.securitiesFilter.Dock = System.Windows.Forms.DockStyle.Top;
            this.securitiesFilter.Location = new System.Drawing.Point(0, 0);
            this.securitiesFilter.Name = "securitiesFilter";
            this.securitiesFilter.Size = new System.Drawing.Size(202, 205);
            this.securitiesFilter.TabIndex = 0;
            this.securitiesFilter.SelectionChanged += new PortfolioAnalysis.Views.ScenarioSetup.SelectionChangedHandler(this.securitiesFilter_SelectionChanged);
            // 
            // cancelButton
            // 
            this.cancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point(733, 56);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(90, 23);
            this.cancelButton.TabIndex = 14;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            // 
            // addAllButton
            // 
            this.addAllButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.addAllButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.addAllButton.Location = new System.Drawing.Point(633, 56);
            this.addAllButton.Name = "addAllButton";
            this.addAllButton.Size = new System.Drawing.Size(90, 23);
            this.addAllButton.TabIndex = 15;
            this.addAllButton.Text = "Add All";
            this.addAllButton.UseVisualStyleBackColor = true;
            this.addAllButton.Click += new System.EventHandler(this.addAllButton_Click);
            // 
            // addSelectedButton
            // 
            this.addSelectedButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.addSelectedButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.addSelectedButton.Location = new System.Drawing.Point(533, 56);
            this.addSelectedButton.Name = "addSelectedButton";
            this.addSelectedButton.Size = new System.Drawing.Size(90, 23);
            this.addSelectedButton.TabIndex = 16;
            this.addSelectedButton.Text = "Add Selected";
            this.addSelectedButton.UseVisualStyleBackColor = true;
            this.addSelectedButton.Click += new System.EventHandler(this.addSelectedButton_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.moveSecuritiesRadioButton);
            this.panel2.Controls.Add(this.radioButton1);
            this.panel2.Controls.Add(this.cancelButton);
            this.panel2.Controls.Add(this.addAllButton);
            this.panel2.Controls.Add(this.addSelectedButton);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(204, 479);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(843, 90);
            this.panel2.TabIndex = 17;
            // 
            // moveSecuritiesRadioButton
            // 
            this.moveSecuritiesRadioButton.AutoSize = true;
            this.moveSecuritiesRadioButton.Location = new System.Drawing.Point(533, 36);
            this.moveSecuritiesRadioButton.Name = "moveSecuritiesRadioButton";
            this.moveSecuritiesRadioButton.Size = new System.Drawing.Size(263, 17);
            this.moveSecuritiesRadioButton.TabIndex = 18;
            this.moveSecuritiesRadioButton.Text = "Move securities already in the list to the new group";
            this.moveSecuritiesRadioButton.UseVisualStyleBackColor = true;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Checked = true;
            this.radioButton1.Location = new System.Drawing.Point(533, 13);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(211, 17);
            this.radioButton1.TabIndex = 17;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Ignore securities already in the scenario";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // grid
            // 
            this.grid.AllowUserToAddRows = false;
            this.grid.AllowUserToDeleteRows = false;
            this.grid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.grid.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grid.Location = new System.Drawing.Point(204, 0);
            this.grid.Name = "grid";
            this.grid.RowHeadersVisible = false;
            this.grid.Size = new System.Drawing.Size(843, 479);
            this.grid.TabIndex = 18;
            // 
            // FilterPortfolios
            // 
            this.AcceptButton = this.addSelectedButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(1047, 569);
            this.Controls.Add(this.grid);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "FilterPortfolios";
            this.Text = "Filter Portfolios";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button addAllButton;
        private System.Windows.Forms.Button addSelectedButton;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView grid;
        private System.Windows.Forms.RadioButton moveSecuritiesRadioButton;
        private System.Windows.Forms.RadioButton radioButton1;
        private SecuritiesFilter securitiesFilter;
    }
}