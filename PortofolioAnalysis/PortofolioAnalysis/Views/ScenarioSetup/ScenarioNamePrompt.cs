﻿using System;
using System.Windows.Forms;
using Castle.ActiveRecord;

namespace PortfolioAnalysis.Views.ScenarioSetup
{
    public partial class ScenarioNamePrompt : Form
    {
        public ScenarioNamePrompt()
        {
            InitializeComponent();
        }

        public string ScenarioName
        {
            get { return scenarioTextBox.Text; }
        }

        private void ScenarioTextBoxTextChanged(object sender, EventArgs e)
        {
            if(scenarioTextBox.Text.Length == 0)
            {
                okButton.Enabled = false;
                return;
            }

            okButton.Enabled = true;
        }
    }
}