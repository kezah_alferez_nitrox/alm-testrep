using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using PortofolioAnalysis.Common.Entities;

namespace PortfolioAnalysis.Views.ScenarioSetup
{
    public partial class ScenarioEditor : Form
    {
        private BindingList<Scenario> scenarios;

        public ScenarioEditor()
        {
            this.InitializeComponent();
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            ScenarioNamePrompt prompt = new ScenarioNamePrompt();
            if (prompt.ShowDialog() == DialogResult.OK)
            {
                Scenario scenario = new Scenario { Name = prompt.ScenarioName };
                this.scenarios.Add(scenario);
            }
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            IList<Scenario> deleted = Context.Workspace.Scenarios.Where(s => !this.scenarios.Contains(s)).ToList();
            IList<Scenario> added = this.scenarios.Where(s => !Context.Workspace.Scenarios.Contains(s)).ToList();

            foreach (Scenario scenario in deleted)
            {
                int index = Context.Workspace.Scenarios.IndexOf(scenario);
                foreach (PortfolioSecurity security in Context.Workspace.SetupSecurities.AllSecurities)
                {
                    security.ScenarioValues.RemoveAt(index);
                }
                Context.Workspace.Scenarios.Remove(scenario);
            }

            foreach (Scenario scenario in added)
            {
                Context.Workspace.Scenarios.Add(scenario);
                foreach (PortfolioSecurity security in Context.Workspace.SetupSecurities.AllSecurities)
                {
                    security.ScenarioValues.Add(new SecurityScenarioValues());
                }
            }
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.scenariosListBox.SelectedIndex < 0) return;

            Scenario selectedScenario = (Scenario)this.scenariosListBox.SelectedValue;
            this.scenarios.Remove(selectedScenario);
        }

        private void downButton_Click(object sender, EventArgs e)
        {
        }

        private void upButton_Click(object sender, EventArgs e)
        {
        }

        private void ScenarioEditor_Load(object sender, EventArgs e)
        {
            this.scenarios = new BindingList<Scenario>();
            foreach (Scenario scenario in Context.Workspace.Scenarios)
            {
                this.scenarios.Add(scenario);
            }
            this.scenariosListBox.DataSource = this.scenarios;
        }
    }
}