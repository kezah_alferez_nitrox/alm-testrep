using System;
using System.Drawing;
using System.Linq;
using System.Linq.Expressions;
using System.Windows.Forms;
using ALMSCommon;
using ALMSCommon.Controls;
using ALMSCommon.Controls.TreeGridView;
using PortofolioAnalysis.Common.DatabaseEntities;
using PortofolioAnalysis.Common.Entities;

namespace PortfolioAnalysis.Views.ScenarioSetup
{
    public class ScenarioSetupTreeGridController : SecuritiesTreeGridController
    {
        public ScenarioSetupTreeGridController(TreeGridView treeGrid)
            : base(treeGrid)
        {
            this.PortfolioSecurityMapper =
                new ScenarioSecurityArrayMapper(new Expression<Func<PortfolioSecurity, object>>[]
                                                {
                                                    e => e.Description,
                                                    e => e.Exclude, e => e.Status, e => e.IdCussip, e => e.ISIN,
                                                    e => e.OriginalAmount, e => e.Factor, e => e.CurrentAmount,
                                                    e => e.RatingMoodys,
                                                    e => e.RatingSandP, e => e.RatingFitch, e => e.Coupon,
                                                    e => e.CouponFrequency,
                                                    e => e.CouponType, e => e.ResetIndex, e => e.Spread,
                                                    e => e.FloatingFrequency, e => e.ResetDate,
                                                    e => e.IssueDate,
                                                    e => e.MaturityDate, e => e.CollateralType, e => e.IndustrySector,
                                                    e => e.GivenType, e => e.GivenAmount, e => e.SpreadToDiscMarginIndex
                                                    ,
                                                    e => e.NumberOfSteps, e => e.StepAmount, e => e.UserNotes
                                                },
                                                new Expression<Func<SecurityScenarioValues, object>>[]
                                                {
                                                    e => e.Prepay, e => e.Default, e => e.Severity
                                                });

            this.TreeGrid.CellEndEdit += this.TreeGridOnCellEndEdit;
            this.TreeGrid.CellParsing += this.TreeGridCellParsing;
            this.TreeGrid.CellFormatting += this.TreeGridCellFormatting;

            this.TreeGrid.MouseDown += this.TreeGridMouseDown;
            this.TreeGrid.DragOver += this.TreeGridDragOver;
            this.TreeGrid.DragDrop += this.TreeGridDragDrop;

            this.TreeGrid.CellValueChanged += this.TreeGridValueChanged;
        }

        private void TreeGridValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            this.TreeGrid.CellValueChanged -= this.TreeGridValueChanged;
            Category category = TreeGrid.Rows[e.RowIndex].Tag as Category;
            PortfolioSecurity portfolio = TreeGrid.Rows[e.RowIndex].Tag as PortfolioSecurity;
            int scenarioIndex = (e.ColumnIndex - 28) / 3;
            int propertyIndex = (e.ColumnIndex - 28) % 3;

            object cellValue = TreeGrid[e.ColumnIndex, e.RowIndex].Value;
            if (category != null && e.ColumnIndex >= 28)
            {
                string value = cellValue.ToString();
                foreach (PortfolioSecurity security in category.AllSecurities)
                {
                    if (security.ScenarioValues.Count < scenarioIndex) continue;

                    SecurityScenarioValues scenario = security.ScenarioValues[scenarioIndex];
                    if (propertyIndex == 0 && !scenario.PrepayValueDefined) scenario.Prepay = value;
                    else if (propertyIndex == 1 && !scenario.DefaultValueDefined) scenario.Default = value;
                    else if (propertyIndex == 2 && !scenario.SeverityValueDefined) scenario.Severity = value;
                }
            }
            else if (portfolio != null && e.ColumnIndex >= 28 && portfolio.ScenarioValues.Count >= scenarioIndex)
            {
                string value = cellValue.ToString();
                SecurityScenarioValues scenario = portfolio.ScenarioValues[scenarioIndex];
                if (propertyIndex == 0)
                {
                    scenario.Prepay = value;
                    scenario.PrepayValueDefined = true;
                }
                else if (propertyIndex == 1)
                {
                    scenario.Default = value;
                    scenario.DefaultValueDefined = true;
                }
                else if (propertyIndex == 2)
                {
                    scenario.Severity = value;
                    scenario.SeverityValueDefined = true;
                }
            }
            this.TreeGrid.CellValueChanged += this.TreeGridValueChanged;
        }

        public override void LoadFromWorkspace()
        {
            base.LoadFromWorkspace();

            this.SetVectorAutoCompleteList();
        }

        private void SetVectorAutoCompleteList()
        {
            string[] vectorNames = Context.Workspace.Vectors.Select(v => v.Name).ToArray();

            this.spreadToDiscMarginIndexDataGridViewComboBoxColumn.ValueList = vectorNames;

            for (int i = this.FixedColumnCount; i < this.TreeGrid.Columns.Count; i++)
            {
                AutoCompleteColumn column = this.TreeGrid.Columns[i] as AutoCompleteColumn;
                if (column != null)
                {
                    column.ValueList = vectorNames;
                }
            }
        }

        private void TreeGridCellParsing(object sender, DataGridViewCellParsingEventArgs e)
        {
            if (this.TreeGrid.Columns[e.ColumnIndex].DefaultCellStyle.Format == PercentageFormat)
            {
                if (e.Value != null)
                {
                    string value = e.Value.ToString().Trim().Replace(" ", "");
                    bool percent = value.EndsWith("%");
                    value = value.Replace("%", "");

                    double parsedValue;
                    e.ParsingApplied = double.TryParse(value, out parsedValue);
                    if (e.ParsingApplied)
                    {
                        // if (percent)
                        {
                            parsedValue /= 100;
                        }
                        e.Value = parsedValue;
                    }
                }
            }
        }

        private void TreeGridCellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (this.TreeGrid.Columns[e.ColumnIndex].DefaultCellStyle.Format == PercentageFormat)
            {
                if (e.Value != null && !e.FormattingApplied)
                {
                    string str = e.Value.ToString();
                    if (str.Contains("%"))
                    {
                        return;
                    }
                    double? d = Helpers.TryConvertToDouble(str);

                    if (d != null)
                    {
                        e.Value = (double) d;
                    }
                }
            }
        }

        private void TreeGridOnCellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 2)
            {
                return;
            }

            PortfolioSecurity security = this.TreeGrid.Rows[e.RowIndex].Tag as PortfolioSecurity;
            if (security == null)
            {
                return;
            }

            DataGridViewCell cell = this.TreeGrid[e.ColumnIndex, e.RowIndex];
            this.PortfolioSecurityMapper.SetPropertyByIndex(security, e.ColumnIndex, cell.Value);

            if (e.ColumnIndex == this.originalAmtDataGridViewTextBoxColumn.Index)
            {
                this.UpdateCell(security, this.currentAmountDataGridViewTextBoxColumn.Index, e.RowIndex);
            }

            if (e.ColumnIndex == this.currentAmountDataGridViewTextBoxColumn.Index)
            {
                this.UpdateCell(security, this.originalAmtDataGridViewTextBoxColumn.Index, e.RowIndex);
            }

            Context.EventAggregator.Publish(new SecurityChangedEvent {Security = security});
        }

        private void UpdateCell(PortfolioSecurity security, int columnIndex, int rowIndex)
        {
            this.TreeGrid[columnIndex, rowIndex].Value =
                this.PortfolioSecurityMapper.GetPropertyByIndex(security, columnIndex);
        }


        private void TreeGridMouseDown(object sender, MouseEventArgs e)
        {
            DataGridView.HitTestInfo hitTestInfo = this.TreeGrid.HitTest(e.X, e.Y);
            if (e.Button == MouseButtons.Right)
            {
                if (hitTestInfo.RowIndex >= 2)
                {
                    this.TreeGrid.ClearSelection();
                    this.TreeGrid.Rows[hitTestInfo.RowIndex].Selected = true;
                }
                return;
            }

            if (hitTestInfo == null ||
                hitTestInfo.Type != DataGridViewHitTestType.RowHeader)
            {
                return;
            }
            TreeGridNode node = this.TreeGrid.GetNodeForRow(hitTestInfo.RowIndex);

            if (node == null || node.Tag == null)
            {
                return;
            }

            this.TreeGrid.DoDragDrop(node, DragDropEffects.Move);
        }

        private void TreeGridDragOver(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.None;
            if (!e.Data.GetDataPresent(typeof (TreeGridNode)))
            {
                return;
            }

            TreeGridNode sourceNode = (TreeGridNode) e.Data.GetData(typeof (TreeGridNode));

            Point pt = this.TreeGrid.PointToClient(new Point(e.X, e.Y));

            DataGridView.HitTestInfo hitTestInfo = this.TreeGrid.HitTest(pt.X, pt.Y);
            if (hitTestInfo == null ||
                (hitTestInfo.Type != DataGridViewHitTestType.RowHeader &&
                 hitTestInfo.Type != DataGridViewHitTestType.Cell))
            {
                return;
            }
            TreeGridNode destinationNode = this.TreeGrid.GetNodeForRow(hitTestInfo.RowIndex);

            if (destinationNode == null || destinationNode.Tag == null)
            {
                return;
            }

            if (destinationNode.Tag is Security)
            {
                return;
            }

            if (destinationNode == sourceNode)
            {
                return;
            }

            if (this.IsParentOf(sourceNode, destinationNode))
            {
                return;
            }

            if (destinationNode.Nodes.Contains(sourceNode))
            {
                return;
            }

            e.Effect = DragDropEffects.Move;
        }

        private bool IsParentOf(TreeGridNode sourceNode, TreeGridNode destinationNode)
        {
            if (sourceNode.Nodes.Contains(destinationNode))
            {
                return true;
            }
            foreach (TreeGridNode childNode in sourceNode.Nodes)
            {
                if (this.IsParentOf(childNode, destinationNode))
                {
                    return true;
                }
            }
            return false;
        }

        private void TreeGridDragDrop(object sender, DragEventArgs e)
        {
            if (!e.Data.GetDataPresent(typeof (TreeGridNode)))
            {
                return;
            }

            TreeGridNode sourceNode = (TreeGridNode) e.Data.GetData(typeof (TreeGridNode));

            Point pt = this.TreeGrid.PointToClient(new Point(e.X, e.Y));

            DataGridView.HitTestInfo hitTestInfo = this.TreeGrid.HitTest(pt.X, pt.Y);
            if (hitTestInfo == null || hitTestInfo.RowIndex < 0)
            {
                return;
            }

            INode destinationNode = this.TreeGrid.GetNodeForRow(hitTestInfo.RowIndex);

            Category destinationCategory = ((TreeGridNode) destinationNode).Tag as Category;
            if (destinationCategory == null)
            {
                return;
            }

            if (destinationCategory.Parent == null)
            {
                destinationNode = this.TreeGrid;
            }

            if (sourceNode.Tag == null)
            {
                return;
            }

            if (sourceNode.Tag is PortfolioSecurity)
            {
                PortfolioSecurity sourceSecurity = (PortfolioSecurity) sourceNode.Tag;

                sourceNode.Parent.Nodes.Remove(sourceNode);
                this.AddSecurity(sourceSecurity, destinationNode);

                sourceSecurity.SetupCategory.Securities.Remove(sourceSecurity);
                destinationCategory.Securities.Add(sourceSecurity);
                sourceSecurity.SetupCategory = destinationCategory;
            }
            else if (sourceNode.Tag is Category)
            {
                Category sourceCategory = (Category) sourceNode.Tag;

                sourceNode.Parent.Nodes.Remove(sourceNode);
                this.AddCategory(sourceCategory, destinationNode);

                sourceCategory.Parent.Categories.Remove(sourceCategory);
                destinationCategory.Categories.Add(sourceCategory);
                sourceCategory.Parent = destinationCategory;
            }

            this.TreeGrid.AutoResizeColumn(0);
        }
    }
}