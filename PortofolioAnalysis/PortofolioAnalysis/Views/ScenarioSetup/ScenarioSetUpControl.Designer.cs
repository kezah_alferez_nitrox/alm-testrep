﻿using ALMSCommon.Controls.TreeGridView;

namespace PortfolioAnalysis.Views.ScenarioSetup
{
    partial class ScenarioSetUpControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.securitiesFilter = new PortfolioAnalysis.Views.ScenarioSetup.SecuritiesFilter();
            this.searchButton = new System.Windows.Forms.Button();
            this.portfolioListBox = new System.Windows.Forms.ListBox();
            this.addAllFromPortfolioButton = new System.Windows.Forms.Button();
            this.treeGrid = new TreeGridView();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1 = new System.Windows.Forms.Panel();
            this.editScenariosButton = new System.Windows.Forms.Button();
            this.comboScenarios = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.treeGrid)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.securitiesFilter);
            this.splitContainer1.Panel1.Controls.Add(this.searchButton);
            this.splitContainer1.Panel1.Controls.Add(this.portfolioListBox);
            this.splitContainer1.Panel1.Controls.Add(this.addAllFromPortfolioButton);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.treeGrid);
            this.splitContainer1.Panel2.Controls.Add(this.panel1);
            this.splitContainer1.Size = new System.Drawing.Size(749, 460);
            this.splitContainer1.SplitterDistance = 204;
            this.splitContainer1.TabIndex = 2;
            // 
            // securitiesFilter
            // 
            this.securitiesFilter.Dock = System.Windows.Forms.DockStyle.Top;
            this.securitiesFilter.Location = new System.Drawing.Point(0, 0);
            this.securitiesFilter.Name = "securitiesFilter";
            this.securitiesFilter.Size = new System.Drawing.Size(202, 205);
            this.securitiesFilter.TabIndex = 47;
            // 
            // searchButton
            // 
            this.searchButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.searchButton.Image = global::PortfolioAnalysis.Properties.Resources.find;
            this.searchButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.searchButton.Location = new System.Drawing.Point(13, 211);
            this.searchButton.Name = "searchButton";
            this.searchButton.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.searchButton.Size = new System.Drawing.Size(179, 32);
            this.searchButton.TabIndex = 46;
            this.searchButton.Text = "Search";
            this.searchButton.UseVisualStyleBackColor = true;
            this.searchButton.Click += new System.EventHandler(this.SearchButtonClick);
            // 
            // portfolioListBox
            // 
            this.portfolioListBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.portfolioListBox.DisplayMember = "Name";
            this.portfolioListBox.FormattingEnabled = true;
            this.portfolioListBox.Location = new System.Drawing.Point(13, 267);
            this.portfolioListBox.Name = "portfolioListBox";
            this.portfolioListBox.Size = new System.Drawing.Size(179, 147);
            this.portfolioListBox.TabIndex = 2;
            this.portfolioListBox.ValueMember = "Self";
            this.portfolioListBox.SelectedValueChanged += new System.EventHandler(this.PortfolioListBoxSelectedValueChanged);
            // 
            // addAllFromPortfolioButton
            // 
            this.addAllFromPortfolioButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.addAllFromPortfolioButton.Image = global::PortfolioAnalysis.Properties.Resources.add;
            this.addAllFromPortfolioButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.addAllFromPortfolioButton.Location = new System.Drawing.Point(13, 420);
            this.addAllFromPortfolioButton.Name = "addAllFromPortfolioButton";
            this.addAllFromPortfolioButton.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.addAllFromPortfolioButton.Size = new System.Drawing.Size(179, 32);
            this.addAllFromPortfolioButton.TabIndex = 0;
            this.addAllFromPortfolioButton.Text = "Add all from portfolio";
            this.addAllFromPortfolioButton.UseVisualStyleBackColor = true;
            this.addAllFromPortfolioButton.Click += new System.EventHandler(this.AddAllFromPortfolioButtonClick);
            // 
            // treeGrid
            // 
            this.treeGrid.AllowDrop = true;
            this.treeGrid.AllowUserToAddRows = false;
            this.treeGrid.AllowUserToDeleteRows = false;
            this.treeGrid.AllowUserToResizeRows = false;
            this.treeGrid.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.treeGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.treeGrid.ColumnHeadersVisible = false;
            this.treeGrid.ContextMenuStrip = this.contextMenuStrip1;
            this.treeGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeGrid.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.treeGrid.ImageList = null;
            this.treeGrid.Location = new System.Drawing.Point(0, 42);
            this.treeGrid.Name = "treeGrid";
            this.treeGrid.Size = new System.Drawing.Size(539, 416);
            this.treeGrid.TabIndex = 3;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deleteToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(159, 26);
            // 
            // deleteToolStripMenuItem
            // 
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Delete)));
            this.deleteToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.deleteToolStripMenuItem.Text = "Delete";
            this.deleteToolStripMenuItem.Click += new System.EventHandler(this.DeleteToolStripMenuItemClick);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.editScenariosButton);
            this.panel1.Controls.Add(this.comboScenarios);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(539, 42);
            this.panel1.TabIndex = 2;
            // 
            // editScenariosButton
            // 
            this.editScenariosButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.editScenariosButton.Image = global::PortfolioAnalysis.Properties.Resources.add;
            this.editScenariosButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.editScenariosButton.Location = new System.Drawing.Point(373, 5);
            this.editScenariosButton.Name = "editScenariosButton";
            this.editScenariosButton.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.editScenariosButton.Size = new System.Drawing.Size(164, 32);
            this.editScenariosButton.TabIndex = 3;
            this.editScenariosButton.Text = "Edit Scenarios ...";
            this.editScenariosButton.UseVisualStyleBackColor = true;
            this.editScenariosButton.Click += new System.EventHandler(this.EditScenariosButtonClick);
            // 
            // comboScenarios
            // 
            this.comboScenarios.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.comboScenarios.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboScenarios.FormattingEnabled = true;
            this.comboScenarios.Location = new System.Drawing.Point(179, 11);
            this.comboScenarios.Name = "comboScenarios";
            this.comboScenarios.Size = new System.Drawing.Size(188, 21);
            this.comboScenarios.TabIndex = 2;
            this.comboScenarios.SelectedIndexChanged += new System.EventHandler(this.ComboScenariousSelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(112, 16);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(63, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "Scenarios : ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Scenario details";
            // 
            // ScenarioSetUpControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainer1);
            this.Name = "ScenarioSetUpControl";
            this.Size = new System.Drawing.Size(749, 460);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.treeGrid)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button addAllFromPortfolioButton;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox portfolioListBox;
        private TreeGridView treeGrid;
        private System.Windows.Forms.ComboBox comboScenarios;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button searchButton;
        private System.Windows.Forms.Button editScenariosButton;
        private SecuritiesFilter securitiesFilter;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;

    }
}
