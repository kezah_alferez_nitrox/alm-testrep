﻿namespace PortfolioAnalysis.Views.ScenarioSetup
{
    partial class SecuritiesFilter
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.issueDateDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.ratingsComboBox = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.ratingsRangeComboBox = new System.Windows.Forms.ComboBox();
            this.industryComboBox = new System.Windows.Forms.ComboBox();
            this.issueDateRangeComboBox = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.collateralComboBox = new System.Windows.Forms.ComboBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.issueDateDateTimePicker);
            this.groupBox1.Controls.Add(this.ratingsComboBox);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.ratingsRangeComboBox);
            this.groupBox1.Controls.Add(this.industryComboBox);
            this.groupBox1.Controls.Add(this.issueDateRangeComboBox);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.collateralComboBox);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(198, 205);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Filter Securities";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 109);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 13);
            this.label4.TabIndex = 15;
            this.label4.Text = "Issue Date :";
            // 
            // issueDateDateTimePicker
            // 
            this.issueDateDateTimePicker.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.issueDateDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.issueDateDateTimePicker.Location = new System.Drawing.Point(109, 124);
            this.issueDateDateTimePicker.Name = "issueDateDateTimePicker";
            this.issueDateDateTimePicker.Size = new System.Drawing.Size(79, 20);
            this.issueDateDateTimePicker.TabIndex = 20;
            this.issueDateDateTimePicker.ValueChanged += new System.EventHandler(this.issueDateDateTimePicker_ValueChanged);
            // 
            // ratingsComboBox
            // 
            this.ratingsComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.ratingsComboBox.DisplayMember = "Value";
            this.ratingsComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ratingsComboBox.FormattingEnabled = true;
            this.ratingsComboBox.Items.AddRange(new object[] {
            "AA+",
            "AA",
            "AA-",
            "A+",
            "A",
            "A-",
            "",
            "BBB+",
            "BBB",
            "BBB-",
            "BB+",
            "BB",
            "BB-",
            "B+",
            "B",
            "B-",
            "CCC+",
            "CCC",
            "CCC-",
            "CC+",
            "C",
            "D"});
            this.ratingsComboBox.Location = new System.Drawing.Point(109, 169);
            this.ratingsComboBox.Name = "ratingsComboBox";
            this.ratingsComboBox.Size = new System.Drawing.Size(79, 21);
            this.ratingsComboBox.TabIndex = 19;
            this.ratingsComboBox.ValueMember = "Key";
            this.ratingsComboBox.SelectedIndexChanged += new System.EventHandler(this.ratingsComboBox_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Industry :";
            // 
            // ratingsRangeComboBox
            // 
            this.ratingsRangeComboBox.DisplayMember = "Value";
            this.ratingsRangeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ratingsRangeComboBox.FormattingEnabled = true;
            this.ratingsRangeComboBox.Items.AddRange(new object[] {
            "",
            "Below",
            "Above and incl.",
            "Below and incl."});
            this.ratingsRangeComboBox.Location = new System.Drawing.Point(6, 169);
            this.ratingsRangeComboBox.Name = "ratingsRangeComboBox";
            this.ratingsRangeComboBox.Size = new System.Drawing.Size(97, 21);
            this.ratingsRangeComboBox.TabIndex = 18;
            this.ratingsRangeComboBox.ValueMember = "Key";
            this.ratingsRangeComboBox.SelectedIndexChanged += new System.EventHandler(this.ratingsRangeComboBox_SelectedIndexChanged);
            // 
            // industryComboBox
            // 
            this.industryComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.industryComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.industryComboBox.FormattingEnabled = true;
            this.industryComboBox.Location = new System.Drawing.Point(6, 34);
            this.industryComboBox.Name = "industryComboBox";
            this.industryComboBox.Size = new System.Drawing.Size(182, 21);
            this.industryComboBox.TabIndex = 12;
            this.industryComboBox.SelectedIndexChanged += new System.EventHandler(this.industryComboBox_SelectedIndexChanged);
            // 
            // issueDateRangeComboBox
            // 
            this.issueDateRangeComboBox.DisplayMember = "Value";
            this.issueDateRangeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.issueDateRangeComboBox.FormattingEnabled = true;
            this.issueDateRangeComboBox.Items.AddRange(new object[] {
            "",
            "Below",
            "Above and incl.",
            "Below and incl."});
            this.issueDateRangeComboBox.Location = new System.Drawing.Point(6, 124);
            this.issueDateRangeComboBox.Name = "issueDateRangeComboBox";
            this.issueDateRangeComboBox.Size = new System.Drawing.Size(97, 21);
            this.issueDateRangeComboBox.TabIndex = 17;
            this.issueDateRangeComboBox.ValueMember = "Key";
            this.issueDateRangeComboBox.SelectedIndexChanged += new System.EventHandler(this.issueDateRangeComboBox_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 64);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = "Collateral :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(4, 154);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 13);
            this.label5.TabIndex = 16;
            this.label5.Text = "Ratings :";
            // 
            // collateralComboBox
            // 
            this.collateralComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.collateralComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.collateralComboBox.FormattingEnabled = true;
            this.collateralComboBox.Location = new System.Drawing.Point(8, 79);
            this.collateralComboBox.Name = "collateralComboBox";
            this.collateralComboBox.Size = new System.Drawing.Size(180, 21);
            this.collateralComboBox.TabIndex = 14;
            this.collateralComboBox.SelectedIndexChanged += new System.EventHandler(this.collateralComboBox_SelectedIndexChanged);
            // 
            // SecuritiesFilter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox1);
            this.Name = "SecuritiesFilter";
            this.Size = new System.Drawing.Size(198, 205);
            this.Load += new System.EventHandler(this.SecuritiesFilterLoad);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker issueDateDateTimePicker;
        private System.Windows.Forms.ComboBox ratingsComboBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox ratingsRangeComboBox;
        private System.Windows.Forms.ComboBox industryComboBox;
        private System.Windows.Forms.ComboBox issueDateRangeComboBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox collateralComboBox;
    }
}
