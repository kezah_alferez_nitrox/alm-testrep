﻿using System;
using System.Drawing;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Forms;
using Eu.AlterSystems.ASNetLib.Core;
using PortfolioAnalysis.Properties;
using PortofolioAnalysis.Common.DatabaseEntities;
using PortofolioAnalysis.Common.Entities;

namespace PortfolioAnalysis.Views.ScenarioSetup
{
    public partial class FilterPortfolios : Form
    {
        private DataGridViewTextBoxColumn collateralTypeDataGridViewTextBoxColumn;
        private DataGridViewColumn[] columns;
        private DataGridViewTextBoxColumn couponDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn currentAmountDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn cusipDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn factorDataGridViewTextBoxColumn;
        private DataGridViewComboBoxColumn fitchDataGridViewComboBoxColumn;
        private DataGridViewTextBoxColumn givenAmtDataGridViewTextBoxColumn;
        private DataGridViewComboBoxColumn givenTypeDataGridViewComboBoxColumn;
        private DataGridViewTextBoxColumn industrySectorDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn isinDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn issueDatePriceDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn maturityDatePriceDataGridViewTextBoxColumn;
        private DataGridViewComboBoxColumn moodysSpecDataGridViewComboBoxColumn;
        private DataGridViewTextBoxColumn noOfstepsDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn originalAmtDescriptionDataGridViewTextBoxColumn;
        private DataGridViewCheckBoxColumn securityDataGridViewCheckBoxColumn;
        private DataGridViewTextBoxColumn securityDescriptionDataGridViewTextBoxColumn;
        private DataGridViewComboBoxColumn spSpecDataGridViewComboBoxColumn;
        private DataGridViewTextBoxColumn spreadToDiscMarginIndexDataGridViewComboBoxColumn;
        private DataGridViewTextBoxColumn stepsAmtDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn userNotesDataGridViewTextBoxColumn;
        private DataGridViewImageColumn validatedDataGridViewImageColumn;

        private readonly IList<PortfolioSecurity> allSecurities;
        private bool addAll;

        public FilterPortfolios()
        {
            this.InitializeComponent();
            this.CreateColumns();

            this.grid.AutoGenerateColumns = false;

            this.allSecurities = Context.Workspace.AllSecurities;
        }

        private void CreateColumns()
        {
            this.securityDataGridViewCheckBoxColumn = new DataGridViewCheckBoxColumn();
            this.validatedDataGridViewImageColumn = new DataGridViewImageColumn();
            this.cusipDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            this.isinDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            this.securityDescriptionDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            this.originalAmtDescriptionDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            this.factorDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            this.currentAmountDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            this.moodysSpecDataGridViewComboBoxColumn = new DataGridViewComboBoxColumn();
            this.spSpecDataGridViewComboBoxColumn = new DataGridViewComboBoxColumn();
            this.fitchDataGridViewComboBoxColumn = new DataGridViewComboBoxColumn();
            this.couponDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            this.issueDatePriceDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            this.maturityDatePriceDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            this.collateralTypeDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            this.industrySectorDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            this.givenTypeDataGridViewComboBoxColumn = new DataGridViewComboBoxColumn();
            this.givenAmtDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            this.spreadToDiscMarginIndexDataGridViewComboBoxColumn = new DataGridViewTextBoxColumn();
            this.noOfstepsDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            this.stepsAmtDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            this.userNotesDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();

            // 
            // securityDataGridViewCheckBoxColumn
            // 
            this.securityDataGridViewCheckBoxColumn.HeaderText = "";
            this.securityDataGridViewCheckBoxColumn.Name = "Security";
            this.securityDataGridViewCheckBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            // 
            // validatedDataGridViewImageColumn
            // 
            this.validatedDataGridViewImageColumn.DataPropertyName = "Status";
            this.validatedDataGridViewImageColumn.HeaderText = "Validated?";
            this.validatedDataGridViewImageColumn.Name = "Validated";
            this.validatedDataGridViewImageColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            // 
            // cusipDataGridViewTextBoxColumn
            // 
            this.cusipDataGridViewTextBoxColumn.DataPropertyName = "Id";
            this.cusipDataGridViewTextBoxColumn.HeaderText = "CUSIP";
            this.cusipDataGridViewTextBoxColumn.Name = "CUSIP";
            this.cusipDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            // 
            // isinDataGridViewTextBoxColumn
            // 
            this.isinDataGridViewTextBoxColumn.DataPropertyName = "Isin";
            this.isinDataGridViewTextBoxColumn.HeaderText = "ISIN";
            this.isinDataGridViewTextBoxColumn.Name = "ISIN";
            this.isinDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            // 
            // securityDescriptionDataGridViewTextBoxColumn
            // 
            this.securityDescriptionDataGridViewTextBoxColumn.DataPropertyName = "Description";
            this.securityDescriptionDataGridViewTextBoxColumn.HeaderText = "Security Description";
            this.securityDescriptionDataGridViewTextBoxColumn.Name = "SecurityDescription";
            this.securityDescriptionDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            // 
            // originalAmtDescriptionDataGridViewTextBoxColumn
            // 
            this.originalAmtDescriptionDataGridViewTextBoxColumn.DataPropertyName = "AmountOutstanding";
            this.originalAmtDescriptionDataGridViewTextBoxColumn.HeaderText = "Amount Outstanding";
            this.originalAmtDescriptionDataGridViewTextBoxColumn.Name = "AmountOutstanding";
            this.originalAmtDescriptionDataGridViewTextBoxColumn.DefaultCellStyle.Format = "N2";
            this.originalAmtDescriptionDataGridViewTextBoxColumn.DefaultCellStyle.Alignment =
                DataGridViewContentAlignment.MiddleRight;
            this.originalAmtDescriptionDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            // 
            // factorDataGridViewTextBoxColumn
            // 
            this.factorDataGridViewTextBoxColumn.DataPropertyName = "Factor";
            this.factorDataGridViewTextBoxColumn.HeaderText = "Factor";
            this.factorDataGridViewTextBoxColumn.Name = "Factor";
            this.factorDataGridViewTextBoxColumn.DefaultCellStyle.Format = "N3";
            this.factorDataGridViewTextBoxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.factorDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            // 
            // currentAmountDataGridViewTextBoxColumn
            // 
            this.currentAmountDataGridViewTextBoxColumn.DataPropertyName = "CurrentAmount";
            this.currentAmountDataGridViewTextBoxColumn.HeaderText = "Current Amount";
            this.currentAmountDataGridViewTextBoxColumn.Name = "CurrentAmount";
            this.currentAmountDataGridViewTextBoxColumn.DefaultCellStyle.Format = "N2";
            this.currentAmountDataGridViewTextBoxColumn.DefaultCellStyle.Alignment =
                DataGridViewContentAlignment.MiddleRight;
            this.currentAmountDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            // 
            // moodysSpecDataGridViewComboBoxColumn
            // 
            this.moodysSpecDataGridViewComboBoxColumn.DataPropertyName = "RatingMoodys";
            this.moodysSpecDataGridViewComboBoxColumn.HeaderText = "Moody's";
            this.moodysSpecDataGridViewComboBoxColumn.Name = "Moodys";
            this.moodysSpecDataGridViewComboBoxColumn.DisplayMember = "Value";
            this.moodysSpecDataGridViewComboBoxColumn.ValueMember = "Key";
            this.moodysSpecDataGridViewComboBoxColumn.DataSource = EnumHelper.ToList(typeof(Rating));
            this.moodysSpecDataGridViewComboBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.moodysSpecDataGridViewComboBoxColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;
            // 
            // spSpecDataGridViewComboBoxColumn
            // 
            this.spSpecDataGridViewComboBoxColumn.DataPropertyName = "RatingSandP";
            this.spSpecDataGridViewComboBoxColumn.HeaderText = "S&P";
            this.spSpecDataGridViewComboBoxColumn.Name = "SP";
            this.spSpecDataGridViewComboBoxColumn.DisplayMember = "Value";
            this.spSpecDataGridViewComboBoxColumn.ValueMember = "Key";
            this.spSpecDataGridViewComboBoxColumn.DataSource = EnumHelper.ToList(typeof(Rating));
            this.spSpecDataGridViewComboBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.spSpecDataGridViewComboBoxColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;
            // 
            // fitchDataGridViewComboBoxColumn
            // 
            this.fitchDataGridViewComboBoxColumn.DataPropertyName = "RatingFitch";
            this.fitchDataGridViewComboBoxColumn.HeaderText = "Fitch";
            this.fitchDataGridViewComboBoxColumn.Name = "Fitch";
            this.fitchDataGridViewComboBoxColumn.DisplayMember = "Value";
            this.fitchDataGridViewComboBoxColumn.ValueMember = "Key";
            this.fitchDataGridViewComboBoxColumn.DataSource = EnumHelper.ToList(typeof(Rating));
            this.fitchDataGridViewComboBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.fitchDataGridViewComboBoxColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;
            // 
            // couponDataGridViewTextBoxColumn
            // 
            this.couponDataGridViewTextBoxColumn.DataPropertyName = "Coupon";
            this.couponDataGridViewTextBoxColumn.HeaderText = "Coupon";
            this.couponDataGridViewTextBoxColumn.Name = "Coupon";
            this.couponDataGridViewTextBoxColumn.DefaultCellStyle.Format = "N3";
            this.couponDataGridViewTextBoxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.couponDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            // 
            // issueDatePriceDataGridViewTextBoxColumn
            // 
            this.issueDatePriceDataGridViewTextBoxColumn.DataPropertyName = "IssueDate";
            this.issueDatePriceDataGridViewTextBoxColumn.HeaderText = "Issue Date";
            this.issueDatePriceDataGridViewTextBoxColumn.Name = "IssueDate";
            this.issueDatePriceDataGridViewTextBoxColumn.DefaultCellStyle.Format = "yyyyMMdd";
            this.issueDatePriceDataGridViewTextBoxColumn.DefaultCellStyle.Alignment =
                DataGridViewContentAlignment.MiddleRight;
            this.issueDatePriceDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            // 
            // maturityDatePriceDataGridViewTextBoxColumn
            // 
            this.maturityDatePriceDataGridViewTextBoxColumn.DataPropertyName = "MaturityDate";
            this.maturityDatePriceDataGridViewTextBoxColumn.HeaderText = "Maturity Date";
            this.maturityDatePriceDataGridViewTextBoxColumn.Name = "MaturityDate";
            this.maturityDatePriceDataGridViewTextBoxColumn.DefaultCellStyle.Format = "yyyyMMdd";
            this.maturityDatePriceDataGridViewTextBoxColumn.DefaultCellStyle.Alignment =
                DataGridViewContentAlignment.MiddleRight;
            this.maturityDatePriceDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            // 
            // collateralTypeDataGridViewTextBoxColumn
            // 
            this.collateralTypeDataGridViewTextBoxColumn.DataPropertyName = "CollateralType";
            this.collateralTypeDataGridViewTextBoxColumn.HeaderText = "Collateral Type";
            this.collateralTypeDataGridViewTextBoxColumn.Name = "CollateralType";
            this.collateralTypeDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            // 
            // industrySectorDataGridViewTextBoxColumn
            // 
            this.industrySectorDataGridViewTextBoxColumn.DataPropertyName = "IndustrySector";
            this.industrySectorDataGridViewTextBoxColumn.HeaderText = "Industry Sector";
            this.industrySectorDataGridViewTextBoxColumn.Name = "IndustrySector";
            this.industrySectorDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            // 
            // givenTypeDataGridViewComboBoxColumn
            // 
            this.givenTypeDataGridViewComboBoxColumn.DataPropertyName = "GivenType";
            this.givenTypeDataGridViewComboBoxColumn.HeaderText = "Given Type";
            this.givenTypeDataGridViewComboBoxColumn.Name = "GivenType";
            this.givenTypeDataGridViewComboBoxColumn.DisplayMember = "Value";
            this.givenTypeDataGridViewComboBoxColumn.ValueMember = "Key";
            this.givenTypeDataGridViewComboBoxColumn.DataSource = EnumHelper.ToList(typeof(GivenType));
            this.givenTypeDataGridViewComboBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.givenTypeDataGridViewComboBoxColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;
            // 
            // givenAmtDataGridViewTextBoxColumn
            // 
            this.givenAmtDataGridViewTextBoxColumn.DataPropertyName = "GivenAmount";
            this.givenAmtDataGridViewTextBoxColumn.HeaderText = "Given Amt";
            this.givenAmtDataGridViewTextBoxColumn.Name = "GivenAmt";
            this.givenAmtDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            // 
            // spreadToDiscMarginIndexDataGridViewComboBoxColumn
            // 
            this.spreadToDiscMarginIndexDataGridViewComboBoxColumn.DataPropertyName = "SpreadToDiscMarginIndex";
            this.spreadToDiscMarginIndexDataGridViewComboBoxColumn.HeaderText = "Spread to/Disc Margin Index";
            this.spreadToDiscMarginIndexDataGridViewComboBoxColumn.Name = "SpreadToDiscMarginIndex";
            this.spreadToDiscMarginIndexDataGridViewComboBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.spreadToDiscMarginIndexDataGridViewComboBoxColumn.ValueType = typeof(string);
            // 
            // noOfstepsDataGridViewTextBoxColumn
            // 
            this.noOfstepsDataGridViewTextBoxColumn.DataPropertyName = "NoOfSteps";
            this.noOfstepsDataGridViewTextBoxColumn.HeaderText = "# of Steps";
            this.noOfstepsDataGridViewTextBoxColumn.Name = "NoOfSteps";
            this.noOfstepsDataGridViewTextBoxColumn.DefaultCellStyle.Format = "N0";
            this.noOfstepsDataGridViewTextBoxColumn.DefaultCellStyle.Alignment =
                DataGridViewContentAlignment.MiddleRight;
            this.noOfstepsDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            // 
            // stepsAmtDataGridViewTextBoxColumn
            // 
            this.stepsAmtDataGridViewTextBoxColumn.DataPropertyName = "StepAmount";
            this.stepsAmtDataGridViewTextBoxColumn.HeaderText = "Step Amt";
            this.stepsAmtDataGridViewTextBoxColumn.Name = "StepAmt";
            this.stepsAmtDataGridViewTextBoxColumn.DefaultCellStyle.Format = "N2";
            this.stepsAmtDataGridViewTextBoxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.stepsAmtDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            // 
            // userNotesDataGridViewTextBoxColumn
            // 
            this.userNotesDataGridViewTextBoxColumn.DataPropertyName = "UserNotes";
            this.userNotesDataGridViewTextBoxColumn.HeaderText = "User Notes";
            this.userNotesDataGridViewTextBoxColumn.Name = "UserNotes";
            this.userNotesDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;


            //Validated?	CUSIP	ISIN	Security Description	Original Amt	Factor	Current Amount	Moody's	S&P	Fitch	
            //Coupon	Issue Date	Maturity Date	Collateral Type	Industry Sector	Given Type	Given Amt	
            //Spread to/Disc Margin Index	# of Steps	Step Amt	User Notes

            this.columns = new DataGridViewColumn[]
                               {
                                   this.securityDataGridViewCheckBoxColumn,
                                   this.validatedDataGridViewImageColumn,
                                   this.cusipDataGridViewTextBoxColumn,
                                   this.isinDataGridViewTextBoxColumn,
                                   this.securityDescriptionDataGridViewTextBoxColumn,
                                   this.originalAmtDescriptionDataGridViewTextBoxColumn,
                                   this.factorDataGridViewTextBoxColumn,
                                   this.currentAmountDataGridViewTextBoxColumn,
                                   this.moodysSpecDataGridViewComboBoxColumn,
                                   this.spSpecDataGridViewComboBoxColumn,
                                   this.fitchDataGridViewComboBoxColumn,
                                   this.couponDataGridViewTextBoxColumn,
                                   this.issueDatePriceDataGridViewTextBoxColumn,
                                   this.maturityDatePriceDataGridViewTextBoxColumn,
                                   this.collateralTypeDataGridViewTextBoxColumn,
                                   this.industrySectorDataGridViewTextBoxColumn,
                                   this.givenTypeDataGridViewComboBoxColumn,
                                   this.givenAmtDataGridViewTextBoxColumn,
                                   this.spreadToDiscMarginIndexDataGridViewComboBoxColumn,
                                   this.noOfstepsDataGridViewTextBoxColumn,
                                   this.stepsAmtDataGridViewTextBoxColumn,
                                   this.userNotesDataGridViewTextBoxColumn,
                               };

            this.grid.Columns.Clear();
            this.grid.Columns.AddRange(this.columns);

            for (int i = 1; i < this.grid.Columns.Count; i++)
            {
                this.grid.Columns[i].ReadOnly = true;
            }

            this.grid.CellFormatting += this.DataGridViewCellFormatting;
        }

        private void DataGridViewCellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.ColumnIndex == this.validatedDataGridViewImageColumn.Index && !e.FormattingApplied)
            {
                if (e.Value == null || !e.Value.GetType().IsEnum)
                {
                    return;
                }

                e.Value = Helpers.GetStatusImage((Status)e.Value);
            }
        }

        public IList<PortfolioSecurity> Selection
        {
            get
            {
                List<PortfolioSecurity> result = new List<PortfolioSecurity>();
                foreach (DataGridViewRow row in grid.Rows)
                {
                    if(!this.addAll && !Convert.ToBoolean(row.Cells[0].Value)) continue;
                    result.Add((PortfolioSecurity) row.DataBoundItem);
                }
                return result;
            }
        }

        public bool MoveSecurities
        {
            get { return moveSecuritiesRadioButton.Checked; }
        }

        private void securitiesFilter_SelectionChanged(SecuritiesFilter filter)
        {
            IEnumerable<PortfolioSecurity> result = allSecurities;

            if (!string.IsNullOrEmpty(filter.IndustrySector))
            {
                result = result.Where(s => s.IndustrySector == filter.IndustrySector).ToList();
            }

            if (!string.IsNullOrEmpty(filter.CollateralType))
            {
                result = result.Where(s => s.CollateralType == filter.CollateralType).ToList();
            }

            if(filter.IssueDateRange ==SearchRange.Below)
            {
                result = result.Where(s => s.IssueDate < filter.IssueDate).ToList();
            }
            else if (filter.IssueDateRange == SearchRange.BelowAndInclude)
            {
                result = result.Where(s => s.IssueDate <= filter.IssueDate).ToList();
            }
            else if (filter.IssueDateRange == SearchRange.AboveAndInclude)
            {
                result = result.Where(s => s.IssueDate >= filter.IssueDate).ToList();
            }

            if(filter.RatingsRange == SearchRange.Below)
            {
                result = result.Where(s => s.RatingMoodys > filter.Ratings).ToList(); // the order of ratings enum values is inverted
            }
            else if (filter.RatingsRange == SearchRange.BelowAndInclude)
            {
                result = result.Where(s => s.RatingMoodys >= filter.Ratings).ToList(); // the order of ratings enum values is inverted
            }
            else if (filter.RatingsRange == SearchRange.AboveAndInclude)
            {
                result = result.Where(s => s.RatingMoodys < filter.Ratings).ToList(); // the order of ratings enum values is inverted
            }

            this.grid.AutoGenerateColumns = false;
            this.grid.DataSource = result;
        }

        public void InitFilter(SecuritiesFilter filter)
        {
            this.securitiesFilter.InitFrom(filter);
        }

        private void addSelectedButton_Click(object sender, EventArgs e)
        {
            this.addAll = false;
        }

        private void addAllButton_Click(object sender, EventArgs e)
        {
            this.addAll = true;
        }
    }

    public enum SearchRange
    {
        [EnumDescription("Below")]
        Below,
        [EnumDescription("Above and incl.")]
        AboveAndInclude,
        [EnumDescription("Below and incl.")]
        BelowAndInclude,
    }
}