﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Eu.AlterSystems.ASNetLib.Core;
using PortofolioAnalysis.Common.DatabaseEntities;
using PortofolioAnalysis.Common.Entities;

namespace PortfolioAnalysis.Views.ScenarioSetup
{
    public delegate void SelectionChangedHandler(SecuritiesFilter securitiesFilter);

    public partial class SecuritiesFilter : UserControl
    {
        private IList<PortfolioSecurity> allSecurities;
        private bool isInitialized = false;
        private SecuritiesFilter initFilter;

        public SecuritiesFilter()
        {
            InitializeComponent();

            BindEnumToCombo(typeof(SearchRange), this.issueDateRangeComboBox);
            BindEnumToCombo(typeof(SearchRange), this.ratingsRangeComboBox);

            this.ratingsComboBox.DataSource = EnumHelper.ToList(typeof(Rating));
        }

        private static void BindEnumToCombo(Type enumType, ComboBox comboBox)
        {
            IList list = EnumHelper.ToList(enumType);
            list.Insert(0, new KeyValuePair<Enum, string>(null, ""));
            comboBox.DataSource = list;
        }

        public event SelectionChangedHandler SelectionChanged;

        private void industryComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.FireSelectionChanged();
        }

        private void collateralComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.FireSelectionChanged();
        }

        private void issueDateRangeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.issueDateDateTimePicker.Enabled = this.issueDateRangeComboBox.SelectedIndex > 0;
            this.FireSelectionChanged();
        }

        private void issueDateDateTimePicker_ValueChanged(object sender, EventArgs e)
        {
            this.FireSelectionChanged();
        }

        private void ratingsRangeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.ratingsComboBox.Enabled = this.ratingsRangeComboBox.SelectedIndex > 0;
            this.FireSelectionChanged();
        }

        private void ratingsComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.FireSelectionChanged();
        }

        private void FireSelectionChanged()
        {
            if(!this.isInitialized) return;
            SelectionChangedHandler handler = this.SelectionChanged;
            if (handler != null)
            {
                handler(this);
            }
        }

        public string IndustrySector
        {
            get { return (string)this.industryComboBox.SelectedValue; }
        }

        public string CollateralType
        {
            get { return (string)this.collateralComboBox.SelectedValue; }
        }

        public SearchRange? IssueDateRange
        {
            get { return (SearchRange?)this.issueDateRangeComboBox.SelectedValue; }
        }

        public DateTime IssueDate
        {
            get { return this.issueDateDateTimePicker.Value; }
        }

        public SearchRange? RatingsRange
        {
            get { return (SearchRange?)this.ratingsRangeComboBox.SelectedValue; }
        }

        public Rating Ratings
        {
            get { return (Rating) this.ratingsComboBox.SelectedValue; }
        }

        private void SecuritiesFilterLoad(object sender, EventArgs e)
        {
            Reload();
        }

        public void Reload() 
        {
            this.allSecurities = Context.Workspace.AllSecurities;

            List<string> industrySectors = this.allSecurities.Select(s => s.IndustrySector).Where(s => s != null).Distinct().ToList();
            industrySectors.Insert(0, "");
            this.industryComboBox.DataSource = industrySectors;

            List<string> collateralTypes = this.allSecurities.Select(s => s.CollateralType).ToList().Where(s => s != null).ToList().Distinct().ToList();
            collateralTypes.Insert(0, "");
            this.collateralComboBox.DataSource = collateralTypes;

            if(this.initFilter != null)
            {
                this.industryComboBox.SelectedIndex = this.initFilter.industryComboBox.SelectedIndex;
                this.collateralComboBox.SelectedIndex = this.initFilter.collateralComboBox.SelectedIndex;
                this.issueDateRangeComboBox.SelectedIndex = this.initFilter.issueDateRangeComboBox.SelectedIndex;
                this.issueDateDateTimePicker.Value = this.initFilter.IssueDate;
                this.ratingsRangeComboBox.SelectedIndex = this.initFilter.ratingsRangeComboBox.SelectedIndex;
                this.ratingsComboBox.SelectedIndex = this.initFilter.ratingsComboBox.SelectedIndex;

                this.isInitialized = true;
                this.FireSelectionChanged();
            }
        }

        public void InitFrom(SecuritiesFilter filter)
        {
            this.initFilter = filter;
        }
    }
}
