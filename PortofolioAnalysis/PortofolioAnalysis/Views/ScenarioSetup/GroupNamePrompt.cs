﻿using System;
using System.Windows.Forms;
using Castle.ActiveRecord;

namespace PortfolioAnalysis.Views.ScenarioSetup
{
    public partial class GroupNamePrompt : Form
    {
        public GroupNamePrompt()
        {
            InitializeComponent();
        }

        public string GroupName
        {
            get { return this.textBox.Text; }
        }

        private void textBoxTextChanged(object sender, EventArgs e)
        {
            if(this.textBox.Text.Length == 0)
            {
                okButton.Enabled = false;
                return;
            }

            okButton.Enabled = true;
        }
    }
}