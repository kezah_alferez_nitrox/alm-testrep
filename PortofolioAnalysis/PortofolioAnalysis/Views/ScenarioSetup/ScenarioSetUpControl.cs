﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using PortfolioAnalysis.Infrastructure;
using PortofolioAnalysis.Common.Entities;

namespace PortfolioAnalysis.Views.ScenarioSetup
{
    public partial class ScenarioSetUpControl : UserControl, IHandle<WorkspaceLoadedEvent>, IHandle<DateFormatChangedEvent>
    {
        private readonly SecuritiesTreeGridController securitiesTreeGridController;

        public ScenarioSetUpControl()
        {
            this.InitializeComponent();

            this.securitiesTreeGridController = new ScenarioSetupTreeGridController(this.treeGrid);

            treeGrid.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;

            Context.EventAggregator.Subscribe(this);
        }

        public void HandleMessage(WorkspaceLoadedEvent message)
        {
            this.portfolioListBox.DataSource = Context.Workspace.Portfolios;
            this.comboScenarios.DataSource = Context.Workspace.Scenarios;
        }

        public void HandleMessage(DateFormatChangedEvent message)
        {
            DataGridViewUtil.UpdateDateFormat(this.treeGrid);
        }

        private void ComboScenariousSelectedIndexChanged(object sender, EventArgs e)
        {
            foreach (DataGridViewColumn column in this.treeGrid.Columns)
            {
                if (this.comboScenarios.SelectedItem != null &&
                    column.Name == this.comboScenarios.SelectedItem.ToString())
                {
                    this.treeGrid.FirstDisplayedScrollingColumnIndex = column.Index;
                    break;
                }
            }
        }

        private void SearchButtonClick(object sender, EventArgs e)
        {
            FilterPortfolios filterPortfolios = new FilterPortfolios();
            filterPortfolios.InitFilter(this.securitiesFilter);

            if (filterPortfolios.ShowDialog() == DialogResult.OK)
            {
                if (filterPortfolios.Selection.Count == 0) return;

                GroupNamePrompt groupNamePrompt = new GroupNamePrompt();

                BindingList<PortfolioSecurity> list;
                if (filterPortfolios.MoveSecurities)
                {
                    if (groupNamePrompt.ShowDialog() != DialogResult.OK) return;
                    RemoveExistingSecurities(Context.Workspace.SetupSecurities, filterPortfolios.Selection);
                    list = new BindingList<PortfolioSecurity>(filterPortfolios.Selection);
                }
                else
                {
                    list = GetSecuritiesNotInSetup(filterPortfolios.Selection);
                    if (list.Count == 0) return;
                    if (groupNamePrompt.ShowDialog() != DialogResult.OK) return;
                }

                Category category = new Category { Name = groupNamePrompt.GroupName, Securities = list };
                foreach (PortfolioSecurity security in list)
                {
                    security.SetupCategory = category;
                }
                Context.Workspace.SetupSecurities.Categories.Add(category);
                category.Parent = Context.Workspace.SetupSecurities;

                EnsureSecuritiesHaveScenarioValues();

                this.securitiesTreeGridController.LoadFromWorkspace();
            }
        }

        private void RemoveExistingSecurities(Category category, IEnumerable<PortfolioSecurity> selection)
        {
            IList<PortfolioSecurity> toRemove = new List<PortfolioSecurity>();
            foreach (PortfolioSecurity portfolioSecurity in selection)
            {
                if (category.Securities.Contains(portfolioSecurity))
                {
                    toRemove.Add(portfolioSecurity);
                }
            }
            foreach (PortfolioSecurity portfolioSecurity in toRemove)
            {
                category.Securities.Remove(portfolioSecurity);
            }

            foreach (Category child in category.Categories)
            {
                this.RemoveExistingSecurities(child, selection);
            }
        }

        private static BindingList<PortfolioSecurity> GetSecuritiesNotInSetup(IEnumerable<PortfolioSecurity> securities)
        {
            IList<PortfolioSecurity> allSetupSecurities = Context.Workspace.AllSetupSecurities;
            BindingList<PortfolioSecurity> list = new BindingList<PortfolioSecurity>();

            foreach (PortfolioSecurity security in securities)
            {
                if (allSetupSecurities.Contains(security)) continue;
                list.Add(security);
            }
            return list;
        }

        private void EditScenariosButtonClick(object sender, EventArgs e)
        {
            new ScenarioEditor().ShowDialog();

            EnsureSecuritiesHaveScenarioValues();

            this.securitiesTreeGridController.LoadFromWorkspace();
        }

        private static void EnsureSecuritiesHaveScenarioValues()
        {
            foreach (PortfolioSecurity security in Context.Workspace.AllSetupSecurities)
            {
                int delta = Context.Workspace.Scenarios.Count - security.ScenarioValues.Count;
                if(delta>0)
                {
                    for (int i = 0; i < delta; i++)
                    {
                        security.ScenarioValues.Add(new SecurityScenarioValues());
                    }
                }
            }
        }

        private void AddAllFromPortfolioButtonClick(object sender, EventArgs e)
        {
            Category portfolio = this.portfolioListBox.SelectedValue as Category;
            if (portfolio == null) return;

            // if (Context.Workspace.SetupSecurities.Categories.Any(c => c.Name == portfolio.Name)) return;

            IList<PortfolioSecurity> allSetupSecurities = Context.Workspace.AllSetupSecurities;

            int securityCount = portfolio.AllSecurities.Count(s => allSetupSecurities.All(ss => ss.Id != s.Id));
            if(securityCount == 0)return;

            AddCategoryRecursive(portfolio, Context.Workspace.SetupSecurities, allSetupSecurities);

            EnsureSecuritiesHaveScenarioValues();

            this.securitiesTreeGridController.LoadFromWorkspace();
        }

        private static void AddCategoryRecursive(Category category, Category parent, IEnumerable<PortfolioSecurity> exclude)
        {
            parent.Categories.Add(category);
        }

        private void PortfolioListBoxSelectedValueChanged(object sender, EventArgs e)
        {
            Category portfolio = this.portfolioListBox.SelectedValue as Category;
            addAllFromPortfolioButton.Enabled = portfolio != null;
        }

        private void DeleteToolStripMenuItemClick(object sender, EventArgs e)
        {
            this.securitiesTreeGridController.DeleteSelectedRows();
        }

        public void Shown()
        {
            this.securitiesFilter.Reload();
        }
    }
}