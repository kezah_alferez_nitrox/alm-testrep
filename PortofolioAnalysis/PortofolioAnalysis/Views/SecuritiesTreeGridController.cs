﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using ALMSCommon.Controls;
using ALMSCommon.Controls.TreeGridView;
using Eu.AlterSystems.ASNetLib.Core;
using PortfolioAnalysis.Controls.DataGridViewEx;
using PortfolioAnalysis.Infrastructure;
using PortofolioAnalysis.Common.DatabaseEntities;
using PortofolioAnalysis.Common.Entities;
using CouponType = PortofolioAnalysis.Common.DatabaseEntities.CouponType;

namespace PortfolioAnalysis.Views
{
    public class SecuritiesTreeGridController : IHandle<WorkspaceLoadedEvent>, IHandle<DateFormatChangedEvent>
    {
        private const DataGridViewAutoSizeColumnMode AutoSizeColumnMode =
            DataGridViewAutoSizeColumnMode.None;

        protected const string PercentageFormat = "P2";

        #region column declaration

        private DataGridViewTextBoxColumn collateralTypeDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn couponDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn couponFrequencyDataGridViewTextBoxColumn;

        private DataGridViewComboBoxColumn couponTypeDataGridViewComboBoxColumn;
        private DataGridViewTextBoxColumn indexDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn spreadDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn floatingFrequencyDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn resetDateDataGridViewTextBoxColumn;

        protected DataGridViewTextBoxColumn currentAmountDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn cusipDataGridViewTextBoxColumn;
        protected DataGridViewTextBoxColumn factorDataGridViewTextBoxColumn;
        private DataGridViewComboBoxColumn fitchDataGridViewComboBoxColumn;
        private DataGridViewTextBoxColumn givenAmtDataGridViewTextBoxColumn;
        private DataGridViewComboBoxColumn givenTypeDataGridViewComboBoxColumn;
        private DataGridViewTextBoxColumn industrySectorDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn isinDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn issueDatePriceDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn maturityDatePriceDataGridViewTextBoxColumn;
        private DataGridViewComboBoxColumn moodysSpecDataGridViewComboBoxColumn;
        private DataGridViewTextBoxColumn noOfstepsDataGridViewTextBoxColumn;
        protected DataGridViewTextBoxColumn originalAmtDataGridViewTextBoxColumn;
        private DataGridViewCheckBoxColumn excludeDataGridViewCheckBoxColumn;
        private DataGridViewComboBoxColumn spSpecDataGridViewComboBoxColumn;
        protected AutoCompleteColumn spreadToDiscMarginIndexDataGridViewComboBoxColumn;
        private DataGridViewTextBoxColumn stepsAmtDataGridViewTextBoxColumn;
        private TreeGridColumn treeGridColumn;
        private DataGridViewTextBoxColumn userNotesDataGridViewTextBoxColumn;
        private DataGridViewImageColumn validatedDataGridViewImageColumn;

        #endregion

        private DataGridViewCellStyle headerCellStyle;

        protected readonly TreeGridView TreeGrid;
        protected ScenarioSecurityArrayMapper PortfolioSecurityMapper;

        private readonly Font boldFont;
        protected int FixedColumnCount { get; private set; }

        public SecuritiesTreeGridController(TreeGridView treeGrid)
        {
            this.TreeGrid = treeGrid;
            this.boldFont = new Font(this.TreeGrid.Font, FontStyle.Bold);

            this.CreateColumns();

            this.TreeGrid.RowsAdded += this.OnRowsAdded;
            this.TreeGrid.DataError += OnDataError;

            // this.treeGrid.RowPostPaint += this.treeGrid_RowPostPaint;

            this.TreeGrid.NodeExpanded += (s, e) => treeGrid.AutoResizeColumn(0);
            this.TreeGrid.NodeCollapsed += (s, e) => treeGrid.AutoResizeColumn(0);

            this.TreeGrid.CellFormatting += this.TreeGridCellFormatting;

            Context.EventAggregator.Subscribe(this);
        }

        private void TreeGridCellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.RowIndex > 1 && e.ColumnIndex == this.validatedDataGridViewImageColumn.Index && !e.FormattingApplied)
            {
                if (e.Value == null || !e.Value.GetType().IsEnum)
                {
                    return;
                }

                e.Value = Helpers.GetStatusImage((Status)e.Value);
            }
        }

        public void HandleMessage(WorkspaceLoadedEvent message)
        {
            this.LoadFromWorkspace();
        }

        public void HandleMessage(DateFormatChangedEvent message)
        {
            DataGridViewUtil.UpdateDateFormat(this.TreeGrid);
        }

        public virtual void LoadFromWorkspace()
        {
            this.ClearTreeGrid();

            this.CreateFirstHeader();
            this.CreateSecondHeader();

            this.CreateScenarioColumns();

            this.LoadData();
        }

        private void CreateScenarioColumns()
        {
            IList<Scenario> scenarios = Context.Workspace.Scenarios.OrderBy(s => s.Position).ToList();
            int columnCountToRemove = this.TreeGrid.ColumnCount - this.FixedColumnCount;
            for (int i = 0; i < columnCountToRemove; i++)
            {
                this.TreeGrid.Columns.RemoveAt(this.FixedColumnCount);
            }

            foreach (Scenario scenario in scenarios)
            {
                AutoCompleteColumn prepayDataGridViewTextBoxColumn = new AutoCompleteColumn();
                AutoCompleteColumn defaultDataGridViewTextBoxColumn = new AutoCompleteColumn();
                AutoCompleteColumn severityDataGridViewTextBoxColumn = new AutoCompleteColumn();

                // 
                // scenarioBasePrepayDataGridViewTextBoxColumn
                // 
                prepayDataGridViewTextBoxColumn.HeaderText = "Prepay";
                prepayDataGridViewTextBoxColumn.Name = scenario + "Prepay";
                prepayDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
                prepayDataGridViewTextBoxColumn.DefaultCellStyle.Format = PercentageFormat;
                prepayDataGridViewTextBoxColumn.AutoSizeMode = AutoSizeColumnMode;
                // 
                // scenarioBaseDefaultDataGridViewTextBoxColumn
                // 
                defaultDataGridViewTextBoxColumn.HeaderText = "Default";
                defaultDataGridViewTextBoxColumn.Name = scenario + "Default";
                defaultDataGridViewTextBoxColumn.DefaultCellStyle.Alignment =
                    DataGridViewContentAlignment.MiddleRight;
                defaultDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
                defaultDataGridViewTextBoxColumn.DefaultCellStyle.Format = PercentageFormat;
                defaultDataGridViewTextBoxColumn.AutoSizeMode = AutoSizeColumnMode;
                // 
                // scenarioBaseSeverityDataGridViewTextBoxColumn
                // 
                severityDataGridViewTextBoxColumn.HeaderText = "Severity";
                severityDataGridViewTextBoxColumn.Name = scenario + "Severity";
                severityDataGridViewTextBoxColumn.DefaultCellStyle.Alignment =
                    DataGridViewContentAlignment.MiddleRight;
                severityDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
                severityDataGridViewTextBoxColumn.DefaultCellStyle.Format = PercentageFormat;
                severityDataGridViewTextBoxColumn.AutoSizeMode = AutoSizeColumnMode;

                this.TreeGrid.Columns.AddRange(prepayDataGridViewTextBoxColumn, defaultDataGridViewTextBoxColumn,
                                               severityDataGridViewTextBoxColumn);
            }

            for (int i = 0; i < scenarios.Count; i++)
            {
                int columnIndex = this.FixedColumnCount + i * 3;

                this.TreeGrid[columnIndex, 0] = this.CreateTopBottomBorderCell();
                this.TreeGrid[columnIndex + 1, 0] = this.CreateTopBottomBorderCell();
                this.TreeGrid[columnIndex + 2, 0] = this.CreateTopBottomBorderCell();

                this.TreeGrid[columnIndex, 0].Value = scenarios[i];
                this.TreeGrid[columnIndex, 0].Style.WrapMode = DataGridViewTriState.True;
                this.TreeGrid[columnIndex, 0].Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

                ((DataGridViewCellEx)this.TreeGrid[columnIndex + 2, 0]).DataGridViewAdvancedCellBorderStyle.Right =
                    DataGridViewAdvancedCellBorderStyle.Single;

                this.TreeGrid[columnIndex, 1] = this.CreateHeaderCell(this.TreeGrid.Columns[columnIndex].HeaderText);
                this.TreeGrid[columnIndex + 1, 1] =
                    this.CreateHeaderCell(this.TreeGrid.Columns[columnIndex + 1].HeaderText);
                this.TreeGrid[columnIndex + 2, 1] =
                    this.CreateHeaderCell(this.TreeGrid.Columns[columnIndex + 2].HeaderText);
            }
        }

        private DataGridViewTextBoxCell CreateHeaderCell(string headerText)
        {
            return new DataGridViewTextBoxCell
                       {
                           Value = headerText,
                           Style = this.headerCellStyle
                       };
        }

        private DataGridViewCellEx CreateTopBottomBorderCell()
        {
            return new DataGridViewCellEx
                       {
                           Value = "",
                           Style = this.headerCellStyle,
                           DataGridViewAdvancedCellBorderStyle =
                               new DataGridViewAdvancedBorderStyle
                                   {
                                       Top = DataGridViewAdvancedCellBorderStyle.Single,
                                       Bottom = DataGridViewAdvancedCellBorderStyle.Single
                                   }
                       };
        }

        private void CreateColumns()
        {
            List<DataGridViewColumn> dataGridViewColumns = this.GetColumns();
            DataGridViewColumn[] columns = dataGridViewColumns.ToArray();
            this.FixedColumnCount = columns.Length;

            this.TreeGrid.Columns.Clear();
            this.TreeGrid.Columns.AddRange(columns);
        }

        protected virtual List<DataGridViewColumn> GetColumns()
        {
            this.treeGridColumn = new TreeGridColumn();
            this.excludeDataGridViewCheckBoxColumn = new DataGridViewCheckBoxColumn();
            this.validatedDataGridViewImageColumn = new DataGridViewImageColumn();
            this.cusipDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            this.isinDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            //this.securityDescriptionDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            this.originalAmtDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            this.factorDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            this.currentAmountDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            this.moodysSpecDataGridViewComboBoxColumn = new DataGridViewComboBoxColumn();
            this.spSpecDataGridViewComboBoxColumn = new DataGridViewComboBoxColumn();
            this.fitchDataGridViewComboBoxColumn = new DataGridViewComboBoxColumn();
            this.couponDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            this.couponFrequencyDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();

            this.couponTypeDataGridViewComboBoxColumn = new DataGridViewComboBoxColumn();
            this.indexDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            this.spreadDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            this.floatingFrequencyDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            this.resetDateDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();

            this.issueDatePriceDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            this.maturityDatePriceDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            this.collateralTypeDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            this.industrySectorDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            this.givenTypeDataGridViewComboBoxColumn = new DataGridViewComboBoxColumn();
            this.givenAmtDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            this.spreadToDiscMarginIndexDataGridViewComboBoxColumn = new AutoCompleteColumn();
            this.noOfstepsDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            this.stepsAmtDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            this.userNotesDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();

            this.treeGridColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.treeGridColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
            this.treeGridColumn.Resizable = DataGridViewTriState.False;
            this.treeGridColumn.Frozen = true;
            this.treeGridColumn.DefaultCellStyle.Font = this.boldFont;
            this.treeGridColumn.ReadOnly = true;
            // 
            // excludeDataGridViewCheckBoxColumn
            // 
            this.excludeDataGridViewCheckBoxColumn.DataPropertyName = "Exclude";
            this.excludeDataGridViewCheckBoxColumn.HeaderText = "Exclude";
            this.excludeDataGridViewCheckBoxColumn.Name = "Exclude";
            this.excludeDataGridViewCheckBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.excludeDataGridViewCheckBoxColumn.AutoSizeMode = AutoSizeColumnMode;
            this.excludeDataGridViewCheckBoxColumn.Width = 150;
            this.excludeDataGridViewCheckBoxColumn.Frozen = true;
            // 
            // validatedDataGridViewImageColumn
            // 
            this.validatedDataGridViewImageColumn.DataPropertyName = "Status";
            this.validatedDataGridViewImageColumn.HeaderText = "Validated?";
            this.validatedDataGridViewImageColumn.Name = "Validated";
            this.validatedDataGridViewImageColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.validatedDataGridViewImageColumn.AutoSizeMode = AutoSizeColumnMode;
            this.validatedDataGridViewImageColumn.Frozen = true;
            // 
            // cusipDataGridViewTextBoxColumn
            // 
            this.cusipDataGridViewTextBoxColumn.DataPropertyName = "Cusip";
            this.cusipDataGridViewTextBoxColumn.HeaderText = "CUSIP";
            this.cusipDataGridViewTextBoxColumn.Name = "CUSIP";
            this.cusipDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.cusipDataGridViewTextBoxColumn.AutoSizeMode = AutoSizeColumnMode;
            this.cusipDataGridViewTextBoxColumn.Frozen = true;
            this.cusipDataGridViewTextBoxColumn.DefaultCellStyle.Font = this.boldFont;
            this.cusipDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // isinDataGridViewTextBoxColumn
            // 
            this.isinDataGridViewTextBoxColumn.DataPropertyName = "Isin";
            this.isinDataGridViewTextBoxColumn.HeaderText = "ISIN";
            this.isinDataGridViewTextBoxColumn.Name = "ISIN";
            this.isinDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.isinDataGridViewTextBoxColumn.AutoSizeMode = AutoSizeColumnMode;
            this.isinDataGridViewTextBoxColumn.Frozen = true;
            this.isinDataGridViewTextBoxColumn.DefaultCellStyle.Font = this.boldFont;
            this.isinDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // securityDescriptionDataGridViewTextBoxColumn
            // 
            //this.securityDescriptionDataGridViewTextBoxColumn.DataPropertyName = "SecurityDescription";
            //this.securityDescriptionDataGridViewTextBoxColumn.HeaderText = "Security Description";
            //this.securityDescriptionDataGridViewTextBoxColumn.Name = "SecurityDescription";
            //this.securityDescriptionDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            //this.securityDescriptionDataGridViewTextBoxColumn.AutoSizeMode = AutoSizeColumnMode;
            //this.securityDescriptionDataGridViewTextBoxColumn.Frozen = true;
            // 
            // originalAmtDataGridViewTextBoxColumn
            // 
            this.originalAmtDataGridViewTextBoxColumn.DataPropertyName = "OriginalAmount";
            this.originalAmtDataGridViewTextBoxColumn.HeaderText = "Original Amount";
            this.originalAmtDataGridViewTextBoxColumn.Name = "OriginalAmount";
            this.originalAmtDataGridViewTextBoxColumn.DefaultCellStyle.Format = "N2";
            this.originalAmtDataGridViewTextBoxColumn.DefaultCellStyle.Alignment =
                DataGridViewContentAlignment.MiddleRight;
            this.originalAmtDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.originalAmtDataGridViewTextBoxColumn.AutoSizeMode = AutoSizeColumnMode;
            this.originalAmtDataGridViewTextBoxColumn.ValueType = typeof(double);
            // 
            // factorDataGridViewTextBoxColumn
            // 
            this.factorDataGridViewTextBoxColumn.DataPropertyName = "Factor";
            this.factorDataGridViewTextBoxColumn.HeaderText = "Factor";
            this.factorDataGridViewTextBoxColumn.Name = "Factor";
            this.factorDataGridViewTextBoxColumn.DefaultCellStyle.Format = "N3";
            this.factorDataGridViewTextBoxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.factorDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.factorDataGridViewTextBoxColumn.AutoSizeMode = AutoSizeColumnMode;
            this.factorDataGridViewTextBoxColumn.ValueType = typeof(double);
            this.factorDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // currentAmountDataGridViewTextBoxColumn
            // 
            this.currentAmountDataGridViewTextBoxColumn.DataPropertyName = "CurrentAmount";
            this.currentAmountDataGridViewTextBoxColumn.HeaderText = "Current Amount";
            this.currentAmountDataGridViewTextBoxColumn.Name = "CurrentAmount";
            this.currentAmountDataGridViewTextBoxColumn.DefaultCellStyle.Format = "N2";
            this.currentAmountDataGridViewTextBoxColumn.DefaultCellStyle.Alignment =
                DataGridViewContentAlignment.MiddleRight;
            this.currentAmountDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.currentAmountDataGridViewTextBoxColumn.AutoSizeMode = AutoSizeColumnMode;
            this.currentAmountDataGridViewTextBoxColumn.ValueType = typeof(double);
            // 
            // moodysSpecDataGridViewComboBoxColumn
            // 
            this.moodysSpecDataGridViewComboBoxColumn.DataPropertyName = "Moodys";
            this.moodysSpecDataGridViewComboBoxColumn.HeaderText = "Moody's";
            this.moodysSpecDataGridViewComboBoxColumn.Name = "Moodys";
            this.moodysSpecDataGridViewComboBoxColumn.DisplayMember = "Value";
            this.moodysSpecDataGridViewComboBoxColumn.ValueMember = "Key";
            this.moodysSpecDataGridViewComboBoxColumn.DataSource = EnumHelper.ToList(typeof(Rating));
            this.moodysSpecDataGridViewComboBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.moodysSpecDataGridViewComboBoxColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;
            this.moodysSpecDataGridViewComboBoxColumn.AutoSizeMode = AutoSizeColumnMode;
            this.moodysSpecDataGridViewComboBoxColumn.ValueType = typeof(Rating);
            this.moodysSpecDataGridViewComboBoxColumn.ReadOnly = true;
            // 
            // spSpecDataGridViewComboBoxColumn
            // 
            this.spSpecDataGridViewComboBoxColumn.DataPropertyName = "SP";
            this.spSpecDataGridViewComboBoxColumn.HeaderText = "S&P";
            this.spSpecDataGridViewComboBoxColumn.Name = "SP";
            this.spSpecDataGridViewComboBoxColumn.DisplayMember = "Value";
            this.spSpecDataGridViewComboBoxColumn.ValueMember = "Key";
            this.spSpecDataGridViewComboBoxColumn.DataSource = EnumHelper.ToList(typeof(Rating));
            this.spSpecDataGridViewComboBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.spSpecDataGridViewComboBoxColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;
            this.spSpecDataGridViewComboBoxColumn.AutoSizeMode = AutoSizeColumnMode;
            this.spSpecDataGridViewComboBoxColumn.ValueType = typeof(Rating);
            this.spSpecDataGridViewComboBoxColumn.ReadOnly = true;
            // 
            // fitchDataGridViewComboBoxColumn
            // 
            this.fitchDataGridViewComboBoxColumn.DataPropertyName = "Fitch";
            this.fitchDataGridViewComboBoxColumn.HeaderText = "Fitch";
            this.fitchDataGridViewComboBoxColumn.Name = "Fitch";
            this.fitchDataGridViewComboBoxColumn.DisplayMember = "Value";
            this.fitchDataGridViewComboBoxColumn.ValueMember = "Key";
            this.fitchDataGridViewComboBoxColumn.DataSource = EnumHelper.ToList(typeof(Rating));
            this.fitchDataGridViewComboBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.fitchDataGridViewComboBoxColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;
            this.fitchDataGridViewComboBoxColumn.AutoSizeMode = AutoSizeColumnMode;
            this.fitchDataGridViewComboBoxColumn.ValueType = typeof(Rating);
            this.fitchDataGridViewComboBoxColumn.ReadOnly = true;
            // 
            // couponDataGridViewTextBoxColumn
            // 
            this.couponDataGridViewTextBoxColumn.DataPropertyName = "Coupon";
            this.couponDataGridViewTextBoxColumn.HeaderText = "Coupon";
            this.couponDataGridViewTextBoxColumn.Name = "Coupon";
            this.couponDataGridViewTextBoxColumn.DefaultCellStyle.Format = "N3";
            this.couponDataGridViewTextBoxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.couponDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.couponDataGridViewTextBoxColumn.AutoSizeMode = AutoSizeColumnMode;
            this.couponDataGridViewTextBoxColumn.ValueType = typeof(double);
            this.couponDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // couponFrequencyDataGridViewTextBoxColumn
            // 
            this.couponFrequencyDataGridViewTextBoxColumn.DataPropertyName = "CouponFrequency";
            this.couponFrequencyDataGridViewTextBoxColumn.HeaderText = "Coupon Frequency";
            this.couponFrequencyDataGridViewTextBoxColumn.Name = "CouponFrequency";
            this.couponFrequencyDataGridViewTextBoxColumn.DefaultCellStyle.Format = "N0";
            this.couponFrequencyDataGridViewTextBoxColumn.DefaultCellStyle.Alignment =
                DataGridViewContentAlignment.MiddleRight;
            this.couponFrequencyDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.couponFrequencyDataGridViewTextBoxColumn.AutoSizeMode = AutoSizeColumnMode;
            this.couponFrequencyDataGridViewTextBoxColumn.ValueType = typeof(int);
            this.couponFrequencyDataGridViewTextBoxColumn.ReadOnly = true;

            // 
            // couponTypeDataGridViewComboBoxColumn
            // 
            this.couponTypeDataGridViewComboBoxColumn.DataPropertyName = "CouponType";
            this.couponTypeDataGridViewComboBoxColumn.HeaderText = "Coupon Type";
            this.couponTypeDataGridViewComboBoxColumn.Name = "couponType";
            this.couponTypeDataGridViewComboBoxColumn.DisplayMember = "Value";
            this.couponTypeDataGridViewComboBoxColumn.ValueMember = "Key";
            this.couponTypeDataGridViewComboBoxColumn.DataSource = EnumHelper.ToList(typeof(CouponType));
            this.couponTypeDataGridViewComboBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.couponTypeDataGridViewComboBoxColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;
            this.couponTypeDataGridViewComboBoxColumn.AutoSizeMode = AutoSizeColumnMode;
            this.couponTypeDataGridViewComboBoxColumn.ValueType = typeof(CouponType);
            this.couponTypeDataGridViewComboBoxColumn.ReadOnly = true;

            // 
            // indexDataGridViewTextBoxColumn
            // 
            this.indexDataGridViewTextBoxColumn.DataPropertyName = "ResetIndex";
            this.indexDataGridViewTextBoxColumn.HeaderText = "Index";
            this.indexDataGridViewTextBoxColumn.Name = "Index";
            this.indexDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.indexDataGridViewTextBoxColumn.AutoSizeMode = AutoSizeColumnMode;
            this.indexDataGridViewTextBoxColumn.ReadOnly = true;

            // 
            // spreadDataGridViewTextBoxColumn
            // 
            this.spreadDataGridViewTextBoxColumn.DataPropertyName = "Spread";
            this.spreadDataGridViewTextBoxColumn.HeaderText = "Spread";
            this.spreadDataGridViewTextBoxColumn.Name = "spread";
            this.spreadDataGridViewTextBoxColumn.DefaultCellStyle.Format = "N2";
            this.spreadDataGridViewTextBoxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.spreadDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.spreadDataGridViewTextBoxColumn.AutoSizeMode = AutoSizeColumnMode;
            this.spreadDataGridViewTextBoxColumn.ValueType = typeof(double);
            this.spreadDataGridViewTextBoxColumn.ReadOnly = true;

            // 
            // floatingFrequencyDataGridViewTextBoxColumn
            // 
            this.floatingFrequencyDataGridViewTextBoxColumn.DataPropertyName = "FloatingFrequency";
            this.floatingFrequencyDataGridViewTextBoxColumn.HeaderText = "Floating Frequency";
            this.floatingFrequencyDataGridViewTextBoxColumn.Name = "floatingFrequency";
            this.floatingFrequencyDataGridViewTextBoxColumn.DefaultCellStyle.Format = "N0";
            this.floatingFrequencyDataGridViewTextBoxColumn.DefaultCellStyle.Alignment =
                DataGridViewContentAlignment.MiddleRight;
            this.floatingFrequencyDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.floatingFrequencyDataGridViewTextBoxColumn.AutoSizeMode = AutoSizeColumnMode;
            this.floatingFrequencyDataGridViewTextBoxColumn.ValueType = typeof(int);
            this.floatingFrequencyDataGridViewTextBoxColumn.ReadOnly = true;

            // 
            // resetDateDataGridViewTextBoxColumn
            // 
            this.resetDateDataGridViewTextBoxColumn.DataPropertyName = "ResetDate";
            this.resetDateDataGridViewTextBoxColumn.HeaderText = "Reset Date";
            this.resetDateDataGridViewTextBoxColumn.Name = "resetDate";
            this.resetDateDataGridViewTextBoxColumn.DefaultCellStyle.Format = Context.Workspace.Parameters.DateFormatString;
            this.resetDateDataGridViewTextBoxColumn.DefaultCellStyle.Alignment =
                DataGridViewContentAlignment.MiddleRight;
            this.resetDateDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.resetDateDataGridViewTextBoxColumn.AutoSizeMode = AutoSizeColumnMode;
            this.resetDateDataGridViewTextBoxColumn.ValueType = typeof(DateTime);
            this.resetDateDataGridViewTextBoxColumn.ReadOnly = true;

            // 
            // issueDatePriceDataGridViewTextBoxColumn
            // 
            this.issueDatePriceDataGridViewTextBoxColumn.DataPropertyName = "IssueDate";
            this.issueDatePriceDataGridViewTextBoxColumn.HeaderText = "Issue Date";
            this.issueDatePriceDataGridViewTextBoxColumn.Name = "IssueDate";
            this.issueDatePriceDataGridViewTextBoxColumn.DefaultCellStyle.Format = Context.Workspace.Parameters.DateFormatString;
            this.issueDatePriceDataGridViewTextBoxColumn.DefaultCellStyle.Alignment =
                DataGridViewContentAlignment.MiddleRight;
            this.issueDatePriceDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.issueDatePriceDataGridViewTextBoxColumn.AutoSizeMode = AutoSizeColumnMode;
            this.issueDatePriceDataGridViewTextBoxColumn.ValueType = typeof(DateTime);
            this.issueDatePriceDataGridViewTextBoxColumn.ReadOnly = true;

            // 
            // maturityDatePriceDataGridViewTextBoxColumn
            // 
            this.maturityDatePriceDataGridViewTextBoxColumn.DataPropertyName = "MaturityDate";
            this.maturityDatePriceDataGridViewTextBoxColumn.HeaderText = "Maturity Date";
            this.maturityDatePriceDataGridViewTextBoxColumn.Name = "MaturityDate";
            this.maturityDatePriceDataGridViewTextBoxColumn.DefaultCellStyle.Format = Context.Workspace.Parameters.DateFormatString;
            this.maturityDatePriceDataGridViewTextBoxColumn.DefaultCellStyle.Alignment =
                DataGridViewContentAlignment.MiddleRight;
            this.maturityDatePriceDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.maturityDatePriceDataGridViewTextBoxColumn.AutoSizeMode = AutoSizeColumnMode;
            this.maturityDatePriceDataGridViewTextBoxColumn.ValueType = typeof(DateTime);
            this.maturityDatePriceDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // collateralTypeDataGridViewTextBoxColumn
            // 
            this.collateralTypeDataGridViewTextBoxColumn.DataPropertyName = "CollateralType";
            this.collateralTypeDataGridViewTextBoxColumn.HeaderText = "Collateral Type";
            this.collateralTypeDataGridViewTextBoxColumn.Name = "CollateralType";
            this.collateralTypeDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.collateralTypeDataGridViewTextBoxColumn.AutoSizeMode = AutoSizeColumnMode;
            this.collateralTypeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // industrySectorDataGridViewTextBoxColumn
            // 
            this.industrySectorDataGridViewTextBoxColumn.DataPropertyName = "IndustrySector";
            this.industrySectorDataGridViewTextBoxColumn.HeaderText = "Industry Sector";
            this.industrySectorDataGridViewTextBoxColumn.Name = "IndustrySector";
            this.industrySectorDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.industrySectorDataGridViewTextBoxColumn.AutoSizeMode = AutoSizeColumnMode;
            this.industrySectorDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // givenTypeDataGridViewComboBoxColumn
            // 
            this.givenTypeDataGridViewComboBoxColumn.DataPropertyName = "GivenType";
            this.givenTypeDataGridViewComboBoxColumn.HeaderText = "Given Type";
            this.givenTypeDataGridViewComboBoxColumn.Name = "GivenType";
            this.givenTypeDataGridViewComboBoxColumn.DisplayMember = "Value";
            this.givenTypeDataGridViewComboBoxColumn.ValueMember = "Key";
            this.givenTypeDataGridViewComboBoxColumn.DataSource = EnumHelper.ToList(typeof(GivenType));
            this.givenTypeDataGridViewComboBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.givenTypeDataGridViewComboBoxColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;
            this.givenTypeDataGridViewComboBoxColumn.AutoSizeMode = AutoSizeColumnMode;
            this.givenTypeDataGridViewComboBoxColumn.ValueType = typeof(GivenType);
            // 
            // givenAmtDataGridViewTextBoxColumn
            // 
            this.givenAmtDataGridViewTextBoxColumn.DataPropertyName = "GivenAmt";
            this.givenAmtDataGridViewTextBoxColumn.HeaderText = "Given Amt";
            this.givenAmtDataGridViewTextBoxColumn.Name = "GivenAmt";
            this.givenAmtDataGridViewTextBoxColumn.DefaultCellStyle.Format = "N3";
            this.givenAmtDataGridViewTextBoxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.givenAmtDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.givenAmtDataGridViewTextBoxColumn.AutoSizeMode = AutoSizeColumnMode;
            this.givenAmtDataGridViewTextBoxColumn.ValueType = typeof(double);
            // 
            // spreadToDiscMarginIndexDataGridViewComboBoxColumn
            // 
            this.spreadToDiscMarginIndexDataGridViewComboBoxColumn.DataPropertyName = "SpreadToDiscMarginIndex";
            this.spreadToDiscMarginIndexDataGridViewComboBoxColumn.HeaderText = "Spread to/Disc Margin Index";
            this.spreadToDiscMarginIndexDataGridViewComboBoxColumn.Name = "SpreadToDiscMarginIndex";
            this.spreadToDiscMarginIndexDataGridViewComboBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.spreadToDiscMarginIndexDataGridViewComboBoxColumn.AutoSizeMode = AutoSizeColumnMode;
            this.spreadToDiscMarginIndexDataGridViewComboBoxColumn.ValueType = typeof(string);
            // 
            // noOfstepsDataGridViewTextBoxColumn
            // 
            this.noOfstepsDataGridViewTextBoxColumn.DataPropertyName = "NoOfSteps";
            this.noOfstepsDataGridViewTextBoxColumn.HeaderText = "# of Steps";
            this.noOfstepsDataGridViewTextBoxColumn.Name = "NoOfSteps";
            this.noOfstepsDataGridViewTextBoxColumn.DefaultCellStyle.Format = "N0";
            this.noOfstepsDataGridViewTextBoxColumn.DefaultCellStyle.Alignment =
                DataGridViewContentAlignment.MiddleRight;
            this.noOfstepsDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.noOfstepsDataGridViewTextBoxColumn.AutoSizeMode = AutoSizeColumnMode;
            this.noOfstepsDataGridViewTextBoxColumn.ValueType = typeof(int);
            // 
            // stepsAmtDataGridViewTextBoxColumn
            // 
            this.stepsAmtDataGridViewTextBoxColumn.DataPropertyName = "StepAmt";
            this.stepsAmtDataGridViewTextBoxColumn.HeaderText = "Step Amt";
            this.stepsAmtDataGridViewTextBoxColumn.Name = "StepAmt";
            this.stepsAmtDataGridViewTextBoxColumn.DefaultCellStyle.Format = "N2";
            this.stepsAmtDataGridViewTextBoxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.stepsAmtDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.stepsAmtDataGridViewTextBoxColumn.AutoSizeMode = AutoSizeColumnMode;
            this.stepsAmtDataGridViewTextBoxColumn.ValueType = typeof(double);
            // 
            // userNotesDataGridViewTextBoxColumn
            // 
            this.userNotesDataGridViewTextBoxColumn.DataPropertyName = "UserNotes";
            this.userNotesDataGridViewTextBoxColumn.HeaderText = "User Notes";
            this.userNotesDataGridViewTextBoxColumn.Name = "UserNotes";
            this.userNotesDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.userNotesDataGridViewTextBoxColumn.AutoSizeMode = AutoSizeColumnMode;

            this.headerCellStyle = new DataGridViewCellStyle();
            this.headerCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this.headerCellStyle.BackColor = Color.LightGray;
            this.headerCellStyle.SelectionBackColor = this.headerCellStyle.BackColor;
            this.headerCellStyle.SelectionForeColor = Color.Black;


            //Validated?	CUSIP	ISIN	Security Description	Original Amt	Factor	Current Amount	Moody's	S&P	Fitch	
            //Coupon	Issue Date	Maturity Date	Collateral Type	Industry Sector	Given Type	Given Amt	
            //Spread to/Disc Margin Index	# of Steps	Step Amt	User Notes

            return new List<DataGridViewColumn>
                       {
                           this.treeGridColumn,
                           this.excludeDataGridViewCheckBoxColumn,
                           this.validatedDataGridViewImageColumn,
                           this.cusipDataGridViewTextBoxColumn,
                           this.isinDataGridViewTextBoxColumn,
                           // this.securityDescriptionDataGridViewTextBoxColumn,
                           this.originalAmtDataGridViewTextBoxColumn,
                           this.factorDataGridViewTextBoxColumn,
                           this.currentAmountDataGridViewTextBoxColumn,
                           this.moodysSpecDataGridViewComboBoxColumn,
                           this.spSpecDataGridViewComboBoxColumn,
                           this.fitchDataGridViewComboBoxColumn,
                           this.couponDataGridViewTextBoxColumn,
                           this.couponFrequencyDataGridViewTextBoxColumn,
                           this.couponTypeDataGridViewComboBoxColumn,
                           this.indexDataGridViewTextBoxColumn,
                           this.spreadDataGridViewTextBoxColumn,
                           this.floatingFrequencyDataGridViewTextBoxColumn,
                           this.resetDateDataGridViewTextBoxColumn,
                           this.issueDatePriceDataGridViewTextBoxColumn,
                           this.maturityDatePriceDataGridViewTextBoxColumn,
                           this.collateralTypeDataGridViewTextBoxColumn,
                           this.industrySectorDataGridViewTextBoxColumn,
                           this.givenTypeDataGridViewComboBoxColumn,
                           this.givenAmtDataGridViewTextBoxColumn,
                           this.spreadToDiscMarginIndexDataGridViewComboBoxColumn,
                           this.noOfstepsDataGridViewTextBoxColumn,
                           this.stepsAmtDataGridViewTextBoxColumn,
                           this.userNotesDataGridViewTextBoxColumn
                       };
        }

        private void CreateFirstHeader()
        {
            TreeGridNode gridNode = new TreeGridNode(this.TreeGrid);
            gridNode.CreateCells(this.TreeGrid, new object[] { });
            this.TreeGrid.Nodes.Add(gridNode);

            int rowIdx = this.TreeGrid.Nodes.Count - 1;
            this.TreeGrid.Rows[rowIdx].Frozen = true;

            for (int i = 0; i < this.TreeGrid.Columns.Count; i++)
            {
                this.TreeGrid[i, rowIdx] = this.CreateTopBottomBorderCell();
            }

            this.TreeGrid.Rows[rowIdx].Height = 40;
            this.TreeGrid.Rows[rowIdx].ReadOnly = true;

            this.TreeGrid[0, rowIdx].Value = "Security Details";
            ((DataGridViewCellEx)this.TreeGrid[this.FixedColumnCount - 1, rowIdx]).
                DataGridViewAdvancedCellBorderStyle.Right = DataGridViewAdvancedCellBorderStyle.Single;
        }

        private void CreateSecondHeader()
        {
            TreeGridNode gridNode = new TreeGridNode(this.TreeGrid);
            gridNode.CreateCells(this.TreeGrid, new object[] { });
            this.TreeGrid.Nodes.Add(gridNode);

            int rowIdx = this.TreeGrid.Nodes.Count - 1;
            this.TreeGrid.Rows[rowIdx].Height = 40;
            this.TreeGrid.Rows[rowIdx].Frozen = true;

            for (int i = 0; i < this.TreeGrid.Columns.Count; i++)
            {
                this.TreeGrid[i, rowIdx] = new DataGridViewTextBoxCell
                                               {
                                                   Value = this.TreeGrid.Columns[i].HeaderText,
                                                   Style = this.headerCellStyle
                                               };
            }

            this.TreeGrid.Rows[rowIdx].ReadOnly = true;

            this.TreeGrid.Rows[rowIdx].DefaultCellStyle.Font = this.TreeGrid.Font;
        }

        private void LoadData()
        {
            Category setupSecurities = Context.Workspace.SetupSecurities;
            foreach (Category category in setupSecurities.Categories)
            {
                this.AddCategory(category, this.TreeGrid);
            }
            foreach (PortfolioSecurity security in setupSecurities.Securities)
            {
                this.AddSecurity(security, this.TreeGrid);
            }

            this.TreeGrid.Nodes[0].Tag = this.TreeGrid.Nodes[1].Tag = setupSecurities;

            this.TreeGrid.AutoResizeColumns();
        }

        protected void AddCategory(Category category, INode node)
        {
            object[] nodeData = new object[this.TreeGrid.ColumnCount];

            nodeData[0] = category.Name;
            TreeGridNode groupNode = new TreeGridNode(this.TreeGrid);
            groupNode.Tag = category;
            groupNode.CreateCells(this.TreeGrid, nodeData);

            foreach (Category child in category.Categories)
            {
                this.AddCategory(child, groupNode);
            }

            foreach (PortfolioSecurity security in category.Securities)
            {
                this.AddSecurity(security, groupNode);
            }

            node.Nodes.Add(groupNode);
        }

        protected void AddSecurity(PortfolioSecurity security, INode node)
        {
            object[] values = this.PortfolioSecurityMapper.GetArray(security);
            node.Nodes.Add(values).Tag = security;

            security.PropertyChanged += this.SecurityPropertyChanged;
        }

        private void SecurityPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            PortfolioSecurity security = sender as PortfolioSecurity;
            if (security == null) return;

            TreeGridNode node = TreeGridViewUtil.AllNodes(this.TreeGrid).FirstOrDefault(n => n.Tag == security);
            if (node == null) return;

            object[] values = this.PortfolioSecurityMapper.GetArray(security);
            node.SetValues(values);
        }

        private void ClearTreeGrid()
        {
            foreach (TreeGridNode node in TreeGridViewUtil.AllNodes(this.TreeGrid))
            {
                Security security = node.Tag as Security;
                if (security != null)
                {
                    security.PropertyChanged -= this.SecurityPropertyChanged;
                }
            }

            this.TreeGrid.Nodes.Clear();
        }

        private void OnRowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            TreeGridNode gridNode = this.TreeGrid.Rows[e.RowIndex] as TreeGridNode;
            if (gridNode == null)
            {
                return;
            }
            bool isCategory = gridNode.Tag is Category;
            if (isCategory)
            {
                TreeGridCell cell = (TreeGridCell)this.TreeGrid[0, e.RowIndex];
                cell.DataGridViewAdvancedCellBorderStyle = new DataGridViewAdvancedBorderStyle
                                                               {
                                                                   Bottom =
                                                                       DataGridViewAdvancedCellBorderStyle.
                                                                       Single
                                                               };

                for (int i = 1; i < this.TreeGrid.Columns.Count; i++)
                {
                    this.TreeGrid[i, e.RowIndex] = new DataGridViewCellEx
                                                       {
                                                           Value = "",
                                                           Style = this.headerCellStyle,
                                                           DataGridViewAdvancedCellBorderStyle =
                                                               new DataGridViewAdvancedBorderStyle
                                                                   {
                                                                       Bottom =
                                                                           DataGridViewAdvancedCellBorderStyle.
                                                                           Single
                                                                   }
                                                       };

                    if (i >= this.originalAmtDataGridViewTextBoxColumn.DisplayIndex)
                    {
                        ((DataGridViewCellEx)this.TreeGrid[i, e.RowIndex]).DataGridViewAdvancedCellBorderStyle.
                            Right =
                            DataGridViewAdvancedCellBorderStyle.Single;
                    }

                    if (i == this.excludeDataGridViewCheckBoxColumn.DisplayIndex)
                    {
                        this.TreeGrid[i, e.RowIndex].Style = new DataGridViewCellStyle
                                                                 {
                                                                     Alignment =
                                                                         DataGridViewContentAlignment.MiddleLeft,
                                                                     BackColor = Color.LightGray,
                                                                     SelectionBackColor =
                                                                         this.headerCellStyle.BackColor,
                                                                     SelectionForeColor = Color.Black,
                                                                     Font = new Font(this.TreeGrid.Font, FontStyle.Bold)
                                                                 };
                    }
                    
                    if (i > 27)
                    {
                        this.TreeGrid[i, e.RowIndex].Style = new DataGridViewCellStyle
                        {
                            Alignment = DataGridViewContentAlignment.MiddleLeft,
                            BackColor = Color.White,
                            SelectionBackColor = SystemColors.Highlight,
                            SelectionForeColor = Color.Black,
                            Font = new Font(this.TreeGrid.Font, FontStyle.Bold)
                        };
                        this.TreeGrid[i, e.RowIndex].ReadOnly = false;
                    }
                    else this.TreeGrid[i, e.RowIndex].ReadOnly = true;

                    //if (i == 10)
                    //    this.treeGrid[i, e.RowIndex].Value = ">=BBB+";

                    //if (i == 13)
                    //    this.treeGrid[i, e.RowIndex].Value = "<=1/1/2009";
                }

                this.TreeGrid.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.LightGray;
                this.TreeGrid.Rows[e.RowIndex].DefaultCellStyle.SelectionBackColor = Color.LightGray;
                this.TreeGrid.Rows[e.RowIndex].DefaultCellStyle.SelectionForeColor = Color.Black;
            }
        }

        private static void OnDataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            Debug.WriteLine("DataError[" + e.RowIndex + ", " + e.ColumnIndex + "] : " + e.Exception);

            e.Cancel = true;
            e.ThrowException = false;
        }

        public void DeleteSelectedRows()
        {
            List<DataGridViewRow> selectedRows = this.TreeGrid.SelectedRows.Cast<DataGridViewRow>().ToList();

            foreach (DataGridViewRow row in selectedRows)
            {
                if (row.Index < 2)
                {
                    continue;
                }

                Category category = row.Tag as Category;
                if (category != null)
                {
                    category.Parent.Categories.Remove(category);
                }
                else
                {
                    PortfolioSecurity portfolioSecurity = row.Tag as PortfolioSecurity;
                    if (portfolioSecurity != null)
                    {
                        portfolioSecurity.SetupCategory.Securities.Remove(portfolioSecurity);
                        portfolioSecurity.SetupCategory = null;
                    }
                }

                TreeGridNode treeGridNode = this.TreeGrid.GetNodeForRow(row);
                treeGridNode.Parent.Nodes.Remove(treeGridNode);
            }
        }
    }
}