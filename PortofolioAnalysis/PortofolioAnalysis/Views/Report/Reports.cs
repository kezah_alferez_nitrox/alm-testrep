﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using ALMSCommon.Forms;
using PortfolioAnalysis.Calculations;
using PortfolioAnalysis.Infrastructure;
using PortofolioAnalysis.Common.Entities;

namespace PortfolioAnalysis.Views.Report
{
    public partial class Reports : UserControl, IReports, IHandle<CalculateEvent>
    {
        private ResultCalculator resultCalculator;

        private readonly IDictionary<PortfolioSecurity, SecurityYield> yieldPanels = new Dictionary<PortfolioSecurity, SecurityYield>();

        private readonly ReportsTreeGridController treeGridController;

        public Reports()
        {
            this.InitializeComponent();

            this.treeGridController = new ReportsTreeGridController(this, this.treeGrid);
            treeGrid.ReadOnly = true;

            Context.EventAggregator.Subscribe(this);
        }

        public void HandleMessage(CalculateEvent message)
        {
            this.resultCalculator = new ResultCalculator();
            LoadForm.Instance.DoWork(new WorkParameters(this.resultCalculator.Calculate, this.CalculateCompleted, true));
        }

        private void CalculateCompleted(Exception exception)
        {
            if (this.resultCalculator.Canceled)
            {
                return;
            }

            if (this.resultCalculator.Errors.Count > 0)
            {
                CalculateErrorsDialog dialog = new CalculateErrorsDialog();
                dialog.SetErrors(this.resultCalculator.Errors);
                dialog.ShowDialog();
            }else
            {
                treeGridController.LoadFromWorkspace();

                yieldPanelContainer.Controls.Clear();
                yieldPanels.Clear();
            }
        }

        public void ShowCashflows(PortfolioSecurity security)
        {
            if (this.resultCalculator == null) return;
            if (Context.Workspace.Scenarios.Count == 0) return;

            CashflowData[] cashflowsData;
            this.resultCalculator.Cashflows.TryGetValue(security, out cashflowsData);

            if (cashflowsData == null) return;

            CashflowReport cashflowReport = new CashflowReport(cashflowsData);
            cashflowReport.ShowDialog();
        }

        public void ShowYield(PortfolioSecurity security)
        {
            if (this.resultCalculator == null || this.resultCalculator.Cashflows.Count == 0) return;

            SecurityYield yieldPanel;
            if (this.yieldPanels.ContainsKey(security))
            {
                yieldPanel = this.yieldPanels[security];
                if (yieldPanelContainer.Controls.Contains(yieldPanel))
                {
                    yieldPanelContainer.Controls.Remove(yieldPanel);
                    yieldPanels.Remove(security);
                }
                return;
            }

            this.Cursor = Cursors.WaitCursor;
            yieldPanel = new SecurityYield(security, this.resultCalculator.Cashflows[security]);
            yieldPanels.Add(security, yieldPanel);
            yieldPanelContainer.Controls.Add(yieldPanel);
            this.Cursor = Cursors.Default;
        }

        public void Shown()
        {
            
        }
    }
}