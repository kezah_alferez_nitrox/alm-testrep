﻿using ALMSCommon.Controls.TreeGridView;

namespace PortfolioAnalysis.Views.Report
{
    partial class Reports : IReports
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.treeGrid = new TreeGridView();
            this.yieldPanelContainer = new System.Windows.Forms.FlowLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.treeGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 49);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.treeGrid);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.yieldPanelContainer);
            this.splitContainer1.Size = new System.Drawing.Size(813, 379);
            this.splitContainer1.SplitterDistance = 185;
            this.splitContainer1.TabIndex = 5;
            // 
            // treeGrid
            // 
            this.treeGrid.AllowUserToAddRows = false;
            this.treeGrid.AllowUserToDeleteRows = false;
            this.treeGrid.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.treeGrid.ColumnHeadersVisible = false;
            this.treeGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeGrid.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.treeGrid.ImageList = null;
            this.treeGrid.Location = new System.Drawing.Point(0, 0);
            this.treeGrid.Name = "treeGrid";
            this.treeGrid.RowHeadersVisible = false;
            this.treeGrid.Size = new System.Drawing.Size(813, 185);
            this.treeGrid.TabIndex = 4;
            // 
            // yieldPanelContainer
            // 
            this.yieldPanelContainer.AutoScroll = true;
            this.yieldPanelContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.yieldPanelContainer.Location = new System.Drawing.Point(0, 0);
            this.yieldPanelContainer.Name = "yieldPanelContainer";
            this.yieldPanelContainer.Size = new System.Drawing.Size(813, 190);
            this.yieldPanelContainer.TabIndex = 5;
            // 
            // panel1
            // 
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(813, 49);
            this.panel1.TabIndex = 1;
            // 
            // Reports
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.panel1);
            this.Name = "Reports";
            this.Size = new System.Drawing.Size(813, 428);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.treeGrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private TreeGridView treeGrid;
        private System.Windows.Forms.FlowLayoutPanel yieldPanelContainer;
        private System.Windows.Forms.Panel panel1;



    }
}
