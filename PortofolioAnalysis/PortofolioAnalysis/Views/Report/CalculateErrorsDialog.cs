﻿using System.Collections.Generic;
using System.Windows.Forms;

namespace PortfolioAnalysis.Views.Report
{
    public partial class CalculateErrorsDialog : Form
    {
        public CalculateErrorsDialog()
        {
            this.InitializeComponent();
        }

        public void SetErrors(IList<string> errors)
        {
            textBox1.Clear();

            foreach (string error in errors)
            {
                textBox1.AppendText(error); textBox1.AppendText(System.Environment.NewLine);
            }
        }
    }
}