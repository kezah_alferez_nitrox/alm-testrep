﻿using PortofolioAnalysis.Common.Entities;

namespace PortfolioAnalysis.Views.Report
{
    public interface IReports 
    {
        void ShowCashflows(PortfolioSecurity security);
        void ShowYield(PortfolioSecurity security);
    }
}