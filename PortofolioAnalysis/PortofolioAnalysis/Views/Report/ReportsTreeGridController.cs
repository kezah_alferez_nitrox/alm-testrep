using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Windows.Forms;
using ALMSCommon;
using ALMSCommon.Controls.TreeGridView;
using PortofolioAnalysis.Common.Entities;

namespace PortfolioAnalysis.Views.Report
{
    public class ReportsTreeGridController : SecuritiesTreeGridController
    {
        private readonly IReports reports;
        private DataGridViewButtonColumn yieldTableDataGridViewCheckBoxColumn;
        private DataGridViewLinkColumn viewCashFlowDataGridViewLinkColumn;

        public ReportsTreeGridController(IReports reports, TreeGridView treeGrid)
            : base(treeGrid)
        {
            this.reports = reports;

            this.PortfolioSecurityMapper = new ScenarioSecurityArrayMapper(
                new Expression<Func<PortfolioSecurity, object>>[]
                {
                    e => e.Description, null, null,
                    e => e.Exclude, e => e.Status, e => e.IdCussip, e => e.ISIN,
                    e => e.OriginalAmount, e => e.Factor, e => e.CurrentAmount,
                    e => e.RatingMoodys,
                    e => e.RatingSandP, e => e.RatingFitch, e => e.Coupon, e => e.CouponFrequency,
                    e => e.CouponType, e => e.ResetIndex, e => e.Spread,
                    e => e.FloatingFrequency, e => e.ResetDate,
                    e => e.IssueDate,
                    e => e.MaturityDate, e => e.CollateralType, e => e.IndustrySector,
                    e => e.GivenType, e => e.GivenAmount, e => e.SpreadToDiscMarginIndex,
                    e => e.NumberOfSteps, e => e.StepAmount, e => e.UserNotes,
                },
                new Expression<Func<SecurityScenarioValues, object>>[]
                {
                    e => e.Prepay, e => e.Default, e => e.Severity
                });

            treeGrid.CellDoubleClick += this.TreeViewCellDoubleClick;
            treeGrid.CellClick += this.TreeViewCellClick;
            treeGrid.RowsAdded += this.TreeGridRowsAdded;
        }

        private void TreeGridRowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            if (e.RowIndex > 1 && (this.TreeGrid.Rows[e.RowIndex].Tag as Category) == null)
            {
                this.TreeGrid[this.yieldTableDataGridViewCheckBoxColumn.Index, e.RowIndex].Value = "Show Yield";
                this.TreeGrid[this.viewCashFlowDataGridViewLinkColumn.Index, e.RowIndex].Value = "View Cashflow";
            }
        }

        protected override List<DataGridViewColumn> GetColumns()
        {
            List<DataGridViewColumn> dataGridViewColumns = base.GetColumns();

            this.yieldTableDataGridViewCheckBoxColumn = new DataGridViewButtonColumn();
            this.yieldTableDataGridViewCheckBoxColumn.HeaderText = "";
            this.yieldTableDataGridViewCheckBoxColumn.Name = "YieldTable";
            this.yieldTableDataGridViewCheckBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.yieldTableDataGridViewCheckBoxColumn.Frozen = true;
            dataGridViewColumns.Insert(1, this.yieldTableDataGridViewCheckBoxColumn);

            this.viewCashFlowDataGridViewLinkColumn = new DataGridViewLinkColumn();
            this.viewCashFlowDataGridViewLinkColumn.HeaderText = "";
            this.viewCashFlowDataGridViewLinkColumn.Name = "ViewCashFlow";
            this.viewCashFlowDataGridViewLinkColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.viewCashFlowDataGridViewLinkColumn.Frozen = true;
            dataGridViewColumns.Insert(2, this.viewCashFlowDataGridViewLinkColumn);

            return dataGridViewColumns;
        }

        private void TreeViewCellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0 || e.RowIndex > this.TreeGrid.Rows.Count - 1)
            {
                return;
            }
            TreeGridNode row = this.TreeGrid.Rows[e.RowIndex] as TreeGridNode;
            if (e.RowIndex >= 2 && row != null && row.Nodes.Count == 0)
            {
                if (e.ColumnIndex == this.yieldTableDataGridViewCheckBoxColumn.DisplayIndex)
                {
                    bool state = this.TreeGrid[e.ColumnIndex, e.RowIndex].Tag != null &&
                                 (bool) this.TreeGrid[e.ColumnIndex, e.RowIndex].Tag;

                    this.TreeGrid[e.ColumnIndex, e.RowIndex].Value = state ? "Show yield" : "Hide yield";

                    this.TreeGrid[e.ColumnIndex, e.RowIndex].Tag = !state;

                    PortfolioSecurity portfolioSecurity = row.Tag as PortfolioSecurity;
                    if (portfolioSecurity != null)
                    {
                        this.reports.ShowYield(portfolioSecurity);
                    }
                }
                else if (e.ColumnIndex == this.viewCashFlowDataGridViewLinkColumn.DisplayIndex)
                {
                    PortfolioSecurity portfolioSecurity = row.Tag as PortfolioSecurity;
                    if (portfolioSecurity != null)
                    {
                        this.reports.ShowCashflows(portfolioSecurity);
                    }
                }
            }
        }

        private void TreeViewCellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            TreeGridNode row = this.TreeGrid.Rows[e.RowIndex] as TreeGridNode;
            if (e.RowIndex >= 2 && row != null && row.Nodes.Count == 0 &&
                e.ColumnIndex != this.yieldTableDataGridViewCheckBoxColumn.DisplayIndex)
            {
                PortfolioSecurity portfolioSecurity = row.Tag as PortfolioSecurity;
                if (portfolioSecurity != null)
                {
                    this.reports.ShowCashflows(portfolioSecurity);
                }
            }
        }
    }
}