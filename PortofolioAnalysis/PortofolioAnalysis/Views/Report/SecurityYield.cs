﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using PortfolioAnalysis.Calculations;
using PortfolioAnalysis.Controls.DataGridViewEx;
using PortofolioAnalysis.Common.DatabaseEntities;
using PortofolioAnalysis.Common.Entities;

namespace PortfolioAnalysis.Views.Report
{
    public partial class SecurityYield : UserControl
    {
        private const int FIXED_ROWS = 6;

        private readonly PortfolioSecurity security;
        private readonly CashflowData[] cashflowsData;

        private DataGridViewCellStyle headerCellStyle;

        private int totalRows;

        public SecurityYield(PortfolioSecurity security, CashflowData[] cashflowsData)
        {
            this.security = security;
            this.cashflowsData = cashflowsData;

            InitializeComponent();
            CreateFirstTwoColumns();
            CreateScenarioColumns();
            CreateHeader();

            InitiGrid();

            this.headerLabel.Text = security.Description;
        }

        private void CreateFirstTwoColumns()
        {
            DataGridViewTextBoxColumn givenDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            givenDataGridViewTextBoxColumn.HeaderText = @"Given: Price";
            givenDataGridViewTextBoxColumn.DefaultCellStyle.BackColor = Color.LightGray;
            givenDataGridViewTextBoxColumn.DefaultCellStyle.SelectionBackColor = Color.LightGray;
            givenDataGridViewTextBoxColumn.DefaultCellStyle.SelectionForeColor = Color.Black;
            givenDataGridViewTextBoxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.grid.Columns.Add(givenDataGridViewTextBoxColumn);

            DataGridViewTextBoxColumn emptyDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            emptyDataGridViewTextBoxColumn.HeaderText = "";
            //emptyDataGridViewTextBoxColumn.DefaultCellStyle.BackColor = Color.LightGray;
            emptyDataGridViewTextBoxColumn.DefaultCellStyle.SelectionBackColor = Color.LightGray;
            emptyDataGridViewTextBoxColumn.Width = 100;
            this.grid.Columns.Add(emptyDataGridViewTextBoxColumn);
        }

        private void CreateScenarioColumns()
        {
            BindingList<Scenario> scenarios = Context.Workspace.Scenarios;

            foreach (var scenario in scenarios)
            {
                DataGridViewTextBoxColumn scenarioDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
                scenarioDataGridViewTextBoxColumn.HeaderText = scenario.Name;
                scenarioDataGridViewTextBoxColumn.DefaultCellStyle.BackColor = Color.LightGray;
                scenarioDataGridViewTextBoxColumn.DefaultCellStyle.SelectionBackColor = Color.LightGray;
                scenarioDataGridViewTextBoxColumn.DefaultCellStyle.SelectionForeColor = Color.Black;
                scenarioDataGridViewTextBoxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                this.grid.Columns.Add(scenarioDataGridViewTextBoxColumn);
            }

            this.totalRows = ((security.NumberOfSteps * 2) + 1) * 3;
        }

        private void CreateHeader()
        {
            int rowIdx = this.grid.Rows.Add(1);
            this.grid.Rows[rowIdx].Frozen = true;

            this.grid.Rows[rowIdx].Height = 40;
            this.grid.Rows[rowIdx].ReadOnly = true;

            this.grid[0, 0].Value = "Given: " + security.GivenType;
            this.grid[1, 0].Value = "";

            BindingList<Scenario> scenarios = Context.Workspace.Scenarios;

            for (int i = 0; i < scenarios.Count; i++)
            {
                this.grid[i + 2, 0].Value = scenarios[i].Name;
                this.grid[i + 2, 0].Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                this.grid[i + 2, 0].Style.WrapMode = DataGridViewTriState.True;
            }
        }

        private void InitiGrid()
        {
            // create rows
            for (int i = 1; i <= totalRows + FIXED_ROWS; i++)
            {
                this.grid.Rows.Add();
            }
        }

        private DataGridViewCellEx CreateBorderCell()
        {
            return new DataGridViewCellEx
            {
                Value = "",
                Style = this.headerCellStyle,
                DataGridViewAdvancedCellBorderStyle =
                    new DataGridViewAdvancedBorderStyle
                    {
                        Left = DataGridViewAdvancedCellBorderStyle.Single,
                        Right = DataGridViewAdvancedCellBorderStyle.Single
                    }
            };
        }


        private void SecurityYieldLoad(object sender, EventArgs e)
        {
            BindingList<Scenario> scenarios = Context.Workspace.Scenarios;

            Evaluator evaluator = new Evaluator();
            evaluator.SetCalendar(security.Calendar);
            double[] index = evaluator.Evaluate(security.SpreadToDiscMarginIndex);

            TrancheCalculator trancheCalculator = new TrancheCalculator(this.security, this.cashflowsData, index);
            trancheCalculator.Calculate();

            int delta = 0;
            for (int i = 1; i <= this.grid.Rows.Count; i++)
            {
                if (i < totalRows)
                {
                    // set Price
                    object price = null;
                    if (trancheCalculator.Price.Length > 0)
                    {
                        price = FormatDouble(trancheCalculator.Price[delta]);
                        this.grid[0, i] = CreateBorderCell();
                        SetScenariousCellValue(i, price, scenarios);
                    }
                    this.grid[1, i++].Value = "Price";

                    // set Yield
                    object yield = null;
                    int yieldIdx = 0;
                    if (trancheCalculator.Yield.Length > 0)
                    {
                        yield = FormatDouble(trancheCalculator.Yield[delta]);
                        yieldIdx = i;
                        this.grid[0, i] = CreateBorderCell();
                        SetScenariousCellValue(i, yield, scenarios);
                    }
                    this.grid[1, i++].Value = "Yield";

                    // set Disc Margin
                    object dm = null;
                    if (trancheCalculator.Dm.Length > 0)
                    {
                        dm = FormatDouble(trancheCalculator.Dm[delta]);
                        this.grid[0, i] = CreateBorderCell();
                        ((DataGridViewCellEx)this.grid[0, i]).
                            DataGridViewAdvancedCellBorderStyle.Bottom =
                            DataGridViewAdvancedCellBorderStyle.Single;
                        SetScenariousCellValue(i, dm, scenarios);
                    }
                    this.grid[1, i].Value = "Disc Margin";


                    switch (security.GivenType)
                    {
                        case GivenType.Price:
                            this.grid[0, yieldIdx].Value = price;
                            break;
                        case GivenType.Yield:
                            this.grid[0, yieldIdx].Value = yield;
                            break;
                        case GivenType.Dm:
                            this.grid[0, yieldIdx].Value = dm;
                            break;
                    }

                    delta++;
                }
                else
                {
                    // set WAL
                    SetScenariousCellValue(i, FormatDouble(trancheCalculator.Wal), scenarios);
                    this.grid[1, i++].Value = "WAL";

                    // set Principal Writedown
                    SetScenariousCellValue(i, FormatDouble(double.NaN), scenarios);
                    this.grid[1, i++].Value = "Principal Writedown";

                    // set Accum Int Shortfall
                    SetScenariousCellValue(i, FormatDouble(double.NaN), scenarios);
                    this.grid[1, i++].Value = "Accum Int Shortfall";

                    // set Accum Coup Cap Shortfall
                    SetScenariousCellValue(i, FormatDouble(double.NaN), scenarios);
                    this.grid[1, i++].Value = "Accum Coup Cap Shortfall";

                    // set Min Credit Support Pct
                    SetScenariousCellValue(i, FormatDouble(double.NaN), scenarios);
                    this.grid[1, i++].Value = "Min Credit Support Pct";

                    // set Mod Durn
                    SetScenariousCellValue(i, FormatDouble(double.NaN), scenarios);
                    this.grid[1, i++].Value = "Mod Durn";
                }
            }

            // set grid Height
            SetGridHeight();

            // set grid Width
            SetGridWidth();
        }

        private void SetGridWidth()
        {
            int gridWidth = 0;
            for (int i = 0; i < grid.Columns.Count; i++)
            {
                gridWidth += grid.Columns[i].Width;
            }

            this.Width = gridWidth + 20;
        }

        private void SetGridHeight()
        {
            int gridHeight = 0;
            for (int i = 0; i < grid.Rows.Count; i++)
            {
                gridHeight += grid.Rows[i].Height;
            }

            this.Height = gridHeight;
        }

        private void SetScenariousCellValue(int i, object price, BindingList<Scenario> scenarios)
        {
            for (int scenario = 0; scenario < scenarios.Count; scenario++)
                this.grid[scenario + 2, i].Value = price;
        }


        private static string FormatDouble(double value)
        {
            if (double.IsNaN(value)) return "-";
            return value.ToString("N5");
        }
    }
}
