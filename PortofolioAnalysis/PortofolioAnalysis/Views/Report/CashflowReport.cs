﻿using System.Windows.Forms;
using Eu.AlterSystems.ASNetLib.Core;
using PortfolioAnalysis.Calculations;
using PortofolioAnalysis.Common.Entities;

namespace PortfolioAnalysis.Views.Report
{
    public partial class CashflowReport : Form
    {
        private readonly CashflowData[] cashflowsData;

        public CashflowReport(CashflowData[] cashflowsData)
        {
            this.cashflowsData = cashflowsData;
            InitializeComponent();

            ColumnExcelDate.DefaultCellStyle.Format = Context.Workspace.Parameters.DateFormatString;

            grid.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
        }

        private void SetData(int scenarioNumber)
        {
            grid.SuspendLayout();
            try
            {
                grid.Rows.Clear();

                if (scenarioNumber >= this.cashflowsData.Length) return;

                CashflowData cashflowData = this.cashflowsData[scenarioNumber];
                if (cashflowData.Security == null) return;

                for (int i = 0; i < cashflowData.Security.Calendar.Length; i++)
                {
                    this.grid.Rows.Add(new object[]
                                       {
                                           i,
                                           cashflowData.Security.Calendar[i],
                                           i == 0 ? 0 : cashflowData.GetDayCount(i),
                                           "",
                                           cashflowData.Coupon[i],
                                           cashflowData.CurrentAmount[0],
                                           cashflowData.Principal[i],
                                           cashflowData.Interest[i],
                                           cashflowData.CurrentAmount[i],
                                           cashflowData.Total[i],
                                       });
                }

                this.grid.AutoResizeColumns();
            }
            finally
            {
                grid.ResumeLayout();
            }
        }

        private void CashflowReportLoad(object sender, System.EventArgs e)
        {
            this.scenariosComboBox.DataSource = Context.Workspace.Scenarios;

            if (cashflowsData != null && cashflowsData.Length > 0)
            {
                PortfolioSecurity security = this.cashflowsData[0].Security;
                this.Text = "Cashflow Report " + security.LongDescription;
                ColumnDays.HeaderText = "Days \n" + EnumHelper.GetDescription(security.DayConvention);
            }
        }

        private void ScenariosComboBoxSelectedIndexChanged(object sender, System.EventArgs e)
        {
            this.SetData(scenariosComboBox.SelectedIndex);
        }
    }
}
