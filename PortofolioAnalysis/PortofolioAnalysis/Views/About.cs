using System.Reflection;
using System.Windows.Forms;

namespace PortfolioAnalysis.Views
{
    public partial class About : Form
    {
        public About()
        {
            InitializeComponent();

            this.Text = "About " + Constants.ApplicationName;

            appNameLabel.Text = Constants.ApplicationName;
            appVersionLabel.Text = "Version " + Assembly.GetExecutingAssembly().GetName().Version;
        }
    }
}