﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using ALMSCommon;


namespace PortfolioAnalysis.Infrastructure
{
    public class EntityArrayMapper<T> : IEntityArrayMapper
    {
        private readonly MethodInfo[] getters;
        private readonly string[] propertyNames;
        private readonly MethodInfo[] setters;

        public EntityArrayMapper(params Expression<Func<T, object>>[] propertyExpressions)
        {
            Debug.Assert(propertyExpressions != null);

            this.propertyNames = new string[propertyExpressions.Length];
            for (int i = 0; i < propertyExpressions.Length; i++)
            {
                if (propertyExpressions[i] == null) continue;
                propertyNames[i] = StaticReflector.GetAccessor(propertyExpressions[i]);
            }

            this.getters = new MethodInfo[propertyNames.Length];
            this.setters = new MethodInfo[propertyNames.Length];

            for (int i = 0; i < propertyNames.Length; i++)
            {
                if(string.IsNullOrEmpty(propertyNames[i])) continue;

                PropertyInfo propertyInfo = typeof(T).GetProperty(propertyNames[i]);
                this.getters[i] = propertyInfo.GetGetMethod();
                this.setters[i] = propertyInfo.GetSetMethod();
            }
        }

        public virtual object[] GetArray(object entity)
        {
            Debug.Assert(entity !=null);
            Debug.Assert(typeof(T).IsAssignableFrom(entity.GetType()));

            var result = new object[this.getters.Length];
            for (int i = 0; i < this.getters.Length; i++)
            {
                if (this.getters[i] == null) continue;
                result[i] = this.getters[i].Invoke(entity, null);
            }
            return result;
        }

        public virtual void SetPropertyByIndex(object entity, int index, object value)
        {
            Debug.Assert(entity != null);
            Debug.Assert(typeof(T).IsAssignableFrom(entity.GetType()));

            if(getters[index].ReturnType == typeof(string) && value != null)
            {
                value = value.ToString();
            }

            MethodInfo setter = this.setters[index];
            if(setter != null)
            {
                setter.Invoke(entity, new[] { value });
            }
        }

        public object GetPropertyByIndex(object entity, int index)
        {
            Debug.Assert(entity != null);
            Debug.Assert(typeof(T).IsAssignableFrom(entity.GetType()));

            return this.getters[index].Invoke(entity, null);
        }

        public virtual int Count
        {
            get { return propertyNames.Length; }
        }
    }
}