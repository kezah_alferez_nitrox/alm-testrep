using System;
using System.Collections.Generic;
using System.Linq;
using PortofolioAnalysis.Common.Infrastructure;
using ProtoBuf;

namespace PortfolioAnalysis.Infrastructure
{
    [ProtoContract]
    public class InMemoryRepository<TId, T>// : IRepository<TId, T>
        where T : AbstractEntity<TId>
    {
        [ProtoMember(1)]
        private readonly IList<T> entities = new List<T>();

        public T Get(TId id)
        {
            return this.entities.FirstOrDefault(d => Equals(d.Id, id));
        }

        public void Save(T entity)
        {
            if (this.entities.Contains(entity)) throw new InvalidOperationException("Entity already exists in the repository");

            this.entities.Add(entity);
        }

        public void Update(T entity)
        {
            if (!this.entities.Contains(entity)) throw new InvalidOperationException("Entity doesn't exist in the repository");
        }

        public void Delete(T entity)
        {
            if (!this.entities.Contains(entity)) throw new InvalidOperationException("Entity doesn't exist in the repository");

            this.entities.Remove(entity);
        }

        public void DeleteAll()
        {
            this.entities.Clear();
        }

        public int Count
        {
            get { return this.entities.Count; }
        }

        public IList<T> All
        {
            get { return this.entities; }
        }
    }
}