using System;
using System.Windows.Forms;

namespace PortfolioAnalysis.Infrastructure
{
    public static class DataGridViewUtil
    {
        public static void UpdateDateFormat(DataGridView grid)
        {
            foreach (DataGridViewColumn column in grid.Columns)
            {
                if (column.ValueType == typeof (DateTime))
                {
                    column.DefaultCellStyle.Format = Context.Workspace.Parameters.DateFormatString;
                    for (int rowIndex = 0; rowIndex < grid.RowCount; rowIndex++)
                    {
                        DataGridViewCell cell = grid[column.Name, rowIndex];
                        cell.Style.Format = Context.Workspace.Parameters.DateFormatString;
                    }
                }
            }
        }
    }
}