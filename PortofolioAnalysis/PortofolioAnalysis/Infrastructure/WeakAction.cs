﻿using System;
using System.Reflection;
using System.Threading;
using System.Diagnostics;

namespace PortfolioAnalysis.Infrastructure
{
    public class WeakAction
    {
        private readonly SynchronizationContext synchronizationContext;
        private readonly WeakReference targetRef;
        private readonly MethodInfo method;

        public WeakAction(Delegate del)
            : this(del, null)
        {
            this.targetRef = new WeakReference(del.Target);
            this.method = del.Method;
        }

        public WeakAction(Delegate del, SynchronizationContext synchronizationContext)
        {
            this.synchronizationContext = synchronizationContext;
            this.targetRef = new WeakReference(del.Target);
            this.method = del.Method;
        }

        [DebuggerStepThrough]
        public void Invoke(params object[] args)
        {
            object target = this.targetRef.Target;
            if (target == null) return;

            if (this.synchronizationContext == SynchronizationContext.Current)
            {
                this.method.Invoke(target, args);
            }
            else
            {
                this.synchronizationContext.Post(state => this.method.Invoke(target, args), null);
            }
        }

        public object Target
        {
            get { return this.targetRef.Target; }
        }
    }
}