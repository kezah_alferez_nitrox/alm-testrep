﻿namespace PortfolioAnalysis.Infrastructure
{
    public interface IEntityArrayMapper
    {
        object[] GetArray(object entity);
        void SetPropertyByIndex(object entity, int index, object value);
    }

    public interface IEntityArrayMapper<T> : IEntityArrayMapper
    {
        object[] GetArray(T entity);
        void SetPropertyByIndex(T entity, int index, object value);
    }
}