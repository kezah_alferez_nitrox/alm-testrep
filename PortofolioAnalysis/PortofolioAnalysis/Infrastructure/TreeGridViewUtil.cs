using System;
using System.Collections.Generic;
using ALMSCommon.Controls.TreeGridView;

namespace PortfolioAnalysis.Infrastructure
{
    public static class TreeGridViewUtil
    {
        public static IEnumerable<TreeGridNode> AllNodes(TreeGridView tree)
        {
            foreach (TreeGridNode node in tree.Nodes)
            {
                yield return node;

                foreach (TreeGridNode childnode in AllNodes(node.Nodes))
                {
                    yield return childnode;
                }
            }            
        }

        private static IEnumerable<TreeGridNode> AllNodes(IEnumerable<TreeGridNode> nodes)
        {
            foreach (TreeGridNode node in nodes)
            {
                yield return node;

                foreach (TreeGridNode childnode in AllNodes(node.Nodes))
                {
                    yield return childnode;
                }
            }
        }
    }
}