using System.Collections.Generic;

namespace PortfolioAnalysis.Infrastructure
{
    public interface IRepository<TId, T>
    {
        int Count { get; }
        T Get(TId id);
        void Save(T value);
        void Update(T value);
        void Delete(T value);
        void DeleteAll();
        IEnumerable<T> GetAll();
    }
}