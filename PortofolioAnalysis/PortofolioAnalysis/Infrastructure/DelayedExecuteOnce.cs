using System;
using System.Windows.Forms;
using ALMSCommon.Forms;

namespace PortfolioAnalysis.Infrastructure
{
    public class DelayedExecuteOnce
    {
        private readonly Action action;
        private readonly Timer timer = new Timer();

        public DelayedExecuteOnce(Action action, int waitMilliseconds)
        {
            this.action = action;

            this.timer.Interval = waitMilliseconds;
            this.timer.Tick += TimerCallback;
        }

        private void TimerCallback(object sender, EventArgs eventArgs)
        {
            this.timer.Stop();
            this.action();
        }

        public void TryExecute()
        {
            this.timer.Stop();
            this.timer.Start();
        }
    }
}