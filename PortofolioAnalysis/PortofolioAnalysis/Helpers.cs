using System;
using System.Drawing;
using System.Globalization;
using PortfolioAnalysis.Properties;
using PortofolioAnalysis.Common.Entities;

namespace PortfolioAnalysis
{
    public class Helpers
    {
        private static readonly CultureInfo[] acceptedCultures = new[]
                                                                 {
                                                                     new CultureInfo("fr-FR"),
                                                                     new CultureInfo("en-US"),
                                                                 };

        public static double? TryConvertToDouble(string str)
        {
            if (String.IsNullOrEmpty(str)) return null;
            str = str.Trim();

            double factor = 1;
            if (str.EndsWith("%"))
            {
                str = str.Replace("%", "").Trim();
                factor = 0.01;
            }

            foreach (CultureInfo accepedCulture in acceptedCultures)
            {
                double parsedvalue;
                if (Double.TryParse(str, NumberStyles.Any, accepedCulture, out parsedvalue))
                {
                    return parsedvalue * factor;
                }
            }

            return null;
        }

        public static int? TryConvertToInt(string str)
        {
            if (String.IsNullOrEmpty(str))
            {
                return null;
            }
            str = str.Trim();

            int parsedvalue;
            if (Int32.TryParse(str, out parsedvalue))
            {
                return parsedvalue;
            }

            return null;
        }

        public static Image GetStatusImage(Status status)
        {
            switch (status)
            {
                case Status.Ok:
                    return Resources.green_sphere;
                case Status.No:
                    return Resources.red_sphere;
                default:
                    return Resources.gray_sphere;
            }
        }

        public static int ExcelColumnToIndex(string column)
        {
            int value = 0;

            for (int i = 0; i < column.Length; i++)
            {
                value += (column[i] - 'A' + 1) * (int)Math.Pow(26, column.Length - 1 - i);
            }

            return value - 1;
        }
    }
}