using System;
using PortofolioAnalysis.Common.DatabaseEntities;
using PortofolioAnalysis.Common.Entities;

namespace PortfolioAnalysis.Calculations
{
    public class TrancheCalculator
    {
        private readonly PortfolioSecurity security;
        private readonly CashflowData[] cashflowsData;
        private readonly double[] indexValues;
        private readonly int scenarioCount;

        public double[] Dm { get; private set; }
        public double[] Price { get; private set; }
        public double[] Yield { get; private set; }
        public double Wal { get; private set; }
        public double[] ModDurn { get; private set; }

        public TrancheCalculator(PortfolioSecurity security, CashflowData[] cashflowsData, double[] indexValues)
        {
            this.security = security;
            this.cashflowsData = cashflowsData;
            this.indexValues = indexValues;

            this.scenarioCount = cashflowsData.Length;

            int totalValueCount = (((security.NumberOfSteps*2) + 1))*scenarioCount;

            this.Dm = new double[totalValueCount];
            this.Price = new double[totalValueCount];
            this.Yield = new double[totalValueCount];
            this.ModDurn = new double[totalValueCount];
        }

        public void Calculate()
        {
            int delta = 0;
            for (int scenario = 0; scenario < scenarioCount; scenario++)
            {
                CashflowData cashflowData = this.cashflowsData[scenario];

                PricingCalculator pricingCalculator = new PricingCalculator(this.security.CurrentAmount, cashflowData.Days, cashflowData.Total, this.indexValues);

                this.Wal = pricingCalculator.GetWal(cashflowData.Principal);

                var nrOfSteps = security.NumberOfSteps*2 + 1;
                var ammount = - nrOfSteps / 2;
                for (double step = 0; step < nrOfSteps; step++)
                {
                    double given = security.GivenAmount + security.StepAmount * ammount++;
                    switch (security.GivenType)
                    {
                        case GivenType.Dm:
                            pricingCalculator.SolveWithDm(given);
                            break;
                        case GivenType.Price:
                            pricingCalculator.SolveWithPrice(given);
                            break;
                        case GivenType.Yield:
                            pricingCalculator.SolveWithXirr(given);
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }

                    this.Dm[delta] = pricingCalculator.Dm;
                    this.Price[delta] = pricingCalculator.Price;
                    this.Yield[delta] = pricingCalculator.Xirr;
                    this.ModDurn[delta] = pricingCalculator.GetModDurn();
                    delta++;
                }
            }
        }
    }
}