using System;
using System.Diagnostics;

namespace PortfolioAnalysis.Calculations
{
    // http://support.microsoft.com/kb/100782/en-us
    // http://blogs.msdn.com/b/lucabol/archive/2007/12/17/bisection-based-xirr-implementation-in-c.aspx

    public delegate double GoalFunction(double input);

    public static class GoalSeekCalculator
    {
        private const int MaxIterations = 100;

        private static Bracket FindBrackets(GoalFunction func, double guess)
        {
            // Abracadabra magic numbers ...
            const double bracketStep = 0.5;

            double leftBracket = guess - bracketStep;
            double rightBracket = guess + bracketStep;
            var iteration = 0;

            while (func(leftBracket) * func(rightBracket) > 0 && iteration++ < MaxIterations)
            {
                double delta = bracketStep * Math.Pow(2, iteration / 2.0);
                leftBracket -= delta;
                rightBracket += delta;
            }

            if (iteration >= MaxIterations)
                return new Bracket(0, 0);

            return new Bracket(leftBracket, rightBracket);
        }

        public static Result Bisection(GoalFunction func, Bracket bracket)
        {
            const double tolerance = 0.000001;

            int iter = 1;

            double f3;
            double x3;
            double x1 = bracket.Left;
            double x2 = bracket.Right;

            do
            {
                var f1 = func(x1);
                var f2 = func(x2);

                if (f1 == 0 && f2 == 0)
                    return new Result(ApproximateResultKind.NoSolutionWithinTolerance, x1);

                if (f1 * f2 > 0)
                    throw new ArgumentException("x1 x2 values don't bracket a root");

                x3 = (x1 + x2) / 2;
                f3 = func(x3);

                if (f3 * f1 < 0)
                    x2 = x3;
                else
                    x1 = x3;

                iter++;

            } while (Math.Abs(x1 - x2) / 2 > tolerance && f3 != 0 && iter < MaxIterations);

            //Debug.WriteLine("iter " + iter);

            if (f3 == 0)
                return new Result(ApproximateResultKind.ExactSolution, x3);

            if (Math.Abs(x1 - x2) / 2 < tolerance)
                return new Result(ApproximateResultKind.ApproximateSolution, x3);

            if (iter > MaxIterations)
                return new Result(ApproximateResultKind.NoSolutionWithinTolerance, x3);

            throw new Exception("It should never get here");
        }

        public static Result Calculate(GoalFunction func, double guess)
        {
            var brackets = FindBrackets(func, guess);

            if (brackets.Left == brackets.Right)
                return new Result(ApproximateResultKind.NoSolutionWithinTolerance, brackets.Left);

            return Bisection(func, brackets);
        }

        public struct Bracket
        {
            public readonly double Left;
            public readonly double Right;
            public Bracket(double left, double right)
            {
                this.Left = left; this.Right = right;
            }
        }

        public struct Result
        {
            public Result(ApproximateResultKind kind, double value)
            {
                Kind = kind;
                Value = value;
            }

            public readonly ApproximateResultKind Kind;
            public readonly double Value;
        }

        public enum ApproximateResultKind
        {
            ApproximateSolution,
            ExactSolution,
            NoSolutionWithinTolerance
        }
    }
}