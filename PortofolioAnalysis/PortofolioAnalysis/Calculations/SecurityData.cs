using System;
using ALMS.Products;
using PortofolioAnalysis.Common.Entities;

namespace PortfolioAnalysis.Calculations
{
    public class SecurityData
    {
        private readonly PortfolioSecurity security;
        private readonly Cashflow[] cashflows;

        public SecurityData(PortfolioSecurity security, IFormulaEvaluator formulaEvaluator)
        {
            this.security = security;

            CashflowProduct cashflowProduct = new CashflowProduct(security.FirstCouponDate, security);//, null, formulaEvaluator, null);
            this.cashflows = cashflowProduct.Cashflows;
        }

        public PortfolioSecurity Security
        {
            get { return this.security; }
        }

        public double[] CurrentAmount { get; private set; }

        public double[] Interest { get; private set; }

        public double[] Principal { get; private set; }

        public double[] Total { get; private set; }

        private double[] coupon;        

        public double[] Coupon
        {
            get { return this.coupon; }
        }

        public void Compute(double[] cpn, double[] cdr, double[] cpr, double[] severity)
        {
            Array.Copy(cpn, coupon, cpn.Length);

            this.CurrentAmount[0] = this.security.CurrentAmount;

            int length = this.cashflows.Length;
            for (int i = 1; i < length; i++)
            {
                double dayCount = this.DayCount(i);

                double defaults = this.CurrentAmount[i - 1] * cdr[i];
                double loss = defaults * severity[i];
                double recovery = defaults * (1 - severity[i]);

                this.Interest[i] = (this.CurrentAmount[i - 1] - defaults) * coupon[i] * dayCount / 360.0;

                double prepaidPrincipal = (this.CurrentAmount[i - 1] - defaults) * cpr[i];
                this.Principal[i] = prepaidPrincipal + recovery;

                this.Total[i] = this.Principal[i] + this.Interest[i];

                double amortizationAmount = prepaidPrincipal + defaults;
                this.CurrentAmount[i] = this.CurrentAmount[i - 1] - amortizationAmount;
            }

            this.Principal[length - 1] = this.CurrentAmount[length - 1];
            this.CurrentAmount[length - 1] = 0;
        }

        public double DayCount(int i)
        {
            return this.cashflows[i].Cvg * 360.0;
        }

        public double[] Days
        {
            get
            {
                double[] result = new double[this.cashflows.Length];
                for (int i = 1; i < result.Length; i++)
                {
                    result[i] = this.DayCount(i);
                }
                return result;
            }
        }
    }
}