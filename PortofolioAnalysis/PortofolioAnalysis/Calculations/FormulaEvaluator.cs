using System;
using System.Collections.Generic;
using ALMS.Products;
using NCalc;
using PortofolioAnalysis.Common.Entities;

namespace PortfolioAnalysis.Calculations
{
    public class FormulaEvaluator : IFormulaEvaluator
    {
        private readonly IEnumerable<Vector> vectors;

        public FormulaEvaluator(IEnumerable<Vector> vectors)
        {
            this.vectors = vectors;
            foreach (Vector vector in vectors)
            {
                vector.GenerateDates();
            }
        }

        public double Evaluate(string formula, DateTime date)
        {
            Dictionary<string, object> values = new Dictionary<string, object>(32, StringComparer.InvariantCultureIgnoreCase);

            foreach (Vector vector in vectors)
            {
                values[vector.Name] = vector.GetValueAtDate(date);
            }

            if (string.IsNullOrEmpty(formula)) return 0;

            formula = formula.Replace("%", "*0.01");

            return Evaluate(formula, values);
        }

        public static double Evaluate(string formula, Dictionary<string, object> parameters)
        {
            try
            {
                Expression expression = new Expression(formula);
                expression.Parameters = parameters;
                object result = expression.Evaluate();

                return Convert.ToDouble(result);
            }
            catch (Exception ex)
            {
                throw new Exception("Error in formula '" + formula + "'", ex);
            }
        }
    }
}