using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using ALMS.Products;
using PortofolioAnalysis.Common.Entities;
using CouponType = PortofolioAnalysis.Common.DatabaseEntities.CouponType;

namespace PortfolioAnalysis.Calculations
{
    public class ResultCalculator
    {
        private IList<Vector> vectors;
        private readonly Evaluator evaluator = new Evaluator();

        private IFormulaEvaluator formulaEvaluator;

        private readonly IList<string> errors = new List<string>();

        private readonly IDictionary<PortfolioSecurity, CashflowData[]> cashflows = new Dictionary<PortfolioSecurity, CashflowData[]>();
        private IList<PortfolioSecurity> securities;
        private IFundamentalVector[] fundamentalVectors;

        public bool Canceled { get; private set; }

        public IList<string> Errors
        {
            get { return this.errors; }
        }

        public IDictionary<PortfolioSecurity, CashflowData[]> Cashflows
        {
            get { return this.cashflows; }
        }

        public void Calculate(BackgroundWorker bw)
        {
            this.Errors.Clear();

            this.vectors = Context.Workspace.Vectors;

            this.formulaEvaluator = new FormulaEvaluator(this.vectors);

            /*
            this.fundamentalVectors = this.vectors.Where(v => v.IsFundametal).OrderBy(fv => fv.Unit).ThenBy(fv => fv.Delta).ToArray();

            if (fundamentalVectors.Length == 0)
            {
                this.Errors.Add("No fundamental vectors found.");
            }
             */

            int scenarioCount = Context.Workspace.Scenarios.Count;

            this.securities = Context.Workspace.AllSetupSecurities.Where(s => !s.Exclude).ToList();

            ValidateSecurities();

            if (this.Errors.Count > 0) return;

            Initialize(scenarioCount);

            for (int s = 0; s < scenarioCount; s++)
            {
                if (bw.CancellationPending)
                {
                    this.Canceled = true;
                    return;
                }

                try
                {
                    RunScenario(s);
                }
                catch (Exception ex)
                {
                    this.Errors.Add(ex.ToString());
                    break;
                }

                bw.ReportProgress(100 * (s + 1) / scenarioCount);
            }

            bw.ReportProgress(100);
        }

        private void ValidateSecurities()
        {
            foreach (PortfolioSecurity security in securities)
            {
                this.ValidateSecurity(security);
            }
        }

        private void ValidateSecurity(PortfolioSecurity security)
        {
            if (security.CouponType == CouponType.Fixed) return;

            if (string.IsNullOrEmpty(security.ResetIndex))
            {
                this.Errors.Add(string.Format("No reset index specified for the security [{0}]", security));
                return;
            }

            if (this.vectors.All(v => v.Name != security.ResetIndex))
            {
                this.Errors.Add(string.Format("No vector found for the reset index {0} of the security [{1}]", security.ResetIndex, security));
            }
        }

        private void Initialize(int scenarioCount)
        {
            foreach (PortfolioSecurity security in securities)
            {
                cashflows.Add(security, new CashflowData[scenarioCount]);
            }
        }

        private void RunScenario(int scenarioNumber)
        {
            foreach (PortfolioSecurity security in securities)
            {
                SecurityData securityData = new SecurityData(security, this.formulaEvaluator);

                CashflowData cashflowData = new CashflowData(security);
                this.Cashflows[security][scenarioNumber] = cashflowData;

                SecurityScenarioValues scenarioValues = security.ScenarioValues[scenarioNumber];

                string defaultExpression = string.IsNullOrEmpty(scenarioValues.Default) ? "0" : scenarioValues.Default;
                string prepayExpression = string.IsNullOrEmpty(scenarioValues.Prepay) ? "0" : scenarioValues.Prepay;
                string severityExpression = string.IsNullOrEmpty(scenarioValues.Severity) ? "0" : scenarioValues.Severity;

                evaluator.SetCalendar(cashflowData.Security.Calendar);

                double[] coupon = EvaluateCoupons(cashflowData.Security);
                double[] cdr = evaluator.Evaluate(defaultExpression);
                double[] cpr = evaluator.Evaluate(prepayExpression);
                double[] severity = evaluator.Evaluate(severityExpression);
                cashflowData.Compute(coupon, cdr, cpr, severity);
            }
        }

        private double[] EvaluateCoupons(PortfolioSecurity security)
        {
            double[] fixedCoupon = this.evaluator.Evaluate(security.Coupon + "%");
            if (security.CouponType == CouponType.Fixed)
            {
                return fixedCoupon;
            }

            double[] floatCoupon = this.evaluator.Evaluate(security.ResetIndex);
            for (int i = 0; i < floatCoupon.Length; i++)
            {
                floatCoupon[i] += security.Spread / 100.0;
            }
            if (security.CouponType == CouponType.Floating)
            {
                return floatCoupon;
            }

            int j = 0;
            while (j < security.Calendar.Length && security.Calendar[j] < security.ResetDate)
            {
                floatCoupon[j] = fixedCoupon[j];
                j++;
            }

            return floatCoupon;
        }
    }
}