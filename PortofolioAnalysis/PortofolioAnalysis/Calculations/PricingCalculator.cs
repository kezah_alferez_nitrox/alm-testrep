using System;
using System.Linq;

namespace PortfolioAnalysis.Calculations
{
    public class PricingCalculator
    {
        private readonly double currentBalance;
        private readonly int[] days;
        private readonly double[] totalCashflow;
        private readonly double[] index;

        private readonly double[] dmFactors;
        private readonly double[] cashflowsXirr;

        public double Dm { get; private set; }
        public double Price { get; private set; }
        public double Xirr { get; private set; }

        public PricingCalculator(double currentBalance, int[] days, double[] totalCashflow, double[] index)
        {
            this.currentBalance = currentBalance;
            this.days = days;
            this.totalCashflow = totalCashflow;
            this.index = index;

            this.dmFactors = new double[days.Length];
            this.cashflowsXirr = new double[this.days.Length];
        }

        public double GetWal(double[] principal)
        {
            double sum = 0;
            for (int i = 1; i < principal.Length; i++)
            {
                sum += i * principal[i];
            }
            return sum / 4 / currentBalance;
        }

        public double GetModDurn()
        {
            double[] macaulayDurnPerPeriod = new double[days.Length];

            int totalDays = 0;
            for (int i = 1; i < days.Length; i++)
            {
                totalDays += days[i];
                macaulayDurnPerPeriod[i] = totalDays / 360.0 * this.totalCashflow[i] /
                    Math.Pow(1 + this.Xirr / 200.0, 2 * totalDays / 360.0);
            }

            double macaulayDurn = macaulayDurnPerPeriod.Sum() / currentBalance;

            return macaulayDurn / (1 + this.Xirr / 200.0);
        }

        public void SolveWithDm(double givenDm)
        {
            this.Dm = givenDm;

            this.Price = this.GetPriceGivenDm();
            this.Xirr = this.GetXirrGivenPrice();
        }

        public void SolveWithXirr(double givenXirr)
        {
            this.Xirr = givenXirr;

            this.Price = this.GetPriceGivenXirr();
            this.Dm = this.GetDmGivenPrice();
        }

        public void SolveWithPrice(double givenPrice)
        {
            this.Price = givenPrice;

            this.Xirr = this.GetXirrGivenPrice();
            this.Dm = this.GetDmGivenPrice();
        }

        private double GetPriceGivenXirr()
        {
            PricingFunctions.ComputeCashflowXirr(this.days, this.totalCashflow, this.Xirr, this.cashflowsXirr);

            return this.cashflowsXirr.Sum() / currentBalance;
        }

        private double GetPriceGivenDm()
        {
            PricingFunctions.ComputeDmFactor(this.days, this.index, this.Dm, this.dmFactors);
            return ExcelFunctions.ProductsSum(this.totalCashflow, this.dmFactors, 1) / currentBalance;
        }

        private double GetDmGivenPrice()
        {
            return ExcelFunctions.GoalSeek(this.DmGoalFunction, 1);
        }

        private double GetXirrGivenPrice()
        {
            return ExcelFunctions.GoalSeek(this.XirrGivenDmGoalFunction, 1);
        }

        private double XirrGivenDmGoalFunction(double changingXirr)
        {
            PricingFunctions.ComputeCashflowXirr(this.days, this.totalCashflow, changingXirr, this.cashflowsXirr);

            return this.Price * currentBalance - this.cashflowsXirr.Sum();
        }

        private double DmGoalFunction(double changingDm)
        {
            PricingFunctions.ComputeDmFactor(this.days, this.index, changingDm, this.dmFactors);

            return this.Price * currentBalance - ExcelFunctions.ProductsSum(totalCashflow, dmFactors, 1);
        }

        public static class PricingFunctions
        {
            public static void ComputeDmFactor(int[] days, double[] index, double dm, double[] output)
            {
                output[0] = 1;
                for (int i = 1; i < days.Length; i++)
                {
                    output[i] = output[i - 1] / (1 + (index[i] + dm / 10000.0) * days[i] / 360.0);
                }
            }

            public static void ComputeCashflowXirr(int[] days, double[] totalCashflow, double xirr, double[] output)
            {
                if (xirr <= -1)
                    xirr = -0.99999999;

                int dayCount = 0;
                output[0] = 0;
                for (int i = 1; i < days.Length; i++)
                {
                    dayCount += days[i];
                    output[i] = totalCashflow[i] / Math.Pow(1.0 + xirr / 100.0, dayCount / 365.0);
                }
            }
        }
    }
}