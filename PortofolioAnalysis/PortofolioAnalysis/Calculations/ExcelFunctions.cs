using System;
using System.Diagnostics;

namespace PortfolioAnalysis.Calculations
{
    public static class ExcelFunctions
    {
        public static double ProductsSum(double[] a, double[] b, int startIndex)
        {
            Debug.Assert(a != null);
            Debug.Assert(b != null);
            Debug.Assert(a.Length == b.Length);
            Debug.Assert(startIndex < a.Length);

            double result = 0;
            for (int i = startIndex; i < a.Length; i++)
            {
                result += a[i] * b[i];
            }

            return result;
        }

        public static double ProductsSum(double[] a, double[] b)
        {
            return ProductsSum(a, b, 0);
        }

        public static double GoalSeek(GoalFunction func,double guess)
        {
            GoalSeekCalculator.Result result = GoalSeekCalculator.Calculate(func, guess);

            Debug.WriteLine("GoalSeek : " + result.Kind);

            return result.Kind == GoalSeekCalculator.ApproximateResultKind.NoSolutionWithinTolerance ? double.NaN :
            result.Value;
        }

        public static double GoalSeek(GoalFunction func)
        {
            return GoalSeek(func, 0);
        }
    }
}