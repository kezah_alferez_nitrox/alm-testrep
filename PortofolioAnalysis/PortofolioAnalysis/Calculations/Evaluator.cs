using System;
using System.Collections.Generic;
using NCalc;
using PortofolioAnalysis.Common.Entities;

namespace PortfolioAnalysis.Calculations
{
    public class Evaluator
    {
        private readonly IList<Vector> vectors;
        private readonly IList<Variable> variables;

        private DateTime[] calendar;
        private Dictionary<string, object>[] variableValues;

        public Evaluator()
        {
            this.vectors = Context.Workspace.Vectors;
            foreach (Vector vector in vectors)
            {
                vector.GenerateDates();
            }

            this.variables = Context.Workspace.Variables;
        }

        public void SetCalendar(DateTime[] dates)
        {
            this.calendar = dates;

            this.ComputeVariableValues();
        }

        private void ComputeVariableValues()
        {
            variableValues = new Dictionary<string, object>[calendar.Length];

            for (int i = 0; i < calendar.Length; i++)
            {
                variableValues[i] = new Dictionary<string, object>(32, StringComparer.InvariantCultureIgnoreCase);
                foreach (Vector vector in vectors)
                {
                    variableValues[i][vector.Name] = vector.GetValueAtDate(calendar[i]);
                }

                foreach (Variable variable in variables)
                {
                    variableValues[i][variable.Name] = ComputeExpression(variable.Expression, variableValues[i]);
                }
            }
        }

        public double[] Evaluate(string expression)
        {
            double[] result = new double[calendar.Length];

            if (string.IsNullOrEmpty(expression)) expression = "0";

            expression = expression.Replace("%", "*0.01");

            for (int i = 0; i < result.Length; i++)
            {
                result[i] = Evaluate(expression, variableValues[i]);
            }

            return result;
        }

        public double ComputeExpression(string expression, Dictionary<string, object> values)
        {
            expression = expression.Replace("%", "*0.01");

            double result = Evaluate(expression, values);

            return result;
        }

        public static double Evaluate(string formula, Dictionary<string, object> parameters)
        {
            try
            {
                Expression expression = new Expression(formula);
                expression.Parameters = parameters;
                object result = expression.Evaluate();

                return Convert.ToDouble(result);
            }
            catch (Exception ex)
            {
                throw new Exception("Error in formula '" + formula + "'", ex);
            }
        }
    }
}