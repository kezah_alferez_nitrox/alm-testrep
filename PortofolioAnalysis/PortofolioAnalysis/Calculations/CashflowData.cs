using System;
using ALMS.Products;
using PortofolioAnalysis.Common.Entities;

namespace PortfolioAnalysis.Calculations
{
    public struct CashflowData
    {
        private readonly PortfolioSecurity security;
        private readonly DateTime[] calendar;
        private readonly double[] currentAmount;
        private readonly double[] interest;
        private readonly double[] principal;
        private readonly double[] total;
        private readonly double[] coupon;

        public CashflowData(PortfolioSecurity security)
        {
            this.security = security;

            this.calendar = this.security.Calendar;

            this.currentAmount = new double[calendar.Length];
            this.interest = new double[calendar.Length];
            this.principal = new double[calendar.Length];
            this.total = new double[calendar.Length];

            this.coupon = new double[calendar.Length];
        }

        public PortfolioSecurity Security
        {
            get { return this.security; }
        }

        public double[] CurrentAmount
        {
            get { return this.currentAmount; }
        }

        public double[] Interest
        {
            get { return this.interest; }
        }

        public double[] Principal
        {
            get { return this.principal; }
        }

        public double[] Total
        {
            get { return this.total; }
        }

        public double[] Coupon
        {
            get { return this.coupon; }
        }

        public void Compute(double[] cpn, double[] cdr, double[] cpr, double[] severity)
        {
            Array.Copy(cpn, coupon, cpn.Length);

            this.CurrentAmount[0] = this.security.CurrentAmount;

            for (int i = 1; i < calendar.Length; i++)
            {
                double ratiosYearFraction = GetYearFraction(i, CouponBasis.ACT365);
                double cdrAdjusted = cdr[i] * ratiosYearFraction;
                double cprAdjusted = cpr[i] * ratiosYearFraction;

                double previousAmount = this.CurrentAmount[i - 1];

                double defaults = previousAmount * cdrAdjusted;
                double loss = defaults * severity[i];
                double recovery = defaults * (1 - severity[i]);

                double periodCoupon = coupon[i] * GetYearFraction(i, security.GetCouponBasis());
                this.Interest[i] = (previousAmount - defaults) * periodCoupon;

                double prepaidPrincipal = (previousAmount - defaults) * cprAdjusted;
                this.Principal[i] = prepaidPrincipal + recovery;

                double amortizationAmount = prepaidPrincipal + defaults;
                this.CurrentAmount[i] = previousAmount - amortizationAmount; // previousAmount * (1 - cdrAdjusted) * (1 - cprAdjusted);

                if (i == this.calendar.Length - 1)
                {
                    this.Principal[i] += this.CurrentAmount[i];
                    this.CurrentAmount[i] = 0;
                }

                this.Total[i] = this.Principal[i] + this.Interest[i];
            }
        }

        public int GetDayCount(int i)
        {
            return DayCounter.GetDays(this.calendar[i - 1], this.calendar[i], this.security.GetCouponBasis());
        }

        private double GetYearFraction(int i, CouponBasis couponBasis)
        {
            return DayCounter.GetCvg(this.calendar[i - 1], this.calendar[i], couponBasis);
        }

        public int[] Days
        {
            get
            {
                int[] result = new int[calendar.Length];
                for (int i = 1; i < calendar.Length; i++)
                {
                    result[i] = this.GetDayCount(i);
                }
                return result;
            }
        }
    }
}