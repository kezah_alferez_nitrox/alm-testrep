using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using ALMSCommon;
using Newtonsoft.Json;
using PortfolioAnalysis.IO;
using PortofolioAnalysis.Common;
using PortofolioAnalysis.Common.Entities;

namespace PortfolioAnalysis
{
    public class Workspace
    {
        private readonly WorkspaceSerializer serializer;

        private string fileName;
        public string FileName
        {
            get { return this.fileName; }
            private set
            {
                if (this.fileName != value)
                {
                    this.fileName = value;
                    Context.EventAggregator.Publish(new WorkspaceFileNameChangedEvent());
                }
            }
        }

        public Workspace()
        {
            this.serializer = new WorkspaceSerializer();
            this.Init();
        }

        private bool isDirty;
        public bool IsDirty
        {
            get { return this.isDirty; }
            private set
            {
                if (this.isDirty != value)
                {
                    this.isDirty = value;
                    Context.EventAggregator.Publish(new WorkspaceDirtyChangedEvent());
                }
            }
        }

        private BindingList<Category> portfolios;
        public BindingList<Category> Portfolios
        {
            get { return this.portfolios; }
            private set
            {
                if (this.portfolios != value)
                {
                    if (this.portfolios != null) this.portfolios.ListChanged -= this.ListChanged;
                    this.portfolios = value;
                    if (this.portfolios != null) this.portfolios.ListChanged += this.ListChanged;
                }
            }
        }

        private Category setupSecurities;
        public Category SetupSecurities
        {
            get { return this.setupSecurities; }
            private set
            {
                if (this.setupSecurities != value)
                {
                    if (setupSecurities != null) setupSecurities.PropertyChanged -= this.PropertyChanged;
                    this.setupSecurities = value;
                    if (setupSecurities != null) setupSecurities.PropertyChanged += this.PropertyChanged;
                }
            }
        }

        private BindingList<Scenario> scenarios;
        public BindingList<Scenario> Scenarios
        {
            get { return this.scenarios; }
            private set
            {
                if (this.scenarios != value)
                {
                    if (this.scenarios != null) this.scenarios.ListChanged -= this.ListChanged;
                    this.scenarios = value;
                    if (this.scenarios != null) this.scenarios.ListChanged += this.ListChanged;
                }
            }
        }

        private BindingList<Vector> vectors;
        public BindingList<Vector> Vectors
        {
            get { return this.vectors; }
            private set
            {
                if (this.vectors != value)
                {
                    if (this.vectors != null) this.vectors.ListChanged -= this.ListChanged;
                    this.vectors = value;
                    if (this.vectors != null) this.vectors.ListChanged += this.ListChanged;
                }
            }
        }

        private void ListChanged(object sender, ListChangedEventArgs e)
        {
            this.IsDirty = true;
        }

        private BindingList<Variable> variables;
        public BindingList<Variable> Variables
        {
            get { return this.variables; }
            private set
            {
                if (this.variables != value)
                {
                    if (this.variables != null) this.variables.ListChanged -= this.ListChanged;
                    this.variables = value;
                    if (this.variables != null) this.variables.ListChanged += this.ListChanged;
                }
            }
        }

        private Parameters parameters;
        public Parameters Parameters
        {
            get { return this.parameters; }
            private set
            {
                if (this.parameters != value)
                {
                    if (parameters != null) parameters.PropertyChanged -= this.ParametersChanged;
                    this.parameters = value;
                    Context.EventAggregator.Publish(new DateFormatChangedEvent());
                    if (parameters != null) parameters.PropertyChanged += this.ParametersChanged;
                }
            }
        }


        private void ParametersChanged(object sender, PropertyChangedEventArgs e)
        {
            if(e.PropertyName == "DateFormat")
            {
                Context.EventAggregator.Publish(new DateFormatChangedEvent());
            }

            this.IsDirty = true;
        }

        private void PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            this.IsDirty = true;
        }

        private void Init()
        {
            this.Portfolios = new BindingList<Category>();
            this.SetupSecurities = new Category();
            this.Scenarios = new BindingList<Scenario>();
            this.Vectors = new BindingList<Vector>();
            this.Variables = new BindingList<Variable>();
            this.Parameters = new Parameters();
            this.IsDirty = false;
        }

        public void Load(string newFileName)
        {            
            Workspace workspace = null;

            try
            {
                workspace = WorkspaceSerializer.Load(newFileName);
            }
            catch (Exception ex)
            {
                Logger.Exception("Load Workspace", ex);

                Dialogs.Error("Loading Workspace", "Error while loading Workspace: the file format is not recognised");
            }

            if (workspace == null) return;

            Util.CopyProperties(workspace, this);
            //this.Portfolios = workspace.Portfolios;
            //this.SetupSecurities = workspace.SetupSecurities;
            //this.Scenarios = workspace.Scenarios;
            //this.Vectors = workspace.Vectors;
            //this.Variables = workspace.Variables;

            this.FileName = newFileName;
            this.IsDirty = false;

            Context.EventAggregator.Publish(new WorkspaceLoadedEvent());
        }

        public bool Save()
        {
            if (string.IsNullOrEmpty(this.FileName))
            {
                return false;
            }

            this.serializer.Save(this, this.FileName);

            this.IsDirty = false;

            return true;
        }

        public void SaveAs(string newFileName)
        {
            this.FileName = newFileName;
            this.Save();
        }

        [JsonIgnore]
        public IList<PortfolioSecurity> AllSecurities
        {
            get
            {
                List<PortfolioSecurity> result = new List<PortfolioSecurity>();

                foreach (Category portfolio in this.Portfolios)
                {
                    result.AddRange(portfolio.AllSecurities);
                }

                return result;
            }
        }

        [JsonIgnore]
        public IList<PortfolioSecurity> AllSetupSecurities
        {
            get
            {
                return Context.Workspace.SetupSecurities.AllSecurities;
            }
        }

        public void ImportPortfolio(string newFileName)
        {
            // todo
            Context.EventAggregator.Publish(new WorkspaceLoadedEvent());
        }

        public void ExportPortfolio(Category portfolio, string newFileName)
        {
            // todo
        }

        public void New()
        {
            this.Init();
            this.FileName = null;

            Context.EventAggregator.Publish(new WorkspaceLoadedEvent());
        }

        [Conditional("DEBUG")]
        private void CreateTestData()
        {
            var portfolioSecurities = new List<PortfolioSecurity>
                                                          {
                                                              new PortfolioSecurity {Id="CUSSIP1", ISIN = "ISIN1", Description = "SEC XXXXXX", IndustrySector = "sector 1"},
                                                              new PortfolioSecurity {Id="CUSSIP2", ISIN = "ISIN2", Description = "SEC YYYYYY", IndustrySector = "sector 1"},
                                                              new PortfolioSecurity {Id="CUSSIP3", ISIN = "ISIN3", Description = "SEC ZZZZZZ", IndustrySector = "sector 2"},
                                                          };

            var portfolio = new Category
                                {
                                    Name = "Portfolio 1",
                                    Securities = new BindingList<PortfolioSecurity>
                                                 {
                                                     portfolioSecurities[0], 
                                                     portfolioSecurities[1]
                                                 },

                                };
            var categories = new BindingList<Category>
                                 {
                                     new Category 
                                     {
                                         Name = "Category 1", 
                                         Parent = portfolio,
                                         Securities = new BindingList<PortfolioSecurity>{portfolioSecurities[2]}
                                     }
                                 };
            portfolio.Categories = categories;
            this.Portfolios.Add(portfolio);

            this.SetupSecurities = new Category()
                                {
                                    Name = "Group 1",
                                    Securities = new BindingList<PortfolioSecurity>
                                                 {
                                                     portfolioSecurities[0],
                                                 },

                                };

            this.Scenarios.Add(new Scenario { Name = "Scenario 1 \n\rBase" });
            this.Scenarios.Add(new Scenario { Name = "Scenario 2 \n\rStress" });
            this.Scenarios.Add(new Scenario { Name = "Scenario 3 \n\rHarsh" });

            foreach (PortfolioSecurity portfolioSecurity in portfolioSecurities)
            {
                foreach (Scenario scenario in Scenarios)
                {
                    portfolioSecurity.ScenarioValues.Add(new SecurityScenarioValues { Default = "2%", Prepay = "ABC", Severity = "XYZ" });
                }
            }
        }
    }
}