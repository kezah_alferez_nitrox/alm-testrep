﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Net;
using System.Threading;
using System.Windows.Forms;
using PortfolioAnalysis.Views;
using PortofolioAnalysis.Common.DatabaseEntities;

namespace PortfolioAnalysis
{
    internal static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        [Conditional("DEBUG")] // In production this program will only be available though the launcer
        private static void Main()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");

            SynchronizationContext.SetSynchronizationContext(new SynchronizationContext());

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }

        public static void Test()
        {
            HttpWebRequest webRequest = (HttpWebRequest)
                                        WebRequest.Create(
                                            "http://localhost:3351/PortfolioAnalysis.Web/GetSecurities.aspx");
            webRequest.Method = "GET";

            using (WebResponse response = webRequest.GetResponse())
            {
                Console.WriteLine(((HttpWebResponse) response).StatusDescription);

                using (Stream stream = response.GetResponseStream())
                {
                  //  IList<Security> securities = Serializer.Deserialize<IList<Security>>(stream);
                }
            }
        }
    }
}