﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlServerCe;
using System.IO;
using Castle.ActiveRecord;
using Castle.ActiveRecord.Framework.Config;
using NHibernate;
using PortfolioAnalysis.Infrastructure;
using PortfolioAnalysis.Properties;
using PortofolioAnalysis.Common.DatabaseEntities;

namespace PortfolioAnalysis
{
    public static class Context
    {
        private static Workspace workspace;

        private static EventAggregator eventAggregator;

        public static Workspace Workspace
        {
            get { return workspace = workspace ?? new Workspace(); }
        }

        public static EventAggregator EventAggregator
        {
            get { return eventAggregator = eventAggregator ?? new EventAggregator(); }
        }

        public static ISessionFactory Database { get; private set; }

        public static void Initialize()
        {
            workspace = null;
            eventAggregator = null;
            
            if(Database != null)
            {
                Database.Dispose();
                Database = null;
            }

            DeleteDataBaseFileIfOlderThanThisAssembly();

            ConfigureDatabase();
        }

        private static void DeleteDataBaseFileIfOlderThanThisAssembly()
        {
            string dataBaseFileName = GetDataBaseFilePath();
            DateTime dataBaseTime = new FileInfo(dataBaseFileName).LastWriteTime;
            string assemblyLocation = typeof (Context).Assembly.Location;

            DateTime assemblyTime = new FileInfo(assemblyLocation).LastWriteTime;
            if (dataBaseTime < assemblyTime)
            {
                DeleteDataBaseFile();
            }
        }

        public static string GetDataBaseFilePath()
        {
            string path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),
                                       Settings.Default.DatabaseFile);
            return path;
        }

        private static string GetConnectionString()
        {
            return string.Format(@"Data Source={0}, File Mode=Exclusive", GetDataBaseFilePath());
        }

        private static void ConfigureDatabase()
        {
            var properties = new Hashtable();

            properties.Add("hibernate.connection.driver_class", "BondManager.Domain.ImageSqlServerCeDriver");
            properties.Add("hibernate.dialect", "NHibernate.Dialect.MsSqlCeDialect");
            properties.Add("hibernate.connection.provider", "NHibernate.Connection.DriverConnectionProvider");
            properties.Add("hibernate.connection.connection_string",
                           GetConnectionString());
#if DEBUG
            //properties.Add("hibernate.show_sql", "true");
#endif
            var config = new InPlaceConfigurationSource();

            config.Add(typeof (ActiveRecordBase), properties);

            Type securityType = typeof (Security);
            var types = new[] {securityType};

            ActiveRecordStarter.ResetInitializationFlag();
            ActiveRecordStarter.Initialize(config, types);

            bool isNewDatabase = EnsureDataBaseExists();

            Database = ActiveRecordMediator.GetSessionFactoryHolder().GetSessionFactory(securityType);

            if (isNewDatabase)
            {
                CreateTestData();
            }
        }

        public static void DeleteDataBaseFile()
        {
            string dataBaseFileName = GetDataBaseFilePath();
            if (File.Exists(dataBaseFileName))
            {
                File.Delete(dataBaseFileName);
            }
        }

        public static bool EnsureDataBaseExists()
        {
            string dataBaseFileName = GetDataBaseFilePath();

            if (!File.Exists(dataBaseFileName))
            {
                using (var engine = new SqlCeEngine(GetConnectionString()))
                {
                    engine.CreateDatabase();
                }

                ActiveRecordStarter.CreateSchema();

                return true;
            }
            return false;
        }

        private static void CreateTestData()
        {
            IEnumerable<Security> securities = DemoCsvImporter.Import();

            using (ISession session = Database.OpenSession())
            using (ITransaction transaction = session.BeginTransaction())
            {
                foreach (Security security in securities)
                {
                    session.Save(security);
                }
                transaction.Commit();
            }
        }

        private static void ExecuteInitSQLStatements()
        {
            //  ExecuteSQLStatements(Resources.InitBaseScript);
        }

        public static void ExecuteSqlStatements(string statements)
        {
            string[] sqls = statements.Split(new[] {'\n', '\r'}, StringSplitOptions.RemoveEmptyEntries);

            using (var connection = new SqlCeConnection(GetConnectionString()))
            {
                connection.Open();
                foreach (string sql in sqls)
                {
                    using (var command = new SqlCeCommand(sql, connection))
                    {
                        command.ExecuteNonQuery();
                    }
                }

                connection.Close();
            }
        }
    }
}