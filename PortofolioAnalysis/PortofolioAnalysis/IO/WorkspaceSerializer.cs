﻿using System.IO;
using Newtonsoft.Json;
using NHibernate;
using PortofolioAnalysis.Common.DatabaseEntities;
using PortofolioAnalysis.Common.Entities;

namespace PortfolioAnalysis.IO
{
    public class WorkspaceSerializer
    {
        public static Workspace Load(string fileName)
        {
            string json = File.ReadAllText(fileName);
            Workspace workspace = JsonConvert.DeserializeObject<Workspace>(json,
                                                                           new JsonSerializerSettings
                                                                               {
                                                                                   PreserveReferencesHandling =
                                                                                       PreserveReferencesHandling.
                                                                                       Objects
                                                                               });

            FixSecurities(workspace);

            return workspace;
        }

        private static void FixSecurities(Workspace w)
        {
            using (ISession session = Context.Database.OpenSession())
            using (ITransaction transaction = session.BeginTransaction())
            {
                foreach (PortfolioSecurity security in w.AllSecurities)
                {
                    security.IssueDate = security.IssueDate.Date;
                    security.FirstCouponDate = security.FirstCouponDate.Date;
                    security.MaturityDate = security.MaturityDate.Date;
                    security.ModifiedDate = security.ModifiedDate.Date;
                    if (security.ResetDate != null)
                    {
                        security.ResetDate = security.ResetDate.Value.Date;
                    }

                    Security dbSecurity = session.Get<Security>(security.Id);
                    if (dbSecurity != null)
                    {
                        security.DayConvention = dbSecurity.DayConvention;
                    }
                }

                transaction.Commit();
            }
        }

        public void Save(Workspace workspace, string fileName)
        {
            string json = JsonConvert.SerializeObject(workspace, Formatting.Indented,
                                                      new JsonSerializerSettings
                                                          {
                                                              PreserveReferencesHandling =
                                                                  PreserveReferencesHandling.Objects
                                                          });
            File.WriteAllText(fileName, json);
        }
    }
}