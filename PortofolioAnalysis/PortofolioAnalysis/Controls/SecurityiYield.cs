﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Linq;

namespace PortofolioAnalysis.Controls
{
    public partial class SecurityiYield : UserControl
    {
        private List<Scenario> scenarios;

        public SecurityiYield()
        {
            InitializeComponent();
            InitiGrid();
        }

        public bool AddScenario(Scenario scenario)
        {
            if (scenarios.Where(s=>s.Name == scenario.Name).FirstOrDefault() == null)
            {
                scenarios.Add(scenario);
                FillScenario(scenario);
                return true;
            }
            return false;
        }

        private void InitiGrid()
        {
            dataGridView1.Columns.Add(new DataGridViewTextBoxColumn());
            dataGridView1.Rows.Add(8);
            dataGridView1[0, 0].Value = "Price";
            dataGridView1[0, 1].Value = "Yield";
            dataGridView1[0, 2].Value = "WAL";
            dataGridView1[0, 3].Value = "Principal Writedown";
            dataGridView1[0, 4].Value = "Accum Int Shortfall";
            dataGridView1[0, 5].Value = "Accum Coup Cap Shortfall";
            dataGridView1[0, 6].Value = "Min Credit Support Pct";
            dataGridView1[0, 7].Value = "Mod Durn";
        }

        private void FillScenario(Scenario scenario)
        {
            if (!dataGridView1.Columns.Contains(scenario.Name))
                AddScenarioColumn(scenario.Name);

            dataGridView1[scenario.Name, 0].Value = scenario.Price;
            dataGridView1[scenario.Name, 1].Value = scenario.Yield;
            dataGridView1[scenario.Name, 2].Value = scenario.Wal;
            dataGridView1[scenario.Name, 3].Value = scenario.PrincipalWritedown;
            dataGridView1[scenario.Name, 4].Value = scenario.AccumIntShortfall;
            dataGridView1[scenario.Name, 5].Value = scenario.AccumCoupCapShortfall;
            dataGridView1[scenario.Name, 6].Value = scenario.MinCreditSupportPct;
            dataGridView1[scenario.Name, 7].Value = scenario.ModDurn;
        }

        private void AddScenarioColumn(string name)
        {
            DataGridViewTextBoxColumn columnTextBoxColumn = new DataGridViewTextBoxColumn();
            columnTextBoxColumn.HeaderText = name;
            columnTextBoxColumn.Name = name;
            columnTextBoxColumn.DefaultCellStyle.Format = "N4";
            columnTextBoxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            columnTextBoxColumn.Width = 100;
            dataGridView1.Columns.Add(columnTextBoxColumn);
        }

        public class Scenario
        {
            public string Name { get; set; }
            public decimal Price { get; set; }
            public decimal Yield { get; set; }
            public decimal Wal { get; set; }
            public decimal PrincipalWritedown { get; set; }
            public decimal AccumIntShortfall { get; set; }
            public decimal AccumCoupCapShortfall { get; set; }
            public decimal MinCreditSupportPct { get; set; }
            public decimal ModDurn { get; set; }
        }
    }
}
