﻿using System.Windows.Forms;

namespace PortfolioAnalysis.Controls.DataGridViewEx
{
    interface ISpannedCell
    {
        int ColumnSpan { get; }
        int RowSpan { get; }
        DataGridViewCell OwnerCell { get; }
    }
}