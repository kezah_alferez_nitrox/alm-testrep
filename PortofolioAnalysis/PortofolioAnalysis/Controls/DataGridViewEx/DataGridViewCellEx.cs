﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace PortfolioAnalysis.Controls.DataGridViewEx
{
    public class DataGridViewCellEx : DataGridViewTextBoxCell 
    {
        public DataGridViewAdvancedBorderStyle DataGridViewAdvancedCellBorderStyle { get; set; }

        public override DataGridViewAdvancedBorderStyle AdjustCellBorderStyle(DataGridViewAdvancedBorderStyle dataGridViewAdvancedBorderStyleInput, DataGridViewAdvancedBorderStyle dataGridViewAdvancedBorderStylePlaceholder, bool singleVerticalBorderAdded, bool singleHorizontalBorderAdded, bool isFirstDisplayedColumn, bool isFirstDisplayedRow)
        {
            return DataGridViewAdvancedCellBorderStyle;
        }
    }
}
