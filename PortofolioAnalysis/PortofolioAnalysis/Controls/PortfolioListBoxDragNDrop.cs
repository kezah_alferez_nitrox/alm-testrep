﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using PortofolioAnalysis.Common.Entities;

namespace PortfolioAnalysis.Controls
{
    public class PortfolioListBoxDragNDrop : ListBox
    {
        private int dragItemIndex = -1;
        private int dropIndex = -1;
        private int lastLine = -4;

        public PortfolioListBoxDragNDrop()
        {
            this.AllowDrop = true;
            this.SelectionMode = SelectionMode.One;
        }

        private void DrawVisualQue(Color color, ref Rectangle rectangle)
        {
            Graphics g = Graphics.FromHwnd(this.Handle);
            g.DrawLine(new Pen(color), rectangle.X, lastLine, rectangle.Width, lastLine);
        }

        protected override void OnMouseMove(System.Windows.Forms.MouseEventArgs e)
        {
            int index = this.IndexFromPoint(new Point(e.X, e.Y));
            if (MouseButtons == MouseButtons.Left
                && index != dragItemIndex
                && SelectedItem != null &&
                dragItemIndex > -1)
            {
                //this.SelectedIndex = dragItemIndex;

                dropIndex = -1;
                var portfolio = (Category)Items[dragItemIndex];
                DragDropEffects dde = DoDragDrop(portfolio, DragDropEffects.Copy | DragDropEffects.Move);
                if (DragDropEffects.Move == dde)
                {
                    if (dropIndex > -1 && dragItemIndex > dropIndex)
                        dragItemIndex++;

                    Context.Workspace.Portfolios.RemoveAt(dragItemIndex);
                    //this.Items.RemoveAt(dragItemIndex);
                }

                dragItemIndex = this.SelectedIndex;
            }
            else
                base.OnMouseMove(e);
        }

        protected override void OnMouseDown(System.Windows.Forms.MouseEventArgs e)
        {
            dragItemIndex = this.IndexFromPoint(new Point(e.X, e.Y));
        }

        protected override void OnDragDrop(System.Windows.Forms.DragEventArgs e)
        {
            if (e.Effect == DragDropEffects.Move || e.Effect == DragDropEffects.Copy)
            {
                Point point = this.PointToClient(new Point(e.X, e.Y));
                int index = this.IndexFromPoint(point);
                var portfolio = e.Data.GetData(typeof(Category));
                if (index > -1 && index < this.Items.Count)
                {
                    Context.Workspace.Portfolios.Insert(index, (Category)portfolio);
                    //Items.Insert(index, portfolio);
                    dropIndex = index;

                }
                else
                {

                    Context.Workspace.Portfolios.Add((Category)portfolio);
                    index = Context.Workspace.Portfolios.Count - 1;
                    //index = Items.Add(portfolio);

                }
                ((CurrencyManager)BindingContext[Context.Workspace.Portfolios]).Refresh();

                this.SelectedIndex = index;
            }
        }

        protected override void OnDragEnter(System.Windows.Forms.DragEventArgs e)
        {
            var portfolio = e.Data.GetDataPresent(typeof(Category));
            if (portfolio)
            {
                DragDropEffects dde = (Control.ModifierKeys & Keys.Control) != 0 ? DragDropEffects.Copy : DragDropEffects.Move;

                e.Effect = dde;
            }
            else
                e.Effect = DragDropEffects.None;

            lastLine = -1;
        }

        protected override void OnDragLeave(System.EventArgs e)
        {
            if (lastLine > -1)
            {
                Rectangle rect = this.GetItemRectangle(0);
                DrawVisualQue(Color.White, ref rect);
            }
        }

        protected override void OnDragOver(System.Windows.Forms.DragEventArgs e)
        {
            if (e.Effect != DragDropEffects.None)
            {
                Point point = this.PointToClient(new Point(e.X, e.Y));
                int index = this.IndexFromPoint(point);
                bool belowLastItem = false;
                if (index < 0 || index >= this.Items.Count)
                {
                    index = this.Items.Count - 1;
                    belowLastItem = true;
                }
                Rectangle rect = this.GetItemRectangle(index);
                if (lastLine > -1)
                    DrawVisualQue(Color.White, ref rect);
                lastLine = rect.Y + (belowLastItem ? rect.Height : 0);
                DrawVisualQue(Color.Black, ref rect);
                DragDropEffects dde = (Control.ModifierKeys & Keys.Control) != 0 ? DragDropEffects.Copy : DragDropEffects.Move;
                e.Effect = dde;

            }
        }

        public void RenameItem(TextBox renamePortfolioTextBox)
        {
            int itemSelected = SelectedIndex;
            Category portfolio = (Category)SelectedItem;

            Rectangle r = GetItemRectangle(itemSelected);

            renamePortfolioTextBox.Location = new System.Drawing.Point(r.X + 10, r.Y + 30);
            renamePortfolioTextBox.Size = new Size(r.Width, r.Height);
            renamePortfolioTextBox.Visible = true;
            renamePortfolioTextBox.Text = portfolio.Name;
            renamePortfolioTextBox.Focus();
            renamePortfolioTextBox.SelectAll();
        }
    }
}
