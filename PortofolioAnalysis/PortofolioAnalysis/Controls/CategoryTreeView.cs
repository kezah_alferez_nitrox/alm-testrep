using System;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using PortofolioAnalysis.Common.Entities;

namespace PortfolioAnalysis.Controls
{
    public class CategoryTreeView : TreeView
    {
        public TreeNode Foundnode;

        private bool dragDropIsOrdering = false;
        private TreeNode lastDraggedOverNode;
        private readonly Pen linePen;
        private readonly Pen rectanglePen;
        private bool dragCategory;
        private bool dragSecurity;

        public bool CanOrganize { get; set; }

        public Category Portfolio { get; set; }
        public Category CurrentCategory { get; set; }

        public CategoryTreeView()
        {
            this.AllowDrop = true;
            this.CanOrganize = true;

            this.linePen = new Pen(Color.Black);

            this.rectanglePen = new Pen(Color.Black);
            this.rectanglePen.DashStyle = DashStyle.Dash;
            this.AfterCheck += this.DataTreeView_AfterCheck;
        }

        private bool ignoreNodeCheckedChanged = false;

        void DataTreeView_AfterCheck(object sender, TreeViewEventArgs e)
        {
            if (ignoreNodeCheckedChanged) return;

            ignoreNodeCheckedChanged = true;

            if (e.Node.Checked)
                EnsureParentIsVisible(e.Node);
            else
                ApplyNodeCheckedToChild(e.Node);

            ignoreNodeCheckedChanged = false;
        }

        private static void ApplyNodeCheckedToChild(TreeNode treeNode)
        {
            foreach (TreeNode node in treeNode.Nodes)
            {
                node.Checked = treeNode.Checked;
                ApplyNodeCheckedToChild(node);
            }
        }

        private static void EnsureParentIsVisible(TreeNode node)
        {
            while (node.Parent != null)
            {
                node = node.Parent;
                if (!node.Checked)
                    node.Checked = true;
            }
        }

        public void LoadNodes()
        {
            Nodes.Clear();

            Category root = Portfolio;
            if (root == null) return;

            TreeNode node = new TreeNode(root.Name);
            node.Tag = root;
            AddChildNodes(node, root);
            Nodes.Add(node);

            node.ExpandAll();
            this.SelectedNode = node;

        }

        private static void AddChildNodes(TreeNode parentNode, Category parent)
        {
            foreach (Category category in parent.Categories)
            {
                TreeNode childNode = new TreeNode(category.Name);

                childNode.Tag = category;
                parentNode.Nodes.Add(childNode);
                AddChildNodes(childNode, category);
            }
        }

        public void RenameNode()
        {
            if (null != SelectedNode)
            {
                LabelEdit = true;
                SelectedNode.BeginEdit();
            }
        }

        public void AddNode()
        {
            if (null == SelectedNode) return;

            Category parent = (Category)SelectedNode.Tag;
            Debug.WriteLine("Add Node");
            Debug.WriteLine("Parent: " + parent.Name);

            Category category = new Category();
            category.Parent = parent;
            Debug.WriteLine("Category: " + category.Name);

            TreeNode childNode = new TreeNode(string.Empty);
            childNode.Tag = category;
            SelectedNode.Nodes.Add(childNode);
            SelectedNode = childNode;
            LabelEdit = true;
            childNode.BeginEdit();
        }

        public void DeleteNode()
        {
            if (null == SelectedNode) return;
            if (SelectedNode.Nodes.Count > 0) return;

            Category category = (Category)SelectedNode.Tag;

            category.Parent.Categories.Remove(category);

            Nodes.Remove(SelectedNode);
            //Context.EventAggregator.Publish(new PortfolioChangedEvent());
        }

        protected override void OnAfterLabelEdit(NodeLabelEditEventArgs e)
        {
            Category category = e.Node.Tag as Category;
            if (category == null) return;

            if (!string.IsNullOrEmpty(e.Label))
            {
                category.Name = e.Label;

                if (category.Parent != null && !category.Parent.Categories.Contains(category))
                {
                    category.Parent.Categories.Add(category);
                }

                e.Node.Text = e.Label;
                e.Node.EndEdit(false);

                SelectedNode = e.Node;
                OnAfterSelect(new TreeViewEventArgs(e.Node));
            }
            else
            {
                e.CancelEdit = true;

                if (category.Parent != null && !category.Parent.Categories.Contains(category))
                {
                    TreeNode parent = e.Node.Parent;
                    e.Node.EndEdit(true);
                    SelectedNode = parent;
                    parent.Nodes.Remove(e.Node);
                }
            }
            LabelEdit = false;
            base.OnAfterLabelEdit(e);
        }

        protected override void OnMouseClick(MouseEventArgs e)
        {
            base.OnMouseClick(e);

            if (e.Button != MouseButtons.Right) return;

            TreeNode node = GetNodeAt(e.Location);
            if (null != node)
            {
                SelectedNode = node;
            }
        }

        protected override void OnItemDrag(ItemDragEventArgs e)
        {
            if (!this.CanOrganize) return;

            this.DoDragDrop(e.Item, DragDropEffects.Move);
        }

        protected override void OnDragOver(DragEventArgs e)
        {
            e.Effect = DragDropEffects.None;
            dragCategory = e.Data.GetDataPresent(typeof(TreeNode));
            dragSecurity = e.Data.GetDataPresent(typeof(PortfolioSecurity));

            if (dragSecurity)
                DragOverSecurity(e);
            else if (dragCategory)
                DragOverCategory(e);
            else
            {
                this.SelectedNode = null;
                return;
            }
        }

        private void DragOverCategory(DragEventArgs e)
        {
            TreeNode srcNode = (TreeNode)e.Data.GetData(typeof(TreeNode));
            Point pt = PointToClient(new Point(e.X, e.Y));
            TreeNode destinationNode = this.GetNodeAt(pt);

            Category srcCategory = srcNode.Tag as Category;
            Category destCategory = destinationNode.Tag as Category;

            if (srcCategory == null || destCategory == null)
            {
                return;
            }

            if (this.lastDraggedOverNode != null && this.lastDraggedOverNode != destinationNode)
            {
                InvalidateNode(this.lastDraggedOverNode);
            }
            this.lastDraggedOverNode = destinationNode;

            Rectangle bounds = destinationNode.Bounds;
            bool overBottom = pt.Y + 4 >= bounds.Bottom;

            if (overBottom)
            {
                if (!this.dragDropIsOrdering)
                {
                    InvalidateNode(destinationNode);
                }
                using (Graphics g = Graphics.FromHwnd(this.Handle))
                {
                    g.DrawLine(this.linePen, bounds.Left, bounds.Bottom, bounds.Right, bounds.Bottom);
                }
            }
            else
            {
                if (this.dragDropIsOrdering)
                {
                    InvalidateNode(destinationNode);
                }
                using (Graphics g = Graphics.FromHwnd(this.Handle))
                {
                    g.DrawRectangle(this.rectanglePen, bounds.Left, bounds.Top, bounds.Width, bounds.Height);
                }
            }

            this.dragDropIsOrdering = overBottom;

            bool alowed = CanMoveCategory(srcCategory, destCategory, dragDropIsOrdering);

            e.Effect = alowed ? DragDropEffects.Move : DragDropEffects.None;
        }

        private void DragOverSecurity(DragEventArgs e)
        {
            Point pt = PointToClient(new Point(e.X, e.Y));
            TreeNode destinationNode = this.GetNodeAt(pt);
            if (destinationNode == null) return;

            if (this.lastDraggedOverNode != null && this.lastDraggedOverNode != destinationNode)
                InvalidateNode(this.lastDraggedOverNode);
            this.lastDraggedOverNode = destinationNode;

            Rectangle bounds = destinationNode.Bounds;
            bool overBottom = pt.Y + 4 >= bounds.Bottom;
            if (overBottom)
            {
                if (!this.dragDropIsOrdering)
                {
                    InvalidateNode(destinationNode);
                }
                using (Graphics g = Graphics.FromHwnd(this.Handle))
                {
                    g.DrawLine(this.linePen, bounds.Left, bounds.Bottom, bounds.Right, bounds.Bottom);
                }
            }
            else
            {
                if (this.dragDropIsOrdering)
                {
                    InvalidateNode(destinationNode);
                }
                using (Graphics g = Graphics.FromHwnd(this.Handle))
                {
                    g.DrawRectangle(this.rectanglePen, bounds.Left, bounds.Top, bounds.Width, bounds.Height);
                }
            }

            this.dragDropIsOrdering = overBottom;
            e.Effect = DragDropEffects.Move;
        }

        private static bool CanMoveCategory(Category srcCategory, Category destCategory, bool isOrdering)
        {
            if (isOrdering)
            {
                if (srcCategory == destCategory) return false;

                if (srcCategory.Parent == destCategory.Parent) return true;
                destCategory = destCategory.Parent;
            }

            if (destCategory == null) return false;

            if (srcCategory == destCategory) return false;

            if (IsParent(srcCategory, destCategory)) return false;

            return true;
        }

        private static bool IsParent(Category parent, Category child)
        {
            while (child.Parent != null)
            {
                if (parent == child.Parent) return true;
                child = child.Parent;
            }

            return false;
        }

        private void InvalidateNode(TreeNode node)
        {
            Rectangle rectangle = node.Bounds;
            rectangle.Inflate(1, 1);
            this.Invalidate(rectangle);
        }

        protected override void OnDragDrop(DragEventArgs e)
        {
            if (dragSecurity)
                DragDropSecurity(e);
            else if (dragCategory)
                DragDropCategory(e);
        }

        protected void DragDropCategory(DragEventArgs e)
        {
            Point pt = PointToClient(new Point(e.X, e.Y));
            TreeNode destinationNode = this.GetNodeAt(pt);

            if (this.lastDraggedOverNode != null)
            {
                InvalidateNode(this.lastDraggedOverNode);
                this.lastDraggedOverNode = null;
            }

            // Ensure that the list item index is contained in the data.
            if (e.Data.GetDataPresent(typeof(TreeNode)))
            {
                TreeNode srcNode = (TreeNode)e.Data.GetData(typeof(TreeNode));
                if (srcNode == destinationNode) return;
                if (e.Effect == DragDropEffects.Move)
                {
                    Category srcCategory = srcNode.Tag as Category;
                    Category destCategory = destinationNode.Tag as Category;
                    if (srcCategory == null || destCategory == null) return;

                    if (dragDropIsOrdering)
                    {
                        if (destinationNode.Parent == null) return;

                        srcNode.Remove();
                        destinationNode.Parent.Nodes.Insert(destinationNode.Index + 1, srcNode);

                        if (destCategory.Parent == srcCategory.Parent)
                        {
                            destCategory.Parent.Categories.Remove(srcCategory);
                            destCategory.Parent.Categories.Insert(destCategory.Parent.Categories.IndexOf(destCategory) + 1, srcCategory);
                        }
                        else
                        {
                            srcCategory.Parent = destCategory.Parent;
                            destCategory.Parent.Categories.Insert(destCategory.Parent.Categories.IndexOf(destCategory) + 1, srcCategory);
                        }

                        for (int i = 0; i < destinationNode.Parent.Nodes.Count; i++)
                        {
                            TreeNode node = destinationNode.Parent.Nodes[i];
                            Category category = node.Tag as Category;
                            if (category == null) continue;

                            category.Position = node.Index;
                        }
                    }
                    else
                    {
                        Category category = (Category)srcNode.Tag;
                        category.Parent.Categories.Remove(category);

                        srcNode.Remove();
                        destinationNode.Nodes.Add(srcNode);
                        destinationNode.Expand();
                        srcCategory.Parent = destCategory;
                        destCategory.Categories.Add(srcCategory);
                    }
                }
            }
        }

        protected void DragDropSecurity(DragEventArgs e)
        {
            PortfolioSecurity portfolioSecurity = (PortfolioSecurity)e.Data.GetData(typeof(PortfolioSecurity));
            Point pt = PointToClient(new Point(e.X, e.Y));
            TreeNode destinationNode = this.GetNodeAt(pt);
            if (destinationNode == null) return;

            var destCategory = destinationNode.Tag as Category;
            if (portfolioSecurity == null || destCategory == null) return;

            if (destCategory.Parent != null && !destCategory.Securities.Contains(portfolioSecurity))
            {
                if (CurrentCategory.RemoveSecurity(portfolioSecurity))
                {
                    destCategory.Securities.Add(portfolioSecurity);
                }
            }
        }

        public void ClearAll()
        {
            Nodes.Clear();
        }
    }
}
