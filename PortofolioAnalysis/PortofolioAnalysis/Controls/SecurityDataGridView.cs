﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using ALMSCommon;
using PortofolioAnalysis.Common.Entities;

namespace PortfolioAnalysis.Controls
{
    public class SecurityDataGridView : DataGridView
    {
        protected override void OnMouseMove(System.Windows.Forms.MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                var info = HitTest(e.X, e.Y);
                if (info.RowIndex >= 0 && Rows[info.RowIndex].Selected)
                {
                    var view = (PortfolioSecurity)Rows[info.RowIndex].DataBoundItem;
                    if (view != null)
                    {
                        DoDragDrop(view, DragDropEffects.Move);
                    }
                }
                else
                {
                    base.OnMouseMove(e);
                }
            }
        }

        #region Enable Ctrl + C to Excel
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            switch (keyData)
            {
                case Keys.Control | Keys.C:
                    PerformPaste();
                    return true;
                case Keys.Control | Keys.Insert:
                    PerformPaste();
                    return true;
                default:
                    return base.ProcessCmdKey(ref msg, keyData);
            }
        }

        private void PerformPaste()
        {
            if (this.GetCellCount(DataGridViewElementStates.Selected) > 0)
            {
                try
                {
                    // Add the selection to the clipboard.
                    DataObject clipboardContent = this.GetClipboardContent();
                    if (clipboardContent != null)
                    {
                        Clipboard.SetDataObject(clipboardContent);
                    }
                }
                catch (Exception ex)
                {
                    Logger.Exception("PerformPaste", ex);
                }
            }

        }
        #endregion Enable Ctrl + C to Excel
    }
}
