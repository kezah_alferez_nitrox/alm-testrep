using System.Drawing;

namespace PortfolioAnalysis
{
    public class Constants
    {
        public const string ApplicationName = "Portfolio Analysis";

        public class Fonts
        {
            public static readonly Font MainFont = new Font("Arial", 7f);
        }
    }
}