using System;
using System.Collections.Generic;
using System.Globalization;
using PortfolioAnalysis.Properties;
using PortofolioAnalysis.Common;
using PortofolioAnalysis.Common.DatabaseEntities;

namespace PortfolioAnalysis
{
    public class DemoCsvImporter
    {
        public static IEnumerable<Security> Import()
        {
            string[] lines = Resources.database.Split(new[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 3; i < lines.Length; i++)
            {
                string[] fields = lines[i].Split(';');

                if (fields[0].Contains("#N/A"))
                {
                    continue;
                }

                CouponType couponType = GetCouponType(fields[Helpers.ExcelColumnToIndex("K")]);

                if (couponType == CouponType.Variable && fields[Helpers.ExcelColumnToIndex("M")] != "FIX-TO-FLOAT BONDS")
                {
                    continue;
                }

                Security security = new Security
                                        {
                                            IdCussip = fields[Helpers.ExcelColumnToIndex("A")],
                                            ISIN = fields[Helpers.ExcelColumnToIndex("AG")],
                                            Description = fields[Helpers.ExcelColumnToIndex("F")],
                                            AmountIssued =
                                                Helpers.TryConvertToDouble(fields[Helpers.ExcelColumnToIndex("S")]).
                                                GetValueOrDefault(),
                                            AmountOutstanding =
                                                Helpers.TryConvertToDouble(fields[Helpers.ExcelColumnToIndex("T")]).
                                                GetValueOrDefault(),
                                            Coupon =
                                                Helpers.TryConvertToDouble(fields[Helpers.ExcelColumnToIndex("C")]).
                                                GetValueOrDefault(),
                                            CouponFrequency =
                                                Helpers.TryConvertToInt(fields[Helpers.ExcelColumnToIndex("J")]).
                                                GetValueOrDefault(),
                                            CouponType = couponType,
                                            FirstCouponDate =
                                                ParseDate(fields[Helpers.ExcelColumnToIndex("AC")]).GetValueOrDefault(
                                                    PortofolioAnalysis.Common.Constants.MinSqlDate),
                                            IssueDate =
                                                ParseDate(fields[Helpers.ExcelColumnToIndex("AE")]).GetValueOrDefault(
                                                    PortofolioAnalysis.Common.Constants.MinSqlDate),
                                            MaturityDate =
                                                ParseDate(fields[Helpers.ExcelColumnToIndex("D")]).GetValueOrDefault(
                                                    PortofolioAnalysis.Common.Constants.MinSqlDate),
                                            CollateralType = fields[Helpers.ExcelColumnToIndex("R")],
                                            IndustrySector = fields[Helpers.ExcelColumnToIndex("H")],
                                            RatingFitch =
                                                RatingHelper.ParseFitch((fields[Helpers.ExcelColumnToIndex("FN")])),
                                            RatingMoodys =
                                                RatingHelper.ParseMoodys(fields[Helpers.ExcelColumnToIndex("FM")]),
                                            RatingSandP =
                                                RatingHelper.ParseSandP(fields[Helpers.ExcelColumnToIndex("FO")]),
                                            Factor =
                                                Helpers.TryConvertToDouble(fields[Helpers.ExcelColumnToIndex("DL")]).
                                                GetValueOrDefault(),
                                            DayConvention = ParseDayConvention(fields[Helpers.ExcelColumnToIndex("FS")]),
                                        };

                if (security.CouponType == CouponType.Floating || security.CouponType == CouponType.Variable)
                {
                    security.ResetIndex = ParseString(fields[Helpers.ExcelColumnToIndex("AW")]);
                    if (security.ResetIndex != null)
                    {
                        security.ResetIndex = security.ResetIndex.Replace("  ", " ").Replace(" ", "_");
                    }
                    security.Spread =
                        Helpers.TryConvertToDouble(fields[Helpers.ExcelColumnToIndex("BN")]).GetValueOrDefault();
                    security.FloatingFrequency = Helpers.TryConvertToInt(fields[Helpers.ExcelColumnToIndex("AU")]);
                    security.ResetDate = ParseDate(fields[Helpers.ExcelColumnToIndex("FQ")]);
                }

                yield return security;
            }
        }

        private static readonly IDictionary<string, DayConvention> DayConventions =
            new Dictionary<string, DayConvention>
                {
                    {"ACT/ACT", DayConvention.ActAct},
                    {"ACT/ACT NON-EOM", DayConvention.ActAct},
                    {"ACT/360", DayConvention.Act360},
                    {"ACT/360 NON-EOM", DayConvention.Act360},
                    {"ACT/365", DayConvention.Act365},
                    {"ACT/365 NON-EOM", DayConvention.Act365},
                    {"30/360", DayConvention.US30360},
                    {"30/360 NON-EOM", DayConvention.US30360},
                    {"ISMA-30/360", DayConvention.EU30360},
                    {"ISMA-30/360 NON-EOM", DayConvention.EU30360},
                };

        private static DayConvention ParseDayConvention(string str)
        {
            DayConvention dayConvention;
            if (!DayConventions.TryGetValue(str, out dayConvention))
            {
                dayConvention = DayConvention.Act360;
            }

            return dayConvention;
        }

        private static string ParseString(string text)
        {
            if (string.IsNullOrEmpty(text)) return text;

            if (text.StartsWith("#N/A")) return null;

            return text;
        }

        private static CouponType GetCouponType(string value)
        {
            if (string.Equals("FIXED", value, StringComparison.InvariantCultureIgnoreCase))
            {
                return CouponType.Fixed;
            }

            if (string.Equals("FLOATING", value, StringComparison.InvariantCultureIgnoreCase))
            {
                return CouponType.Floating;
            }

            return CouponType.Variable;
        }

        private static DateTime? ParseDate(string str)
        {
            DateTime result;

            if (!DateTime.TryParseExact(str, "dd/MM/yy", null, DateTimeStyles.AllowWhiteSpaces, out result))
            {
                if (!DateTime.TryParseExact(str, "yyyyMMdd", null, DateTimeStyles.AllowWhiteSpaces, out result))
                {
                    if (!DateTime.TryParse(str, out result))
                    {
                        return null;
                    }
                }
            }

            if (result.Year < 1970) result = result.AddYears(100);

            return result;
        }
    }
}