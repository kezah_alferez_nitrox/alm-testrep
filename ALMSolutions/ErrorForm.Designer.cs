

using ALMSolutions.Resources.Language;

namespace ALMSolutions
{
    partial class ErrorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.errorTextBox = new System.Windows.Forms.TextBox();
            this.detailsButton = new System.Windows.Forms.Button();
            this.closeButton = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.sendReportCheckBox = new System.Windows.Forms.CheckBox();
            this.clearTemporaryDataCheckBox = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.trySaveCheckBox = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(77, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(168, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "An unexpected error has occurred";
            // 
            // errorTextBox
            // 
            this.errorTextBox.Location = new System.Drawing.Point(12, 145);
            this.errorTextBox.Multiline = true;
            this.errorTextBox.Name = "errorTextBox";
            this.errorTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.errorTextBox.Size = new System.Drawing.Size(291, 202);
            this.errorTextBox.TabIndex = 2;
            // 
            // detailsButton
            // 
            this.detailsButton.Location = new System.Drawing.Point(12, 104);
            this.detailsButton.Name = "detailsButton";
            this.detailsButton.Size = new System.Drawing.Size(107, 23);
            this.detailsButton.TabIndex = 3;
            this.detailsButton.Text = global::ALMSolutions.Resources.Language.Labels.ErrorForm_InitializeComponent_Details;
            this.detailsButton.UseVisualStyleBackColor = true;
            this.detailsButton.Click += new System.EventHandler(this.detailsButton_Click);
            // 
            // closeButton
            // 
            this.closeButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.closeButton.Location = new System.Drawing.Point(125, 104);
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size(175, 23);
            this.closeButton.TabIndex = 1;
            this.closeButton.Text = global::ALMSolutions.Resources.Language.Labels.ErrorForm_InitializeComponent_Close_and_restart_the_application;
            this.closeButton.UseVisualStyleBackColor = true;
            this.closeButton.Click += new System.EventHandler(this.closeButton_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::ALMSolutions.Properties.Resources.warning_64;
            this.pictureBox1.Location = new System.Drawing.Point(7, 9);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(64, 64);
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            // 
            // sendReportCheckBox
            // 
            this.sendReportCheckBox.AutoSize = true;
            this.sendReportCheckBox.Checked = true;
            this.sendReportCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.sendReportCheckBox.Location = new System.Drawing.Point(80, 35);
            this.sendReportCheckBox.Name = "sendReportCheckBox";
            this.sendReportCheckBox.Size = new System.Drawing.Size(149, 17);
            this.sendReportCheckBox.TabIndex = 6;
            this.sendReportCheckBox.Text = global::ALMSolutions.Resources.Language.Labels.ErrorForm_InitializeComponent_Send_error_report_by_e_mail;
            this.sendReportCheckBox.UseVisualStyleBackColor = true;
            // 
            // clearTemporaryDataCheckBox
            // 
            this.clearTemporaryDataCheckBox.AutoSize = true;
            this.clearTemporaryDataCheckBox.Checked = true;
            this.clearTemporaryDataCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.clearTemporaryDataCheckBox.Location = new System.Drawing.Point(80, 58);
            this.clearTemporaryDataCheckBox.Name = "clearTemporaryDataCheckBox";
            this.clearTemporaryDataCheckBox.Size = new System.Drawing.Size(123, 17);
            this.clearTemporaryDataCheckBox.TabIndex = 7;
            this.clearTemporaryDataCheckBox.Text = global::ALMSolutions.Resources.Language.Labels.ErrorForm_InitializeComponent_Clear_temporary_data;
            this.clearTemporaryDataCheckBox.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(77, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 13);
            this.label2.TabIndex = 1;
            // 
            // trySaveCheckBox
            // 
            this.trySaveCheckBox.AutoSize = true;
            this.trySaveCheckBox.Checked = true;
            this.trySaveCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.trySaveCheckBox.Location = new System.Drawing.Point(80, 81);
            this.trySaveCheckBox.Name = "trySaveCheckBox";
            this.trySaveCheckBox.Size = new System.Drawing.Size(188, 17);
            this.trySaveCheckBox.TabIndex = 8;
            this.trySaveCheckBox.Text = "Try to save the current workspace";
            this.trySaveCheckBox.UseVisualStyleBackColor = true;
            // 
            // ErrorForm
            // 
            this.AcceptButton = this.closeButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.closeButton;
            this.ClientSize = new System.Drawing.Size(315, 138);
            this.ControlBox = false;
            this.Controls.Add(this.trySaveCheckBox);
            this.Controls.Add(this.clearTemporaryDataCheckBox);
            this.Controls.Add(this.sendReportCheckBox);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.closeButton);
            this.Controls.Add(this.detailsButton);
            this.Controls.Add(this.errorTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "ErrorForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Error";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox errorTextBox;
        private System.Windows.Forms.Button detailsButton;
        private System.Windows.Forms.Button closeButton;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.CheckBox sendReportCheckBox;
        private System.Windows.Forms.CheckBox clearTemporaryDataCheckBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox trySaveCheckBox;
    }
}