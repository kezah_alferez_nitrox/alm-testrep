﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Threading;
using System.Windows.Forms;
using ALMSCommon;
using ALMSolutions.Resources.Language;
using BondManager;
using BondManager.Calculations;
using log4net.Config;

namespace ALMSolutions
{
    internal static class Program
    {
        private static volatile Mutex appStartMutex;
        private static string[] args;

        /// <summary>
        /// Point d'entrée principal de l'application.
        /// </summary>
        [STAThread]
        private static void Main(string[] args)
        {
#if DEBUG
            //debug BETA version
            //args = new string[1];
            //args[0] = "beta";
#endif
            Program.args = args;

            XmlConfigurator.Configure();

            Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
//#if !DEBUG
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
            Application.ThreadException += Application_ThreadException;            
//#endif

#if !DEBUG
            if(AnotherInstanceRunning(args.Length > 0 && args[0] == "beta"))
            {
                return;
            }
#endif

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            MainForm mf = new MainForm(args);

            if (args.Length > 1)
            {
                mf.AutoLaunch = args[1];
            }
            Application.Run(mf);

            GC.KeepAlive(appStartMutex);
        }

        private static bool AnotherInstanceRunning(bool isBeta)
        {
            bool isOwnedHere;
            appStartMutex = new Mutex(
                true,
                Application.ProductName + (isBeta ? " beta" : ""),
                out isOwnedHere
            );

            return !isOwnedHere;
        }

//#if !DEBUG
        private static void Application_ThreadException(object sender, ThreadExceptionEventArgs e)
        {
            ManageUnexpectedException(e.Exception);
        }


        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            ManageUnexpectedException((Exception) e.ExceptionObject);
        }

        private static void ManageUnexpectedException(Exception exception)
        {
            Cursor.Current = Cursors.Default;

            Logger.Exception("ThreadException", exception);

            MainForm.SetForcedCloseFlag();

            ErrorForm errorForm = new ErrorForm(exception);
            errorForm.ShowDialog();

            try
            {
                if (errorForm.TrySave)
                    MainForm.TrySaveForm();

                if (errorForm.ClearTemporaryData)
                    MainForm.ClearTemporaryData();

                if (errorForm.SendReport)
                {
                    try
                    {
                        string subject = Labels.Program_Application_ThreadException_ALM_Solutions_Exception_v + typeof (Program).Assembly.GetName().Version;
                        Util.SendMail("info@alm-vision.com", subject, exception.ToString());
                    }
                    catch (Exception ex)
                    {
                        Logger.Exception("Impossible d'envoyer un mail pour reporter l'erreur", ex);
                    }
                }
            }
            finally
            {
                Application.Restart();
            }
        }

        //#endif
    }
}