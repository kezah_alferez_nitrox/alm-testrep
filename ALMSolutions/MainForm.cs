using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Resources;
using System.Threading;
using System.Web;
using System.Windows.Forms;
using ALMSCommon;
using ALMSCommon.Forms;
using ALMSolutions.Licence;
using ALMSolutions.Properties;
using ALMSolutions.Resources.Language;
using BankALM.Infrastructure.Data;
//using Sensitivity_Analysis_Calculator.Sensitivity;
// using AssetModelingModule.Views.BalanceSheet;

namespace ALMSolutions
{
    public partial class MainForm : Form
    {
        public static string Version { get; set; }

        private readonly LicenceValidator licenceValidator = new LicenceValidator();
        private string[] args;
        private string autoLaunch = "";
        private bool isBeta = false;     // This Parameter is Set to true at launch if the Argument "beta" is present.
        private static IModuleMainForm moduleMainForm;
        private static string localFileName;
        private static string MainFormTitleBase;
        public MainForm(string[] args)
        {
            //if (!string.IsNullOrEmpty(Settings.Default["Language"] as string))
            //    Thread.CurrentThread.CurrentUICulture = new CultureInfo((string)Settings.Default["Language"]);
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");
            
            this.args = args;
            this.InitializeComponent();

            //Thread.CurrentThread.CurrentUICulture = System.Threading.Thread.CurrentThread.CurrentCulture;
            // ResourceManager rm = new ResourceManager("ALMSLauncher.Resources.Language.Labels", Assembly.GetExecutingAssembly());
            isBeta = args.Length > 0 && args[0] == "beta";

            MainFormTitleBase = Labels.MainForm_MainForm_ALM_Solutions + (isBeta ? " beta" : "");

            SetMainFormTitle();
        }

        private void SetMainFormTitle(bool loading=false)
        {
            this.Text = MainFormTitleBase;

            if (AlmConfiguration.GetModuleName(AlmConfiguration.CurrentModule).Length>0)
            {
                this.Text += " - " +
                        AlmConfiguration.GetModuleName(AlmConfiguration.CurrentModule);    
            }
            if (loading)
                this.Text += "(loading)";
            
        }

        public string AutoLaunch
        {
            get { return this.autoLaunch; }
            set { this.autoLaunch = value; }
        }

        private void LoadModuleByArgs()
        {
            if (this.args != null && this.args.Length > 0)
            {
                string fileName = this.args[0];
                if (File.Exists(fileName))
                {
                    string extension = Path.GetExtension(fileName);
                    switch (extension)
                    {
                        case ".alw":
                            this.LoadBondManager(); 
                            break;
                        case ".alm":
                            this.LoadBondManager();
                            break;
                        case ".sct":
                            this.LoadBondManager();
                            break;
                        //case ".amw":
                        //    this.LoadAssetModeling();
                        //    break;
                        //case ".amm":
                        //    this.LoadAssetModeling();
                        //    break;
                        //case ".ams":
                        //    this.LoadAssetModeling();
                        //    break;
                        //case ".pfm":
                        //    this.LoadPensionFund();
                        //    break;
                        //case ".pfd":
                        //    this.LoadPensionFund();
                        //    break;
                        //case ".sam":
                        //    this.LoadSensitivityAnalysis();
                        //    break;
                        //case ".sad":
                        //    this.LoadSensitivityAnalysis();
                        //    break;
                    }
                }
            }
        }

        private T GetActiveFormOrLoadIt<T>() where T : Form, new()
        {
            T form = null;
            foreach (Form childForm in this.MdiChildren)
            {
                if (childForm.GetType() != typeof(T)) continue;
                form = (T)childForm;
            }

            if (null == form)
            {
                form = new T();
                this.SetMdiChildProperties(form);
            }

            return form;
        }

        private void SetMdiChildProperties<T>(T form) where T : Form, new()
        {
            form.MdiParent = this;
            form.ShowInTaskbar = false;
            if (form.MainMenuStrip != null)
            {
                form.MainMenuStrip.Visible = false;
            }
            form.ControlBox = false;
            form.WindowState = FormWindowState.Maximized;
            form.FormClosed += this.FormFormClosed;
        }

        private Form ShowForm<T>() where T : Form, new()
        {
            this.Cursor = Cursors.WaitCursor;

            try
            {
                // Fermer la fenetre si une autre fenetre deja ouverte
                if (null != this.ActiveMdiChild && this.ActiveMdiChild.GetType() != typeof(T))
                {
                    this.ActiveMdiChild.Close();
                }
                if (null == this.ActiveMdiChild)
                {
                    Form form = this.GetActiveFormOrLoadIt<T>();
                    var module = form as IModuleMainForm;
                    if (module != null)
                    {
                        module.LicenceInfo = licenceValidator.LicenceInfo;
                        module.Args = this.args;
                        //this.args = null;
                    }

                    if (!form.Visible) form.Show();

                    modulesToolStripMenuItem.Visible = false;

                    moduleMainForm = form as IModuleMainForm;

                    return form;
                }
                else
                {
                    return this.ActiveMdiChild;
                }
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void FormFormClosed(object sender, FormClosedEventArgs e)
        {
            Form closedForm = ((Form)sender);
            closedForm.MdiParent = null;
            closedForm.FormClosed -= this.FormFormClosed;

            modulesToolStripMenuItem.Visible = true;

            if (sender is BondManager.Views.MainForm)
            {
                if (((BondManager.Views.MainForm)sender).Restart)
                {
                    modulesToolStripMenuItem.Visible = false;
                    var newBalanceSheet = this.ShowForm<BondManager.Views.MainForm>();//new BondManager.Views.MainForm(this.args);
                    //this.SetMdiChildProperties(newBalanceSheet);
                    newBalanceSheet.Show();
                }
            }
        }

        private void MainFormLoad(object sender, EventArgs e)
        {
            this.SetVersion(typeof(MainForm));
            if(isBeta)
                this.Icon = new System.Drawing.Icon("almsBeta.ico");
            this.LoadLanguages();

            bool licenceIsValid = this.ValidateLicence();
            if (!licenceIsValid)
            {
                Application.Exit();
                return;
            }

            if (isBeta)
            {
                if (this.SilentCheckForUpdate(false)) return;
            }

            LoadForm.Instance.WorkFinished += this.Instance_WorkFinished;

            this.LoadModuleByArgs();

            switch (this.autoLaunch)
            {
                case "BalanceSheet":
                    this.BalanceSheetToolStripMenuItemClick(sender, e);
                    return;
                case "LifeInsuranceForm":
                    this.BalanceSheetToolStripMenuItemClick(sender, e);
                    return;
            }

        }

        private void LoadLanguages()
        {
            string[] languages = new string[] { "en-US", "fr-FR" };
            foreach (string language in languages)
                this.CreateLanguageMenu(language);
        }

        private void CreateLanguageMenu(string language)
        {
            CultureInfo culture = new CultureInfo(language);
            ToolStripMenuItem menu = new ToolStripMenuItem(culture.NativeName);
            menu.Click += ChangeLanguange;
            menuLanguage.DropDownItems.Add(menu);
            menu.Checked = Thread.CurrentThread.CurrentUICulture.Name == language;
            menu.Tag = language;
        }

        private void ChangeLanguange(object sender, EventArgs e)
        {
            string language = ((ToolStripMenuItem)sender).Tag as string;
            if (string.IsNullOrEmpty(language)) return;

            foreach (ToolStripMenuItem menu in menuLanguage.DropDownItems.OfType<ToolStripMenuItem>())
                menu.Checked = menu == sender;

            CultureInfo culture = new CultureInfo(language);
            Thread.CurrentThread.CurrentUICulture = culture;
            Settings.Default["Language"] = language;
            Settings.Default.Save();

            MessageBox.Show(Labels.MessageBoxChangeLanguageConfirmation, Labels.MessageBoxChangeLanguageTitle, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        void Instance_WorkFinished(object sender, LoadForm.WorkFinishedEventArgs e)
        {
            toolStripStatusLabel.Text = "Last operation time: " + e.Duration.ToString();
        }

        private void FillRecentFileList()
        {
            FillRecentFileListForModule(AlmConfiguration.Module.BankALM, bankAlmToolStripMenuItem);
            FillRecentFileListForModule(AlmConfiguration.Module.LifeInsurance, toolStripMenuItem7);
            //FillRecentFileListForModule(AlmConfiguration.Module.PortfolioAnalysis, portfolioAnalysisToolStripMenuItem);
        }

        private void FillRecentFileListForModule(AlmConfiguration.Module module, ToolStripMenuItem menuItem)
        {
            FileHistory fileHistory = new FileHistory(module);
            string[] files = fileHistory.GetFiles();

            menuItem.DropDownItems.Clear();
            foreach (string file in files)
            {
                ToolStripMenuItem toolStripMenuItem = new ToolStripMenuItem();
                toolStripMenuItem.Text = file;
                string fileCopy = file;
                toolStripMenuItem.Click += (o, e) => this.OpenModule(module, fileCopy);
                menuItem.DropDownItems.Add(toolStripMenuItem);
            }
        }

        private void OpenModule(AlmConfiguration.Module module, string file)
        {
            if (!this.IsModuleLicenced(module)) return;

            Form form = null;

            if (this.ActiveMdiChild is BondManager.Views.MainForm &&
                BondManager.Views.MainForm.ModifiedValues &&
                MessageBox.Show(BondManager.Properties.Resources.NewFilePrompt, "",
                MessageBoxButtons.OKCancel, MessageBoxIcon.Question) != DialogResult.OK)
                return;

            AlmConfiguration.CurrentModule = module;
            SetMainFormTitle();

            switch (module)
            {
                case AlmConfiguration.Module.BankALM:
                    form = this.ShowForm<BondManager.Views.MainForm>();
                    break;
                case AlmConfiguration.Module.LifeInsurance:
                    form = this.ShowForm<InsuranceBondManager.Views.MainForm>();
                    break;
                //case AlmConfiguration.Module.PensionFund:
                //    form = this.ShowForm<PensionFund.Views.MainForm>();
                //    break;
                //case AlmConfiguration.Module.SensitivityAnalysis:
                //    form = this.ShowForm<SensitivityForm>();
                //    break;
                //case AlmConfiguration.Module.PortfolioAnalysis:
                //    form = this.ShowForm<PortfolioAnalysis.Views.MainForm>();
                //    break;
                //case AlmConfiguration.Module.AssetModeling:
                //    // form = this.ShowForm<BalanceSheet>();
                //    break;
                default:
                    throw new ArgumentOutOfRangeException("module");
            }

            form.FormClosed += new FormClosedEventHandler(ChildClosed);

            if (file == null) return;

            moduleMainForm = form as IModuleMainForm;
            if (moduleMainForm == null) return;

            moduleMainForm.OpenFile(file);
        }


        private void SetVersion(Type type)
        {
            Version = type.Assembly.GetName().Version + (Environment.Is64BitProcess ? " x64" : " x86") + (isBeta ? " beta" : "");
            this.versionToolStripStatusLabel.Text = "v" + Version;
        }

        private void BalanceSheetToolStripMenuItemClick(object sender, EventArgs e)
        {
            this.bankAlmToolStripMenuItem.HideDropDown();
            this.bankAlmToolStripMenuItem.GetCurrentParent().Hide();
            this.LoadBondManager();
        }

        private void LoadModule(AlmConfiguration.Module module)
        {
            if (this.IsModuleLicenced(module))
            {
                AlmConfiguration.CurrentModule = module;
                SetMainFormTitle();
                Form child = null;
                if (module == AlmConfiguration.Module.LifeInsurance)
                    child = this.ShowForm<InsuranceBondManager.Views.MainForm>();
                else
                    child = this.ShowForm<BondManager.Views.MainForm>();
                child.FormClosed += new FormClosedEventHandler(ChildClosed);
                
            }
        }

        private void ChildClosed(object sender, FormClosedEventArgs formClosedEventArgs)
        {
            SetMainFormTitle();
        }


        private void LoadBondManager()
        {
            LoadModule(AlmConfiguration.Module.BankALM);
        }

        private void LoadPensionFundALM()
            {
            LoadModule(AlmConfiguration.Module.PensionFund);
            }


        private void LoadInsuranceALM()
        {
            LoadModule(AlmConfiguration.Module.LifeInsurance);
        }

        private void LoadTreasuryManagement()
        {
            LoadModule(AlmConfiguration.Module.TreasuryManagement);
        }


        private bool IsModuleLicenced(AlmConfiguration.Module module)
        {
#if NO_LICENCE
            return true;
#else
            bool isModuleLicenced = this.licenceValidator.IsModuleLicenced((int)module);

            if (!isModuleLicenced)
            {
                Dialogs.Error(Labels.MainForm_IsModuleLicenced_Licence,
                              "Your licence does not include " + AlmConfiguration.GetModuleName(module) + " module.\n" +
                              Labels.MainForm_IsModuleLicenced_Please_contact_your_vendor_for_a_licence_upgrade_);
            }

            return isModuleLicenced;
#endif
        }




        private void ExitSogalmsToolStripMenuItemClick(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private bool ValidateLicence()
        {
            try
            {
                if (this.licenceValidator.Validate())
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                Logger.Exception("ValidateLicence", ex);
            }

            return false;
        }

        private void LicenceInformationToolStripMenuItemClick(object sender, EventArgs e)
        {
#if NO_LICENCE
            return;
#else
            try
            {
                this.licenceValidator.Update();
            }
            catch (Exception ex)
            {
                Logger.Exception("LicenceInformation", ex);
            }
#endif
        }

        private void ClearAllTemporaryDataToolStripMenuItemClick(object sender, EventArgs e)
        {
            if (
                MessageBox.Show(
                    Labels.MainForm_ClearAllTemporaryDataToolStripMenuItemClick_Really_delete_all_temporary_data_from_all_modules___You_will_lose_all_unsaved_changes__,
                    Labels.MainForm_ClearAllTemporaryDataToolStripMenuItemClick_Clear_All_Temporary_Data, MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK)
            {
                foreach (Form mdiChild in this.MdiChildren)
                {
                    mdiChild.Close();
                }

                ClearTemporaryData();
            }
        }

        public static void ClearTemporaryData()
        {
            SessionManager.DeleteDataBase();
        }


        private void CheckForUpdatesToolStripMenuItemClick(object sender, EventArgs e)
        {
            this.SilentCheckForUpdate(true);

        }

        public bool SilentCheckForUpdate(bool openNavigator)
        {
            bool licenseIsValid = this.licenceValidator != null && LicenceValidator.IsLicenceValid(this.licenceValidator.LicenceInfo);

            string key = DateTime.Today.ToString("yyyyMMdd");
            key = Crypt.EncryptStringAES(key, key);

            string version = typeof(MainForm).Assembly.GetName().Version.ToString();

            string lk = this.licenceValidator == null ? "" : this.licenceValidator.LicenceInfo.LicenceNumber;

            key = HttpUtility.UrlEncode(key);
            version = HttpUtility.UrlEncode(version);
            lk = HttpUtility.UrlEncode(lk);

            string url = Settings.Default.UpdateUrl + "?k=" + key + "&v=" + version + "&lk=" + lk;
            if (!licenseIsValid) url += "&l=true";
            if (this.isBeta)
            {
                url += "&b=true";
            }

            if (openNavigator)
            {
                Process.Start(url);
                this.Close();
            }
            else
            {
#if !DEBUG
                try
                {
                    var cli = new WebClient();
                    string data = cli.DownloadString(url);
                    int endPos = data.IndexOf("\" id=\"link\">Download update</a>");
                    int startPos = data.IndexOf("<a href=\"");

                    if (startPos > 0 && endPos > 0)
                    {
                        if (Dialogs.Confirm("New BETA version available", "A new BETA version is available, do you want to automatically download it?"))
                        {
                            //System.Threading.AutoResetEvent waiter = new System.Threading.AutoResetEvent(false);
                            string fileUrl;
                            fileUrl = data.Substring(startPos + "<a href=\"".Length,
                                                     endPos - (startPos + "<a href=\"".Length));
                            localFileName = Path.GetTempFileName();
                            localFileName = Path.ChangeExtension(localFileName, ".almsupdbeta");

                            cli.DownloadProgressChanged += new DownloadProgressChangedEventHandler(NewBetaInDownload);
                            this.toolStripProgressBar.Maximum = 100;
                            cli.DownloadFileAsync(new Uri("https://alm-vision.com/update/" + fileUrl), localFileName);
                            while (cli.IsBusy)
                            {
                                Thread.Sleep(50);
                                Application.DoEvents();
                            }
                            //hope everything is ok....
                            for (int i = 0; i < 50; i++)
                            {
                                Thread.Sleep(50);
                                Application.DoEvents();
                            }
                            Process.Start(localFileName); 
                            Application.Exit();
                        }
                    }
                }
                catch
                {
                    //todo : something ? :)
                }
#endif
            }
            return false;

        }



        private void NewBetaInDownload(object sender, DownloadProgressChangedEventArgs downloadProgressChangedEventArgs)
        {
            this.toolStripProgressBar.Value = downloadProgressChangedEventArgs.ProgressPercentage;
            Thread.Sleep(1);
        }

        private void LicenceAdministrationToolStripMenuItemClick(object sender, EventArgs e)
        {
            Process.Start(Settings.Default.LicenceUrl + "Default.aspx");
        }

        private void ModulesToolStripMenuItemDropDownOpening(object sender, EventArgs e)
        {
            FillRecentFileList();
        }

        public static void SetForcedCloseFlag()
        {
            if (moduleMainForm != null)
            {
                moduleMainForm.IsForcedClose = true;
            }
        }

        public static void TrySaveForm()
        {
            try
            {
                if (moduleMainForm is BondManager.Views.MainForm)
                {
                    BondManager.Views.MainForm bondManagerForm = (BondManager.Views.MainForm)moduleMainForm;
                    string path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + Path.DirectorySeparatorChar + "ALMAutoRecovery." + DateTime.Now.ToString("yyyyMMdd.HHmmss") + ".alw";
                    bondManagerForm.SaveWorkSpace(path);
                    if (File.Exists(path)) bondManagerForm.AddFileHistory(path);
                }
                else if (moduleMainForm is InsuranceBondManager.Views.MainForm)
                {
                    InsuranceBondManager.Views.MainForm bondManagerForm = (InsuranceBondManager.Views.MainForm)moduleMainForm;
                    string path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + Path.DirectorySeparatorChar + "ALMAutoRecovery." + DateTime.Now.ToString("yyyyMMdd.HHmmss") + ".ilw";
                    bondManagerForm.SaveWorkSpace(path);
                    if (File.Exists(path)) bondManagerForm.AddFileHistory(path);
                }
            }
            catch (Exception ex)
            {
                Logger.Exception("Exception cast after trying to save after the previous exception", ex);
            }
        }

        private void toolStripMenuItem6_Click(object sender, EventArgs e)
        {
            LoadPensionFundALM();
        }

        private void toolStripMenuItem7_Click(object sender, EventArgs e)
        {
            LoadInsuranceALM();
        }

        private void toolStripMenuItem8_Click(object sender, EventArgs e)
        {
            LoadTreasuryManagement();
        }


 

        //// http://memprofiler.com/forum/viewtopic.php?t=1160
        //// memody leak bugfix
        //protected override void OnMdiChildActivate(EventArgs e)
        //{
        //    base.OnMdiChildActivate(e);

        //    typeof(Form).InvokeMember("FormerlyActiveMdiChild",
        //        BindingFlags.Instance | BindingFlags.SetProperty |
        //        BindingFlags.NonPublic, null, this, new object[] { null });

        //    GC.Collect();
        //    GC.WaitForPendingFinalizers();
        //}
    }
}
