﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Web;
using System.Windows.Forms;
using ALMSCommon;
using ALMSolutions.Properties;

namespace ALMSolutions.Licence
{
    // 8AA1-0789-9653-A59A
    public partial class SerialNumberForm : Form
    {
        private readonly string machineId = LicenceValidator.GetMachineId();
        private LicenceInfo licenceInfo;

        public SerialNumberForm()
        {
            this.InitializeComponent();

            foreach (string moduleName in AlmConfiguration.ModuleNames)
            {
                this.activatedModulesCheckedListBox.Items.Add(moduleName);
            }
        }

        private string SerialNumber
        {
            get
            {
                return this.serialNumberTextBox.Text.Replace(" ", "").Replace("-", "").Replace(".", "").
                    Replace("\t", "").Replace("\n", "").Replace("\r", "").ToUpper();
            }
        }

        public string ActivationKey
        {
            get { return this.activationKeyTextBox.Text; }
            set
            {
                ILicenceInfo info = LicenceInfo.Deserialize(value);

                if (info != null)
                {
                    this.SetSerialNumber(info.LicenceNumber);
                    this.firstNameTextBox.Text = info.FirstName;
                    this.lastNameTextBox.Text = info.LastName;
                    this.companyTextBox.Text = info.Company;
                    this.phoneNumberTextBox.Text = info.PhoneNumber;
                    this.emailTextBox.Text = info.Email;

                    this.activationKeyTextBox.Text = value;
                }
                else
                {
                    string oldMethodSerialNumber = TryRecoverSerialNumber(value);
                    this.SetSerialNumber(oldMethodSerialNumber);
                }
            }
        }

        private static string TryRecoverSerialNumber(string enctypted)
        {
            try
            {
                string decrypted = Crypt.DecryptStringAES(enctypted, LicenceInfo.Pass);
                return decrypted.Split('|')[0];
            }
            catch (Exception ex)
            {
                Logger.Exception("TryRecoverSerialNumber", ex);
            }

            return null;
        }

        private void SerialNumberFormShown(object sender, EventArgs e)
        {
            this.serialNumberTextBox.Focus();
        }

        private void RegistrationChanged(object sender, EventArgs e)
        {
            this.activationKeyTextBox.Text = "";

            this.obtainActivationKeyButton.Enabled = this.SerialNumber.Length == 16 &&
                                                     this.firstNameTextBox.Text.Length > 0 &&
                                                     this.lastNameTextBox.Text.Length > 0 &&
                                                     this.companyTextBox.Text.Length > 0 &&
                                                     this.phoneNumberTextBox.Text.Length > 0 &&
                                                     this.emailTextBox.Text.Length > 0;
        }

        private void OkButtonClick(object sender, EventArgs e)
        {
            this.Close();
        }

        private void SerialNumberFormKeyDown(object sender, KeyEventArgs e)
        {
            if (!this.serialNumberTextBox.Focused || !e.Control || e.KeyCode != Keys.V)
            {
                return;
            }

            string text = Clipboard.GetText();
            this.SetSerialNumber(text);

            this.obtainActivationKeyButton.Focus();

            e.Handled = true;
        }

        private void SetSerialNumber(string text)
        {
            if (string.IsNullOrEmpty(text))
            {
                return;
            }

            text =
                text.Replace(" ", "").Replace("-", "").Replace(".", "").Replace("\t", "").Replace("\n", "").Replace(
                    "\r", "");
            if (text.Length != 16)
            {
                return;
            }

            text = text.ToUpper();

            this.serialNumberTextBox.Text = string.Format("{0}-{1}-{2}-{3}",
                                                          text.Substring(0, 4), text.Substring(4, 4),
                                                          text.Substring(8, 4), text.Substring(12, 4));
        }

        private void ObtainActivationKeyButtonClick(object sender, EventArgs e)
        {
            string url = Settings.Default.LicenceUrl + "Admin/Activate?l={0}&m={1}&fn={2}&ln={3}&c={4}&p={5}&e={6}";
                                        // used to be Activate.aspx

            url = string.Format(url, this.SerialNumber, this.machineId,
                                UrlEncode(this.firstNameTextBox.Text),
                                UrlEncode(this.lastNameTextBox.Text),
                                UrlEncode(this.companyTextBox.Text),
                                UrlEncode(this.phoneNumberTextBox.Text),
                                UrlEncode(this.emailTextBox.Text));

            try
            {
                Process.Start(url);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
                new ActivationUrlForm(url).ShowDialog(this);
            }
        }

        private static string UrlEncode(string text)
        {
            return HttpUtility.UrlEncode(text);
        }

        private void ActivationKeyTextBoxTextChanged(object sender, EventArgs e)
        {
            try
            {
                this.licenceInfo = LicenceInfo.Deserialize(this.activationKeyTextBox.Text);
                for (int i = 0; i < AlmConfiguration.ModuleNames.Length; i++)
                {
                    this.activatedModulesCheckedListBox.SetItemChecked(i, this.licenceInfo.IsModuleEnabled(i + 1));
                }
            }
            catch
            {
                for (int i = 0; i < this.activatedModulesCheckedListBox.Items.Count; i++)
                {
                    this.activatedModulesCheckedListBox.SetItemChecked(i, false);
                }
                this.okButton.Enabled = false;
                return;
            }

            if (this.licenceInfo == null)
            {
                this.debutValiditeLabel.Visible = this.finValiditeLabel.Visible = this.okButton.Enabled = false;
                return;
            }

            this.okButton.Enabled = this.SerialNumber == this.licenceInfo.LicenceNumber &&
                                    LicenceValidator.IsLicenceValid(this.licenceInfo);
            this.debutValiditeLabel.Visible = this.finValiditeLabel.Visible = true;

            this.debutValiditeLabel.Text = this.licenceInfo.Start.ToShortDateString();
            this.finValiditeLabel.Text = this.licenceInfo.End.ToShortDateString();

            this.debutValiditeLabel.ForeColor = this.licenceInfo.Start > DateTime.Today ? Color.Red : Color.Black;

            this.finValiditeLabel.ForeColor = this.licenceInfo.End < DateTime.Today ? Color.Red : Color.Black;
        }

        private void ActivationKeyTextBoxMouseUp(object sender, MouseEventArgs e)
        {
            this.activationKeyTextBox.SelectAll();
        }

        private void licenseButton_Click(object sender, EventArgs e)
        {
            try
            {
                string pdfPath = Path.Combine(Application.StartupPath, "ALM Solutions License Agreement.pdf");
                Process.Start(pdfPath);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
                Dialogs.Error("License Agreement", "There was a problem displaying the license agreement.\nPlease make sure you have a PDF reader installed.");
            }
        }
    }
}