﻿using System;
using System.Windows.Forms;

namespace ALMSolutions.Licence
{
    public partial class ActivationUrlForm : Form
    {
        private readonly string url;

        public ActivationUrlForm(string url)
        {
            this.url = url;
            this.InitializeComponent();
        }

        private void CopyButtonClick(object sender, EventArgs e)
        {
            urlTextBox.SelectAll();
            urlTextBox.Copy();
        }

        private void ActivationUrlFormLoad(object sender, EventArgs e)
        {
            urlTextBox.Text = url;
        }
    }
}