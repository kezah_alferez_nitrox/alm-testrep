﻿using ALMSolutions.Resources.Language;

namespace ALMSolutions.Licence
{
    partial class SerialNumberForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.okButton = new System.Windows.Forms.Button();
            this.cancelBtton = new System.Windows.Forms.Button();
            this.serialNumberTextBox = new System.Windows.Forms.TextBox();
            this.obtainActivationKeyButton = new System.Windows.Forms.Button();
            this.activationKeyTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.debutValiditeLabel = new System.Windows.Forms.Label();
            this.finValiditeLabel = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.activatedModulesCheckedListBox = new System.Windows.Forms.CheckedListBox();
            this.label5 = new System.Windows.Forms.Label();
            this.firstNameTextBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.lastNameTextBox = new System.Windows.Forms.TextBox();
            this.companyTextBox = new System.Windows.Forms.TextBox();
            this.phoneNumberTextBox = new System.Windows.Forms.TextBox();
            this.emailTextBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.licenseButton = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(314, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "To activate ALM Solutions please enter the following informations";
            // 
            // okButton
            // 
            this.okButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okButton.Enabled = false;
            this.okButton.Location = new System.Drawing.Point(335, 492);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(75, 23);
            this.okButton.TabIndex = 1;
            this.okButton.Text = global::ALMSolutions.Resources.Language.Labels.SerialNumberForm_InitializeComponent_OK;
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Click += new System.EventHandler(this.OkButtonClick);
            // 
            // cancelBtton
            // 
            this.cancelBtton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelBtton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelBtton.Location = new System.Drawing.Point(254, 492);
            this.cancelBtton.Name = "cancelBtton";
            this.cancelBtton.Size = new System.Drawing.Size(75, 23);
            this.cancelBtton.TabIndex = 2;
            this.cancelBtton.Text = "Cancel";
            this.cancelBtton.UseVisualStyleBackColor = true;
            // 
            // serialNumberTextBox
            // 
            this.serialNumberTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.serialNumberTextBox.Location = new System.Drawing.Point(143, 29);
            this.serialNumberTextBox.MaxLength = 19;
            this.serialNumberTextBox.Name = "serialNumberTextBox";
            this.serialNumberTextBox.Size = new System.Drawing.Size(267, 24);
            this.serialNumberTextBox.TabIndex = 3;
            this.serialNumberTextBox.TextChanged += new System.EventHandler(this.RegistrationChanged);
            // 
            // obtainActivationKeyButton
            // 
            this.obtainActivationKeyButton.Enabled = false;
            this.obtainActivationKeyButton.Location = new System.Drawing.Point(12, 214);
            this.obtainActivationKeyButton.Name = "obtainActivationKeyButton";
            this.obtainActivationKeyButton.Size = new System.Drawing.Size(398, 23);
            this.obtainActivationKeyButton.TabIndex = 9;
            this.obtainActivationKeyButton.Text = global::ALMSolutions.Resources.Language.Labels.SerialNumberForm_InitializeComponent_Connect_to_internet_to_obtain_the_activation_key;
            this.obtainActivationKeyButton.UseVisualStyleBackColor = true;
            this.obtainActivationKeyButton.Click += new System.EventHandler(this.ObtainActivationKeyButtonClick);
            // 
            // activationKeyTextBox
            // 
            this.activationKeyTextBox.Location = new System.Drawing.Point(12, 266);
            this.activationKeyTextBox.Multiline = true;
            this.activationKeyTextBox.Name = "activationKeyTextBox";
            this.activationKeyTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.activationKeyTextBox.Size = new System.Drawing.Size(398, 81);
            this.activationKeyTextBox.TabIndex = 10;
            this.activationKeyTextBox.TextChanged += new System.EventHandler(this.ActivationKeyTextBoxTextChanged);
            this.activationKeyTextBox.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ActivationKeyTextBoxMouseUp);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 250);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(156, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "Please enter the activation key:";
            // 
            // debutValiditeLabel
            // 
            this.debutValiditeLabel.AutoSize = true;
            this.debutValiditeLabel.Location = new System.Drawing.Point(89, 358);
            this.debutValiditeLabel.Name = "debutValiditeLabel";
            this.debutValiditeLabel.Size = new System.Drawing.Size(65, 13);
            this.debutValiditeLabel.TabIndex = 13;
            this.debutValiditeLabel.Text = "00/00/0000";
            this.debutValiditeLabel.Visible = false;
            // 
            // finValiditeLabel
            // 
            this.finValiditeLabel.AutoSize = true;
            this.finValiditeLabel.Location = new System.Drawing.Point(341, 358);
            this.finValiditeLabel.Name = "finValiditeLabel";
            this.finValiditeLabel.Size = new System.Drawing.Size(65, 13);
            this.finValiditeLabel.TabIndex = 14;
            this.finValiditeLabel.Text = "00/00/0000";
            this.finValiditeLabel.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(267, 358);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 13);
            this.label3.TabIndex = 16;
            this.label3.Text = "Validity End :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 358);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 13);
            this.label4.TabIndex = 15;
            this.label4.Text = "Validity Start :";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.activatedModulesCheckedListBox);
            this.groupBox1.Location = new System.Drawing.Point(12, 379);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(398, 99);
            this.groupBox1.TabIndex = 17;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Activated modules";
            // 
            // activatedModulesCheckedListBox
            // 
            this.activatedModulesCheckedListBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.activatedModulesCheckedListBox.FormattingEnabled = true;
            this.activatedModulesCheckedListBox.IntegralHeight = false;
            this.activatedModulesCheckedListBox.Location = new System.Drawing.Point(3, 16);
            this.activatedModulesCheckedListBox.MultiColumn = true;
            this.activatedModulesCheckedListBox.Name = "activatedModulesCheckedListBox";
            this.activatedModulesCheckedListBox.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.activatedModulesCheckedListBox.Size = new System.Drawing.Size(392, 80);
            this.activatedModulesCheckedListBox.TabIndex = 0;
            this.activatedModulesCheckedListBox.TabStop = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(10, 36);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(115, 13);
            this.label5.TabIndex = 18;
            this.label5.Text = "Product serial number :";
            // 
            // firstNameTextBox
            // 
            this.firstNameTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.firstNameTextBox.Location = new System.Drawing.Point(143, 59);
            this.firstNameTextBox.MaxLength = 20;
            this.firstNameTextBox.Name = "firstNameTextBox";
            this.firstNameTextBox.Size = new System.Drawing.Size(267, 24);
            this.firstNameTextBox.TabIndex = 4;
            this.firstNameTextBox.TextChanged += new System.EventHandler(this.RegistrationChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(10, 66);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(61, 13);
            this.label6.TabIndex = 20;
            this.label6.Text = "First name :";
            // 
            // lastNameTextBox
            // 
            this.lastNameTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lastNameTextBox.Location = new System.Drawing.Point(143, 89);
            this.lastNameTextBox.MaxLength = 20;
            this.lastNameTextBox.Name = "lastNameTextBox";
            this.lastNameTextBox.Size = new System.Drawing.Size(267, 24);
            this.lastNameTextBox.TabIndex = 5;
            this.lastNameTextBox.TextChanged += new System.EventHandler(this.RegistrationChanged);
            // 
            // companyTextBox
            // 
            this.companyTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.companyTextBox.Location = new System.Drawing.Point(143, 119);
            this.companyTextBox.MaxLength = 20;
            this.companyTextBox.Name = "companyTextBox";
            this.companyTextBox.Size = new System.Drawing.Size(267, 24);
            this.companyTextBox.TabIndex = 6;
            this.companyTextBox.TextChanged += new System.EventHandler(this.RegistrationChanged);
            // 
            // phoneNumberTextBox
            // 
            this.phoneNumberTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.phoneNumberTextBox.Location = new System.Drawing.Point(143, 149);
            this.phoneNumberTextBox.MaxLength = 20;
            this.phoneNumberTextBox.Name = "phoneNumberTextBox";
            this.phoneNumberTextBox.Size = new System.Drawing.Size(267, 24);
            this.phoneNumberTextBox.TabIndex = 7;
            this.phoneNumberTextBox.TextChanged += new System.EventHandler(this.RegistrationChanged);
            // 
            // emailTextBox
            // 
            this.emailTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.emailTextBox.Location = new System.Drawing.Point(143, 179);
            this.emailTextBox.MaxLength = 40;
            this.emailTextBox.Name = "emailTextBox";
            this.emailTextBox.Size = new System.Drawing.Size(267, 24);
            this.emailTextBox.TabIndex = 8;
            this.emailTextBox.TextChanged += new System.EventHandler(this.RegistrationChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(10, 96);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(62, 13);
            this.label7.TabIndex = 25;
            this.label7.Text = "Last name :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(10, 126);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(57, 13);
            this.label8.TabIndex = 26;
            this.label8.Text = "Company :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(10, 156);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(84, 13);
            this.label9.TabIndex = 27;
            this.label9.Text = "Phone Number :";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(10, 186);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(41, 13);
            this.label10.TabIndex = 28;
            this.label10.Text = "E-mail :";
            // 
            // licenseButton
            // 
            this.licenseButton.Location = new System.Drawing.Point(12, 492);
            this.licenseButton.Name = "licenseButton";
            this.licenseButton.Size = new System.Drawing.Size(179, 23);
            this.licenseButton.TabIndex = 29;
            this.licenseButton.Text = "View License Agreement";
            this.licenseButton.UseVisualStyleBackColor = true;
            this.licenseButton.Click += new System.EventHandler(this.licenseButton_Click);
            // 
            // SerialNumberForm
            // 
            this.AcceptButton = this.okButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelBtton;
            this.ClientSize = new System.Drawing.Size(422, 527);
            this.Controls.Add(this.licenseButton);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.emailTextBox);
            this.Controls.Add(this.phoneNumberTextBox);
            this.Controls.Add(this.companyTextBox);
            this.Controls.Add(this.lastNameTextBox);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.firstNameTextBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.finValiditeLabel);
            this.Controls.Add(this.debutValiditeLabel);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.activationKeyTextBox);
            this.Controls.Add(this.obtainActivationKeyButton);
            this.Controls.Add(this.serialNumberTextBox);
            this.Controls.Add(this.cancelBtton);
            this.Controls.Add(this.okButton);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SerialNumberForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "ALM Solutions";
            this.Shown += new System.EventHandler(this.SerialNumberFormShown);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SerialNumberFormKeyDown);
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button cancelBtton;
        private System.Windows.Forms.TextBox serialNumberTextBox;
        private System.Windows.Forms.Button obtainActivationKeyButton;
        private System.Windows.Forms.TextBox activationKeyTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label debutValiditeLabel;
        private System.Windows.Forms.Label finValiditeLabel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckedListBox activatedModulesCheckedListBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox firstNameTextBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox lastNameTextBox;
        private System.Windows.Forms.TextBox companyTextBox;
        private System.Windows.Forms.TextBox phoneNumberTextBox;
        private System.Windows.Forms.TextBox emailTextBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button licenseButton;
    }
}