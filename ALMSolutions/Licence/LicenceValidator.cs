using System;
using System.IO;
using System.Management;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;
using ALMSCommon;

namespace ALMSolutions.Licence
{
    // 0B5C-5BDA-0098-6658
    public class LicenceValidator
    {
        private static string machineIdCache;

        public static string GetMachineId()
        {
            if (machineIdCache == null)
            {
                machineIdCache = GetMd5(GetDeviceId());
            }
            return machineIdCache;
        }

        private static string GetMd5(string text)
        {
            MD5CryptoServiceProvider x = new MD5CryptoServiceProvider();
            byte[] bs = Encoding.UTF8.GetBytes(text);
            bs = x.ComputeHash(bs);
            StringBuilder s = new StringBuilder();
            foreach (byte b in bs)
            {
                s.Append(b.ToString("x2").ToLower());
            }
            return s.ToString();
        }

        private static string GetDeviceId()
        {
            string macAdd = GetProcessorId() + GetMotherBoardId() + GetCompSysProdId();
            macAdd += GetSysProp("Win32_OperatingSystem", "SerialNumber");
            return macAdd;
        }

        private static string GetProcessorId()
        {
            return GetSysProp("Win32_Processor", "ProcessorId");
        }

        private static string GetMotherBoardId()
        {
            return GetSysProp("Win32_BaseBoard", "SerialNumber");
        }

        private static string GetCompSysProdId()
        {
            return GetSysProp("Win32_ComputerSystemProduct", "UUID");
        }

        private static string GetSysProp(string key, string prop)
        {
            string id = "";
            string sQuery = "Select * From " + key;
            ManagementObjectSearcher oManagementObjectSearcher = new ManagementObjectSearcher(sQuery);
            ManagementObjectCollection oCollection = oManagementObjectSearcher.Get();
            foreach (ManagementObject oManagementObject in oCollection)
            {
                id += (string) oManagementObject[prop];
            }

            return id;
        }

        private static string GetLicenceFilePath()
        {
            return Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\balms.lic";
        }
        
        private static string GetLicenceFileOldPath()
        {
            return Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\balms.lic";
        }

        public bool Validate()
        {
            return this.Verify(false);
        }

        private bool Verify(bool force)
        {
            string activationKey = null;

            // if "balms.lic" is in my document (OldPath), but not in AppData (Path), copy the file
            if (!File.Exists(GetLicenceFilePath()))
            {
                if (File.Exists(GetLicenceFileOldPath()))
                {
                    // Copy from My Document to AppData\Roaming
                    System.IO.File.Copy(GetLicenceFileOldPath(), GetLicenceFilePath());
                }
            }
            
            try
            {   // read .lic file
                activationKey = File.ReadAllText(GetLicenceFilePath());
            }
            catch(Exception ex)
            {
                Logger.Exception(" File.ReadAllText(GetLicenceFilePath())", ex);
            }

            this.LicenceInfo = LicenceInfo.Deserialize(activationKey);
            if (!force && this.LicenceInfo != null && IsLicenceValid(this.LicenceInfo))
            {
                return true;
            }

            SerialNumberForm serialNumberForm = new SerialNumberForm();
            serialNumberForm.ShowInTaskbar = true;
            serialNumberForm.ActivationKey = activationKey;
            String version = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
            serialNumberForm.Text = "ALM Vision                                                                           version: "+version;

            if (serialNumberForm.ShowDialog() == DialogResult.OK)
            {
                this.LicenceInfo = LicenceInfo.Deserialize(serialNumberForm.ActivationKey);
                if (this.LicenceInfo != null)
                {
                    this.LicenceInfo.Save(GetLicenceFilePath());
                }
            }

            return this.LicenceInfo != null && IsLicenceValid(this.LicenceInfo);
        }

        public static bool IsLicenceValid(ILicenceInfo licenceInfo)
        {
            if(licenceInfo == null) return false;

            return (licenceInfo.LicenceNumber != null) && (licenceInfo.LicenceNumber.Length == 16) &&
                   (licenceInfo.MachineId == GetMachineId()) &&
                   licenceInfo.Start <= DateTime.Today && licenceInfo.End >= DateTime.Today;
        }

        public bool Update()
        {
            return this.Verify(true);
        }

        public LicenceInfo LicenceInfo { get; private set; }

        public bool IsModuleLicenced(int moduleNumber)
        {
            return this.LicenceInfo != null && this.LicenceInfo.IsModuleEnabled(moduleNumber);
        }
    }
}