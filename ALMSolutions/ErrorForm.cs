using System;
using System.Windows.Forms;
using ALMSCommon.Forms;

namespace ALMSolutions
{
    public partial class ErrorForm : DpiForm
    {
        private bool collapsed;

        public ErrorForm(Exception ex)
        {
            InitializeComponent();
            this.collapsed = false;

            if (ex == null)
            {
                this.errorTextBox.Text = "";
            }
            else
            {
                this.errorTextBox.Text = ex.ToString();
                clearTemporaryDataCheckBox.Checked = ex.ToString().Contains("NHibernate");
            }
        }

        public bool SendReport
        {
            get { return sendReportCheckBox.Checked; }
        }

        public bool ClearTemporaryData
        {
            get { return clearTemporaryDataCheckBox.Checked; }
        }

        public bool TrySave
        {
            get { return trySaveCheckBox.Checked; }
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void detailsButton_Click(object sender, EventArgs e)
        {
            //this.Height = this.Height == 373 ? 151 : 373;
            this.collapsed = !this.collapsed;
            this.ResizeForm();
        }

        private void ResizeForm()
        {
            if (this.collapsed) this.Height = this.closeButton.Bottom + 15 + SystemInformation.CaptionHeight;
            else this.Height = this.errorTextBox.Bottom + 15 + SystemInformation.CaptionHeight;
        }

        protected override void LoadDpiForm(object sender, EventArgs e)
        {
            this.ResizeForm();
        }
    }
}