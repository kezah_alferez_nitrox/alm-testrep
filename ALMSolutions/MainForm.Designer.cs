﻿namespace ALMSolutions
{
    partial class MainForm
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.mainMenuStrip = new System.Windows.Forms.MenuStrip();
            this.modulesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bankAlmToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lifeInsurranceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.simulationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sensitivityAnalysisToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem8 = new System.Windows.Forms.ToolStripMenuItem();
            this.assetModelingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.portfolioAnalysisToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.licenceInformationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.licenceAdministrationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripSeparator();
            this.menuLanguage = new System.Windows.Forms.ToolStripMenuItem();
            this.checkForUpdatesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripSeparator();
            this.clearAllTemporaryDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.exitSOGALMSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            this.versionToolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.mainMenuStrip.SuspendLayout();
            this.statusStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainMenuStrip
            // 
            this.mainMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.modulesToolStripMenuItem});
            this.mainMenuStrip.Location = new System.Drawing.Point(0, 0);
            this.mainMenuStrip.Name = "mainMenuStrip";
            this.mainMenuStrip.ShowItemToolTips = true;
            this.mainMenuStrip.Size = new System.Drawing.Size(846, 24);
            this.mainMenuStrip.TabIndex = 0;
            this.mainMenuStrip.Text = "menuStrip1";
            // 
            // modulesToolStripMenuItem
            // 
            this.modulesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bankAlmToolStripMenuItem,
            this.lifeInsurranceToolStripMenuItem,
            this.toolStripMenuItem3,
            this.toolStripMenuItem6,
            this.toolStripMenuItem7,
            this.toolStripMenuItem8,
            this.assetModelingToolStripMenuItem,
            this.portfolioAnalysisToolStripMenuItem,
            this.toolStripMenuItem1,
            this.licenceInformationToolStripMenuItem,
            this.licenceAdministrationToolStripMenuItem,
            this.toolStripMenuItem5,
            this.menuLanguage,
            this.checkForUpdatesToolStripMenuItem,
            this.toolStripMenuItem4,
            this.clearAllTemporaryDataToolStripMenuItem,
            this.toolStripMenuItem2,
            this.exitSOGALMSToolStripMenuItem});
            this.modulesToolStripMenuItem.Name = "modulesToolStripMenuItem";
            this.modulesToolStripMenuItem.Size = new System.Drawing.Size(65, 20);
            this.modulesToolStripMenuItem.Text = global::ALMSolutions.Resources.Language.Labels.MainForm_InitializeComponent_Modules;
            this.modulesToolStripMenuItem.DropDownOpening += new System.EventHandler(this.ModulesToolStripMenuItemDropDownOpening);
            // 
            // bankAlmToolStripMenuItem
            // 
            this.bankAlmToolStripMenuItem.Name = "bankAlmToolStripMenuItem";
            this.bankAlmToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.bankAlmToolStripMenuItem.Text = "&Bank ALM";
            this.bankAlmToolStripMenuItem.Click += new System.EventHandler(this.BalanceSheetToolStripMenuItemClick);
            // 
            // lifeInsurranceToolStripMenuItem
            // 
            this.lifeInsurranceToolStripMenuItem.Name = "lifeInsurranceToolStripMenuItem";
            this.lifeInsurranceToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.lifeInsurranceToolStripMenuItem.Text = "&Life Insurance";
            this.lifeInsurranceToolStripMenuItem.Visible = false;
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.simulationToolStripMenuItem,
            this.sensitivityAnalysisToolStripMenuItem1});
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(206, 22);
            this.toolStripMenuItem3.Text = global::ALMSolutions.Resources.Language.Labels.MainForm_InitializeComponent_Pension_Fund;
            this.toolStripMenuItem3.Visible = false;
            // 
            // simulationToolStripMenuItem
            // 
            this.simulationToolStripMenuItem.Name = "simulationToolStripMenuItem";
            this.simulationToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.simulationToolStripMenuItem.Text = global::ALMSolutions.Resources.Language.Labels.MainForm_InitializeComponent_Simulation;
            // 
            // sensitivityAnalysisToolStripMenuItem1
            // 
            this.sensitivityAnalysisToolStripMenuItem1.Name = "sensitivityAnalysisToolStripMenuItem1";
            this.sensitivityAnalysisToolStripMenuItem1.Size = new System.Drawing.Size(173, 22);
            this.sensitivityAnalysisToolStripMenuItem1.Text = global::ALMSolutions.Resources.Language.Labels.MainForm_InitializeComponent_Sensitivity_Analysis;
            // 
            // toolStripMenuItem6
            // 
            this.toolStripMenuItem6.Name = "toolStripMenuItem6";
            this.toolStripMenuItem6.Size = new System.Drawing.Size(206, 22);
            this.toolStripMenuItem6.Text = "&Pension Fund ALM";
            this.toolStripMenuItem6.Click += new System.EventHandler(this.toolStripMenuItem6_Click);
            // 
            // toolStripMenuItem7
            // 
            this.toolStripMenuItem7.Name = "toolStripMenuItem7";
            this.toolStripMenuItem7.Size = new System.Drawing.Size(206, 22);
            this.toolStripMenuItem7.Text = "Insurance ALM";
            this.toolStripMenuItem7.Click += new System.EventHandler(this.toolStripMenuItem7_Click);
            // 
            // toolStripMenuItem8
            // 
            this.toolStripMenuItem8.Name = "toolStripMenuItem8";
            this.toolStripMenuItem8.Size = new System.Drawing.Size(206, 22);
            this.toolStripMenuItem8.Text = "&Treasury Management";
            this.toolStripMenuItem8.Click += new System.EventHandler(this.toolStripMenuItem8_Click);
            // 
            // assetModelingToolStripMenuItem
            // 
            this.assetModelingToolStripMenuItem.Name = "assetModelingToolStripMenuItem";
            this.assetModelingToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.assetModelingToolStripMenuItem.Text = global::ALMSolutions.Resources.Language.Labels.MainForm_InitializeComponent_Asset_Modeling;
            this.assetModelingToolStripMenuItem.Visible = false;
            // 
            // portfolioAnalysisToolStripMenuItem
            // 
            this.portfolioAnalysisToolStripMenuItem.Name = "portfolioAnalysisToolStripMenuItem";
            this.portfolioAnalysisToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.portfolioAnalysisToolStripMenuItem.Text = global::ALMSolutions.Resources.Language.Labels.MainForm_InitializeComponent_P_ortfolio_Analysis;
            this.portfolioAnalysisToolStripMenuItem.Visible = false;
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(203, 6);
            // 
            // licenceInformationToolStripMenuItem
            // 
            this.licenceInformationToolStripMenuItem.Name = "licenceInformationToolStripMenuItem";
            this.licenceInformationToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.licenceInformationToolStripMenuItem.Text = global::ALMSolutions.Resources.Language.Labels.MainForm_InitializeComponent__Licence_Information___;
            this.licenceInformationToolStripMenuItem.Click += new System.EventHandler(this.LicenceInformationToolStripMenuItemClick);
            // 
            // licenceAdministrationToolStripMenuItem
            // 
            this.licenceAdministrationToolStripMenuItem.Name = "licenceAdministrationToolStripMenuItem";
            this.licenceAdministrationToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.licenceAdministrationToolStripMenuItem.Text = global::ALMSolutions.Resources.Language.Labels.MainForm_InitializeComponent_Licence__Administration___;
            this.licenceAdministrationToolStripMenuItem.Visible = false;
            this.licenceAdministrationToolStripMenuItem.Click += new System.EventHandler(this.LicenceAdministrationToolStripMenuItemClick);
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(203, 6);
            // 
            // menuLanguage
            // 
            this.menuLanguage.Name = "menuLanguage";
            this.menuLanguage.Size = new System.Drawing.Size(206, 22);
            this.menuLanguage.Text = global::ALMSolutions.Resources.Language.Labels.MainForm_InitializeComponent_Language;
            this.menuLanguage.Visible = false;
            // 
            // checkForUpdatesToolStripMenuItem
            // 
            this.checkForUpdatesToolStripMenuItem.Name = "checkForUpdatesToolStripMenuItem";
            this.checkForUpdatesToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.checkForUpdatesToolStripMenuItem.Text = global::ALMSolutions.Resources.Language.Labels.MainForm_InitializeComponent__Check_For_Updates___;
            this.checkForUpdatesToolStripMenuItem.Click += new System.EventHandler(this.CheckForUpdatesToolStripMenuItemClick);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(203, 6);
            // 
            // clearAllTemporaryDataToolStripMenuItem
            // 
            this.clearAllTemporaryDataToolStripMenuItem.Name = "clearAllTemporaryDataToolStripMenuItem";
            this.clearAllTemporaryDataToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.clearAllTemporaryDataToolStripMenuItem.Text = global::ALMSolutions.Resources.Language.Labels.MainForm_InitializeComponent_Clear_All_Temporary_Data;
            this.clearAllTemporaryDataToolStripMenuItem.Click += new System.EventHandler(this.ClearAllTemporaryDataToolStripMenuItemClick);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(203, 6);
            // 
            // exitSOGALMSToolStripMenuItem
            // 
            this.exitSOGALMSToolStripMenuItem.Name = "exitSOGALMSToolStripMenuItem";
            this.exitSOGALMSToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.exitSOGALMSToolStripMenuItem.Text = "E&xit ALM Solutions";
            this.exitSOGALMSToolStripMenuItem.Click += new System.EventHandler(this.ExitSogalmsToolStripMenuItemClick);
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel,
            this.toolStripProgressBar,
            this.versionToolStripStatusLabel});
            this.statusStrip.Location = new System.Drawing.Point(0, 421);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(846, 22);
            this.statusStrip.TabIndex = 2;
            this.statusStrip.Text = "statusStrip1";
            // 
            // toolStripStatusLabel
            // 
            this.toolStripStatusLabel.Name = "toolStripStatusLabel";
            this.toolStripStatusLabel.Size = new System.Drawing.Size(698, 17);
            this.toolStripStatusLabel.Spring = true;
            this.toolStripStatusLabel.Text = global::ALMSolutions.Resources.Language.Labels.MainForm_InitializeComponent_Ready;
            this.toolStripStatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // toolStripProgressBar
            // 
            this.toolStripProgressBar.Name = "toolStripProgressBar";
            this.toolStripProgressBar.Size = new System.Drawing.Size(100, 16);
            // 
            // versionToolStripStatusLabel
            // 
            this.versionToolStripStatusLabel.Name = "versionToolStripStatusLabel";
            this.versionToolStripStatusLabel.Size = new System.Drawing.Size(31, 17);
            this.versionToolStripStatusLabel.Text = "v 0.1";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::ALMSolutions.Properties.Resources.logo400x400transparent;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(846, 443);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.mainMenuStrip);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.mainMenuStrip;
            this.Name = "MainForm";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.MainFormLoad);
            this.mainMenuStrip.ResumeLayout(false);
            this.mainMenuStrip.PerformLayout();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip mainMenuStrip;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel;
        private System.Windows.Forms.ToolStripProgressBar toolStripProgressBar;
        private System.Windows.Forms.ToolStripStatusLabel versionToolStripStatusLabel;
        private System.Windows.Forms.ToolStripMenuItem modulesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bankAlmToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lifeInsurranceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitSOGALMSToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem assetModelingToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem clearAllTemporaryDataToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem simulationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sensitivityAnalysisToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem portfolioAnalysisToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem licenceInformationToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem checkForUpdatesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem licenceAdministrationToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem menuLanguage;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem8;
    }
}