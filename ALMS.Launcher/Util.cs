﻿using System;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;
using ICSharpCode.SharpZipLib.Zip;

namespace ALMS.Launcher
{
    public class Util
    {


        public static void ShowError(string text, string title)
        {
            MessageBox.Show(text, title, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public static bool Ask(string question, string title)
        {
            return MessageBox.Show(question, title, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes;
        }

        public static string GetInstallPath()
        {
            return Path.GetDirectoryName(Path.GetDirectoryName(typeof (Program).Assembly.Location)); 
            // return Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "ALM Solutions");
        }

        public static void UncompressZip(string zipFile, string directory)
        {
            using (FileStream zipFielStream = File.OpenRead(zipFile))
            using (ZipInputStream zipIn = new ZipInputStream(zipFielStream))
            {
                ZipEntry entry;
                while ((entry = zipIn.GetNextEntry()) != null)
                {
                    if (entry.IsDirectory)
                    {
                        string dirName = Path.Combine(directory, entry.Name);
                        if (!Directory.Exists(dirName))
                        {
                            Directory.CreateDirectory(dirName);
                        }
                    }
                    else
                    {
                        string fileName = Path.Combine(directory, entry.Name);
                        string dirName = Path.GetDirectoryName(fileName);
                        if (dirName != null && !Directory.Exists(dirName))
                        {
                            Directory.CreateDirectory(dirName);
                        }

                        using (FileStream streamWriter = File.Create(Path.Combine(directory, fileName)))
                        {
                            long size = entry.Size;
                            byte[] data = new byte[size];
                            while (true)
                            {
                                size = zipIn.Read(data, 0, data.Length);
                                if (size > 0) streamWriter.Write(data, 0, (int)size);
                                else break;
                            }
                            streamWriter.Close();
                        }
                    }
                }

                zipIn.Close();
                zipFielStream.Close();
            }
        }
        public static DateTime LastDayOfMonth(DateTime date)
        {
            date = date.AddMonths(1);
            date = date.AddDays(-date.Day);

            return date;
        }


    }
}