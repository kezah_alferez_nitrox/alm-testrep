﻿using System;
using System.Diagnostics;
using System.IO;

namespace ALMS.Launcher
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            if (Updater.InstallUpdate(args))
            {
                return; 
            }

            //bool isBeta = args.Length > 0 && args[0] == "beta";
            //string cfgFile = isBeta ? "beta.cfg" : "prod.cfg"; //There are 2 distinct Directories
            string cfgFile = "prod.cfg";
            
            string installPath = Util.GetInstallPath();            
            string configFilePath = Path.Combine(installPath, cfgFile);

            if (!File.Exists(configFilePath))
            {
                Util.ShowError("Invalid installation, error code : 1", "ALM Solutions");
                return;
            }

            string[] configLines = File.ReadAllLines(configFilePath);
            if (configLines.Length == 0)
            {
                Util.ShowError("Invalid installation, error code : 2", "ALM Solutions");
                return;
            }

            string exeFolder = Path.Combine(installPath, configLines[0]);
            string exeFilePath = Path.Combine(exeFolder, "ALMSolutions.exe");
            if (!File.Exists(exeFilePath))
            {
                Util.ShowError("Invalid installation, error code : 3", "ALM Solutions");
                return;
            }

            LaunchApp(exeFolder, exeFilePath, args);
        }

        private static void LaunchApp(string workingDirectory, string exeFilePath, string[] args)
        {
            Process process = new Process();
            process.StartInfo.Arguments = string.Join(" ", args);
            process.StartInfo.FileName = exeFilePath;
            process.StartInfo.WorkingDirectory = workingDirectory;
            process.StartInfo.UseShellExecute = false;
            process.Start();
        }
    }
}
