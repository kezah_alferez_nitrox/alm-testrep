using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using ALMS.Launcher.Resources.Language;

namespace ALMS.Launcher
{
    internal class Updater
    {
        private const string AlmSolutionsExe = "ALMSolutions.exe";
        private static string appName;

        public static bool InstallUpdate(string[] args)
        {
            if (args != null && args.Length == 1)
            {
                if (Path.GetExtension(args[0]) == ".almsupd")
                {
                    appName = Labels.Updater_InstallUpdate_ALM_Solutions;
                    InstallUpdate(args[0], "prod.cfg", false);
                    return true;
                }

                if (Path.GetExtension(args[0]) == ".almsupdbeta")
                {
                    appName = Labels.Updater_InstallUpdate_ALM_Solutions_beta;
                    InstallUpdate(args[0], "prod.cfg", true);   // used to be "beta.cfg"
                    return true;
                }
            }

            return false;
        }

        private static void InstallUpdate(string updateFile, string cfgFilePath, bool isBeta)
        {
            string newVersion = GetNewVersion(updateFile);
            if (newVersion == null) return;

            cfgFilePath = Path.Combine(Util.GetInstallPath(), cfgFilePath);
            string oldVersion = GetOldVersion(cfgFilePath);  // reads the file prod.cfg or beta.cfg

            if (!CanUpdate(oldVersion, newVersion)) return;

            if (!string.IsNullOrEmpty(oldVersion))
            {
                string oldPath = Path.Combine(Util.GetInstallPath(), oldVersion);
                string oldExe = Path.Combine(oldPath, AlmSolutionsExe);

                if (!CheckRunning(oldExe)) return;
            }

            string newPath = Path.Combine(Util.GetInstallPath(), newVersion);

            if (InstallNewVersion(updateFile, cfgFilePath, newVersion, newPath))
            {
                Process process = new Process();
                process.StartInfo.Arguments = isBeta ? "beta" : "";
                process.StartInfo.FileName = typeof(Program).Assembly.Location;
                process.StartInfo.UseShellExecute = false;
                process.Start();
            }
        }

        private static bool InstallNewVersion(string updateFile, string cfgFilePath, string newVersion, string newPath)
        {
            try
            {
                if (Directory.Exists(newPath)) Directory.Delete(newPath, true);
                Directory.CreateDirectory(newPath);

                Util.UncompressZip(updateFile, newPath);

                File.WriteAllText(cfgFilePath, newVersion);

                MessageBox.Show(string.Format(Labels.Updater_InstallNewVersion_Version__0__was_installed_, newVersion), appName, MessageBoxButtons.OK,
                                MessageBoxIcon.Information);
            }
            catch (Exception)
            {
                Util.ShowError(string.Format(Labels.Updater_InstallNewVersion_Invalid_update_file__error_code____0_, 5), appName);
                return false;
            }

            return true;
        }

        private static bool CanUpdate(string oldv, string newv)
        {
            if (string.IsNullOrEmpty(oldv)) return true;

            Version oldVersion;
            Version newVersion;
            try
            {
                oldVersion = new Version(oldv);
                newVersion = new Version(newv);
            }
            catch (Exception)
            {
                Util.ShowError(string.Format(Labels.Updater_InstallNewVersion_Invalid_update_file__error_code____0_, 4), appName);
                return false;
            }

            if (oldVersion >= newVersion)
            {
                MessageBox.Show(
                        Labels.Updater_CanUpdate_Can_t_upgrade_because_the_installed_version_is_more_recent,
                        appName,
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            string question;
            if (string.IsNullOrEmpty(oldv))
            {
                question = string.Format(Labels.Updater_CanUpdate_Update_ALM_Solutions_to_version__0___, newVersion);
            }
            else
            {
                question = string.Format(Labels.Updater_CanUpdate_Update_ALM_Solutions_from_version__0__to_version__1___, oldv, newv);
            }

            return Util.Ask(question, appName);
        }

        private static string GetOldVersion(string cfgFilePath)
        {
            if (!File.Exists(cfgFilePath))
            {
                return null;
            }

            string readAllText = File.ReadAllText(cfgFilePath);
            return readAllText.Trim();
        }

        private static bool CheckRunning(string filePath)
        {
            while (IsFileInUse(filePath))
            {
                if (MessageBox.Show(
                    Labels.Updater_CheckRunning_,
                    appName,
                    MessageBoxButtons.RetryCancel, MessageBoxIcon.Question) == DialogResult.Cancel)
                {
                    return false;
                }
            }

            return true;
        }

        private static string GetNewVersion(string updateFile)
        {
            string version = null;

            try
            {
                string tempRoot = Path.GetTempPath();
                if (!Directory.Exists(tempRoot))
                {
                    tempRoot = Path.GetPathRoot(Environment.SystemDirectory) ?? "c:\\";
                }

                string tempDir = Path.Combine(tempRoot, "ALMSolutionsTemp");                

                if(Directory.Exists(tempDir))Directory.Delete(tempDir, true);
                Directory.CreateDirectory(tempDir);
                
                Util.UncompressZip(updateFile, tempDir);

                string exePath = Path.Combine(tempDir, AlmSolutionsExe);  // Version of ALMSolution.exe in the update

                version = AssemblyName.GetAssemblyName(exePath).Version.ToString();
            }
            catch(Exception)
            {
                Util.ShowError(string.Format(Labels.Updater_InstallNewVersion_Invalid_update_file__error_code____0_, 1), appName);
            }

            return version;
        }

        private static bool IsFileInUse(string filePath)
        {
            if (!File.Exists(filePath)) return false;

            FileStream stream = null;

            try
            {
                stream = File.Open(filePath, FileMode.Open, FileAccess.ReadWrite, FileShare.None);
            }
            catch (IOException)
            {
                return true;
            }
            finally
            {
                if (stream != null)
                {
                    stream.Close();
                }
            }

            return false;
        }
    }
}