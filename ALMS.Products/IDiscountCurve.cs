using System;

namespace ALMS.Products
{
    public interface IDiscountCurve
    {
        DateTime RefDate { get; }
        double GetValueAtDate(DateTime date);
        double DiscountFactor(DateTime maturite1, DateTime maturite2);
        double Forward(DateTime date, DateTime startDate, DateTime endDate);
        double MarketDiscount(DateTime date, DateTime paymentDate, double spread, CouponBasis basis, int numCoupon);
        double ComputeForwardCoupon(ICouponExpressionEvaluator valorisationEvaluator, IFundamentalVector fundamentalVector, DateTime startDate);
        double MarketDuration(DateTime date);
    }
}