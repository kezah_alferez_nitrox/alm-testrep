using System;

namespace ALMS.Products
{
    public class PricerUtil
    {
        public const double DaysPerYear = 360.0;

        public static double YieldDiscount(DateTime date, DateTime paymentDate, double yield)
        {
            return 1 / Math.Pow(1 + yield, (paymentDate - date).TotalDays / DaysPerYear);
        }

        public static DateTime OffsetDateByFundamentalVector(DateTime date, IFundamentalVector fundamentalVector)
        {
            if (fundamentalVector == null) return date;

            switch (fundamentalVector.Unit)
            {
                case TimeUnit.Days:
                    return date.AddDays(fundamentalVector.Delta);
                case TimeUnit.Months:
                    return date.AddMonths(fundamentalVector.Delta);
                case TimeUnit.Years:
                    return date.AddYears(fundamentalVector.Delta);
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public static int DateToInt(DateTime date, bool daily)
        {
            return daily ? (int)(date - DateTime.MinValue).TotalDays : date.Year * 12 + date.Month;
        }

        public static DateTime StepToDate(DateTime startDate, int deltaSteps, bool daily)
        {
            return daily ? startDate.AddDays(deltaSteps) : startDate.AddMonths(deltaSteps);
        }

        public static int DateDiffInSteps(DateTime date1, DateTime date2, bool isDaily)
        {
            return DateToInt(date1, isDaily) - DateToInt(date2, isDaily);
        }

        public static int GetProductExpiryStep(IProduct product, DateTime valuationDate, bool isDaily)
        {
            int result = DateDiffInSteps(product.GetExpiryDate(valuationDate), valuationDate, isDaily);
            //if (result == 0 && product.GetExpiryDate(valuationDate)>= valuationDate && !isDaily)
            //    result = 1;

            return result;
        }

        public static DateTime LastDayOfMonth(DateTime date)
        {
            date = date.AddMonths(1);
            date = date.AddDays(-date.Day);

            return date;
        }
    }
}