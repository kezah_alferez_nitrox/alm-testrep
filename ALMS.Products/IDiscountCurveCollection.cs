using System;

namespace ALMS.Products
{
    public interface IDiscountCurveCollection
    {
        IDiscountCurve GetDiscountCurveAtDate(DateTime date);
        IDiscountCurve[] DiscountCurves { get; }
    }
}