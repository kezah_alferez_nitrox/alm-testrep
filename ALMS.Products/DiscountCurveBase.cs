using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using ALMSCommon.Threading;

namespace ALMS.Products
{
    public class DiscountCurveBase : IDiscountCurve
    {
        protected readonly DateTime refDate;
        protected readonly IList<IFundamentalVector> fundamentalVectors;
        protected readonly ThreadSafeDictionary<string, CashflowProduct> allCashFlowProduct;
        protected readonly DateTime startDate;
        protected double[] zc;
        protected readonly DateTime[] dates;

        private readonly bool useCache;

        public DiscountCurveBase(DateTime refDate, IList<IFundamentalVector> fundamentalVectors, ThreadSafeDictionary<string, CashflowProduct> allCashFlowProduct)
            : this(refDate, fundamentalVectors, allCashFlowProduct, false)
        {
        }

        public DiscountCurveBase(DateTime refDate, IList<IFundamentalVector> fundamentalVectors, ThreadSafeDictionary<string, CashflowProduct> allCashFlowProduct, bool useCache)
        {
            this.refDate = refDate;
            this.fundamentalVectors = fundamentalVectors;

#if !DONT_CACHE_RESULTS
            this.allCashFlowProduct = allCashFlowProduct;
            this.useCache = useCache;
#else
            this.allCashFlowProduct = new ThreadSafeDictionary<string, CashflowProduct>();
            this.useCache = false;
#endif
            this.dates = new DateTime[fundamentalVectors.Count];
            this.zc = new double[fundamentalVectors.Count];

            this.ComputeObservedDates();
        }

        public double[] Zc
        {
            get { return this.zc; }
        }

        public DateTime[] Dates
        {
            get { return this.dates; }
        }

        public DateTime RefDate
        {
            get { return this.refDate; }
        }

        private void ComputeObservedDates()
        {
            for (int i = 0; i < fundamentalVectors.Count; i++)
            {
                this.Dates[i] = PricerUtil.OffsetDateByFundamentalVector(this.refDate.AddDays(1), fundamentalVectors[i]).AddDays(-1);
            }
        }

        public double GetValueAtDate(DateTime date)
        {
            if (zc == null || zc.Length == 0) return 0;

            int n = this.dates.Length;

            double result;// = Math.Log(1 + this.zc[0]);

            int i = 0;
            while (date > this.dates[i] && (i < n - 1)) i++;

            if (i > 0)
            {
                i--;
                if (date < this.dates[i + 1])
                {
                    result = Math.Log(1 + this.zc[i]) +
                             (date - this.dates[i]).TotalDays /
                             (this.dates[i + 1] - this.dates[i]).TotalDays *
                             (Math.Log(1 + this.zc[i + 1]) - Math.Log(1 + this.zc[i]));
                }
                else
                {
                    result = Math.Log(1 + this.zc[i + 1]);
                }
            }
            else result = Math.Log(1 + this.zc[0]);

            return result;
        }

        private readonly ConcurrentDictionary<DateTime, double> discountFactorCache = new ConcurrentDictionary<DateTime, double>();

        public double DiscountFactor(DateTime maturite)
        {
            double value;

            if (useCache)
            {
                if (!discountFactorCache.TryGetValue(maturite, out value))
                {
                    value = this.DiscountFactorInternal(maturite);
                    discountFactorCache[maturite] = value;
                }
            }
            else
            {
                value = this.DiscountFactorInternal(maturite);
            }

            return value;
        }

        public double DiscountFactor(DateTime maturite1, DateTime maturite2)
        {
            return DiscountFactor(maturite2) / DiscountFactor(maturite1);
        }

        private double DiscountFactorInternal(DateTime maturite)
        {
            double t = (maturite - this.RefDate).TotalDays / PricerUtil.DaysPerYear;
            double taux = this.GetValueAtDate(maturite);
            return Math.Exp(-taux * t);
        }

        public double Forward(DateTime date, DateTime startDate, DateTime endDate)
        {
            return (DiscountFactor(date, startDate) / DiscountFactor(date, endDate) - 1);
        }

        public double MarketDiscount(DateTime date, double spread, CouponBasis spreadBasis, int numCoupon)
        {
            if (spread == 0) return DiscountFactor(date);
            double ts = DayCounter.GetCvg(this.RefDate, date, spreadBasis);
            if (ts == 0) return DiscountFactor(date);
            double delta = ts / (double)(numCoupon + 1);
            double df = DiscountFactor(date);
            double result = df * Math.Pow(1.0 + delta * spread * Math.Pow(df, delta / ts), -ts / delta);
            return result;
        }

        public double MarketDiscount(DateTime date, DateTime paymentDate, double spread, CouponBasis spreadBasis, int numCoupon)
        {
            return MarketDiscount(paymentDate, spread, spreadBasis, numCoupon) / MarketDiscount(date, spread, spreadBasis, numCoupon);
        }

        public double ComputeForwardCoupon(ICouponExpressionEvaluator valorisationEvaluator, IFundamentalVector fundamentalVector, DateTime startDate)
        {
            DiscountCurveCollection discountCurves = new DiscountCurveCollection(this.RefDate, new[] { this });
            FundamentalVectorProduct fundamentalVectorProduct = new FundamentalVectorProduct(fundamentalVector, 1.0);

#if !DONT_CACHE_RESULTS
            string key = "F" + fundamentalVector.VariableName + startDate;
            CashflowProduct cashflowProduct = this.allCashFlowProduct.
                TryGetValueOrAdd(key, () => new CashflowProduct(startDate, fundamentalVectorProduct, false));
#else
            CashflowProduct cashflowProduct = new CashflowProduct(startDate, fundamentalVectorProduct);
#endif
            double[] face = new double[cashflowProduct.NumCashFlows+1];
            cashflowProduct.CalculateFace( face, this.RefDate, 0.0,  valorisationEvaluator, null, discountCurves, this.startDate);
            double f = cashflowProduct.GetFairFixedCoupon(face, startDate, 0.0,  valorisationEvaluator, null, discountCurves);
      
            return f;
        }

        public double MarketDuration(DateTime maturite)
        {
            double t = (maturite - this.RefDate).TotalDays / PricerUtil.DaysPerYear;
            return t * DiscountFactor(maturite);

        }

        public class FundamentalVectorProduct : IProduct
        {
            private readonly IFundamentalVector fundamentalVector;
            private readonly double coupon;
            public string Amortization { get { return "1"; } } //dummy
            public int ALMID { get { return -1; } } //dummy
            public string ComplementString { get { return "0"; } } //dummy
            public int LagNoAmortisationPeriodInMonths { get { return 0; } }//dummy
            public int IntDuration { get; set; }//dummy
            public DateTime? StartDate { get; set; }//dummy
            

            public FundamentalVectorProduct(IFundamentalVector fundamentalVector, double coupon)
            {
                this.fundamentalVector = fundamentalVector;
                this.coupon = coupon;
            }

            #region Implementation of IProduct

            public double Complement
            {
                get { return 0; }
            }

            public ProductAmortizationType AmortizationType
            {
                get { return ProductAmortizationType.BULLET; }
            }

            public double GetFace(DateTime date)
            {
                return 1.0;
            }
            private ProductAttrib attrib;
            public ProductAttrib Attrib
            {
                get { return this.attrib; }
                set { this.attrib = value; }
            }
            public CouponBasis CouponBasis
            {
                get { return this.fundamentalVector.DateConvention; }
            }

            public string Coupon
            {
                get { return this.coupon.ToString(); }
            }

            public string IRC { get; private set; }

            public double? FirstRate
            {
                get { return 0; }
            }

            public ICurrency Currency
            {
                get { return null; }
            }

            public ProductFrequency Frequency
            {
                get { return this.fundamentalVector.Frequency; }
            }

            public bool IsReceived
            {
                get { return true; }
            }

            public CouponType CouponType
            {
                get { return CouponType.Fixed; }
            }

            public CouponCalcType CouponCalcType
            {
                get { return CouponCalcType.InAdvance; }
            }

            public string Spread
            {
                get { return "0"; }
            }

            public bool IsDerivative
            {
                get { return false; }
            }


            public DateTime? IssueDate
            {
                get { return null; }
            }

            public DateTime GetExpiryDate(DateTime valuationDate)
            {
                return PricerUtil.OffsetDateByFundamentalVector(valuationDate, this.fundamentalVector);
            }

            #endregion
        }
    }


}

