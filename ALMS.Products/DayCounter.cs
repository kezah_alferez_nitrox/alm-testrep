using System;

namespace ALMS.Products
{
    public static class DayCounter
    {
        public static double GetCvg(DateTime start, DateTime end, CouponBasis couponBasis)
        {
            double cvge = (end.Date - start.Date).TotalDays;

            switch (couponBasis)
            {
                case CouponBasis.ACT360:
                    return cvge / 360.0;
                case CouponBasis.A30360:
                    return GetYearFractionUs30360(start, end);
                case CouponBasis.ISMA30360:
                    return GetYearFractionEu30360(start, end);
                case CouponBasis.ACT365:
                    return cvge / 365.0;
                case CouponBasis.ACTACT:
                    return GetYearFractionActAct(start, end);
                case CouponBasis.CONT:
                default:
                    return cvge / 365.25;
            }
        }

        public static int GetDays(DateTime start, DateTime end, CouponBasis couponBasis)
        {
            double cvg = GetCvg(start, end, couponBasis);

            double days;

            switch (couponBasis)
            {
                case CouponBasis.ACT360:
                    days = cvg * 360.0;
                    break;
                case CouponBasis.A30360:
                    days = GetYearFractionUs30360(start, end) * 360.0;
                    break;
                case CouponBasis.ISMA30360:
                    days = GetYearFractionEu30360(start, end) * 360.0;
                    break;
                case CouponBasis.ACT365:
                    days = cvg * 365.0;
                    break;
                case CouponBasis.ACTACT:
                    days = (end - start).TotalDays;
                    break;
                case CouponBasis.CONT:
                default:
                    days = cvg * 365.25;
                    break;
            }

            return (int)Math.Round(days);
        }

        private static double GetYearFractionUs30360(DateTime start, DateTime end)
        {
            int day1 = start.Day;
            int day2 = end.Day;
            int month1 = start.Month;
            int month2 = end.Month;
            int month12 = start.AddDays(1).Month;
            int month22 = end.AddDays(1).Month;
            int year1 = start.Year;
            int year2 = end.Year;

            if (month12 != month1)
            {
                day1 = 30;
            }

            if (month22 != month2) day2 = 30;

            int nofDays = 360 * (year2 - year1) + 30 * (month2 - month1 - 1) + Math.Max(0, 30 - day1) + Math.Min(30, day2);
            double cvge = nofDays / 360.0;

            return cvge;
        }

        private static double GetYearFractionEu30360(DateTime start, DateTime end)
        {
            int day1 = start.Day;
            int day2 = end.Day;
            int month1 = start.Month;
            int month2 = end.Month;
            int year1 = start.Year;
            int year2 = end.Year;

            int nofDays = 360 * (year2 - year1) + 30 * (month2 - month1 - 1) + Math.Max(0, 30 - day1) + Math.Min(30, day2);
            double cvge = nofDays / 360.0;

            return cvge;
        }

        private static double GetYearFractionActAct(DateTime start, DateTime end)
        {
            double cvge;

            //fraction of days within same year, simply divide by number of days in year
            if (start.Year == end.Year)
            {
                cvge = (end.Date - start.Date).TotalDays / GetDaysInYear(start.Year);
            }
            //else if none is leap
            else if (!DateTime.IsLeapYear(start.Year) && !DateTime.IsLeapYear(end.Year))
            {
                cvge = (end.Date - start.Date).TotalDays / 365.0;
            }
            //else one of them is, count number of days in each year fraction
            else
            {
                cvge = (new DateTime(start.Year, 12, 31).Date - start.Date).TotalDays / GetDaysInYear(start.Year);
                cvge += (end.Date - new DateTime(end.Year - 1, 12, 31).Date).TotalDays / GetDaysInYear(end.Year);

                cvge += end.Year - start.Year - 1.0;
            }

            return cvge;
        }

        private static int GetDaysInYear(int year)
        {
            return DateTime.IsLeapYear(year) ? 366 : 365;
        }
    }
}