﻿using System;

namespace ALMS.Products
{
    public interface ICouponExpressionEvaluator
    {
        double Evaluate(string formula, DateTime fixingDate, IDiscountCurve discountCurve);
    }
}
