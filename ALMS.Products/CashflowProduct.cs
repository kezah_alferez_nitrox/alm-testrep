using System;
using System.Collections.Generic;
using System.Linq;



namespace ALMS.Products
{
    public class CashflowProduct
    {
        private readonly List<Cashflow> cashflows = new List<Cashflow>();
        private readonly ICurrency currency;
        private readonly DateTime expiryDate;
        private readonly List<Cashflow> interestCashflows = new List<Cashflow>();
        private readonly List<Cashflow> ircCashflows = new List<Cashflow>();
        private readonly int nonAdjustedNumCashFlows;
        private readonly int numCashFlows;
        private readonly List<DateTime> paymentDates;
        private readonly int period;
        private readonly List<Cashflow> principalCashflows = new List<Cashflow>();
        private readonly IProduct product;
        private readonly bool isDaily;
        private readonly DateTime startDate;
        private readonly int durationCap;


        public CashflowProduct(DateTime startDate, IProduct product, bool isDaily)
        {
            this.startDate = startDate;
            this.product = product;
            this.isDaily = isDaily;
            this.expiryDate = product.GetExpiryDate(startDate.AddDays(1));//.AddDays(-1);
            if (product is DiscountCurveBase.FundamentalVectorProduct)
                this.expiryDate=this.expiryDate.AddDays(-1);
            this.currency = product.Currency;

            this.period = (int)product.Frequency > 0 ? 12 / (int)product.Frequency : 0;

            DateTime adjustedExpiry = this.expiryDate;



            if (this.IsProductAmortizationAdjustable())
            {

                // todo : il faut mettre ici la date max de simulation
                int durationmax = 100;

                //on s'assure qu'on ajoute un multiple de la p�riode
                durationmax = durationmax - (durationmax % this.period);
                adjustedExpiry = this.expiryDate.AddDays(1).AddMonths(durationmax).AddDays(-1);

                if (this.product.AmortizationType == ProductAmortizationType.ADJUSTABLE_CAPPED_TERM)
                {
                    double complement = 0;
                    double.TryParse(this.product.ComplementString, out complement);
                    
                    this.durationCap = (int)Math.Ceiling(complement / (double)this.period);
                }
            }

            this.paymentDates = GetPaymentDates(this.startDate, adjustedExpiry, this.period, product);

            this.numCashFlows = this.paymentDates.Count;

            this.BuildCashflows();

            this.nonAdjustedNumCashFlows = this.IsProductAmortizationAdjustable() ?
                this.interestCashflows.Count(c => c.PaymentDate <= this.expiryDate) :
                this.numCashFlows;
        }


        public Cashflow[] Cashflows
        {
            get { return this.cashflows.ToArray(); }
        }

        public int NumCashFlows
        {
            get { return this.numCashFlows; }
        }

        public List<Cashflow> InterestCashflows
        {
            get { return this.interestCashflows; }
        }

        public List<Cashflow> PrincipalCashflows
        {
            get { return this.principalCashflows; }
        }
        private bool IsProductAmortizationAdjustable()
        {
            return this.product.AmortizationType == ProductAmortizationType.ADJUSTABLE_TERM ||
                   this.product.AmortizationType == ProductAmortizationType.ADJUSTABLE_CAPPED_TERM;
        }

        /*        public void CalculateFace( double[] face, double coupon, ICouponExpressionEvaluator valorisationEvaluator,
                                       IFormulaEvaluator standardEvaluator, IDiscountCurveCollection discountCurves)
                {
                    //face need to be deleted afterward
                    //double[] face = new double[this.numCashFlows+1];
                    for (int i = 0; i <= this.numCashFlows; i++)
                        face[i] = GetFace(i, coupon, valorisationEvaluator,
                                        standardEvaluator, discountCurves);
                    return;
                }*/
        public void CalculateFace(double[] face, 
                                DateTime evalDate, 
                                double coupon, 
                                ICouponExpressionEvaluator valorisationEvaluator,
                                IFormulaEvaluator standardEvaluator, 
                                IDiscountCurveCollection discountCurves, 
                                DateTime startDate, 
                                bool lazyUpdate = false,
                                double[] reserve_cashflows = null

            )
        {
            int n = this.numCashFlows;

            //Do we need to update
            if (false && lazyUpdate &&
                (this.product.AmortizationType == ProductAmortizationType.BULLET ||
                this.product.AmortizationType == ProductAmortizationType.NO_MATURITY ||
                    this.product.AmortizationType == ProductAmortizationType.LINEAR_AMORT ||
                    this.product.AmortizationType == ProductAmortizationType.CST_PAY ||
                    this.product.AmortizationType == ProductAmortizationType.VAR ||
                    this.product.AmortizationType == ProductAmortizationType.VAR_NOMINAL))
                return;

            switch (this.product.AmortizationType)
            {
                case ProductAmortizationType.BULLET:
                case ProductAmortizationType.NO_MATURITY:
                    for (int i = 0; i < n; i++)
                        face[i] = 1;
                    face[n] = 0;
                    break;

                case ProductAmortizationType.LINEAR_AMORT:
                    for (int i = 0; i < n; i++)
                        face[i] = 1.0 * (n - i) / n;
                    face[n] = 0;
                    break;

                case ProductAmortizationType.CST_PAY:
                    int numberOfPayementsToStart = 0;
                    //first find the start amortisation cashflow 
                    for (int i = 0; i < n; i++)
                    {
                        if (this.paymentDates[i] > PricerUtil.LastDayOfMonth(startDate.AddMonths(product.LagNoAmortisationPeriodInMonths)))
                        {
                            break;
                        }
                        numberOfPayementsToStart++;
                    }
                    //if (product.Attrib == ProductAttrib.InFuture)
                    //    numberOfPayementsToStart--;
                    //MTU lag
                    for (int i = 0; i < n; i++)
                    {
                        if ((Math.Abs(coupon - 0) < Double.MinValue) ||
                            (i < numberOfPayementsToStart))
                        {
                            face[i] = 1.0;
                        }
                        else
                        {
                            double divBy = (1 - Math.Pow(1 + coupon, -(n - numberOfPayementsToStart)));
                            face[i] = divBy == 0 ? 1 : (1 - Math.Pow(1 + coupon, i - n)) / divBy;
                        }
                    }
                    face[n] = 0;
                    break;

                case ProductAmortizationType.VAR:
                case ProductAmortizationType.VAR_NOMINAL:
                    for (int i = 0; i < n; i++)
                    {
                        Cashflow cashflow = this.cashflows.First(cf => cf.CashflowType == CashflowType.Interest && cf.NumCashFlow == i);
                        face[i] = this.product.GetFace(cashflow.StartDate);
                    }
                    face[n] = 0;
                    break;
                case ProductAmortizationType.CST_TERM:
                    this.CstTermFace(face, evalDate, valorisationEvaluator, standardEvaluator, discountCurves, lazyUpdate);
                    break;
                case ProductAmortizationType.ADJUSTABLE_TERM:
                    this.CstTermAjustableDurationFace(face, evalDate, valorisationEvaluator, standardEvaluator, discountCurves, lazyUpdate);
                    //this.product.IntDuration= 
                    break;
                case ProductAmortizationType.ADJUSTABLE_CAPPED_TERM:
                    this.CstTermAjustableDurationFace(face, evalDate, valorisationEvaluator, standardEvaluator, discountCurves, lazyUpdate, durationCap);
                    break;
                case ProductAmortizationType.RESERVE_CASHFLOW:
                case ProductAmortizationType.RESERVE_NOMINAL:
                    for (int i = 0; i < n; i++)
                    {
                        face[i] = 0;
                        for (int j = i; j < n; j++)
                        {
                            face[i] += reserve_cashflows[j];
                        }
                    }
                    face[n] = 0;
                    break;
                
                    
                default:

                    throw new ArgumentOutOfRangeException();
            }
        }



        /*        private double GetFace(int i, double coupon, ICouponExpressionEvaluator valorisationEvaluator,
                                       IFormulaEvaluator standardEvaluator, IDiscountCurveCollection discountCurves)
                {
                    if (i == this.numCashFlows) return 0;

                    int n = this.numCashFlows;

                    switch (this.product.AmortizationType)
                    {
                        case ProductAmortizationType.BULLET:
                            return 1;

                        case ProductAmortizationType.LINEAR_AMORT:
                            return 1.0 * (n - i) / n;

                        case ProductAmortizationType.CST_PAY:
                            if (Math.Abs(coupon - 0) < Double.MinValue) return 1;

                            return (1 - Math.Pow(1 + coupon , i - n)) /
                                   (1 - Math.Pow(1 + coupon , -n));

                        case ProductAmortizationType.VAR:
                        case ProductAmortizationType.VAR_NOMINAL:
                            Cashflow cashflow = this.cashflows.First(cf => cf.CashflowType == CashflowType.Interest && cf.NumCashFlow == i);
                            double face = this.product.GetFace(cashflow.StartDate);
                            return face;

                        case ProductAmortizationType.CST_TERM:
                            return this.CstTermFace(i, valorisationEvaluator, standardEvaluator, discountCurves);

                        case ProductAmortizationType.ADJUSTABLE_TERM:
                            return this.CstTermAjustableDurationFace(i, valorisationEvaluator, standardEvaluator, discountCurves);

                        case ProductAmortizationType.ADJUSTABLE_CAPPED_TERM:
                            return this.CstTermAjustableDurationFace(i, valorisationEvaluator, standardEvaluator, discountCurves, this.product.Complement);

                        default:

                            throw new ArgumentOutOfRangeException();
                    }
                }*/

        // todo: mettre en cache
        private void CstTermFace(double[] faceTable, DateTime evalDate, ICouponExpressionEvaluator valorisationEvaluator,
                                   IFormulaEvaluator standardEvaluator, IDiscountCurveCollection discountCurves, bool lazyUpdate)
        {
            double face = 1;
            //int firstAmortcashflowIndex = 0;
            ////first find the start amortisation cashflow 
            //for (int i = 0; i < this.numCashFlows; i++)
            //{
            //    if (this.paymentDates[i] > startDate.AddMonths(lagNoAmortisationPeriodInMonths))
            //    {
            //        break;
            //    }
            //    firstAmortcashflowIndex++;
            //}

            for (int j = 0; j < this.numCashFlows; j++)
            {
                if (face < Double.Epsilon)
                {
                    faceTable[j] = 0;
                }
                else
                {
                    faceTable[j] = face;

                    Cashflow cashflow = this.interestCashflows[this.numCashFlows - j - 1];

                    if (lazyUpdate && cashflow.FixingDate <= evalDate.AddDays(1).AddMonths(-1).AddDays(-1))
                        face = faceTable[j + 1];
                    else
                    {
                        int duration = this.numCashFlows - j;

                        IDiscountCurve discountCurve = discountCurves.GetDiscountCurveAtDate(cashflow.FixingDate);
                        double spread = this.GetSpread(this.product.Spread, cashflow.FixingDate, discountCurve, valorisationEvaluator, standardEvaluator);
                        double rate = cashflow.GetCashflowValue(cashflow.FixingDate, spread, valorisationEvaluator, standardEvaluator, discountCurves);

                        double interestIncome = rate;
                        double termPayment = this.TermPayment(duration, interestIncome);
                        double principalIncome = termPayment - interestIncome;

                        face = face * (1 - principalIncome);
                    }
                }
            }
            faceTable[this.numCashFlows] = 0;
        }

        public double GetSpread(string spreadFormula, DateTime date, IDiscountCurve discountCurve, ICouponExpressionEvaluator valorisationEvaluator, IFormulaEvaluator standardEvaluator)
        {
            if (valorisationEvaluator == null)
            {
                return standardEvaluator.Evaluate(spreadFormula, date);
            }
            else
            {
                return valorisationEvaluator.Evaluate(spreadFormula, date, discountCurve);
            }
        }

        // todo: mettre en cache
        private int CstTermAjustableDurationFace(double[] faceTable, DateTime evalDate, ICouponExpressionEvaluator valorisationEvaluator,
                                                    IFormulaEvaluator standardEvaluator, IDiscountCurveCollection discountCurves, bool lazyUpdate = false, double durationCap = int.MaxValue)
        {
            double targetTermPayment = 0;
            double face = 1;
            double finalProductDuration = 0;
            for (int j = 0; j < this.numCashFlows; j++)
            {
                if (face < Double.Epsilon)
                {
                    faceTable[j] = 0;

                    break;
                }
                else
                {
                    faceTable[j] = face;

                    Cashflow cashflow = this.interestCashflows[this.numCashFlows - j - 1];
                    if (lazyUpdate && j != 0 && cashflow.FixingDate <= evalDate.AddDays(1).AddMonths(-1).AddDays(-1))
                        face = faceTable[j + 1];
                    else
                    {

                        IDiscountCurve discountCurve = discountCurves.GetDiscountCurveAtDate(cashflow.FixingDate);
                        double spread = this.GetSpread(this.product.Spread, cashflow.FixingDate, discountCurve, valorisationEvaluator, standardEvaluator);
                        double rate = cashflow.GetCashflowValue(cashflow.FixingDate, spread, valorisationEvaluator, standardEvaluator, discountCurves);
                        double coupon = rate;

                        double duration;
                        if (j == 0)
                        {
                            duration = this.nonAdjustedNumCashFlows;
                            targetTermPayment = this.TermPayment(duration, face * coupon);
                        }
                        else
                        {
                            double cap = durationCap - j + 1;
                            duration = Math.Min(this.CstPaymentDuration(coupon, targetTermPayment, face), cap);
                        }

                        face = Math.Max(face * (1 - coupon * (1 / (1 - Math.Pow(1 + coupon, -duration)) - 1)), 0);
                        if (face < Double.Epsilon)
                            finalProductDuration = PricerUtil.DateDiffInSteps(cashflow.EndDate, evalDate, false) + 1;
                    }
                }
            }
            faceTable[this.numCashFlows] = 0;
            return (int)finalProductDuration;
        }

        private double TermPayment(double duration, double coupon)
        {
            double term = coupon/(1 - Math.Pow(1 + coupon, -duration));
            if (double.IsNaN(term))
                throw new Exception("CST_TERM error : TermPayement is NaN");
            return term;
        }

        private double CstPaymentDuration(double coupon, double termPayment, double face)
        {
            if (coupon / (termPayment / face) >= 1.0 - Double.Epsilon)
                return 999;

            return -Math.Log(1 - coupon / (termPayment / face)) / Math.Log(1 + coupon);
        }

        public double GetWeight(double[] face, Cashflow cashflow)
        {
            double weight;

            switch (cashflow.CashflowType)
            {
                case CashflowType.Interest:
                case CashflowType.Irc:
                    weight = face[cashflow.NumCashFlow];
                    break;

                case CashflowType.Principal:
                    double face1 = face[cashflow.NumCashFlow];
                    double face2 = face[cashflow.NumCashFlow + 1];
                    weight = face1 - face2;
                    break;

                default:

                    throw new ArgumentOutOfRangeException();
            }

            return this.product.IsReceived ? weight : -weight;
        }


        private void BuildCashflows()
        {
            int n = this.paymentDates.Count;

            for (int i = 0; i < n; i++)
            {

                DateTime prevDate = i == 0 && this.product.IssueDate != null
                                        ? this.product.IssueDate.GetValueOrDefault()
                                        : this.paymentDates[i].AddDays(1).AddMonths(-this.period).AddDays(-1);

                if (this.period == 0)
                    prevDate = this.startDate;

                string spread = this.product.Spread;
                string coupon = this.product.Coupon;
                CouponType couponType = this.product.CouponType;

                if (String.Equals(spread, String.Empty))
                    spread = "0";

                DateTime fixingDate = couponType == CouponType.Floating ? GetFixingDate(prevDate, this.paymentDates[i]) : this.startDate;
                if (fixingDate < this.startDate)
                {
                    fixingDate = this.startDate;
                    if (this.product.FirstRate != null)
                    {
                        couponType = CouponType.Fixed;
                        coupon = "" + this.product.FirstRate;
                    }
                }

                Cashflow cashflow = new Cashflow(this.paymentDates[i], fixingDate, /*prevDate,*/ prevDate,
                                                 this.paymentDates[i],
                                                 CashflowType.Interest, spread, coupon,
                                                 couponType, this.product.CouponBasis, this.product.CouponCalcType, i);

                this.cashflows.Add(cashflow);
                this.interestCashflows.Add(cashflow);

                if (i == n - 1 || (this.product.AmortizationType != ProductAmortizationType.BULLET && this.product.AmortizationType != ProductAmortizationType.NO_MATURITY))
                {
                    cashflow = new Cashflow(this.paymentDates[i], this.startDate, this.startDate,
                                            this.paymentDates[i],
                                            CashflowType.Principal, spread, coupon,
                                            couponType, this.product.CouponBasis, this.product.CouponCalcType, i);

                    this.cashflows.Add(cashflow);
                    this.principalCashflows.Add(cashflow);
                }
                ////build IRC cashflows
                //cashflow = new Cashflow(this.paymentDates[i], fixingDate, /*prevDate,*/ prevDate,
                //                                this.paymentDates[i],
                //                                CashflowType.Irc, "0", this.product.IRC,
                //                                couponType, this.product.CouponBasis, this.product.CouponCalcType, i);

                ////this.cashflows.Add(cashflow);
                //this.ircCashflows.Add(cashflow);

            }

            this.interestCashflows.Reverse();
            this.principalCashflows.Reverse();
            this.cashflows.Reverse();
            this.ircCashflows.Reverse();
        }

        public DateTime GetFixingDate(DateTime startDate, DateTime endDate)
        {
            switch (this.product.CouponCalcType)
            {
                case CouponCalcType.InAdvance:
                    return startDate;

                case CouponCalcType.InArrears:
                    return endDate;

                case CouponCalcType.Annually:
                    return GetPeriodEnd(startDate, 12);

                case CouponCalcType.Quarterly:
                    return GetPeriodEnd(startDate, 3);

                case CouponCalcType.Monthly:
                    return GetPeriodEnd(startDate, 1);

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public DateTime GetModelFixingDate(DateTime startDate, DateTime productStartDate)
        {
            DateTime firstMonthOfCouponPeriod = DateTime.Now;
            DateTime lastMonthOfCouponPeriod = DateTime.Now;
            for (int i = 0; i < this.paymentDates.Count; i++)
            {
                if (paymentDates[i] > startDate)
                {
                    if (i > 0)
                        firstMonthOfCouponPeriod = paymentDates[i - 1];
                    else
                        firstMonthOfCouponPeriod = productStartDate;
                    lastMonthOfCouponPeriod = paymentDates[i].AddDays(-1);
                    break;
                }
            }

            switch (this.product.CouponCalcType)
            {
                case CouponCalcType.InAdvance:
                    return firstMonthOfCouponPeriod;

                case CouponCalcType.InArrears:
                    return lastMonthOfCouponPeriod;

                case CouponCalcType.Annually:
                    return GetPeriodEnd(startDate, 12);

                case CouponCalcType.Quarterly:
                    return GetPeriodEnd(startDate, 3);

                case CouponCalcType.Monthly:
                    return GetPeriodEnd(startDate, 1);

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }


        private DateTime GetPeriodEnd(DateTime date, int months)
        {
            int delta = ((expiryDate.Year - date.Year) * 12 + (expiryDate.Month - date.Month)) % months;
            return delta == 0 ? date : date.AddDays(1).AddMonths(delta - months).AddDays(-1);
        }

        private static List<DateTime> GetPaymentDates(DateTime start, DateTime expiry, int period, IProduct product)
        {
            List<DateTime> dates = new List<DateTime>();

            DateTime date = expiry;
            DateTime realstart ;
            if (product.GetType()!=typeof(DiscountCurveBase.FundamentalVectorProduct))
                realstart=start.AddDays(1);
            else
                realstart=start.AddDays(-1);

            while (date > realstart)
            {
                dates.Add(date);

                if (period == 0) break;

                date = date.AddDays(1);
                date = date.AddMonths(-period);
                date = date.AddDays(-1);
                date =PricerUtil.LastDayOfMonth(date);
            }

            dates.Reverse();

            return dates;
        }


        public double ActuarialPrice(double[] face, DateTime date, double yield,
                                     ICouponExpressionEvaluator valorisationEvaluator,
                                     IFormulaEvaluator standardEvaluator, IDiscountCurveCollection discountCurves)
        {
            double sum = 0;


            foreach (Cashflow cashflow in this.principalCashflows)
            {
                if (date < cashflow.PaymentDate) //&& cashflow.CouponType != CouponType.Formula)
                {
                    double weight = this.GetWeight(face, cashflow);

                    double yieldDiscount = PricerUtil.YieldDiscount(date, cashflow.PaymentDate, yield);

                    sum += weight * yieldDiscount;
                }
                else break;
            }


            return double.IsNaN(sum) ? 0 : sum; // todo
        }


        public double MarketPrice(double[] face, DateTime date, double basis, double spread,
                                  ICouponExpressionEvaluator valorisationEvaluator, IFormulaEvaluator standardEvaluator,
                                  IDiscountCurveCollection discountCurves, bool cleanPrice = false)
        {
            double sum = 0;


            double b = (this.product.CouponType != CouponType.Floating) ? 0 : basis;

            double s = spread + ((this.product.CouponType != CouponType.Floating) ? -basis : 0);


            IDiscountCurve discountCurve = discountCurves.GetDiscountCurveAtDate(date);

            foreach (Cashflow cashflow in this.cashflows) //.nonFormulaCashflows)
            {
                if (date < cashflow.PaymentDate)
                {
                    double cashflowValue = cashflow.GetCashflowValue(date, s, valorisationEvaluator, standardEvaluator,
                                                                     discountCurves);

                    double marketDiscount = discountCurve.MarketDiscount(date, cashflow.PaymentDate, b,
                                                                         cashflow.CouponBasis, cashflow.NumCashFlow);

                    sum +=
                        this.GetWeight(face, cashflow) *
                        cashflowValue * marketDiscount;


                    if (double.IsNaN(sum))
                    {
                    }
                }

                else break;
            }

            if (cleanPrice)

                sum -= this.AccruedInterest(face, date, spread, valorisationEvaluator, standardEvaluator,
                                            discountCurves, basis);

            return double.IsNaN(sum) ? 0 : sum; // todo
        }

        public double MarketDuration(double[] face, DateTime date, double basis, double spread,
                                          ICouponExpressionEvaluator valorisationEvaluator, IFormulaEvaluator standardEvaluator,
                                          IDiscountCurveCollection discountCurves)
        {
            double sum = 0;

            double s = spread + ((this.product.CouponType != CouponType.Floating) ? -basis : 0);

            IDiscountCurve discountCurve = discountCurves.GetDiscountCurveAtDate(date);

            foreach (Cashflow cashflow in this.cashflows)
            {
                if (date < cashflow.PaymentDate)
                {
                    double cashflowValue = cashflow.GetCashflowValue(date, s, valorisationEvaluator, standardEvaluator,
                                                                        discountCurves);

                    double marketDuration = discountCurve.MarketDuration(cashflow.PaymentDate);

                    sum += this.GetWeight(face, cashflow) *
                        cashflowValue * marketDuration;
                }

                else break;
            }


            return double.IsNaN(sum) ? 0 : sum; // todo
        }

        public double GetFairFixedCoupon(double[] face, DateTime date, double basis,
                                         ICouponExpressionEvaluator valorisationEvaluator,
                                         IFormulaEvaluator standardEvaluator, IDiscountCurveCollection discountCurves)
        {
            double sumcoupon = 0;

            double sumprincipal = 0;


            IDiscountCurve discountCurve = discountCurves.GetDiscountCurveAtDate(date);

            foreach (Cashflow cashflow in this.Cashflows) //.nonFormulaCashflows)
            {
                if (date < cashflow.PaymentDate)
                {
                    double marketDiscount = discountCurve.MarketDiscount(this.startDate, cashflow.PaymentDate, basis,
                                                                         cashflow.CouponBasis, cashflow.NumCashFlow);

                    if (cashflow.CashflowType == CashflowType.Principal)

                        sumprincipal +=
                            this.GetWeight(face, cashflow) * marketDiscount;

                    else
                    {
                        sumcoupon +=
                            this.GetWeight(face, cashflow) * cashflow.Cvg * marketDiscount;


                        //accrued

                        if (date >= cashflow.StartDate && date < cashflow.EndDate)
                        {
                            sumcoupon -=
                                this.GetWeight(face, cashflow) *
                                DayCounter.GetCvg(cashflow.StartDate, date, cashflow.CouponBasis);
                        }
                    }
                }

                else break;
            }


            return (1.0 - sumprincipal) / sumcoupon; // todo
        }


        public double AccruedInterest(double[] face, DateTime date, double spread,
                                      ICouponExpressionEvaluator valorisationEvaluator,
                                      IFormulaEvaluator standardEvaluator, IDiscountCurveCollection discountCurves,
                                      double basis = 0)
        {
            double sum = 0;

            foreach (Cashflow cashflow in this.interestCashflows)
            {
                if (date >= cashflow.PaymentDate) break;

                if (date >= cashflow.StartDate && date < cashflow.EndDate)
                {
                    sum +=
                        this.GetWeight(face, cashflow) *
                        cashflow.GetCashflowValue(date, spread, valorisationEvaluator, standardEvaluator, discountCurves) *
                        DayCounter.GetCvg(cashflow.StartDate, date, cashflow.CouponBasis) /
                        DayCounter.GetCvg(cashflow.StartDate, cashflow.EndDate, cashflow.CouponBasis);
                }
            }


            return double.IsNaN(sum) ? 0 : sum; // todo
        }

        //public double GetRealInterestRate(double[] face, DateTime date1, DateTime date2, double spread,
        //                              ICouponExpressionEvaluator valorisationEvaluator,
        //                              IFormulaEvaluator standardEvaluator, IDiscountCurveCollection discountCurves,
        //                              double basis = 0)
        //{
        //    double sum = 0;

        //    foreach (Cashflow cashflow in this.interestCashflows)
        //    {
        //        if (date2 >= cashflow.StartDate && date1 < cashflow.EndDate)
        //        {
        //            sum +=
        //                this.GetWeight(face, cashflow) *
        //                cashflow.GetCashflowValue(date2, spread, valorisationEvaluator, standardEvaluator, discountCurves) *
        //                DayCounter.GetCvg(date1, date2, cashflow.CouponBasis) /
        //                DayCounter.GetCvg(cashflow.StartDate, cashflow.EndDate, cashflow.CouponBasis);
        //        }
        //    }


        //    return double.IsNaN(sum) ? 0 : sum; // todo
        //}


        public double GetFirstRateValue(DateTime date, double spread, ICouponExpressionEvaluator valorisationEvaluator,
                                          IFormulaEvaluator standardEvaluator, IDiscountCurveCollection discountCurves)
        {
            double sum = 0;

            foreach (Cashflow cashflow in this.interestCashflows)
            {
                if (date >= cashflow.PaymentDate) break;

                if (date >= cashflow.StartDate && date < cashflow.EndDate)
                {
                    sum += cashflow.GetCashflowValue(date, spread, valorisationEvaluator, standardEvaluator,
                                                     discountCurves);
                }
            }

            return double.IsNaN(sum) ? 0 : sum; // todo
        }


        public double InterestIncome(double[] face, DateTime date1, DateTime date2, double spread,
                                     ICouponExpressionEvaluator valorisationEvaluator,
                                     IFormulaEvaluator standardEvaluator, IDiscountCurveCollection discountCurves)
        {
            double sum = 0;


            foreach (Cashflow cashflow in this.interestCashflows)
            {
                if (date2 < cashflow.PaymentDate) continue;

                if (date1 < cashflow.PaymentDate)
                {
                    sum +=
                        this.GetWeight(face, cashflow) *
                        cashflow.GetCashflowValue(cashflow.FixingDate, spread, valorisationEvaluator, standardEvaluator,
                                                  discountCurves);
                }

                else break;
            }

            return double.IsNaN(sum) ? 0 : sum; // todo
        }
        //public double IRCValue(double[] face, DateTime date1, DateTime date2, double spread,
        //                     ICouponExpressionEvaluator valorisationEvaluator,
        //                     IFormulaEvaluator standardEvaluator, IDiscountCurveCollection discountCurves)
        //{
        //    double sum = 0;

        //    foreach (Cashflow cashflow in this.ircCashflows)
        //    {
        //        if (date2 < cashflow.PaymentDate) continue;

        //        if (date1 < cashflow.PaymentDate)
        //        {
        //            sum +=
        //                this.GetWeight(face, cashflow) *
        //                cashflow.GetCashflowValue(cashflow.FixingDate, 0, valorisationEvaluator, standardEvaluator,
        //                                          discountCurves);
        //        }

        //        else break;
        //    }

        //    return double.IsNaN(sum) ? 0 : sum; // todo
        //}

        public double PrincipalIncome(double[] face, DateTime date1, DateTime date2)
        {
            double sum = 0;

            foreach (Cashflow cashflow in this.principalCashflows)
            {
                if (date2 < cashflow.PaymentDate) continue;

                if (date1 < cashflow.PaymentDate)
                {
                    sum += this.GetWeight(face, cashflow);
                }

                else break;
            }

            return double.IsNaN(sum) ? 0 : sum; // todo
        }

        public double CurrentPrincipal(double[] face, DateTime date)
        {
            double sum = 0;

            foreach (Cashflow cashflow in this.principalCashflows)
            {
                if (date < cashflow.PaymentDate)
                {
                    sum += this.GetWeight(face, cashflow);
                }

                else break;
            }

            return sum;
        }

        public int GetLastCouponStep(DateTime date, DateTime valuationDate, ProductAttrib attrib)
        {
            IEnumerable<DateTime> dateTimes;

            dateTimes = isDaily ?
                this.paymentDates.Where(d => d < date) :
                this.paymentDates.Where(d => (d.Year * 12 + d.Month) < (date.Year * 12 + date.Month));

            DateTime lastCouponDate;
            if (dateTimes.Any())
            {
                lastCouponDate = dateTimes.Max();
            }
            else
            {
                return 0;
            }

            int steps = PricerUtil.DateDiffInSteps(lastCouponDate, valuationDate, isDaily);

            //if (attrib != ProductAttrib.InFuture) 
                steps++;
            
            return steps;
        }

        public bool IsCurrentStepCouponStep(DateTime date)
        {
            if (isDaily) return paymentDates.Any(d => d.Year == date.Year && d.DayOfYear == date.DayOfYear);

            foreach (DateTime payday in paymentDates)
            {
                if (payday.Month == date.Month && payday.Year == date.Year)
                    return true;
            }
            return false;
        }
    }
}