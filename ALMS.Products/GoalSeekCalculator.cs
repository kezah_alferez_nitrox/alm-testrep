using System;

namespace ALMS.Products
{
    // http://support.microsoft.com/kb/100782/en-us
    // http://blogs.msdn.com/b/lucabol/archive/2007/12/17/bisection-based-xirr-implementation-in-c.aspx

    public delegate double GoalFunction(double input);

    public static class GoalSeekCalculator
    {
        private const int MaxIterations = 100;

        private static Bracket FindBrackets(GoalFunction func, double guess)
        {
            // Abracadabra magic numbers ...
            const double bracketStep = 0.5;

            double leftBracket = guess - bracketStep;
            double rightBracket = guess + bracketStep;
            var iteration = 0;

            double fLeft = func(leftBracket);
            double fRight = func(rightBracket);

            while (!double.IsNaN(fLeft) && !double.IsNaN(fLeft) &&
                fLeft * fRight > 0 && iteration++ < MaxIterations)
            {
                double delta = bracketStep * Math.Pow(2, iteration / 2.0);
                leftBracket -= delta;
                rightBracket += delta;

                fLeft = func(leftBracket);
                fRight = func(rightBracket);
            }

            double fGuess = func(guess);
            if (!double.IsNaN(fGuess))
            {
                if (fRight * fGuess <= 0)
                {
                    fLeft = fGuess;
                    leftBracket = guess;
                }
                else if (fLeft * fGuess <= 0)
                {
                    fRight = fGuess;
                    rightBracket = guess;
                }
            }

            if (double.IsNaN(fLeft) || double.IsNaN(fLeft) || iteration >= MaxIterations)
                return new Bracket(0, 0);

            return new Bracket(leftBracket, rightBracket);
        }

        public static Result Bisection(GoalFunction func, Bracket bracket)
        {
            const double tolerance = 0.000001;

            int iter = 1;

            double f3;
            double x3;
            double x1 = bracket.Left;
            double x2 = bracket.Right;

            do
            {
                var f1 = func(x1);
                var f2 = func(x2);

                if (f1 == 0 && f2 == 0)
                    return new Result(ApproximateResultKind.NoSolutionWithinTolerance, x1);

                if (f1 == 0)
                {
                    return new Result(ApproximateResultKind.ExactSolution, x1);
                }

                if (f2 == 0)
                {
                    return new Result(ApproximateResultKind.ExactSolution, x2);
                }

                if (f1 * f2 > 0)
                    throw new ArgumentException("x1 x2 values don't bracket a root");

                x3 = (x1 + x2) / 2;
                f3 = func(x3);

                if (f3 * f1 < 0)
                    x2 = x3;
                else
                    x1 = x3;

                iter++;

            } while (Math.Abs(x1 - x2) / 2 > tolerance && f3 != 0 && iter < MaxIterations);

            //if (iter >0)
            //    Console.WriteLine("iter " + iter);

            if (f3 == 0)
                return new Result(ApproximateResultKind.ExactSolution, x3);

            if (Math.Abs(x1 - x2) / 2 < tolerance)
                return new Result(ApproximateResultKind.ApproximateSolution, x3);

            if (iter > MaxIterations)
                return new Result(ApproximateResultKind.NoSolutionWithinTolerance, x3);

            throw new Exception("It should never get here");
        }

        
        
        public static Result Brent(GoalFunction func, Bracket bracket)
        //Using Brent�s method, find the root of a function func known to lie between x1 and x2. The
        //root, returned as zbrent, will be refined until its accuracy is tol.
        {
	        int iter;
	        const int ITMAX = 100;
            const double DOUBLE_EPS = 1e-15;
            const double accuracy = 1e-12;

	        double a=bracket.Left,b=bracket.Right,c=bracket.Right,d=0,e=0,min1,min2;
	        double fa,fb,fc,p,q,r,s,tol1,xm;
            fa = func(bracket.Left);
            fb = func(bracket.Right);
	        //assert( fa * fb <= 0.0 );//Root must be bracketed
	        fc=fb;
	        for (iter=0;iter<ITMAX;iter++) {
		        if ((fb > 0.0 && fc > 0.0) || (fb < 0.0 && fc < 0.0)) {
			        c=a; //Rename a, b, c and adjust bounding interval
			        fc=fa; //d.
			        e=d=b-a;
		        }
		        if (Math.Abs(fc) < Math.Abs(fb)) {
			        a=b;
			        b=c;
			        c=a;
			        fa=fb;
			        fb=fc;
			        fc=fa;
		        }
		        tol1=4.0*DOUBLE_EPS*Math.Abs(b)+0.5*accuracy; //Convergence check.
		        xm=0.5*(c-b);
                if (Math.Abs(xm) <= tol1 || fb == 0.0)
                {
#if DEBUG
                    //if (iter>10)
                    //    Console.WriteLine("iter " + iter);
#endif
                    return new Result(ApproximateResultKind.ApproximateSolution, b);
		        }

		        if (Math.Abs(e) >= tol1 && Math.Abs(fa) > Math.Abs(fb)) {
			        s=fb/fa; //Attempt inverse quadratic interpolation.
			        if (a == c) {
				        p=2.0*xm*s;
				        q=1.0-s;
			        } else {
				        q=fa/fc;
				        r=fb/fc;
				        p=s*(2.0*xm*q*(q-r)-(b-a)*(r-1.0));
				        q=(q-1.0)*(r-1.0)*(s-1.0);
			        }
			        if (p > 0.0) q = -q; //Check whether in bounds.
			        p=Math.Abs(p);
			        min1=3.0*xm*q-Math.Abs(tol1*q);
			        min2=Math.Abs(e*q);
			        if (2.0*p < (min1 < min2 ? min1 : min2)) {
				        e=d; //Accept interpolation.
				        d=p/q;
			        } else {
				        d=xm; //Interpolation failed, use bisection.
				        e=d;
			        }
		        } else { //Bounds decreasing too slowly, use bisection.
			        d=xm;
			        e=d;
		        }
		        a=b; //Move last best guess to a.
		        fa=fb;
		        if (Math.Abs(d) > tol1) //Evaluate new trial root.
			        b += d;
		        else
			        b += (xm>=0.0)?Math.Abs(tol1):-Math.Abs(tol1);//sign(tol1,xm);
		        fb = func(b); 
		        //if (verbose) cout << "Coord " << coord_in << " - Iteration " << iter << " : " << fb * fb << endl;
	        }
	        return new Result(ApproximateResultKind.NoSolutionWithinTolerance, b);
        }


        public static Result Calculate(GoalFunction func, double guess)
        {
            var brackets = FindBrackets(func, guess);

            if (brackets.Left == brackets.Right)
                return new Result(ApproximateResultKind.NoSolutionWithinTolerance, brackets.Left);

            return Brent(func, brackets);
        }

        public static double GoalSeek(GoalFunction func, double guess)
        {
            Result result = Calculate(func, guess);

            return result.Kind == ApproximateResultKind.NoSolutionWithinTolerance ? double.NaN :
            result.Value;
        }

        public static double GoalSeek(GoalFunction func)
        {
            return GoalSeek(func, 0);
        }

        public struct Bracket
        {
            public readonly double Left;
            public readonly double Right;
            public Bracket(double left, double right)
            {
                this.Left = left; this.Right = right;
            }
        }

        public struct Result
        {
            public Result(ApproximateResultKind kind, double value)
            {
                Kind = kind;
                Value = value;
            }

            public readonly ApproximateResultKind Kind;
            public readonly double Value;
        }

        public enum ApproximateResultKind
        {
            ApproximateSolution,
            ExactSolution,
            NoSolutionWithinTolerance
        }
    }
}