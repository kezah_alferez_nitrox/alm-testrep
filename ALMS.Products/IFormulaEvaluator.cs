﻿using System;

namespace ALMS.Products
{
    public interface IFormulaEvaluator
    {
        double Evaluate(string formula, DateTime date);
    }
}