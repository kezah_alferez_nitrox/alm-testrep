using System;
using System.Linq;
using System.Collections.Generic;
using ALMSCommon.Threading;

namespace ALMS.Products
{
public class DiscountCurve : DiscountCurveBase
    {
        protected readonly IDictionary<string, object> variables;
        
        protected readonly IFormulaEvaluator formulaEvaluator;
        private readonly ICouponExpressionEvaluator valorisationEvaluator;
        private readonly IFormulaEvaluator standardEvaluator;

        public DiscountCurve(DateTime refDate, ICouponExpressionEvaluator valorisationEvaluator, IFormulaEvaluator standardEvaluator, IList<IFundamentalVector> fundamentalVectors, ThreadSafeDictionary<string, CashflowProduct> allCashFlowProduct)
            : base(refDate, fundamentalVectors, allCashFlowProduct, true)
        {
            this.valorisationEvaluator = valorisationEvaluator;
            this.standardEvaluator = standardEvaluator;
            //           this.ComputeZc();
        }

        public void ComputeZc(IDictionary<string, object> variables)
        {
            for (int i = 0; i < this.fundamentalVectors.Count; i++)
            {
                this.zc[i] = this.ComputeZc(i, variables);
            }

            if (zc.Any(x => double.IsNaN(x) || x < 0))
            {
                // todo ?! 
                // Debugger.Break();
            }
        }

        protected double ComputeZc(int index, IDictionary<string, object> variables)
        {
            IFundamentalVector fundamentalVector = this.fundamentalVectors[index];
            double coupon = (double)(double)variables[fundamentalVector.VariableName];
            if (fundamentalVector.Type == FundamentalVectorType.Libor)
            {
                //DateTime prevdate = index == 0 ? this.refDate : this.dates[index - 1];
                return this.ComputeLiborZc(fundamentalVector, this.refDate, this.dates[index], coupon);
            }
            else
            {
                return this.ComputeSwapZc(fundamentalVector, index, coupon);
            }
        }

        private double ComputeLiborZc(IFundamentalVector fundamentalVector, DateTime date1, DateTime date2, double coupon)
        {
            double cvg = DayCounter.GetCvg(date1, date2, fundamentalVector.DateConvention);
            double ti = (date2 - date1).TotalDays / PricerUtil.DaysPerYear;
            return Math.Exp(Math.Log(1.0 + coupon * cvg) / ti) - 1.0;
        }

        private double ComputeSwapZc(IFundamentalVector fundamentalVector, int index, double coupon)
        {
            //            double coupon = (double)(double)this.variables[fundamentalVector.VariableName];
            FundamentalVectorProduct fundamentalVectorProduct = new FundamentalVectorProduct(fundamentalVector, coupon);

            DiscountCurveBase goalSeekDiscountCurve = new DiscountCurveBase(this.refDate, this.fundamentalVectors, this.allCashFlowProduct );
            //            DiscountCurveCollection discountCurves = new DiscountCurveCollection(this.RefDate, new[] { goalSeekDiscountCurve });
            CashflowProduct cashflowProduct = new CashflowProduct(this.RefDate,
                                                                  fundamentalVectorProduct, false);
            double[] face = new double [cashflowProduct.NumCashFlows+1];
            cashflowProduct.CalculateFace(face, this.refDate, 0, null, null, null,startDate);             
            
            Array.Copy(this.zc, goalSeekDiscountCurve.Zc, this.zc.Length);

            double goalSeek =
                GoalSeekCalculator.GoalSeek(
                    goal => this.SwapZcGoalFunction(cashflowProduct, goalSeekDiscountCurve, face, index, goal), 0.5);

            return goalSeek;
        }


        private double SwapZcGoalFunction(CashflowProduct cashflowProduct, DiscountCurveBase goalSeekDiscountCurve,
                                   double[]face, int index, double goal)
        {
            goalSeekDiscountCurve.Zc[index] = goal;
            DiscountCurveCollection discountCurves = new DiscountCurveCollection(this.RefDate, new[] { goalSeekDiscountCurve });
            return cashflowProduct.MarketPrice(face, this.RefDate, 0, 0, this.valorisationEvaluator, this.standardEvaluator, discountCurves) - 1;
        }
    }      
  

    public enum TimeUnit
    {
        Days,
        Months,
        Years
    }
}