﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;
using System.Text;
using NCalc;
using NCalc.Domain;
using Sogalms.Products;

namespace BondManager.Calculations
{
    public class CouponExpressionEvaluator
    {
        protected string _expression;
        protected Dictionary<string, object> _variablesAndVectorValues;
        protected Dictionary<string, double> _variances;
        public CouponValorisationType Type { get; set; }

        public CouponExpressionEvaluator(string expression)
            : this(expression, new Dictionary<string, object>(), new Dictionary<string, double>())
        {
        }

        public CouponExpressionEvaluator(string expression, Dictionary<string, object> variableAndVectors, Dictionary<string, double> variances)
        {
            this._expression = expression;
            this._variablesAndVectorValues = variableAndVectors;
            this._variances = variances;
        }

        public double Evaluate()
        {
            return this.EvaluateExpression(Expression.Compile(this.GenerateParsedExpression(), false));
        }

        protected double EvaluateExpression(LogicalExpression expression)
        {
            if (expression is BinaryExpression)
            {
                BinaryExpression binaryExpression = (BinaryExpression)expression;
                LogicalExpression left = binaryExpression.LeftExpression;
                LogicalExpression right = binaryExpression.RightExpression;
                switch (binaryExpression.Type)
                {
                    case BinaryExpressionType.Plus:
                        return EvaluateExpression(left) + EvaluateExpression(right);
                    case BinaryExpressionType.Times:
                        return EvaluateExpression(left) * EvaluateExpression(right);
                    case BinaryExpressionType.Minus:
                        return EvaluateExpression(left) - EvaluateExpression(right);
                    case BinaryExpressionType.Div:
                        return EvaluateExpression(left) / this.EvaluateExpression(right);
                    default:
                        throw new InvalidOperationException("This kind of operation is not managed");
                }
            }
            else if (expression is Function)
            {
                Function function = (Function)expression;
                string name = function.Identifier.Name.ToLower();

                if (name == "min" || name == "max")
                {
                    if (function.Expressions.Length != 2)
                        throw new EvaluationException("This function must have two parameters");
                    LogicalExpression first = function.Expressions[0];
                    LogicalExpression second = function.Expressions[1];

                    if (name == "min") return this.Min(first, second);
                    else if (name == "max") return this.Max(first, second);
                }

                if (function.Expressions.Length != 1) throw new EvaluationException("This function must have one parameter");
                else
                {
                    if (function.Expressions[0] is Identifier && _variances[((Identifier)function.Expressions[0]).Name] != 0)
                        throw new EvaluationException("Variance must be null to evaluate function " + name);
                    foreach (MethodInfo method in typeof(Math).GetMethods())
                    {
                        if (method.Name.ToLower() != name || method.ReturnType != typeof(double)) continue;
                        if (method.GetParameters().Length != 1) continue;
                        return (double)method.Invoke(null, new object[] { this.EvaluateExpression(function.Expressions[0]) });
                    }
                }
                throw new EvaluationException("Unknow function : " + name);
            }
            else if (expression is Identifier)
            {
                Identifier identifier = (Identifier)expression;
                if (!_variablesAndVectorValues.ContainsKey(identifier.Name)) throw new EvaluationException("The value " + identifier.Name + " is not defined");
                return Convert.ToDouble(_variablesAndVectorValues[((Identifier)expression).Name], CultureInfo.InvariantCulture);
            }
            else if (expression is ValueExpression || expression is UnaryExpression)
            {
                return Convert.ToDouble(new Expression(expression).Evaluate());
            }
            else throw new InvalidOperationException("Invalid expression");
        }

        protected double Min(LogicalExpression first, LogicalExpression second)
        {
            if (second is ValueExpression)
            {
                if (first is ValueExpression)
                    return Math.Min(this.EvaluateExpression(second), this.EvaluateExpression(first));
                return this.Min(first, second);
            }
            if (first is ValueExpression && this.EvaluateExpression(first) == 0.0)
            {
                double secondValue = this.EvaluateExpression(second);
                if (second is Identifier)
                {
                    double secondVariance = this.GetExpressionVariance(second);
                    return this.GaussianMin(0, secondValue, Math.Sqrt(secondVariance));
                }
                return Math.Min(secondValue, 0);
            }
            return this.EvaluateExpression(Expression.Compile("min(0," + second.Minus(first) + ")", false).Plus(first));
        }

        protected double Max(LogicalExpression first, LogicalExpression second)
        {
            return this.Min(first.Mult(-1), second.Mult(-1)) * -1;
        }

        protected double GetExpressionVariance(LogicalExpression logicalExpression)
        {
            Expression expression = new Expression(logicalExpression);
            foreach (KeyValuePair<string, double> pair in _variances) expression.Parameters.Add(pair.Key, pair.Value);

            return Convert.ToDouble(expression.Evaluate());
        }

        protected string GenerateParsedExpression()
        {
            return this._expression.Replace("%", "/100");
        }

        protected double GaussianMin(double k, double mu, double sigma)
        {
            return mu / sigma + this.NormalMin((k - mu) / sigma);
            //double d1 = (Math.Log(mu / k) + sigma * sigma / 2) / sigma;
            //double d2 = (Math.Log(mu / k) - sigma * sigma / 2) / sigma;
            //return mu * this.Phi(d1) - k * this.Phi(d2);
        }

        protected double NormalMin(double a)
        {
            return 1 / Math.Sqrt(2 * Math.PI) * Math.Exp(-a * a / 2) + a * (1 - this.Phi(a));
        }

        protected double Phi(double x)
        {
            const double CDFa = 0.2316419;
            const double CDFb = 0.319381530;
            const double CDFc = -0.356563782;
            const double CDFd = 1.781477937;
            const double CDFe = -1.821255978;
            const double CDFf = 1.330274429;
            const double CDFh = 0.3989422;

            double k = 1.0 / (1.0 + CDFa * Math.Abs(x));

            if (x < -5.0) return 0.0;
            else if (Math.Abs(x) < 3.0e-14) return 0.5;
            else if (x > 5.0) return 1.0;
            else
            {
                double g = k * k;
                double temp = 1.0 - CDFh * Math.Exp(-x * x / 2) * (CDFb * k + CDFc * g + CDFd * g * k + CDFe * g * g + CDFf * g * g * k);
                if (x > 0.0) return temp;
                else return (1.0 - temp);
            }
        }

    }
}
