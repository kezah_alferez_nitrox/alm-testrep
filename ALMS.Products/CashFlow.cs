using System;

namespace ALMS.Products
{
    public class Cashflow
    {
        private readonly DateTime fixingDate;
        private readonly DateTime paymentDate;
        private readonly DateTime startDate;
        private readonly DateTime endDate;
        private readonly CashflowType cashflowType;
        private readonly CouponBasis couponBasis;
        private readonly CouponCalcType couponCalcType;
        private readonly string spread;
        private readonly string coupon;
        private readonly CouponType couponType;
        private readonly int numCashFlow;

        public Cashflow(DateTime paymentDate, DateTime fixingDate, DateTime startDate, DateTime endDate,
            CashflowType cashflowType, string spread, string coupon,
                        CouponType couponType, CouponBasis couponBasis, CouponCalcType couponCalcType, int numCashFlow)
        {
            this.paymentDate = paymentDate;
            this.couponBasis = couponBasis;
            this.couponCalcType = couponCalcType;
            this.spread = spread;
            this.coupon = coupon;
            this.couponType = couponType;
            this.cashflowType = cashflowType;
            this.endDate = endDate;
            this.startDate = startDate;
            this.fixingDate = fixingDate;
            this.numCashFlow = numCashFlow;
        }

        public DateTime PaymentDate
        {
            get { return this.paymentDate; }
        }

        public double GetCashflowValue(DateTime date, double spread, ICouponExpressionEvaluator valorisationEvaluator, IFormulaEvaluator standardEvaluator, IDiscountCurveCollection discountCurves)
        {
            if (this.CashflowType == CashflowType.Principal) return 1;

            DateTime calcDate = date < this.fixingDate ? date : this.fixingDate;

            IDiscountCurve discountCurve = discountCurves.GetDiscountCurveAtDate(calcDate);
            double couponValue = this.GetCouponValue(valorisationEvaluator, standardEvaluator, discountCurve);

            return (couponValue + spread) * Cvg;
        }

        public double GetCouponValue(ICouponExpressionEvaluator valorisationEvaluator, IFormulaEvaluator standardEvaluator, IDiscountCurve discountCurve)
        {
            DateTime date = this.fixingDate;

            return valorisationEvaluator == null ?
                standardEvaluator.Evaluate(this.coupon, date) :
                valorisationEvaluator.Evaluate(this.coupon, date, discountCurve);
        }

        public double Cvg
        {
            get
            {
                return DayCounter.GetCvg(this.StartDate, this.EndDate, this.CouponBasis);
            }
        }

        public CouponType CouponType
        {
            get { return this.couponType; }
        }

        public DateTime StartDate
        {
            get { return this.startDate; }
        }

        public DateTime EndDate
        {
            get { return this.endDate; }
        }

        public CashflowType CashflowType
        {
            get { return this.cashflowType; }
        }

        public CouponBasis CouponBasis
        {
            get { return this.couponBasis; }
        }

        public DateTime FixingDate
        {
            get { return this.fixingDate; }
        }

        public int NumCashFlow
        {
            get { return this.numCashFlow; }
        }

        public override string ToString()
        {
            return string.Format("{0}: {1:dd/MM/yyyy}-{2:dd/MM/yyyy}", this.cashflowType, this.startDate, this.endDate);
        }
    }
}