﻿using System;
using NHibernate;

namespace ALMS.Products
{
    public interface IProduct
    {

        ProductAmortizationType AmortizationType { get; }
        //string Amortization { get; }
        int LagNoAmortisationPeriodInMonths{get; }
        int ALMID { get; }
        string ComplementString { get; }
        CouponBasis CouponBasis { get; }
        string Coupon { get; }
        string IRC { get; }
        double? FirstRate { get; }
        ICurrency Currency { get; }
        ProductFrequency Frequency { get; }
        bool IsReceived { get; }
        CouponType CouponType { get; }
        CouponCalcType CouponCalcType { get; }
        string Spread { get; }
        bool IsDerivative { get; }
        int IntDuration { get; set; }
        ProductAttrib Attrib { get; set; }
        
        

        DateTime? IssueDate { get; }

        DateTime GetExpiryDate(DateTime valuationDate);

        double GetFace(DateTime date);

        
    }
}