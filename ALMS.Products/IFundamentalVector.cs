﻿namespace ALMS.Products
{
    public interface IFundamentalVector 
    {
        string VariableName { get; }
        TimeUnit Unit { get; }
        int Delta { get; }
        ProductFrequency Frequency { get; }
        CouponBasis DateConvention { get;}        
        FundamentalVectorType Type { get;}
        double[] VolatilityValues { get; }
    }
}