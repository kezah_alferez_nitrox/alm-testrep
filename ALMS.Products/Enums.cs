﻿using System;
using Eu.AlterSystems.ASNetLib.Core;

namespace ALMS.Products
{
    public enum ProductAmortizationType
    {
        [EnumDescription("BULLET")]
        BULLET = 0,
        [EnumDescription("LINEAR AMORT")]
        LINEAR_AMORT = 2,
        [EnumDescription("CST PAY")]
        CST_PAY = 3,
        [EnumDescription("VAR %")]
        VAR = 4,
        [EnumDescription("VAR NOMINAL")]
        VAR_NOMINAL = 5,
        [EnumDescription("CST TERM")]
        CST_TERM = 6,
        [EnumDescription("ADJUSTABLE TERM")]
        ADJUSTABLE_TERM= 7,
        [EnumDescription("ADJUSTABLE CAPPED TERM")]
        ADJUSTABLE_CAPPED_TERM = 8,
        [EnumDescription("RESERVE CASHFLOW")]
        RESERVE_CASHFLOW = 10,
        [EnumDescription("RESERVE NOMINAL")]
        RESERVE_NOMINAL = 11,
        [EnumDescription("NO MATURITY")]
        NO_MATURITY = 9,
    }

    public enum ProductFrequency
    {
        [EnumDescription("Monthly")]
        Monthly = 12,
        [EnumDescription("Quarterly")]
        Quarterly = 4,
        [EnumDescription("Semi Annual")]
        SemiAnnual = 2,
        [EnumDescription("Annual")]
        Annual = 1,
        [EnumDescription("At Maturity")]
        AtMaturity = 0,
    }

    public enum CouponCalcType
    {
        [EnumDescription("In Advance")]
        InAdvance,
        [EnumDescription("In Arrears")]
        InArrears,
        [EnumDescription("Annually")]
        Annually,
        [EnumDescription("Quarterly")]
        Quarterly,
        [EnumDescription("Monthly")]
        Monthly,
    }

    public enum CouponBasis
    {
        ACT360, A30360, ACT365, ACTACT, CONT,
        ISMA30360
    }

    public enum CashflowType
    {
        Principal,
        Interest,
        Irc
    }

    public enum CouponType
    {
        Fixed,
        Floating,
        Variable,
    }

    public enum FundamentalVectorType
    {
        Libor,
        Swap
    }

    public enum CouponValorisationType
    {
        Constante,
        Variable
    }

    public enum VariableType
    {
        Number,
        Vector,
        Formula,
        Array
    }

    [Obsolete]
    public enum OldProductAmortizationType
    {
        [EnumDescription("BULLET")]
        BULLET,
        [EnumDescription("GROWTH")]
        GROWTH,
        [EnumDescription("LINEAR AMORT")]
        LINEAR_AMORT,
        [EnumDescription("CST PAY")]
        CST_PAY,
        [EnumDescription("VAR")]
        VAR,
    }

    public enum ProductAccounting
    {
        [EnumDescription("L&R")]
        LAndR,
        [EnumDescription("HTM")]
        HTM,
        [EnumDescription("AFS")]
        AFS,
        [EnumDescription("HFT")]
        HFT,
        [EnumDescription("Other")]
        Other
    }

    public enum ProductAttrib
    {
        [EnumDescription("In Balance")]
        InBalance,
        [EnumDescription("Notional")]
        Notional,
        [EnumDescription("Roll")]
        Roll,
        [EnumDescription("Cash Adj")]
        CashAdj,
        [EnumDescription("Treasury")]
        Treasury,
        [EnumDescription("Impairments")]
        Impairments,
        [EnumDescription("Receivable")]
        Receivable,
        [EnumDescription("Reserves")]
        Reserves,
        [EnumDescription("Other Comprehensive Income")]
        OtherComprehensiveIncome,
        [EnumDescription("Positive Derivatives Valuation")]
        PositiveDerivativesValuation,
        [EnumDescription("Negative Derivatives Valuation")]
        NegativeDerivativesValuation,
        [EnumDescription("Payable")]
        Payable,
        [EnumDescription("Interest Received On Derivatives")]
        InterestReceivedOnDerivatives,
        [EnumDescription("Interest Payed On Derivatives")]
        InterestPayedOnDerivatives,
        [EnumDescription("In Future")]
        InFuture,
        [EnumDescription("IBNR")]
        IBNR,
        [EnumDescription("Margin Call Paid")]
        MarginCallPaid,
        [EnumDescription("Margin Call Received")]
        MarginCallReceived,
        [EnumDescription("Model Impairments")]
        ModelImpairments

    }

    public enum ProductTax
    {
        [EnumDescription("Normal")]
        Standard,
        [EnumDescription("Non Taxable")]
        NonTaxable,
        [EnumDescription("Long Term")]
        LongTerm,
        [EnumDescription("Special")]
        Special,
    }

    public enum ProductInvestmentRule
    {
        Constant,
        Growth,
        Var,
        Roll
    }

    public enum ProductType
    {
        //MTU : default=<void> and doing the same thing as till today, simple bond. 2 new types : BondFr and BondVr
        [EnumDescription("")]
        Bond = 0,
        [EnumDescription("Fixed Rate")]
        BondFr = 1,
        [EnumDescription("Variable Rate")]
        BondVr = 2,
        [EnumDescription("Swap")]
        Swap = 3,
        [EnumDescription("Cap")]
        Cap = 4,
        [EnumDescription("Floor")]
        Floor = 5,
        [EnumDescription("Stock Equity")]
        StockEquity = 6,
        [EnumDescription("Custom")]
        Custom = 7,
        [EnumDescription("Swaption")]
        Swaption = 8,
        [EnumDescription("Amortizing")]
        Amortizing = 9,

    }

    public enum BondSwapLeg
    {
        [EnumDescription("Received")]
        Received = 0,
        [EnumDescription("Paid")]
        Paid = 1,

        ReceivedFixed = 0,
        PayFixed = 1,
        ReceivedFloat = 0,
        PayFloat = 1
    }

    public enum LiquidityEligibility
    {
        [EnumDescription("Not eligible")]
        NotEligible,
        [EnumDescription("Level 1")]
        Level1,
        [EnumDescription("Level 2A")]
        Level2,
        [EnumDescription("Level 2B")]
        Level2B,
    }

    public enum MonthsOfYear
    {
        January = 1,
        February,
        March,
        April,
        May,
        June,
        July,
        August,
        September,
        October,
        November,
        December,
    }
}
