﻿using System;

namespace ALMS.Products
{
    public class DiscountCurveCollection : IDiscountCurveCollection
    {
        private readonly IDiscountCurve[] discountCurves;
        private DateTime valuationDate;

        public DiscountCurveCollection(DateTime valuationDate, IDiscountCurve[] discountCurves)
        {
            this.valuationDate = valuationDate;
            this.discountCurves = discountCurves;
        }

        public IDiscountCurve[] DiscountCurves
        {
            get { return this.discountCurves; }
        }

        public IDiscountCurve GetDiscountCurveAtDate(DateTime date)
        {
            int discountCurveCount = this.DiscountCurves.Length;
            DateTime date0 = this.valuationDate.AddDays(-1);
            int index = (date.Year * 12 + date.Month - date0.Year * 12 - date0.Month);

            if (index < 0) index = 0;
            if (index >= discountCurveCount) 
                index = discountCurveCount - 1;

            return this.DiscountCurves[index];
        }
    }
}