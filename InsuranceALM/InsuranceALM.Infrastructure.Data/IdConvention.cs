using FluentNHibernate.Conventions;
using FluentNHibernate.Conventions.Instances;

namespace InsuranceALM.Infrastructure.Data
{
    public class IdConvention : IIdConvention
    {
        public void Apply(IIdentityInstance instance)
        {
            if (typeof(IGenericEntity<int>).IsAssignableFrom(instance.EntityType))
            {
                instance.GeneratedBy.HiLo("1000");
            }
        }
    }
}
