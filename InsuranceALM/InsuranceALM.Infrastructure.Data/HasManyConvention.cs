using FluentNHibernate.Conventions;
using FluentNHibernate.Conventions.Instances;

namespace InsuranceALM.Infrastructure.Data
{
    public class HasManyConvention : IHasManyConvention
    {
        public void Apply(IOneToManyCollectionInstance instance)
        {
            instance.LazyLoad();
            instance.Cascade.AllDeleteOrphan();
            instance.Inverse();
            instance.BatchSize(20);
        }
    }
}