﻿using FluentNHibernate.Mapping;

namespace InsuranceALM.Infrastructure.Data
{
    public class EntityMap<T, TId> : ClassMap<T> where T : IGenericEntity<TId>
    {
        protected EntityMap()
        {
            this.Id(m => m.Id);
            this.Not.LazyLoad();
        }
    }
}