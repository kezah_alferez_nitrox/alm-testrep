﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using NHibernate.Driver;
using NHibernate.SqlTypes;

namespace InsuranceBondManager.Domain
{
    public class ImageSqlServerCeDriver : SqlServerCeDriver
    {
        protected override void InitializeParameter(IDbDataParameter dbParam, string name, SqlType sqlType)
        {
            base.InitializeParameter(dbParam, name, sqlType);

            if (sqlType is BinarySqlType)
            {
                PropertyInfo dbParamSqlDbTypeProperty = dbParam.GetType().GetProperty("SqlDbType");

                dbParamSqlDbTypeProperty.SetValue(dbParam, SqlDbType.Image, null);
            }
        }
    }
}
