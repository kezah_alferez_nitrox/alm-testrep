using FluentNHibernate.Conventions;
using FluentNHibernate.Conventions.Instances;

namespace InsuranceALM.Infrastructure.Data
{
    public class PropertyConvention : IPropertyConvention
    {
        public void Apply(IPropertyInstance instance)
        {
            if (instance.Type == typeof(string))
            {
                instance.Length(4000);
            }
        }
    }
}
