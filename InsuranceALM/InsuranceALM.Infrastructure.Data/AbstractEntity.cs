using System;
using System.Xml.Serialization;

namespace InsuranceALM.Infrastructure.Data
{
    [Serializable]
    public abstract class AbstractEntity<TIdentity> : IGenericEntity<TIdentity>
    {
        private int? requestedHashCode;

        #region IGenericEntity<TIdentity> Members

        public virtual TIdentity Id { get; set; }

        /// <summary>
        /// Compare equality trough Id
        /// </summary>
        /// <param name="other">Entity to compare.</param>
        /// <returns>true is are equals</returns>
        /// <remarks>
        /// Two entities are equals if they are of the same hierarcy tree/sub-tree
        /// and has same id.
        /// </remarks>
        public virtual bool Equals(IGenericEntity<TIdentity> other)
        {
            if (null == other || !this.GetType().IsInstanceOfType(other))
            {
                return false;
            }
            if (ReferenceEquals(this, other))
            {
                return true;
            }

            bool otherIsTransient = Equals(other.Id, default(TIdentity));
            bool thisIsTransient = this.IsTransient();
            if (otherIsTransient && thisIsTransient)
            {
                return ReferenceEquals(other, this);
            }

            return other.Id.Equals(this.Id);
        }

        #endregion

        protected bool IsTransient()
        {
            return Equals(this.Id, default(TIdentity));
        }

        public override bool Equals(object obj)
        {
            var that = obj as IGenericEntity<TIdentity>;
            return this.Equals(that);
        }

        public static bool operator ==(AbstractEntity<TIdentity> left, AbstractEntity<TIdentity> right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(AbstractEntity<TIdentity> left, AbstractEntity<TIdentity> right)
        {
            return !Equals(left, right);
        }

        public override int GetHashCode()
        {
            if (!this.requestedHashCode.HasValue)
            {
                this.requestedHashCode = this.IsTransient() ? base.GetHashCode() : this.Id.GetHashCode();
            }
            return this.requestedHashCode.Value;
        }
    }
}
