﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankALM.Infrastructure.Data;
using NHibernate;
using NUnit.Framework;

namespace BondManager.Domain.Test
{
    [TestFixture]
    public class CurrencyTests : DbTestBase
    {
        [Test]
        public void CreateAndFindOne()
        {
            using (ISession session = SessionManager.OpenSession())
            using (ITransaction transaction = session.BeginTransaction())
            {
                Currency currency = new Currency();
                currency.Symbol = "EUR";
                currency.Create(session);
                transaction.Commit();
            }

            using (ISession session = SessionManager.OpenSession())
            {
                Currency currency = Currency.FindOne(session);

                Currency findFirstBySymbol = Currency.FindFirstBySymbol(session, "EUR");

                Assert.IsNotNull(currency);
                Assert.IsNotNull(findFirstBySymbol);
            }
        }

        [Test]
        public void CreateAndFindAll()
        {
            const int currenciesCount = 10;

            using (ISession session = SessionManager.OpenSession())
            using (ITransaction transaction = session.BeginTransaction())
            {
                for (int i = 0; i < currenciesCount; i++)
                {
                    Currency currency = new Currency();
                    currency.Create(session);
                }
                transaction.Commit();
            }

            using (ISession session = SessionManager.OpenSession())
            {
                Currency[] currencies = Currency.FindAll(session);
                Assert.AreEqual(currenciesCount, currencies.Length);
            }
        }

        [Test]
        public void CreateAndFindById()
        {
            const int currenciesCount = 10;

            using (ISession session = SessionManager.OpenSession())
            using (ITransaction transaction = session.BeginTransaction())
            {
                for (int i = 0; i < currenciesCount; i++)
                {
                    Currency currency = new Currency();
                    currency.Create(session);
                }
                transaction.Commit();
            }

            using (ISession session = SessionManager.OpenSession())
            {
                int id = Currency.FindFirst(session).Id;
                Currency currency = Currency.Find(session, id);
                Assert.NotNull(currency);
            }
        }
        [Test]
        public void CreateAndFindAllByProperty()
        {
            const int currenciesCount = 10;

            using (ISession session = SessionManager.OpenSession())
            using (ITransaction transaction = session.BeginTransaction())
            {
                for (int i = 0; i < currenciesCount; i++)
                {
                    Currency currency = new Currency();
                    currency.ShortName = i % 2 == 0 ? "pair" : "impair";
                    currency.Create(session);
                }
                transaction.Commit();
            }

            using (ISession session = SessionManager.OpenSession())
            {
                Currency[] currencies = Currency.FindAllByProperty(session, "ShortName", "pair");
                Assert.AreEqual(currenciesCount / 2, currencies.Length);
            }
        }

        [Test]
        public void CreateAndDelete()
        {
            using (ISession session = SessionManager.OpenSession())
            using (ITransaction transaction = session.BeginTransaction())
            {
                Currency currency = new Currency();
                currency.Create(session);
                transaction.Commit();
            }

            int id;
            using (ISession session = SessionManager.OpenSession())
            {
                Currency currency = Currency.FindOne(session);
                Assert.IsNotNull(currency);
                id = currency.Id;
                currency.Delete(session);
                session.Flush();
                currency = Currency.Find(session, id);
                Assert.IsNull(currency);
            }
        }

        [Test]
        public void CreateSaveAndDelete()
        {
            using (ISession session = SessionManager.OpenSession())
            {
                Currency currency = new Currency();
                currency.ShortName = "save";
                currency.Save(session);
                session.Flush();
                currency = Currency.Find(session, currency.Id);
                Assert.IsNotNull(currency);
                Assert.AreEqual(currency.ShortName, "save");
            }
        }

        [Test]
        public void CreateAndUpdate()
        {
            using (ISession session = SessionManager.OpenSession())
            {
                Currency currency = new Currency();
                currency.Create(session);
                currency.ShortName = "update";
                currency.Update(session);
                session.Flush();
                currency = Currency.Find(session, currency.Id);
                Assert.IsNotNull(currency);
                Assert.AreEqual(currency.ShortName, "update");
            }
        }

        [Test]
        public void CreateAndSaveCopy()
        {
            Currency currency = new Currency();

            using (ISession session = SessionManager.OpenSession())
            {
                currency.ShortName = "savecopy";
                currency.SaveCopy(session);
                session.Flush();
                currency = Currency.Find(session, currency.Id);
                Assert.IsNull(currency);
                Currency[] currencies = Currency.FindAllByProperty(session, "ShortName", "savecopy");
                Assert.AreEqual(currencies.Length, 1);
            }
        }

        [Test]
        public void CreateAndDeleteAll()
        {
            const int currenciesCount = 10;

            using (ISession session = SessionManager.OpenSession())
            {
                for (int i = 0; i < currenciesCount; i++)
                {
                    Currency currency = new Currency();
                    currency.Save(session);
                }
                session.Flush();
                Currency[] currencies = Currency.FindAll(session);
                Assert.LessOrEqual(currenciesCount, currencies.Length);
                
                Currency.DeleteAll(session);
                session.Flush();
                currencies = Currency.FindAll(session);
                Assert.AreEqual(currencies.Length, 0);
            }
        }
    }
}
