using BankALM.Infrastructure.Data;
using NHibernate;
using NUnit.Framework;

namespace BondManager.Domain.Test
{
    [TestFixture]
    public class CategoryTests : DbTestBase
    {
        [Test]
        public void CreateAndFindOne()
        {
            using (ISession session = SessionManager.OpenSession())
            using (ITransaction transaction = session.BeginTransaction())
            {
                Category category = new Category();
                category.Create(session);
                transaction.Commit();
            }

            using (ISession session = SessionManager.OpenSession())
            {
                Category category = Category.FindOne(session);
                Assert.IsNotNull(category);
            }
        }

        [Test]
        public void CreateWithChildAndFindFirst()
        {
            using (ISession session = SessionManager.OpenSession())
            using (ITransaction transaction = session.BeginTransaction())
            {
                Category parent = new Category();
                Category enfant = new Category();
                enfant.Parent = parent;
                parent.Children.Add(enfant);
                parent.Create(session);
                transaction.Commit();
            }

            using (ISession session = SessionManager.OpenSession())
            {
                Category category = Category.FindFirst(session);
                Assert.IsNotNull(category);
                Assert.IsNotNull(category.Children.Count == 1);
            }
        }

        [Test]
        public void CreateWithProducts()
        {
            using (ISession session = SessionManager.OpenSession())
            using (ITransaction transaction = session.BeginTransaction())
            {
                Category category = new Category();
                Product product = new Product();
                product.Category = category;
                category.Bonds.Add(product);
                category.Create(session);
                transaction.Commit();
            }

            using (ISession session = SessionManager.OpenSession())
            {
                Category category = Category.FindFirst(session);
                Assert.IsNotNull(category);
                Assert.IsNotNull(category.Bonds.Count == 1);
            }
        }

        [Test]
        public void CreateAndFindAll()
        {
            const int categoriesCount = 10;

            using (ISession session = SessionManager.OpenSession())
            using (ITransaction transaction = session.BeginTransaction())
            {
                for (int i = 0; i < categoriesCount; i++)
                {
                    Category category = new Category();
                    category.Create(session);
                }
                transaction.Commit();
            }

            using (ISession session = SessionManager.OpenSession())
            {
                Category[] categories = Category.FindAll(session);
                Assert.AreEqual(categoriesCount, categories.Length);
            }
        }

        [Test]
        public void CreateAndFindById()
        {
            const int categoriesCount = 10;

            using (ISession session = SessionManager.OpenSession())
            using (ITransaction transaction = session.BeginTransaction())
            {
                for (int i = 0; i < categoriesCount; i++)
                {
                    Category category = new Category();
                    category.Label = i.ToString();
                    category.Create(session);
                }
                transaction.Commit();
            }

            using (ISession session = SessionManager.OpenSession())
            {
                int id = Category.FindFirst(session).Id;
                Category category = Category.Find(session, id);
                Assert.NotNull(category);
            }
        }
        [Test]
        public void CreateAndFindAllByProperty()
        {
            const int categoriesCount = 10;

            using (ISession session = SessionManager.OpenSession())
            using (ITransaction transaction = session.BeginTransaction())
            {
                for (int i = 0; i < categoriesCount; i++)
                {
                    Category category = new Category();
                    category.Label = i % 2 == 0 ? "pair" : "impair";
                    category.Create(session);
                }
                transaction.Commit();
            }

            using (ISession session = SessionManager.OpenSession())
            {
                Category[] categories = Category.FindAllByProperty(session, "Label", "pair");
                Assert.AreEqual(categoriesCount / 2, categories.Length);
            }
        }

        [Test]
        public void CreateAndDelete()
        {
            using (ISession session = SessionManager.OpenSession())
            using (ITransaction transaction = session.BeginTransaction())
            {
                Category category = new Category();
                category.Create(session);
                transaction.Commit();
            }

            int id;
            using (ISession session = SessionManager.OpenSession())
            {
                Category category = Category.FindOne(session);
                Assert.IsNotNull(category);
                id = category.Id;
                category.Delete(session);
                session.Flush();
                category = Category.Find(session, id);
                Assert.IsNull(category);
            }
        }

        [Test]
        public void CreateSaveAndDelete()
        {
            using (ISession session = SessionManager.OpenSession())
            {
                Category category = new Category();
                category.Label = "save";
                category.Save(session);
                session.Flush();
                category = Category.Find(session, category.Id);
                Assert.IsNotNull(category);
                Assert.AreEqual(category.Label, "save");
            }
        }

        [Test]
        public void CreateAndUpdate()
        {
            using (ISession session = SessionManager.OpenSession())
            {
                Category category = new Category();
                category.Create(session);
                category.Label = "update";
                category.Update(session);
                session.Flush();
                category = Category.Find(session, category.Id);
                Assert.IsNotNull(category);
                Assert.AreEqual(category.Label, "update");
            }
        }

        [Test]
        public void CreateAndSaveCopy()
        {
            Category category = new Category();

            using (ISession session = SessionManager.OpenSession())
            {
                category.Label = "savecopy";
                category.SaveCopy(session);
                session.Flush();
                category = Category.Find(session, category.Id);
                Assert.IsNull(category);
                Category[] categories = Category.FindAllByProperty(session, "Label", "savecopy");
                Assert.AreEqual(categories.Length, 1);
            }
        }

        [Test]
        public void CreateAndDeleteAll()
        {
            const int categoriesCount = 10;

            using (ISession session = SessionManager.OpenSession())
            {
                for (int i = 0; i < categoriesCount;i++ )
                {
                    Category category = new Category();
                    category.Save(session);
                }
                session.Flush();
                Category[] categories = Category.FindAll(session);
                Assert.LessOrEqual(categoriesCount, categories.Length);
                Category.DeleteAll(session);
                session.Flush();
                categories = Category.FindAll(session);
                Assert.AreEqual(categories.Length, 0);
            }
        }

        [Test]
        public void CreateChildren()
        {
            int childId;
            int parentId;
            using (ISession session = SessionManager.OpenSession())
            using (ITransaction transaction = session.BeginTransaction())
            {
                Category parent = new Category();
                Category child = new Category();
                //parent.Children.Add(child);
                child.Parent = parent;
                session.Save(parent);
                session.Save(child);

                transaction.Commit();

                parentId = parent.Id;
                childId = child.Id;
            }

            using (ISession session = SessionManager.OpenSession())
            {
                Category parent = Category.Find(session, parentId);
                Assert.IsNotNull(parent);
                Assert.AreEqual(1, parent.Children.Count);

                Category child = Category.Find(session, childId);                
                Assert.IsNotNull(child);
                Assert.IsNotNull(child.Parent);
                // Assert.ReferenceEquals(parent, child.Parent);
            }
        }
    }
}