using System;
using System.Xml.Serialization;
using InsuranceALM.Infrastructure.Data;

namespace InsuranceBondManager.Domain
{
    // [ActiveRecord(Lazy = false)]
    [Serializable]
    [XmlRoot]
    public class Dividend : EntityBase<Dividend, int>
    {
        private string name;
        private string amount;
        private double? minimum;

        // [Property]
        [XmlElement]
        public virtual string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }

        // [Property]
        [XmlElement]
        public virtual string Amount
        {
            get { return this.amount; }
            set { this.amount = value; }
        }

        // [Property]
        [XmlElement]
        public virtual double? Minimum
        {
            get { return this.minimum; }
            set { this.minimum = value; }
        }

        public virtual bool IsValueAmount
        {
            get
            {
                double temp;
                return double.TryParse(this.Amount, out temp);
            }
        }

        public virtual bool Equals(Dividend obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.Id == this.Id && Equals(obj.Name, this.Name);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof(Dividend)) return false;
            return this.Equals((Dividend)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (this.Id * 397) ^ (this.Name != null ? this.Name.GetHashCode() : 0);
            }
        }


    }


}