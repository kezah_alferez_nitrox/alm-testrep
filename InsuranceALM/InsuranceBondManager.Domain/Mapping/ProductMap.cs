﻿using InsuranceALM.Infrastructure.Data;

namespace InsuranceBondManager.Domain.Mapping
{
    // [Property] -> Map
    // [BelongsTo] -> References
    // [HasMany] -> HasMany
    public class ProductMap : EntityMap<Product, int>
    {
        public ProductMap()
        {
            this.Not.LazyLoad();

            this.Id(e => e.Id);

            this.Map(e => e.Position);
            this.Map(e => e.Exclude);
            this.Map(e => e.Status);
            this.Map(e => e.SecId);
            this.Map(e => e.Description);
            this.Map(e => e.AmortizationType);
            this.Map(e => e.Amortization);
            this.Map(e => e.InvestmentParameter);
            this.Map(e => e.Accounting);
            this.Map(e => e.OrigFace);
            this.Map(e => e.BookPrice);
            this.Map(e => e.ActuarialPrice);
            this.Map(e => e.BookValue);
            this.Map(e => e.ALMID);
            this.Map(e => e.RollSpec);
            this.Map(e => e.RollRatio);
            this.Map(e => e.StartDate);
            this.Map(e => e.Coupon);
            this.Map(e => e.CouponType);
            this.Map(e => e.Attrib).Index("idx_product_attrib");
            this.Map(e => e.IsModel);
            this.Map(e => e.CDR);
            this.Map(e => e.CPR);
            //this.Map(e => e.OriginBasedOnStartDate);
            this.Map(e => e.TheoreticalCurrFace);
            this.Map(e => e.LGD);
            this.Map(e => e.RecoveryLag);
            this.Map(e => e.Basel2);
            this.Map(e => e.Frequency);
            this.Map(e => e.Duration);
            this.Map(e => e.ExpirationDate);
            this.Map(e => e.Tax);
            this.Map(e => e.IRC);
            this.Map(e => e.IsVisibleChecked);
            this.Map(e => e.CouponBasis);
            this.Map(e => e.InvestmentRule);
            this.Map(e => e.ProductType);
            this.Map(e => e.SwapLeg);
            this.Map(e => e.Spread);
            this.Map(e => e.MarketBasis);
            this.Map(e => e.FirstRate);
            this.Map(e => e.LiquidityEligibility);
            this.Map(e => e.LiquidityHaircut);
            this.Map(e => e.LiquidityRunOff);
            this.Map(e => e.CouponCalcType);
            this.Map(e => e.ComplementString);
            this.Map(e => e.ApplicationFee);
            this.Map(e => e.ApplicationCharge);
            this.Map(e => e.PrePayementFee);


            this.References(e => e.Category).Index("idx_product_category"); ;
            this.References(e => e.Currency).Index("idx_product_currency"); ;
            this.References(e => e.OtherSwapLeg);

            this.HasManyToMany(e => e.LinkedProducts).
                Table("LinkedProducts").
                ParentKeyColumn("Product1").
                ChildKeyColumn("Product2").
                Cascade.SaveUpdate();
        }
    }
}