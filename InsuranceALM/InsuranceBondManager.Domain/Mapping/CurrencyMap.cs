﻿using InsuranceALM.Infrastructure.Data;

namespace InsuranceBondManager.Domain.Mapping
{
    public class CurrencyMap : EntityMap<Currency, int>
    {
        public CurrencyMap()
        {
            this.Id(e => e.Id);
            this.Map(e => e.ShortName);
            this.Map(e => e.Symbol);
        }
    }
}