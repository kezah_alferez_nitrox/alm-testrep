﻿using InsuranceALM.Infrastructure.Data;

namespace InsuranceBondManager.Domain.Mapping
{
    // [Property] -> Map
    // [BelongsTo] -> References
    // [HasMany] -> HasMany
    public class VariableValueMap : EntityMap<VariableValue, int>
    {
        public VariableValueMap()
        {
            this.Not.LazyLoad();

            this.Id(e => e.Id);

            this.Map(e => e.Value).CustomType("StringClob");
            this.Map(e => e.Generated);

            this.References(e => e.Variable).UniqueKey("UniqueVariableScenario");
            this.References(e => e.Scenario).UniqueKey("UniqueVariableScenario");

        }
    }
}