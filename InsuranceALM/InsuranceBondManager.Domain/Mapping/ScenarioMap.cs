﻿using InsuranceALM.Infrastructure.Data;

namespace InsuranceBondManager.Domain.Mapping
{
    // [Property] -> Map
    // [BelongsTo] -> References
    // [HasMany] -> HasMany
    public class ScenarioMap : EntityMap<Scenario, int>
    {
        public ScenarioMap()
        {
            this.Not.LazyLoad();

            this.Id(e => e.Id);

            this.Map(e => e.Position);
            this.Map(e => e.Name).Unique();
            this.Map(e => e.Selected);
            this.Map(e => e.Generated);
            this.Map(e => e.IsDefault);
            this.Map(e => e.Description);

            this.HasMany(e => e.VariableValues);
        }
    }
}