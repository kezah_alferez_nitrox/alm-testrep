﻿using InsuranceALM.Infrastructure.Data;

namespace InsuranceBondManager.Domain.Mapping
{
    // [Property] -> Map
    // [BelongsTo] -> References
    // [HasMany] -> HasMany
    public class VariableMap : EntityMap<Variable, int>
    {
        public VariableMap()
        {
            this.Not.LazyLoad();

            this.Id(e => e.Id);

            this.Map(e => e.Name).Unique();
            this.Map(e => e.CreationDate);
            this.Map(e => e.Selected);
            this.Map(e => e.VariableType);

            this.HasMany(e => e.VariableValues);
        }
    }
}