﻿using InsuranceALM.Infrastructure.Data;

namespace InsuranceBondManager.Domain.Mapping
{
    // [Property] -> Map
    // [BelongsTo] -> References
    // [HasMany] -> HasMany
    public class SolvencyAdjustmentMap : EntityMap<SolvencyAdjustment, int>
    {
        public SolvencyAdjustmentMap()
        {
            this.Id(e => e.Id);
            this.Map(e => e.Description);
            this.Map(e => e.Vector);
        }
    }
}