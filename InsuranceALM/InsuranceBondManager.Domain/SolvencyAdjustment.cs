using System;
using System.Xml.Serialization;
using InsuranceALM.Infrastructure.Data;
using NHibernate;

namespace InsuranceBondManager.Domain
{
    // [ActiveRecord(Lazy = false)]
    [Serializable]
    [XmlRoot]
    public class SolvencyAdjustment : EntityBase<SolvencyAdjustment, int>
    {

        public static class Types
        {
            public const string RWAAdjustment = "RWA adjustment";
            public const string CoreEquityAdjustment = "Core Equity Adjustment";
            public const string OtherTierOneAdjustment = "Other Tier One Adjustment";
            public const string Tiers2Adjustment = "Tiers 2 Adjustment";
        }

        //[Property]
        public virtual string Description { get; set; }

        //[Property]
        public virtual string Vector { get; set; }

        public virtual double[] Values { get; set; }

        public virtual bool Equals(SolvencyAdjustment other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return other.Id == this.Id;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof(SolvencyAdjustment)) return false;
            return this.Equals((SolvencyAdjustment)obj);
        }

        public override int GetHashCode()
        {
            return this.Id;
        }

        public static void EnsureExist()
        {
            using (ISession session = SessionManager.OpenSession())
            {
                SolvencyAdjustment[] adjustments = FindAll(session);
                if (adjustments.Length < 4)
                {
                    DeleteAll(session);

                    (new SolvencyAdjustment {Id = 1, Description = Types.RWAAdjustment, Vector = "0"}).Create(session);
                    (new SolvencyAdjustment {Id = 2, Description = Types.CoreEquityAdjustment, Vector = "0"}).Create(
                        session);
                    (new SolvencyAdjustment {Id = 3, Description = Types.OtherTierOneAdjustment, Vector = "0"}).Create(
                        session);
                    (new SolvencyAdjustment {Id = 4, Description = Types.Tiers2Adjustment, Vector = "0"}).Create(session);
                }
                session.Flush();
            }
        }
        
    }




}