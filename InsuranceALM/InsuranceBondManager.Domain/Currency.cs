using System;
using System.Linq;
using System.Xml.Serialization;
using System.Collections.Generic;
using ALMS.Products;
using InsuranceALM.Infrastructure.Data;
using NHibernate;
using NHibernate.Linq;

namespace InsuranceBondManager.Domain
{
    //[ActiveRecord]
    [Serializable]
    [XmlRoot]
    public class Currency : EntityBase<Currency, int>, ICurrency
    {
        private string shortName;
        private string symbol;

        //[Property]
        [XmlElement]
        public virtual string ShortName
        {
            get { return this.shortName; }
            set { this.shortName = value; }
        }

        //[Property]
        [XmlElement]
        public virtual string Symbol
        {
            get { return this.symbol; }
            set { this.symbol = value; }
        }

        public virtual Currency Self
        {
            get { return this; }
        }

        public static Currency FindFirstBySymbol(ISession session, string s)
        {
            return s == null
                       ? session.Query<Currency>().FirstOrDefault(c => c.Symbol == null)
                       : session.Query<Currency>().FirstOrDefault(c => c.Symbol == s);
            //return s == null ? FindFirst(Expression.IsNull("Symbol")) : FindFirst(Expression.Eq("Symbol", s));
        }

        private static IDictionary<string, Currency> cacheByShortName = null;
        public static Currency FindOrCreateByShortName(ISession session, string shortName)
        {
            if (cacheByShortName == null)
            {
                cacheByShortName = new SortedDictionary<string, Currency>();

                Currency[] all = FindAll(session);
                foreach (Currency currency in all)
                {
                    string cachedShortName = currency.ShortName ?? "";
                    if (!cacheByShortName.ContainsKey(cachedShortName)) cacheByShortName.Add(cachedShortName, currency);
                }
            }

            shortName = shortName ?? "";
            if (!cacheByShortName.ContainsKey(shortName))
            {
                Currency currency = new Currency();
                currency.ShortName = currency.Symbol = shortName;
                currency.Create(session);
                cacheByShortName.Add(shortName, currency);
            }

            return cacheByShortName[shortName];
        }

        public static Currency[] FindAllNotEmpty(ISession session)
        {
            return session.Query<Currency>().Where(c => c.Symbol != null).ToArray();
        }

        public override string ToString()
        {
            return this.ShortName;
        }
    }
}
