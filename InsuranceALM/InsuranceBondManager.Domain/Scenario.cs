using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using InsuranceALM.Infrastructure.Data;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Linq;

namespace InsuranceBondManager.Domain
{
    // [ActiveRecord(Lazy = false)]
    [Serializable]
    [XmlRoot]
    public class Scenario : EntityBase<Scenario, int>
    {
        private int position;
        private string name;
        private bool selected;
        private bool generated;
        private bool isDefault;
        private string description;
        private IList<VariableValue> variableValues = new List<VariableValue>();


        //[Property]
        [XmlElement]
        public virtual int Position
        {
            get { return this.position; }
            set { this.position = value; }
        }

        //[Property(Unique = true)]
        [XmlElement]
        public virtual string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }

        //[Property]
        [XmlElement]
        public virtual bool Selected
        {
            get { return this.selected; }
            set { this.selected = value; }
        }

        //[Property]
        [XmlElement]
        public virtual bool Generated
        {
            get { return this.generated; }
            set { this.generated = value; }
        }

        //[Property]
        [XmlElement]
        public virtual bool IsDefault
        {
            get { return this.isDefault; }
            set { this.isDefault = value; }
        }

        //[Property]
        [XmlElement]
        public virtual string Description
        {
            get { return this.description; }
            set { this.description = value; }
        }

        //[HasMany(Lazy = true, Inverse = true, Cascade = ManyRelationCascadeEnum.AllDeleteOrphan, BatchSize = 10)]
        [XmlIgnore]
        public virtual IList<VariableValue> VariableValues
        {
            get { return this.variableValues; }
            set { this.variableValues = value; }
        }

        public virtual Scenario Self
        {
            get { return this; }
        }

        public override string ToString()
        {
            return this.Name;
        }

        public static Scenario[] FindAvailable(ISession session)
        {
            return session.Query<Scenario>().Where(s => !s.Generated && !s.Selected).OrderBy(s => s.Position).ToArray();
            //return FindAll(session, Order.Asc("Position"), Expression.And(
            //    Expression.Eq("Generated", false),
            //    Expression.Eq("Selected", false)));
        }

        public static Scenario[] FindSelected(ISession session)
        {
            //return FindAll(session, Order.Asc("Position"), Expression.And(
            //    Expression.Eq("Generated", false),
            //    Expression.Eq("Selected", true)));
            return session.Query<Scenario>().Where(s => !s.Generated && s.Selected).OrderBy(s => s.Position).ToArray();
        }

        public static Scenario[] FindNonGenerated(ISession session)
        {
            //return FindAll(session, Order.Asc("Position"), Expression.Eq("Generated", false));
            return session.Query<Scenario>().Where(s => !s.Generated).OrderBy(s => s.Position).ToArray();
        }

        public static Scenario[] FindGenerated(ISession session)
        {
            //return FindAll(session, Order.Asc("Position"), Expression.Eq("Generated", true));

            return session.Query<Scenario>().Where(s => s.Generated).OrderBy(s => s.Position).ToArray();
        }

        public static Scenario FindDefault(ISession session)
        {
            return
                session.Query<Scenario>().FirstOrDefault(s => s.IsDefault);
            //return FindFirst(session, Expression.Eq("IsDefault", true));
        }

        public static Scenario FindByName(ISession session, string name)
        {
            return
                session.Query<Scenario>().FirstOrDefault(s => !s.Generated && s.Name == name);
            //return FindFirst(session, Expression.And(
            //    Expression.Eq("Generated", false),
            //    Expression.Eq("Name", name)));
        }

        public static bool ExistsByName(ISession session, string scenarioName)
        {
            return session.Query<Scenario>().Where(s => !s.Generated && s.Name == scenarioName).Count() > 0;
//            return Exists(session, "Generated=0 And Name = ?", scenarioName);
        }

        public static void DeleteGenerated(ISession session)
        {
            int query = ExecuteDelete(session, "from Scenario s where s.Generated=1");
        }

        public virtual bool Equals(Scenario obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return Equals(obj.name, this.name);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof(Scenario)) return false;
            return this.Equals((Scenario)obj);
        }

        public override int GetHashCode()
        {
            return (this.name != null ? this.name.GetHashCode() : 0);
        }

        public static bool operator ==(Scenario left, Scenario right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Scenario left, Scenario right)
        {
            return !Equals(left, right);
        }

        public static int GeneratedCount(ISession session)
        {
            // return Count(session, Expression.Eq("Generated", true));

            return session.Query<Scenario>().Where(s => s.Generated).Count();
        }
        
    }




}