using System;
using System.Linq;
using System.Xml.Serialization;
using InsuranceALM.Infrastructure.Data;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Linq;

namespace InsuranceBondManager.Domain
{
    // [ActiveRecord(Lazy = false)]
    [Serializable]
    [XmlRoot]
    public class OtherItemAssum : EntityBase<OtherItemAssum, int>
    {
        public const string CostGroup = "Cost";
        public const string CommisionsAndOthersGroup = "Commissions & Others";
        public const string ImpairmentsRisk = "Impairments";

        private string descrip;

        private bool? isVisibleChecked;
        private string subtotalGroup;
        private string status;
        private string vect;
        private double[] values;
        private int position;
        private bool cannotDelete = false;

        // [Property]
        [XmlElement]
        public virtual string Descrip
        {
            get { return this.descrip; }
            set { this.descrip = value; }
        }

        //[Property]
        [XmlElement]
        public virtual string Vect
        {
            get { return this.vect; }
            set { this.vect = value; }
        }

        [XmlIgnore]
        public virtual double[] Values
        {
            get { return this.values; }
            set { this.values = value; }
        }

        //[Property]
        [XmlElement]
        public virtual bool? IsVisibleChecked
        {
            get { return this.isVisibleChecked; }
            set { this.isVisibleChecked = value; }
        }

        //[Property]
        [XmlElement]
        public virtual string SubtotalGroup
        {
            get { return this.subtotalGroup; }
            set { this.subtotalGroup = value; }
        }

        [Obsolete]
        public string Status
        {
            get { return this.status; }
            set { this.status = value; }
        }

        //[Property]
        [XmlElement]
        public virtual int Position
        {
            get { return this.position; }
            set { this.position = value; }
        }
        
        [XmlElement]
        public bool CannotDelete
        {
            get { return this.cannotDelete; }
            set { this.cannotDelete = value; }
        }


        public virtual bool Equals(OtherItemAssum obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.Id == this.Id && Equals(obj.descrip, this.descrip);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof(OtherItemAssum)) return false;
            return this.Equals((OtherItemAssum)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (this.Id * 397) ^ (this.descrip != null ? this.descrip.GetHashCode() : 0);
            }
        }

        public static bool operator ==(OtherItemAssum left, OtherItemAssum right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(OtherItemAssum left, OtherItemAssum right)
        {
            return !Equals(left, right);
        }

        public static OtherItemAssum[] FindDataByStatus(ISession session, string stat)
        {
            return
                session.Query<OtherItemAssum>().Where(oia => oia.Status == stat).OrderBy(oia => oia.Position).ToArray();
            //return FindAllByProperty(session, "Position", "Status", stat);
        }

        public new static OtherItemAssum[] FindAll(ISession session)
        {
            //return FindAll(session, Order.Asc("Position"));
            return session.Query<OtherItemAssum>().OrderBy(oia => oia.Position).ToArray();
        }

        public static OtherItemAssum[] FindBySubtotalGroup(ISession session, string subtotalGroup)
        {
            return
                session.Query<OtherItemAssum>().Where(oia => oia.SubtotalGroup == subtotalGroup).OrderBy(oia => oia.Position).ToArray();
            //return FindAllByProperty(session, "Position", "SubtotalGroup", subtotalGroup);
        }

        public static OtherItemAssum FindByID(ISession session, int id)
        {
            return session.Query<OtherItemAssum>().FirstOrDefault(oia => oia.Id == id);
            //return FindOne(session, Expression.Eq("Id", id));
        }

        public static string[] GetAllSubgroups(ISession session)
        {
            return session.Query<OtherItemAssum>().Select(oia => oia.SubtotalGroup).Distinct().ToArray();
        }
    }
}