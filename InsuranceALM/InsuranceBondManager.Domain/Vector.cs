using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using InsuranceALM.Infrastructure.Data;
using Eu.AlterSystems.ASNetLib.Core;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Linq;

namespace InsuranceBondManager.Domain
{
    [Serializable]
    [XmlRoot]
    public class Vector : EntityBase<Vector, int>
    {
        public Vector()
        {
            this.VectorPoints = new List<VectorPoint>();
        }

        [XmlElement]
        public virtual string Name { get; set; }

        [XmlElement]
        public virtual VectorGroup Group { get; set; }

        [XmlElement]
        public virtual DateTime? CreationDate { get; set; }

        [XmlElement]
        public virtual VectorFrequency Frequency { get; set; }

        //[XmlElement]
        //public virtual VectorOriginReference OriginReference { get; set; }

        [XmlIgnore]
        public virtual IList<VectorPoint> VectorPoints { get; set; }

        [XmlElement]
        public VectorType VectorType { get; set; }

        [XmlElement]
        public virtual bool ImportantMarketData { get; set; }

        [XmlElement]
        public int Position { get; set; }

        public virtual Vector Self
        {
            get { return this; }
        }

        public virtual bool Equals(Vector other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return other.Id == this.Id;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof(Vector)) return false;
            return this.Equals((Vector)obj);
        }

        public override int GetHashCode()
        {
            return this.Id;
        }

        public override string ToString()
        {
            return this.Name;
        }

        public static bool operator ==(Vector left, Vector right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Vector left, Vector right)
        {
            return !Equals(left, right);
        }

        public virtual Vector Copy()
        {
            Vector vectorCopy = new Vector();
            vectorCopy.Name = Name;
            vectorCopy.Group = Group;
            vectorCopy.Frequency = Frequency;
            vectorCopy.VectorType = VectorType;
            foreach (VectorPoint point in this.VectorPoints)
            {
                VectorPoint vectorPointCopy = point.Copy();
                vectorPointCopy.Vector = vectorCopy;
                vectorCopy.VectorPoints.Add(vectorPointCopy);
            }

            return vectorCopy;
        }

        public static Vector[] FindByGroup(ISession session, VectorGroup group)
        {
            return session.Query<Vector>().Where(v => v.Group == group).ToArray();
        }

        public static Vector[] FindByImportantMarketData(ISession session)
        {
            return session.Query<Vector>().Where(v => v.ImportantMarketData).ToArray();
        }

        public static Vector FindByName(ISession session, string name)
        {
            return session.Query<Vector>().FirstOrDefault(v => v.Name == name);
        }

        public static Vector[] FindNames(ISession session, string[] names)
        {
            Vector[] vectors = session.Query<Vector>().Where(v => names.Contains(v.Name)).ToArray();
            return vectors;
        }

        public static Vector[] FindLikeName(ISession session, string name)
        {
            return FindAll(session, Restrictions.Like("Name", name, MatchMode.Start));
        }

        public static Vector FindById(ISession session, int id)
        {
            return session.Query<Vector>().FirstOrDefault(v => v.Id == id);
        }

        public static Func<DateTime, int, DateTime> GetIncrementFunc(VectorFrequency frequency)
        {
            Func<DateTime, int, DateTime> func;
            switch (frequency)
            {
                case VectorFrequency.None:
                    func = (d, c) => d.AddMonths(c * 1); // todo ?!
                    break;
                case VectorFrequency.Daily:
                    func = (d, c) => d.AddDays(c);
                    break;
                case VectorFrequency.Monthly:
                    func = (d, c) => d.AddMonths(c * 1);
                    break;
                case VectorFrequency.Quaterly:
                    func = (d, c) => d.AddMonths(c * 3);
                    break;
                case VectorFrequency.SemiAnnual:
                    func = (d, c) => d.AddMonths(c * 6);
                    break;
                case VectorFrequency.Annual:
                    func = (d, c) => d.AddMonths(c * 12);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            return func;
        }

        public virtual Func<DateTime, int, DateTime> GetIncrementFunc()
        {
            return GetIncrementFunc(this.Frequency);
        }

        public virtual VectorPoint GetCurrentValue(ISession session, DateTime date)
        {
            date = date.Date;
            Func<DateTime, int, DateTime> func = this.GetIncrementFunc();
            DateTime dateMin = func(date, -1);
            DateTime dateMax = date.AddDays(1);

            VectorPoint point = session.Query<VectorPoint>().
                FirstOrDefault(vp => vp.Vector == this && vp.Date >= dateMin && vp.Date < dateMax);
            return point;
        }

    }

    public enum VectorType
    {
        Percentage,
        Value
    }


    public enum VectorOriginReference
    {
        [EnumDescription("Valuation Date (absolue value)")]
        ValuationDate,
        [EnumDescription("Start Date (per product relative value)")]
        StartDate
    }

    public enum VectorGroup
    {
        Workspace,
        MarketData,
        MarketVol,
        [EnumDescription("Customer Data Vectors")]
        CustomerData
    }

    public enum VectorFrequency
    {
        None,
        Daily,
        Monthly,
        Quaterly,
        SemiAnnual,
        Annual,
    }

}