using System;
using System.Linq;
using System.Xml.Serialization;
using InsuranceALM.Infrastructure.Data;
using NHibernate;
using NHibernate.Linq;

namespace InsuranceBondManager.Domain
{
    // [ActiveRecord(Lazy = false)]
    [Serializable]
    [XmlRoot]
    public class VectorPoint : EntityBase<VectorPoint, int>
    {
        private Vector vector;
        private DateTime date = DateTime.Now;
        private double value;


        //[BelongsTo]
        [XmlElement] // TODO ajouter index
        public virtual Vector Vector
        {
            get
            {
                return this.vector;
            }
            set
            {
                this.vector = value;
            }
        }

        //[Property]
        [XmlElement]
        public virtual DateTime Date
        {
            get
            {
                return this.date;
            }
            set
            {
                this.date = value;
            }
        }

        //[Property]
        [XmlElement]
        public virtual double Value
        {
            get
            {
                return this.value;
            }
            set
            {
                this.value = value;
            }
        }

        public virtual VectorPoint Copy()
        {
            VectorPoint newVectorPoint = new VectorPoint();
            newVectorPoint.Date = this.Date;
            newVectorPoint.Value = this.Value;

            return newVectorPoint;
        }

        public static VectorPoint[] FindByVectorGroup(ISession session, VectorGroup group)
        {
            return session.Query<VectorPoint>().Where(vp => vp.Vector.Group==group).ToArray();
//            SimpleQuery<VectorPoint> query = new SimpleQuery<VectorPoint>(
//                @"from VectorPoint vp
//                    where
//                        vp.Vector.Group = :group
//                ");

//            query.SetParameter("group", group);
//            VectorPoint[] vectorPoints = query.Execute();

//            return vectorPoints;

        }

        public static void DeleteByGroup(ISession session, VectorGroup group)
        {
            foreach (Vector vector in Vector.FindByGroup(session, group))
            {
                foreach (VectorPoint vectorpoint in vector.VectorPoints)
                    vectorpoint.Delete(session);
            }
        }

        public static VectorPoint[] FindByVector(ISession session,  Vector vector)
        {
            return FindAllByProperty(session, "Vector", vector);
        }

        public static VectorPoint[] FindByDate(ISession session,  DateTime date)
        {
            return FindAllByProperty(session, "Date", date);
        }

        public static VectorPoint FindByMonthYear(int year, int month, Vector vector)
        {
            if (vector == null) return null;

            foreach (VectorPoint _vp in vector.VectorPoints)
            {
                if (_vp.date.Year == year && _vp.date.Month == month)
                    return _vp;
            }
            return null;
        }
        
    }



}