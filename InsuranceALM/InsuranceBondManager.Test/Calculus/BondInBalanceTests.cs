using BondManager.Calculus;
using BondManager.Domain;
using NUnit.Framework;
using Sogalms.Products;

namespace BondManager.Test.Calculus
{
    [TestFixture]
    public class BondInBalanceTests
    {
        private const int CurrFace = 100;
        private const int Duration = 10;

        private Product product;
        private IAlgorithmConfiguration algorithms;
        private BondData simulationData;

        [SetUp]
        public void SetUp()
        {
            this.product = new Product();
            this.product.Attrib = ProductAttrib.InBalance;
            this.product.OrigFace = CurrFace.ToString();
            this.product.Duration = Duration;

            algorithms = new AlgorithmConfiguration();
            // simulationData = new BondData(Product, algorithms);
        }

        [TearDown]
        public void TearDown()
        {
            this.product = null;
            algorithms = null;
            simulationData = null;
        }

        [Test, Ignore]
        public void InBalance_Bullet_Test()
        {
            this.product.AmortizationType = ProductAmortizationType.BULLET;

            // simulationData.Run(new Scenario());

            Assert.AreEqual(Param.ResultMonths, simulationData.CurrFace.Length);
            Assert.AreEqual(CurrFace, simulationData.CurrFace[0]);
            Assert.AreEqual(CurrFace, simulationData.CurrFace[Duration]);
            Assert.AreEqual(0, simulationData.CurrFace[Duration + 1]);
            Assert.AreEqual(0, simulationData.CurrFace[Param.ResultMonths - 1]);
        }
    }
}