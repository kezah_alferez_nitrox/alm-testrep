using System;
using System.Linq;
using BondManager.Generator;
using NUnit.Framework;

namespace BondManager.Test.Generator
{
    [TestFixture]
    public class RandomGeneratorsTest
    {
        private const int Size = 100000;
        private const double Delta = 0.01;

        private static double[] GenerateVector(int size, Func<double> random)
        {
            double[] result = new double[size];
            for (int i = 0; i < size; i++)
            {
                result[i] = random();
            }

            return result;
        }

        [Test]
        public void DefaultRandomTest()
        {
            TestUniformDistribution(new DefaultRandom());
        }

        [Test]
        public void MersenneTwisterTest()
        {
            TestUniformDistribution(new MersenneTwister());
        }

        [Test]
        public void BoxMullerTest()
        {
            TestNormalDistribution(new BoxMuller());
            TestNormalDistribution(new BoxMuller(new MersenneTwister()));
        }

        [Test]
        public void ZigguratTest()
        {
            TestNormalDistribution(new Ziggurat());
            TestNormalDistribution(new Ziggurat(new MersenneTwister()));
        }

        private static void TestUniformDistribution(IUniformRandom random)
        {
            double[] vector = GenerateVector(Size, random.NextDouble);

            double stdev = Statistics.StandardDeviation(vector);
            double average = vector.Average();

            Assert.AreEqual(1/Math.Sqrt(12), stdev, Delta);
            Assert.AreEqual(0.5, average, Delta);
        }

        private static void TestNormalDistribution(INormalRandom random)
        {
            double[] vector = GenerateVector(Size, random.Next);

            double stdev = Statistics.StandardDeviation(vector);
            double average = vector.Average();

            Assert.AreEqual(1, stdev, Delta);
            Assert.AreEqual(0, average, Delta);
        }
    }
}