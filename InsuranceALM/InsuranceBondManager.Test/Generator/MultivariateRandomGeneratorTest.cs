using System.Diagnostics;
using BondManager.Generator;
using NUnit.Framework;

namespace BondManager.Test.Generator
{
    [TestFixture]
    public class MultivariateRandomGeneratorTest
    {
        private const double Delta = 0.01;
        private const int VectorSize = 120;
        private const int Iterations = 10000;

        private double[] means;
        private double[] stdev;
        private double[,] correlations;

        [SetUp]
        public void Setup()
        {
            this.means = new double[] { 10, 20, 30 };
            this.stdev = new double[] { 1, 2, 3 };
            this.correlations = new[,]
                {
                    {1.0, 0.6, 0.3},
                    {0.6, 1.0, 0.5},
                    {0.3, 0.5, 1.0}
                };
        }

        [Test]
        public void PositiveDefiniteTest()
        {
            Assert.IsTrue(MatrixHelper.IsPositiveDefinite(new[,]
                {
                    {1.0, 0.6, 0.3},
                    {0.6, 1.0, 0.5},
                    {0.3, 0.5, 1.0}
                }));

            Assert.IsFalse(MatrixHelper.IsPositiveDefinite(new[,]
                {
                    {1.0, 1.0, 1.0},
                    {1.0, 1.0, 1.0},
                    {1.0, 1.0, 1.0}
                }));

            Assert.IsFalse(MatrixHelper.IsPositiveDefinite(new[,]
                {
                    {0.0, 0.0, 0.0},
                    {0.0, 0.0, 0.0},
                    {0.0, 0.0, 0.0},
                }));
        }

        [Test, Explicit]
        public void SpeedTest()
        {
            this.GenerateCorrelationAndResults(10);
            this.GenerateCorrelationAndResults(100);
            this.GenerateCorrelationAndResults(200);
        }

        private void GenerateCorrelationAndResults(int variableCount)
        {
            MersenneTwister uniformRandom = new MersenneTwister();
            MultivariateRandomGenerator generator = new MultivariateRandomGenerator(new BoxMuller(new MersenneTwister()));

            const int sampleCount = 20;
            double[,] input = new double[variableCount, sampleCount];

            for (int i = 0; i < variableCount; i++)
            {
                for (int j = 0; j < sampleCount; j++)
                {
                    input[i, j] = uniformRandom.NextDouble();
                }
            }

            this.correlations = MatrixHelper.CorrelationMatrix(input);

            this.stdev = new double[variableCount];
            double[][] meansArray = new double[variableCount][];
            for (int i = 0; i < variableCount; i++)
            {
                this.stdev[i] = uniformRandom.NextDouble() * 100;
                meansArray[i] = new double[VectorSize];
            }

            Stopwatch s = Stopwatch.StartNew();
            //generator.GenerateMultivariateNormals(VectorSize, 1.0/12, meansArray, this.stdev, this.correlations);
            s.Stop();

            Debug.WriteLine("Generate " + variableCount + " Multivariate Normals : " + s.ElapsedMilliseconds + "ms");
        }

        [Test]
        public void Ziggurat_MersenneTwister_Test()
        {
            Stopwatch s = Stopwatch.StartNew();
            this.TestByGenerator(new MultivariateRandomGenerator(new Ziggurat(new MersenneTwister())));
            Debug.WriteLine(s.ElapsedMilliseconds);
        }

        [Test]
        public void Ziggurat_DefaultRandom_Test()
        {
            Stopwatch s = Stopwatch.StartNew();
            this.TestByGenerator(new MultivariateRandomGenerator(new Ziggurat(new DefaultRandom())));
            Debug.WriteLine(s.ElapsedMilliseconds);
        }

        [Test]
        public void BoxMuller_MersenneTwister_Test()
        {
            Stopwatch s = Stopwatch.StartNew();
            this.TestByGenerator(new MultivariateRandomGenerator(new BoxMuller(new MersenneTwister())));
            Debug.WriteLine(s.ElapsedMilliseconds);
        }

        [Test]
        public void BoxMuller_DefaultRandom_Test()
        {
            Stopwatch s = Stopwatch.StartNew();
            this.TestByGenerator(new MultivariateRandomGenerator(new BoxMuller(new DefaultRandom())));
            Debug.WriteLine(s.ElapsedMilliseconds);
        }

        private void TestByGenerator(MultivariateRandomGenerator generator)
        {
            double[][,] resultCorrelations = new double[Iterations][,];
            double[][] resultStdevs = new double[Iterations][];
            double[][] resultMeans = new double[Iterations][];

            for (int i = 0; i < Iterations; i++)
            {
                double[][] meansArray = new double[1][];
                meansArray[0] = new double[VectorSize];
                //generator.GenerateMultivariateNormals(VectorSize, 1/12, meansArray, this.stdev, this.correlations);

                resultStdevs[i] = MatrixHelper.MatrixStdev(generator.Result);
                resultMeans[i] = MatrixHelper.MatrixMeans(generator.Result);
                resultCorrelations[i] = MatrixHelper.CorrelationMatrix(generator.Result);
            }

            double[,] averageResultCorrelations = new double[resultCorrelations[0].GetLength(0), resultCorrelations[0].GetLength(1)];
            double[] averageResultStdev = new double[resultStdevs[0].Length];
            double[] averageResultMeans = new double[resultStdevs[0].Length];
            ComputeAverages(Iterations, resultStdevs, resultMeans, resultCorrelations, averageResultStdev, averageResultMeans, averageResultCorrelations);

            for (int i = 0; i < this.stdev.Length; i++)
            {
                Assert.AreEqual(this.stdev[i], averageResultStdev[i], Delta);
            }

            for (int i = 0; i < this.means.Length; i++)
            {
                Assert.AreEqual(this.means[i], averageResultMeans[i], Delta);
            }

            for (int i = 0; i < averageResultCorrelations.GetLength(0); i++)
            {
                for (int j = 0; j < averageResultCorrelations.GetLength(1); j++)
                {
                    Assert.AreEqual(this.correlations[i, j], averageResultCorrelations[i, j], Delta);
                }
            }
        }

        private static void ComputeAverages(int iterations, double[][] resultStdevs, double[][] resultMeans, double[][,] correlationMatrices,
                                            double[] averageStdev, double[] averageMeans, double[,] averageCorrelationMatrix)
        {
            for (int i = 0; i < iterations; i++)
            {
                for (int j = 0; j < correlationMatrices[i].GetLength(0); j++)
                {
                    for (int k = 0; k < correlationMatrices[i].GetLength(1); k++)
                    {
                        averageCorrelationMatrix[j, k] += correlationMatrices[i][j, k];
                    }
                }

                for (int j = 0; j < averageStdev.Length; j++)
                {
                    averageStdev[j] += resultStdevs[i][j];
                }

                for (int j = 0; j < averageMeans.Length; j++)
                {
                    averageMeans[j] += resultMeans[i][j];
                }
            }

            for (int i = 0; i < averageCorrelationMatrix.GetLength(0); i++)
            {
                for (int j = 0; j < averageCorrelationMatrix.GetLength(1); j++)
                {
                    averageCorrelationMatrix[i, j] = averageCorrelationMatrix[i, j] / iterations;
                }
            }

            for (int j = 0; j < averageStdev.Length; j++)
            {
                averageStdev[j] = averageStdev[j] / iterations;
            }

            for (int j = 0; j < averageMeans.Length; j++)
            {
                averageMeans[j] = averageMeans[j] / iterations;
            }
        }
    }
}