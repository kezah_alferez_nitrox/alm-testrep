using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace InsuranceBondManager.Generator
{
    public class Statistics
    { 
        public static double StandardDeviation(double[] values)
        {
            Debug.Assert(values != null);

            if (values.Length <= 1) return 0;

            double sum = 0.0, sumOfSqrs = 0.0;
            foreach (double value in values)
            {
                sum += value;
                sumOfSqrs += value * value;
            }
            double n = values.Length;
            double topSum = n * sumOfSqrs - sum * sum;
            return Math.Sqrt(topSum / (n * (n - 1)));
        }

        public static double Correlation(double[] x, double[] y)
        {
            Debug.Assert(x != null && x.Length > 0);
            Debug.Assert(y != null && y.Length > 0);
            Debug.Assert(x.Length == y.Length);

            double n = x.Length;
            double sumX = x.Sum();
            double sumY = y.Sum();
            double sumX2 = x.Sum(a => a * a);
            double sumY2 = y.Sum(a => a * a);
            double sumXY = SumXY(x, y);

            double correlation = (n * sumXY - sumX * sumY) / Math.Sqrt((n * sumX2 - sumX * sumX) * (n * sumY2 - sumY * sumY));

            return correlation;
        }

        private static double SumXY(double[] x, double[] y)
        {
            double sum = 0;
            for (int i = 0; i < x.Length; i++)
            {
                sum += x[i] * y[i];
            }
            return sum;
        }
    }
}