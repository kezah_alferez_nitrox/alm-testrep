﻿using System;

namespace InsuranceBondManager.Generator
{
    public class Ziggurat : INormalRandom
    {
        private readonly IUniformRandom random;

        public Ziggurat(IUniformRandom random)
        {
            this.random = random;
            zigset();
        }

        public Ziggurat()
            : this(new DefaultRandom())
        {
        }

        public double Next()
        {
            return this.RNOR();
        }

        private static int[] kn;

        private static double[] wn, fn;

        private static bool initialized = false;

        private double RNOR()
        {
            int hz = this.SHR3();
            int iz = hz & 127;

            return (Math.Abs(hz) < kn[iz]) ? hz*wn[iz] : this.nfix(hz, iz);
        }

        private int SHR3()
        {
            return this.random.NextInt32();

            //int jz = jsr;
            //int jzr = jsr;
            //jzr ^= (jzr << 13);
            //jzr ^= (int)(((uint)jzr) >> 17); // java : (jzr >>> 17);
            //jzr ^= (jzr << 5);
            //jsr = jzr;

            //return jz + jzr;
        }

        private double nfix(int hz, int iz)
        {
            double r = 3.442619855899; /* The start of the right tail */
            double r1 = 1/r;
            double x,
                   y;

            for (;;)
            {
                x = hz*wn[iz]; /* iz==0, handles the base strip */

                if (iz == 0)
                {
                    do
                    {
                        x = (-Math.Log(this.UNI())*r1);
                        y = -Math.Log(this.UNI());
                    } while (y + y < x*x);

                    return (hz > 0) ? r + x : -r - x;
                }

                /* iz>0, handle the wedges of other strips */
                if (fn[iz] + this.UNI()*(fn[iz - 1] - fn[iz]) < Math.Exp(-0.5*x*x))
                    return x;

                /* initiate, try to exit for(;;) for loop*/
                hz = this.SHR3();
                iz = hz & 127;

                if (Math.Abs(hz) < kn[iz])
                    return (hz*wn[iz]);
            }
        }

        private double UNI()
        {
            return this.random.NextDouble(); // 0.5 * (1 + (double)SHR3() / (double)int.MinValue);
        }

        private static void zigset()
        {
            if (initialized)
                return;

            initialized = true;

            wn = new double[128];
            fn = new double[128];
            kn = new int[128];

            double m1 = 2147483648.0;
            double dn = 3.442619855899,
                   tn = dn,
                   vn = 9.91256303526217e-3,
                   q;
            int i;

            /* Set up tables for RNOR */
            q = vn/Math.Exp(-0.5*dn*dn);
            kn[0] = (int) ((dn/q)*m1);
            kn[1] = 0;

            wn[0] = q/m1;
            wn[127] = dn/m1;

            fn[0] = 1.0;
            fn[127] = Math.Exp(-0.5*dn*dn);

            for (i = 126; i >= 1; i--)
            {
                dn = Math.Sqrt(-2.0*Math.Log(vn/dn + Math.Exp(-0.5*dn*dn)));
                kn[i + 1] = (int) ((dn/tn)*m1);
                tn = dn;
                fn[i] = Math.Exp(-0.5*dn*dn);
                wn[i] = dn/m1;
            }
        }
    }
}