using System;

namespace InsuranceBondManager.Generator
{
    public class DefaultRandom : IUniformRandom
    {
        private readonly Random random = new Random();

        public double NextDouble()
        {
            return this.random.NextDouble();
        }

        public int NextInt32()
        {
            return this.random.Next(int.MinValue, int.MaxValue);
        }
    }
}