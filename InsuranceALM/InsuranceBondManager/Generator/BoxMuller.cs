using System;
using System.Collections.Generic;

namespace InsuranceBondManager.Generator
{
    public class BoxMuller : INormalRandom
    {
        private readonly IUniformRandom random;
        private readonly IEnumerator<double> enumerator;

        public BoxMuller() : this(new DefaultRandom()) {}

        public BoxMuller(IUniformRandom random)
        {
            this.random = random;
            this.enumerator = this.Enumeration().GetEnumerator();
        }

        public double Next()
        {
            this.enumerator.MoveNext();
            return this.enumerator.Current;
        }

        private IEnumerable<double> Enumeration()
        {
            while (true)
            {
                double u1 = this.random.NextDouble();
                double u2 = this.random.NextDouble();

                double r = Math.Sqrt(-2*Math.Log(u1));
                double theta = 2*Math.PI*u2;

                yield return r*Math.Cos(theta);
                yield return r*Math.Sin(theta);
            }
        }
    }
}