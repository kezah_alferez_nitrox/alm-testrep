﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34014
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace InsuranceBondManager.Properties {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("InsuranceBondManager.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        public static System.Drawing.Bitmap _48px_Gnome_accessories_calculator_svg {
            get {
                object obj = ResourceManager.GetObject("48px-Gnome-accessories-calculator_svg", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        public static System.Drawing.Bitmap arrow_down {
            get {
                object obj = ResourceManager.GetObject("arrow_down", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        public static System.Drawing.Bitmap arrow_up {
            get {
                object obj = ResourceManager.GetObject("arrow_up", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Are you sure you want to clear all data, all unsaved datas, both scenario and portfolio will be lost ?.
        /// </summary>
        public static string ClearAllPrompt {
            get {
                return ResourceManager.GetString("ClearAllPrompt", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        public static System.Drawing.Bitmap CopyHS {
            get {
                object obj = ResourceManager.GetObject("CopyHS", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        public static System.Drawing.Bitmap DeleteHS {
            get {
                object obj = ResourceManager.GetObject("DeleteHS", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        public static System.Drawing.Bitmap dollar_sign {
            get {
                object obj = ResourceManager.GetObject("dollar_sign", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        public static System.Drawing.Bitmap down_16 {
            get {
                object obj = ResourceManager.GetObject("down_16", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        public static System.Drawing.Bitmap down_32 {
            get {
                object obj = ResourceManager.GetObject("down_32", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Can not create or modify the vector because another vector with the same name exists..
        /// </summary>
        public static string DuplicateVector {
            get {
                return ResourceManager.GetString("DuplicateVector", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        public static System.Drawing.Bitmap EditInformationHS {
            get {
                object obj = ResourceManager.GetObject("EditInformationHS", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        public static System.Drawing.Bitmap find {
            get {
                object obj = ResourceManager.GetObject("find", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        public static System.Drawing.Bitmap GoLtrHS {
            get {
                object obj = ResourceManager.GetObject("GoLtrHS", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        public static System.Drawing.Bitmap GoRtlHS {
            get {
                object obj = ResourceManager.GetObject("GoRtlHS", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        public static System.Drawing.Bitmap gray_sphere {
            get {
                object obj = ResourceManager.GetObject("gray_sphere", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        public static System.Drawing.Bitmap green_sphere {
            get {
                object obj = ResourceManager.GetObject("green_sphere", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Impossible to remove the variable {0} because it&apos;s used in scenario setup..
        /// </summary>
        public static string ImpossibleToRemoveVariable {
            get {
                return ResourceManager.GetString("ImpossibleToRemoveVariable", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Impossible to remove the variable {0} because it&apos;s used in fundamental vectors..
        /// </summary>
        public static string ImpossibleToRemoveVariableUsedByFundamentalVector {
            get {
                return ResourceManager.GetString("ImpossibleToRemoveVariableUsedByFundamentalVector", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to INSERT INTO Currency(Id, Symbol, ShortName) Values ( 1, null, &apos;&apos;);
        ///INSERT INTO Currency(Id, Symbol, ShortName) Values ( 2, &apos;€&apos;, &apos;EUR&apos;);
        ///
        ///
        ///INSERT INTO Param(Id, Val) Values ( &apos;Standard Rate&apos;, &apos;&apos;);
        ///INSERT INTO Param(Id, Val) Values ( &apos;Long Term&apos;, &apos;&apos;);
        ///INSERT INTO Param(Id, Val) Values ( &apos;Special&apos;, &apos;&apos;);
        ///INSERT INTO Param(Id, Val) Values ( &apos;Valuation Currency&apos;, &apos;2&apos;);
        ///INSERT INTO Param(Id, Val) Values ( &apos;FileName&apos;, &apos;New ALW&apos;);.
        /// </summary>
        public static string InitBaseScript {
            get {
                return ResourceManager.GetString("InitBaseScript", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &quot;{0}&quot; is an invalid name..
        /// </summary>
        public static string InvalidVectorOrVariableName {
            get {
                return ResourceManager.GetString("InvalidVectorOrVariableName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        public static System.Drawing.Bitmap left_16 {
            get {
                object obj = ResourceManager.GetObject("left_16", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        public static System.Drawing.Bitmap logo200x200 {
            get {
                object obj = ResourceManager.GetObject("logo200x200", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        public static System.Drawing.Bitmap logo400x400 {
            get {
                object obj = ResourceManager.GetObject("logo400x400", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        public static System.Drawing.Bitmap logo48 {
            get {
                object obj = ResourceManager.GetObject("logo48", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        public static System.Drawing.Bitmap NewDocumentHS {
            get {
                object obj = ResourceManager.GetObject("NewDocumentHS", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Are you sure to continue, all unsaved changes will be lost ?.
        /// </summary>
        public static string NewFilePrompt {
            get {
                return ResourceManager.GetString("NewFilePrompt", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0} is not the last day of {1}.
        ///
        ///Do you want to change date input to {2}?.
        /// </summary>
        public static string NotLastDay {
            get {
                return ResourceManager.GetString("NotLastDay", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        public static System.Drawing.Bitmap percentile {
            get {
                object obj = ResourceManager.GetObject("percentile", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        public static System.Drawing.Bitmap red_sphere {
            get {
                object obj = ResourceManager.GetObject("red_sphere", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &quot;{0}&quot; is an invalid name because it&apos;s a function name..
        /// </summary>
        public static string ReservedFunctionName {
            get {
                return ResourceManager.GetString("ReservedFunctionName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        public static System.Drawing.Bitmap right_16 {
            get {
                object obj = ResourceManager.GetObject("right_16", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        public static System.Drawing.Bitmap saveHS {
            get {
                object obj = ResourceManager.GetObject("saveHS", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Impossible to add the scenario {0}. A scenario with the same name already exists..
        /// </summary>
        public static string ScenarioAlreadyExists {
            get {
                return ResourceManager.GetString("ScenarioAlreadyExists", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        public static System.Drawing.Bitmap up_16 {
            get {
                object obj = ResourceManager.GetObject("up_16", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        public static System.Drawing.Bitmap up_32 {
            get {
                object obj = ResourceManager.GetObject("up_32", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Impossible to add the variable {0}. A variable with the same name already exists..
        /// </summary>
        public static string VariableAlreadyExists {
            get {
                return ResourceManager.GetString("VariableAlreadyExists", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;?xml version=&quot;1.0&quot; encoding=&quot;utf-8&quot; ?&gt;
        ///&lt;workspace name=&quot;UserDefined&quot; version=&quot;2&quot;&gt;
        ///  &lt;category name=&quot;Portfolio&quot;&gt;
        ///
        ///    &lt;category name=&quot;Off Balance Sheet&quot; balanceType=&quot;4&quot; readOnly=&quot;True&quot; type=&quot;Standard&quot;&gt;
        ///      &lt;category name=&quot;Swaps&quot; balanceType=&quot;5&quot; readOnly=&quot;True&quot; type=&quot;Standard&quot;/&gt;
        ///      &lt;category name=&quot;Cap-Floor&quot; balanceType=&quot;5&quot; readOnly=&quot;True&quot; type=&quot;Standard&quot;/&gt;
        ///      &lt;category name=&quot;Swaptions&quot; balanceType=&quot;5&quot; readOnly=&quot;True&quot; type=&quot;Standard&quot;/&gt;
        ///    &lt;/category&gt;     
        ///  &lt;/category&gt;
        ///&lt;/workspace&gt;.
        /// </summary>
        public static string WorkspaceTemplateV2 {
            get {
                return ResourceManager.GetString("WorkspaceTemplateV2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        public static System.Drawing.Bitmap yellow_sphere {
            get {
                object obj = ResourceManager.GetObject("yellow_sphere", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
    }
}
