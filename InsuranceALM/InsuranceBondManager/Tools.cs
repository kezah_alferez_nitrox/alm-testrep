using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Windows.Forms;
using ALMSCommon;
using ALMSCommon.Threading;
using InsuranceALM.Infrastructure.Data;
using InsuranceBondManager.Domain;
using InsuranceBondManager.Resources.Language;
using InsuranceBondManager.Views.Vectors;
using NHibernate;

namespace InsuranceBondManager
{
    internal class Tools
    {
        private static readonly char[] operators = new[] { ' ', '\t', '+', '-', '/', '*', '(', ')', '%', ',', '?', ':', ';', '.', '<', '>', '=', '[', ']', '{', '}', '^' };

        private static readonly string[] reservedJavascriptWords = new[]{
            "break",
            "case",
            "catch",
            "continue",
            "debugger",
            "default",
            "delete",
            "do",
            "else",
            "finally",
            "for",
            "function",
            "if",
            "in",
            "instanceof",
            "new",
            "return",
            "switch",
            "this",
            "throw",
            "try",
            "typeof",
            "var",
            "void",
            "while",
            "with"};

        private static readonly ThreadSafeDictionary<string, string> FormulaCache = new ThreadSafeDictionary<string, string>();

        public static bool InDesignMode
        {
            get { return LicenseManager.UsageMode == LicenseUsageMode.Designtime; }
        }

        public static string[] GetOperandsFromExpression(string expression)
        {
            if (expression == null) return new string[0];

            expression = PreProcessExpression(expression);

            string[] operandsAndFunctions = expression.Split(operators, StringSplitOptions.RemoveEmptyEntries);
            List<string> operands = new List<string>();
            foreach (string operandOrFunction in operandsAndFunctions)
            {
                if (!IsFunctionName(operandOrFunction))
                {
                    operands.Add(operandOrFunction);
                }
            }
            return operands.ToArray();
        }

        public static object CallPrivateInstanceMethod<T>(T target, string methodName, params object[] args)
        {
            Type type = typeof(T);

            MethodInfo methodInfo = type.GetMethod(methodName, BindingFlags.Instance | BindingFlags.NonPublic);
            if (methodInfo == null) return null;

            return methodInfo.Invoke(target, args);
        }

        public static string EscapeIdentifier(string name)
        {
            return name.Replace(' ', '_'); // todo: replace +,- etc
        }

        public static string PreProcessExpression(string expression)
        {
            return FormulaCache.TryGetValueOrAdd(expression, () => PreProcessExpressionInternal(expression));
        }

        private static string PreProcessExpressionInternal(string expression)
        {
            if (String.IsNullOrEmpty(expression)) return expression;

            expression = expression.Replace("%", "*0.01");
            //expression = expression.Replace("bp", "*0.0001");
            if (expression.Substring(0, 1) == "+" ||
                expression.Substring(0, 1) == "=")
            {
                expression = expression.Substring(1);
            }

            for (int index = 0; index < Constants.SupportedFunctions.Count; index++)
            {
                string loweredFunctionName = Constants.LoweredFunctions[index];
                if (!expression.Contains(loweredFunctionName)) continue;
                string functionName = Constants.SupportedFunctions[index];
                expression = expression.Replace(functionName.ToUpperInvariant(), functionName);
                expression = expression.Replace(functionName.ToLowerInvariant(), functionName);
            }

            return expression;
        }

        public static bool IsFunctionName(string text)
        {
            foreach (string functionName in Constants.SupportedFunctions)
            {
                if (text == functionName || text == functionName.ToUpperInvariant() ||
                    text == functionName.ToLowerInvariant())
                {
                    return true;
                }
            }

            return false;
        }

        public static bool IsReservedJavascriptWord(string text)
        {
            return reservedJavascriptWords.Any(w => w == text);
        }

        public static bool IsDecimalString(string value)
        {
            double tempDecimal;
            return Double.TryParse(value, out tempDecimal);
        }

        public static Form GetMainForm()
        {
            return Application.OpenForms.Count == 0 ? null : Application.OpenForms[0];
        }

        public static DialogResult ShowDialog(Form form)
        {
            Form mainForm = null; // GetMainForm();

            if (mainForm == null)
            {
                return form.ShowDialog();
            }
            else
            {
                return form.ShowDialog(mainForm);
            }
        }

        public static bool IsValidIdentifier(string name, out string message)
        {
            message = null;

            if (!Constants.VectorOrVariableRegex.IsMatch(name))
            {
                message = String.Format(Labels.Tools_IsValidIdentifier___0___is_an_invalid_name_, name);
                return false;
            }

            if (IsFunctionName(name))
            {
                message = String.Format(Labels.Tools_IsValidIdentifier___0___is_an_invalid_name_because_it_s_a_function_name_, name);
                return false;
            }

            if (IsReservedJavascriptWord(name))
            {
                message = String.Format(Labels.Tools_IsValidIdentifier___0___is_an_invalid_name_because_it_s_a_reserved_word_, name);
                return false;
            }

            return true;
        }

        public static Image browseForLogo()
        {
            OpenFileDialog dialog = new OpenFileDialog();
            if (dialog.ShowDialog() != DialogResult.OK || !dialog.CheckFileExists) return null;

            Image image = Image.FromFile(dialog.FileName);
            return resizeImage(image, new Size(216, 72));
        }

        public static Image resizeImage(Image image, Size size)
        {
            int sourceWidth = image.Width;
            int sourceHeight = image.Height;

            float width = (float)size.Width / sourceWidth;
            float height = (float)size.Height / sourceHeight;
            float ratio;
            if (height < width) ratio = height;
            else ratio = width;
            int newWidth = (int)(sourceWidth * ratio);
            int newHeight = (int)(sourceHeight * ratio);

            Bitmap bitmap = new Bitmap(newWidth, newHeight);
            using (Graphics g = Graphics.FromImage(bitmap))
            {
                g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                g.DrawImage(image, 0, 0, newWidth, newHeight);
            }

            return bitmap;
        }

        public static void RecalculateBookValues()
        {
            //MTU-echelonner les vecteurs par rapport � la nouvelle valuation date
            using (ISession session = SessionManager.OpenSession())
            {
                foreach (Product product in EntityBase<Product, int>.FindAll(session))
                {
                    product.FixBookValue();
                }
                session.Flush();
            }
        }

        public static void RedateVectorsAfterValuationDateChanged()
        {
            //MTU-echelonner les vecteurs par rapport � la nouvelle valuation date
            using (ISession session = SessionManager.OpenSession())
            {
                Vector[] vectors = EntityBase<Vector, int>.FindAll(session);
                DateTime valDate = Param.GetValuationDate(session).GetValueOrDefault(DateTime.Today);//.Subtract(new TimeSpan(1, 0, 0, 0));
                foreach (Vector vector in vectors)
                {
                    Func<DateTime, int, DateTime> frequencyFunc = vector.GetIncrementFunc();
                    for (int i = 0; i < vector.VectorPoints.Count; i++)
                    {
                        vector.VectorPoints[i].Date = frequencyFunc(valDate, i);
                    }
                }
                session.Flush();
            }
        }

        public static VectorPoint[] RedateVectorsFromWorkspace(VectorPoint[] vectorPoints, DateTime valuationDate)
        {
            //MTU-echelonner les vecteurs par rapport � la nouvelle valuation date
            Dictionary<Vector, List<VectorPoint>> datas = new Dictionary<Vector, List<VectorPoint>>();
            for (int i = 0; i < vectorPoints.Length; i++)
            {
                if (!datas.ContainsKey(vectorPoints[i].Vector))
                    datas.Add(vectorPoints[i].Vector, new List<VectorPoint>());
                datas[vectorPoints[i].Vector].Add(vectorPoints[i]);
            }
            VectorPoint[] result = new VectorPoint[vectorPoints.Length];
            int j = 0;
            foreach (Vector vector in datas.Keys)
            {
                datas[vector].Sort((vp1, vp2) => vp1.Date.CompareTo(vp2.Date));
                VectorPoint[] vp = datas[vector].ToArray();
                Func<DateTime, int, DateTime> incrementFunc = vector.GetIncrementFunc();
                for (int i = 0; i < vp.Length; i++)
                {
                    vp[i].Date = incrementFunc(valuationDate, i);
                }
                Array.Copy(vp, 0, result, j, vp.Length);
                j += vp.Length;
            }
            return result;
        }

        public static DateTime ResetDateToFirstOfMonth(DateTime date)
        {
            return new DateTime(date.Year, date.Month, 1);
        }

        public static DateTime ResetDateToLastOfMonth(DateTime date)
        {
            return new DateTime(date.Year, date.Month, 1).AddMonths(1).AddDays(-1);
        }

        public static Point GetNextDrawingPoint(Control previousControl, int Y, bool rightToLeft = false)
        {
            Point result = new Point();
            result.Y = Y;
            int sign = 1;
            if (rightToLeft) sign = -1;
            result.X = previousControl.Location.X + sign * (previousControl.Width + 10);
            return result;

        }

        /// <summary>
        /// Start an Action within an STA Thread
        /// </summary>
        /// <param name="goForIt"></param>
        public static void RunAsSTAThread(Action goForIt)
        {
            AutoResetEvent @event = new AutoResetEvent(false);
            Thread thread = new Thread(
                () =>
                {
                    goForIt();
                    @event.Set();
                });
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();
            @event.WaitOne();
        }
    }

    public class EventArgs<T> : EventArgs
    {
        private readonly T _value;

        public EventArgs(T value)
        {
            this._value = value;
        }

        public T Value
        {
            get { return this._value; }
        }
    }





}