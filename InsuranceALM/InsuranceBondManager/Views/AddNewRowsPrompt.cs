using System;
using System.Windows.Forms;
using ALMSCommon.Forms;
using InsuranceBondManager.Domain;
using Eu.AlterSystems.ASNetLib.Core;

namespace InsuranceBondManager.Views
{
    public partial class AddNewRowsPrompt : DpiForm
    {
        public AddNewRowsPrompt()
        {
            this.InitializeComponent();
        }

        public int RowCount
        {
            get
            {
                return (int)this.rowCountNumericUpDown.Value;
            }
        }

        private void AddNewRowsPrompt_Load(object sender, EventArgs e)
        {
            this.rowCountNumericUpDown.Focus();
            this.rowCountNumericUpDown.Select(0, this.rowCountNumericUpDown.Text.Length);
        }
    }
}