using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using ALMSCommon.Controls;
using InsuranceBondManager.BusinessLogic;
using InsuranceBondManager.Core;
using InsuranceBondManager.Domain;
using InsuranceBondManager.Controls;
using InsuranceBondManager.Resources.Language;


namespace InsuranceBondManager.Views
{
    public partial class SolvencyAdjustmentConfiguration : SessionForm
    {
        private DataGridViewTextBoxColumn descriptionDataGridViewTextBoxColumn;
        private AutoCompleteColumn vectorAutoCompleteColumn;

        private DataGridViewColumn[] columns;

        public SolvencyAdjustmentConfiguration()
        {
            InitializeComponent();

            grid.AutoGenerateColumns = false;
            grid.AllowUserToAddRows = false;
            grid.AllowUserToDeleteRows = false;
            grid.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
            grid.RowTemplate.Height = Constants.GRID_ROW_HEIGHT;
        }

        private string[] variableAndVectorNames;

        private void InitVariableAndVector()
        {
            variableAndVectorNames = DomainTools.GetAllVariablesAndVectorNames(this.Session);
        }

        private void InitColumns()
        {
            InitVariableAndVector();

            descriptionDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            vectorAutoCompleteColumn = new AutoCompleteColumn();

            //descriptionDataGridViewTextBoxColumn 
            descriptionDataGridViewTextBoxColumn.DataPropertyName = "Description";
            descriptionDataGridViewTextBoxColumn.HeaderText = Labels.SolvencyAdjustmentConfiguration_InitColumns_Description;
            descriptionDataGridViewTextBoxColumn.Name = "Description";
            descriptionDataGridViewTextBoxColumn.ReadOnly = true;
            descriptionDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            descriptionDataGridViewTextBoxColumn.Width = 200;

            //vectorAutoCompleteColumn
            vectorAutoCompleteColumn.DataPropertyName = "Vector";
            vectorAutoCompleteColumn.HeaderText = Labels.SolvencyAdjustmentConfiguration_InitColumns_Vector;
            vectorAutoCompleteColumn.Name = "Vector";
            vectorAutoCompleteColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            vectorAutoCompleteColumn.Width = 100;
            vectorAutoCompleteColumn.ValueList = variableAndVectorNames;

            columns = new DataGridViewColumn[]
            {
                descriptionDataGridViewTextBoxColumn,
                vectorAutoCompleteColumn,
            };

            grid.Columns.Clear();
            grid.Columns.AddRange(columns);
        }

        private void SolvencyAdjustment_Load(object sender, System.EventArgs e)
        {
            if (DesignMode || Tools.InDesignMode) return;

            this.InitColumns();
            this.Refresh(false);
        }

        private BindingList<SolvencyAdjustment> DataSource
        {
            get { return grid.DataSource as BindingList<SolvencyAdjustment>; }
            set { grid.DataSource = value; }
        }

        private void Refresh(bool resetColumns)
        {
            if (resetColumns)
            {
                InitColumns();
            }

            BindingList<SolvencyAdjustment> list = new BindingList<SolvencyAdjustment>(
                new List<SolvencyAdjustment>(SolvencyAdjustment.FindAll(this.Session)));
            DataSource = list;
        }

        private void okButton_Click(object sender, System.EventArgs e)
        {
            this.SaveChangesInSession();
        }

        private void GridDataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            e.ThrowException = false;
            e.Cancel = true;
        }
    }
}