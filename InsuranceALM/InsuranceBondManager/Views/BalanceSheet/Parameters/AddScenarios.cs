using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using ALMSCommon;
using InsuranceBondManager.Core;
using InsuranceBondManager.Domain;
using InsuranceBondManager.Resources.Language;
using NHibernate.Criterion;


namespace InsuranceBondManager.Views.BalanceSheet.Parameters
{
    public partial class AddScenarios : SessionForm
    {
        private BindingList<Scenario> availableList;
        private BindingList<Scenario> selectedList;
        private BindingList<Scenario> defaultList;

        private Scenario selectedScenario;

        public AddScenarios()
        {
            this.InitializeComponent();
        }

        private void RefreshAvailable()
        {
            Scenario[] scenarios = Scenario.FindAvailable(this.Session);
            this.availableList = new BindingList<Scenario>(new List<Scenario>(scenarios));
            this.availableListBox.DataSource = this.availableList;
        }

        private void RefreshSelected()
        {
            Scenario[] scenarios = Scenario.FindSelected(this.Session);
            this.selectedList = new BindingList<Scenario>(new List<Scenario>(scenarios));
            this.selectedListBox.DataSource = this.selectedList;
        }

        private void RefreshDefault()
        {
            Scenario[] scenarios = Scenario.FindSelected(this.Session);
            this.defaultList = new BindingList<Scenario>(new List<Scenario>(scenarios));
            this.comboDefaultScenario.DataSource = this.defaultList;
            this.comboDefaultScenario.SelectedItem = Scenario.FindDefault(this.Session);
        }

        private void AddScenariosLoad(object sender, EventArgs e)
        {
            this.availableListBox.DisplayMember = "Name";
            this.availableListBox.ValueMember = "Self";
            this.RefreshAvailable();
            this.availableListBox.SelectedItem = null;

            this.selectedListBox.DisplayMember = "Name";
            this.selectedListBox.ValueMember = "Self";
            this.RefreshSelected();
            this.selectedListBox.SelectedItem = null;

            this.selectedListBox.DisplayMember = "Name";
            this.selectedListBox.ValueMember = "Self";
            this.RefreshDefault();

            this.selectedListBox.SelectedIndexChanged += this.SelectedListBoxSelectedIndexChanged;
            this.comboDefaultScenario.SelectedIndexChanged += comboDefaultScenario_SelectedIndexChanged;
        }

        private void comboDefaultScenario_SelectedIndexChanged(object sender, EventArgs e)
        {
            MainForm.ModifiedValues = true;
        }

        private void AddNewButtonClick(object sender, EventArgs e)
        {
            CreateScenario dialog = new CreateScenario();
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                Scenario sc = this.CreateAvailableScenarios(dialog.ScenarioName);
                this.selectedListBox.SelectedItem = null;
                this.availableListBox.SelectedItem = sc;
                MainForm.ModifiedScenarioTemplate = true;
                MainForm.ModifiedValues = true;
            }
        }

        private Scenario CreateAvailableScenarios(string scenarioName)
        {
            Scenario newScenario = this.availableList.AddNew();
            if (newScenario == null) return null;

            newScenario.Name = scenarioName;
            newScenario.Selected = false;
            newScenario.Create(this.Session);

            this.availableList.ResetBindings();

            return newScenario;
        }

        private void AddButtonClick(object sender, EventArgs e)
        {
            Scenario[] toAdd = this.availableListBox.SelectedIndices.Cast<int>().Select(i => this.availableList[i]).ToArray();

            foreach (Scenario scenario in toAdd)
            {
                this.availableList.Remove(scenario);
                this.selectedList.Add(scenario);
                this.defaultList.Add(scenario);
            }

            this.availableListBox.SelectedItem = null;
            this.selectedListBox.SelectedItem = null;
            for (int i = 0; i < toAdd.Length; i++)
            {
                this.selectedListBox.SetSelected(this.selectedListBox.Items.Count - i - 1, true);
            }

            MainForm.ModifiedScenarioTemplate = true;
            MainForm.ModifiedValues = true;
        }

        private void DeleteButtonClick(object sender, EventArgs e)
        {
            Scenario[] toDelete = this.availableListBox.SelectedIndices.Cast<int>().Select(i => this.availableList[i]).ToArray();

            if (Dialogs.Confirm(Labels.AddScenarios_DeleteButtonClick_Delete_scenarios__, string.Format(Labels.AddScenarios_DeleteButtonClick_Delete_the__0__selected_scenario_s___, toDelete.Length)))
            {
                this.Cursor = Cursors.WaitCursor;
                foreach (Scenario scenario in toDelete)
                {
                    this.availableList.Remove(scenario);

                    DeleteScenario(scenario);
                }
                this.Cursor = Cursors.Default;
            }

            MainForm.ModifiedValues = true;
        }

        private  void DeleteScenario(Scenario scenario)
        {
            foreach (VolatilityConfig volatilityConfig in VolatilityConfig.FindAll(this.Session, Expression.Eq("Scenario", scenario)))
            {
                volatilityConfig.Delete(this.Session);
            }

            foreach (VariableCorrelation variableCorrelation in VariableCorrelation.FindAll(this.Session, Expression.Eq("Scenario", scenario)))
            {
                variableCorrelation.Delete(this.Session);
            }

            foreach (VariableValue variableValue in VariableValue.FindAll(this.Session, Expression.Eq("Scenario", scenario)))
            {
                variableValue.Delete(this.Session);
            }

            scenario.Delete(this.Session);
        }

        private void RemoveButtonClick(object sender, EventArgs e)
        {
            Scenario[] toRemove = this.selectedListBox.SelectedIndices.Cast<int>().Select(i => this.selectedList[i])
                .Where(s => s != comboDefaultScenario.SelectedItem as Scenario).ToArray();

            foreach (Scenario scenario in toRemove)
            {
                this.selectedList.Remove(scenario);
                this.defaultList.Remove(scenario);
                this.availableList.Add(scenario);
            }

            this.selectedListBox.SelectedItem = null;
            this.availableListBox.SelectedItem = null;
            for (int i = 0; i < toRemove.Length; i++)
            {
                this.availableListBox.SetSelected(this.availableListBox.Items.Count - i - 1, true);
            }

            MainForm.ModifiedScenarioTemplate = true;
            MainForm.ModifiedValues = true;
        }

        private void OkButtonClick(object sender, EventArgs e)
        {
            for (int index = 0; index < this.selectedList.Count; index++)
            {
                Scenario scenario = this.selectedList[index];
                scenario.Position = index;
                scenario.Selected = true;
            }

            for (int index = 0; index < this.availableList.Count; index++)
            {
                Scenario scenario = this.availableList[index];
                scenario.Position = 10000 + index;
                scenario.Selected = false;
            }

            Scenario oldDefault = Scenario.FindDefault(this.Session);
            Scenario newDefault = comboDefaultScenario.SelectedItem as Scenario;
            if (newDefault != null && oldDefault != newDefault)
            {
                if (oldDefault != null) oldDefault.IsDefault = false;
                newDefault.IsDefault = true;
            }
            MainForm.Instance.InitializeOpeningBalance();
            this.Cursor = Cursors.WaitCursor;
            //PersistenceSession.Flush();
            this.SaveChangesInSession();
            this.Cursor = Cursors.Default;
        }

        private void AvailableListBoxSelectedIndexChanged(object sender, EventArgs e)
        {
            this.deleteButton.Enabled = this.addButton.Enabled = this.availableListBox.SelectedIndices.Count > 0;

            this.SelectScenario(this.availableListBox);
        }

        private void SelectScenario(ListBox listBox)
        {
            if (this.selectedScenario != null)
            {
                this.selectedScenario.Description = this.descriptionTextBox.Text;
            }
            this.selectedScenario = null;

            if (listBox.SelectedIndices.Count == 1)
            {
                this.selectedScenario = (Scenario)listBox.Items[listBox.SelectedIndices[0]];
            }
            else
            {
                this.selectedScenario = null;
            }

            if (this.selectedScenario != null)
            {
                this.descriptionGroupBox.Text = this.selectedScenario.Name;
                this.descriptionTextBox.Text = this.selectedScenario.Description;
            }
            else
            {
                this.descriptionGroupBox.Text = "";
                this.descriptionTextBox.Text = "";
            }

            this.descriptionGroupBox.Enabled = this.selectedScenario != null;
        }

        private void CloneScenarioClick(object sender, EventArgs e)
        {
            CopyScenario dialog = new CopyScenario();
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                Scenario scenario = dialog.SelectedScenario;
                if (scenario != null)
                {
                    Scenario scenarioCopy = this.CreateAvailableScenarios(dialog.ScenarioName);

                    if (scenarioCopy != null)
                    {
                        foreach (VariableValue variable in VariableValue.FindByScenario(this.Session, scenario))
                        {
                            VariableValue variableValueCopy = new VariableValue();
                            variableValueCopy.Value = variable.Value;
                            if (dialog.ApplyOffsetToVariable.Contains(variable.Variable))
                                variableValueCopy.Value += dialog.OffsetFormula;
                            variableValueCopy.Variable = variable.Variable;
                            variableValueCopy.Scenario = scenarioCopy;

                            scenarioCopy.VariableValues.Add(variableValueCopy);
                        }

                        this.availableListBox.SelectedItem = null;
                        this.availableListBox.SelectedItem = scenarioCopy;
                        
                        MainForm.ModifiedScenarioTemplate = true;
                        MainForm.ModifiedValues = true;

                        this.Session.Flush();
                    }
                }
            }
        }

        private void descriptionTextBox_TextChanged(object sender, EventArgs e)
        {
            if (this.selectedScenario != null)
            {
                this.selectedScenario.Description = this.descriptionTextBox.Text;
            }
        }

        private void SelectedListBoxSelectedIndexChanged(object sender, EventArgs e)
        {
            this.SelectScenario(this.selectedListBox);

            this.UpdateUpDownButtons();

            this.removeButton.Enabled = this.selectedListBox.SelectedIndices.Count > 0;
        }

        private void UpdateUpDownButtons()
        {
            if (this.selectedListBox.SelectedIndices.Count != 1)
            {
                this.upButton.Enabled = this.downButton.Enabled = false;
                return;
            }

            this.upButton.Enabled = this.selectedListBox.SelectedIndex > 0;
            this.downButton.Enabled = this.selectedListBox.SelectedIndex < this.selectedListBox.Items.Count - 1;
        }

        private void upButton_Click(object sender, EventArgs e)
        {
            this.MoveSelectedScenario(-1);
        }

        private void downButton_Click(object sender, EventArgs e)
        {
            this.MoveSelectedScenario(+1);
        }

        private void MoveSelectedScenario(int delta)
        {
            int startIndex = this.selectedListBox.SelectedIndex;
            int endIndex = startIndex + delta;

            Scenario scenario = this.selectedList[startIndex];
            this.selectedList.RemoveAt(startIndex);
            this.selectedList.Insert(endIndex, scenario);

            this.selectedListBox.SelectedItem = null;
            this.selectedListBox.SelectedItem = scenario;

            MainForm.ModifiedScenarioTemplate = true;
        }

        private void comboDefaultScenario_DropDown(object sender, EventArgs e)
        {
            int width = this.comboDefaultScenario.Width;
            Graphics g = this.comboDefaultScenario.CreateGraphics();
            Font font = this.comboDefaultScenario.Font;
            int vertScrollBarWidth =
                (this.comboDefaultScenario.Items.Count > this.comboDefaultScenario.MaxDropDownItems)
                ? SystemInformation.VerticalScrollBarWidth : 0;
            int newWidth;
            foreach (Scenario s in this.defaultList)
            {
                newWidth = (int)g.MeasureString(s.ToString(), font).Width
                    + vertScrollBarWidth;
                if (width < newWidth)
                {
                    width = newWidth + 20;
                }
            }
            this.comboDefaultScenario.DropDownWidth = width;
        }
    }
}
