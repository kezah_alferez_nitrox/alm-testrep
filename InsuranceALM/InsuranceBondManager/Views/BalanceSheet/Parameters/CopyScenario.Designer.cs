﻿namespace InsuranceBondManager.Views.BalanceSheet.Parameters
{
    partial class CopyScenario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.okButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.txtScenarioName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBaseScenario = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtOffsetFormula = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.VarsToApplyOffset = new System.Windows.Forms.CheckedListBox();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // okButton
            // 
            this.okButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okButton.Enabled = false;
            this.okButton.Location = new System.Drawing.Point(99, 416);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(80, 23);
            this.okButton.TabIndex = 5;
            this.okButton.Text = global::InsuranceBondManager.Resources.Language.Labels.CreateScenario_InitializeComponent_OK;
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Click += new System.EventHandler(this.okButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point(185, 416);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(80, 23);
            this.cancelButton.TabIndex = 4;
            this.cancelButton.Text = global::InsuranceBondManager.Resources.Language.Labels.CreateScenario_InitializeComponent_Cancel;
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // txtScenarioName
            // 
            this.txtScenarioName.Location = new System.Drawing.Point(107, 39);
            this.txtScenarioName.Name = "txtScenarioName";
            this.txtScenarioName.Size = new System.Drawing.Size(169, 20);
            this.txtScenarioName.TabIndex = 7;
            this.txtScenarioName.TextChanged += new System.EventHandler(this.scenarioNameTextBox_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Scenario Name";
            // 
            // comboBaseScenario
            // 
            this.comboBaseScenario.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBaseScenario.FormattingEnabled = true;
            this.comboBaseScenario.Location = new System.Drawing.Point(107, 12);
            this.comboBaseScenario.Name = "comboBaseScenario";
            this.comboBaseScenario.Size = new System.Drawing.Size(169, 21);
            this.comboBaseScenario.TabIndex = 8;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Base Scenario";
            // 
            // txtOffsetFormula
            // 
            this.txtOffsetFormula.Location = new System.Drawing.Point(107, 65);
            this.txtOffsetFormula.Name = "txtOffsetFormula";
            this.txtOffsetFormula.Size = new System.Drawing.Size(169, 20);
            this.txtOffsetFormula.TabIndex = 11;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Offset formula";
            // 
            // VarsToApplyOffset
            // 
            this.VarsToApplyOffset.FormattingEnabled = true;
            this.VarsToApplyOffset.Location = new System.Drawing.Point(16, 102);
            this.VarsToApplyOffset.Name = "VarsToApplyOffset";
            this.VarsToApplyOffset.Size = new System.Drawing.Size(256, 289);
            this.VarsToApplyOffset.TabIndex = 12;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 86);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(222, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "Apply offset formula only to following variables";
            // 
            // CopyScenario
            // 
            this.AcceptButton = this.okButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(281, 451);
            this.ControlBox = false;
            this.Controls.Add(this.label4);
            this.Controls.Add(this.VarsToApplyOffset);
            this.Controls.Add(this.txtOffsetFormula);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.comboBaseScenario);
            this.Controls.Add(this.txtScenarioName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.okButton);
            this.Controls.Add(this.cancelButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "CopyScenario";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Copy Scenario";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CreateScenario_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.TextBox txtScenarioName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBaseScenario;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtOffsetFormula;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckedListBox VarsToApplyOffset;
        private System.Windows.Forms.Label label4;
    }
}