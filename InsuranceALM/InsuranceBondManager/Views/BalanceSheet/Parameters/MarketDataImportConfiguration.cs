﻿using ALMSCommon.Forms;
using InsuranceBondManager.Core.Import;
using InsuranceBondManager.Domain;
using Eu.AlterSystems.ASNetLib.Core;

namespace InsuranceBondManager.Views.BalanceSheet.Parameters
{
    public partial class MarketDataImportConfiguration : DpiForm
    {
        public MarketDataImportConfiguration()
        {
            this.InitializeComponent();

            this.groupComboBox.DisplayMember = "Value";
            this.groupComboBox.ValueMember = "Key";
            this.groupComboBox.DataSource = EnumHelper.ToList(typeof (VectorGroup));

            this.dataDirectionComboBox.DisplayMember = "Value";
            this.dataDirectionComboBox.ValueMember = "Key";
            this.dataDirectionComboBox.DataSource = EnumHelper.ToList(typeof (DataDirection));
        }

        public VectorGroup VectorGroup
        {
            get { return (VectorGroup) this.groupComboBox.SelectedValue; }
        }

        public DataDirection DataDirection
        {
            get { return (DataDirection)this.dataDirectionComboBox.SelectedValue; }
        }
    }
}