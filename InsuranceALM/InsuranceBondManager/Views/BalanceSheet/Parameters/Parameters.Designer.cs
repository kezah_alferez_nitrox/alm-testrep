﻿using ALMSCommon.Controls;
using ALMSCommon.Controls.TreeGridView;
using InsuranceBondManager.Resources.Language;

namespace InsuranceBondManager.Views.BalanceSheet.Parameters
{
    partial class Parameters
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.parametersSplitContainer = new System.Windows.Forms.SplitContainer();
            this.marketParametersGroupBox = new System.Windows.Forms.GroupBox();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnImportantMarketDataDown = new System.Windows.Forms.Button();
            this.btnImportantMarketDataUp = new System.Windows.Forms.Button();
            this.marketParametersDataGridView = new ALMSCommon.Controls.DataGridViewEx();
            this.importantMarketDataMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.moveUpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.downToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.fundamentalVectorsGrid = new ALMSCommon.Controls.DataGridViewEx();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.currencyComboBox = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.marketParamtersPanel = new System.Windows.Forms.Panel();
            this.Change = new System.Windows.Forms.Button();
            this.valuationDateLabel = new System.Windows.Forms.Label();
            this.rollEnabledComboBox = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.importMarketDataButton = new System.Windows.Forms.Button();
            this.valuationCurrencyComboBox = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.scenariosSetupGroupBox = new System.Windows.Forms.GroupBox();
            this.scenariosSetupDataGridView = new ALMSCommon.Controls.DataGridViewEx();
            this.scenarioMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.menuScenarioCopy = new System.Windows.Forms.ToolStripMenuItem();
            this.menuScenarioPaste = new System.Windows.Forms.ToolStripMenuItem();
            this.menuScenarioUp = new System.Windows.Forms.ToolStripMenuItem();
            this.menuScenarioDown = new System.Windows.Forms.ToolStripMenuItem();
            this.scenarioSetupPanel = new System.Windows.Forms.Panel();
            this.addVariablesButton = new System.Windows.Forms.Button();
            this.addScenariosButton = new System.Windows.Forms.Button();
            this.marketDataOpenFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.fundamentalMarketDataRowHeader = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.deleteRowsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.parametersSplitContainer)).BeginInit();
            this.parametersSplitContainer.Panel1.SuspendLayout();
            this.parametersSplitContainer.Panel2.SuspendLayout();
            this.parametersSplitContainer.SuspendLayout();
            this.marketParametersGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.marketParametersDataGridView)).BeginInit();
            this.importantMarketDataMenu.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fundamentalVectorsGrid)).BeginInit();
            this.panel1.SuspendLayout();
            this.marketParamtersPanel.SuspendLayout();
            this.scenariosSetupGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scenariosSetupDataGridView)).BeginInit();
            this.scenarioMenu.SuspendLayout();
            this.scenarioSetupPanel.SuspendLayout();
            this.fundamentalMarketDataRowHeader.SuspendLayout();
            this.SuspendLayout();
            // 
            // parametersSplitContainer
            // 
            this.parametersSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.parametersSplitContainer.Location = new System.Drawing.Point(0, 0);
            this.parametersSplitContainer.Name = "parametersSplitContainer";
            this.parametersSplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // parametersSplitContainer.Panel1
            // 
            this.parametersSplitContainer.Panel1.Controls.Add(this.marketParametersGroupBox);
            // 
            // parametersSplitContainer.Panel2
            // 
            this.parametersSplitContainer.Panel2.Controls.Add(this.scenariosSetupGroupBox);
            this.parametersSplitContainer.Size = new System.Drawing.Size(929, 536);
            this.parametersSplitContainer.SplitterDistance = 254;
            this.parametersSplitContainer.TabIndex = 1;
            // 
            // marketParametersGroupBox
            // 
            this.marketParametersGroupBox.Controls.Add(this.splitContainer1);
            this.marketParametersGroupBox.Controls.Add(this.marketParamtersPanel);
            this.marketParametersGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.marketParametersGroupBox.Location = new System.Drawing.Point(0, 0);
            this.marketParametersGroupBox.Name = "marketParametersGroupBox";
            this.marketParametersGroupBox.Size = new System.Drawing.Size(929, 254);
            this.marketParametersGroupBox.TabIndex = 0;
            this.marketParametersGroupBox.TabStop = false;
            this.marketParametersGroupBox.Text = "Market Parameters";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(3, 78);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.groupBox1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.groupBox2);
            this.splitContainer1.Size = new System.Drawing.Size(923, 173);
            this.splitContainer1.SplitterDistance = 462;
            this.splitContainer1.TabIndex = 4;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.panel2);
            this.groupBox1.Controls.Add(this.marketParametersDataGridView);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(462, 173);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Important Market Data";
            // 
            // panel2
            // 
            this.panel2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.panel2.Controls.Add(this.btnImportantMarketDataDown);
            this.panel2.Controls.Add(this.btnImportantMarketDataUp);
            this.panel2.Location = new System.Drawing.Point(415, 61);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(44, 63);
            this.panel2.TabIndex = 2;
            // 
            // btnImportantMarketDataDown
            // 
            this.btnImportantMarketDataDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.btnImportantMarketDataDown.Location = new System.Drawing.Point(0, 32);
            this.btnImportantMarketDataDown.Name = "btnImportantMarketDataDown";
            this.btnImportantMarketDataDown.Size = new System.Drawing.Size(44, 28);
            this.btnImportantMarketDataDown.TabIndex = 1;
            this.btnImportantMarketDataDown.Text = "Down";
            this.btnImportantMarketDataDown.UseVisualStyleBackColor = true;
            this.btnImportantMarketDataDown.Click += new System.EventHandler(this.MoveImportantMarketData);
            // 
            // btnImportantMarketDataUp
            // 
            this.btnImportantMarketDataUp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.btnImportantMarketDataUp.Location = new System.Drawing.Point(0, 3);
            this.btnImportantMarketDataUp.Name = "btnImportantMarketDataUp";
            this.btnImportantMarketDataUp.Size = new System.Drawing.Size(44, 28);
            this.btnImportantMarketDataUp.TabIndex = 0;
            this.btnImportantMarketDataUp.Text = "Up";
            this.btnImportantMarketDataUp.UseVisualStyleBackColor = true;
            this.btnImportantMarketDataUp.Click += new System.EventHandler(this.MoveImportantMarketData);
            // 
            // marketParametersDataGridView
            // 
            this.marketParametersDataGridView.AllowUserToAddRows = false;
            this.marketParametersDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.marketParametersDataGridView.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.marketParametersDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.marketParametersDataGridView.ContextMenuStrip = this.importantMarketDataMenu;
            this.marketParametersDataGridView.Location = new System.Drawing.Point(3, 16);
            this.marketParametersDataGridView.Name = "marketParametersDataGridView";
            this.marketParametersDataGridView.ReadOnly = true;
            this.marketParametersDataGridView.Size = new System.Drawing.Size(411, 154);
            this.marketParametersDataGridView.TabIndex = 1;
            this.marketParametersDataGridView.SelectionChanged += new System.EventHandler(this.ImportantMarketDataSelectionChanged);
            // 
            // importantMarketDataMenu
            // 
            this.importantMarketDataMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.moveUpToolStripMenuItem,
            this.downToolStripMenuItem});
            this.importantMarketDataMenu.Name = "importantMarketDataMenu";
            this.importantMarketDataMenu.Size = new System.Drawing.Size(106, 48);
            this.importantMarketDataMenu.Opening += new System.ComponentModel.CancelEventHandler(this.MarketDataMenuOpening);
            // 
            // moveUpToolStripMenuItem
            // 
            this.moveUpToolStripMenuItem.Name = "moveUpToolStripMenuItem";
            this.moveUpToolStripMenuItem.Size = new System.Drawing.Size(105, 22);
            this.moveUpToolStripMenuItem.Text = "Up";
            this.moveUpToolStripMenuItem.Click += new System.EventHandler(this.MoveImportantMarketData);
            // 
            // downToolStripMenuItem
            // 
            this.downToolStripMenuItem.Name = "downToolStripMenuItem";
            this.downToolStripMenuItem.Size = new System.Drawing.Size(105, 22);
            this.downToolStripMenuItem.Text = "Down";
            this.downToolStripMenuItem.Click += new System.EventHandler(this.MoveImportantMarketData);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.fundamentalVectorsGrid);
            this.groupBox2.Controls.Add(this.panel1);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(457, 173);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Fundamental Market Data";
            // 
            // fundamentalVectorsGrid
            // 
            this.fundamentalVectorsGrid.AllowUserToOrderColumns = true;
            this.fundamentalVectorsGrid.AllowUserToResizeRows = false;
            this.fundamentalVectorsGrid.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.fundamentalVectorsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.fundamentalVectorsGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fundamentalVectorsGrid.Location = new System.Drawing.Point(3, 50);
            this.fundamentalVectorsGrid.Name = "fundamentalVectorsGrid";
            this.fundamentalVectorsGrid.Size = new System.Drawing.Size(451, 120);
            this.fundamentalVectorsGrid.TabIndex = 1;
            this.fundamentalVectorsGrid.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.FundamentalVectorDataError);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.currencyComboBox);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(3, 16);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(451, 34);
            this.panel1.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(213, 0);
            this.button1.Name = "button1";
            this.button1.Padding = new System.Windows.Forms.Padding(5);
            this.button1.Size = new System.Drawing.Size(161, 30);
            this.button1.TabIndex = 9;
            this.button1.Text = global::InsuranceBondManager.Resources.Language.Labels.Parameters_InitializeComponent_Forward_Curves_Analyser;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // currencyComboBox
            // 
            this.currencyComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.currencyComboBox.FormattingEnabled = true;
            this.currencyComboBox.Location = new System.Drawing.Point(64, 0);
            this.currencyComboBox.Name = "currencyComboBox";
            this.currencyComboBox.Size = new System.Drawing.Size(121, 21);
            this.currencyComboBox.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 3);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(55, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Currency :";
            // 
            // marketParamtersPanel
            // 
            this.marketParamtersPanel.Controls.Add(this.Change);
            this.marketParamtersPanel.Controls.Add(this.valuationDateLabel);
            this.marketParamtersPanel.Controls.Add(this.rollEnabledComboBox);
            this.marketParamtersPanel.Controls.Add(this.label4);
            this.marketParamtersPanel.Controls.Add(this.importMarketDataButton);
            this.marketParamtersPanel.Controls.Add(this.valuationCurrencyComboBox);
            this.marketParamtersPanel.Controls.Add(this.label2);
            this.marketParamtersPanel.Controls.Add(this.label1);
            this.marketParamtersPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.marketParamtersPanel.Location = new System.Drawing.Point(3, 16);
            this.marketParamtersPanel.Name = "marketParamtersPanel";
            this.marketParamtersPanel.Size = new System.Drawing.Size(923, 62);
            this.marketParamtersPanel.TabIndex = 0;
            this.marketParamtersPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.marketParamtersPanel_Paint);
            // 
            // Change
            // 
            this.Change.Location = new System.Drawing.Point(179, 4);
            this.Change.Name = "Change";
            this.Change.Size = new System.Drawing.Size(70, 20);
            this.Change.TabIndex = 10;
            this.Change.Text = "Change";
            this.Change.UseVisualStyleBackColor = true;
            this.Change.Click += new System.EventHandler(this.Change_Click);
            // 
            // valuationDateLabel
            // 
            this.valuationDateLabel.AutoSize = true;
            this.valuationDateLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.valuationDateLabel.Location = new System.Drawing.Point(93, 8);
            this.valuationDateLabel.Name = "valuationDateLabel";
            this.valuationDateLabel.Size = new System.Drawing.Size(0, 13);
            this.valuationDateLabel.TabIndex = 9;
            // 
            // rollEnabledComboBox
            // 
            this.rollEnabledComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.rollEnabledComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rollEnabledComboBox.FormattingEnabled = true;
            this.rollEnabledComboBox.Items.AddRange(new object[] {
            global::InsuranceBondManager.Resources.Language.Labels.Parameters_InitializeComponent_Yes,
            global::InsuranceBondManager.Resources.Language.Labels.Parameters_InitializeComponent_No});
            this.rollEnabledComboBox.Location = new System.Drawing.Point(384, 34);
            this.rollEnabledComboBox.Name = "rollEnabledComboBox";
            this.rollEnabledComboBox.Size = new System.Drawing.Size(78, 21);
            this.rollEnabledComboBox.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(276, 34);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(31, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Roll :";
            // 
            // importMarketDataButton
            // 
            this.importMarketDataButton.Location = new System.Drawing.Point(540, 7);
            this.importMarketDataButton.Name = "importMarketDataButton";
            this.importMarketDataButton.Padding = new System.Windows.Forms.Padding(5);
            this.importMarketDataButton.Size = new System.Drawing.Size(114, 46);
            this.importMarketDataButton.TabIndex = 4;
            this.importMarketDataButton.Text = global::InsuranceBondManager.Resources.Language.Labels.Parameters_InitializeComponent_Import_Market_Data;
            this.importMarketDataButton.UseVisualStyleBackColor = true;
            this.importMarketDataButton.Click += new System.EventHandler(this.importMarketDataButton_Click);
            // 
            // valuationCurrencyComboBox
            // 
            this.valuationCurrencyComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.valuationCurrencyComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.valuationCurrencyComboBox.FormattingEnabled = true;
            this.valuationCurrencyComboBox.Location = new System.Drawing.Point(384, 8);
            this.valuationCurrencyComboBox.Name = "valuationCurrencyComboBox";
            this.valuationCurrencyComboBox.Size = new System.Drawing.Size(78, 21);
            this.valuationCurrencyComboBox.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(276, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(102, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Valuation Currency :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Valuation Date :";
            // 
            // scenariosSetupGroupBox
            // 
            this.scenariosSetupGroupBox.Controls.Add(this.scenariosSetupDataGridView);
            this.scenariosSetupGroupBox.Controls.Add(this.scenarioSetupPanel);
            this.scenariosSetupGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scenariosSetupGroupBox.Location = new System.Drawing.Point(0, 0);
            this.scenariosSetupGroupBox.Name = "scenariosSetupGroupBox";
            this.scenariosSetupGroupBox.Size = new System.Drawing.Size(929, 278);
            this.scenariosSetupGroupBox.TabIndex = 0;
            this.scenariosSetupGroupBox.TabStop = false;
            this.scenariosSetupGroupBox.Text = "Scenarios Setup";
            // 
            // scenariosSetupDataGridView
            // 
            this.scenariosSetupDataGridView.AllowUserToAddRows = false;
            this.scenariosSetupDataGridView.AllowUserToDeleteRows = false;
            this.scenariosSetupDataGridView.AllowUserToOrderColumns = true;
            this.scenariosSetupDataGridView.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.scenariosSetupDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.scenariosSetupDataGridView.ContextMenuStrip = this.scenarioMenu;
            this.scenariosSetupDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scenariosSetupDataGridView.Location = new System.Drawing.Point(3, 53);
            this.scenariosSetupDataGridView.Name = "scenariosSetupDataGridView";
            this.scenariosSetupDataGridView.Size = new System.Drawing.Size(923, 222);
            this.scenariosSetupDataGridView.TabIndex = 1;
            this.scenariosSetupDataGridView.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.ValidateScenarioCell);
            // 
            // scenarioMenu
            // 
            this.scenarioMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuScenarioCopy,
            this.menuScenarioPaste,
            this.menuScenarioUp,
            this.menuScenarioDown});
            this.scenarioMenu.Name = "scenarioMenu";
            this.scenarioMenu.Size = new System.Drawing.Size(199, 92);
            // 
            // menuScenarioCopy
            // 
            this.menuScenarioCopy.Name = "menuScenarioCopy";
            this.menuScenarioCopy.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this.menuScenarioCopy.Size = new System.Drawing.Size(198, 22);
            this.menuScenarioCopy.Text = global::InsuranceBondManager.Resources.Language.Labels.Parameters_InitializeComponent_Copy;
            this.menuScenarioCopy.Click += new System.EventHandler(this.ScenarioCopyClipboard);
            // 
            // menuScenarioPaste
            // 
            this.menuScenarioPaste.Name = "menuScenarioPaste";
            this.menuScenarioPaste.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V)));
            this.menuScenarioPaste.Size = new System.Drawing.Size(198, 22);
            this.menuScenarioPaste.Text = global::InsuranceBondManager.Resources.Language.Labels.Parameters_InitializeComponent_Paste;
            this.menuScenarioPaste.Click += new System.EventHandler(this.ScenarioPasteClipboard);
            // 
            // menuScenarioUp
            // 
            this.menuScenarioUp.Name = "menuScenarioUp";
            this.menuScenarioUp.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.Up)));
            this.menuScenarioUp.Size = new System.Drawing.Size(198, 22);
            this.menuScenarioUp.Text = global::InsuranceBondManager.Resources.Language.Labels.Parameters_InitializeComponent_Move_up;
            this.menuScenarioUp.Click += new System.EventHandler(this.MoveScenario);
            // 
            // menuScenarioDown
            // 
            this.menuScenarioDown.Name = "menuScenarioDown";
            this.menuScenarioDown.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.Down)));
            this.menuScenarioDown.Size = new System.Drawing.Size(198, 22);
            this.menuScenarioDown.Text = global::InsuranceBondManager.Resources.Language.Labels.Parameters_InitializeComponent_Move_down;
            this.menuScenarioDown.Click += new System.EventHandler(this.MoveScenario);
            // 
            // scenarioSetupPanel
            // 
            this.scenarioSetupPanel.Controls.Add(this.addVariablesButton);
            this.scenarioSetupPanel.Controls.Add(this.addScenariosButton);
            this.scenarioSetupPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.scenarioSetupPanel.Location = new System.Drawing.Point(3, 16);
            this.scenarioSetupPanel.Name = "scenarioSetupPanel";
            this.scenarioSetupPanel.Size = new System.Drawing.Size(923, 37);
            this.scenarioSetupPanel.TabIndex = 0;
            // 
            // addVariablesButton
            // 
            this.addVariablesButton.Location = new System.Drawing.Point(127, 5);
            this.addVariablesButton.Name = "addVariablesButton";
            this.addVariablesButton.Size = new System.Drawing.Size(114, 23);
            this.addVariablesButton.TabIndex = 1;
            this.addVariablesButton.Text = global::InsuranceBondManager.Resources.Language.Labels.Parameters_InitializeComponent_Add_Variables____;
            this.addVariablesButton.UseVisualStyleBackColor = true;
            this.addVariablesButton.Click += new System.EventHandler(this.addVariablesButton_Click);
            // 
            // addScenariosButton
            // 
            this.addScenariosButton.Location = new System.Drawing.Point(7, 5);
            this.addScenariosButton.Name = "addScenariosButton";
            this.addScenariosButton.Size = new System.Drawing.Size(114, 23);
            this.addScenariosButton.TabIndex = 0;
            this.addScenariosButton.Text = global::InsuranceBondManager.Resources.Language.Labels.Parameters_InitializeComponent_Add_Scenarios____;
            this.addScenariosButton.UseVisualStyleBackColor = true;
            this.addScenariosButton.Click += new System.EventHandler(this.addScenariosButton_Click);
            // 
            // fundamentalMarketDataRowHeader
            // 
            this.fundamentalMarketDataRowHeader.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deleteRowsToolStripMenuItem});
            this.fundamentalMarketDataRowHeader.Name = "fundamentalMarketDataRowHeader";
            this.fundamentalMarketDataRowHeader.Size = new System.Drawing.Size(146, 26);
            // 
            // deleteRowsToolStripMenuItem
            // 
            this.deleteRowsToolStripMenuItem.Name = "deleteRowsToolStripMenuItem";
            this.deleteRowsToolStripMenuItem.Size = new System.Drawing.Size(145, 22);
            this.deleteRowsToolStripMenuItem.Text = global::InsuranceBondManager.Resources.Language.Labels.Parameters_InitializeComponent_Delete_Row_s_;
            this.deleteRowsToolStripMenuItem.Click += new System.EventHandler(this.deleteRowsToolStripMenuItem_Click);
            // 
            // Parameters
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.parametersSplitContainer);
            this.Name = "Parameters";
            this.Size = new System.Drawing.Size(929, 536);
            this.Load += new System.EventHandler(this.Parameters_Load);
            this.parametersSplitContainer.Panel1.ResumeLayout(false);
            this.parametersSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.parametersSplitContainer)).EndInit();
            this.parametersSplitContainer.ResumeLayout(false);
            this.marketParametersGroupBox.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.marketParametersDataGridView)).EndInit();
            this.importantMarketDataMenu.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fundamentalVectorsGrid)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.marketParamtersPanel.ResumeLayout(false);
            this.marketParamtersPanel.PerformLayout();
            this.scenariosSetupGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scenariosSetupDataGridView)).EndInit();
            this.scenarioMenu.ResumeLayout(false);
            this.scenarioSetupPanel.ResumeLayout(false);
            this.fundamentalMarketDataRowHeader.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer parametersSplitContainer;
        private System.Windows.Forms.GroupBox marketParametersGroupBox;
        private DataGridViewEx marketParametersDataGridView;
        private System.Windows.Forms.Panel marketParamtersPanel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button importMarketDataButton;
        private System.Windows.Forms.ComboBox valuationCurrencyComboBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox scenariosSetupGroupBox;
        private DataGridViewEx scenariosSetupDataGridView;
        private System.Windows.Forms.Panel scenarioSetupPanel;
        private System.Windows.Forms.Button addVariablesButton;
        private System.Windows.Forms.Button addScenariosButton;
        private System.Windows.Forms.OpenFileDialog marketDataOpenFileDialog;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox currencyComboBox;
        private System.Windows.Forms.Label label5;
        private DataGridViewEx fundamentalVectorsGrid;
        private System.Windows.Forms.ContextMenuStrip fundamentalMarketDataRowHeader;
        private System.Windows.Forms.ToolStripMenuItem deleteRowsToolStripMenuItem;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ContextMenuStrip scenarioMenu;
        private System.Windows.Forms.ToolStripMenuItem menuScenarioCopy;
        private System.Windows.Forms.ToolStripMenuItem menuScenarioPaste;
        private System.Windows.Forms.ToolStripMenuItem menuScenarioUp;
        private System.Windows.Forms.ToolStripMenuItem menuScenarioDown;
        private System.Windows.Forms.ComboBox rollEnabledComboBox;
        private System.Windows.Forms.ContextMenuStrip importantMarketDataMenu;
        private System.Windows.Forms.ToolStripMenuItem moveUpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem downToolStripMenuItem;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnImportantMarketDataDown;
        private System.Windows.Forms.Button btnImportantMarketDataUp;
        private System.Windows.Forms.Button Change;
        private System.Windows.Forms.Label valuationDateLabel;
    }
}
