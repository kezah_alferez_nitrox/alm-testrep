using InsuranceBondManager.Domain;

namespace InsuranceBondManager.Views.BalanceSheet.Parameters
{
    public class MarketParameters
    {
        private string curve;
        private string day;
        private string onem;
        private string threem;
        private string sixm;
        private string oney;
        private string twoy;
        private string fivey;
        private string teny;
        private Vector vector;


        public Vector Vector
        {
            get { return this.vector; }
            set { this.vector = value; }
        }

        public string Curve
        {
            get { return curve; }
            set { curve = value; }
        }

        public string Day
        {
            get { return day; }
            set { day = value; }
        }

        public string Onem
        {
            get { return onem; }
            set { onem = value; }
        }

        public string Threem
        {
            get { return threem; }
            set { threem = value; }
        }

        public string Sixm
        {
            get { return sixm; }
            set { sixm = value; }
        }

        public string Oney
        {
            get { return oney; }
            set { oney = value; }
        }

        public string Twoy
        {
            get { return twoy; }
            set { twoy = value; }
        }

        public string Fivey
        {
            get { return fivey; }
            set { fivey = value; }
        }

        public string Teny
        {
            get { return teny; }
            set { teny = value; }
        }
    }
}