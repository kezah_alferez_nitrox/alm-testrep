﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ALMSCommon.Forms;
using InsuranceBondManager.Core;
using InsuranceBondManager.Domain;
using InsuranceBondManager.Resources.Language;

namespace InsuranceBondManager.Views.BalanceSheet.Parameters
{
    public partial class CopyScenario : SessionForm // todo faire heriter session for; de dpi from
    {
        private bool valid;

        public string ScenarioName
        {
            get
            {
                return txtScenarioName.Text;
            }
        }

        public Variable[] Variables { get; set; }
        public List<Variable> ApplyOffsetToVariable { get; set; }

        public Scenario[] Scenarios { get; set; }
        public Scenario SelectedScenario
        {
            get 
            {
                if (!(comboBaseScenario.SelectedItem is KeyValuePair<Scenario, string>)) return null;
                KeyValuePair<Scenario, string> pair = (KeyValuePair<Scenario, string>) comboBaseScenario.SelectedItem;
                return pair.Key;
            }
        }
        public string OffsetFormula { get { return this.txtOffsetFormula.Text ?? ""; } }

        public CopyScenario()
        {
            InitializeComponent();

            this.Scenarios = Scenario.FindNonGenerated(this.Session);
            if (Scenarios != null)
            {
                comboBaseScenario.Items.Clear();
                
                foreach (Scenario scenario in Scenarios)
                {
                    KeyValuePair<Scenario, string> pair = new KeyValuePair<Scenario, string>(scenario, scenario.Name);
                    comboBaseScenario.Items.Add(pair);
                }
                comboBaseScenario.DisplayMember = "Value";
                comboBaseScenario.ValueMember = "Key";
            }

            this.Variables = Variable.FindAll(this.Session);
            if (Variables != null)
            {
                VarsToApplyOffset.Items.Clear();
                foreach (Variable var in Variables)
                {
                    KeyValuePair<Variable, string> pair = new KeyValuePair<Variable, string>(var, var.Name);
                    VarsToApplyOffset.Items.Add(pair);
                    VarsToApplyOffset.SetItemChecked(VarsToApplyOffset.Items.Count - 1, true);
                }
                VarsToApplyOffset.DisplayMember = "Value";
                VarsToApplyOffset.ValueMember = "Key";
                VarsToApplyOffset.Sorted = true;
                VarsToApplyOffset.CheckOnClick = true;
            }
            
        }

        private void okButton_Click(object sender, System.EventArgs e)
        {
            if (Scenario.ExistsByName(this.Session, txtScenarioName.Text))
            {
                MessageBox.Show(string.Format(Properties.Resources.ScenarioAlreadyExists, txtScenarioName.Text),
                    Labels.CreateScenario_okButton_Click_Error, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                valid = false;
                return;
            }
            ApplyOffsetToVariable = new List<Variable>(VarsToApplyOffset.CheckedItems.Count);
            foreach (KeyValuePair<Variable, string> var in VarsToApplyOffset.CheckedItems)
            {
                ApplyOffsetToVariable.Add(var.Key);
            }

            valid = true;
        }

        private void CreateScenario_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = !valid;
        }

        private void cancelButton_Click(object sender, System.EventArgs e)
        {
            valid = true;
        }

        private void scenarioNameTextBox_TextChanged(object sender, System.EventArgs e)
        {
            okButton.Enabled = !string.IsNullOrEmpty(txtScenarioName.Text);
        }
    }
}
