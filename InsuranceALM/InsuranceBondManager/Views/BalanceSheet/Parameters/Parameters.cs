using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using ALMSCommon.Controls;
using ALMSCommon.Forms;
using InsuranceALM.Infrastructure.Data;
using InsuranceBondManager.BusinessLogic;
using InsuranceBondManager.Controls;
using InsuranceBondManager.Core;
using InsuranceBondManager.Core.Import;
using InsuranceBondManager.Domain;
using InsuranceBondManager.Resources.Language;
using NHibernate;


namespace InsuranceBondManager.Views.BalanceSheet.Parameters
{
    public partial class Parameters : SessionUserControl
    {
        private const string AlmsScenarioRowsFormat = "almsScenarioRowsFormat";

        public delegate void ValuationCurrencyChangedEventHandler(object sender, EventArgs e);

        public event ValuationCurrencyChangedEventHandler ValuationCurrencyChanged = delegate { };

        private readonly List<DataGridViewColumn> columns = new List<DataGridViewColumn>();

        private readonly MarketParametersDataGridController _marketParametersDataGridView;
        private FundamentalVectorsGridControler fundamentalVectorsGridControler;

        private Variable[] variables;
        private Scenario[] scenarios;

        public Parameters()
        {
            InitializeComponent();

            valuationCurrencyComboBox.DisplayMember = "Symbol";
            valuationCurrencyComboBox.ValueMember = "Self";

            currencyComboBox.DisplayMember = "Symbol";
            currencyComboBox.ValueMember = "Self";

            scenariosSetupDataGridView.EditMode = DataGridViewEditMode.EditOnEnter;
            scenariosSetupDataGridView.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableWithoutHeaderText;

            this.scenariosSetupDataGridView.CellValueChanged += this.scenariosSetupDataGridView_CellValueChanged;
            this.scenariosSetupDataGridView.DragDrop += DragLine;
            this.scenariosSetupDataGridView.DragOver += DragOverGrid;
            this.scenariosSetupDataGridView.MouseDown += CheckDragLine;
            this.scenariosSetupDataGridView.AllowDrop = true;

            this.fundamentalVectorsGrid.MouseUp += ChangeSelection;
            this.marketParametersDataGridView.MouseUp += ChangeSelection;
            this.scenariosSetupDataGridView.MouseUp += ChangeSelection;
            this.fundamentalVectorsGrid.CreateCopyContextMenu();
            this.marketParametersDataGridView.CellValueChanged += new DataGridViewCellEventHandler(marketParametersDataGridView_CellValueChanged);
            this.fundamentalVectorsGrid.CellValueChanged += fundamentalVectorsGrid_CellValueChanged;

            fundamentalVectorsGrid.RowTemplate.HeaderCell.ContextMenuStrip = fundamentalMarketDataRowHeader;

            if (!DesignMode && !Tools.InDesignMode)
            {
                _marketParametersDataGridView = new MarketParametersDataGridController(marketParametersDataGridView);

                this.fundamentalVectorsGridControler = new FundamentalVectorsGridControler(this.fundamentalVectorsGrid);
            }
        }

        private void marketParametersDataGridView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            MainForm.ModifiedValues = true;
        }

        private void fundamentalVectorsGrid_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            MainForm.ModifiedValues = true;
        }

        private void ChangeSelection(object sender, MouseEventArgs e)
        {
            DataGridView grid = sender as DataGridView;
            if (grid == null) return;

            grid.EditMode = grid.SelectedCells.Count > 1 ? DataGridViewEditMode.EditProgrammatically : DataGridViewEditMode.EditOnKeystrokeOrF2;
            if (grid.SelectedCells.Count == 1) grid.BeginEdit(true);
            else grid.EndEdit();
        }

        private void DragOverGrid(object sender, DragEventArgs e)
        {
            System.Drawing.Point point = scenariosSetupDataGridView.PointToClient(new Point(e.X, e.Y));
            DataGridView.HitTestInfo hit = scenariosSetupDataGridView.HitTest(point.X, point.Y);
            if (hit.RowIndex >= 0) e.Effect = DragDropEffects.Move;
            else e.Effect = DragDropEffects.None;
        }

        private void CheckDragLine(object sender, MouseEventArgs e)
        {
            DataGridView.HitTestInfo hit = scenariosSetupDataGridView.HitTest(e.X, e.Y);
            if (hit.ColumnIndex == -1 && hit.RowIndex > -1 && scenariosSetupDataGridView.Rows[hit.RowIndex].Selected)
                scenariosSetupDataGridView.DoDragDrop(scenariosSetupDataGridView.SelectedRows, DragDropEffects.Move);
        }

        private void DragLine(object sender, DragEventArgs e)
        {
            Point point = scenariosSetupDataGridView.PointToClient(new Point(e.X, e.Y));
            DataGridView.HitTestInfo hit = scenariosSetupDataGridView.HitTest(point.X, point.Y);
            foreach (string format in e.Data.GetFormats())
            {
                if (hit.RowIndex >= 0 && e.Data.GetData(format) is DataGridViewSelectedRowCollection)
                {
                    DataGridViewSelectedRowCollection movedRows = (DataGridViewSelectedRowCollection)e.Data.GetData(format);
                    Scenario[] movedScenarios = new Scenario[movedRows.Count];
                    for (int i = 0; i < movedRows.Count; i++) movedScenarios[i] = scenarios[movedRows[i].Index];
                    int moveIndex = hit.RowIndex;

                    int index = 0;
                    for (int i = 0; i < scenarios.Length; i++)
                    {
                        if (i == moveIndex)
                        {
                            foreach (Scenario movedScenario in movedScenarios)
                                movedScenario.Position = index++;
                        }
                        if (movedScenarios.Contains(this.scenarios[i])) continue;

                        scenarios[i].Position = index++;
                    }

                    scenariosSetupDataGridView.ClearSelection();
                    RefreshScenarioSetup();
                }
            }
        }

        private void Parameters_Load(object sender, EventArgs e)
        {
            if (DesignMode || Tools.InDesignMode) return;


            RefreshScenarioSetup();

            _marketParametersDataGridView.Refresh(false, this.ValuationDate);
            fundamentalVectorsGridControler.Refresh((Currency)currencyComboBox.SelectedValue);

            rollEnabledComboBox.SelectedIndexChanged += this.rollEnabledComboBox_SelectedIndexChanged;
        }

        private void fxRatesComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            MainForm.ModifiedValues = true;
        }

        public int[] GetScenarioSetupColumnsOrder()
        {
            int[] result = new int[scenariosSetupDataGridView.Columns.Count];
            for (int i = 0; i < scenariosSetupDataGridView.Columns.Count; i++)
            {
                int index = this.scenariosSetupDataGridView.Columns[i].DisplayIndex;
                result[i] = index;
            }
            return result;
        }

        public void SetSetupColumnsOrder(int[] order)
        {
            if (order == null) return;

            for (int i = 0; i < order.Length; i++)
            {
                if (i >= this.scenariosSetupDataGridView.Columns.Count) break;
                this.scenariosSetupDataGridView.Columns[i].DisplayIndex = order[i];
            }
        }

        private void LoadCurrenciesComboBox()
        {
            valuationCurrencyComboBox.SelectedIndexChanged -= this.valuationCurrencyComboBox_SelectedIndexChanged;
            currencyComboBox.SelectedIndexChanged -= this.currencyComboBox_SelectedIndexChanged;

            valuationCurrencyComboBox.DataSource = Currency.FindAllNotEmpty(this.Session);
            currencyComboBox.DataSource = Currency.FindAllNotEmpty(this.Session);

            if (Param.GetValuationCurrency(this.Session) != null)
            {
                foreach (object item in valuationCurrencyComboBox.Items)
                {
                    Currency currency = item as Currency;
                    if (currency == null) continue;
                    if (currency.ShortName == Param.GetValuationCurrency(this.Session).ShortName)
                    {
                        valuationCurrencyComboBox.SelectedItem = currency;
                        currencyComboBox.SelectedItem = currency;
                    }
                }
            }

            valuationCurrencyComboBox.SelectedIndexChanged += this.valuationCurrencyComboBox_SelectedIndexChanged;
            currencyComboBox.SelectedIndexChanged += this.currencyComboBox_SelectedIndexChanged;
        }

        void currencyComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            fundamentalVectorsGridControler.Refresh((Currency)currencyComboBox.SelectedItem);

            MainForm.ModifiedValues = true;
        }

        public void InitFxRates()
        {
            Currency valuationCurrency = Param.GetValuationCurrency(this.Session);
            if (valuationCurrency == null) return;

            Currency[] allCurrencies = Currency.FindAllNotEmpty(this.Session);



            CurrencyBL.ResetValuationDateFxRates();
            foreach (Currency currency in allCurrencies)
            {
                if (valuationCurrency != currency)
                {
                    this.Session.Flush();
                    double fxRate = CurrencyBL.FxRate(currency, valuationCurrency);

                }
            }
        }

        private void RefreshScenarioSetup()
        {
            columns.Clear();
            AutoCompleteColumn[] variableColumns = new AutoCompleteColumn[(Variable.FindSelected(this.Session)).Length];
            scenariosSetupDataGridView.Columns.Clear();
            scenariosSetupDataGridView.Rows.Clear();

            // set columns
            DataGridViewTextBoxColumn scenarioColumn = new DataGridViewTextBoxColumn();
            scenarioColumn.HeaderText = Labels.Parameters_RefreshScenarioSetup_Scenario;
            //scenarioColumn.ReadOnly = true;
            //scenarioColumn.DefaultCellStyle.BackColor = Color.LightGray;
            scenarioColumn.Frozen = true;
            columns.Add(scenarioColumn);

            string[] vectorNames = DomainTools.GetAllVectorNames(this.Session);

            variables = Variable.FindSelected(this.Session);
            int _i = 0;
            foreach (Variable variable in variables)
            {
                variableColumns[_i] = new AutoCompleteColumn();
                variableColumns[_i].HeaderText = variable.Name;
                variableColumns[_i].Name = variable.Name;
                variableColumns[_i].ValueList = vectorNames;
                _i++;
            }
            columns.AddRange(variableColumns);
            scenariosSetupDataGridView.Columns.AddRange(columns.ToArray());

            FillScenarioSetup();

            if (Param.GetValuationDate(this.Session) != null)
            {
                valuationDateLabel.Text = Param.GetValuationDate(this.Session).Value.ToString("MMM yyyy");
            }
            else
            {
                Param.SetValuationDate(this.Session, DateTime.Parse(valuationDateLabel.Text));
                this.Session.Flush();
            }

            rollEnabledComboBox.SelectedIndex = Param.GetRollEnabled(this.Session) ? 0 : 1;

            InitFxRates();

            LoadCurrenciesComboBox();
        }

        public DateTime ValuationDate
        {
            get
            {
                return Param.GetValuationDate(this.Session).Value;
            }
            set
            {
                Param.SetValuationDate(this.Session, value);
                valuationDateLabel.Text = Param.GetValuationDate(this.Session).Value.ToString("MMM yyyy");
            }
        }

        private void FillScenarioSetup()
        {
            this.scenariosSetupDataGridView.CellValueChanged -= this.scenariosSetupDataGridView_CellValueChanged;

            scenarios = Scenario.FindSelected(this.Session);
            if (scenarios.Length <= 0) return;

            scenariosSetupDataGridView.Rows.Add(scenarios.Length);

            for (int i = 0; i < scenarios.Length; i++)
            {
                scenariosSetupDataGridView[0, i].Value = scenarios[i].Name;
                IList<VariableValue> variableValues = scenarios[i].VariableValues;
                foreach (VariableValue variableValue in variableValues)
                {
                    int columnIndex = FindColumnIndexbyName(variableValue.Variable.Name);
                    if (columnIndex <= 0) continue;

                    scenariosSetupDataGridView[columnIndex, i].Value = variableValue.Value;
                }
            }

            this.scenariosSetupDataGridView.CellValueChanged += this.scenariosSetupDataGridView_CellValueChanged;
        }

        private int FindColumnIndexbyName(string columnName)
        {
            for (int i = 0; i < scenariosSetupDataGridView.Columns.Count; i++)
            {
                if (scenariosSetupDataGridView.Columns[i].Name == columnName)
                {
                    return i;
                }
            }

            return -1;
        }

        private void addScenariosButton_Click(object sender, EventArgs e)
        {
            if (Tools.ShowDialog(new AddScenarios()) == DialogResult.OK)
            {
                RefreshScenarioSetup();
            }
        }

        private void addVariablesButton_Click(object sender, EventArgs e)
        {
            if (Tools.ShowDialog(new AddVariables()) == DialogResult.OK)
            {
                RefreshScenarioSetup();
            }
        }

        private void scenariosSetupDataGridView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex < 0) return;

            Scenario scenario = scenarios[e.RowIndex];
            scenario = Scenario.Find(this.Session, scenario.Id);

            if (e.ColumnIndex == 0)
            {
                scenario.Name = Convert.ToString(scenariosSetupDataGridView[e.ColumnIndex, e.RowIndex].Value);
                scenario.Save(this.Session);
                return;
            }

            Variable variable = variables[e.ColumnIndex - 1];

            VariableValue value = VariableValue.Find(this.Session, variable, scenario);

            if (value == null)
            {
                value = new VariableValue();
                value.Variable = variable;
                value.Scenario = scenario;
            }
            if (!scenario.VariableValues.Contains(value))
            {
                scenario.VariableValues.Add(value);
                scenario.Save(this.Session);
            }

            string _val = Convert.ToString(scenariosSetupDataGridView[e.ColumnIndex, e.RowIndex].Value);

            if (_val != value.Value)
            {
                MainForm.ModifiedScenarioTemplate = true;
                value.Value = _val;
                value.Save(this.Session);
            }
            this.Session.Flush();
            MainForm.ModifiedValues = true;
        }

        public void RefreshParameters()
        {
            this.ParentForm.Cursor = Cursors.WaitCursor;
            this.Session.Flush();

            this.ClearSession();

            RefreshScenarioSetup();
            _marketParametersDataGridView.Refresh(true, this.ValuationDate);

            fundamentalVectorsGridControler.Refresh((Currency)currencyComboBox.SelectedValue);
            this.ParentForm.Cursor = Cursors.Default;
        }

        private void importMarketDataButton_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            MarketDataImportConfiguration marketDataImportConfiguration = new MarketDataImportConfiguration();
            if (marketDataImportConfiguration.ShowDialog() == DialogResult.OK)
            {
                if (marketDataOpenFileDialog.ShowDialog() == DialogResult.OK)
                {
                    MarketDataImport marketDataImport = new MarketDataImport(marketDataOpenFileDialog.FileName, this.ValuationDate,
                        marketDataImportConfiguration.VectorGroup, marketDataImportConfiguration.DataDirection);

                    LoadForm.Instance.DoWork(new WorkParameters(marketDataImport.Import, this.AfterMarketDataImport), null);
                    this.Cursor = Cursors.Arrow;
                    MessageBox.Show(marketDataImport.ImportedVectorCount +
                        (marketDataImport.ImportedVectorCount <= 1 ? " vector was" : " vectors were") +
                        " successfully imported in your scenario.");
                    MainForm.ModifiedScenarioTemplate = true;
                }
            }
        }

        private void AfterMarketDataImport(Exception exception)
        {
            Tools.RedateVectorsAfterValuationDateChanged();
            
            this.RefreshParameters();
        }

        private void valuationCurrencyComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            Param.SetValuationCurrency(this.Session, valuationCurrencyComboBox.SelectedItem as Currency);
            this.Session.Flush();

            BalanceBL.AjustBalance();

            CurrencyBL.SetSpecialBondsToValuationCurrency();

            ValuationCurrencyChanged(this, new EventArgs());

            MainForm.ModifiedValues = true;
        }




        private void rollEnabledComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            Param.SetRollEnabled(this.Session, rollEnabledComboBox.SelectedIndex == 0);
            this.Session.Flush();
            MainForm.ModifiedValues = true;
        }

        public void FindAndSelectVariableValue(VariableValue value)
        {
            foreach (DataGridViewRow row in scenariosSetupDataGridView.Rows)
            {
                if ((string)row.Cells[0].Value == value.Scenario.Name)
                {
                    foreach (DataGridViewColumn column in scenariosSetupDataGridView.Columns)
                    {
                        if (column.Name == value.Variable.Name)
                        {
                            scenariosSetupDataGridView.ClearSelection();
                            scenariosSetupDataGridView[column.Index, row.Index].Selected = true;
                            return;
                        }
                    }
                    return;
                }
            }
        }

        private void deleteRowsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fundamentalVectorsGridControler.DeleteRows();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Currency currency = currencyComboBox.SelectedItem as Currency;
            if (currency != null)
            {
                this.Cursor = Cursors.WaitCursor;
                try
                {
                    new ForwardCurvesAnalyser(currency.Id).ShowDialog();
                }
                finally
                {
                    this.Cursor = Cursors.Default;
                }

            }
        }

        private void FundamentalVectorDataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            //e.Cancel = true;
        }

        private void ScenarioCopyClipboard(object sender, EventArgs e)
        {
            if (scenariosSetupDataGridView.EditingControl is TextBox)
            {
                Clipboard.SetText(((TextBox)scenariosSetupDataGridView.EditingControl).SelectedText);
            }
            else if (scenariosSetupDataGridView.SelectedCells.Count > 1)
            {
                scenariosSetupDataGridView.CopyToClipboardAsExcel();
            }

            this.AddSelectedRowsToClipboard();
        }

        private void AddSelectedRowsToClipboard()
        {
            int[] selectedRowsIndexes = new int[this.scenariosSetupDataGridView.SelectedRows.Count];
            for (int i = 0; i < selectedRowsIndexes.Length; i++) selectedRowsIndexes[i] = this.scenariosSetupDataGridView.SelectedRows[i].Index;
            int selectedRowCount = selectedRowsIndexes.Length;
            if (selectedRowCount > 0)
            {
                object[,] values = new object[this.scenariosSetupDataGridView.Columns.Count,selectedRowCount];
                for (int x = 0; x < this.scenariosSetupDataGridView.Columns.Count; x++)
                {
                    for (int y = 0; y < selectedRowCount; y++)
                        values[x, y] = this.scenariosSetupDataGridView[x, selectedRowsIndexes[y]].Value;
                }

                IDataObject dataObject = Clipboard.GetDataObject();
                if (dataObject != null)
                {
                    DataObject newData = new DataObject();
                    newData.SetData(AlmsScenarioRowsFormat, values);
                    string[] formats = dataObject.GetFormats();
                    foreach (var format in formats)
                        newData.SetData(format, dataObject.GetData(format));

                    Clipboard.SetDataObject(newData);
                }
            }
        }

        private void ScenarioPasteClipboard(object sender, EventArgs e)
        {
            IDataObject dataObject = Clipboard.GetDataObject();

            object[,] values = dataObject == null ?
                Clipboard.GetData(AlmsScenarioRowsFormat) as object[,] :
                dataObject.GetData(AlmsScenarioRowsFormat) as object[,];

            string[] formats = dataObject.GetFormats();

            if (values == null)
            {
                if (scenariosSetupDataGridView.EditingControl is TextBox)
                {
                    int index = ((TextBox)scenariosSetupDataGridView.EditingControl).SelectionStart;
                    if (index < 0) return;
                    string text = scenariosSetupDataGridView.EditingControl.Text.Remove(index, ((TextBox)scenariosSetupDataGridView.EditingControl).SelectionLength);
                    scenariosSetupDataGridView.EditingControl.Text = text.Insert(index, Clipboard.GetText());
                    ((TextBox)scenariosSetupDataGridView.EditingControl).Select(index + Clipboard.GetText().Length, 0);
                }
            }
            else
            {
                DataGridViewSelectedRowCollection selectedRows = scenariosSetupDataGridView.SelectedRows;
                for (int y = 0; y < selectedRows.Count && y < values.GetLength(1); y++)
                {
                    DataGridViewRow row = selectedRows[y];
                    if (row.ReadOnly) continue;
                    for (int x = 1; x < row.Cells.Count && x < values.GetLength(0); x++)
                        if (!row.Cells[x].ReadOnly && !scenariosSetupDataGridView.Columns[x].ReadOnly) row.Cells[x].Value = values[x, y];
                }
            }
        }

        private void ValidateScenarioCell(object sender, DataGridViewCellValidatingEventArgs e)
        {
            e.Cancel = false;
            if (e.ColumnIndex == 0)
            {
                foreach (Scenario scenario in scenarios)
                {
                    if (scenario.Name == e.FormattedValue as string
                        && scenario.Name != scenariosSetupDataGridView[e.ColumnIndex, e.RowIndex].Value as string)
                    {
                        e.Cancel = true;
                        break;
                    }
                }
            }
        }

        private void MoveScenario(object sender, EventArgs e)
        {
            DataGridView grid = scenariosSetupDataGridView;

            List<DataGridViewRow> selectedRows = new List<DataGridViewRow>();

            foreach (DataGridViewRow row in grid.Rows)
            {
                bool selected = false;
                foreach (DataGridViewCell cell in row.Cells) if (cell.Selected) selected = true;
                if (row.Selected) selected = true;
                if (selected) selectedRows.Add(row);
            }

            grid.CurrentCell = null;

            int move;
            if (sender == menuScenarioUp) move = -1;
            else if (sender == menuScenarioDown) move = 1;
            else throw new ArgumentException();

            int i;
            if (move > 0) i = grid.Rows.Count - 1;
            else i = 0;

            DataGridViewRow last = null;

            while (i >= 0 && i < grid.Rows.Count)
            {
                DataGridViewRow row = grid.Rows[i];
                if (!selectedRows.Contains(row))
                {
                    i += move * -1;
                    continue;
                }
                if (i == 0 && move == -1 || i == grid.Rows.Count - 1 && move == 1) return;

                grid.Rows.Remove(row);
                grid.Rows.Insert(i + move, row);
                row.Selected = false;
                last = row;

                i += move * -1;
            }

            if (last != null)
            {
                grid.ClearSelection();
                last.Selected = true;
                grid.CurrentCell = last.Cells[0];
            }

            this.SetScenariosPosition();
        }

        private void SetScenariosPosition()
        {
            for (int i = 0; i < scenariosSetupDataGridView.Rows.Count; i++)
            {
                string name = scenariosSetupDataGridView[0, i].Value as string;
                Scenario scenario = Scenario.FindByName(this.Session, name);
                if (scenario == null) continue;
                scenario.Position = i;
            }
        }

        private void valuationDateTimePicker_MouseCaptureChanged(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            MainForm.ModifiedValues = true;
            this.Cursor = Cursors.Arrow;
        }

        private void MoveImportantMarketData(object sender, EventArgs e)
        {
            bool up = sender == this.moveUpToolStripMenuItem || sender == this.btnImportantMarketDataUp;

            IEnumerable<DataGridViewRow> selectedRows = this.marketParametersDataGridView.SelectedRows.OfType<DataGridViewRow>();
            if (up) selectedRows = selectedRows.OrderBy(r => r.Index);
            else selectedRows = selectedRows.OrderByDescending(r => r.Index);

            List<DataGridViewRow> rows = this.marketParametersDataGridView.Rows.OfType<DataGridViewRow>().ToList();

            List<int> selectedIndexes = new List<int>();
            foreach (DataGridViewRow row in selectedRows)
            {
                int index = row.Index;
                if (index <= 0 && up || index >= this.marketParametersDataGridView.Rows.Count - 1 && !up) continue;
                rows.Remove(row);
                int newIndex = up ? index - 1 : index + 1;
                rows.Insert(newIndex, row);
                selectedIndexes.Add(newIndex);
            }

            using (ISession session = SessionManager.OpenSession())
            {
                for (int index = 0; index < rows.Count; index++)
                {
                    DataGridViewRow row = rows[index];
                    if (row.DataBoundItem is MarketParameters && ((MarketParameters)row.DataBoundItem).Vector != null)
                    {
                        ((MarketParameters)row.DataBoundItem).Vector.Position = index;
                        ((MarketParameters)row.DataBoundItem).Vector.Update(session);
                    }
                }
                session.Flush();
            }

            this._marketParametersDataGridView.Refresh(false, ValuationDate);

            this.marketParametersDataGridView.ClearSelection();
            foreach (int selectedIndex in selectedIndexes)
                this.marketParametersDataGridView.Rows[selectedIndex].Selected = true;
        }

        private void MarketDataMenuOpening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (this.marketParametersDataGridView.SelectedRows.Count == 0)
            {
                e.Cancel = true;
                return;
            }

            this.EnableUpDownButtons();
        }

        private void ImportantMarketDataSelectionChanged(object sender, EventArgs e)
        {
            this.EnableUpDownButtons();
        }

        private void EnableUpDownButtons()
        {
            DataGridViewRow[] rows =
                this.marketParametersDataGridView.Rows.OfType<DataGridViewRow>().OrderBy(r => r.Index).ToArray();
            DataGridViewRow[] selectedRows =
                this.marketParametersDataGridView.SelectedRows.OfType<DataGridViewRow>().OrderBy(r => r.Index).ToArray();

            bool upEnabled = selectedRows.Length > 0 && !selectedRows.Contains(rows.First());
            bool downEnabled = selectedRows.Length > 0 && !selectedRows.Contains(rows.Last());

            this.moveUpToolStripMenuItem.Enabled = upEnabled;
            this.btnImportantMarketDataUp.Enabled = upEnabled;
            this.downToolStripMenuItem.Enabled = downEnabled;
            this.btnImportantMarketDataDown.Enabled = downEnabled;
        }

        private void marketParamtersPanel_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Change_Click(object sender, EventArgs e)
        {
            DateTime oldValDate = MainForm.ValuationDate;
            ValuationDateChanger form = new ValuationDateChanger();
            form.ShowDialog();
            if (oldValDate != MainForm.ValuationDate)
            {
                MainForm.ValuationDate = Tools.ResetDateToFirstOfMonth(MainForm.ValuationDate);
                this.Cursor = Cursors.WaitCursor;
                valuationDateLabel.Text = MainForm.ValuationDate.ToString("MMM yyyy");
                Param.SetValuationDate(this.Session, MainForm.ValuationDate);
                this.Session.Flush();
                Tools.RedateVectorsAfterValuationDateChanged();
                _marketParametersDataGridView.Refresh(false, this.ValuationDate);
                this.Cursor = Cursors.Arrow;
            }
        }

        public void RefreshMarketParametersDataGrid()
        {
            _marketParametersDataGridView.Refresh(true, this.ValuationDate);
        }


    }
}
