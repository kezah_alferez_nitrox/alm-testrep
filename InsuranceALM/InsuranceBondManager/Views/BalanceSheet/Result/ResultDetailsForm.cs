﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using ALMS.Products;
using ALMSCommon;
using ALMSCommon.Controls.TreeGridView;
using InsuranceBondManager.Calculations;
using InsuranceBondManager.Core;
using InsuranceBondManager.Domain;
using InsuranceBondManager.Resources.Language;
using Eu.AlterSystems.ASNetLib.Core;
using ReportType = InsuranceBondManager.Calculations.ReportType;

namespace InsuranceBondManager.Views.BalanceSheet.Result
{
    public partial class ResultDetailsForm : Form
    {
        private readonly Simulation simulation;
        private readonly Category category;
        private readonly Mouvements[] sourceMouvements;
        private static string totalLabel = "TOTAL ---------";
        private ResultAggregator resultAggregator;
        

        private readonly IList<int> expandedIndexes = new List<int>();
        private Currency valuationCurrency;

        public ResultDetailsForm(Simulation simulation, Category category, Mouvements[] sourceMouvements, ReportType reportType, ReportAggregationType aggregationType, Scenario scenario)
        {
            this.simulation = simulation;
            this.category = category;
            this.sourceMouvements = sourceMouvements;
            this.InitializeComponent();

            this.KeyPreview = true;

            IList aggregationLevels = EnumHelper.ToList(typeof(ReportAggregationLevel));
            this.aggregateBy.ValueMember = "Key";
            this.aggregateBy.DisplayMember = "Value";
            this.aggregateBy.DataSource = aggregationLevels;
            this.aggregateBy.SelectedValue = ReportAggregationLevel.Product;

            IList reportTypes = EnumHelper.ToList(typeof(ReportType));
            if (!AlmConfiguration.IsComputationDaily())
                reportTypes.RemoveAt(reportTypes.Count - 1);
            this.reportTypeComboBox.ValueMember = "Key";
            this.reportTypeComboBox.DisplayMember = "Value";
            this.reportTypeComboBox.DataSource = reportTypes;
            this.reportTypeComboBox.SelectedValue = reportType;

            IList aggregationTypes = EnumHelper.ToList(typeof(ReportAggregationType));
            this.aggregationTypeComboBox.ValueMember = "Key";
            this.aggregationTypeComboBox.DisplayMember = "Value";
            this.aggregationTypeComboBox.DataSource = aggregationTypes;
            this.aggregationTypeComboBox.SelectedValue = aggregationType;

            IList resultDetailsFields = EnumHelper.ToList(typeof(ResultDetailsFields));
            this.defaultDataShow.ValueMember = "Key";
            this.defaultDataShow.DisplayMember = "Value";
            this.defaultDataShow.DataSource = resultDetailsFields;
            this.defaultDataShow.SelectedValue = ResultDetailsFields.BookValue_OrderByALMID;


            this.scenarioComboBox.ValueMember = "Self";
            this.scenarioComboBox.DisplayMember = "Name";
            this.scenarioComboBox.DataSource = this.simulation.Scenarios.ToArray();
            this.scenarioComboBox.SelectedValue = scenario;




            treegrid.NodeExpanded += TreegridNodeExpanded;

        }

        private static void TreegridNodeExpanded(object sender, ExpandedEventArgs e)
        {
            DataGridView dataGridView = e.Node.DataGridView;
            for (int i = 0; i < dataGridView.RowCount; i++)
            {
                dataGridView.AutoResizeRow(i);
            }
            dataGridView.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);
        }

        private void Reload()
        {
            this.Cursor = Cursors.WaitCursor;

            for (int index = 0; index < this.treegrid.Nodes.Count; index++)
            {
                TreeGridNode node = this.treegrid.Nodes[index];
                if (node.IsExpanded)
                {
                    expandedIndexes.Add(index);
                }
            }

            this.treegrid.Nodes.Clear();
            this.treegrid.Columns.Clear();

            if (this.reportTypeComboBox.SelectedValue == null ||
                this.aggregationTypeComboBox.SelectedValue == null ||
                this.scenarioComboBox.SelectedValue == null)
            {
                this.Cursor = Cursors.Arrow;
                return;
            }

            ReportType reportType = (ReportType)this.reportTypeComboBox.SelectedValue;
            ReportAggregationType aggregationType = (ReportAggregationType)this.aggregationTypeComboBox.SelectedValue;
            Scenario scenario = (Scenario)this.scenarioComboBox.SelectedValue;
            ResultDetailsFields fieldToShow = (ResultDetailsFields)this.defaultDataShow.SelectedValue;
            

            ScenarioData scenarioData = this.simulation[scenario];

            this.resultAggregator = new ResultAggregator(reportType, aggregationType, scenarioData.ValuationDate, scenarioData.ResultMonths);
            this.valuationCurrency = scenarioData.ValuationCurrency;

            ProductData[] productDatas = null;
            if (category != null)
            {
                if (category.Type == CategoryType.InterestReceivedOnDerivatives)
                {
                    productDatas = scenarioData.ProductDatas.
                        Where(p => p.Product.IsDerivative && p.Product.BalanceSign * p.Product.SwapLegSign > 0).ToArray();
                }
                else if (category.Type == CategoryType.InterestPayedOnDerivatives)
                {
                    productDatas = scenarioData.ProductDatas.
                        Where(p => p.Product.IsDerivative && p.Product.BalanceSign * p.Product.SwapLegSign < 0).ToArray();
                }
                else
                {
                    productDatas = scenarioData.GetProductDatasByCategory(this.category.FullPath());
                }
            }

            if ((productDatas == null || productDatas.Length == 0) && sourceMouvements == null)
            {
                this.Cursor = Cursors.Arrow;
                return;
            }

            this.CreateColumns();
            treegrid.DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            if (productDatas != null)
            {
                this.CreateProductDatasRows("", this.treegrid.Nodes, productDatas, false, fieldToShow, (this.category == null ? "" : this.category.FullPath()));
            }
            this.Text = "Detail " + (this.category == null ? "" : "on " + this.category.FullPath());
            if (sourceMouvements != null)
            {
                this.CreateMouvementsRow("", this.treegrid.Nodes, sourceMouvements);
            }

            foreach (int index in expandedIndexes)
            {
                treegrid.Nodes[index].Expand();
            }
            expandedIndexes.Clear();

            //for (int i = 0; i < treegrid.RowCount; i++)
            //{
            //    treegrid.AutoResizeRow(i);
            //}

            this.treegrid.AutoResizeColumns();
            this.treegrid.Columns[0].Width = 200;
            this.Cursor = Cursors.Arrow;
            this.ActiveControl = this.treegrid;
        }

        private void CreateProductDatasRows(string name, TreeGridNodeCollection nodes, IEnumerable<ProductData> datas,
                                bool showIndex, ResultDetailsFields fieldToShow, string parentCategory = "")
        {
            bool convertFxRate = !productCurrencyRadioButton.Checked;

            //add subtotal fake product / MTU
            int resultMonths = 0;
            if (datas != null && datas.ToList().Count > 0 && datas.ToList()[0] != null)
                resultMonths = datas.ToList()[0].BookPrice.Length;
            ProductData fakeSubtotalProductData = new ProductData(resultMonths);
            fakeSubtotalProductData.Product = new Product();

            fakeSubtotalProductData.Product.ALMIDDisplayed = totalLabel;
            fakeSubtotalProductData.Currency = valuationCurrency;
            double bookValue;
            int categoriesCount = 0;
            IDictionary<string, ProductData> fakeCategoryProductData = null;
            IEnumerable<string> categories = null;
            if ((ReportAggregationLevel)aggregateBy.SelectedValue == ReportAggregationLevel.Category)
            {
                categories = datas.Select(p => p.Product.Category.FullPath()).Distinct();
                categoriesCount = categories.Count();
                fakeCategoryProductData = new Dictionary<string, ProductData>(categoriesCount);

                ProductData pd = null;
                foreach (string category in categories)
                {
                    pd = new ProductData(resultMonths);
                    pd.Product = new Product();
                    pd.Product.ALMIDDisplayed = category;
                    fakeCategoryProductData.Add(new KeyValuePair<string, ProductData>(category, pd));
                }
            }
            for (int i = 0; i < resultMonths; i++)
            {
                bookValue = 0;
                foreach (ProductData pd in datas)
                {
                    if ((ReportAggregationLevel)aggregateBy.SelectedValue == ReportAggregationLevel.Category)
                    {
                        fakeCategoryProductData[pd.Product.Category.FullPath()].BookPrice[i] = 100;
                        fakeCategoryProductData[pd.Product.Category.FullPath()].ActuarialPrice[i] = 100;
                        fakeCategoryProductData[pd.Product.Category.FullPath()].CouponIncome[i] += pd.CouponIncome[i] * pd.FxRate[i];
                        fakeCategoryProductData[pd.Product.Category.FullPath()].InterestIncome[i] += pd.InterestIncome[i] * pd.FxRate[i];
                        fakeCategoryProductData[pd.Product.Category.FullPath()].PrincipalIncome[i] += pd.PrincipalIncome[i] * pd.FxRate[i];
                        fakeCategoryProductData[pd.Product.Category.FullPath()].GainAndLosesIncome[i] += pd.GainAndLosesIncome[i] * pd.FxRate[i];
                        fakeCategoryProductData[pd.Product.Category.FullPath()].RemainPrincipal[i] += pd.RemainPrincipal[i] * pd.FxRate[i];
                        fakeCategoryProductData[pd.Product.Category.FullPath()].LossImpairment[i] += pd.LossImpairment[i] * pd.FxRate[i];
                        fakeCategoryProductData[pd.Product.Category.FullPath()].FxPnlIncome[i] += pd.FxPnlIncome[i];
                        if (pd.Product.IsModel)
                        {
                            fakeCategoryProductData[pd.Product.Category.FullPath()].Product.IsModel = true;
                            fakeCategoryProductData[pd.Product.Category.FullPath()].LaggedRecovery[i] += pd.LaggedRecovery[i] * pd.FxRate[i];
                            fakeCategoryProductData[pd.Product.Category.FullPath()].Default[i] += pd.Default[i] * pd.FxRate[i];
                        }
                        fakeCategoryProductData[pd.Product.Category.FullPath()].CurrFace[i] += pd.GetBookValueInValuationCurrency(i);
                    }
                    fakeSubtotalProductData.BookPrice[i] = 100;
                    fakeSubtotalProductData.ActuarialPrice[i] = 100;

                    bookValue += pd.GetBookValueInValuationCurrency(i);
                    fakeSubtotalProductData.CouponIncome[i] += pd.CouponIncome[i] * pd.FxRate[i];

                    //fakeSubtotalProductData.CouponValues[i] += pd.CouponValues[i] * pd.FxRate[i];
                    fakeSubtotalProductData.InterestIncome[i] += pd.InterestIncome[i] * pd.FxRate[i];
                    fakeSubtotalProductData.PrincipalIncome[i] += pd.PrincipalIncome[i] * pd.FxRate[i];
                    fakeSubtotalProductData.GainAndLosesIncome[i] += pd.GainAndLosesIncome[i] * pd.FxRate[i];
                    //fakeSubtotalProductData.FxPnlIncome[i] += (double.IsNaN(pd.FxPnlIncome[i]) || double.IsInfinity(pd.FxPnlIncome[i]) ? 0 : pd.FxPnlIncome[i]) * pd.FxRate[i];
                    fakeSubtotalProductData.RemainPrincipal[i] += pd.RemainPrincipal[i] * pd.FxRate[i];
                    fakeSubtotalProductData.LossImpairment[i] += pd.LossImpairment[i] * pd.FxRate[i];
                    //fakeSubtotalProductData.IRCValue[i] += pd.IRCValue[i] * pd.FxRate[i];

                    if (pd.Product.IsModel)
                    {
                        fakeSubtotalProductData.Product.IsModel = true;
                        fakeSubtotalProductData.LaggedRecovery[i] += pd.LaggedRecovery[i] * pd.FxRate[i];
                        fakeSubtotalProductData.Default[i] += pd.Default[i] * pd.FxRate[i];
                    }

                }
                fakeSubtotalProductData.CurrFace[i] = bookValue;
                //fakeSubtotalProductData.Product.Category = fakeCateg;
            }

            List<ProductData> finalDatas;
            if ((ReportAggregationLevel)aggregateBy.SelectedValue == ReportAggregationLevel.Category)
            {
                datas = fakeCategoryProductData.Values.ToArray();
            }
            if (fieldToShow != ResultDetailsFields.BookValue_OrderByALMID)
                finalDatas = datas.OrderByDescending(p => GetDataToShow(fieldToShow, p)[0]).ToList();
            else
                finalDatas = datas.OrderBy(p => p.Product.ALMID).ToList();

            finalDatas.Add(fakeSubtotalProductData);

            int j = 0;
            foreach (ProductData productData in finalDatas)
            {
                j++;
                if (productData.GetBookValues().Length > 1)
                {
                    string rowName;
                    if ((ReportAggregationLevel)aggregateBy.SelectedValue == ReportAggregationLevel.Category)
                    {
                        rowName = productData.Product.ALMIDDisplayed;
                    }
                    else
                    {
                        if (!productData.Product.IsDerivative)
                            rowName = productData.Product.Category != null ? (productData.Product.Category.Label == parentCategory ? productData.Product.Category.FullPath().Substring(productData.Product.Category.Label.Length + 1) : parentCategory) + "/" + productData.Product.ALMIDDisplayed : productData.Product.ALMIDDisplayed;
                        else
                            rowName = (productData.Product.Category != null ? productData.Product.Category.FullPath() : parentCategory) + "/" + productData.Product.ALMIDDisplayed;
                    }
                    if (!string.IsNullOrWhiteSpace(productData.Product.Description)) rowName += " " + productData.Product.Description;
                    if (!string.IsNullOrWhiteSpace(name)) rowName += " " + name;
                    if (showIndex) rowName += " " + j;
                    Currency currency = convertFxRate ? this.valuationCurrency : (productData.Currency ?? this.valuationCurrency);
                    rowName += " (" + currency.Symbol + ")";


                    TreeGridNode node = this.CreateRow(rowName, nodes, GetDataToShow(fieldToShow, productData), false, convertFxRate, productData.FxRate, false, productData.Product.ALMIDDisplayed == totalLabel);
                    if (productData.Product.ALMIDDisplayed != totalLabel) this.CreateRow(Labels.ResultDetailsForm_CreateRows_Book_Price, node.Nodes, productData.BookPrice, false);
                    if (productData.Product.ALMIDDisplayed != totalLabel) this.CreateRow(Labels.ResultDetailsForm_CreateRows_Actuarial_Price, node.Nodes,
                                   productData.ActuarialPrice, false);
                    this.CreateRow(Labels.ResultDetailsForm_CreateRows_Curr_Face, node.Nodes, productData.CurrFace, false, convertFxRate, productData.FxRate);
                    this.CreateRow(Labels.ResultDetailsForm_CreateRows_Coupon_Income, node.Nodes, productData.CouponIncome,
                                   true, convertFxRate, productData.FxRate);
                    //this.CreateRow("Coupon Values", node.Nodes, productData.CouponValues,true, convertFxRate, productData.FxRate);
                    this.CreateRow(Labels.ResultDetailsForm_CreateRows_Interest_Income, node.Nodes,
                                   productData.InterestIncome, true, convertFxRate, productData.FxRate);
                    this.CreateRow(Labels.ResultDetailsForm_CreateRows_Principal_Income, node.Nodes,
                                   productData.PrincipalIncome, true, convertFxRate, productData.FxRate);
                    this.CreateRow(Labels.ResultDetailsForm_CreateRows_P_L, node.Nodes, productData.GainAndLosesIncome, true, convertFxRate, productData.FxRate);
                    if (productData.Product.ALMIDDisplayed != totalLabel) this.CreateRow("FX P&L (in " + currency.Symbol + ")", node.Nodes, productData.FxPnlIncome, true, !convertFxRate, productData.FxRate, true);
                    if (productData.Product.ALMIDDisplayed != totalLabel) this.CreateRow(Labels.ResultDetailsForm_CreateRows_Remain_Principal, node.Nodes,
                                   productData.RemainPrincipal, false);


                    this.CreateRow(Labels.ResultDetailsForm_CreateRows_Loss_Impairment, node.Nodes,
                                   productData.LossImpairment, true, convertFxRate, productData.FxRate);

                    //this.CreateRow("Real Interest Rate", node.Nodes,
                    //               productData.RealCouponRate, false);

                    if (productData.Product.Category != null)
                        this.CreateRow("IRC (" + productData.Product.Category.AnalysisCategory + "=>Treasuries)", node.Nodes,
                                   productData.IRCValue, true, convertFxRate, productData.FxRate);

                    if (productData.Product.IsModel)
                    {
                        this.CreateRow("Recovery (with lag)", node.Nodes,
                                       productData.LaggedRecovery, true, convertFxRate, productData.FxRate);
                        this.CreateRow("Default", node.Nodes,
                                       productData.Default, true, convertFxRate, productData.FxRate);
                    }
                    this.CreateMouvementsRow("Source", node.Nodes, productData.Mouvements);

                    if (productData.Product.InvestmentRule != ProductInvestmentRule.Constant)
                    {
                        this.CreateRow(Labels.ResultDetailsForm_CreateRows_Investment_Parameter, node.Nodes,
                                   productData.InvestmentParameter, false);
                    }

                    RollProductData rollProductData = productData as RollProductData;
                    if (rollProductData != null)
                    {
                        this.CreateProductDatasRows(Labels.ResultDetailsForm_CreateRows_sub_roll, node.Nodes,
                                        rollProductData.SubRollDatas, true, fieldToShow, parentCategory);
                    }
                }
            }


        }

        private static double[] GetDataToShow(ResultDetailsFields fieldToShow, ProductData productData)
        {
            double[] dataToShow;
            switch (fieldToShow)
            {
                case ResultDetailsFields.BookValue:
                case ResultDetailsFields.BookValue_OrderByALMID:
                    dataToShow = productData.GetBookValues();
                    break;
                case ResultDetailsFields.InvestmentParameter:
                    dataToShow = productData.InvestmentParameter;
                    break;
                case ResultDetailsFields.LaggedRecovery:
                    dataToShow = productData.LaggedRecovery;
                    break;
                case ResultDetailsFields.Default:
                    dataToShow = productData.Default;
                    break;
                case ResultDetailsFields.LossImpairment:
                    dataToShow = productData.LossImpairment;
                    break;
                case ResultDetailsFields.RemainPrincipal:
                    dataToShow = productData.RemainPrincipal;
                    break;
                case ResultDetailsFields.FxPnlIncome:
                    dataToShow = productData.FxPnlIncome;
                    break;
                case ResultDetailsFields.GainAndLosesIncome:
                    dataToShow = productData.GainAndLosesIncome;
                    break;
                case ResultDetailsFields.PrincipalIncome:
                    dataToShow = productData.PrincipalIncome;
                    break;
                case ResultDetailsFields.InterestIncome:
                    dataToShow = productData.InterestIncome;
                    break;
                case ResultDetailsFields.CouponIncome:
                    dataToShow = productData.CouponIncome;
                    break;
                case ResultDetailsFields.CurrFace:
                    dataToShow = productData.CurrFace;
                    break;
                case ResultDetailsFields.ActuarialPrice:
                    dataToShow = productData.ActuarialPrice;
                    break;
                case ResultDetailsFields.BookPrice:
                    dataToShow = productData.BookPrice;
                    break;
                default:
                    dataToShow = productData.GetBookValues();
                    break;
            }
            return dataToShow;
        }

        private void CreateMouvementsRow(string name, TreeGridNodeCollection nodes, Mouvements[] mouvements)
        {
            if (mouvements == null || mouvements.Length == 0) return;

            int rowCount = mouvements.Max(m => m.Items.Count);

            for (int row = 0; row < rowCount; row++)
            {
                object[] data = new object[this.treegrid.ColumnCount];
                data[0] = name;
                for (int i = 0; i < data.Length - 1; i++)
                {
                    Mouvements mouvement = this.resultAggregator.DateIntervalValueGeneric(mouvements, i);
                    if (mouvement == null) continue;

                    if (mouvement.Items.Count > row)
                    {
                        Tuple<string, double> tuple = mouvement.Items[row];
                        data[i + 1] = string.Format("{0}: {1}", tuple.Item1, tuple.Item2.ToString("N2"));
                    }
                }

                nodes.Add(data);
            }
        }

        private TreeGridNode CreateRow(string name, TreeGridNodeCollection nodes, double[] values, bool isSum, bool convertFxRate = false, double[] fxRate = null, bool revertFxChange = false, bool isTotal = false)
        {
            object[] data = new object[this.treegrid.ColumnCount];
            data[0] = name;

            if (values != null)
            {
                if (convertFxRate && fxRate != null)
                {
                    Debug.Assert(values.Length == fxRate.Length);

                    double[] convertedValues = new double[values.Length];
                    for (int i = 0; i < values.Length; i++)
                    {
                        convertedValues[i] = values[i] * (revertFxChange ? 1 / fxRate[i] : fxRate[i]);
                    }
                    values = convertedValues;
                }

                double[] aggregatedValues = new double[this.treegrid.ColumnCount - 1];
                for (int i = 0; i < aggregatedValues.Length; i++)
                {
                    aggregatedValues[i] = isSum
                                              ? this.resultAggregator.DateIntervalSum(values, i)
                                              : this.resultAggregator.DateIntervalValue(values, i);
                }

                Array.Copy(aggregatedValues, 0, data, 1, data.Length - 1);
            }

            TreeGridNode node = nodes.Add(data);

            if (isTotal)
            {
                for (int i = 0; i < node.Cells.Count; i++)
                {
                    node.Cells[i].Style.Font = new Font(this.Font, FontStyle.Bold);
                    node.Cells[i].Style.BackColor = Color.Black;
                    node.Cells[i].Style.ForeColor = Color.White;
                    node.Cells[i].ToolTipText = "Step " + (i - 2).ToString();
                }
            }
            return node;
        }

        private void CreateColumns()
        {
            IList<DateInterval> dateIntervals = this.resultAggregator.DateIntervals;

            DataGridViewColumn[] columns = new DataGridViewColumn[dateIntervals.Count + 1];

            columns[0] = new TreeGridColumn
                             {
                                 HeaderText = Labels.ResultDetailsForm_CreateColumns_Product,
                                 AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells,
                                 //Width = 200,
                                 Frozen = true,
                             };

            for (int i = 1; i < dateIntervals.Count + 1; i++)
            {
                columns[i] = new DataGridViewTextBoxColumn
                                 {
                                     HeaderText = dateIntervals[i - 1].ToString(i == 1),
                                 };
                columns[i].DefaultCellStyle.Format = Constants.ResultNumericFormat;
                columns[i].FillWeight = 10;
            }

            this.treegrid.Columns.AddRange(columns);
        }

        private void ResultDetailsFormKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        private void ReportTypeComboBoxSelectedIndexChanged(object sender, EventArgs e)
        {
            this.Reload();
        }

        private void AggregationTypeComboBoxSelectedIndexChanged(object sender, EventArgs e)
        {
            this.Reload();
        }

        private void ScenarioComboBoxSelectedIndexChanged(object sender, EventArgs e)
        {
            this.Reload();
        }

        private void ProductCurrencyRadioButtonCheckedChanged(object sender, EventArgs e)
        {
            this.Reload();
        }

        private void defaultDataShow_SelectedIndexChanged(object sender, EventArgs e)
        {
            ResultDetailsFields fieldToShow = (ResultDetailsFields)this.defaultDataShow.SelectedValue;

            if (fieldToShow == ResultDetailsFields.CouponIncome ||
                fieldToShow == ResultDetailsFields.FxPnlIncome ||
                fieldToShow == ResultDetailsFields.GainAndLosesIncome ||
                fieldToShow == ResultDetailsFields.InterestIncome ||
                fieldToShow == ResultDetailsFields.LossImpairment ||
                fieldToShow == ResultDetailsFields.Default || 
                fieldToShow == ResultDetailsFields.PrincipalIncome)
            {
                if ((ReportAggregationType)this.aggregationTypeComboBox.SelectedValue != ReportAggregationType.Sum)
                {
                    this.aggregationTypeComboBox.SelectedValue = ReportAggregationType.Sum;
                    flickerAgregate();
                }
            }
            else
            {
                if ((ReportAggregationType)this.aggregationTypeComboBox.SelectedValue != ReportAggregationType.Final)
                {
                    this.aggregationTypeComboBox.SelectedValue = ReportAggregationType.Final;
                    flickerAgregate();
                }
                
            }
            this.Reload();
        }

        private void flickerAgregate()
        {
            Color original = this.label2.BackColor;
            for (int i = 0; i < 3; i++)
            {
                this.label2.BackColor = Color.Red;
                this.label2.ForeColor = Color.White;
                //this.aggregationTypeComboBox.BackColor = Color.Red;
                //this.aggregationTypeComboBox.ForeColor = Color.White;
                this.flickerPanel.BackColor = Color.Red;
                this.Refresh();
                Thread.Sleep(300);
                this.label2.BackColor = original;
                this.label2.ForeColor = Color.Black;
                this.flickerPanel.BackColor = original;
                //this.aggregationTypeComboBox.BackColor = original;
                //this.aggregationTypeComboBox.ForeColor = Color.Black;

                this.Refresh();
                Thread.Sleep(300);
            }
        }



        private void ResultDetailsForm_Shown(object sender, EventArgs e)
        {
            treegrid.Focus();
        }

        private void aggregateBy_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.Reload();
        }
    }
}