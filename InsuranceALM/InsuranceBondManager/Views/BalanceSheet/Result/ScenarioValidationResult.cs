using System;
using System.Collections.Generic;
using System.Windows.Forms;
using InsuranceBondManager.Calculations;
using InsuranceBondManager.Resources.Language;

namespace InsuranceBondManager.Views.BalanceSheet.Result
{
    public partial class ScenarioValidationResult : Form
    {
        private readonly Form parent;

        public ScenarioValidationResult(Form parent)
        {
            this.parent = parent;
            InitializeComponent();
        }

        public void DisplayMessages(List<ValidationMessage> messages, bool warningOnly = true)
        {
            messagesListBox.DisplayMember = "Message";
            messagesListBox.DataSource = messages;
            if (warningOnly)
            {
                this.label1.Text = "There have been some WARNINGS when trying to run scenario(s).";
                this.label2.Text = "Double click on the WARNING message to go to its source.";
            }
            else
            {
                this.label1.Text =
                    Labels
                        .ScenarioValidationResult_InitializeComponent_There_have_been_some_errors_when_trying_to_run_scenario_s__;
                this.label2.Text = Labels.ScenarioValidationResult_InitializeComponent_Double_click_on_the_error_message_to_go_to_its_source_;
            }
            if(!this.Visible) this.Show(parent);
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void messagesListBox_DoubleClick(object sender, EventArgs e)
        {
            ValidationMessage validationMessage = messagesListBox.SelectedItem as ValidationMessage;
            if (validationMessage == null) return;

            switch (validationMessage.Type)
            {
                case ValidationMessageType.Bond:
                    Constants.BalanceSheetForm.FindAndSelectProduct(validationMessage.Product, validationMessage.Property);
                    break;
                case ValidationMessageType.VariableValue:
                    Constants.BalanceSheetForm.FindAndSelectVariableValue(validationMessage.VariableValue);
                    break;
                default:
                    // nothing
                    break;
            }
        }
    }
}