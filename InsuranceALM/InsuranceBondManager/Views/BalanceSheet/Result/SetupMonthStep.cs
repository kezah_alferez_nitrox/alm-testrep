using System;
using System.Windows.Forms;
using InsuranceBondManager.Core;
using InsuranceBondManager.Domain;

namespace InsuranceBondManager.Views.BalanceSheet.Result
{
    public partial class SetupMonthStep : SessionForm
    {
        public SetupMonthStep()
        {
            InitializeComponent();

            dataGridView.AutoGenerateColumns = false;
        }

        private void SetupMonthStep_Load(object sender, System.EventArgs e)
        {
            dataGridView.Rows.Add(Param.ResultMonths(this.Session));

            DateTime now = DateTime.Now;
            DateTime startDate = new DateTime(now.Year, now.Month, DateTime.DaysInMonth(now.Year, now.Month));
            DateTime endDate = startDate.AddYears(Param.GetVectorYears(this.Session));

            // todo: load from database
            DateTime date = startDate;
            int i = 0;
            while (date < endDate)
            {
                dataGridView[0, i].Value = date;
                dataGridView[1, i].Value = 1;
                i++;
                date = date.AddMonths(1);
                date = new DateTime(date.Year, date.Month, DateTime.DaysInMonth(date.Year, date.Month));
            }
        }
    }
}