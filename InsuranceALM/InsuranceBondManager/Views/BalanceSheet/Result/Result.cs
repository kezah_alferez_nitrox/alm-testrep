using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using ALMSCommon;
using ALMSCommon.Forms;
using InsuranceBondManager.Calculations;
using InsuranceBondManager.Core;
using InsuranceBondManager.Core.BetaTools;
using InsuranceBondManager.Core.Export.MultiScenario;
using InsuranceBondManager.Domain;
using InsuranceBondManager.Reports;
using InsuranceBondManager.Resources.Language;
using Eu.AlterSystems.ASNetLib.Core;
using NHibernate;
using ReportType = InsuranceBondManager.Calculations.ReportType;

namespace InsuranceBondManager.Views.BalanceSheet.Result
{
    public partial class Result : SessionUserControl
    {
        private readonly ResultGridController resultController;
        private List<Product> productsWithNaNResults;

        private Dictionary<ReportAnalysisType, Dictionary<ReportType, Dictionary<ReportAggregationType, Dictionary<Scenario, ResultModel>>>> resultModels;
        private ScenarioValidationResult scenarioValidationResultForm;
        private Simulation simulation;
        private ReportType currentReportType;
        private ReportAnalysisType currentAnalysisType;
        private Scenario currentScenario;
        private ReportAggregationType currentAggregationType;

        private List<Form> openedForms = new List<Form>();

        public Result()
        {
            this.InitializeComponent();

            this.reportTypeComboBox.DisplayMember = "Value";
            this.reportTypeComboBox.ValueMember = "Key";
            IList dataSource = EnumHelper.ToList(typeof(ReportType));


            if (!AlmConfiguration.IsTreasuryManagementModule())
            {
                dataSource.RemoveAt(dataSource.Count - 1);
            }

            this.reportTypeComboBox.DataSource = dataSource;

            
            this.reportAnalysisTypeComboBox.DisplayMember = "Value";
            this.reportAnalysisTypeComboBox.ValueMember = "Key";
            IList dataSourceAnalysis = EnumHelper.ToList(typeof(ReportAnalysisType));

            this.reportAnalysisTypeComboBox.DataSource = dataSourceAnalysis;




#if DEBUG
            this.reportTypeComboBox.SelectedIndex = 4;
#endif

            if (AlmConfiguration.IsComputationDaily())
            {
                this.reportTypeComboBox.SelectedIndex = 5;
            }

            this.scenarioComboBox.ValueMember = "Self";

            this.aggregationTypeComboBox.SelectedIndex = 0;

            if (!this.DesignMode && !Tools.InDesignMode)
            {
                this.resultController = new ResultGridController(this.resultDataGridView);

                this.resultDataGridView.ImageList = this.treeImageList;
            }

            this.resultDataGridView.CellDoubleClick += this.GridCellDoubleClick;
        }

        private void GridCellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex <= 1) return;

            if (this.simulation == null || !this.simulation.IsSuccesfull) return;

            if (this.reportTypeComboBox.SelectedIndex < 0) return;
            ReportType reportType = (ReportType)this.reportTypeComboBox.SelectedValue;

            if (this.aggregationTypeComboBox.SelectedIndex < 0) return;
            ReportAggregationType aggregationType =
                this.aggregationTypeComboBox.SelectedIndex == 0 ?
                ReportAggregationType.Final :
                ReportAggregationType.Average;

            Scenario scenario = this.scenarioComboBox.SelectedItem as Scenario;
            if (scenario == null) return;

            ResultRowModel resultRowModel = this.resultDataGridView.Rows[e.RowIndex].Tag as ResultRowModel;
            if (resultRowModel == null) return;
            if (resultRowModel.Category == null && resultRowModel.SourceMouvements == null) return;

            if (this.simulation.Count <= 0) return;

            ScenarioData scenarioData = this.simulation[scenario];
            if (scenarioData == null) return;

            if (!scenarioData.HasProductDetails)
            {
                Dialogs.Info("Result Details", "The Result Details are not available.\nTo enable them please go to the menu \"Options\" -> \"Parameters\", check \"Per product result details\" option then press \"Calculate\".");
                return;
            }
            this.Cursor = Cursors.WaitCursor;
            ResultDetailsForm resultDetailsForm = new ResultDetailsForm(this.simulation, resultRowModel.Category, resultRowModel.SourceMouvements, reportType, aggregationType, scenario);
            this.Cursor = Cursors.Arrow;
            openedForms.Add(resultDetailsForm);

            resultDetailsForm.Show(this.ParentForm);
        }

        public void BeforeRunSimulation()
        {
            if (this.scenarioValidationResultForm != null && !this.scenarioValidationResultForm.IsDisposed &&
                this.scenarioValidationResultForm.Visible)
                this.scenarioValidationResultForm.Close();

            this.ClearData();
        }

        public void ClearData()
        {
            foreach (Form form in openedForms)
            {
                form.Close();
            }
            openedForms.Clear();

            this.resultDataGridView.Rows.Clear();
            this.resultModels = null;
            this.simulation = null;
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }

        public void RunSimulation(BackgroundWorker backgroundWorker)
        {
            try
            {
                this.simulation = new Simulation();
                this.simulation.Run(backgroundWorker);
            }
            catch (Exception ex)
            {
                Log.Exception("this.simulation.Run(backgroundWorker)", ex);

                MessageBox.Show(ex.Message, Labels.Result_RunSimulation_Error,
                                MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void RefreshResultGrid()
        {
            
            if (this.simulation == null || !this.simulation.IsSuccesfull) return;

            if (!Param.GetComputeAnalysisCategories(this.Session))
            {
                reportAnalysisTypeComboBox.Visible = false;
                reportAnalysisTypeComboBox.SelectedIndex = 0;
                label10.Visible = false;

            }
            else
            {
                reportAnalysisTypeComboBox.Visible = true;
                label10.Visible = true;
            }

            this.ClearSession();

            if (this.reportTypeComboBox.SelectedIndex < 0) return;
            this.currentReportType = (ReportType)this.reportTypeComboBox.SelectedValue;
            if (this.reportAnalysisTypeComboBox.SelectedIndex < 0)
                this.reportAnalysisTypeComboBox.SelectedIndex = 0;
            this.currentAnalysisType = (ReportAnalysisType)this.reportAnalysisTypeComboBox.SelectedValue;

            if (this.aggregationTypeComboBox.SelectedIndex < 0) return;
            this.currentAggregationType = this.aggregationTypeComboBox.SelectedIndex == 0 ?
                ReportAggregationType.Final :
                ReportAggregationType.Average;

            this.currentScenario = this.scenarioComboBox.SelectedItem as Scenario;
            if (this.currentScenario == null) return;

            this.CalculateResultModels();

            this.showErrorsButton.Visible = this.productsWithNaNResults.Count > 0;

            ResultModel resultModel = this.resultModels[this.currentAnalysisType][this.currentReportType][this.currentAggregationType][this.currentScenario];

            if (!string.IsNullOrEmpty(BetaResultCSV.BetaResultFileNameCSV) && BetaResultCSV.SaveBetaResultAsCSV)
            {
                ResultModel betaResultModel = this.resultModels[this.currentAnalysisType][ReportType.Monthly]
                                                               [this.currentAggregationType][this.currentScenario];
                BetaResultCSV betaResultCSV = new BetaResultCSV(betaResultModel, this.currentAnalysisType, this.simulation, this.currentScenario);
                if (!betaResultCSV.BetaOutputResultToCSV()) return;
            }

            PersistOpeningBalanceForBalanceSheet(this.Session);


            this.resultController.Refresh(resultModel, currentAnalysisType);
        }

        private void PersistOpeningBalanceForBalanceSheet(ISession session)
        {
            Scenario first = Scenario.FindAll(session).First(s => s.IsDefault);

            MainForm.OpeningBalance = new Dictionary<int, double>();
            foreach (Category categ in this.simulation[first].Categories)
            {
                if (this.simulation[first].CategoryBookValue.ContainsKey(categ.Id))
                    MainForm.OpeningBalance[categ.Id] = this.simulation[first].CategoryBookValue[categ.Id][0];
                else
                    MainForm.OpeningBalance[categ.Id] = 0;
            }
        }

        private void CalculateResultModels()
        {
            this.productsWithNaNResults = new List<Product>();

            if (this.resultModels == null)
            {
                Category[] categories = Category.FindAll(this.Session);
                DateTime valuationDate = Param.GetValuationDate(this.Session).GetValueOrDefault(DateTime.Today);
                this.resultModels = new Dictionary<ReportAnalysisType, Dictionary<ReportType, Dictionary<ReportAggregationType, Dictionary<Scenario, ResultModel>>>>();

                foreach (ReportAnalysisType reportAnalysisType in Enum.GetValues(typeof(ReportAnalysisType)).Cast<ReportAnalysisType>())
                {
                    this.resultModels[reportAnalysisType] = new Dictionary<ReportType, Dictionary<ReportAggregationType, Dictionary<Scenario, ResultModel>>>();

                    foreach (ReportType reportType in Enum.GetValues(typeof(ReportType)).Cast<ReportType>())
                    {
                        this.resultModels[reportAnalysisType][reportType] = new Dictionary<ReportAggregationType, Dictionary<Scenario, ResultModel>>();
                        foreach (ReportAggregationType aggregationType in Enum.GetValues(typeof(ReportAggregationType)).Cast<ReportAggregationType>())
                        {
                            this.resultModels[reportAnalysisType][reportType][aggregationType] = new Dictionary<Scenario, ResultModel>();
                            foreach (Scenario scenario in this.simulation.Scenarios)
                            {
                                ScenarioData scenarioData = this.simulation[scenario];

                                this.CheckNaN(scenarioData.ProductDatas);

                                ResultModel resultModel = new ResultModel(scenarioData, categories, reportType, aggregationType, valuationDate, reportAnalysisType);
                                this.resultModels[reportAnalysisType][reportType][aggregationType][scenario] = resultModel;
                            }
                        }
                    }
                }
            }
        }

        private void CheckNaN(IEnumerable<ProductData> productDatas)
        {
            if (productDatas == null) return;
            foreach (ProductData productData in productDatas)
            {
                int length = productData.BookPrice.Length;
                for (int i = 0; i < length; i++)
                {
                    if (double.IsNaN(productData.BookPrice[i]) ||
                        double.IsNaN(productData.CurrFace[i]) ||
                        double.IsNaN(productData.CouponIncome[i]))
                    {
                        this.productsWithNaNResults.Add(productData.Product);
                        break;
                    }
                }
            }
        }

        public bool SimulationEnd()
        {
            if (this.simulation.IsSuccesfull)
            {
                this.scenarioComboBox.DataSource = Scenario.FindSelected(this.Session);
                // this.RefreshResultGrid();
            }
            else
            {
                if (this.simulation.ValidationErrors != null && this.simulation.ValidationErrors.Count > 0)
                {
                    this.DisplayValidationErrors(this.simulation.ValidationErrors);
                }
            }

            if (this.simulation.ValidationWarnings != null && this.simulation.ValidationWarnings.Count > 0)
            {
                this.DisplayValidationErrors(this.simulation.ValidationWarnings);
            }


            return this.simulation.IsSuccesfull;
        }

        private void DisplayValidationErrors(List<ValidationMessage> errors, bool warningOnly=true)
        {
            if (this.scenarioValidationResultForm == null || this.scenarioValidationResultForm.IsDisposed)
                this.scenarioValidationResultForm = new ScenarioValidationResult(this.ParentForm);
            this.scenarioValidationResultForm.DisplayMessages(errors, warningOnly);
        }

        private void ResultLoad(object sender, EventArgs e)
        {
            if (this.DesignMode || Tools.InDesignMode) return;

            this.scenarioComboBox.DataSource = Scenario.FindSelected(this.Session);
        }

        private void ReportTypeComboBoxSelectedIndexChanged(object sender, EventArgs e)
        {
            this.userDefinedButton.Visible = (this.reportTypeComboBox.SelectedIndex == 5);

            this.RefreshResultGrid();
        }
        private void ReportAnalysisTypeComboBoxSelectedIndexChanged(object sender, EventArgs e)
        {
            //this.userDefinedButton.Visible = (this.reportTypeComboBox.SelectedIndex == 5);

            this.RefreshResultGrid();
        }

        private void ScenarioComboBoxSelectedIndexChanged(object sender, EventArgs e)
        {
            this.RefreshResultGrid();
        }

        private void UserDefinedButtonClick(object sender, EventArgs e)
        {
            (new SetupMonthStep()).ShowDialog();
        }

        private void ConfigButtonClick(object sender, EventArgs e)
        {
            if (Tools.ShowDialog(new Config()) == DialogResult.OK)
            {
                this.resultModels = null;
                this.RefreshResultGrid();
            }
        }

        public void ClipboardCopy()
        {
            this.resultController.ClipboardCopy();
        }

        private void PrintButtonClick(object sender, EventArgs e)
        {
            if (this.resultDataGridView.Rows.Count != 0)
            {
                ShowReport();
                //PrintForm pf = new PrintForm(this.resultDataGridView);
                //pf.ShowDialog();
            }
        }

        private void ShowReport()
        {
            if (this.resultModels == null) return;

            LoadForm.Instance.DoWork(new WorkParameters(StartRenderReport, EndRenderReport));
        }

        private void StartRenderReport(BackgroundWorker backgroundworker)
        {
            Dictionary<Scenario, ResultModel> models = this.resultModels[currentAnalysisType][currentReportType][currentAggregationType];
            ILicenceInfo licenceInfo = MainForm.Instance.LicenceInfo;
            string fullName = licenceInfo == null ? "" : licenceInfo.FirstName + " " + licenceInfo.LastName;
            string version = Assembly.GetEntryAssembly().GetName().Version.ToString();
            ALMAnalysisReport report = new ALMAnalysisReport(models, fullName, version, backgroundworker);
            report.Render();
        }

        private void EndRenderReport(Exception exception)
        {
        }

        public void InvalidateResult()
        {
            this.simulation = new Simulation(false);
            this.resultController.InvalidateResults();
        }

        private void ExportAllButtonClick(object sender, EventArgs e)
        {
            if (this.simulation != null && this.simulation.IsSuccesfull)
            {
                ReportType reportType = (ReportType)this.reportTypeComboBox.SelectedValue;
                if (reportType == ReportType.Monthly)
                {
                    reportType = ReportType.Quarterly;
                }
                DateTime valuationDate = Param.GetValuationDate(this.Session) ?? DateTime.Today;
                ReportAggregationType aggregationType =
                this.aggregationTypeComboBox.SelectedIndex == 0 ?
                        ReportAggregationType.Final :
                        ReportAggregationType.Average;
                MultiScenarioExporter exporter = new MultiScenarioExporter(this.simulation, reportType, aggregationType, valuationDate);

                TimeSpan ts = new TimeSpan(0, 0, int.Parse(ConfigurationManager.AppSettings["LAST_EXPORTALL_DURATION"]));

                LoadForm.Instance.DoWork(new WorkParameters(bw => exporter.Export(), null), ts);

                ConfigurationManager.AppSettings["LAST_EXPORTALL_DURATION"] =
                    LoadForm.Instance.WorkDurationInSeconds.ToString();
            }
        }

        private void GridContextMenuStripOpening(object sender, CancelEventArgs e)
        {
            e.Cancel = !this.resultController.CanShowScenarioComparison(this.simulation);
        }

        private void ScenarioComparisonToolStripMenuItemClick(object sender, EventArgs e)
        {
            if (this.resultModels.Count <= 0) return;
            this.resultController.ShowScenarioComparison(this.resultModels[currentAnalysisType]);
        }

        private void AggregationTypeComboBoxSelectedIndexChanged(object sender, EventArgs e)
        {
            this.RefreshResultGrid();
        }

        private void ShowErrorsButtonClick(object sender, EventArgs e)
        {
            List<ValidationMessage> messages = this.productsWithNaNResults.
                Distinct().
                Select(p => new ValidationMessage(p, "Invalid results")).ToList();

            this.DisplayValidationErrors(messages);
        }

        private void scenarioComboBox_DropDown(object sender, EventArgs e)
        {
            int width = this.scenarioComboBox.Width;
            Graphics g = this.scenarioComboBox.CreateGraphics();
            Font font = this.scenarioComboBox.Font;
            int vertScrollBarWidth =
                (this.scenarioComboBox.Items.Count > this.scenarioComboBox.MaxDropDownItems)
                ? SystemInformation.VerticalScrollBarWidth : 0;
            int newWidth;
            foreach (Scenario s in Scenario.FindAll(this.Session))
            {
                newWidth = (int)g.MeasureString(s.ToString(), font).Width
                    + vertScrollBarWidth;
                if (width < newWidth)
                {
                    width = newWidth + 20;
                }
            }
            this.scenarioComboBox.DropDownWidth = width;
        }
    }
}