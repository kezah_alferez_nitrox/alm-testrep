﻿using InsuranceBondManager.Resources.Language;

namespace InsuranceBondManager.Views.BalanceSheet.Result
{
    partial class Config
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.saveasbutton = new System.Windows.Forms.Button();
            this.savebutton = new System.Windows.Forms.Button();
            this.loadbutton = new System.Windows.Forms.Button();
            this.okButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.categoryTreeView = new InsuranceBondManager.Controls.CategoryTreeView();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.ProfitsLossesCheckBoxList = new System.Windows.Forms.CheckedListBox();
            this.showDecimalValueCheckBox = new System.Windows.Forms.CheckBox();
            this.showDynamicVariablesCheckBox = new System.Windows.Forms.CheckBox();
            this.panel1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.saveasbutton);
            this.panel1.Controls.Add(this.savebutton);
            this.panel1.Controls.Add(this.loadbutton);
            this.panel1.Controls.Add(this.okButton);
            this.panel1.Controls.Add(this.cancelButton);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 434);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(425, 44);
            this.panel1.TabIndex = 0;
            // 
            // saveasbutton
            // 
            this.saveasbutton.Location = new System.Drawing.Point(14, 9);
            this.saveasbutton.Name = "saveasbutton";
            this.saveasbutton.Size = new System.Drawing.Size(75, 23);
            this.saveasbutton.TabIndex = 7;
            this.saveasbutton.Text = global::InsuranceBondManager.Resources.Language.Labels.Config_InitializeComponent_Save_As___;
            this.saveasbutton.UseVisualStyleBackColor = true;
            this.saveasbutton.Visible = false;
            // 
            // savebutton
            // 
            this.savebutton.Location = new System.Drawing.Point(95, 9);
            this.savebutton.Name = "savebutton";
            this.savebutton.Size = new System.Drawing.Size(75, 23);
            this.savebutton.TabIndex = 7;
            this.savebutton.Text = global::InsuranceBondManager.Resources.Language.Labels.Config_InitializeComponent_Save;
            this.savebutton.UseVisualStyleBackColor = true;
            this.savebutton.Visible = false;
            // 
            // loadbutton
            // 
            this.loadbutton.Location = new System.Drawing.Point(176, 9);
            this.loadbutton.Name = "loadbutton";
            this.loadbutton.Size = new System.Drawing.Size(75, 23);
            this.loadbutton.TabIndex = 2;
            this.loadbutton.Text = global::InsuranceBondManager.Resources.Language.Labels.Config_InitializeComponent_Load;
            this.loadbutton.UseVisualStyleBackColor = true;
            this.loadbutton.Visible = false;
            // 
            // okButton
            // 
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okButton.Location = new System.Drawing.Point(257, 9);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(75, 23);
            this.okButton.TabIndex = 1;
            this.okButton.Text = global::InsuranceBondManager.Resources.Language.Labels.Config_InitializeComponent_OK;
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Click += new System.EventHandler(this.okButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point(338, 9);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 0;
            this.cancelButton.Text = global::InsuranceBondManager.Resources.Language.Labels.Config_InitializeComponent_Cancel;
            this.cancelButton.UseVisualStyleBackColor = true;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.showDecimalValueCheckBox);
            this.tabPage3.Controls.Add(this.showDynamicVariablesCheckBox);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(417, 408);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Options";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // showDecimalValueCheckBox
            // 
            this.showDecimalValueCheckBox.AutoSize = true;
            this.showDecimalValueCheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.showDecimalValueCheckBox.Location = new System.Drawing.Point(10, 37);
            this.showDecimalValueCheckBox.Name = "showDecimalValueCheckBox";
            this.showDecimalValueCheckBox.Size = new System.Drawing.Size(160, 17);
            this.showDecimalValueCheckBox.TabIndex = 2;
            this.showDecimalValueCheckBox.Text = "Show decimal value in result";
            this.showDecimalValueCheckBox.UseVisualStyleBackColor = true;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.categoryTreeView);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(417, 408);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Balance Sheet";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // categoryTreeView
            // 
            this.categoryTreeView.AllowDrop = true;
            this.categoryTreeView.CanOrganize = true;
            this.categoryTreeView.CheckBoxes = true;
            this.categoryTreeView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.categoryTreeView.Location = new System.Drawing.Point(3, 3);
            this.categoryTreeView.Name = "categoryTreeView";
            this.categoryTreeView.Size = new System.Drawing.Size(411, 402);
            this.categoryTreeView.TabIndex = 0;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(425, 434);
            this.tabControl1.TabIndex = 1;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.ProfitsLossesCheckBoxList);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(417, 408);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = global::InsuranceBondManager.Resources.Language.Labels.Config_InitializeComponent_Profits___Losses;
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // ProfitsLossesCheckBoxList
            // 
            this.ProfitsLossesCheckBoxList.CheckOnClick = true;
            this.ProfitsLossesCheckBoxList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ProfitsLossesCheckBoxList.FormattingEnabled = true;
            this.ProfitsLossesCheckBoxList.Location = new System.Drawing.Point(3, 3);
            this.ProfitsLossesCheckBoxList.Name = "ProfitsLossesCheckBoxList";
            this.ProfitsLossesCheckBoxList.Size = new System.Drawing.Size(411, 402);
            this.ProfitsLossesCheckBoxList.TabIndex = 2;
            this.ProfitsLossesCheckBoxList.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.ProfitsLossesCheckBoxList_ItemCheck);
            // 
            // showDynamicVariablesCheckBox
            // 
            this.showDynamicVariablesCheckBox.AutoSize = true;
            this.showDynamicVariablesCheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.showDynamicVariablesCheckBox.Location = new System.Drawing.Point(10, 15);
            this.showDynamicVariablesCheckBox.Name = "showDynamicVariablesCheckBox";
            this.showDynamicVariablesCheckBox.Size = new System.Drawing.Size(179, 17);
            this.showDynamicVariablesCheckBox.TabIndex = 1;
            this.showDynamicVariablesCheckBox.Text = "Show dynamic variables in result";
            this.showDynamicVariablesCheckBox.UseVisualStyleBackColor = true;
            // 
            // Config
            // 
            this.AcceptButton = this.okButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(425, 478);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "Config";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Config";
            this.Load += new System.EventHandler(this.ConfigLoad);
            this.panel1.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button loadbutton;
        private System.Windows.Forms.Button saveasbutton;
        private System.Windows.Forms.Button savebutton;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage1;
        private InsuranceBondManager.Controls.CategoryTreeView categoryTreeView;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.CheckedListBox ProfitsLossesCheckBoxList;
        private System.Windows.Forms.CheckBox showDynamicVariablesCheckBox;
        private System.Windows.Forms.CheckBox showDecimalValueCheckBox;
    }
}