using System;
using System.Windows.Forms;
using InsuranceBondManager.Core;
using InsuranceBondManager.Resources.Language;

namespace InsuranceBondManager.Views.BalanceSheet.Result
{
    public partial class DividendForm : SessionForm
    {
        private readonly DividendGridController dividedGridController;

        public DividendForm()
        {
            this.InitializeComponent();

            if (!this.DesignMode && !Tools.InDesignMode)
            {
                this.dividedGridController = new DividendGridController(this.grid, this.Session);
            }
        }

        private void DividendFormLoad(object sender, EventArgs e)
        {
            this.dividedGridController.Refresh();
        }

        private void AddButtonClick(object sender, EventArgs e)
        {
            AddNewRowsPrompt addNewRowsPrompt = new AddNewRowsPrompt();
            addNewRowsPrompt.Text = Labels.DividendForm_addButton_Click_Dividend;
            if (addNewRowsPrompt.ShowDialog() == DialogResult.OK)
            {
               this.dividedGridController.AddNewItems(addNewRowsPrompt.RowCount);
            }
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            this.dividedGridController.SaveDividend();
        }

        private void DeleteToolStripMenuItemClick(object sender, EventArgs e)
        {
            this.dividedGridController.DeleteSelected();           
        }

        private void GridMouseDown(object sender, MouseEventArgs e)
        {
            DataGridView.HitTestInfo hti = this.grid.HitTest(e.X, e.Y);
            if (hti.Type == DataGridViewHitTestType.Cell && this.grid.SelectedCells.Count <= 1)
            {
                this.grid.ClearSelection();

                this.grid.Rows[hti.RowIndex].Cells[hti.ColumnIndex].Selected = true;
            }
        }
    }
}