using InsuranceBondManager.Resources.Language;

namespace InsuranceBondManager.Views.BalanceSheet.Result
{
    partial class PrintForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.excelRadioButton = new System.Windows.Forms.RadioButton();
            this.pdfRadioButton = new System.Windows.Forms.RadioButton();
            this.printButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.csvRadioButton = new System.Windows.Forms.RadioButton();
            this.SuspendLayout();
            // 
            // excelRadioButton
            // 
            this.excelRadioButton.AutoSize = true;
            this.excelRadioButton.Location = new System.Drawing.Point(86, 28);
            this.excelRadioButton.Name = "excelRadioButton";
            this.excelRadioButton.Size = new System.Drawing.Size(51, 17);
            this.excelRadioButton.TabIndex = 4;
            this.excelRadioButton.Text = Labels.PrintForm_InitializeComponent_Excel;
            this.excelRadioButton.UseVisualStyleBackColor = true;
            // 
            // pdfRadioButton
            // 
            this.pdfRadioButton.AutoSize = true;
            this.pdfRadioButton.Location = new System.Drawing.Point(153, 28);
            this.pdfRadioButton.Name = "pdfRadioButton";
            this.pdfRadioButton.Size = new System.Drawing.Size(46, 17);
            this.pdfRadioButton.TabIndex = 1;
            this.pdfRadioButton.Text = Labels.PrintForm_InitializeComponent_PDF;
            this.pdfRadioButton.UseVisualStyleBackColor = true;
            // 
            // printButton
            // 
            this.printButton.Location = new System.Drawing.Point(12, 86);
            this.printButton.Name = "printButton";
            this.printButton.Size = new System.Drawing.Size(75, 23);
            this.printButton.TabIndex = 2;
            this.printButton.Text = Labels.PrintForm_InitializeComponent_Ok;
            this.printButton.UseVisualStyleBackColor = true;
            this.printButton.Click += new System.EventHandler(this.PrintButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(134, 86);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 3;
            this.cancelButton.Text = Labels.PrintForm_InitializeComponent_Cancel;
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // csvRadioButton
            // 
            this.csvRadioButton.AutoSize = true;
            this.csvRadioButton.Checked = true;
            this.csvRadioButton.Location = new System.Drawing.Point(23, 28);
            this.csvRadioButton.Name = "csvRadioButton";
            this.csvRadioButton.Size = new System.Drawing.Size(46, 17);
            this.csvRadioButton.TabIndex = 3;
            this.csvRadioButton.TabStop = true;
            this.csvRadioButton.Text = Labels.PrintForm_InitializeComponent_CSV;
            this.csvRadioButton.UseVisualStyleBackColor = true;
            // 
            // PrintForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(221, 121);
            this.Controls.Add(this.csvRadioButton);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.printButton);
            this.Controls.Add(this.pdfRadioButton);
            this.Controls.Add(this.excelRadioButton);
            this.Name = "PrintForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = Labels.PrintForm_InitializeComponent_Print;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton excelRadioButton;
        private System.Windows.Forms.RadioButton pdfRadioButton;
        private System.Windows.Forms.Button printButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.RadioButton csvRadioButton; 
    }
}