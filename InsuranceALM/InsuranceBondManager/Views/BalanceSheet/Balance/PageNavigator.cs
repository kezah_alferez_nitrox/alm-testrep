﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace InsuranceBondManager.Views.BalanceSheet.Balance
{
    public partial class PageNavigator : UserControl
    {
        private int pageIndex;
        public int PageIndex
        {
            get { return this.pageIndex; }
            set
            {
                if (this.pageIndex == value) return;
                if (this.PageBeforeChange != null) this.PageBeforeChange.Invoke(this, EventArgs.Empty);
                this.pageIndex = value;
                this.txtPageIndex.Text = (this.PageIndex + 1).ToString();
                if (this.PageChanged != null) this.PageChanged.Invoke(this, EventArgs.Empty);
            }
        }

        private int maxPage;
        public int MaxPage
        {
            get { return this.maxPage; }
            set
            {
                this.lblPageCount.Text = "/ " + (value + 1);
                if (this.maxPage == value) return;
                this.maxPage = value;
                if (this.MaxPageChanged != null)
                {
                    if (this.PageIndex > this.MaxPage) this.PageIndex = this.MaxPage;
                    this.MaxPageChanged.Invoke(this, EventArgs.Empty);
                }
            }
        }

        public event EventHandler PageBeforeChange;
        public event EventHandler PageChanged;
        public event EventHandler MaxPageChanged;

        public PageNavigator()
        {
            InitializeComponent();
        }

        public void RefreshPage(int page)
        {
            if (this.PageIndex != page) this.PageIndex = page;
            else
            {
                this.txtPageIndex.Text = (this.PageIndex + 1).ToString();
                if (this.PageChanged != null) this.PageChanged.Invoke(this, EventArgs.Empty);
            }
        }

        private void ClickChangePage(object sender, EventArgs e)
        {
            int index = this.PageIndex;
            if (sender == btnPageFirst) index = 0;
            else if (sender == btnPageLast) index = maxPage;
            else if (sender == btnPageNext) index++;
            else if (sender == btnPagePrev) index--;

            if (index < 0) index = 0;
            else if (index > maxPage) index = maxPage;

            txtPageIndex.Text = (this.pageIndex + 1).ToString();
            this.PageIndex = index;
        }

        private void SetPage(object sender, EventArgs e)
        {
            int numero;
            if (!int.TryParse(txtPageIndex.Text, out numero) || numero - 1 == PageIndex) return;

            if (numero <= 0) numero = 1;
            else if (numero > this.MaxPage + 1) numero = this.MaxPage + 1;

            this.PageIndex = numero - 1;
        }
    }
}
