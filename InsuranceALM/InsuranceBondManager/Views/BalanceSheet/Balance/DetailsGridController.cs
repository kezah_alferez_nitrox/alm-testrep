using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using ALMS.Products;
using ALMSCommon;
using ALMSCommon.Controls;
using ALMSCommon.Forms;
using InsuranceALM.Infrastructure.Data;
using InsuranceBondManager.BusinessLogic;
using InsuranceBondManager.Calculations;
using InsuranceBondManager.Core.Export.MultiScenario;
using InsuranceBondManager.Core.UndoRedo;
using InsuranceBondManager.Domain;
using InsuranceBondManager.Properties;
using InsuranceBondManager.Resources.Language;

using Eu.AlterSystems.ASNetLib.Core;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Linq;

namespace InsuranceBondManager.Views.BalanceSheet.Balance
{
    public class DetailsGridController
    {
        private Product originalCurrentProduct;

        private bool AlreadyInGridValueChanged = false;

        private ISession session;

        private const int DecimalCellWidth = 70;
        private const string TheoreticalCurrFaceColumnName = "TheoreticalCurrFace";
        private const string CprColumnName = "CPR";
        private const string CdrColumnName = "CDR";
        private const string LgdColumnName = "LGD";
        private const string RecoveryLagColumnName = "RecoveryLag";

        private const string CreateNew = "Create New";

        private readonly DataGridViewEx grid;
        private DataGridViewComboBoxColumn accountingDataGridViewComboBoxColumn;
        private AutoCompleteColumn actuarialPriceColumn;

        private DataGridViewTextBoxColumn almIdDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn otherLegDataGridViewTextBoxColumn;
        private AutoCompleteColumn amortizationColumn;
        private DataGridViewComboBoxColumn amortizationTypeColumn;
        private DataGridViewComboBoxColumn attribDataGridViewTextBoxColumn;
        private AutoCompleteColumn basel2DataGridViewTextBoxColumn;
        private DataGridViewComboBoxColumn bondSwapLegDataGridViewComboBoxColumn;
        private AutoCompleteColumn bookPriceColumn;
        private DataGridViewTextBoxColumn bookValueDataGridViewTextBoxColumn;
        private Category category;
        private AutoCompleteColumn cdrColumn;
        private DataGridViewColumn[] columns;
        private AutoCompleteColumn complementDataGridViewTextBoxColumn;
        private List<DataGridViewColumn> copyAbleColumns;
        private DataGridViewComboBoxColumn couponBasisColumn;
        private DataGridViewComboBoxColumn couponCalcTypeColumn;
        private AutoCompleteColumn couponColumn;
        private DataGridViewComboBoxColumn couponTypeColumn;
        private AutoCompleteColumn cprColumn;
        private DataGridViewComboBoxColumn currencyDataGridViewComboBoxColumn;
        private ComboBox currentEditComboBox;
        private DataGridViewTextBoxColumn descriptionDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn durationDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn expirationDateColumn;
        private Control editingControl;
        private DataGridViewCheckBoxColumn excludeDataGridViewCheckBoxColumn;
        private DataGridViewComboBoxColumn financialTypeColumn;
        private DataGridViewTextBoxColumn firstRateDataGridViewTextBoxColumn;
        private AutoCompleteColumn growthColumn;
        private DataGridViewComboBoxColumn investmentRuleColumn;
        private DataGridViewCheckBoxColumn isModelColumn;
        //private DataGridViewCheckBoxColumn isOriginBasedOnStartDateColumn;
        private AutoCompleteColumn lgdColumn;
        private DataGridViewComboBoxColumn liquidityEligibilityColumn;
        private DataGridViewTextBoxColumn liquidityHaircutColumn;
        private DataGridViewTextBoxColumn liquidityRunOffColumn;
        private AutoCompleteColumn marketBasisColumn;
        private int menuColumn = -1;
        private AutoCompleteColumn origFaceColumn;
        private DataGridViewTextBoxColumn paymentDataGridViewTextBoxColumn;
        private DataGridViewComboBoxColumn periodDataGridViewComboBoxColumn;
        private AutoCompleteColumn rollRatioColumn;
        private DataGridViewComboBoxColumn rollSpecDataGridViewComboBoxColumn;
        private DataGridViewTextBoxColumn rwaColumn;
        private DataGridViewTextBoxColumn secIdDataGridViewTextBoxColumn;
        private AutoCompleteColumn spreadDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn startDateColumn;
        private DataGridViewImageColumn statusDataGridViewTextBoxColumn;
        private DataGridViewComboBoxColumn taxDataGridViewComboBoxColumn;

        private AutoCompleteColumn tciDataGridViewTextBoxColumn;
        private AutoCompleteColumn theoreticalCurrFaceColumn;
        private AutoCompleteColumn applicationFeeColumn;
        private AutoCompleteColumn applicationChargeColumn;
        private AutoCompleteColumn prePayementFeeColumn;
        private DataGridViewTextBoxColumn recoveryLagColumn;

        private DateTimePicker cellDateTimePicker;

        private string[] variableAndVectorNames;
        private string[] variableVectorAndDynamicVariableNames;
        private bool isClipboardCut = false;


        public DetailsGridController(DataGridViewEx grid)
        {
            this.CurrentPage = 0;
            this.grid = grid;
            this.session = SessionManager.OpenSession();

            CheckBox dummy = new CheckBox();
            dummy.Font = new Font(FontFamily.GenericMonospace.Name, 1);
            dummy.Text = "";
            dummy.AutoSize = true;
            int checkBoxHeight = dummy.PreferredSize.Height;

            float fontSize = 0;
            float.TryParse(Settings.Default.FontSize, out fontSize);
            int fontHeight = new Font(Settings.Default.FontFamily, fontSize).Height;

            //foreach (DataGridViewRow row in grid.Rows)
            //    row.Height = Math.Max(row.Cells.OfType<DataGridViewCell>().Max(c => c.ContentBounds.Height + c.ContentBounds.Y), checkBoxHeight) + 4;

            grid.RowTemplate.Height = Math.Max(fontHeight, checkBoxHeight) + 4;

            grid.AutoGenerateColumns = false;
            grid.AllowUserToAddRows = false;
            grid.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableWithoutHeaderText;

            grid.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None;
            grid.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCellsExceptHeader;

            // this.InitColumns();

            this.grid.UserDeletingRow += this.GridUserDeletingRow;
            this.grid.UserDeletedRow += this.GridUserDeletedRow;
            this.grid.CellBeginEdit += this.GridCellBeginEdit;
            this.grid.CellValueChanged += this.GridCellValueChanged;
            this.grid.CellValidating += this.GridCellValidating;
            this.grid.CellFormatting += this.GridCellFormatting;
            this.grid.CellPainting += this.GridCellPainting;
            this.grid.DataError += GridDataError;
            this.grid.ColumnHeaderMouseClick += this.GridColumnHeaderMouseClick;
            this.grid.CurrentCellDirtyStateChanged += this.GridCurrentCellDirtyStateChanged;
            this.grid.EditingControlShowing += this.GridEditingControlShowing;
            this.grid.CellParsing += this.GridCellParsing;
            this.grid.CellLeave += this.GridCellLeave;
            this.grid.CellEndEdit += this.GridCellEndEdit;
            this.grid.MouseDown += this.GridClick;
            this.grid.MouseUp += this.GridMouseUp;

            this.grid.DragDrop += this.DragLine;
            this.grid.DragOver += this.DragOverGrid;
            this.grid.MouseDown += this.CheckDragLine;
            this.grid.CellMouseDoubleClick += this.GridCellMouseDoubleClick;
            this.grid.Scroll += this.GridScroll;
        }

        private void GridScroll(object sender, ScrollEventArgs e)
        {
            if (this.cellDateTimePicker != null)
            {
                this.cellDateTimePicker.Visible = false;
            }
            if (this.editingControl != null)
            {
                this.editingControl.Visible = false;
            }
        }

        private void GridCellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex < 0 || this.grid.CurrentCell == null) return;

            if (this.grid.CurrentCell.ReadOnly == false && 
                (e.ColumnIndex == this.startDateColumn.Index || 
                e.ColumnIndex == this.expirationDateColumn.Index))
            {
                Rectangle tempRect = this.grid.GetCellDisplayRectangle(e.ColumnIndex, e.RowIndex, false);
                this.cellDateTimePicker.Visible = true;
                this.cellDateTimePicker.Location = tempRect.Location;
                this.cellDateTimePicker.Width = tempRect.Width;
                if (this.grid[e.ColumnIndex, e.RowIndex].Value != null)
                {
                    this.cellDateTimePicker.Value = DateTime.Parse(this.grid[e.ColumnIndex, e.RowIndex].Value.ToString());
                }
                else
                {
                    this.cellDateTimePicker.Value = DateTime.Today;
                }
            }
        }

        public int CurrentPage { get; set; }

        public event EventHandler OnModifiedValues;

        private SortableBindingList<Product> ProductList
        {
            get { return this.grid.DataSource as SortableBindingList<Product>; }
            set { this.grid.DataSource = value; }
        }


        private void DragOverGrid(object sender, DragEventArgs e)
        {
            Point point = this.grid.PointToClient(new Point(e.X, e.Y));
            DataGridView.HitTestInfo hit = this.grid.HitTest(point.X, point.Y);
            if (hit.RowIndex >= 0) e.Effect = DragDropEffects.Move;
            else e.Effect = DragDropEffects.None;
        }

        private void CheckDragLine(object sender, MouseEventArgs e)
        {
            DataGridView.HitTestInfo hit = this.grid.HitTest(e.X, e.Y);
            if (e.Button == MouseButtons.Left && hit.ColumnIndex == -1 && hit.RowIndex > -1 &&
                this.grid.Rows[hit.RowIndex].Selected)
                this.grid.DoDragDrop(this.grid.SelectedRows, DragDropEffects.Move);
        }

        private void DragLine(object sender, DragEventArgs e)
        {
            Point point = this.grid.PointToClient(new Point(e.X, e.Y));
            DataGridView.HitTestInfo hit = this.grid.HitTest(point.X, point.Y);
            foreach (string format in e.Data.GetFormats())
            {
                if (hit.RowIndex >= 0 && e.Data.GetData(format) is DataGridViewSelectedRowCollection)
                {
                    DataGridViewSelectedRowCollection movedRows =
                        (DataGridViewSelectedRowCollection)e.Data.GetData(format);
                    BindingList<Product> products = (SortableBindingList<Product>)this.grid.DataSource;

                    Product dropProduct = (Product)(this.grid.Rows[hit.RowIndex]).DataBoundItem;
                    if (dropProduct.SwapLeg != null &&
                        products.IndexOf(dropProduct) > products.IndexOf(dropProduct.OtherSwapLeg))
                        dropProduct = dropProduct.OtherSwapLeg;

                    if (dropProduct == null) continue;

                    List<Product> movedProducts = new List<Product>();
                    foreach (DataGridViewRow movedRow in movedRows)
                    {
                        Product product = (Product)movedRow.DataBoundItem;
                        if (product == dropProduct) continue;
                        movedProducts.Add(product);
                        products.Remove(product);
                    }
                    int index = products.IndexOf(dropProduct);

                    foreach (Product movedProduct in movedProducts)
                    {
                        products.Insert(index, movedProduct);

                        if (movedProduct.SwapLeg != null)
                        {
                            products.Remove(movedProduct.OtherSwapLeg);
                            products.Insert(products.IndexOf(movedProduct), movedProduct.OtherSwapLeg);
                        }
                    }

                    foreach (Product product in this.category.Bonds)
                        product.Position = products.IndexOf(product);

                    this.grid.ClearSelection();
                }
            }

            MainForm.ModifiedValues = true;
        }

        private void GridCellLeave(object sender, DataGridViewCellEventArgs e)
        {
            if (this.editingControl == null) return;

            this.editingControl.Enter -= DetailsGridControllerEnter;
            this.editingControl = null;
        }

        private void SetModifiedValues(bool modified)
        {
            MainForm.ModifiedValues = modified;
            if (this.OnModifiedValues != null) this.OnModifiedValues.Invoke(this, EventArgs.Empty);
        }

        private void GridMouseUp(object sender, MouseEventArgs e)
        {
            DataGridView grid = sender as DataGridView;
            if (grid == null) return;

            grid.EditMode = grid.SelectedCells.Count > 1 ? DataGridViewEditMode.EditProgrammatically : DataGridViewEditMode.EditOnKeystrokeOrF2;
            if (grid.SelectedCells.Count == 1 && grid.CurrentCell != null) grid.BeginEdit(true);
            else grid.EndEdit();
        }

        private void GridCellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {

            if (e.ColumnIndex == this.financialTypeColumn.DisplayIndex && !this.grid[e.ColumnIndex, e.RowIndex].ReadOnly)
            {
                e.Cancel = ((string)e.FormattedValue == EnumHelper.GetDescription(ProductType.Swap)) ||
                    ((string)e.FormattedValue == EnumHelper.GetDescription(ProductType.Swaption));
            }

        }

        private void GridClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                DataGridView.HitTestInfo hit = this.grid.HitTest(e.X, e.Y);

                if (hit.RowIndex < 0) this.grid.EndEdit();
            }

            if (e.Button == MouseButtons.Right)
            {
                DataGridView.HitTestInfo hit = this.grid.HitTest(e.X, e.Y);
                this.menuColumn = hit.ColumnIndex;
                if (hit.ColumnIndex < 0 || hit.RowIndex < 0) return;

                this.grid.ClearSelection();
                DataGridViewCell cell = this.grid[hit.ColumnIndex, hit.RowIndex];
                cell.Selected = true;
            }
        }

        private void GridCellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            this.cellDateTimePicker.Visible = false;

            int position = e.RowIndex;
            if (position < 0) return;

            Product product = Product.Clone(this.GetBondAtRowIndex(position));

            if (originalCurrentProduct != null && originalCurrentProduct.CompareTo(product) != 0)
            {
                //Date Validation Pop-up
                if (e.ColumnIndex == this.startDateColumn.Index &&
                    originalCurrentProduct.StartDate != product.StartDate &&
                    product.StartDate != null && !Param.GetDateValidateMsgPopUp(this.session))
                {
                    DialogResult res = this.DateValidationMessageBox(this.startDateColumn.HeaderText, (DateTime)product.StartDate);
                    if (res == DialogResult.Yes)
                        product.StartDate = Tools.ResetDateToLastOfMonth(product.StartDate.GetValueOrDefault());
                    if (res == DialogResult.Cancel)
                        product.StartDate = this.originalCurrentProduct.StartDate;
                    this.grid.CurrentCell.Value = product.StartDate;
                }
                if (e.ColumnIndex == this.expirationDateColumn.Index &&
                    originalCurrentProduct.ExpirationDate != product.ExpirationDate &&
                    product.ExpirationDate != null && !Param.GetDateValidateMsgPopUp(this.session))
                {
                    DialogResult res = this.DateValidationMessageBox(this.expirationDateColumn.HeaderText, (DateTime)product.ExpirationDate);
                    if (res == DialogResult.Yes)
                        product.ExpirationDate = Tools.ResetDateToLastOfMonth(product.ExpirationDate.GetValueOrDefault());
                    if (res == DialogResult.Cancel)
                        product.ExpirationDate = this.originalCurrentProduct.ExpirationDate;
                    this.grid.CurrentCell.Value = product.ExpirationDate;
                }

                this.FlushChanges(new ChangeLinesBalanceSheetOperation(category, CurrentPage, this.grid.CurrentRow.Index, new List<Product>() { originalCurrentProduct }, new List<Product>() { product }));
            }
        }

        private void GridCellParsing(object sender, DataGridViewCellParsingEventArgs e)
        {
            if (this.grid.Columns[e.ColumnIndex].DefaultCellStyle.Format == Constants.PERCENTAGE_CELL_FORMAT)
            {
                if (e.Value != null)
                {
                    string value = e.Value.ToString().Trim().Replace(" ", "");
                    bool percent = value.EndsWith("%");
                    value = value.Replace("%", "");

                    double parsedValue;
                    e.ParsingApplied = double.TryParse(value, out parsedValue);
                    if (e.ParsingApplied)
                    {
                        if (percent)
                        {
                            parsedValue /= 100;
                        }
                        e.Value = parsedValue;
                    }
                }
            }

            if (e.ColumnIndex == this.startDateColumn.DisplayIndex || e.ColumnIndex == this.expirationDateColumn.DisplayIndex)
            {
                DateTime date;

                //format: MM/yyyy or MMyyyy
                if (Convert.ToString(e.Value).Length < 8 && Convert.ToString(e.Value).Length > 4)
                {
                    //clean string
                    string datetemp = Convert.ToString(e.Value).Trim();
                    datetemp = datetemp.Replace("/", "");
                    datetemp = datetemp.Replace(" ", "");
                    if (datetemp.Length == 5)
                        datetemp = "0" + datetemp;

                    //get month and year
                    int month = 0;
                    int.TryParse(datetemp.Substring(0, 2), out month);
                    int year = 0;
                    int.TryParse(datetemp.Substring(datetemp.Length - 4), out year);
                    
                    //get last day
                    int lastday = GetLastDayofMonthYear(month, year);

                    e.Value = lastday.ToString() + datetemp;
                }

                if (DateTime.TryParseExact(Convert.ToString(e.Value), Constants.DateFormat_8Digits, DateTimeFormatInfo.InvariantInfo,
                                       DateTimeStyles.None, out date))
                {
                    e.Value = date;
                    e.ParsingApplied = true;
                }
            }
        }

        private void GridEditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            Point point = this.grid.PointToClient(new Point(Cursor.Position.X, Cursor.Position.Y));
            DataGridView.HitTestInfo hit = this.grid.HitTest(point.X, point.Y);

            if (e.Control is ComboBox && hit.Type == DataGridViewHitTestType.Cell)
            {
                this.editingControl = e.Control;
                this.editingControl.Enter += DetailsGridControllerEnter;

                ComboBox comboBox = (ComboBox)e.Control;
                comboBox.SelectedValueChanged += this.EditingCellComboBoxSelectedValueChanged;

                if ((e.Control as ComboBox).Items.Contains(CreateNew))
                {
                    this.currentEditComboBox = (ComboBox)e.Control;
                }
            }

            if (this.grid.CurrentCell.EditType == typeof(AutoCompleteEditingControl))
            {
                this.editingControl = this.grid.EditingControl;
            }
        }

        void EditingCellComboBoxSelectedValueChanged(object sender, EventArgs e)
        {
            ComboBox comboBox = sender as ComboBox;
            if (comboBox != null && comboBox.SelectedValue != null && comboBox.SelectedValue is ProductAccounting &&
                 this.grid.CurrentRow != null && grid.CurrentCell != null &&
                 grid.CurrentCell.ColumnIndex == this.accountingDataGridViewComboBoxColumn.DisplayIndex)
            {
                Product product = this.grid.CurrentRow.DataBoundItem as Product;

                ProductAccounting newValue = (ProductAccounting)comboBox.SelectedValue;
                if (newValue == product.Accounting)
                {
                    // no change, nothing to do
                    return;
                }
                if ((product.Accounting == ProductAccounting.AFS || product.Accounting == ProductAccounting.HFT) &&
                    (newValue != ProductAccounting.AFS && newValue != ProductAccounting.HFT))
                {
                    if(product.BookValue>0)
                        product.BookPrice = "100";
                    else
                        product.BookPrice = "-100";
                    product.Accounting = newValue;
                    if (product.OtherSwapLeg != null)
                    {
                        if (product.OtherSwapLeg.BookValue > 0)
                            product.OtherSwapLeg.BookPrice = "100";
                        else
                            product.OtherSwapLeg.BookPrice = "-100";
                        product.OtherSwapLeg.Accounting = newValue;
                        this.ComputeBookPriceValueAndBookValue(this.grid.CurrentRow.Index, product.OtherSwapLeg, product.OtherSwapLeg.BookPrice, false);
                    }
                    this.ComputeBookPriceValueAndBookValue(this.grid.CurrentRow.Index, product, product.BookPrice, false);
                }
                if ((product.Accounting != ProductAccounting.AFS && product.Accounting != ProductAccounting.HFT) &&
                    (newValue == ProductAccounting.AFS || newValue == ProductAccounting.HFT))
                {   // ZZZ this is a test
                    product.Accounting = newValue;
                    //product.ActuarialPrice = "";
                    Util.UpdateProductBookPrice(product);
                    this.ComputeBookPriceValueAndBookValue(this.grid.CurrentRow.Index, product, product.BookPrice, false); 
                    if (product.OtherSwapLeg != null)
                    {
                        product.OtherSwapLeg.Accounting = newValue;
                        //product.OtherSwapLeg.ActuarialPrice = "";
                        Util.UpdateProductBookPrice(product.OtherSwapLeg);
                        this.ComputeBookPriceValueAndBookValue(this.grid.CurrentRow.Index, product.OtherSwapLeg, product.OtherSwapLeg.BookPrice, false);
                    }
                }
            }

            //Derivatives Leg changed
            if (comboBox != null && comboBox.SelectedValue != null && comboBox.SelectedValue is BondSwapLeg &&
                this.grid.CurrentRow != null && grid.CurrentCell != null)
            {
                Product product = this.grid.CurrentRow.DataBoundItem as Product;
                BondSwapLeg newValue = (BondSwapLeg)comboBox.SelectedValue;

                if (newValue != product.SwapLeg)
                {
                    product.BookValue *= -1;                  
                    double akt = Double.Parse(product.ActuarialPrice) * 1.0;
                    akt *= -1;
                    product.ActuarialPrice = "" + akt;
                    if (product.BookPriceValue == 0)
                    {
                        Util.UpdateProductBookPrice(product);
                    }
                    
                    product.BookPriceValue *= -1;
                    product.BookPrice = Convert.ToString(product.BookPriceValue);
                    this.ComputeBookPriceValueAndBookValue(this.grid.CurrentRow.Index, product, product.BookPrice, false);

                    //update otherswapleg
                    if (product.OtherSwapLeg != null)
                    {
                        int positionSwap = FindRowIndexByALMID(product.OtherSwapLeg.ALMIDDisplayed);
                        product.OtherSwapLeg.BookValue *= -1;
                        akt = Double.Parse(product.OtherSwapLeg.ActuarialPrice) * 1.0;
                        akt *= -1;
                        product.OtherSwapLeg.ActuarialPrice = "" + akt;
                        if (product.OtherSwapLeg.BookPriceValue == 0)
                        {
                            Util.UpdateProductBookPrice(product.OtherSwapLeg);
                        }

                        product.OtherSwapLeg.BookPriceValue *= -1;
                        product.OtherSwapLeg.BookPrice = Convert.ToString(product.OtherSwapLeg.BookPriceValue);
                        this.ComputeBookPriceValueAndBookValue(positionSwap, product.OtherSwapLeg, product.OtherSwapLeg.BookPrice, false);
                    }
                }
            }
        }

        private static void DetailsGridControllerEnter(object sender, EventArgs e)
        {
            ComboBox comboBox = (ComboBox)sender;
            comboBox.Enter -= DetailsGridControllerEnter;
            if (comboBox.Visible)
            {
                comboBox.DroppedDown = true;
            }
        }

        private void InitColumns()
        {
            this.UpdateVariableAndVectorNameList();

            this.excludeDataGridViewCheckBoxColumn = new DataGridViewCheckBoxColumn();
            this.statusDataGridViewTextBoxColumn = new DataGridViewImageColumn();
            this.secIdDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            this.descriptionDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            this.amortizationTypeColumn = new DataGridViewComboBoxColumn();
            this.amortizationColumn = new AutoCompleteColumn();
            this.investmentRuleColumn = new DataGridViewComboBoxColumn();
            this.couponBasisColumn = new DataGridViewComboBoxColumn();
            this.growthColumn = new AutoCompleteColumn();
            this.origFaceColumn = new AutoCompleteColumn();
            this.bookPriceColumn = new AutoCompleteColumn();
            this.actuarialPriceColumn = new AutoCompleteColumn();
            this.bookValueDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            this.complementDataGridViewTextBoxColumn = new AutoCompleteColumn();
            this.almIdDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            this.otherLegDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            this.rollSpecDataGridViewComboBoxColumn = new DataGridViewComboBoxColumn();
            this.rollRatioColumn = new AutoCompleteColumn();
            this.startDateColumn = new DataGridViewTextBoxColumn();
            this.couponColumn = new AutoCompleteColumn();
            this.paymentDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            this.attribDataGridViewTextBoxColumn = new DataGridViewComboBoxColumn();
            this.couponTypeColumn = new DataGridViewComboBoxColumn();
            this.couponCalcTypeColumn = new DataGridViewComboBoxColumn();
            this.accountingDataGridViewComboBoxColumn = new DataGridViewComboBoxColumn();
            this.periodDataGridViewComboBoxColumn = new DataGridViewComboBoxColumn();
            this.durationDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            this.expirationDateColumn = new DataGridViewTextBoxColumn();
            this.taxDataGridViewComboBoxColumn = new DataGridViewComboBoxColumn();
            this.isModelColumn = new DataGridViewCheckBoxColumn();
            //this.isOriginBasedOnStartDateColumn = new DataGridViewCheckBoxColumn();
            this.lgdColumn = new AutoCompleteColumn();
            this.theoreticalCurrFaceColumn = new AutoCompleteColumn();
            this.cprColumn = new AutoCompleteColumn();
            this.cdrColumn = new AutoCompleteColumn();
            this.basel2DataGridViewTextBoxColumn = new AutoCompleteColumn();
            this.currencyDataGridViewComboBoxColumn = new DataGridViewComboBoxColumn();
            this.tciDataGridViewTextBoxColumn = new AutoCompleteColumn();
            this.rwaColumn = new DataGridViewTextBoxColumn();
            this.liquidityEligibilityColumn = new DataGridViewComboBoxColumn();
            this.liquidityHaircutColumn = new DataGridViewTextBoxColumn();
            this.liquidityRunOffColumn = new DataGridViewTextBoxColumn();
            this.bondSwapLegDataGridViewComboBoxColumn = new DataGridViewComboBoxColumn();
            this.financialTypeColumn = new DataGridViewComboBoxColumn();
            this.spreadDataGridViewTextBoxColumn = new AutoCompleteColumn();
            this.marketBasisColumn = new AutoCompleteColumn();
            this.firstRateDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            this.applicationFeeColumn = new AutoCompleteColumn();
            this.applicationChargeColumn = new AutoCompleteColumn();
            this.prePayementFeeColumn = new AutoCompleteColumn();
            this.recoveryLagColumn = new DataGridViewTextBoxColumn();
            // 
            // excludeDataGridViewCheckBoxColumn
            // 
            this.excludeDataGridViewCheckBoxColumn.DataPropertyName = "Exclude";
            this.excludeDataGridViewCheckBoxColumn.HeaderText = Labels.DetailsGridController_InitColumns_Exclude;
            this.excludeDataGridViewCheckBoxColumn.Name = "Exclude";
            this.excludeDataGridViewCheckBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.excludeDataGridViewCheckBoxColumn.MinimumWidth = 30;
            // 
            // statusDataGridViewTextBoxColumn
            // 
            this.statusDataGridViewTextBoxColumn.DataPropertyName = "Status";
            this.statusDataGridViewTextBoxColumn.HeaderText = Labels.DetailsGridController_InitColumns_Status;
            this.statusDataGridViewTextBoxColumn.Name = "Status";
            this.statusDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.statusDataGridViewTextBoxColumn.MinimumWidth = 20;
            // 
            // secIdDataGridViewTextBoxColumn
            // 
            this.secIdDataGridViewTextBoxColumn.DataPropertyName = "SecId";
            this.secIdDataGridViewTextBoxColumn.HeaderText = Labels.DetailsGridController_InitColumns_SecId;
            this.secIdDataGridViewTextBoxColumn.Name = "SecId";
            this.secIdDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.secIdDataGridViewTextBoxColumn.MinimumWidth = 40;
            // 
            // descriptionDataGridViewTextBoxColumn
            // 
            this.descriptionDataGridViewTextBoxColumn.DataPropertyName = "Description";
            this.descriptionDataGridViewTextBoxColumn.HeaderText = Labels.DetailsGridController_InitColumns_Description;
            this.descriptionDataGridViewTextBoxColumn.Name = "Description";
            this.descriptionDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.descriptionDataGridViewTextBoxColumn.Frozen = true;
            this.descriptionDataGridViewTextBoxColumn.MinimumWidth = 100;

            // 
            // typeDataGridViewTextBoxColumn
            // 
            this.amortizationTypeColumn.DataPropertyName = "AmortizationType";
            this.amortizationTypeColumn.HeaderText = Labels.DetailsGridController_InitColumns_Amortization;
            this.amortizationTypeColumn.Name = "AmortizationType";
            this.amortizationTypeColumn.DisplayMember = "Value";
            this.amortizationTypeColumn.ValueMember = "Key";
            this.amortizationTypeColumn.DataSource = EnumHelper.ToList(typeof(ProductAmortizationType));
            this.amortizationTypeColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.amortizationTypeColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;
            this.amortizationTypeColumn.MinimumWidth = 50;
            this.amortizationTypeColumn.DropDownWidth = 100;
            //
            // investmentRuleColumn
            //
            this.investmentRuleColumn.DataPropertyName = "InvestmentRule";
            this.investmentRuleColumn.HeaderText = Labels.DetailsGridController_InitColumns_Investment_Rule;
            this.investmentRuleColumn.Name = "InvestmentRule";
            this.investmentRuleColumn.DisplayMember = "Value";
            this.investmentRuleColumn.ValueMember = "Key";
            this.investmentRuleColumn.DataSource = EnumHelper.ToList(typeof(ProductInvestmentRule));
            this.investmentRuleColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.investmentRuleColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;
            this.investmentRuleColumn.MinimumWidth = 40;
            this.investmentRuleColumn.DropDownWidth = 120;

            //
            // couponBasisColumn
            //
            this.couponBasisColumn.DataPropertyName = "CouponBasis";
            this.couponBasisColumn.HeaderText = Labels.DetailsGridController_InitColumns_Coupon_Basis;
            this.couponBasisColumn.Name = "CouponBasis";
            this.couponBasisColumn.DisplayMember = "Value";
            this.couponBasisColumn.ValueMember = "Key";
            this.couponBasisColumn.DataSource = EnumHelper.ToList(typeof(CouponBasis));
            this.couponBasisColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.couponBasisColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;
            this.couponBasisColumn.MinimumWidth = 50;
            this.couponBasisColumn.DropDownWidth = 120;

            // 
            // growthDataGridViewTextBoxColumn
            // 
            this.growthColumn.DataPropertyName = "InvestmentParameter";
            this.growthColumn.HeaderText = Labels.DetailsGridController_InitColumns_Growth;
            this.growthColumn.Name = "InvestmentParameter";
            this.growthColumn.DefaultCellStyle.Format = Constants.MONEY_CELL_FORMAT;
            this.growthColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.growthColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.growthColumn.MinimumWidth = 40;
            this.growthColumn.ValueList = this.variableVectorAndDynamicVariableNames;

            // 
            // origFaceColumn
            // 
            this.origFaceColumn.DataPropertyName = "OrigFace";
            this.origFaceColumn.HeaderText = Labels.DetailsGridController_InitColumns_Orig_Face;
            this.origFaceColumn.Name = "OrigFace";
            this.origFaceColumn.DefaultCellStyle.Format = Constants.MONEY_CELL_FORMAT;
            this.origFaceColumn.ValueType = typeof(double);
            this.origFaceColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.origFaceColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.origFaceColumn.MinimumWidth = DecimalCellWidth;
            this.origFaceColumn.ValueList = this.variableAndVectorNames;

            // 
            // bookPriceDataGridViewTextBoxColumn
            // 
            this.bookPriceColumn.DataPropertyName = "BookPrice";
            this.bookPriceColumn.HeaderText = Labels.DetailsGridController_InitColumns_Book_Price;
            this.bookPriceColumn.Name = "BookPrice";
            this.bookPriceColumn.DefaultCellStyle.Format = Constants.MONEY_CELL_FORMAT;
            this.bookPriceColumn.DefaultCellStyle.Alignment =
                DataGridViewContentAlignment.MiddleRight;
            this.bookPriceColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.bookPriceColumn.MinimumWidth = 40;
            this.bookPriceColumn.ValueList = this.variableVectorAndDynamicVariableNames;


            // 
            // actuarialPriceDataGridViewTextBoxColumn
            // 
            this.actuarialPriceColumn.DataPropertyName = "ActuarialPrice";
            this.actuarialPriceColumn.HeaderText = Labels.DetailsGridController_InitColumns_Actuarial_Price;
            this.actuarialPriceColumn.Name = "ActuarialPrice";
            this.actuarialPriceColumn.DefaultCellStyle.Format = Constants.MONEY_CELL_FORMAT;
            this.actuarialPriceColumn.DefaultCellStyle.Alignment =
                DataGridViewContentAlignment.MiddleRight;
            this.actuarialPriceColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.actuarialPriceColumn.MinimumWidth = 45;
            this.actuarialPriceColumn.ValueList = this.variableAndVectorNames;

            // 
            // bookValueDataGridViewTextBoxColumn
            // 
            this.bookValueDataGridViewTextBoxColumn.DataPropertyName = "BookValue";
            this.bookValueDataGridViewTextBoxColumn.HeaderText = Labels.DetailsGridController_InitColumns_Book_Value;
            this.bookValueDataGridViewTextBoxColumn.Name = "BookValue";
            this.bookValueDataGridViewTextBoxColumn.DefaultCellStyle.Format = Constants.MONEY_CELL_FORMAT;
            this.bookValueDataGridViewTextBoxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.bookValueDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            
            this.bookValueDataGridViewTextBoxColumn.MinimumWidth = DecimalCellWidth;// +40;
            //this.bookValueDataGridViewTextBoxColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            this.bookValueDataGridViewTextBoxColumn.ReadOnly = true;
            this.bookValueDataGridViewTextBoxColumn.DefaultCellStyle.BackColor = Color.LightGray;

            // 
            // complementDataGridViewTextBoxColumn
            // 
            this.complementDataGridViewTextBoxColumn.DataPropertyName = "ComplementString";
            this.complementDataGridViewTextBoxColumn.HeaderText = Labels.DetailsGridController_InitColumns_Complement;
            this.complementDataGridViewTextBoxColumn.Name = "Complement";
            this.complementDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.complementDataGridViewTextBoxColumn.MinimumWidth = 60;
            this.complementDataGridViewTextBoxColumn.ValueList = this.variableVectorAndDynamicVariableNames;

            // 
            // aLMIDDataGridViewTextBoxColumn
            // 
            this.almIdDataGridViewTextBoxColumn.DataPropertyName = "ALMIDDisplayed";
            this.almIdDataGridViewTextBoxColumn.HeaderText = Labels.DetailsGridController_InitColumns_ID;
            this.almIdDataGridViewTextBoxColumn.Name = "ALMID";
            this.almIdDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.almIdDataGridViewTextBoxColumn.ReadOnly = true;
            this.almIdDataGridViewTextBoxColumn.DefaultCellStyle.BackColor = Color.LightGray;
            this.almIdDataGridViewTextBoxColumn.MinimumWidth = 40;
            this.almIdDataGridViewTextBoxColumn.Frozen = true;
            // 
            // aLMIDDataGridViewTextBoxColumn
            // 
            this.otherLegDataGridViewTextBoxColumn.DataPropertyName = "OtherSwapLeg";
            this.otherLegDataGridViewTextBoxColumn.HeaderText = "Other Leg";
            this.otherLegDataGridViewTextBoxColumn.Name = "OtherSwapLegAlmId";
            this.otherLegDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.otherLegDataGridViewTextBoxColumn.ReadOnly = true;
            this.otherLegDataGridViewTextBoxColumn.DefaultCellStyle.BackColor = Color.LightGray;
            this.otherLegDataGridViewTextBoxColumn.MinimumWidth = 40;
            this.otherLegDataGridViewTextBoxColumn.Frozen = true;
            // 
            // rollSpecDataGridViewTextBoxColumn
            // 
            this.rollSpecDataGridViewComboBoxColumn.DataPropertyName = "RollSpec";
            this.rollSpecDataGridViewComboBoxColumn.HeaderText = Labels.DetailsGridController_InitColumns_Roll_Spec;
            this.rollSpecDataGridViewComboBoxColumn.Name = "RollSpec";
            this.rollSpecDataGridViewComboBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.rollSpecDataGridViewComboBoxColumn.MinimumWidth = 45;
            this.rollSpecDataGridViewComboBoxColumn.DropDownWidth = 80;
            this.rollSpecDataGridViewComboBoxColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;

            // 
            // rollRatioColumn
            // 
            this.rollRatioColumn.DataPropertyName = "RollRatio";
            this.rollRatioColumn.HeaderText = Labels.DetailsGridController_InitColumns_Roll_Ratio;
            this.rollRatioColumn.Name = "RollRatio";
            this.rollRatioColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.rollRatioColumn.MinimumWidth = 20;
            this.rollRatioColumn.DefaultCellStyle.Format = Constants.PERCENTAGE_CELL_FORMAT;
            this.rollRatioColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this.rollRatioColumn.ValueList = this.variableAndVectorNames;

            // 
            // couponDataGridViewTextBoxColumn
            // 
            this.couponColumn.DataPropertyName = "Coupon";
            this.couponColumn.HeaderText = Labels.DetailsGridController_InitColumns_Coupon;
            this.couponColumn.Name = "Coupon";
            this.couponColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.couponColumn.MinimumWidth = 60;
            this.couponColumn.ValueList = this.variableVectorAndDynamicVariableNames;

            // 
            // startDateColumn
            // 
            this.startDateColumn.DataPropertyName = "StartDate";
            this.startDateColumn.HeaderText = "Start Date"; // todo cdu
            this.startDateColumn.Name = "StartDate";
            this.startDateColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.startDateColumn.DefaultCellStyle.Format = Constants.DateFormat;
            this.startDateColumn.MinimumWidth = 60;
            this.startDateColumn.ValueType = typeof(DateTime?);

            // 
            // amortizationColumn
            // 
            this.amortizationColumn.DataPropertyName = "Amortization";
            this.amortizationColumn.ToolTipText = "Please enter fomula for VAR and the lag in month number for CST_PAY";
            this.amortizationColumn.HeaderText = Labels.DetailsGridController_InitColumns_Amortization_Formula;
            this.amortizationColumn.Name = "Amortization";
            this.amortizationColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.amortizationColumn.MinimumWidth = 60;
            this.amortizationColumn.ValueList = this.variableVectorAndDynamicVariableNames;

            // 
            // paymentDataGridViewTextBoxColumn
            // 
            this.paymentDataGridViewTextBoxColumn.DataPropertyName = "Payment";
            this.paymentDataGridViewTextBoxColumn.HeaderText = Labels.DetailsGridController_InitColumns_Payment;
            this.paymentDataGridViewTextBoxColumn.Name = "Payment";
            this.paymentDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.paymentDataGridViewTextBoxColumn.MinimumWidth = 60;

            // 
            // attribDataGridViewTextBoxColumn
            // 
            this.attribDataGridViewTextBoxColumn.DataPropertyName = "Attrib";
            this.attribDataGridViewTextBoxColumn.HeaderText = Labels.DetailsGridController_InitColumns_Attrib;
            this.attribDataGridViewTextBoxColumn.Name = "Attrib";
            this.attribDataGridViewTextBoxColumn.DisplayMember = "Value";
            this.attribDataGridViewTextBoxColumn.ValueMember = "Key";
            this.attribDataGridViewTextBoxColumn.DataSource = EnumHelper.ToList(typeof(ProductAttrib));
            this.attribDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.attribDataGridViewTextBoxColumn.MinimumWidth = 65;
            this.attribDataGridViewTextBoxColumn.DropDownWidth = 120;
            this.attribDataGridViewTextBoxColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;

            // 
            // couponTypeColumn
            // 
            this.couponTypeColumn.DataPropertyName = "CouponType";
            this.couponTypeColumn.HeaderText = Labels.DetailsGridController_InitColumns_Coupon_Type;
            this.couponTypeColumn.Name = "CouponType";
            this.couponTypeColumn.DisplayMember = "Value";
            this.couponTypeColumn.ValueMember = "Key";
            IList dataSource = EnumHelper.ToList(typeof(CouponType));
            dataSource.RemoveAt(dataSource.Count-1);
            this.couponTypeColumn.DataSource = dataSource;
            this.couponTypeColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.couponTypeColumn.MinimumWidth = 45;
            this.couponTypeColumn.DropDownWidth = 80;
            this.couponTypeColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;

            // 
            // couponCalcTypeColumn
            // 
            this.couponCalcTypeColumn.DataPropertyName = "CouponCalcType";
            this.couponCalcTypeColumn.HeaderText = "Fixing Type";
            this.couponCalcTypeColumn.Name = "CouponCalcType";
            this.couponCalcTypeColumn.DisplayMember = "Value";
            this.couponCalcTypeColumn.ValueMember = "Key";
            this.couponCalcTypeColumn.DataSource = EnumHelper.ToList(typeof(CouponCalcType));
            this.couponCalcTypeColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.couponCalcTypeColumn.MinimumWidth = 50;
            this.couponCalcTypeColumn.DropDownWidth = 80;
            this.couponCalcTypeColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;

            // 
            // taxDataGridViewTextBoxColumn
            // 
            this.taxDataGridViewComboBoxColumn.DataPropertyName = "Tax";
            this.taxDataGridViewComboBoxColumn.HeaderText = Labels.DetailsGridController_InitColumns_Tax;
            this.taxDataGridViewComboBoxColumn.Name = "Tax";
            this.taxDataGridViewComboBoxColumn.DisplayMember = "Value";
            this.taxDataGridViewComboBoxColumn.ValueMember = "Key";
            this.taxDataGridViewComboBoxColumn.DataSource = EnumHelper.ToList(typeof(ProductTax));
            this.taxDataGridViewComboBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.taxDataGridViewComboBoxColumn.MinimumWidth = 70;
            this.taxDataGridViewComboBoxColumn.DropDownWidth = 90;
            this.taxDataGridViewComboBoxColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;
            // 
            // accountingDataGridViewTextBoxColumn
            // 
            this.accountingDataGridViewComboBoxColumn.DataPropertyName = "Accounting";
            this.accountingDataGridViewComboBoxColumn.HeaderText = Labels.DetailsGridController_InitColumns_Accounting;
            this.accountingDataGridViewComboBoxColumn.Name = "Accounting";
            this.accountingDataGridViewComboBoxColumn.DisplayMember = "Value";
            this.accountingDataGridViewComboBoxColumn.ValueMember = "Key";
            this.accountingDataGridViewComboBoxColumn.DataSource = EnumHelper.ToList(typeof(ProductAccounting));
            this.accountingDataGridViewComboBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.accountingDataGridViewComboBoxColumn.MinimumWidth = 30;
            this.accountingDataGridViewComboBoxColumn.DropDownWidth = 80;
            this.accountingDataGridViewComboBoxColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;
            // 
            // periodDataGridViewTextBoxColumn
            // 
            this.periodDataGridViewComboBoxColumn.DataPropertyName = "Frequency";
            this.periodDataGridViewComboBoxColumn.HeaderText = Labels.DetailsGridController_InitColumns_Frequency;
            this.periodDataGridViewComboBoxColumn.Name = "Frequency";
            this.periodDataGridViewComboBoxColumn.DisplayMember = "Value";
            this.periodDataGridViewComboBoxColumn.ValueMember = "Key";
            this.periodDataGridViewComboBoxColumn.DataSource = EnumHelper.ToList(typeof(ProductFrequency));
            this.periodDataGridViewComboBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.periodDataGridViewComboBoxColumn.MinimumWidth = 45;
            this.periodDataGridViewComboBoxColumn.DropDownWidth = 90;
            this.periodDataGridViewComboBoxColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;
            // 
            // durationDataGridViewTextBoxColumn
            // 
            this.durationDataGridViewTextBoxColumn.DataPropertyName = "Duration";
            this.durationDataGridViewTextBoxColumn.HeaderText = Labels.DetailsGridController_InitColumns_Duration;
            this.durationDataGridViewTextBoxColumn.Name = "Duration";
            this.durationDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.durationDataGridViewTextBoxColumn.DefaultCellStyle.Format = Constants.DURATION_CELL_FORMAT;
            this.durationDataGridViewTextBoxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.durationDataGridViewTextBoxColumn.MinimumWidth = 35;


            // 
            // expirationDateColumn
            // 
            this.expirationDateColumn.DataPropertyName = "ExpirationDate";
            this.expirationDateColumn.HeaderText = "Expiration Date"; // todo cdu
            this.expirationDateColumn.Name = "ExpirationDate";
            this.expirationDateColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.expirationDateColumn.DefaultCellStyle.Format = Constants.DateFormat;
            this.expirationDateColumn.ValueType = typeof(DateTime?);
            this.expirationDateColumn.MinimumWidth = 60;

            // 
            // isModelColumn
            // 
            this.isModelColumn.DataPropertyName = "IsModel";
            this.isModelColumn.HeaderText = Labels.DetailsGridController_InitColumns_Model;
            this.isModelColumn.Name = "Model";
            this.isModelColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.isModelColumn.MinimumWidth = 30;
            // 
            // isModelColumn
            // 
            //this.isOriginBasedOnStartDateColumn.DataPropertyName = "OriginBasedOnStartDate";
            //this.isOriginBasedOnStartDateColumn.HeaderText = "Origin";
            //this.isOriginBasedOnStartDateColumn.HeaderCell.ToolTipText = "Origin for CPR, CDR & LGD based on the Start Date (relative). Uncheck if you want to let it based on the Valuation Date (absolute)";
            //this.isOriginBasedOnStartDateColumn.Name = "OriginBasedOnStartDate";
            //this.isOriginBasedOnStartDateColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            //this.isOriginBasedOnStartDateColumn.Width = 30;
            // 
            // theoreticalCurrFaceColumn
            // 
            this.theoreticalCurrFaceColumn.DataPropertyName = "TheoreticalCurrFace";
            this.theoreticalCurrFaceColumn.HeaderText = Labels.DetailsGridController_InitColumns_Theoretical_Curr_Face;
            this.theoreticalCurrFaceColumn.Name = TheoreticalCurrFaceColumnName;
            this.theoreticalCurrFaceColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.theoreticalCurrFaceColumn.DefaultCellStyle.Format = Constants.MONEY_CELL_FORMAT;
            this.theoreticalCurrFaceColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.theoreticalCurrFaceColumn.MinimumWidth = 70;
            this.theoreticalCurrFaceColumn.ValueList = this.variableAndVectorNames;
            // 
            // cprColumn
            // 
            this.cprColumn.DataPropertyName = "CPR";
            this.cprColumn.HeaderText = Labels.DetailsGridController_InitColumns_CPR;
            this.cprColumn.Name = CprColumnName;
            this.cprColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.cprColumn.DefaultCellStyle.Format = Constants.MONEY_CELL_FORMAT;
            this.cprColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.cprColumn.MinimumWidth = 70;
            this.cprColumn.ValueList = this.variableAndVectorNames;
            // 
            // cdrColumn
            // 
            this.cdrColumn.DataPropertyName = "CDR";
            this.cdrColumn.HeaderText = Labels.DetailsGridController_InitColumns_CDR;
            this.cdrColumn.Name = CdrColumnName;
            this.cdrColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.cdrColumn.DefaultCellStyle.Format = Constants.MONEY_CELL_FORMAT;
            this.cdrColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.cdrColumn.MinimumWidth = 70;
            this.cdrColumn.ValueList = this.variableAndVectorNames;
            // 
            // lgdColumn
            // 
            this.lgdColumn.DataPropertyName = "LGD";
            this.lgdColumn.HeaderText = Labels.DetailsGridController_InitColumns_LGD;
            this.lgdColumn.Name = LgdColumnName;
            this.lgdColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.lgdColumn.DefaultCellStyle.Format = Constants.MONEY_CELL_FORMAT;
            this.lgdColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.lgdColumn.ValueList = this.variableAndVectorNames;
            this.lgdColumn.MinimumWidth = 70;
            // 
            // basel2DataGridViewTextBoxColumn
            // 
            this.basel2DataGridViewTextBoxColumn.DataPropertyName = "Basel2";
            this.basel2DataGridViewTextBoxColumn.HeaderText = Labels.DetailsGridController_InitColumns_Basel_II;
            this.basel2DataGridViewTextBoxColumn.Name = "Basel2";
            this.basel2DataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.basel2DataGridViewTextBoxColumn.DefaultCellStyle.Format = Constants.PERCENTAGE_CELL_FORMAT;
            this.basel2DataGridViewTextBoxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.basel2DataGridViewTextBoxColumn.MinimumWidth = 50;
            this.basel2DataGridViewTextBoxColumn.ValueList = new[]
                                                             {
                                                                 Product.Basel2Types.CoreEquity,
                                                                 Product.Basel2Types.OtherTiers1,
                                                                 Product.Basel2Types.Tiers2,
                                                             };
            // 
            // currencyDataGridViewComboBoxColumn
            // 
            this.currencyDataGridViewComboBoxColumn.DataPropertyName = "Currency";
            this.currencyDataGridViewComboBoxColumn.HeaderText = Labels.DetailsGridController_InitColumns_Currency;
            this.currencyDataGridViewComboBoxColumn.Name = "Currency";
            this.currencyDataGridViewComboBoxColumn.DisplayMember = "Symbol";
            this.currencyDataGridViewComboBoxColumn.ValueMember = "Self";
            this.currencyDataGridViewComboBoxColumn.DataSource = Currency.FindAll(this.session);
            this.currencyDataGridViewComboBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.currencyDataGridViewComboBoxColumn.MinimumWidth = 55;
            this.currencyDataGridViewComboBoxColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;
            // 
            // tciDataGridViewTextBoxColumn
            // 
            this.tciDataGridViewTextBoxColumn.DataPropertyName = "IRC";
            this.tciDataGridViewTextBoxColumn.HeaderText = Labels.DetailsGridController_InitColumns_TCI;
            this.tciDataGridViewTextBoxColumn.Name = "IRC";
            this.tciDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.tciDataGridViewTextBoxColumn.ValueList = this.variableAndVectorNames;

            // 
            // rwaDataGridViewTextBoxColumn
            // 
            this.rwaColumn.DataPropertyName = "RWA";
            this.rwaColumn.HeaderText = Labels.DetailsGridController_InitColumns_RWA;
            this.rwaColumn.Name = "RWA";
            this.rwaColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.rwaColumn.MinimumWidth = 100;
            this.rwaColumn.ReadOnly = true;
            this.rwaColumn.DefaultCellStyle.BackColor = Color.LightGray;
            this.rwaColumn.DefaultCellStyle.Format = Constants.MONEY_CELL_FORMAT;
            this.rwaColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            // 
            // rwaDataGridViewTextBoxColumn
            // 
            this.liquidityEligibilityColumn.DataPropertyName = "LiquidityEligibility";
            this.liquidityEligibilityColumn.HeaderText = Labels.DetailsGridController_InitColumns_Liquidity_Eligibility;
            this.liquidityEligibilityColumn.Name = "LiquidityEligibility";
            this.liquidityEligibilityColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.liquidityEligibilityColumn.MinimumWidth = 80;
            this.liquidityEligibilityColumn.DisplayMember = "Value";
            this.liquidityEligibilityColumn.ValueMember = "Key";
            this.liquidityEligibilityColumn.DataSource = EnumHelper.ToList(typeof(LiquidityEligibility));
            this.liquidityEligibilityColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;

            // 
            // liquidityHaircutColumn
            // 
            this.liquidityHaircutColumn.DataPropertyName = "LiquidityHaircut";
            this.liquidityHaircutColumn.HeaderText = Labels.DetailsGridController_InitColumns_Liquidity_Haircut;
            this.liquidityHaircutColumn.Name = "LiquidityHaircut";
            this.liquidityHaircutColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.liquidityHaircutColumn.MinimumWidth = 70;
            this.liquidityHaircutColumn.DefaultCellStyle.Format = Constants.PERCENTAGE_CELL_FORMAT;
            this.liquidityHaircutColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            // 
            // liquidityRunOffColumn
            // 
            this.liquidityRunOffColumn.DataPropertyName = "LiquidityRunOff";
            this.liquidityRunOffColumn.HeaderText = Labels.DetailsGridController_InitColumns_Liquidity_Run_Off;
            this.liquidityRunOffColumn.Name = "LiquidityRunOff";
            this.liquidityRunOffColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.liquidityRunOffColumn.MinimumWidth = 70;
            this.liquidityRunOffColumn.DefaultCellStyle.Format = Constants.PERCENTAGE_CELL_FORMAT;
            this.liquidityRunOffColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            // 
            // financialTypeColumn
            // 
            this.financialTypeColumn.DataPropertyName = "ProductType";
            this.financialTypeColumn.HeaderText = Labels.DetailsGridController_InitColumns_Financial_Type;
            this.financialTypeColumn.Name = "ProductType";
            this.financialTypeColumn.DisplayMember = "Value";
            this.financialTypeColumn.ValueMember = "Key";
            this.financialTypeColumn.DataSource =
                EnumHelper.ToList(typeof(ProductType)).Cast<KeyValuePair<Enum, string>>().Where(
                    a => ((ProductType)a.Key) != ProductType.Swap && ((ProductType)a.Key) != ProductType.Swaption).ToList();
            this.financialTypeColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.financialTypeColumn.MinimumWidth = 40;
            this.financialTypeColumn.DropDownWidth = 100;
            this.financialTypeColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;
            this.financialTypeColumn.ReadOnly = true;
            this.financialTypeColumn.Frozen = true;
            this.financialTypeColumn.DefaultCellStyle.BackColor = Color.LightGray;

            // 
            // bondSwapLegDataGridViewComboBoxColumn
            // 
            this.bondSwapLegDataGridViewComboBoxColumn.DataPropertyName = "SwapLeg";
            this.bondSwapLegDataGridViewComboBoxColumn.HeaderText =
                Labels.DetailsGridController_InitColumns_Derivatives_Leg;
            this.bondSwapLegDataGridViewComboBoxColumn.Name = "SwapLeg";
            this.bondSwapLegDataGridViewComboBoxColumn.DisplayMember = "Value";
            this.bondSwapLegDataGridViewComboBoxColumn.ValueMember = "Key";
            this.bondSwapLegDataGridViewComboBoxColumn.DataSource =
                new ArrayList
                    {
                        new KeyValuePair<BondSwapLeg, string>(BondSwapLeg.Received, Labels.DetailsGridController_InitColumns_Received),
                        new KeyValuePair<BondSwapLeg, string>(BondSwapLeg.Paid, Labels.DetailsGridController_InitColumns_Paid),
                    };
            // EnumHelper.ToList(typeof(BondSwapLeg));
            this.bondSwapLegDataGridViewComboBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.bondSwapLegDataGridViewComboBoxColumn.MinimumWidth = 100;
            this.bondSwapLegDataGridViewComboBoxColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;

            // 
            // spreadDataGridViewTextBoxColumn
            // 
            this.spreadDataGridViewTextBoxColumn.DataPropertyName = "Spread";
            this.spreadDataGridViewTextBoxColumn.HeaderText = Labels.DetailsGridController_InitColumns_Spread;
            this.spreadDataGridViewTextBoxColumn.Name = "Spread";
            this.spreadDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.spreadDataGridViewTextBoxColumn.MinimumWidth = 100;
            this.spreadDataGridViewTextBoxColumn.DefaultCellStyle.Format = Constants.PERCENTAGE_CELL_FORMAT;
            this.spreadDataGridViewTextBoxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this.spreadDataGridViewTextBoxColumn.ValueList = this.variableAndVectorNames;

            // 
            // marketBasisDataGridViewTextBoxColumn
            // 
            this.marketBasisColumn.DataPropertyName = "MarketBasis";
            this.marketBasisColumn.HeaderText = Labels.DetailsGridController_InitColumns_Market_Basis;
            this.marketBasisColumn.Name = "MarketBasis";
            this.marketBasisColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.marketBasisColumn.MinimumWidth = 100;
            this.marketBasisColumn.DefaultCellStyle.Format = Constants.PERCENTAGE_CELL_FORMAT;
            this.marketBasisColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this.marketBasisColumn.ValueList = this.variableAndVectorNames;

            // 
            // firstRateDataGridViewTextBoxColumn
            // 
            this.firstRateDataGridViewTextBoxColumn.DataPropertyName = "FirstRate";
            this.firstRateDataGridViewTextBoxColumn.HeaderText = Labels.DetailsGridController_InitColumns_First_Coupon;
            this.firstRateDataGridViewTextBoxColumn.Name = "FirstRate";
            this.firstRateDataGridViewTextBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.firstRateDataGridViewTextBoxColumn.MinimumWidth = 100;
            this.firstRateDataGridViewTextBoxColumn.DefaultCellStyle.Format = "P4";
            this.firstRateDataGridViewTextBoxColumn.DefaultCellStyle.Alignment =
                DataGridViewContentAlignment.MiddleRight;

            // 
            // applicationFeeColumn
            // 
            this.applicationFeeColumn.DataPropertyName = "ApplicationFee";
            this.applicationFeeColumn.HeaderText = "Application Fees";
            this.applicationFeeColumn.ToolTipText = "Formula applied on the book value the start month only for the IN FUTURE products.";
            this.applicationFeeColumn.Name = "ApplicationFee";
            this.applicationFeeColumn.DefaultCellStyle.Format = Constants.MONEY_CELL_FORMAT;
            this.applicationFeeColumn.DefaultCellStyle.Alignment =
                DataGridViewContentAlignment.MiddleRight;
            this.applicationFeeColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.applicationFeeColumn.MinimumWidth = DecimalCellWidth;
            this.applicationFeeColumn.ValueList = this.variableAndVectorNames;
            // 
            // applicationChargeColumn
            // 
            this.applicationChargeColumn.DataPropertyName = "ApplicationCharge";
            this.applicationChargeColumn.HeaderText = "Application Charges";
            this.applicationChargeColumn.ToolTipText = "Formula applied on the book value the start month only for the IN FUTURE products.";
            this.applicationChargeColumn.Name = "ApplicationCharge";
            this.applicationChargeColumn.DefaultCellStyle.Format = Constants.MONEY_CELL_FORMAT;
            this.applicationChargeColumn.DefaultCellStyle.Alignment =
                DataGridViewContentAlignment.MiddleRight;
            this.applicationChargeColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.applicationChargeColumn.MinimumWidth = DecimalCellWidth;
            this.applicationChargeColumn.ValueList = this.variableAndVectorNames;

            // 
            // prePayementFeeColumn
            // 
            this.prePayementFeeColumn.DataPropertyName = "PrePayementFee";
            this.prePayementFeeColumn.HeaderText = "Prepayement Fees";
            this.prePayementFeeColumn.Name = "PrePayementFee";
            this.prePayementFeeColumn.DefaultCellStyle.Format = Constants.MONEY_CELL_FORMAT;
            this.prePayementFeeColumn.DefaultCellStyle.Alignment =
                DataGridViewContentAlignment.MiddleRight;
            this.prePayementFeeColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.prePayementFeeColumn.MinimumWidth = DecimalCellWidth;
            this.prePayementFeeColumn.ValueList = this.variableAndVectorNames;

            // 
            // recoveryLagColumnColumn
            // 
            this.recoveryLagColumn.DataPropertyName = "RecoveryLag";
            this.recoveryLagColumn.HeaderText = "Recovery Lag";
            this.recoveryLagColumn.Name = "RecoveryLag";
            this.recoveryLagColumn.DefaultCellStyle.Format = Constants.MONEY_CELL_FORMAT;
            this.recoveryLagColumn.DefaultCellStyle.Alignment =
                DataGridViewContentAlignment.MiddleRight;
            this.recoveryLagColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.recoveryLagColumn.MinimumWidth = DecimalCellWidth;

            this.columns = new DataGridViewColumn[]
                           {
                                   this.almIdDataGridViewTextBoxColumn,
                                   this.otherLegDataGridViewTextBoxColumn,
                               this.financialTypeColumn,
                               this.descriptionDataGridViewTextBoxColumn,
                               this.excludeDataGridViewCheckBoxColumn,
                               this.statusDataGridViewTextBoxColumn,
                               this.secIdDataGridViewTextBoxColumn,
                               this.isModelColumn,
                               this.amortizationTypeColumn,
                               this.amortizationColumn,
                               this.complementDataGridViewTextBoxColumn,
                               this.investmentRuleColumn,
                               this.growthColumn,
                               this.accountingDataGridViewComboBoxColumn,
                               this.origFaceColumn,
                               this.bookPriceColumn,
                               this.actuarialPriceColumn,
                               this.bookValueDataGridViewTextBoxColumn,
                               this.rollSpecDataGridViewComboBoxColumn,
                               this.rollRatioColumn,
                               this.startDateColumn,
                               this.durationDataGridViewTextBoxColumn,
                               this.expirationDateColumn,
                               this.couponColumn,
                               this.couponTypeColumn,
                               this.couponCalcTypeColumn,
                               this.couponBasisColumn,
                               this.periodDataGridViewComboBoxColumn,
                               this.taxDataGridViewComboBoxColumn,
                               this.attribDataGridViewTextBoxColumn,
                               //this.theoreticalCurrFaceColumn,
                               //this.isOriginBasedOnStartDateColumn,
                               this.cprColumn,
                               this.cdrColumn,
                               this.lgdColumn,
                               this.recoveryLagColumn,
                               this.currencyDataGridViewComboBoxColumn,
                               this.basel2DataGridViewTextBoxColumn,
                               this.rwaColumn,
                               this.tciDataGridViewTextBoxColumn,
                               this.liquidityEligibilityColumn,
                               this.liquidityHaircutColumn,
                               this.liquidityRunOffColumn,
                               this.bondSwapLegDataGridViewComboBoxColumn,
                               this.spreadDataGridViewTextBoxColumn,
                               this.marketBasisColumn,
                               this.firstRateDataGridViewTextBoxColumn,
                               this.applicationFeeColumn,
                               this.applicationChargeColumn,
                               this.prePayementFeeColumn,
                           };

            this.copyAbleColumns = new List<DataGridViewColumn>(new DataGridViewColumn[]
                                                                {
                                                                    this.financialTypeColumn,
                                                                    this.descriptionDataGridViewTextBoxColumn,
                                                                    this.isModelColumn,
                                                                    this.amortizationTypeColumn,
                                                                    this.amortizationColumn,
                                                                    this.complementDataGridViewTextBoxColumn,
                                                                    this.investmentRuleColumn,
                                                                    this.growthColumn,
                                                                    this.accountingDataGridViewComboBoxColumn,
                                                                    this.origFaceColumn,
                                                                    this.bookPriceColumn,
                                                                    this.actuarialPriceColumn,
                                                                    this.bookValueDataGridViewTextBoxColumn,
                                                                    this.rollSpecDataGridViewComboBoxColumn,
                                                                    this.rollRatioColumn,
                                                                    this.startDateColumn,
                                                                    this.couponColumn,
                                                                    this.couponTypeColumn,
                                                                    this.tciDataGridViewTextBoxColumn,
                                                                    this.couponCalcTypeColumn,
                                                                    this.couponBasisColumn,
                                                                    this.periodDataGridViewComboBoxColumn,
                                                                    this.durationDataGridViewTextBoxColumn,
                                                                    this.expirationDateColumn,
                                                                    this.taxDataGridViewComboBoxColumn,
                                                                    this.attribDataGridViewTextBoxColumn,
                                                                    //this.theoreticalCurrFaceColumn,
                                                                    //this.isOriginBasedOnStartDateColumn,
                                                                    this.cprColumn,
                                                                    this.cdrColumn,
                                                                    this.lgdColumn,
                                                                    this.tciDataGridViewTextBoxColumn,
                                                                    this.recoveryLagColumn,
                                                                    this.basel2DataGridViewTextBoxColumn,
                                                                    this.currencyDataGridViewComboBoxColumn,
                                                                    this.rwaColumn,
                                                                    this.liquidityEligibilityColumn,
                                                                    this.liquidityHaircutColumn,
                                                                    this.liquidityRunOffColumn,
                                                                    this.bondSwapLegDataGridViewComboBoxColumn,
                                                                    this.spreadDataGridViewTextBoxColumn,
                                                                    this.marketBasisColumn,
                                                                    this.firstRateDataGridViewTextBoxColumn,
                                                                    this.applicationFeeColumn,
                                                                    this.applicationChargeColumn,
                                                                    this.prePayementFeeColumn,
                                                                });
            this.grid.Columns.Clear();
            this.grid.Columns.AddRange(this.columns);
            this.grid.Columns[0].Width = 20;

            this.cellDateTimePicker = new DateTimePicker();
            this.cellDateTimePicker.Leave += new EventHandler(cellDateTimePicker_Leave);
            this.cellDateTimePicker.Validated += new EventHandler(cellDateTimePicker_Validated);
            this.cellDateTimePicker.Visible = false;
            this.cellDateTimePicker.CustomFormat = Constants.DateFormat;
            this.cellDateTimePicker.Format = DateTimePickerFormat.Custom;
            this.grid.Controls.Add(this.cellDateTimePicker);
        }

        void cellDateTimePicker_Validated(object sender, EventArgs e)
        {
            this.grid.CurrentCell.Value = cellDateTimePicker.Value.ToString();
        }

        void cellDateTimePicker_Leave(object sender, EventArgs e)
        {
            this.cellDateTimePicker.Visible = false;
        }

        private void UpdateVariableAndVectorNameList()
        {
            Stopwatch stopwatch = Stopwatch.StartNew();

            this.variableAndVectorNames = DomainTools.GetAllVariablesAndVectorNames(this.session);

            this.variableVectorAndDynamicVariableNames = this.variableAndVectorNames.Concat(this.session.Query<DynamicVariable>().Select(v => v.Name)).ToArray();

            stopwatch.Stop();

            Debug.WriteLine("UpdateVariableAndVectorNameList took " + stopwatch.ElapsedMilliseconds + "ms");
        }

        private void GridCurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (!this.grid.IsCurrentCellDirty)
            {
                return;
            }

            DataGridViewColumn column = this.grid.CurrentCell.OwningColumn;
            if (column is DataGridViewCheckBoxColumn ||
                column is DataGridViewComboBoxColumn ||
                column is CalendarColumn)
            {
                this.grid.CommitEdit(DataGridViewDataErrorContexts.Commit);
                this.grid.Refresh();
            }
        }

        private void GridColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (this.grid.Rows.Count == 0)
            {
                return;
            }

            this.grid.EndEdit();
            this.grid.CurrentCell = null;

            this.grid.ClearSelection();
            for (int i = 0; i < this.grid.Rows.Count - 1; i++)
            {
                this.grid[e.ColumnIndex, i].Selected = true;
            }
        }

        private void GridCellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (0 == this.grid.Rows.Count ||
                e.ColumnIndex < 0)
            {
                return;
            }

            string colName = this.grid.Columns[e.ColumnIndex].Name;

            if (this.IsTotalAtIndex(e.RowIndex) &&
                colName != "Description" &&
                colName != "BookValue" &&
                colName != "RWA")
            {
                e.Graphics.FillRectangle(new SolidBrush(e.CellStyle.BackColor), e.CellBounds);
                e.Graphics.DrawLine(new Pen(VisualStyleInformation.TextControlBorder),
                                    e.CellBounds.X, e.CellBounds.Y + e.CellBounds.Height - 1,
                                    e.CellBounds.X + e.CellBounds.Width - 1, e.CellBounds.Y + e.CellBounds.Height - 1);
                e.Handled = true;
            }
        }

        private void FormatGrowthCell(int rowIndex)
        {
            ProductInvestmentRule investmentRule =
                (ProductInvestmentRule)this.grid[this.investmentRuleColumn.Name, rowIndex].Value;

            DataGridViewCell growthCell = this.grid[this.growthColumn.Name, rowIndex];
            growthCell.ReadOnly = investmentRule == ProductInvestmentRule.Constant;
            growthCell.Style.BackColor = growthCell.ReadOnly ? Color.LightGray : Color.White;
            if (growthCell.ReadOnly)
            {
                growthCell.Value = null;
            }
        }

        private void FormatFrequencyCell(int rowIndex)
        {
            if (rowIndex < 0)
            {
                return;
            }
            Product product = this.grid.Rows[rowIndex].DataBoundItem as Product;
            if (product == null || product.Category == null)
            {
                return;
            }
            if (product.AmortizationType == ProductAmortizationType.NO_MATURITY ||
                product.AmortizationType == ProductAmortizationType.RESERVE_CASHFLOW ||
                    product.AmortizationType == ProductAmortizationType.RESERVE_NOMINAL)
            {
                product.Frequency = ProductFrequency.Monthly;
                this.EnableCell(rowIndex, this.periodDataGridViewComboBoxColumn.Name, false);
            }
            else
            {

                this.EnableCell(rowIndex, this.periodDataGridViewComboBoxColumn.Name, true);
            }
        }

        private void FormatExpirationDateCell(int rowIndex)
        {
            if (rowIndex < 0)
            {
                return;
            }
            Product product = this.grid.Rows[rowIndex].DataBoundItem as Product;
            if (product == null || product.Category == null)
            {
                return;
            }

            if (product.AmortizationType == ProductAmortizationType.NO_MATURITY)
            {
                this.EnableCell(rowIndex, this.durationDataGridViewTextBoxColumn.Name, false);
                this.EnableCell(rowIndex, this.expirationDateColumn.Name, false);

                return;//all blocked and that's all
            }

            if (product.ExpirationDate == null && (product.Duration == null || product.IntDuration == 0))
            {
                this.EnableCell(rowIndex, this.durationDataGridViewTextBoxColumn.Name, true);
                this.EnableCell(rowIndex, this.expirationDateColumn.Name, true);
            }
            else
            {
                this.EnableCell(rowIndex, this.durationDataGridViewTextBoxColumn.Name, !(product.ExpirationDate != null));
                this.EnableCell(rowIndex, this.expirationDateColumn.Name, (product.ExpirationDate != null));
            }

        }



        private void FormatAccountingCell(int rowIndex)
        {
            if (rowIndex < 0)
            {
                return;
            }
            Product product = this.grid.Rows[rowIndex].DataBoundItem as Product;
            if (product == null || product.Category == null)
            {
                return;
            }

            bool forceLandR = (product.Category.BalanceType == BalanceType.Equity ||
                               product.Category.BalanceType == BalanceType.Liability) &&
                              product.ProductType == ProductType.Bond;
            bool accountingBlocked = forceLandR;
            if (forceLandR)
            {
                product.Accounting = ProductAccounting.LAndR;
            }

            if (product.AmortizationType == ProductAmortizationType.RESERVE_CASHFLOW ||
                product.AmortizationType == ProductAmortizationType.RESERVE_NOMINAL)
            {
                product.Accounting = ProductAccounting.HFT;
                accountingBlocked = true;
            }

            this.EnableCell(rowIndex, this.accountingDataGridViewComboBoxColumn.Name, !accountingBlocked);
        }

        private void FormatInvestmentRuleColumn(int colIndex, int rowIndex)
        {
            Product product = this.grid.Rows[rowIndex].DataBoundItem as Product;
            if (product == null)
            {
                return;
            }

            this.EnableCell(rowIndex, this.investmentRuleColumn.Name, product.Attrib != ProductAttrib.Roll);
        }

        private void FormatFinancialTypeColumn(int colIndex, int rowIndex)
        {
            try
            {
                Product product = this.grid.Rows[rowIndex].DataBoundItem as Product;
                if (product == null || product.Category == null)
                {
                    return;
                }

                this.EnableCell(rowIndex, this.financialTypeColumn.Name, product.ProductType != ProductType.Swap && product.ProductType != ProductType.Swaption);
            }
            catch
            {
            }
        }

        private void FormatTCIColumn(int colIndex, int rowIndex)
        {
            try
            {
                Product product = this.grid.Rows[rowIndex].DataBoundItem as Product;
                if (product == null || product.Category == null)
                {
                    return;
                }

                this.EnableCell(rowIndex, this.tciDataGridViewTextBoxColumn.Name, product.Category.BalanceType != BalanceType.Derivatives && product.Category.BalanceType != BalanceType.OffBalanceSheet);
            }
            catch
            {
            }
        }


        private void GridCellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (AlreadyInGridValueChanged)
            {
                //update other leg if swap
                if (e.RowIndex < 0 || this.grid.CurrentCell == null) return;
                Product productSwap = this.GetBondAtRowIndex(e.RowIndex);

                //orig face - recalculate bookvalue
                if (e.ColumnIndex == this.origFaceColumn.Index)
                {
                    this.grid.Rows[this.grid.CurrentRow.Index].Selected = true;
                    this.grid.Rows[this.grid.CurrentRow.Index].Selected = false;
                    this.ComputeBookPriceValueAndBookValue(this.grid.CurrentRow.Index, productSwap, productSwap.BookPrice, false);

                    if (productSwap.OtherSwapLeg != null)
                    {
                        int positionSwap = FindRowIndexByALMID(productSwap.OtherSwapLeg.ALMIDDisplayed);
                        if (positionSwap > -1)
                        {
                            this.grid.Rows[positionSwap].Selected = true;
                            this.grid.Rows[positionSwap].Selected = false;
                            this.ComputeBookPriceValueAndBookValue(this.grid.CurrentRow.Index, productSwap.OtherSwapLeg, productSwap.OtherSwapLeg.BookPrice, false);
                        }
                    }
                }

                //accounting - if value changed through "apply value to entire column"
                if (e.ColumnIndex == this.accountingDataGridViewComboBoxColumn.Index)
                {
                    if ((productSwap.Accounting == ProductAccounting.LAndR || productSwap.Accounting == ProductAccounting.HTM))
                    {
                        if (productSwap.BookValue > 0)
                            productSwap.BookPrice = "100";
                        else
                            productSwap.BookPrice = "-100";
                        this.ComputeBookPriceValueAndBookValue(this.grid.CurrentRow.Index, productSwap, productSwap.BookPrice, false);
                        this.ResetRow(this.grid.CurrentRow.Index);

                        if (productSwap.OtherSwapLeg != null)
                        {
                            int positionSwap = FindRowIndexByALMID(productSwap.OtherSwapLeg.ALMIDDisplayed);
                            if (productSwap.OtherSwapLeg.BookValue > 0)
                                productSwap.OtherSwapLeg.BookPrice = "100";
                            else
                                productSwap.OtherSwapLeg.BookPrice = "-100";
                            this.ComputeBookPriceValueAndBookValue(positionSwap, productSwap.OtherSwapLeg, productSwap.OtherSwapLeg.BookPrice, false);
                            this.ResetRow(positionSwap);
                        }
                    }
                    if ((productSwap.Accounting == ProductAccounting.AFS || productSwap.Accounting == ProductAccounting.HFT))
                    {
                        Util.UpdateProductBookPrice(productSwap);
                        this.ComputeBookPriceValueAndBookValue(this.grid.CurrentRow.Index, productSwap, productSwap.BookPrice, false);
                        this.ResetRow(this.grid.CurrentRow.Index);
                        if (productSwap.OtherSwapLeg != null)
                        {
                            int positionSwap = FindRowIndexByALMID(productSwap.OtherSwapLeg.ALMIDDisplayed);
                            Util.UpdateProductBookPrice(productSwap.OtherSwapLeg);
                            this.ComputeBookPriceValueAndBookValue(positionSwap, productSwap.OtherSwapLeg, productSwap.OtherSwapLeg.BookPrice, false);
                            this.ResetRow(positionSwap);
                        }
                    }
                }

                //start date
                if (e.ColumnIndex == this.startDateColumn.Index)
                {
                    if (productSwap.OtherSwapLeg != null)
                    {
                        int positionSwap = FindRowIndexByALMID(productSwap.OtherSwapLeg.ALMIDDisplayed);
                        productSwap.OtherSwapLeg.StartDate = productSwap.StartDate;
                        this.ResetRow(positionSwap);
                    }
                }

                return;
            }

            AlreadyInGridValueChanged = true;
            int position = e.RowIndex;
            if (position < 0) return;

            this.FlushChanges(null);
            Product product = this.GetBondAtRowIndex(position);
            this.SetModifiedValues(true);


            string colName = this.grid.Columns[e.ColumnIndex].Name;

            if (e.ColumnIndex == this.amortizationTypeColumn.DisplayIndex)
            {
                //Product product = this.GetBondAtRowIndex(position);
                if (product != null && product.AmortizationType == ProductAmortizationType.NO_MATURITY)
                {
                    product.StartDate = null;
                    product.ExpirationDate = null;
                    product.Duration = Util.InfiniteDuration;
                    product.Frequency = ProductFrequency.Monthly;

                    this.ResetRow(position);
                }
            }

            if (e.ColumnIndex == this.financialTypeColumn.DisplayIndex)
            {
                this.ResetRow(position);
                this.SetEnabledStateByFinancialType(this.grid.Rows[position]);
                return;
            }

            if (colName == "AmortizationType")
            {
                this.FormatOrigFaceCell(position);
                //this.FormatThCurrFaceCell(e.RowIndex);
                this.FormatGrowthCell(position);
            }

            if (colName == startDateColumn.Name ||
                colName == durationDataGridViewTextBoxColumn.Name ||
                colName == expirationDateColumn.Name)
            {
                //recalculate real product duration
                if (product.ExpirationDate != null)
                {
                    DateTime? realStart = product.StartDate ?? Param.GetValuationDate(this.session);
                    if (realStart != null)
                    {
                        product.IntDuration = Util.GetMonthsBeetwenDates((DateTime)realStart, (DateTime)product.ExpirationDate)+1;
                    }

                }

            }

            if (colName == startDateColumn.Name)
            {
                product = this.GetBondAtRowIndex(position);
                if (product.StartDate != null)
                    product.StartDate = Tools.ResetDateToLastOfMonth(product.StartDate.GetValueOrDefault());
                this.ResetRow(position);
                
                BalanceBL.BalanceAdjustmentNeeded = true;
            }

            if ("Exclude" == colName ||
                "BookValue" == colName ||
                "Basel2" == colName ||
                "Currency" == colName ||
                "Accounting" == colName ||
                "Model" == colName)
            {
                //Product product = this.GetBondAtRowIndex(position);
                product.UpdateStatusProperty();
                this.ResetRow(position);

                BalanceBL.BalanceAdjustmentNeeded = true;

                if (colName == excludeDataGridViewCheckBoxColumn.Name ||
                    colName == isModelColumn.Name)
                {
                    if (product.OtherSwapLeg != null)
                    {
                        product.OtherSwapLeg.Exclude = product.Exclude;
                        product.OtherSwapLeg.IsModel = product.IsModel;
                    }
                    foreach (Product linkedProduct in product.LinkedProducts)
                    {
                        linkedProduct.Exclude = product.Exclude;
                        linkedProduct.IsModel = product.IsModel;
                    }
                }

                ComputeGridTotal(this.ProductList);
            }

            if (colName == this.amortizationTypeColumn.Name || colName == this.amortizationColumn.Name || colName == this.complementDataGridViewTextBoxColumn.Name)
            {
                //Product product = this.GetBondAtRowIndex(position);
                if (product.AmortizationType == ProductAmortizationType.VAR_NOMINAL)
                {
                    product.OrigFace = Util.GetValueWithFormula(product.Amortization).ToString();
                    this.ResetRow(position);
                }
                if (product.AmortizationType == ProductAmortizationType.RESERVE_CASHFLOW ||
                    product.AmortizationType == ProductAmortizationType.RESERVE_NOMINAL)
                {
                    this.grid.Cursor = Cursors.WaitCursor;
                    Util.SetOrigFaceAndBookPriceForCashflow(product);
                    this.ResetRow(position);
                    ComputeGridTotal(this.ProductList);
                    this.grid.Cursor = Cursors.Arrow;
                }

            }

            //if ("ApplicationFee" == colName)
            //{
            //    Product product = this.GetBondAtRowIndex(position);

            //}

            if ("OrigFace" == colName ||
                "BookPrice" == colName)
            {
                //Product product = this.GetBondAtRowIndex(position);
                if (!string.IsNullOrEmpty(product.OrigFace) && !string.IsNullOrEmpty(product.BookPrice))
                {
                    string formula = (string)this.grid[this.bookPriceColumn.Index, position].Value;

                    this.ComputeBookPriceValueAndBookValue(position, product, formula, true);
                }
                else
                {
                    product.UpdateStatusProperty();
                    this.ResetRow(position);
                }

                BalanceBL.BalanceAdjustmentNeeded = true;
            }

            if (colName == "Model")
            {
                this.FormatModelCells(position);
            }

            if (colName == RecoveryLagColumnName)
            {

            }
            if ((colName == this.bondSwapLegDataGridViewComboBoxColumn.Name ||
                 colName == this.excludeDataGridViewCheckBoxColumn.Name)
                && this.grid.CurrentRow != null)
            {
                //Product product = this.grid.CurrentRow.DataBoundItem as Product;
                if (product != null && product.OtherSwapLeg != null)
                {
                    DataGridViewRow row = this.GetRowByDataBound(product.OtherSwapLeg);
                    if (row != null)
                    {
                        this.grid.UpdateCellValue(e.ColumnIndex, row.Index);
                    }
                }
            }

            if (colName == this.rollSpecDataGridViewComboBoxColumn.Name)
            {
                if (this.grid[e.ColumnIndex, position].Value.ToString() == CreateNew)
                {
                    Category[] rollCategories = Category.FindByBalanceTypeAndCatType(this.session, this.category.BalanceType,
                                                                                     CategoryType.Roll);

                    Category currentRollCat = rollCategories.FirstOrDefault(c => c.Label == Category.DEFAULT_ROLL_LABEL);

                    if (currentRollCat == null) return;

                    Product newProduct = new Product();
                    newProduct.Category = currentRollCat;
                    newProduct.Currency = Param.GetValuationCurrency(this.session);
                    newProduct.Duration = Param.GetDefaultBondDuration(this.session);
                    newProduct.StartDate = null;
                    newProduct.GenerateALMID(this.session);
                    newProduct.Attrib = ProductAttrib.Roll;
                    newProduct.InvestmentRule = ProductInvestmentRule.Roll;

                    newProduct.Create(this.session);
                    currentRollCat.Bonds.Add(newProduct);
                    this.FlushChanges(null);

                    this.RefreshRollBonds();
                    this.grid[e.ColumnIndex, position].Value = newProduct.ALMIDDisplayed;
                    this.currentEditComboBox.Text = newProduct.ALMIDDisplayed;
                    this.grid.UpdateCellValue(e.ColumnIndex, position);
                }
            }

            if (this.grid.CurrentRow != null && colName == this.accountingDataGridViewComboBoxColumn.Name)
            {
                //Product product = this.grid.CurrentRow.DataBoundItem as Product;
                if (product != null)
                {
                    DataGridViewCell bookPriceCell = this.grid.CurrentRow.Cells[this.bookPriceColumn.Index];
                    this.EnableCell(bookPriceCell.RowIndex, this.bookPriceColumn.Name,
                                    product.ProductType != ProductType.StockEquity &&
                                    (product.Accounting != ProductAccounting.AFS &&
                                     product.Accounting != ProductAccounting.HFT));
                }
            }

            if (this.grid.CurrentRow != null &&
                (colName == this.accountingDataGridViewComboBoxColumn.Name ||
                 colName == this.startDateColumn.Name ||
                 colName == this.couponColumn.Name ||
                 colName == this.couponBasisColumn.Name ||
                 colName == this.couponTypeColumn.Name ||
                 colName == this.firstRateDataGridViewTextBoxColumn.Name ||
                 colName == this.marketBasisColumn.Name ||
                 colName == this.durationDataGridViewTextBoxColumn.Name ||
                 colName == this.spreadDataGridViewTextBoxColumn.Name ||
                 colName == this.expirationDateColumn.Name ||
                 colName == this.bookPriceColumn.Name))
            {
                

                if (colName == this.couponColumn.Name && product != null)
                {
                    Variable variable = Variable.FindByName(this.session, product.Coupon);
                    if (variable != null && product.ProductType == ProductType.BondVr)
                    {
                        FundamentalVector vector = FundamentalVector.FindFirst(this.session, Expression.Eq("Variable", variable));
                        if (vector != null)
                        {
                            product.CouponType = CouponType.Floating;
                            product.Frequency = vector.Frequency;
                        }
                    }

                    this.ResetRow(this.grid.CurrentRow.Index);
                }

                if (Param.GetAutoCalculateBookPrice(this.session))
                {
                    this.RecalculateBookPriceForCurentRow();
                }
            }

            //if (colName != startDateColumn.Name)
            this.SetModifiedValues(true);
            AlreadyInGridValueChanged = false;
        }

        private void ComputeBookPriceValueAndBookValue(int position, Product product, string formula, bool resetItem)
        {
            double bookPriceValue = 100;
            if (formula != "100")
            {
                try
                {
                    bookPriceValue = Util.GetValueWithFormula(formula,true);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }

            product.BookPriceValue = bookPriceValue;
            product.ComputeBookValue(bookPriceValue);
            product.UpdateStatusProperty();
            if (null != this.ProductList)
            {
                if (resetItem) this.ProductList.ResetItem(position);
                ComputeGridTotal(this.ProductList);
            }
        }

        private void RecalculateBookPriceForCurentRow()
        {

            if (this.grid.CurrentRow == null) return;


            Product product = this.grid.CurrentRow.DataBoundItem as Product;
            //bool haveToComputeBookPrice = product != null &&
            //                              (product.Accounting != ProductAccounting.AFS && product.Accounting != ProductAccounting.HFT) ||
            //                              product.ProductType == ProductType.StockEquity;

            if ( (product != null &&
                                                (product.Accounting == ProductAccounting.AFS || 
                                                product.Accounting == ProductAccounting.HFT) &&
                                           product.ProductType != ProductType.StockEquity))
            {
                this.grid.Cursor = Cursors.WaitCursor;
                try
                {
                    this.FlushChanges(null);
                    Util.UpdateProductBookPrice(product);

                    if (product.OtherSwapLeg != null)
                    {
                        Util.UpdateProductBookPrice(product.OtherSwapLeg);
                    }

                    if (this.ProductList != null)
                    {
                        this.ResetRow(this.grid.CurrentRow.Index);
                        if (product.OtherSwapLeg != null)
                        {
                            int otherLegIndex = this.ProductList.IndexOf(product.OtherSwapLeg);
                            if (otherLegIndex >= 0)
                            {
                                this.ResetRow(otherLegIndex);
                            }
                        }
                        this.ComputeGridTotal();
                    }
                }
                finally
                {
                    this.grid.Cursor = Cursors.Default;
                    
                }
            }
        }

        private void FormatModelCells(int rowIndex)
        {
            bool isModel = (bool)this.grid["Model", rowIndex].Value;

            //this.FormatThCurrFaceCell(rowIndex);
            //this.EnableCell(rowIndex, isOriginBasedOnStartDateColumn.Name, isModel);
            this.EnableCell(rowIndex, CprColumnName, isModel);
            this.EnableCell(rowIndex, CdrColumnName, isModel);
            this.EnableCell(rowIndex, LgdColumnName, isModel);
            this.EnableCell(rowIndex, RecoveryLagColumnName, isModel);
        }

        private void EnableCell(int rowIndex, string colName, bool enabled)
        {
            DataGridViewCell cell = this.grid[colName, rowIndex];
            cell.ReadOnly = !enabled;
            cell.Style.BackColor = !enabled ? Color.LightGray : Color.White;
        }

        private void FormatOrigFaceCell(int rowIndex)
        {
            ProductAmortizationType productAmortizationType =
                (ProductAmortizationType)this.grid[this.amortizationTypeColumn.Name, rowIndex].Value;

            this.EnableCell(rowIndex, this.origFaceColumn.Name, productAmortizationType != ProductAmortizationType.VAR_NOMINAL && 
                                                                productAmortizationType != ProductAmortizationType.RESERVE_CASHFLOW && 
                                                                productAmortizationType != ProductAmortizationType.RESERVE_NOMINAL);
            //bool formulaAlowed = productAmortizationType == ProductAmortizationType.VAR;

            //((AutoCompleteCell)this.grid[this.origFaceColumn.Name, rowIndex]).ValueList =
            //    formulaAlowed ? this.variableAndVectorNames : new string[0];
        }

        private void FormatBookPriceCell(int rowIndex)
        {
            return;
            // TODO VALIDATION PRODUIT
            Product product = this.grid.Rows[rowIndex].DataBoundItem as Product;
            if (product == null) return;

            ((AutoCompleteCell)this.grid[this.bookPriceColumn.Name, rowIndex]).ValueList =
                (product.ProductType == ProductType.StockEquity)
                    ? this.variableAndVectorNames
                    : new string[0];
        }

        private void FormatIsModelCell(int rowIndex)
        {
            return;
            // TODO VALIDATION PRODUIT

            Product product = this.grid.Rows[rowIndex].DataBoundItem as Product;
            if (product == null) return;

            bool isModelAlowed = (product.Accounting == ProductAccounting.HTM ||
                                  product.Accounting == ProductAccounting.LAndR) &&
                                 product.ProductType != ProductType.StockEquity;

            if (!isModelAlowed)
            {
                product.IsModel = false;
            }

            this.EnableCell(rowIndex, this.isModelColumn.Name, isModelAlowed);
        }

        private void FormatAmortizationTypeCell(int rowIndex)
        {
            return;
            // TODO VALIDATION PRODUIT

            Product product = this.grid.Rows[rowIndex].DataBoundItem as Product;
            if (product == null) return;

            bool forceBullet = product.ProductType != ProductType.Swap && product.ProductType != ProductType.StockEquity;

            this.EnableCell(rowIndex, this.amortizationTypeColumn.Name, forceBullet);

            if (forceBullet)
            {
                product.AmortizationType = ProductAmortizationType.BULLET;
            }
        }

        private void FormatAmortizationCell(int rowIndex)
        {
            Product product = this.grid.Rows[rowIndex].DataBoundItem as Product;
            if (product == null) return;

            this.EnableCell(rowIndex, this.amortizationColumn.Name,
                            product.AmortizationType == ProductAmortizationType.NO_MATURITY ||
                            product.AmortizationType == ProductAmortizationType.VAR ||
                            product.AmortizationType == ProductAmortizationType.VAR_NOMINAL ||
                            product.AmortizationType == ProductAmortizationType.CST_PAY ||
                            product.AmortizationType == ProductAmortizationType.RESERVE_CASHFLOW ||
                            product.AmortizationType == ProductAmortizationType.RESERVE_NOMINAL);
        }

        private void FormatStartDateCell(int rowIndex)
        {
            Product product = this.grid.Rows[rowIndex].DataBoundItem as Product;
            if (product == null) return;

            this.EnableCell(rowIndex, this.startDateColumn.Name,
                            product.AmortizationType != ProductAmortizationType.NO_MATURITY);
        }

        private void FormatCouponCell(int rowIndex)
        {

            Product product = this.grid.Rows[rowIndex].DataBoundItem as Product;
            if (product == null) return;

            if (product.AmortizationType == ProductAmortizationType.RESERVE_CASHFLOW ||
                product.AmortizationType == ProductAmortizationType.RESERVE_NOMINAL)
            {
                product.Coupon = "0";
                this.EnableCell(rowIndex, this.couponColumn.Name, false);
                return;
            }
            this.EnableCell(rowIndex, this.couponColumn.Name, true);
            //((AutoCompleteCell)this.grid[this.couponColumn.Name, rowIndex]).ValueList =
            //    product.IsCouponFormulaAllowed ? this.variableAndVectorNames : new string[0];
        }

        private void FormatAttribCell(string colName, int rowIndex)
        {
            if (rowIndex < 0)
            {
                return;
            }
            Product product = this.grid.Rows[rowIndex].DataBoundItem as Product;
            if (product == null)
            {
                return;
            }

            this.EnableCell(rowIndex, colName, product.OtherSwapLeg == null);
        }

        public void ResetRow(int rowIndex)
        {
            if (null != this.ProductList)
            {
                this.ProductList.ResetItem(rowIndex);
            }

        }

        public void ComputeGridTotal()
        {
            ComputeGridTotal(this.ProductList);
        }

        private static void ComputeGridTotal(SortableBindingList<Product> gridData)
        {
            using (ISession session = SessionManager.OpenSession())
            {
                if (gridData == null || gridData.Count == 0 || !gridData[gridData.Count - 1].IsTotal)
                {
                    return;
                }

                double sumBookValue = 0;
                double sumRWA = 0;
                Currency valuationCurrency = Param.GetValuationCurrency(session);
                foreach (Product bond in gridData)
                {
                    if (bond.HasBookValue() && !bond.Exclude)
                    {
                        sumBookValue +=
                            CurrencyBL.ConvertToValuationCurrency(bond.BookValue.HasValue ? bond.BookValue.Value : 0,
                                                                  bond.Currency, valuationCurrency);
                        sumRWA += CurrencyBL.ConvertToValuationCurrency(bond.RWA.HasValue ? bond.RWA.Value : 0,
                                                                        bond.Currency, valuationCurrency);
                    }
                }

                Product total = gridData[gridData.Count - 1];
                total.BookValue = sumBookValue;
                total.RWA = sumRWA;
                try
                {
                    gridData.ResetItem(gridData.Count - 1);
                }
                catch { }
            }
        }

        private void AddTotalRow(SortableBindingList<Product> gridData)
        {
            if (null == gridData || 0 == gridData.Count)
            {
                return;
            }

            Product totalProduct = new Product();
            totalProduct.IsTotal = true;
            totalProduct.Description = GetTotalRowDescription();
            gridData.Add(totalProduct);

            ComputeGridTotal(gridData);
        }

        private string GetTotalRowDescription()
        {
            return string.Format(Labels.DetailsGridController_GetTotalRowDescription_TOTAL___0__,
                                 Param.GetValuationCurrency(this.session).Symbol);
        }

        private void GridCellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            e.Cancel = this.IsTotalAtIndex(e.RowIndex);

            if (!e.Cancel)
                originalCurrentProduct = Product.Clone(GetBondAtRowIndex(e.RowIndex));
        }

        private bool IsTotalAtIndex(int rowIndex)
        {
            Product product = this.GetBondAtRowIndex(rowIndex);

            return null != product && product.IsTotal;
        }

        private void GridCellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            string colName = this.grid.Columns[e.ColumnIndex].Name;
            Product product = this.GetBondAtRowIndex(e.RowIndex);

            if (e.ColumnIndex < 0 || e.RowIndex < 0) return;

            if (e.ColumnIndex == this.startDateColumn.DisplayIndex && e.Value != null && e.Value is DateTime)
            {
                e.Value = ((DateTime)e.Value).ToString(Constants.DateFormat);
                e.FormattingApplied = true;
            }

            if (e.ColumnIndex == this.financialTypeColumn.DisplayIndex && product != null)
            {
                if (e.Value as string != "Swaption" && product.ProductType == ProductType.Swaption)
                {
                    e.FormattingApplied = true;
                    e.Value = "Swaption";
                }
                if (e.Value as string != "Swap" && product.ProductType == ProductType.Swap)
                {
                    e.FormattingApplied = true;
                    e.Value = "Swap";
                }
                if (product.Category != null && product.Category.Type != CategoryType.Normal && product.Category.Type != CategoryType.Roll)
                {
                    product.Frequency = ProductFrequency.Monthly;
                    EnableCell(e.RowIndex, "Frequency", false);
                }
                else
                {
                    EnableCell(e.RowIndex, "Frequency", true);
                }
            }

            if (e.ColumnIndex == this.origFaceColumn.Index || e.ColumnIndex == this.bookPriceColumn.Index)
            {
                DataGridViewTextBoxCell cell = (DataGridViewTextBoxCell)this.grid[e.ColumnIndex, e.RowIndex];
                double? value = ParsingTools.ClipboardStringToDouble(cell.Value as string);
                if (value.HasValue)
                {
                    string formattedValue = "";
                    if (e.ColumnIndex == this.origFaceColumn.Index)
                        formattedValue = value.Value.ToString("N0");
                    else
                        formattedValue = value.Value.ToString("N2");
                    if ((string)cell.Value != formattedValue)
                    {
                        MainForm.ModifiedValuesSuspended = true;
                        cell.Value = formattedValue; //cdu : speedup // todo
                        MainForm.ModifiedValuesSuspended = false;
                    }
                }
            }

            if (colName == this.tciDataGridViewTextBoxColumn.Name)
            {
                this.FormatTCIColumn(e.ColumnIndex, e.RowIndex);
            }


            if (colName == this.financialTypeColumn.Name)
            {
                this.FormatFinancialTypeColumn(e.ColumnIndex, e.RowIndex);
            }

            if (colName == this.investmentRuleColumn.Name)
            {
                this.FormatInvestmentRuleColumn(e.ColumnIndex, e.RowIndex);
            }

            if (colName == this.accountingDataGridViewComboBoxColumn.Name)
            {
                this.FormatAccountingCell(e.RowIndex);
            }

            if (colName == this.periodDataGridViewComboBoxColumn.Name)
            {
                this.FormatFrequencyCell(e.RowIndex);
            }

            if (colName == this.expirationDateColumn.Name || colName == this.durationDataGridViewTextBoxColumn.Name)
            {
                FormatExpirationDateCell(e.RowIndex);
            }

            if (colName == this.growthColumn.Name)
            {
                this.FormatGrowthCell(e.RowIndex);
            }

            if (colName == this.amortizationTypeColumn.Name)
            {
                this.FormatAmortizationTypeCell(e.RowIndex);
            }

            if (colName == this.amortizationColumn.Name)
            {
                this.FormatAmortizationCell(e.RowIndex);
            }

            if (colName == this.couponColumn.Name)
            {
                this.FormatCouponCell(e.RowIndex);

                if (!this.couponColumn.ValueList.Contains(e.Value as string))
                {
                    double? value = ParsingTools.ClipboardStringToDouble(e.Value as string);
                    if (value.HasValue)
                    {
                        string formattedValue = value.Value.ToString("P4");
                        if ((string)e.Value != formattedValue)
                        {
                            MainForm.ModifiedValuesSuspended = true;
                            e.Value = formattedValue;
                            MainForm.ModifiedValuesSuspended = false;
                        }
                    }
                }
            }

            if (colName == this.attribDataGridViewTextBoxColumn.Name)
            {
                this.FormatAttribCell(colName, e.RowIndex);
            }

            if (colName == this.origFaceColumn.Name)
            {
                this.FormatOrigFaceCell(e.RowIndex);
            }

            if (colName == this.bookPriceColumn.Name)
            {
                this.FormatBookPriceCell(e.RowIndex);
            }

            if (colName == this.bondSwapLegDataGridViewComboBoxColumn.Name)
            {
                this.EnableCell(e.RowIndex, colName, product.IsDerivative);
            }

            if (colName == this.isModelColumn.Name)
            {
                this.FormatIsModelCell(e.RowIndex);
            }

            if (colName == this.startDateColumn.Name)
            {
                this.FormatStartDateCell(e.RowIndex);
            }

            if (colName == this.marketBasisColumn.Name)
            {
                //this.EnableCell(e.RowIndex, colName,
                //    product.Accounting != ProductAccounting.HTM && product.Accounting != ProductAccounting.LAndR);
            }

            if (colName == CprColumnName || 
                colName == CdrColumnName || 
                colName == LgdColumnName || 
                colName == RecoveryLagColumnName //||
                //colName == isOriginBasedOnStartDateColumn.Name
                )
            {
                this.EnableCell(e.RowIndex, colName, product.IsModel);
            }



            if (colName == TheoreticalCurrFaceColumnName)
            {
                this.EnableCell(e.RowIndex, TheoreticalCurrFaceColumnName,
                                (product.AmortizationType == ProductAmortizationType.VAR ||
                                product.AmortizationType == ProductAmortizationType.VAR_NOMINAL) && product.IsModel);
            }

            if (colName == this.prePayementFeeColumn.Name)
            {
                this.EnableCell(e.RowIndex, this.prePayementFeeColumn.Name,
                                product.IsModel);
            }

            if (colName == this.bookPriceColumn.Name)
            {
                this.EnableCell(e.RowIndex, this.bookPriceColumn.Name,
                                (product.ProductType == ProductType.StockEquity ||
                                (product.Accounting != ProductAccounting.AFS &&
                                 product.Accounting != ProductAccounting.HFT)) &&
                                 (!product.hasStartDateInPast(Param.GetValuationDate(this.session)??DateTime.Now)));
                if (product.hasStartDateInPast(Param.GetValuationDate(this.session) ?? DateTime.Now) && string.IsNullOrEmpty(product.ActuarialPrice))
                    product.ActuarialPrice = "100";
            }

            if (colName == this.actuarialPriceColumn.Name)
            {
                this.EnableCell(e.RowIndex, this.actuarialPriceColumn.Name,
                                product.ProductType != ProductType.StockEquity &&
                                (product.Accounting != ProductAccounting.LAndR &&
                                product.Accounting != ProductAccounting.HTM));

                DataGridViewTextBoxCell cell = (DataGridViewTextBoxCell)this.grid[e.ColumnIndex, e.RowIndex];
                double? value = ParsingTools.ClipboardStringToDouble(cell.Value as string);
                if (value.HasValue)
                {
                    string formattedValue = value.Value.ToString("N2");
                    if ((string)cell.Value != formattedValue)
                    {
                        MainForm.ModifiedValuesSuspended = true;
                        cell.Value = formattedValue; //cdu : speedup // todo
                        MainForm.ModifiedValuesSuspended = false;
                    }
                }
            }

            if (colName == "Status")
            {
                switch (product.Status)
                {
                    case Product.BondStatus.None:
                        e.Value = Properties.Resources.gray_sphere;
                        break;
                    case Product.BondStatus.OK:
                        e.Value = Properties.Resources.green_sphere;
                        break;
                    case Product.BondStatus.Warning:
                        e.Value = Properties.Resources.yellow_sphere;
                        break;
                    case Product.BondStatus.Error:
                        e.Value = Properties.Resources.red_sphere;
                        break;
                }
            }

            if (product != null && product.IsTotal)
            {
                e.CellStyle.BackColor = Color.LemonChiffon;
                e.CellStyle.SelectionBackColor = e.CellStyle.BackColor;
                e.CellStyle.SelectionForeColor = e.CellStyle.ForeColor;
                e.CellStyle.Font = new Font(e.CellStyle.Font, FontStyle.Bold);
            }

            if (colName == this.rollRatioColumn.Name)
            {
                this.EnableCell(e.RowIndex, colName, !product.IsTotal && product.HasRoll);

                if (!this.rollRatioColumn.ValueList.Contains(e.Value as string))
                {
                    double? value = ParsingTools.ClipboardStringToDouble(e.Value as string);
                    if (value.HasValue)
                    {
                        string formattedValue = value.Value.ToString("P2");
                        if ((string)e.Value != formattedValue)
                        {
                            MainForm.ModifiedValuesSuspended = true;
                            e.Value = formattedValue;
                            MainForm.ModifiedValuesSuspended = false;
                        }
                    }
                }

            }

            if (colName == this.liquidityEligibilityColumn.Name)
            {
                this.EnableCell(e.RowIndex, colName,
                                !product.IsTotal && product.Category.BalanceType == BalanceType.Asset);
            }

            if (colName == this.liquidityHaircutColumn.Name)
            {
                this.EnableCell(e.RowIndex, colName,
                                !product.IsTotal && product.Category.BalanceType == BalanceType.Asset &&
                                (product.LiquidityEligibility == LiquidityEligibility.Level1 ||
                                 product.LiquidityEligibility == LiquidityEligibility.Level2 ||
                                 product.LiquidityEligibility == LiquidityEligibility.Level2B));
            }

            if (colName == this.liquidityRunOffColumn.Name)
            {
                this.EnableCell(e.RowIndex, colName,
                                !product.IsTotal && product.LiquidityEligibility == LiquidityEligibility.NotEligible);
            }

            if (product != null && (product.ProductType == ProductType.Swap || product.ProductType == ProductType.Swaption))
            {
                this.grid.Rows[e.RowIndex].DefaultCellStyle.BackColor = Constants.Colors.SwapLeg;
            }
            else if (product != null &&
                     product.ProductType == ProductType.StockEquity &&
                     this.accountingDataGridViewComboBoxColumn.Index >= 0)
            {
                DataGridViewComboBoxCell accountingCell =
                    (DataGridViewComboBoxCell)
                    this.grid.Rows[e.RowIndex].Cells[this.accountingDataGridViewComboBoxColumn.Index];
                if (accountingCell.DataSource == this.accountingDataGridViewComboBoxColumn.DataSource)
                {
                    accountingCell.DataSource = new ArrayList { ProductAccounting.HFT, ProductAccounting.AFS };
                }
            }
            else if (product != null && product.ProductType == ProductType.Cap)
            {
                this.grid.Rows[e.RowIndex].DefaultCellStyle.BackColor = Constants.Colors.Cap;
            }
            else if (product != null && product.ProductType == ProductType.Floor)
            {
                this.grid.Rows[e.RowIndex].DefaultCellStyle.BackColor = Constants.Colors.Floor;
            }

            if (e.ColumnIndex == this.couponCalcTypeColumn.DisplayIndex)
            {
                this.EnableCell(e.RowIndex, colName, product.ProductType != ProductType.BondFr && product.CouponType != CouponType.Fixed);
            }

            if (e.ColumnIndex == this.spreadDataGridViewTextBoxColumn.Index)
            {
                this.EnableCell(e.RowIndex, this.spreadDataGridViewTextBoxColumn.Name, product != null && product.IsVariableRate);

                if (!this.spreadDataGridViewTextBoxColumn.ValueList.Contains(e.Value as string))
                {
                    double? value = ParsingTools.ClipboardStringToDouble(e.Value as string);
                    if (value.HasValue)
                    {
                        string formattedValue = value.Value.ToString("P4");
                        if ((string)e.Value != formattedValue)
                        {
                            MainForm.ModifiedValuesSuspended = true;
                            e.Value = formattedValue;
                            MainForm.ModifiedValuesSuspended = false;
                        }
                    }
                }
            }
        }

        private Product GetBondAtRowIndex(int rowIndex)
        {
            try
            {
                if (rowIndex < 0)
                {
                    return null;
                }

                return this.grid.Rows[rowIndex].DataBoundItem as Product;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }
            return null;
        }

        private DataGridViewRow GetRowByDataBound(object dataBound)
        {
            return this.grid.Rows.Cast<DataGridViewRow>().FirstOrDefault(row => row.DataBoundItem == dataBound);
        }

        private static void GridDataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            e.Cancel = false;
            e.ThrowException = false;

            // Debug.WriteLine("Details Grid DataError: " + e.Exception);
        }

        private void GridUserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            // todo: show confirmation
            e.Cancel = true;
            this.DeleteRows(e.Row.Index);
        }

        private void GridUserDeletedRow(object sender, DataGridViewRowEventArgs e)
        {
            this.CheckEmptyGrid();
        }

        public void DeleteRows(int menuRowIndex)
        {
            List<Product> toDelete = new List<Product>();
            int line = 0;
            if (this.grid.SelectedRows.Count == 0)
            {
                toDelete.Add(this.grid.Rows[menuRowIndex].DataBoundItem as Product);
                line = menuRowIndex;
                this.DeleteRow(this.grid.Rows[menuRowIndex]);
            }
            else
            {
                while (this.grid.SelectedRows.Count > 0)
                {
                    DataGridViewRow row = this.grid.SelectedRows[0];
                    toDelete.Add(row.DataBoundItem as Product);
                    row.Selected = false;
                    this.DeleteRow(row);
                }
            }

            this.FlushChanges(new RemoveLinesBalanceSheetOperation(category,CurrentPage,line,toDelete, toDelete));

            this.CheckEmptyGrid();

            BalanceBL.BalanceAdjustmentNeeded = true;
        }

        private void DeleteRow(DataGridViewRow row)
        {
            Product product = row.DataBoundItem as Product;
            if ((product != null && product.IsTotal) || row.Index == -1)
            {
                return;
            }

            if (product != null)
            {
                //fix products non-swaptions with LinkedProducts before delete
                if (product.LinkedProducts != null && product.LinkedProducts.Count > 0 && product.ProductType != ProductType.Swaption)
                {
                    product.LinkedProducts.Clear();
                }

                this.category.Bonds.Remove(product);
                product.Delete(this.session);
                if (product.Attrib == ProductAttrib.Roll)
                {
                    RemoveRollFromBonds(product.ALMIDDisplayed);
                    this.RefreshRollBonds();
                }

                if (product.LinkedProducts.Count > 0)
                {
                    Product[] linkedProducts = product.LinkedProducts.ToArray();
                    foreach (Product linkedProduct in linkedProducts)
                    {
                        DataGridViewRow gridViewRow = this.GetRowByDataBound(linkedProduct);
                        if (gridViewRow != null)
                        {
                            linkedProduct.LinkedProducts.Clear();
                            this.DeleteRow(gridViewRow);
                        }
                        product.LinkedProducts.Clear();
                    }
                }

                if (product.OtherSwapLeg != null)
                {
                    DataGridViewRow gridViewRow = this.GetRowByDataBound(product.OtherSwapLeg);
                    if (gridViewRow != null)
                    {
                        product.OtherSwapLeg.OtherSwapLeg = null;
                        this.DeleteRow(gridViewRow);
                    }
                }
            }

            if (this.grid.Rows.Contains(row)) this.grid.Rows.Remove(row);
            this.SetModifiedValues(true);
        }

        private void RemoveRollFromBonds(string rollId)
        {
            foreach (Product bond in Product.FindAll(this.session))
            {
                if (bond.RollSpec == rollId)
                {
                    bond.RollSpec = Product.NoRoll;
                }
            }
        }

        private void CheckEmptyGrid()
        {
            if (this.grid.Rows.Count == 1)
            {
                this.grid.Rows.Clear();
            }
        }

        private void RefreshSession()
        {
            if (this.session != null)
            {
                this.FlushChanges(null);

                this.session.Dispose();
            }

            this.session = SessionManager.OpenSession();
        }

        public int Refresh(Category newCategory, int pageIndex, bool resetColumns)
        {
            this.CurrentPage = pageIndex;
            return this.Refresh(newCategory == null ? (int?)null : newCategory.Id, resetColumns);
        }

        public int Refresh(int? categoryId, bool resetColumns)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();

            //MTU BalanceBL.AjustBalanceIfNeeded();
            BalanceBL.AjustBalance();

            this.RefreshSession();

            if (resetColumns)
            {
                this.InitColumns();
            }

            if (categoryId == 0)
            {
                this.ProductList = null;
                return 0;
            }

            this.category = categoryId == null ? null : Category.Find(this.session, categoryId.Value);



            if (null == this.category)
            {
                this.ProductList = null;
                return 0;
            }

            if (this.category.BalanceType == BalanceType.Equity)
            {
                this.basel2DataGridViewTextBoxColumn.ValueList = new[]
                                                                 {
                                                                     Product.Basel2Types.CoreEquity,
                                                                     Product.Basel2Types.OtherTiers1,
                                                                     Product.Basel2Types.Tiers2,
                                                                 };
            }
            else
            {
                this.basel2DataGridViewTextBoxColumn.ValueList = null;
            }

            if (this.category.BalanceType == BalanceType.Derivatives)
            {
                this.grid.Columns[this.isModelColumn.Index].HeaderText = "Margin";
            }

            if (this.category.IsNew)
            {
                return 0;
            }

            if (this.grid.RowTemplate != null && this.grid.RowTemplate.HeaderCell != null && this.grid.RowTemplate.HeaderCell.ContextMenuStrip != null)
            {
                this.grid.RowTemplate.HeaderCell.ContextMenuStrip.Enabled =
                    this.category.Type != CategoryType.Treasury &&
                    this.category.Type != CategoryType.CashAdj &&
                    this.category.Type != CategoryType.PositiveDerivativesValuation &&
                    this.category.Type != CategoryType.NegativeDerivativesValuation;
            }

            SortableBindingList<Product> gridData = new SortableBindingList<Product>();

            int totalProductCount;
            IEnumerable<Product> products = Category.LoadProducts(this.session, this.category, this.CurrentPage, Settings.Default.CategoryRowsPerPages, out totalProductCount);
            bool hasSwapOrSwaptions = false;
            foreach (Product product in products)
            {
                this.FixProductRoll(product);
                gridData.Add(product);
                if (product.ProductType == ProductType.Swap || product.ProductType == ProductType.Swaption)
                    hasSwapOrSwaptions = true;
            }
            if (hasSwapOrSwaptions)
                this.otherLegDataGridViewTextBoxColumn.Visible = true;
            else
                this.otherLegDataGridViewTextBoxColumn.Visible = false;

            AddTotalRow(gridData);

            this.ProductList = gridData;

            if (this.category.Type != CategoryType.Normal)
            {
                this.attribDataGridViewTextBoxColumn.ReadOnly = true;
                this.attribDataGridViewTextBoxColumn.DataSource = EnumHelper.ToList(typeof(ProductAttrib));

                this.excludeDataGridViewCheckBoxColumn.ReadOnly = true;
            }
            else
            {
                this.attribDataGridViewTextBoxColumn.ReadOnly = false;
                ArrayList list = new ArrayList();
                list.Add(new KeyValuePair<Enum, string>(ProductAttrib.InBalance,
                                                        EnumHelper.GetDescription(ProductAttrib.InBalance)));
                list.Add(new KeyValuePair<Enum, string>(ProductAttrib.InFuture,
                                                        EnumHelper.GetDescription(ProductAttrib.InFuture)));
                list.Add(new KeyValuePair<Enum, string>(ProductAttrib.Notional,
                                                        EnumHelper.GetDescription(ProductAttrib.Notional)));
                this.attribDataGridViewTextBoxColumn.DataSource = list;

                this.excludeDataGridViewCheckBoxColumn.ReadOnly = false;
            }

            if (this.category.Type == CategoryType.Treasury || this.category.Type == CategoryType.CashAdj ||
                this.category.Type == CategoryType.PositiveDerivativesValuation ||
                this.category.Type == CategoryType.NegativeDerivativesValuation)
            {
                this.grid.AllowUserToDeleteRows = false;
            }
            else
            {
                this.grid.AllowUserToDeleteRows = true;
            }

            if (this.category.Type == CategoryType.Roll)
            {
                this.origFaceColumn.ReadOnly = true;
                this.origFaceColumn.DefaultCellStyle.BackColor = Color.LightGray;
            }
            else
            {
                this.origFaceColumn.ReadOnly = false;
                this.origFaceColumn.DefaultCellStyle.BackColor = Color.White;
            }


            this.RefreshRollBonds();

            this.SetEnabledStateByFinancialType();

            this.ChangeLiquidityRunOffColumnName();

            stopwatch.Stop();

            if (this.grid.Columns[0].AutoSizeMode != DataGridViewAutoSizeColumnMode.None)
            {
                for (int i = 0; i < this.grid.Columns.Count; i++)
                {
                    int colWidth = this.grid.Columns[i].Width;
                    if (i == this.bookValueDataGridViewTextBoxColumn.Index) continue;
                    this.grid.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                    this.grid.Columns[i].Width = colWidth;
                }
            }

            Debug.WriteLine("DetailsGridController.Refresh took " + stopwatch.ElapsedMilliseconds + "ms");

            return totalProductCount;
        }

        private void FixProductRoll(Product product)
        {
            if (string.IsNullOrEmpty(product.RollSpec) || !product.HasRoll) return;
            if (product.RollSpec.Length <= 1) return;

            string rollSpecString = product.RollSpec.Substring(1);
            int rollSpecInt;
            if (int.TryParse(rollSpecString, out rollSpecInt))
            {
                product.RollSpec = Product.GetAlmidDisplayed("R", rollSpecInt);
            }
        }

        private void ChangeLiquidityRunOffColumnName()
        {
            if (this.category == null) return;

            MainForm.ModifiedValuesSuspended = true;
            this.liquidityRunOffColumn.HeaderText = this.category.BalanceType == BalanceType.Asset
                                                        ? Labels.
                                                              DetailsGridController_ChangeLiquidityRunOffColumnName_Liquidity_inflow
                                                        : Labels.
                                                              DetailsGridController_ChangeLiquidityRunOffColumnName_Liquidity_outflow;
            MainForm.ModifiedValuesSuspended = false;
        }

        private void RefreshRollBonds()
        {
            Product[] rollProducts = null;
            if (this.category != null)
            {
                rollProducts = Product.GetBondsByTypeAndAtrib(this.session, this.category.BalanceType, ProductAttrib.Roll);
            }
            List<string> roll = new List<string>();
            roll.Add(Product.NoRoll);
            roll.Add(CreateNew);

            if (rollProducts != null)
            {
                foreach (Product bond in rollProducts)
                {
                    roll.Add(bond.ALMIDDisplayed);
                }
            }

            this.rollSpecDataGridViewComboBoxColumn.DataSource = roll;
        }

        private void SetEnabledStateByFinancialType()
        {
            foreach (DataGridViewRow row in this.grid.Rows)
            {
                this.SetEnabledStateByFinancialType(row);
            }
        }
        private void SetEnabledStateByFinancialType(DataGridViewRow row)
        {
            Product product = row.DataBoundItem as Product;
            bool isDerivative = product != null && product.IsDerivative;
            bool isVariable = product != null && product.IsVariableRate;

            this.EnableCell(row.Index, this.bondSwapLegDataGridViewComboBoxColumn.Name, isDerivative);
            this.EnableCell(row.Index, this.spreadDataGridViewTextBoxColumn.Name, isVariable);
            this.EnableCell(row.Index, this.firstRateDataGridViewTextBoxColumn.Name,
                            product != null && product.IsVariableRate);

            //EnableCell(row.Index, marketBasisDataGridViewTextBoxColumn.Name, isTV ||
            //    (isSwap && (Product.SwapLeg == BondSwapLeg.PayFloat || Product.SwapLeg == BondSwapLeg.ReceivedFloat)));
        }

        public void AddNewBonds(int bondCount, ProductType productType)
        {
            this.AddNewBonds(bondCount, productType, null, null, null);
        }

        public void AddNewBonds(int bondCount, ProductType productType, Func<Product> initBond1, Func<Product> initBond2, Func<Product> initBond3)
        {
            LoadForm.Instance.DoWork(
                new WorkParameters(bw => this.AddBonds(bw, bondCount, productType, initBond1, initBond2, initBond3),
                                   this.AddBondsCompleted));
        }

        private void AddBonds(BackgroundWorker backgroundWorker, int bondCount, ProductType productType,
                              Func<Product> initBond1, Func<Product> initBond2, Func<Product> initBond3)
        {
            int almid = Product.GetMaxAlmIdForCategory(this.session, this.category) + 1;
            int position = Product.GetMaxPosition(session, this.category);
            BalanceSheetOperation bso = new AddLinesBalanceSheetOperation(category, CurrentPage, position,
                                                                          new List<Product>(), new List<Product>());

            List<Product> newProd;
            for (int i = 0; i < bondCount; i++)
            {
                newProd=this.AddBond(productType, initBond1, initBond2, initBond3, ref almid, ref position);
                
                bso.UndoProducts.AddRange(newProd);
                bso.RedoProducts.AddRange(newProd);
                backgroundWorker.ReportProgress(90 * i / bondCount);
            }
            this.FlushChanges(bso);
        }

        private void AddBondsCompleted(Exception exception)
        {
            this.Refresh(this.category.Id, false);
        }

        private List<Product> AddBond(ProductType productType, Func<Product> initBond1, Func<Product> initBond2, Func<Product> initBond3, ref int almId, ref int position)
        {
            List<Product> result = new List<Product>(3);
            Product swaption = null;
            Product product2 = null;

            if (productType == ProductType.Swaption)
            {
                swaption = initBond3();
                swaption.Category = this.category;
                swaption.Position = ++position;
                swaption.ALMID = almId++;
                swaption.BookPriceValue = ParsingTools.StringToDouble(swaption.BookPrice).GetValueOrDefault();
                swaption.ComputeBookValue(swaption.BookPriceValue);

                swaption.Create(session);
                result.Add(swaption);
            }

            Product product1 = initBond1();

            product1.Category = this.category;
            product1.Position = ++position;
            product1.ALMID = almId++;

            if (this.category.Type == CategoryType.Roll)
            {
                product1.Attrib = ProductAttrib.Roll;
                product1.InvestmentRule = ProductInvestmentRule.Roll;
            }

            product1.BookPriceValue = ParsingTools.StringToDouble(product1.BookPrice).GetValueOrDefault();
            product1.ComputeBookValue(product1.BookPriceValue);
            product1.Create(session);
            result.Add(product1);

            if (product1.ProductType == ProductType.Swap || product1.ProductType == ProductType.Cap || product1.ProductType == ProductType.Floor)
            {
                product2 = initBond2();
                product2.Position = ++position;
                product2.OtherSwapLeg = product1;
                product2.ALMID = almId++;
                product2.Category = product1.Category;

                product2.BookPriceValue = ParsingTools.StringToDouble(product2.BookPrice).GetValueOrDefault();
                product2.ComputeBookValue(product2.BookPriceValue);

                product2.OtherSwapLeg = product1;
                product1.OtherSwapLeg = product2;

                product2.Create(session);
                result.Add(product2);
            }

            if (productType == ProductType.Swaption)
            {
                Debug.Assert(swaption != null);
                Debug.Assert(product2 != null);

                swaption.LinkedProducts.Add(product1);
                swaption.LinkedProducts.Add(product2);

                product1.LinkedProducts.Add(swaption);
                product1.LinkedProducts.Add(product2);

                product2.LinkedProducts.Add(swaption);
                product2.LinkedProducts.Add(product1);
            }

            return result; //swaption ?? product1;
        }

        private Product CreateSwapBondLink()
        {
            Product product = new Product
                              {
                                  ProductType = ProductType.Swap,
                                  Category = this.category,
                                  Currency = Param.GetValuationCurrency(this.session),
                                  Duration = Param.GetDefaultBondDuration(this.session),
                                  Attrib =
                                          (this.category.Type == CategoryType.Roll)
                                              ? ProductAttrib.Roll
                                              : ProductAttrib.InBalance,
                                  SwapLeg = BondSwapLeg.Paid,
                                  CouponType = CouponType.Floating
                              };

            product.Create(this.session);

            return product;
        }

        private void CopyBondsToClipboard()
        {
            List<Product> bondsToCopy = new List<Product>();

            DataGridViewRow[] selectedRows = new DataGridViewRow[this.grid.SelectedRows.Count];
            this.grid.SelectedRows.CopyTo(selectedRows, 0);
            selectedRows = selectedRows.OrderBy(r => r.Index).ToArray();

            foreach (DataGridViewRow row in selectedRows)
            {
                Product product = this.GetBondAtRowIndex(row.Index);
                if (product == null || product.IsTotal) continue;

                if (product.LinkedProducts.Count > 0)
                {
                    product = product.LinkedProducts.FirstOrDefault(p => p.ProductType == ProductType.Swaption) ?? product;
                }

                if (product.ProductType == ProductType.Swap || product.ProductType == ProductType.Cap || product.ProductType == ProductType.Floor)
                {
                    if (product.SwapLeg == BondSwapLeg.Paid)
                    {
                        product = product.OtherSwapLeg;
                    }
                }

                if (!bondsToCopy.Contains(product))
                {
                    bondsToCopy.Add(product);
                }
            }

            BinaryFormatter bf = new BinaryFormatter();
            MemoryStream ms = new MemoryStream();
            bf.Serialize(ms, bondsToCopy);

            DataFormats.Format serializedObject = DataFormats.GetFormat(typeof(MemoryStream).FullName);

            IDataObject dataObj = new DataObject();
            dataObj.SetData(serializedObject.Name, false, ms);
            Clipboard.SetDataObject(dataObj, false);
        }

        private void CopyCellsToClipboard()
        {
            if (this.grid.IsCurrentCellInEditMode)
            {
                TextBox textBox = this.grid.EditingControl as TextBox;
                if (textBox != null)
                {
                    string selectedText = textBox.SelectedText;
                    selectedText = ParsingTools.ClipboardFormatString(selectedText, ParsingTools.GetWindowsCulture());
                    if (!string.IsNullOrEmpty(selectedText)) Clipboard.SetText(selectedText);
                    return;
                }

            }

            grid.CopyToClipboardAsExcel();
        }

        public void ClipboardCopy(bool cut)
        {
            this.isClipboardCut = cut;

            this.grid.CellFormatting -= this.GridCellFormatting;

            if (this.grid.SelectedRows.Count > 0)
            {
                this.CopyBondsToClipboard();
            }
            else
            {
                this.CopyCellsToClipboard();
            }

            this.grid.CellFormatting += this.GridCellFormatting;
        }

        private List<Product> GetBondsFromClipboard()
        {
            List<Product> bonds = null;
            BinaryFormatter bf = new BinaryFormatter();
            IDataObject dataObj = Clipboard.GetDataObject();
            if (dataObj != null && dataObj.GetDataPresent(typeof(MemoryStream).FullName))
            {
                MemoryStream ms = dataObj.GetData(typeof(MemoryStream).FullName) as MemoryStream;
                if (ms != null)
                {
                    bonds = bf.Deserialize(ms) as List<Product>;
                }
            }

            return bonds;
        }

        private void PasteBonds(IEnumerable<Product> bonds)
        {
            BalanceSheetOperation bso=null;
            int line = (this.grid.CurrentRow == null ? 0 : this.grid.CurrentRow.Index);
            if (isClipboardCut)
            {
                foreach (Product product in bonds)
                {
                    if (product.Category.Type == CategoryType.Roll && this.category.Type != CategoryType.Roll)
                    {
                        Dialogs.Error(Constants.APPLICATION_NAME, "Can not paste a Roll in this category");
                        return;
                    }
                    product.Category = this.category;
                    session.Merge(product);//todo MTU : change field category
                }
            }
            else
            {
                List<Product> newProd;
                bso = new AddLinesBalanceSheetOperation(category, CurrentPage, line, 
                                                  new List<Product>(), new List<Product>());
                int almid = Product.GetMaxAlmIdForCategory(this.session, this.category) + 1;
                int position = Product.GetMaxPosition(session, this.category);
                foreach (Product product in bonds)
                {
                    Func<Product> initBond1 = () => this.CopyProduct(product);
                    Func<Product> initBond2 = () => new Product();
                    Func<Product> initBond3 = () => new Product();

                    if (product != null)
                    {
                        if (product.ProductType == ProductType.Swaption)
                        {
                            Debug.Assert(product.LinkedProducts != null);
                            Debug.Assert(product.LinkedProducts.Count == 2);

                            Product[] linkedProducts = product.LinkedProducts.ToArray();
                            initBond3 = initBond1;
                            initBond1 = () => this.CopyProduct(linkedProducts[0]);
                            initBond2 = () => this.CopyProduct(linkedProducts[1]);
                        }

                        if (product.ProductType == ProductType.Swap || product.ProductType == ProductType.Floor ||
                            product.ProductType == ProductType.Cap)
                        {
                            initBond2 = () => this.CopyProduct(product.OtherSwapLeg);
                        }

                        newProd=this.AddBond(product.ProductType, initBond1, initBond2, initBond3, ref almid, ref position);
                        bso.UndoProducts.AddRange(newProd);
                        bso.RedoProducts.AddRange(newProd);
                    }
                }
            }

            this.FlushChanges(bso);

            MainForm.ModifiedValues = true;

            this.Refresh(this.category.Id, false);
        }

        private Product CopyProduct(Product from)
        {
            if (from == null) return null;

            Product to = new Product();// from.ClonePropertiesOnly(true);

            to.ProductType = from.ProductType;
            to.Exclude = from.Exclude;
            to.Status = from.Status;
            to.SecId = from.SecId;
            to.Description = from.Description;
            to.AmortizationType = from.AmortizationType;
            to.InvestmentRule = from.InvestmentRule;
            to.InvestmentParameter = from.InvestmentParameter;
            to.OrigFace = from.OrigFace;
            to.BookPrice = from.BookPrice;
            to.ActuarialPrice = from.ActuarialPrice;
            to.BookValue = from.BookValue;
            to.StartDate = from.StartDate;
            to.FirstRate = from.FirstRate;
            to.RollSpec = from.RollSpec;
            to.Coupon = from.Coupon;
            to.CouponType = from.CouponType;
            to.CouponBasis = from.CouponBasis;
            to.Attrib = from.Attrib;
            to.Tax = from.Tax;
            to.Accounting = from.Accounting;
            to.Frequency = from.Frequency;
            to.Duration = from.Duration;
            to.IsModel = from.IsModel;
            to.TheoreticalCurrFace = from.TheoreticalCurrFace;
            to.CPR = from.CPR;
            to.CDR = from.CDR;
            to.LGD = from.LGD;
            to.Basel2 = from.Basel2;
            to.Currency = from.Currency;
            to.IRC = from.IRC;
            to.RWA = from.RWA;
            to.LiquidityEligibility = from.LiquidityEligibility;
            to.LiquidityHaircut = from.LiquidityHaircut;
            to.LiquidityRunOff = from.LiquidityRunOff;
            to.SwapLeg = from.SwapLeg;
            to.Spread = from.Spread;
            to.MarketBasis = from.MarketBasis;
            to.RecoveryLag = from.RecoveryLag;
            to.ApplicationCharge = from.ApplicationCharge;
            to.ApplicationFee = from.ApplicationFee;
            to.PrePayementFee = from.PrePayementFee;

            return to;
        }

        private void PasteTextFromClipboard()
        {
            IDataObject dataObject = Clipboard.GetDataObject();
            if (dataObject != null)
            {
                string[] formats = dataObject.GetFormats();
                foreach (string format in formats)
                {
                    Debug.WriteLine(format);
                    object data = dataObject.GetData(format);

                    string s = data as string;
                    if (s != null)
                    {
                        Debug.WriteLine("string : " + s);
                    }

                    MemoryStream memoryStream = data as MemoryStream;
                    if (memoryStream != null)
                    {
                        byte[] bytes = memoryStream.ToArray();
                        Debug.WriteLine("MemoryStream : " + bytes.Length);
                        string s1 = Encoding.Default.GetString(bytes);
                        Debug.WriteLine("Decoded : " + s1);
                    }
                    Debug.WriteLine("----------------------------------------\n");
                }
            }

            int startRowIndex;
            int startColIndex;
            if (this.grid.SelectedCells.Count > 0)
            {
                startRowIndex = this.grid.SelectedCells.Cast<DataGridViewCell>().Min(c => c.RowIndex);
                startColIndex = this.grid.SelectedCells.Cast<DataGridViewCell>().Min(c => c.ColumnIndex);
            }
            else
            {
                if (this.grid.CurrentCell == null) return;

                startRowIndex = this.grid.CurrentCell.RowIndex;
                startColIndex = this.grid.CurrentCell.ColumnIndex;
            }
            this.PasteTextFromClipboardAt(startColIndex, startRowIndex);
        }
        public void PasteColumn()
        {
            if (this.menuColumn < 0) return;
            this.PasteTextFromClipboardAt(this.menuColumn, 0);
        }
        private void PasteTextFromClipboardAt(int startColIndex, int startRowIndex)
        {
            string clipboardTxt = Clipboard.GetText();
            if (string.IsNullOrEmpty(clipboardTxt))
            {
                return;
            }
            else if (false && this.grid.EditingControl is TextBox) // todo cdu
            {
                int index = ((TextBox)this.grid.EditingControl).SelectionStart;
                if (index < 0) return;
                string text = this.grid.EditingControl.Text.Remove(index,
                                                                   ((TextBox)this.grid.EditingControl).SelectionLength);
                this.grid.EditingControl.Text = text.Insert(index, Clipboard.GetText());
                ((TextBox)this.grid.EditingControl).Select(index + Clipboard.GetText().Length, 0);
                return;
            }

            string[] clipboardRows = clipboardTxt.Split(new[] { Environment.NewLine },
                                                        StringSplitOptions.RemoveEmptyEntries);
            if (clipboardRows.Length == 0)
            {
                return;
            }

            if (this.grid.CurrentCell == null)
            {
                return;
            }

            List<string[]> clipboardCells = new List<string[]>();

            for (int i = 0; i < clipboardRows.Length; i++)
            {
                string[] row = clipboardRows[i].Split(Constants.VALUE_SEPARATOR);
                clipboardCells.Add(row);
            }

            this.grid.SuspendLayout();
            for (int i = 0; i < clipboardCells.Count; i++)
            {
                int rowNumber = startRowIndex + i;
                if (rowNumber >= this.grid.Rows.Count - 1)
                    break;

                for (int j = 0; j < clipboardCells[i].Length; j++)
                {
                    int colNumber = startColIndex + j;
                    if (colNumber >= this.grid.Columns.Count)
                        break;

                    if (this.grid[colNumber, rowNumber].ReadOnly || this.grid.Columns[colNumber].Name == "ALMID" ||
                        this.grid.Columns[colNumber].Name == "Status")
                        continue;

                    Type colType = this.grid.Columns[colNumber].ValueType;
                    string clipboardValue = clipboardCells[i][j];
                    this.grid[colNumber, rowNumber].Value = ClipboadToColumn(this.session, colType, clipboardValue);
                    this.grid.RefreshEdit();
                }
            }
            this.grid.ResumeLayout();
            this.grid.EndEdit();
        }

        public void ClipboadPaste()
        {
            List<Product> bonds = this.GetBondsFromClipboard();

            if (bonds != null)
            {
                this.PasteBonds(bonds);
            }
            else
            {
                this.PasteTextFromClipboard();
            }
        }

        private static object ClipboadToColumn(ISession session, Type colType, string clipboardValue)
        {
            object colValue = null;

            if (colType.IsEnum)
            {
                foreach (KeyValuePair<Enum, string> pair in EnumHelper.ToList(colType))
                {
                    if (clipboardValue == pair.Value)
                    {
                        colValue = pair.Key;
                        break;
                    }
                }
                // colValue = Enum.Parse(colType, clipboardValue, true);
            }
            else if (colType == typeof(Currency))
            {
                colValue = Currency.FindFirstBySymbol(session, clipboardValue);
            }
            //else if (colType == typeof (double?))
            //{
            //    colValue = Tools.StringToDouble(clipboardValue);
            //}
            else
            {
                double? doubleValue = ParsingTools.ClipboardStringToDouble(clipboardValue);

                colValue = doubleValue.HasValue ? (object)doubleValue.Value : clipboardValue;
            }

            return colValue;
        }

        public bool IsColumnCopyAble(int columnIndex)
        {
            return this.copyAbleColumns.Contains(this.grid.Columns[columnIndex]);
        }

        public void CurrenciesChanged()
        {
            this.Refresh(this.category == null ? (int?)null : this.category.Id, true);
            //if (grid.Rows.Count > 1)
            //{

            //    grid["Description", grid.Rows.Count - 1].Value = GetTotalRowDescription();
            //}

            //ComputeGridTotal();
        }

        public void SelectCellFor(Product product, string property)
        {
            foreach (DataGridViewRow row in this.grid.Rows)
            {
                Product rowProduct = (Product)row.DataBoundItem;
                if (rowProduct.Id == product.Id)
                {
                    this.grid.ClearSelection();
                    this.grid.FirstDisplayedScrollingRowIndex = row.Index;

                    this.grid.ClearSelection();

                    if (property == null || !this.grid.Columns.Contains(property))
                    {
                        row.Selected = true;
                    }
                    else
                    {
                        DataGridViewCell cell = this.grid[property, row.Index];

                        if (cell != null)
                        {
                            cell.Selected = true;
                        }
                    }

                    return;
                }
            }
        }

        public bool IsModelRow(int rowIndex)
        {
            Product product = this.GetBondAtRowIndex(rowIndex);
            return product != null && product.IsModel;
        }

        public Product GetProductAtIndex(int rowIndex)
        {
            return this.GetBondAtRowIndex(rowIndex);
        }

        public void EndEdit()
        {
            this.grid.EndEdit();
            this.FlushChanges(null);
        }

        public void ExcludeAbove(bool exclude)
        {
            DataGridViewCell cell = this.grid.SelectedCells.Count > 0
                                        ? this.grid.SelectedCells[0]
                                        : this.grid.CurrentCell;
            int rowIndex = cell.RowIndex;
            for (int i = 0; i <= rowIndex; i++)
            {
                this.ExcludeIndex(exclude, i);
            }
        }
        public void ExcludeBelow(bool exclude)
        {
            DataGridViewCell cell = this.grid.SelectedCells.Count > 0
                                        ? this.grid.SelectedCells[0]
                                        : this.grid.CurrentCell;
            int rowIndex = cell.RowIndex;
            for (int i = rowIndex; i < this.grid.RowCount; i++)
            {
                this.ExcludeIndex(exclude, i);
            }
        }
        private void ExcludeIndex(bool exclude, int rowIndex)
        {
            Product product = this.GetProductAtIndex(rowIndex);
            if (product == null) return;
            product.Exclude = exclude;
            this.ResetRow(rowIndex);
        }

        public void RecalculateBookValue(int rowIndex)
        {
            this.RecalculateBookPriceForCurentRow();
        }

        public void FlushChanges(BalanceSheetOperation bso)
        {
            if (this.session != null)
            {
                try
                {
                    this.session.Flush();
                }
                catch { }
                if (bso != null)
                {
                    ActionStack.GetInstance().Do(bso);
                }
            }
        }

        public bool CanLinkSelectedLinesAsSwap()
        {
            DataGridViewSelectedRowCollection rows = grid.SelectedRows;
            if (rows.Count != 2) return false;

            Product product1 = rows[0].DataBoundItem as Product;
            Product product2 = rows[1].DataBoundItem as Product;

            if (product1 == null || product2 == null) return false;
            bool isSwap = product1.ProductType == ProductType.Swap && product2.ProductType == ProductType.Swap;
            bool isCap = product1.ProductType == ProductType.Cap && product2.ProductType == ProductType.Cap;
            bool isFloor = product1.ProductType == ProductType.Floor && product2.ProductType == ProductType.Floor;
            return (isSwap || isCap || isFloor) && (product1.OtherSwapLeg != product2 || product2.OtherSwapLeg != product1);
        }

        public void LinkSelectedProductsAsSwap()
        {
            DataGridViewSelectedRowCollection rows = grid.SelectedRows;
            if (rows.Count != 2) return;

            Product product1 = rows[0].DataBoundItem as Product;
            Product product2 = rows[1].DataBoundItem as Product;

            if (product1 == null || product2 == null) return;

            product1.OtherSwapLeg = product2;
            product2.OtherSwapLeg = product1;

            product2.Exclude = product1.Exclude;
            product2.Accounting = product1.Accounting;
            product2.Duration = product1.Duration;
            product2.IsModel = product1.IsModel;

        }


        private int FindRowIndexByALMID(string productALMID)
        {
            foreach (DataGridViewRow row in this.grid.Rows)
            {
                string data = (string)row.Cells[this.almIdDataGridViewTextBoxColumn.Index].Value;
                if (data == productALMID)
                    return row.Index;
            }

            return -1;
        }

        private int GetLastDayofMonthYear(int month, int year)
        {
            switch (month)
            {
                case 1:     //January
                case 3:     //March
                case 5:     //May
                case 7:     //July
                case 8:     //August
                case 10:	//October
                case 12:	//December
                    return 31;
                case 4:     //April
                case 6:     //June
                case 9:     //September
                case 11:    //November
                    return 30;
                case 2:     //February
                    if (year % 4 == 0 && year % 100 != 0 || year % 400 == 0) //Leap Year
                        return 29;
                    else
                        return 28;
                default:
                    return 0;
            }
        }

        private DialogResult DateValidationMessageBox(string columnName, DateTime newDate)
        {
            DateTime lastDay = Tools.ResetDateToLastOfMonth(newDate);

            //if last date, return "NO" 
            if (newDate == lastDay) return DialogResult.No;

            MessageBoxManager.Yes = "Yes";
            MessageBoxManager.No = "No";
            MessageBoxManager.Cancel = "Cancel";
            MessageBoxManager.Register();

            DialogResult res = MessageBox.Show(new Form() { TopMost = true }, string.Format(Properties.Resources.NotLastDay,
                                               newDate.ToString("dd/MM/yyyy"), newDate.Date.ToString("MMMM"),
                                               lastDay.ToString("dd/MM/yyyy")), "Date Validation - " + columnName,
                                               MessageBoxButtons.YesNoCancel,
                                               MessageBoxIcon.Warning,
                                               MessageBoxDefaultButton.Button1);

            MessageBoxManager.Unregister();
            return res;
        }
    }
}