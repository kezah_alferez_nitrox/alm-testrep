﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using ALMS.Products;
using InsuranceBondManager.BusinessLogic;
using InsuranceBondManager.Calculations;
using InsuranceBondManager.Core;
using InsuranceBondManager.Domain;
using InsuranceBondManager.Resources.Language;
using Eu.AlterSystems.ASNetLib.Core;

namespace InsuranceBondManager.Views.BalanceSheet.Balance
{
    public partial class AddProductsDialog : SessionForm
    {
        private readonly string[] allVariablesAndVectorNames;
        private readonly Currency[] currencies;

        private readonly Dictionary<Product, NumericUpDown> firstRateControls = new Dictionary<Product, NumericUpDown>();
        private readonly FundamentalVector[] fundamentalVectors;
        private readonly Product leftProduct;
        private readonly int panelWidth;
        private int indexColumn;
        private int indexRow;
        private Product rightProduct;
        private Product swaptionProduct;
        private DateTimePicker optionDatePicker;
        private DateTime valuationDate;

        public AddProductsDialog()
        {
            this.leftProduct = new Product();
            this.allVariablesAndVectorNames = DomainTools.GetAllVariablesAndVectorNames(this.Session);
            this.fundamentalVectors = FundamentalVector.FindAll(this.Session);
            this.currencies = Currency.FindAllNotEmpty(this.Session);

            this.InitializeComponent();
            this.panelWidth = this.panel.Width;
        }

        public int RowCount
        {
            get { return (int)this.rowCountNumericUpDown.Value; }
        }

        public ProductType ProductType
        {
            get { return (ProductType)this.comboBoxBondFinancialType.SelectedValue; }
        }

        public Product GetLeftProduct()
        {
            Product product = this.leftProduct.ClonePropertiesOnly();
            product.Currency = this.leftProduct.Currency;
            return product;
        }

        public Product GetRightProduct()
        {
            Product product = this.rightProduct.ClonePropertiesOnly();
            product.Currency = this.leftProduct.Currency;
            return product;
        }

        public Product GetSwaption()
        {
            if (swaptionProduct == null) return null;
            Product product = this.swaptionProduct.ClonePropertiesOnly();
            product.Currency = this.leftProduct.Currency;
            return product;
        }

        private void FormLoad(object sender, EventArgs e)
        {
            this.rowCountNumericUpDown.Focus();
            this.rowCountNumericUpDown.Select(0, this.rowCountNumericUpDown.Text.Length);

            this.comboBoxBondFinancialType.DisplayMember = "Value";
            this.comboBoxBondFinancialType.ValueMember = "Key";
            //ProductType[] values = new[]
            //                               {
            //                                   ProductType.BondFr,
            //                                   ProductType.BondVr,
            //                                   ProductType.Swap,
            //                                   ProductType.Swaption,
            //                                   ProductType.Cap,
            //                                   ProductType.Floor,
            //                                   ProductType.StockEquity
            //                               };
            //this.comboBoxBondFinancialType.DataSource = values.ToDictionary(p => p,
            //    p => typeof(ProductType).GetField(Enum.GetName(typeof(ProductType), p)).
            //        GetCustomAttributes(false).OfType<EnumDescriptionAttribute>().First().Description).ToList();
            this.comboBoxBondFinancialType.DataSource = EnumHelper.ToList(typeof (ProductType));
            this.comboBoxBondFinancialType.SelectedIndex = 0;

            this.valuationDate = Param.GetValuationDate(this.Session).GetValueOrDefault(DateTime.Today);
        }

        private void SetProductDefaultValues(ProductType type, Product product)
        {
            product.ProductType = type;

            product.Duration = type != ProductType.StockEquity ? 0 : 12 + Param.GetMaxDuration(this.Session);
            product.OrigFace = "0";
            product.Frequency = type != ProductType.BondVr ? ProductFrequency.Annual : ProductFrequency.SemiAnnual;
            product.CouponBasis = type != ProductType.BondVr && type != ProductType.Cap && type != ProductType.Floor ?
                (type != ProductType.StockEquity ? CouponBasis.A30360 : CouponBasis.ACTACT) : CouponBasis.ACT360;
            product.Coupon = type == ProductType.BondVr && this.fundamentalVectors.Length > 0 ? this.fundamentalVectors[0].Name : "0";
            product.Spread = "0";
            product.MarketBasis = "0";
            product.BookPrice = "100";
            product.SwapLeg = null;
            product.Accounting = type == ProductType.BondFr || type == ProductType.BondVr ? ProductAccounting.LAndR : ProductAccounting.AFS;

            product.AmortizationType = ProductAmortizationType.BULLET;
            product.InvestmentRule = ProductInvestmentRule.Constant;
            product.RollSpec = Product.NoRoll;
            product.CouponType = type == ProductType.BondVr || type == ProductType.StockEquity || type == ProductType.Cap || type == ProductType.Floor ?
                CouponType.Floating : CouponType.Fixed;
            product.Tax = ProductTax.Standard;
            product.Attrib = ProductAttrib.InBalance;
            product.Basel2 = "0";
            product.LiquidityEligibility = LiquidityEligibility.NotEligible;
            product.LiquidityHaircut = 0;
            product.LiquidityRunOff = 0;
            product.FirstRate = null;
            product.Currency = Param.GetValuationCurrency(this.Session);
        }

        private void FinancialTypeChanged(object sender, EventArgs e)
        {
            this.SuspendLayout();
            this.GenerateForm(this.ProductType);
            this.ResumeLayout(true);
            this.panel.Height = this.panel.Controls.Cast<Control>().Max(c => c.Bottom) + 10;
            this.Size = new Size(this.panel.Right + this.panel.Left * 2, this.panel.Bottom + 80);
            this.panel.Width += 5;
        }

        private void GenerateForm(ProductType type)
        {
            this.Controls.Remove(this.panel);
            this.panel.Controls.Clear();
            this.indexRow = 0;
            this.indexColumn = 0;
            this.panel.Height = 0;

            if (type == ProductType.Swap || type == ProductType.Swaption) this.panel.Width = this.panelWidth * 2;
            else this.panel.Width = this.panelWidth;

            this.SetProductDefaultValues(type, this.leftProduct);
            if (type == ProductType.Swap || type == ProductType.Swaption)
            {
                this.rightProduct = new Product();
                this.SetProductDefaultValues(type, this.rightProduct);
                this.leftProduct.SwapLeg = BondSwapLeg.Received;
                this.rightProduct.SwapLeg = BondSwapLeg.Paid;
                this.rightProduct.CouponType = CouponType.Floating;
                this.rightProduct.CouponBasis = CouponBasis.ACT360;
            }
            else this.rightProduct = null;

            if (type == ProductType.Swaption)
            {
                this.swaptionProduct = new Product();
                this.SetProductDefaultValues(type, this.swaptionProduct);
                this.swaptionProduct.CouponType = CouponType.Floating;
                this.swaptionProduct.CouponBasis = CouponBasis.ACT360;
            }
            else this.swaptionProduct = null;

            if (type == ProductType.Swaption)
            {
                this.AddOptionDate();
                this.AddNominal(this.swaptionProduct);
                this.AddAccounting(this.swaptionProduct);
                this.AddBookPrice(this.swaptionProduct);
                this.AddSeparator("Underlying Swap");
            }

            if (type == ProductType.StockEquity)
            {
                this.AddNumberStock(this.leftProduct);
            }
            else
            {
                if (type != ProductType.Swaption) this.AddNominal(this.leftProduct);
            }
            this.AddCurrency(this.leftProduct);

            switch (type)
            {
                case ProductType.Swap:
                case ProductType.Swaption:

                    this.AddDuration(this.leftProduct);

                    this.AddSeparator(Labels.AddBondsDialog_GenerateForm_Leg_1__Fixed_rate_);
                    this.AddReceivedPaid(this.leftProduct, true);
                    this.AddCoupon(this.leftProduct);
                    this.AddFrequency(this.leftProduct);
                    this.AddDateConvention(this.leftProduct);

                    this.indexRow = this.panel.Controls[Labels.AddBondsDialog_GenerateForm_Leg_1__Fixed_rate_].Top;
                    this.indexColumn = 2;

                    this.AddSeparator(Labels.AddBondsDialog_GenerateForm_Leg_2__Variable_rate_);
                    this.AddReceivedPaid(this.rightProduct, false);
                    this.AddIndex(this.rightProduct);
                    this.AddFrequency(this.rightProduct);
                    this.AddDateConvention(this.rightProduct);
                    this.AddSpread(this.rightProduct);
                    this.AddFirstRate(this.rightProduct);

                    this.indexColumn = 0;
                    break;

                case ProductType.Floor:
                case ProductType.Cap:
                    this.leftProduct.SwapLeg = BondSwapLeg.Received;

                    this.AddSeparator();
                    this.AddReceivedPaid(this.leftProduct, true);
                    this.AddIndex(this.leftProduct);
                    this.AddStrike(this.leftProduct);
                    this.AddDuration(this.leftProduct);
                    this.AddFrequency(this.leftProduct);
                    this.AddDateConvention(this.leftProduct);
                    this.AddFirstRate(this.leftProduct);
                    break;

                case ProductType.BondFr:
                case ProductType.BondVr:

                    this.AddSeparator();
                    if (type == ProductType.BondVr)
                    {
                        this.AddIndex(this.leftProduct);
                    }
                    else
                    {
                        this.AddCoupon(this.leftProduct);
                    }
                    this.AddDuration(this.leftProduct);
                    this.AddFrequency(this.leftProduct);
                    this.AddDateConvention(this.leftProduct);
                    if (type == ProductType.BondVr) this.AddSpread(this.leftProduct);
                    this.AddMarketBasis(this.leftProduct);
                    if (type == ProductType.BondVr)
                    {
                        this.AddFirstRate(this.leftProduct);
                    }
                    break;

                case ProductType.StockEquity:

                    this.AddStockPrice(this.leftProduct);
                    this.AddDividend(this.leftProduct);
                    break;
            }

            if (type != ProductType.Swaption)
            {
                this.AddSeparator();
                this.AddAccounting(this.leftProduct);
                if (type != ProductType.StockEquity) this.AddBookPrice(this.leftProduct);
            }

            if (this.panel.Controls["Index"] is ComboBox && ((ComboBox)this.panel.Controls["Index"]).Items.Count > 0)
            {
                List<FundamentalVector> vectors = ((ComboBox)this.panel.Controls["Index"]).DataSource as List<FundamentalVector>;
                if (vectors != null && vectors.Count > 0)
                {
                    ((ComboBox)this.panel.Controls["Index"]).SelectedIndex = 0;
                    this.leftProduct.Frequency = vectors[0].Frequency;
                }
            }

            this.Controls.Add(this.panel);
        }

        private void Validate(object sender, FormClosingEventArgs e)
        {
            this.Validate(true);
            this.ValidateProduct(this.leftProduct);

            if (swaptionProduct != null)
            {                
                this.swaptionProduct.Currency = this.leftProduct.Currency;
                //this.swaptionProduct.Duration = (int)Math.Round((optionDatePicker.Value - this.valuationDate).TotalDays/30.0);
                this.swaptionProduct.Duration = PricerUtil.DateDiffInSteps(this.optionDatePicker.Value,this.valuationDate, false);

                this.leftProduct.StartDate = optionDatePicker.Value;
                this.leftProduct.Accounting = this.swaptionProduct.Accounting;
                this.leftProduct.BookPrice = "0";
                this.leftProduct.OrigFace = "0";
                this.leftProduct.ProductType = this.rightProduct.ProductType = ProductType.Swap;
            }

            if (this.leftProduct.ProductType == ProductType.Swap||
                this.leftProduct.ProductType == ProductType.Cap ||
                this.leftProduct.ProductType == ProductType.Floor)
            {
                double bookPrice;
                if (double.TryParse(this.leftProduct.BookPrice, out bookPrice))
                {
                    if (leftProduct.SwapLeg == BondSwapLeg.Paid)
                    {
                        bookPrice = -bookPrice;
                        this.leftProduct.BookPrice = bookPrice.ToString();
                    }
                    this.rightProduct.BookPrice = (-bookPrice).ToString();
                }
                this.rightProduct.OrigFace = this.leftProduct.OrigFace;
                this.rightProduct.Currency = this.leftProduct.Currency;
                this.rightProduct.Accounting = this.leftProduct.Accounting;
                this.rightProduct.Duration = this.leftProduct.Duration;
                this.rightProduct.StartDate = this.leftProduct.StartDate;
                this.rightProduct.SwapLeg = this.leftProduct.SwapLeg == BondSwapLeg.Received ? BondSwapLeg.Paid : BondSwapLeg.Received;
            }

            MainForm.ModifiedValues = true;
        }

        private void ValidateProduct(Product product)
        {
            if (product.ProductType == ProductType.Cap || product.ProductType == ProductType.Floor)
            {
                NumericUpDown numStrike = (NumericUpDown)this.panel.Controls["Strike"];
                ComboBox comboIndex = (ComboBox)this.panel.Controls["Index"];
                if (product.ProductType == ProductType.Cap) product.Coupon = "max(" + comboIndex.Text + " - " + numStrike.Value + "%, 0)";
                else product.Coupon = "max(" + numStrike.Value + "% - " + comboIndex.Text + ",0)";

                this.rightProduct = new Product();
                this.SetProductDefaultValues(this.leftProduct.ProductType, this.rightProduct);
                this.leftProduct.OtherSwapLeg = this.rightProduct;
                this.rightProduct.OtherSwapLeg = this.leftProduct;
                this.rightProduct.SwapLeg = this.leftProduct.SwapLeg == BondSwapLeg.Received ? BondSwapLeg.Paid : BondSwapLeg.Received;
            }
        }

        #region Controls creation

        private void AddRow(string label, Control control)
        {
            Label lab = new Label();
            lab.Text = label;
            lab.TextAlign = ContentAlignment.MiddleLeft;

            int xSize = (this.panelWidth - 5) / 2;

            if (string.IsNullOrEmpty(lab.Text))
            {
                this.panel.Controls.Add(control);
                control.Width = xSize * 2;
                control.Location = new Point(5 + this.indexColumn + this.panelWidth * this.indexColumn / 2, this.indexRow);
            }
            else
            {
                this.panel.Controls.Add(lab);
                this.panel.Controls.Add(control);
                control.Location = new Point(5 + (this.panelWidth / 2) + (this.panelWidth + 5) * this.indexColumn / 2, this.indexRow);
                lab.Location = new Point(5 + this.panelWidth * this.indexColumn / 2, this.indexRow);
                lab.Height = control.Height;
                control.Width = xSize - 5;
                lab.Width = xSize;
            }
            this.indexRow += control.Height + 4;
        }

        private void AddSeparator()
        {
            Label label = new Label();
            label.Height = 2;
            label.BorderStyle = BorderStyle.Fixed3D;
            label.Location = new Point(this.indexColumn / 2, this.indexRow + 2);
            label.Width = this.panelWidth;
            this.panel.Controls.Add(label);
            this.indexRow += 10;
        }

        private void AddSeparator(string text)
        {
            Label label = new Label();
            label.Text = text;
            label.Name = text;
            label.Height = 17;
            label.BorderStyle = BorderStyle.Fixed3D;
            label.Font = new Font(label.Font, FontStyle.Bold);
            label.Location = new Point(5 + this.panelWidth * this.indexColumn / 2, this.indexRow);
            label.Width = this.panelWidth - 5;

            this.panel.Controls.Add(label);
            this.indexRow += label.Height + 4;
        }

        private NumericUpDown AddNumBox(string label)
        {
            NumericUpDown numBox = new NumericUpDown();
            numBox.DecimalPlaces = 3;
            numBox.Minimum = decimal.MinValue;
            numBox.Maximum = decimal.MaxValue;

            this.AddRow(label, numBox);
            return numBox;
        }

        private DateTimePicker AddDateBox(string label)
        {
            DateTimePicker datePicker = new DateTimePicker();
            datePicker.ShowCheckBox = false;
            datePicker.MinDate = new DateTime(1900, 1, 1);
            datePicker.Value = this.valuationDate;
            datePicker.Format = DateTimePickerFormat.Short;
            datePicker.CustomFormat = Constants.DateFormat;
            this.AddRow(label, datePicker);
            return datePicker;
        }

        private ComboBox AddComboBox(string label)
        {
            ComboBox combo = new ComboBox();
            combo.DropDownStyle = ComboBoxStyle.DropDownList;

            this.AddRow(label, combo);
            return combo;
        }

        private ComboBox AddAutoCompleteBox(string label)
        {
            ComboBox combo = new ComboBox();
            combo.DropDownStyle = ComboBoxStyle.DropDown;
            combo.Items.AddRange(this.allVariablesAndVectorNames);

            this.AddRow(label, combo);
            return combo;
        }

        private void AddNominal(Product product)
        {
            NumericUpDown numBox = this.AddNumBox(Labels.AddBondsDialog_AddNominal_Nominal);
            numBox.DecimalPlaces = 2;
            numBox.ThousandsSeparator = true;
            numBox.DataBindings.Add("Value", product, "OrigFace", true, DataSourceUpdateMode.OnValidation, "", "G");
            this.numericUpDownNominal = numBox;
            this.numericUpDownNominal.Enter += new System.EventHandler(this.numericUpDownNominalSelectAll);
            this.numericUpDownNominal.Click += new System.EventHandler(this.numericUpDownNominalSelectAll);
        }

        private void numericUpDownNominalSelectAll(object sender, EventArgs e)
        {
            this.numericUpDownNominal.Focus();
            this.numericUpDownNominal.Select(0, this.numericUpDownNominal.Text.Length);
        }

        private void AddOptionDate()
        {
            this.optionDatePicker = this.AddDateBox("Option Date");
        }

        private void AddCurrency(Product product)
        {
            ComboBox combo = this.AddComboBox(Labels.AddBondsDialog_AddCurrency_Currency);
            combo.DataSource = this.currencies;
            combo.DataBindings.Add("SelectedItem", product, "Currency", true, DataSourceUpdateMode.OnPropertyChanged);
        }

        private void AddCoupon(Product product)
        {
            ComboBox combo = this.AddAutoCompleteBox(Labels.AddBondsDialog_AddCoupon_Coupon);
            combo.DataBindings.Add("Text", product, "Coupon");
            combo.Tag = product;
        }

        private void AddDuration(Product product)
        {
            NumericUpDown numBox = this.AddNumBox(Labels.AddBondsDialog_AddDuration_Duration__in_months_);
            numBox.Minimum = 0;
            numBox.DecimalPlaces = 0;
            numBox.DataBindings.Add("Value", product, "Duration", true, DataSourceUpdateMode.OnValidation);
        }

        private void AddFrequency(Product product)
        {
            ComboBox combo = this.AddComboBox(Labels.AddBondsDialog_AddFrequency_Frequency);
            combo.DisplayMember = "Value";
            combo.ValueMember = "Key";
            combo.DataSource = EnumHelper.ToList(typeof(ProductFrequency));
            combo.DataBindings.Add("SelectedValue", product, "Frequency");
            combo.Name = "Frequency";
        }

        private void AddDateConvention(Product product)
        {
            ComboBox combo = this.AddComboBox(Labels.AddBondsDialog_AddDateConvention_Date_convention);
            combo.DataSource = EnumHelper.ToList(typeof(CouponBasis));
            combo.DisplayMember = "Value";
            combo.ValueMember = "Key";
            combo.DataBindings.Add("SelectedValue", product, "CouponBasis");
        }

        private void AddMarketBasis(Product product)
        {
            ComboBox combo = this.AddAutoCompleteBox(Labels.AddBondsDialog_AddMarketBasis_Market_basis);
            combo.DataBindings.Add("Text", product, "MarketBasis");
        }

        private void AddAccounting(Product product)
        {
            ComboBox combo = this.AddComboBox(Labels.AddBondsDialog_AddAccounting_Accounting);
            combo.SelectedIndexChanged += this.SetBookPriceEnable;
            combo.DisplayMember = "Value";
            combo.ValueMember = "Key";
            if (product.ProductType == ProductType.StockEquity)
            {
                combo.DataSource = new ArrayList
                                       {
                                           new KeyValuePair<Enum, string>(ProductAccounting.AFS, "AFS"),
                                           new KeyValuePair<Enum, string>(ProductAccounting.HFT, "HFT")
                                       };
            }
            else
            {
                combo.DataSource = EnumHelper.ToList(typeof(ProductAccounting));
            }

            combo.DataBindings.Add("SelectedValue", product, "Accounting");
        }

        private void SetBookPriceEnable(object sender, EventArgs e)
        {
            if (!this.panel.Controls.ContainsKey("BookPrice")) return;
            NumericUpDown numBP = this.panel.Controls["BookPrice"] as NumericUpDown;
            ComboBox comboAccounting = sender as ComboBox;

            if (numBP != null && comboAccounting != null && comboAccounting.SelectedValue is ProductAccounting)
            {
                numBP.Enabled = !((ProductAccounting)comboAccounting.SelectedValue == ProductAccounting.AFS ||
                                  (ProductAccounting)comboAccounting.SelectedValue == ProductAccounting.HFT);
            }
        }

        private void AddBookPrice(Product product)
        {
            // NumericUpDown control = this.AddNumBox(Labels.AddBondsDialog_AddBookPrice_Book_price);
            // control.DataBindings.Add("Value", product, "BookPrice", true, DataSourceUpdateMode.OnValidation, "", "G");
            ComboBox control = this.AddComboBox(Labels.AddBondsDialog_AddBookPrice_Book_price);
            control.DataBindings.Add("Text", product, "BookPrice", true, DataSourceUpdateMode.OnValidation);
            control.DropDownStyle = ComboBoxStyle.DropDown;
            control.DataSource = this.allVariablesAndVectorNames;
            control.Name = "BookPrice";
        }

        private void AddStockPrice(Product product)
        {
            //NumericUpDown numBox = this.AddNumBox(Labels.AddBondsDialog_AddStockPrice_StockPrice);
            //numBox.DataBindings.Add("Value", product, "BookPrice", true, DataSourceUpdateMode.OnValidation, "", "G");
            ComboBox control = this.AddComboBox(Labels.AddBondsDialog_AddBookPrice_Book_price);
            control.DataBindings.Add("Text", product, "BookPrice", true, DataSourceUpdateMode.OnValidation);
            control.DropDownStyle = ComboBoxStyle.DropDown;
            control.DataSource = this.allVariablesAndVectorNames;
        }

        private void AddIndex(Product product)
        {
            ComboBox combo = this.AddAutoCompleteBox(Labels.AddBondsDialog_AddIndex_Index);
            combo.Name = "Index";
            combo.DropDownStyle = ComboBoxStyle.DropDownList;
            combo.DisplayMember = "Name";
            combo.ValueMember = "Name";
            combo.DataSource = this.fundamentalVectors/*.Select(fv => fv.Name)*/.ToList();
            combo.DataBindings.Add("SelectedValue", product, "Coupon");
            combo.SelectedIndexChanged += this.IndexCouponSelectedIndexChanged;
        }

        private void IndexCouponSelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox comboBox = sender as ComboBox;
            if (comboBox == null) return;

            Binding selectedValueBinding = comboBox.DataBindings["SelectedValue"];
            if (selectedValueBinding == null) return;

            Product product = selectedValueBinding.DataSource as Product;
            if (product == null) return;

            if (product.ProductType == ProductType.BondVr)
            {
                FundamentalVector vector = comboBox.SelectedItem as FundamentalVector;
                if (vector != null)
                {
                    product.Frequency = vector.Frequency;
                    foreach (
                        ComboBox ctrlFrequency in
                            this.panel.Controls.OfType<ComboBox>().Where(
                                ctrlFrequency => ctrlFrequency.Name == "Frequency"))
                    {
                        Binding freqBind = ctrlFrequency.DataBindings["SelectedValue"];
                        if (freqBind != null && freqBind.DataSource == product)
                            ctrlFrequency.SelectedValue = vector.Frequency;
                    }
                }
            }

            NumericUpDown firstRateControl;
            if (!this.firstRateControls.TryGetValue(product, out firstRateControl)) return;

            this.SetDefaultFirstRate(firstRateControl, (string)comboBox.SelectedValue);
        }

        private void SetDefaultFirstRate(NumericUpDown firstRateControl, string couponFormula)
        {
            this.Cursor = Cursors.WaitCursor;
            try
            {
                double coupon = Util.GetValueWithFormula(couponFormula,true);
                firstRateControl.Text = (coupon * 100).ToString();
                firstRateControl.Value = (decimal)(coupon * 100);
            }
            catch (Exception ex)
            {
                Log.Exception("IndexCouponSelectedIndexChanged", ex);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void AddSpread(Product product)
        {
            ComboBox combo = this.AddAutoCompleteBox(Labels.AddBondsDialog_AddSpread_Spread);
            combo.DataBindings.Add("Text", product, "Spread");
        }

        private void AddFirstRate(Product product)
        {
            NumericUpDown numBox = this.AddNumBox(Labels.AddBondsDialog_AddFirstRate_First_rate____);
            numBox.Text = "";
            numBox.Tag = product;
            numBox.ValueChanged += this.SetPercentageValue;

            this.firstRateControls[product] = numBox;

            this.SetDefaultFirstRate(numBox, product.Coupon);
        }

        private void SetPercentageValue(object sender, EventArgs e)
        {
            NumericUpDown numericUpDown = ((NumericUpDown)sender);

            Product product = (Product)numericUpDown.Tag;
            product.FirstRate = numericUpDown.Text == "" ?
                                                               null :
                                                                        (double?)numericUpDown.Value / 100.0;
        }

        private void AddReceivedPaid(Product product, bool received)
        {
            ComboBox combo = this.AddComboBox("");
            combo.Name = "ReceivedPaid";
            combo.DisplayMember = "Value";
            combo.ValueMember = "Key";
            Dictionary<BondSwapLeg, string> values = new Dictionary<BondSwapLeg, string>();
            values.Add(BondSwapLeg.Paid, Labels.AddBondsDialog_AddReceivedPaid_Paid);
            values.Add(BondSwapLeg.Received, Labels.AddBondsDialog_AddReceivedPaid_Received);
            combo.DataSource = values.ToList();
            combo.DataBindings.Add("SelectedValue", product, "SwapLeg", true, DataSourceUpdateMode.OnPropertyChanged, BondSwapLeg.Received);
            combo.SelectionChangeCommitted += this.ReceivedPaidChanged;
        }

        private void ReceivedPaidChanged(object sender, EventArgs e)
        {
            if (!this.panel.Controls.ContainsKey("BookPrice")) return;
            ComboBox comboThis = sender as ComboBox;
            if (comboThis == null) return;

            foreach (ComboBox combo in this.panel.Controls.OfType<ComboBox>().Where(c => c.Name == "ReceivedPaid"))
            {
                if (comboThis == combo) continue;
                if (combo.SelectedValue as BondSwapLeg? == comboThis.SelectedValue as BondSwapLeg?)
                    combo.SelectedValue = ((BondSwapLeg)comboThis.SelectedValue == BondSwapLeg.Paid ? BondSwapLeg.Received : BondSwapLeg.Paid);
            }
        }

        private void AddStrike(Product product)
        {
            NumericUpDown numBox = this.AddNumBox(Labels.AddBondsDialog_AddStrike_Strike____);
            numBox.Name = "Strike";
            //numBox.DataBindings.Add("Value", product, "Strike", true, DataSourceUpdateMode.OnValidation, "", "G");
        }

        private void AddNumberStock(Product product)
        {
            NumericUpDown numBox = this.AddNumBox(Labels.AddBondsDialog_AddNumberStock_Number_of_stocks);
            numBox.DecimalPlaces = 0;
            numBox.DataBindings.Add("Value", product, "OrigFace", true, DataSourceUpdateMode.OnValidation, "", "G");
        }

        private void AddDividend(Product product)
        {
            NumericUpDown numBox = this.AddNumBox(Labels.AddBondsDialog_AddDividend_Dividend);
            numBox.DataBindings.Add("Value", product, "Coupon", true, DataSourceUpdateMode.OnValidation, "", "G");
        }

        #endregion

        private System.Windows.Forms.NumericUpDown numericUpDownNominal;
    }
}