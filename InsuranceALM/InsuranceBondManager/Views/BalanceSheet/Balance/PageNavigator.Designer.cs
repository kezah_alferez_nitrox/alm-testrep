﻿namespace InsuranceBondManager.Views.BalanceSheet.Balance
{
    partial class PageNavigator
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelPages = new System.Windows.Forms.Panel();
            this.lblPageCount = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnPageLast = new System.Windows.Forms.Button();
            this.btnPageNext = new System.Windows.Forms.Button();
            this.txtPageIndex = new System.Windows.Forms.TextBox();
            this.btnPagePrev = new System.Windows.Forms.Button();
            this.btnPageFirst = new System.Windows.Forms.Button();
            this.panelPages.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelPages
            // 
            this.panelPages.Controls.Add(this.lblPageCount);
            this.panelPages.Controls.Add(this.label1);
            this.panelPages.Controls.Add(this.btnPageLast);
            this.panelPages.Controls.Add(this.btnPageNext);
            this.panelPages.Controls.Add(this.txtPageIndex);
            this.panelPages.Controls.Add(this.btnPagePrev);
            this.panelPages.Controls.Add(this.btnPageFirst);
            this.panelPages.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelPages.Location = new System.Drawing.Point(0, 0);
            this.panelPages.Name = "panelPages";
            this.panelPages.Size = new System.Drawing.Size(372, 29);
            this.panelPages.TabIndex = 3;
            // 
            // lblPageCount
            // 
            this.lblPageCount.AutoSize = true;
            this.lblPageCount.Location = new System.Drawing.Point(162, 8);
            this.lblPageCount.Name = "lblPageCount";
            this.lblPageCount.Size = new System.Drawing.Size(21, 13);
            this.lblPageCount.TabIndex = 15;
            this.lblPageCount.Text = "/ 1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(86, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 13);
            this.label1.TabIndex = 14;
            this.label1.Text = "Page";
            // 
            // btnPageLast
            // 
            this.btnPageLast.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.btnPageLast.BackColor = System.Drawing.SystemColors.Control;
            this.btnPageLast.Location = new System.Drawing.Point(231, 3);
            this.btnPageLast.Name = "btnPageLast";
            this.btnPageLast.Size = new System.Drawing.Size(36, 23);
            this.btnPageLast.TabIndex = 13;
            this.btnPageLast.Text = ">>";
            this.btnPageLast.UseVisualStyleBackColor = true;
            this.btnPageLast.Click += new System.EventHandler(this.ClickChangePage);
            // 
            // btnPageNext
            // 
            this.btnPageNext.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.btnPageNext.BackColor = System.Drawing.SystemColors.Control;
            this.btnPageNext.Location = new System.Drawing.Point(189, 3);
            this.btnPageNext.Name = "btnPageNext";
            this.btnPageNext.Size = new System.Drawing.Size(36, 23);
            this.btnPageNext.TabIndex = 12;
            this.btnPageNext.Text = ">";
            this.btnPageNext.UseVisualStyleBackColor = true;
            this.btnPageNext.Click += new System.EventHandler(this.ClickChangePage);
            // 
            // txtPageIndex
            // 
            this.txtPageIndex.Location = new System.Drawing.Point(124, 5);
            this.txtPageIndex.Name = "txtPageIndex";
            this.txtPageIndex.Size = new System.Drawing.Size(32, 20);
            this.txtPageIndex.TabIndex = 11;
            this.txtPageIndex.Text = "1";
            this.txtPageIndex.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPageIndex.Validated += new System.EventHandler(this.SetPage);
            // 
            // btnPagePrev
            // 
            this.btnPagePrev.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.btnPagePrev.BackColor = System.Drawing.SystemColors.Control;
            this.btnPagePrev.Location = new System.Drawing.Point(44, 3);
            this.btnPagePrev.Name = "btnPagePrev";
            this.btnPagePrev.Size = new System.Drawing.Size(36, 23);
            this.btnPagePrev.TabIndex = 10;
            this.btnPagePrev.Text = "<";
            this.btnPagePrev.UseVisualStyleBackColor = true;
            this.btnPagePrev.Click += new System.EventHandler(this.ClickChangePage);
            // 
            // btnPageFirst
            // 
            this.btnPageFirst.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.btnPageFirst.BackColor = System.Drawing.SystemColors.Control;
            this.btnPageFirst.Location = new System.Drawing.Point(2, 3);
            this.btnPageFirst.Name = "btnPageFirst";
            this.btnPageFirst.Size = new System.Drawing.Size(36, 23);
            this.btnPageFirst.TabIndex = 9;
            this.btnPageFirst.Text = "<<";
            this.btnPageFirst.UseVisualStyleBackColor = true;
            this.btnPageFirst.Click += new System.EventHandler(this.ClickChangePage);
            // 
            // PageNavigator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelPages);
            this.Name = "PageNavigator";
            this.Size = new System.Drawing.Size(372, 29);
            this.panelPages.ResumeLayout(false);
            this.panelPages.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelPages;
        private System.Windows.Forms.Button btnPageLast;
        private System.Windows.Forms.Button btnPageNext;
        private System.Windows.Forms.TextBox txtPageIndex;
        private System.Windows.Forms.Button btnPagePrev;
        private System.Windows.Forms.Button btnPageFirst;
        private System.Windows.Forms.Label lblPageCount;
        private System.Windows.Forms.Label label1;
    }
}
