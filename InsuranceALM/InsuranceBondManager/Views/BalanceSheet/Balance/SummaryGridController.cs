using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using ALMSCommon;
using ALMSCommon.Controls;
using InsuranceALM.Infrastructure.Data;
using InsuranceBondManager.BusinessLogic;
using InsuranceBondManager.Domain;
using InsuranceBondManager.Resources.Language;
using NHibernate;
using NHibernate.Linq;

namespace InsuranceBondManager.Views.BalanceSheet.Balance
{
    public class SummaryGridController
    {
        private const string CreateNew = "< Create New >";

        

        private readonly string[] defaultAnalysisCategories = { "Deposits", "Capitals", Category.TREASURIES_ANALYSIS_CATEGORY, "Loans" };

        private readonly DataGridViewEx grid;

        private DataGridViewTextBoxColumn descriptionDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn valueDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn openingBalanceDataGridViewTextBoxColumn;
        private DataGridViewComboBoxColumn analysisCategoryGridViewComboBoxColumn;
        private ComboBox currentEditComboBox;
        private Control editingControl;
        private ContextMenu popupMenu;

        private ISession session;

        private DataGridViewColumn[] columns;

        private Category category;
        private Currency[] currencies;

        public SummaryGridController(DataGridViewEx grid)
        {
            this.grid = grid;
            grid.ReadOnly = false;
            grid.AutoGenerateColumns = false;
            grid.AllowUserToAddRows = false;
            grid.AllowUserToDeleteRows = false;
            grid.AllowUserToResizeRows = false;
            grid.RowTemplate.Height = Constants.GRID_ROW_HEIGHT;
            grid.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            this.InitColumns();

            this.grid.EditingControlShowing += this.GridEditingControlShowing;
            this.grid.CellLeave += this.GridCellLeave;
            this.grid.CellFormatting += this.GridCellFormatting;
            this.grid.CellValueChanged += this.GridCellValueChanged;

            this.session = SessionManager.OpenSession();
        }
    

        void GridCellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
           FlushAnalysisCategoryAndReload();
            
        }

        public void EndEdit()
        {
            this.grid.EndEdit();
            
        }

        public void FlushAnalysisCategoryAndReload()
        {
            if (this.grid.CurrentCell.ColumnIndex == this.analysisCategoryGridViewComboBoxColumn.DisplayIndex)
            {
                DataGridViewRow dataGridViewRow = this.grid.CurrentRow;
                if (dataGridViewRow != null)
                {
                    SummaryModel dataBoundItem = (SummaryModel) dataGridViewRow.DataBoundItem;
                    Category currentCateg = dataBoundItem.Category;
                    RecursiveSetAnalysisCategory(currentCateg, this.grid.CurrentCell.Value.ToString());
                }

                this.session.Flush();
                this.RefreshAnalysisCategories();     
            }
        }

        private static void RecursiveSetAnalysisCategory(Category currentCateg,string analysisCategory)
        {
            currentCateg.AnalysisCategory = analysisCategory;
            if (currentCateg.Children.Count > 0)
            {
                //apply analysis category to children
                foreach (Category child in currentCateg.Children)
                {
                    RecursiveSetAnalysisCategory(child,analysisCategory);
                }
            }
        }

        void EditingCellComboBoxSelectedValueChanged(object sender, EventArgs e)
        {
            ComboBox comboBox = sender as ComboBox;
            if (comboBox != null && comboBox.SelectedValue != null && comboBox.SelectedValue is string &&
                 this.grid.CurrentRow != null && grid.CurrentCell != null &&
                 grid.CurrentCell.ColumnIndex == this.analysisCategoryGridViewComboBoxColumn.DisplayIndex)
            {
                string selectedValue = (string)comboBox.SelectedItem;
                if (selectedValue == CreateNew)
                {
                    string newAnalyseCategory = Dialogs.InputString("New Analysis Category", "New Analysis Category");
                    if (string.IsNullOrEmpty(newAnalyseCategory))
                    {
                        comboBox.Text = "";
                    }
                    else
                    {
                        BindingList<string> dataSource = (BindingList<string>)comboBox.DataSource;
                        if (!dataSource.Contains(newAnalyseCategory))
                        {
                            dataSource.Add(newAnalyseCategory);
                        }

                        comboBox.SelectedIndex = dataSource.IndexOf(newAnalyseCategory);
                    }
                }
            }
            
        }

        private void GridEditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            Point point = this.grid.PointToClient(new Point(Cursor.Position.X, Cursor.Position.Y));
            DataGridView.HitTestInfo hit = this.grid.HitTest(point.X, point.Y);

            if (e.Control is ComboBox && hit.Type == DataGridViewHitTestType.Cell)
            {
                this.editingControl = e.Control;
                this.editingControl.Enter += DetailsGridControllerEnter;

                ComboBox comboBox = (ComboBox)e.Control;
                comboBox.SelectedValueChanged += this.EditingCellComboBoxSelectedValueChanged;

                if ((e.Control as ComboBox).Items.Contains(CreateNew))
                {
                    this.currentEditComboBox = (ComboBox)e.Control;
                }
            }
        }

        private void GridCellLeave(object sender, DataGridViewCellEventArgs e)
        {
            if (this.editingControl == null) return;

            this.editingControl.Enter -= DetailsGridControllerEnter;
            this.editingControl = null;
            session.Flush();
        }

        private static void DetailsGridControllerEnter(object sender, EventArgs e)
        {
            ComboBox comboBox = (ComboBox)sender;
            comboBox.Enter -= DetailsGridControllerEnter;
            if (comboBox.Visible)
            {
                comboBox.DroppedDown = true;
            }
        }

        //private void GridCellValueChanged(object sender, DataGridViewCellEventArgs e)
        //{
        //    int position = e.RowIndex;
        //    if (position < 0) return;

        //    string colName = this.grid.Columns[e.ColumnIndex].Name;


        //}

        private void InitColumns()
        {
            this.descriptionDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            this.valueDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            this.openingBalanceDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            this.analysisCategoryGridViewComboBoxColumn = new DataGridViewComboBoxColumn();

            // 
            // summaryDescriptionDataGridViewTextBoxColumn
            // 
            this.descriptionDataGridViewTextBoxColumn.DataPropertyName = "Description";
            this.descriptionDataGridViewTextBoxColumn.HeaderText = Labels.SummaryGridController_InitColumns_Description;
            this.descriptionDataGridViewTextBoxColumn.Name = "Description";
            this.descriptionDataGridViewTextBoxColumn.MinimumWidth = 400;
            this.descriptionDataGridViewTextBoxColumn.HeaderCell.Style.Alignment =
                DataGridViewContentAlignment.MiddleCenter;
            this.descriptionDataGridViewTextBoxColumn.ReadOnly = true;

            // 
            // summaryValueDataGridViewTextBoxColumn
            // 
            this.valueDataGridViewTextBoxColumn.DataPropertyName = "Value";
            this.valueDataGridViewTextBoxColumn.HeaderText = "Input";
            this.valueDataGridViewTextBoxColumn.Name = "Value";
            this.valueDataGridViewTextBoxColumn.MinimumWidth = 100;
            this.valueDataGridViewTextBoxColumn.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.valueDataGridViewTextBoxColumn.DefaultCellStyle.Format = Constants.MONEY_CELL_FORMAT;
            this.valueDataGridViewTextBoxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.valueDataGridViewTextBoxColumn.ReadOnly = true;

            // 
            // summaryValueDataGridViewTextBoxColumn
            // 
            this.openingBalanceDataGridViewTextBoxColumn.DataPropertyName = "OpeningBalance";
            this.openingBalanceDataGridViewTextBoxColumn.HeaderText = "Book Value";
            this.openingBalanceDataGridViewTextBoxColumn.Name = "OpeningBalance";
            this.openingBalanceDataGridViewTextBoxColumn.MinimumWidth = 100;
            this.openingBalanceDataGridViewTextBoxColumn.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.openingBalanceDataGridViewTextBoxColumn.DefaultCellStyle.Format = Constants.MONEY_CELL_FORMAT;
            this.openingBalanceDataGridViewTextBoxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.openingBalanceDataGridViewTextBoxColumn.ReadOnly = true;

            this.analysisCategoryGridViewComboBoxColumn.DataPropertyName = "AnalysisCategory";
            this.analysisCategoryGridViewComboBoxColumn.HeaderText = "Analysis Category";
            this.analysisCategoryGridViewComboBoxColumn.Name = "AnalysisCategory";
            this.analysisCategoryGridViewComboBoxColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
            this.analysisCategoryGridViewComboBoxColumn.MinimumWidth = 150;
            this.analysisCategoryGridViewComboBoxColumn.DropDownWidth = 150;
            this.analysisCategoryGridViewComboBoxColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;



            this.columns = new DataGridViewColumn[]
                           {
                               this.descriptionDataGridViewTextBoxColumn,
                               this.valueDataGridViewTextBoxColumn,
                               this.openingBalanceDataGridViewTextBoxColumn,
                               this.analysisCategoryGridViewComboBoxColumn
                           };

            this.grid.Columns.Clear();
            this.grid.Columns.AddRange(this.columns);
        }

        private void RefreshAnalysisCategories()
        {
            string[] analysisCategories = Category.GetAllAnalysisCategories(session);

            List<string> analysisCategoriesList = new List<string>();
            analysisCategoriesList.Add(Category.DEFAULT_ANALYSIS_CATEGORY);
            analysisCategoriesList.Add(CreateNew);

            analysisCategoriesList.AddRange(analysisCategories.Where(categ => categ != null && categ != Category.DEFAULT_ANALYSIS_CATEGORY));

            foreach (string defaultAnalysisCategory in defaultAnalysisCategories)
            {
                if (!analysisCategoriesList.Contains(defaultAnalysisCategory))
                    analysisCategoriesList.Add(defaultAnalysisCategory);
            }

            this.analysisCategoryGridViewComboBoxColumn.DataSource = new BindingList<string>(analysisCategoriesList);


        }

        private void GridCellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {

            SummaryModel summary = this.GetDataItemAtRowIndex(e.RowIndex);
            if (null == summary)
            {
                return;
            }

            string colName = this.grid.Columns[e.ColumnIndex].Name;

            switch (summary.Type)
            {
                case SummaryType.Header:
                    e.CellStyle.BackColor = Constants.Colors.TableHeader1;
                    e.CellStyle.Font = new Font(e.CellStyle.Font, FontStyle.Bold);
                    if ("Value" == colName)
                    {
                        e.Value = null;
                    }
                    break;
                case SummaryType.Footer:
                    e.CellStyle.BackColor = Constants.Colors.TableFooter1;
                    e.CellStyle.Font = new Font(e.CellStyle.Font, FontStyle.Bold);
                    e.CellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                    break;
                case SummaryType.MasterFooter:
                    e.CellStyle.BackColor = Constants.Colors.TableFooter2;
                    e.CellStyle.Font = new Font(e.CellStyle.Font, FontStyle.Bold);
                    e.CellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                    break;
                default: // SummaryType.Standard, SummaryType.None
                    break;
            }
        }

        private SummaryModel GetDataItemAtRowIndex(int rowIndex)
        {
            if (0 > rowIndex)
            {
                return null;
            }

            return this.grid.Rows[rowIndex].DataBoundItem as SummaryModel;
        }

        public void Refresh(Category newCategory)
        {
            if (this.session != null)
            {
                session.Flush();
                session.Dispose();
            }

            session = SessionManager.OpenSession();

            //MTU BalanceBL.AjustBalanceIfNeeded();
            BalanceBL.AjustBalance();

            Stopwatch stopwatch = Stopwatch.StartNew();

            BindingList<SummaryModel> gridData = new BindingList<SummaryModel>();

            this.category = newCategory;

            if (this.category == null)
            {
                this.grid.DataSource = null;
                return;
            }

            this.category = Category.Find(session, category.Id);
            if (this.category == null)
            {
                this.grid.DataSource = null;
                return;
            }

            if (category.BalanceType == BalanceType.Portfolio)
                category = Category.FindBalanceSheet(session);

            if (category == null) return;

            Currency valuationCurrency = Param.GetValuationCurrency(session);

            this.currencies = Currency.FindAll(session);

            Dictionary<Category, double> categoryBookValues = this.GetAllCategoriesBookValues(session, valuationCurrency);

            if (this.category.BalanceType == BalanceType.BalanceSheet)
            {
                double total = 0;
                double totalBV = 0;

                foreach (Category rootSubCategory in this.category.ChildrenOrdered)
                {

                    SummaryModel headerSummary = new SummaryModel(string.Format("{0}", rootSubCategory.Label), 0, "", "", null);
                    headerSummary.Type = SummaryType.Header;
                    gridData.Add(headerSummary);

                    double subTotal = 0;
                    foreach (Category subCategory in rootSubCategory.ChildrenOrdered)
                    {
                        SummaryModel s = new SummaryModel(subCategory.Label, 
                                                        GetCategoryBookValue(subCategory, categoryBookValues), 
                                                        GetOpeningBalanceValue(subCategory), 
                                                        subCategory.AnalysisCategory, 
                                                        subCategory);
                        subTotal += s.Value;
                        gridData.Add(s);
                    }

                    if (rootSubCategory.BalanceType != BalanceType.Asset)
                    {
                        total += subTotal;
                        totalBV += GetOpeningBalanceValueAsDouble(rootSubCategory);
                    }

                    SummaryModel footerSummary =
                        new SummaryModel(
                            string.Format(Labels.SummaryGridController_Refresh_Total__0_, rootSubCategory.Label),
                            subTotal, GetOpeningBalanceValue(rootSubCategory), "", null);
                    footerSummary.Type = SummaryType.Footer;
                    gridData.Add(footerSummary);
                }
                SummaryModel totalSummary =
                    new SummaryModel(Labels.SummaryGridController_Refresh_Total_Liabilities___Equities, total,
                                     (MainForm.OpeningBalance.Count == 0 ? "Calculate first" : totalBV.ToString("N2")), "", null);
                totalSummary.Type = SummaryType.MasterFooter;
                gridData.Add(totalSummary);
            }
            else
            {
                double total = 0;
                foreach (Category subCategory in this.category.ChildrenOrdered)
                {
                    SummaryModel s = new SummaryModel(subCategory.Label, GetCategoryBookValue(subCategory, categoryBookValues), GetOpeningBalanceValue(subCategory), subCategory.AnalysisCategory, subCategory);
                    total += s.Value;
                    gridData.Add(s);
                }

                SummaryModel totalSummary =
                    new SummaryModel(string.Format(Labels.SummaryGridController_Refresh_Total___0__,
                                      Param.GetValuationCurrency(session).Symbol), total, GetOpeningBalanceValue(this.category), "", null);
                totalSummary.Type = SummaryType.Footer;
                gridData.Add(totalSummary);
            }

            RefreshAnalysisCategories();
            this.grid.DataSource = gridData;

            stopwatch.Stop();

            Debug.WriteLine("SummaryGridController.Refresh took " + stopwatch.ElapsedMilliseconds + "ms");
        }

        private string GetOpeningBalanceValue(Category categ)
        {
            if (MainForm.OpeningBalance.Count == 0)
                return "Calculate first";
            return GetOpeningBalanceValueAsDouble(categ).ToString("N2");
        }

        private double GetOpeningBalanceValueAsDouble(Category categ)
        {
            if (categ.Children.Count == 0)
            {
                if (MainForm.OpeningBalance.ContainsKey(categ.Id))
                    return MainForm.OpeningBalance[categ.Id];
                else
                    return 0;
            }
            return categ.Children.Sum(sc => GetOpeningBalanceValueAsDouble(sc));

        }

        private static double GetCategoryBookValue(Category category, Dictionary<Category, double> categoryBookValues)
        {
            if (category.Children.Count == 0 && categoryBookValues.ContainsKey(category)) return categoryBookValues[category];

            return category.Children.Sum(sc => GetCategoryBookValue(sc, categoryBookValues));
        }

        private Dictionary<Category, double> GetAllCategoriesBookValues(ISession session, Currency valuationCurrency)
        {
            Category[] categories = Category.FindAllWithChildren(session);

            System.Tuple<int, int, double>[] bookValues = Product.GetBookValueByCategoryAndCurrency(session, valuationCurrency);

            Dictionary<Category, double> result =
                categories.ToDictionary(
                    c => c,
                    c => bookValues.
                        Where(bv => bv.Item1 == c.Id).
                        Sum(bv => (double?)CurrencyBL.ConvertToValuationCurrency(bv.Item3, currencies.First(cur => cur.Id == bv.Item2), valuationCurrency)).GetValueOrDefault()
                );

            return result;
        }

        public void ClipboardCopy()
        {
            if (this.grid.GetCellCount(DataGridViewElementStates.Selected) > 0)
            {
                this.grid.CopyToClipboardAsExcel();
            }
        }

        public void ToggleBookValueColumn()
        {
            openingBalanceDataGridViewTextBoxColumn.Visible = !openingBalanceDataGridViewTextBoxColumn.Visible;
        }
        public void ToggleInputColumn()
        {
            valueDataGridViewTextBoxColumn.Visible = !valueDataGridViewTextBoxColumn.Visible;
        }
        public void ToggleAnalysisCategoryColumn()
        {
            analysisCategoryGridViewComboBoxColumn.Visible = !analysisCategoryGridViewComboBoxColumn.Visible;
        }
    }
}