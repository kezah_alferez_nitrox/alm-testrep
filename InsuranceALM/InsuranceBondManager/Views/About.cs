using System;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using ALMSCommon.Forms;
using InsuranceBondManager.Resources.Language;

namespace InsuranceBondManager.Views
{
    public partial class About : DpiForm
    {
        public About()
        {
            InitializeComponent();

            this.Text = Labels.About_About_About_ALM_Solutions;

            appNameLabel.Text = Constants.APPLICATION_NAME;
            appVersionLabel.Text = Labels.About_About_Version_ + Assembly.GetExecutingAssembly().GetName().Version;
        }
    }
}