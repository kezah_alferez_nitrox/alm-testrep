﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using ALMSCommon.Forms;
using InsuranceALM.Infrastructure.Data;
using InsuranceBondManager.Calculations;
using InsuranceBondManager.Core;
using InsuranceBondManager.Core.IO;
using InsuranceBondManager.Domain;
using InsuranceBondManager.Resources.Language;
using NHibernate;
using ZedGraph;

namespace InsuranceBondManager.Views.Generator
{
    public partial class MultiScenarioResult : SessionUserControl
    {
        private Simulation simulation;

        public MultiScenarioResult()
        {
            this.InitializeComponent();
        }

        public GeneratorResultNode[] GetGeneratorResult()
        {
            if (!includeResultsCheckBox.Checked) return null;

            return GetGeneratorResult(treeView.Nodes);
        }

        private static GeneratorResultNode[] GetGeneratorResult(TreeNodeCollection treeNodes)
        {
            if (treeNodes == null || treeNodes.Count == 0) return null;

            GeneratorResultNode[] result = new GeneratorResultNode[treeNodes.Count];
            for (int i = 0; i < treeNodes.Count; i++)
            {
                TreeNode treeNode = treeNodes[i];
                var subNodes = GetGeneratorResult(treeNode.Nodes);
                result[i] = new GeneratorResultNode
                {
                    Label = treeNode.Text,
                    Data = treeNode.Tag as double[][],
                    Nodes = subNodes,
                    IsExpanded = treeNode.IsExpanded,
                };
            }

            return result;
        }

        public void SetGeneratorResult(GeneratorResultNode[] nodes)
        {
            treeView.Nodes.Clear();
            SetGeneratorResult(treeView.Nodes, nodes);
        }

        private static void SetGeneratorResult(TreeNodeCollection treeNodes, GeneratorResultNode[] generatorResultNodes)
        {
            if (generatorResultNodes == null || generatorResultNodes.Length == 0) return;

            foreach (GeneratorResultNode resultNode in generatorResultNodes)
            {
                TreeNode treeNode = treeNodes.Add(resultNode.Label);
                treeNode.Tag = resultNode.Data;

                SetGeneratorResult(treeNode.Nodes, resultNode.Nodes);

                if (resultNode.IsExpanded)
                {
                    treeNode.Expand();
                }
            }
        }

        private void treeView_DoubleClick(object sender, EventArgs e)
        {
            if (this.treeView.SelectedNode == null) return;

            var data = this.GetData(this.treeView.SelectedNode);
            if (data == null) return;

            this.DisplayCategoryPanel(this.treeView.SelectedNode.Text, data);
        }

        private double[][] GetData(TreeNode node)
        {
            if (node.Tag is double[][]) return (double[][])node.Tag;

            int count = Scenario.GeneratedCount(this.Session);
            double[][] sum = new double[count][];
            foreach (TreeNode subNode in node.Nodes)
            {
                double[][] subData = this.GetData(subNode);
                if (subData == null) return null;
                for (int indexScenario = 0; indexScenario < count; indexScenario++)
                {
                    if (sum[indexScenario] == null) sum[indexScenario] = new double[subData[indexScenario].Length];
                    for (int i = 0; i < subData[indexScenario].Length; i++)
                        sum[indexScenario][i] += subData[indexScenario][i];
                }
            }
            return sum;
        }

        private void DisplayCategoryPanel(string label, double[][] data)
        {
            var simulationPanel = new PercentilPanel(label, data);
            simulationPanel.SelectedScenarioChanged += this.SimulationPanelSelectedScenarioChanged;
            simulationPanel.ZoomChanged += this.SimulationPanelZoomChanged;
            simulationPanel.Dock = DockStyle.Top;
            this.containerPanel.Controls.Add(simulationPanel);
        }

        private void SimulationPanelZoomChanged(object sender, EventArgs<Scale> args)
        {
            foreach (Control control in this.containerPanel.Controls)
            {
                PercentilPanel pp = control as PercentilPanel;
                if (pp == null || pp == sender) continue;

                pp.ApplyZoom(args.Value);
            }
        }

        private void SimulationPanelSelectedScenarioChanged(object sender, EventArgs<int> args)
        {
            this.selectedScenarioNumericUpDown.Value = args.Value;
        }

        public void Reload()
        {
            if (this.simulation != null)
            {
                this.LoadTreeViewFromSimulation();
            }

            int count = Scenario.GeneratedCount(this.Session);
            this.scenarioCountLabel.Text = "of " + count;
            this.selectedScenarioNumericUpDown.Maximum = count;

            this.containerPanel.Controls.Clear();
        }

        private void LoadTreeViewFromSimulation()
        {
            this.Cursor = Cursors.WaitCursor;
            this.treeView.Nodes.Clear();

            this.CreateCategoryNodes(ResultSection.BookValue);
            this.CreateCategoryNodes(ResultSection.Income);

            this.CreateCustomNode(this.treeView.Nodes, Labels.MultiScenarioResult_LoadTreeViewFromSimulation_Net_Interest_Income, sd => sd.GetNetInterestIncome());
            this.CreateCustomNode(this.treeView.Nodes, Labels.MultiScenarioResult_LoadTreeViewFromSimulation_Gain_Losses_on_Financial_Instrument,
                                  sd => sd.GetGainLosesOnFinancialInstrument());
            this.CreateOIANodes();
            this.CreateCustomNode(this.treeView.Nodes, Labels.MultiScenarioResult_LoadTreeViewFromSimulation_Income_before_impairment_charge,
                                  sd => sd.GetInterestIncomeBeforeImpairment());
            this.CreateImpairmentNodes();
            this.CreateCustomNode(this.treeView.Nodes, Labels.MultiScenarioResult_LoadTreeViewFromSimulation_Impairment_Charge, sd => sd.GetImpairment());
            this.CreateCustomNode(this.treeView.Nodes, Labels.MultiScenarioResult_LoadTreeViewFromSimulation_Income_Pre_Tax, sd => sd.GetIncomePreTax());
            this.CreateCustomNode(this.treeView.Nodes, Labels.MultiScenarioResult_LoadTreeViewFromSimulation_Income_tax_benefit____expense_, sd => sd.GetTaxes());
            this.CreateCustomNode(this.treeView.Nodes, Labels.MultiScenarioResult_LoadTreeViewFromSimulation_Net_Income, sd => sd.GetNetIncome());
            this.CreateDividendNodes();
            this.CreateCustomNode(this.treeView.Nodes, Labels.MultiScenarioResult_LoadTreeViewFromSimulation_ROE, ROEFunc);
            this.CreateCustomNode(this.treeView.Nodes, Labels.MultiScenarioResult_LoadTreeViewFromSimulation_Periodic_ROA, PeriodicROAFunc);

            this.CreateCategoryNodes(ResultSection.Yield);

            this.CreateSolvencyNodes();

            this.CreateLcrNodes();

            foreach (TreeNode node in this.treeView.Nodes)
            {
                node.Expand();
            }

            this.Cursor = Cursors.Default;
        }

        private void CreateSolvencyNodes()
        {
            TreeNode solvencyNode = this.treeView.Nodes.Add(Labels.MultiScenarioResult_CreateSolvencyNodes_Solvency);

            this.CreateCustomNode(solvencyNode.Nodes, Labels.ResultModel_CreateSolvencyRows_RWA, sd => sd.Solvency.Rwa);
            this.CreateCustomNode(solvencyNode.Nodes, Labels.ResultModel_CreateSolvencyRows_Core_Equity, sd => sd.Solvency.CoreEquity);
            this.CreateCustomNode(solvencyNode.Nodes, Labels.ResultModel_CreateSolvencyRows_Tiers_1, sd => sd.Solvency.Tiers1);
            this.CreateCustomNode(solvencyNode.Nodes, Labels.ResultModel_CreateSolvencyRows_Regulatory_Capital, sd => sd.Solvency.RegulatoryCapital);
            this.CreateCustomNode(solvencyNode.Nodes, Labels.ResultModel_CreateSolvencyRows_Solvency_Ratio_on_Core_Equity, sd => sd.Solvency.SolvencyRatioOnCoreEquity);
            this.CreateCustomNode(solvencyNode.Nodes, Labels.ResultModel_CreateSolvencyRows_Solvency_Ratio_Tiers_1, sd => sd.Solvency.SolvencyRatioTiers1);
            this.CreateCustomNode(solvencyNode.Nodes, Labels.ResultModel_CreateSolvencyRows_Solvency_Ratio, sd => sd.Solvency.SolvencyRatio);
        }

        private void CreateLcrNodes()
        {
            TreeNode lcrNode = this.treeView.Nodes.Add(Labels.MultiScenarioResult_CreateLcrNodes_LCR);

            this.CreateCustomNode(lcrNode.Nodes, Labels.ResultModel_CreateLcrRows_L1_Liquid_assets, sd => sd.Lcr.LiquidAssetsL1);
            this.CreateCustomNode(lcrNode.Nodes, Labels.ResultModel_CreateLcrRows_L2_Liquid_assets, sd => sd.Lcr.LiquidAssetsL1);
            this.CreateCustomNode(lcrNode.Nodes, Labels.ResultModel_CreateLcrRows_Liquid_assets, sd => sd.Lcr.LiquidAssets);
            this.CreateCustomNode(lcrNode.Nodes, Labels.ResultModel_CreateLcrRows_Cash_Inflows, sd => sd.Lcr.NetCashOutflowsIn);
            this.CreateCustomNode(lcrNode.Nodes, Labels.ResultModel_CreateLcrRows_Cash_Outflows, sd => sd.Lcr.NetCashOutflowsOut);
            this.CreateCustomNode(lcrNode.Nodes, Labels.ResultModel_CreateLcrRows_Net_Cash_Outflows, sd => sd.Lcr.NetCashOutflows);
            this.CreateCustomNode(lcrNode.Nodes, Labels.ResultModel_CreateLcrRows_Liquidity_Coverage_Ratio_, GetliquidityCoverageRatio);
        }

        private double[] GetliquidityCoverageRatio(ScenarioData sd)
        {
            Lcr lcr = sd.Lcr;
            double[] liquidityCoverageRatio = new double[lcr.LiquidAssets.Count()];

            double[] netCashOutflows = lcr.NetCashOutflows;
            double[] liquidAssets = lcr.LiquidAssets;

            for (int i = 0; i < liquidityCoverageRatio.Length; i++)
            {
                if (Util.IsNotZero(netCashOutflows[i]))
                {
                    liquidityCoverageRatio[i] = liquidAssets[i] / netCashOutflows[i];
                }
            }

            return liquidityCoverageRatio;
        }

        private void CreateImpairmentNodes()
        {
            TreeNode node = this.treeView.Nodes.Add(Labels.MultiScenarioResult_CreateImpairmentNodes_Impairments);

            foreach (Category category in Category.FindAll(this.Session))
            {
                if (category.IsVisibleCheckedModel == true)
                {
                    Category key = category;
                    this.CreateCustomNode(node.Nodes, category.Label, sd => sd.GetImpermentData(key).Select(v => (double)v).ToArray());
                }
            }
        }

        private void CreateDividendNodes()
        {
            TreeNode node = this.treeView.Nodes.Add(Labels.MultiScenarioResult_CreateDividendNodes_Dividends);

            IDictionary<Dividend, double[][]> data = new Dictionary<Dividend, double[][]>();

            int scenarioIndex = 0;
            foreach (Scenario scenario in this.simulation.Scenarios)
            {
                ScenarioData scenarioData = this.simulation[scenario];

                double[] netIncome = scenarioData.GetNetIncome();
                IDictionary<Dividend, double[]> dividendAmounts = scenarioData.GetDividendAmounts();

                if (scenarioIndex == 0)
                {
                    foreach (Dividend dividend in dividendAmounts.Keys)
                    {
                        data.Add(dividend, new double[this.simulation.Count][]);
                    }
                }

                foreach (KeyValuePair<Dividend, double[]> pair in dividendAmounts)
                {
                    Dividend dividend = pair.Key;
                    double[] values = pair.Value;

                    data[dividend][scenarioIndex] = new double[Param.ResultMonths(this.Session)];
                    for (int i = 0; i < Param.ResultMonths(this.Session); i++)
                    {
                        data[dividend][scenarioIndex][i] = (double)values[i];
                    }
                }

                scenarioIndex++;
            }

            foreach (Dividend dividend in data.Keys)
            {
                TreeNode subNode = node.Nodes.Add(dividend.Name);
                subNode.Tag = data[dividend];
            }
        }

        private void CreateOIANodes()
        {
            TreeNode node = this.treeView.Nodes.Add(Labels.MultiScenarioResult_CreateOIANodes_Other_Item_Assumptions);

            IDictionary<string, double[][]> oiaData = new Dictionary<string, double[][]>();

            int scenarioIndex = 0;
            foreach (Scenario scenario in this.simulation.Scenarios)
            {
                ScenarioData scenarioData = this.simulation[scenario];

                IDictionary<OtherItemAssum, double[]> oiaValues = scenarioData.GetOtherItemAssumptionsNotImpairments();

                if (scenarioIndex == 0)
                {
                    foreach (OtherItemAssum oia in oiaValues.Keys)
                    {
                        oiaData.Add(oia.Descrip, new double[this.simulation.Count][]);
                    }
                }

                foreach (OtherItemAssum oia in oiaValues.Keys)
                {
                    oiaData[oia.Descrip][scenarioIndex] = new double[Param.ResultMonths(this.Session)];
                    for (int i = 0; i < Param.ResultMonths(this.Session); i++)
                    {
                        oiaData[oia.Descrip][scenarioIndex][i] = (double)oiaValues[oia][i];
                    }
                }
                scenarioIndex++;
            }

            foreach (string oiaName in oiaData.Keys)
            {
                TreeNode subNode = node.Nodes.Add(oiaName);
                subNode.Tag = oiaData[oiaName];
            }
        }

        private  double[] PeriodicROAFunc(ScenarioData scenarioData)
        {
            double[] netIncome = scenarioData.GetNetIncome();
            double[] totalAssets = scenarioData.GetTotalAssets();

            double[] result = new double[Param.ResultMonths(this.Session)];
            for (int i = 0; i < netIncome.Length; i++)
            {
                if (Util.IsNotZero(totalAssets[i]))
                {
                    result[i] = netIncome[i] / totalAssets[i] * 12;
                }
            }

            return result;
        }

        private  double[] ROEFunc(ScenarioData scenarioData)
        {
            double[] netIncome = scenarioData.GetNetIncome();
            double[] totalEquity = scenarioData.GetTotalEquity();

            double[] result = new double[Param.ResultMonths(this.Session)];
            for (int i = 0; i < netIncome.Length; i++)
            {
                if (Util.IsNotZero(totalEquity[i]))
                {
                    result[i] = netIncome[i] / totalEquity[i];
                }
            }

            return result;
        }

        private void CreateCustomNode(TreeNodeCollection nodes, string title, Func<ScenarioData, double[]> func)
        {
            TreeNode node = nodes.Add(title);

            if (this.simulation == null || this.simulation.Count == 0) return;

            double[][] data = new double[this.simulation.Count][];

            int scenarioIndex = 0;
            foreach (Scenario scenario in this.simulation.Scenarios)
            {
                ScenarioData scenarioData = this.simulation[scenario];
                double[] values = func(scenarioData);

                data[scenarioIndex] = new double[Param.ResultMonths(this.Session)];
                for (int i = 0; i < Param.ResultMonths(this.Session); i++)
                {
                    data[scenarioIndex][i] = (double)values[i];
                }
                scenarioIndex++;
            }

            node.Tag = data;
        }

        private void CreateCategoryNodes(ResultSection section)
        {
            string label = GetSectionLabel(section, null);
            TreeNode node = this.treeView.Nodes.Add(label);

            Category root = Category.FindPortfolio(this.Session);

            this.CreateSubCategoryNodes(root, node, section);
        }

        private void CreateSubCategoryNodes(Category parentCategory, TreeNode parentNode, ResultSection section)
        {
            foreach (Category cat in parentCategory.ChildrenOrdered)
            {
                TreeNode node = parentNode.Nodes.Add(cat.Label);
                node.Tag = this.GetCategoryValuesBySection(cat, section);

                this.CreateSubCategoryNodes(cat, node, section);
            }
        }

        private double[][] GetCategoryValuesBySection(Category category, ResultSection section)
        {
            if (this.simulation == null || this.simulation.Count == 0) return null;

            double[][] result = new double[this.simulation.Count][];

            int scenarioIndex = 0;
            foreach (Scenario scenario in this.simulation.Scenarios)
            {
                ScenarioData scenarioData = this.simulation[scenario];
                double[] values = new double[Param.ResultMonths(this.Session)];
                switch (section)
                {
                    case ResultSection.BookValue:
                        values = scenarioData.GetBookValue(category);
                        break;
                    case ResultSection.Income:
                        values = scenarioData.GetIncome(category);
                        break;
                    case ResultSection.Yield:
                        double[] income = scenarioData.GetIncome(category);
                        double[] bookValues = scenarioData.GetBookValue(category);
                        for (int i = 1; i < income.Length; i++)
                        {
                            if (Util.IsNotZero(bookValues[i]) && i < bookValues.Length && i < values.Length) values[i] = income[i] / bookValues[i] * 100;
                        }
                        break;
                    default:
                        throw new ArgumentOutOfRangeException("section");
                }

                result[scenarioIndex] = new double[Param.ResultMonths(this.Session)];
                for (int i = 0; i < Param.ResultMonths(this.Session); i++)
                {
                    result[scenarioIndex][i] = (double)values[i];
                }
                scenarioIndex++;
            }

            return result;
        }

        private static string GetSectionLabel(ResultSection section, Category category)
        {
            string type;
            switch (section)
            {
                case ResultSection.BookValue:
                    type = Labels.MultiScenarioResult_GetSectionLabel_Book_Values;
                    break;
                case ResultSection.Income:
                    if (category == null)
                    {
                        type = Labels.MultiScenarioResult_GetSectionLabel_Interest_Income___Expenses;
                    }
                    else if (category.BalanceType == BalanceType.Asset)
                    {
                        type = Labels.MultiScenarioResult_GetSectionLabel_Interest_Income;
                    }
                    else
                    {
                        type = Labels.MultiScenarioResult_GetSectionLabel_Interest_Expenses;
                    }
                    break;
                case ResultSection.Yield:
                    type = Labels.MultiScenarioResult_GetSectionLabel_Yield;
                    break;
                default:
                    throw new ArgumentOutOfRangeException("section");
            }
            return type;
        }

        public void Reload(Simulation sim)
        {
            if (this.simulation == sim) return;

            this.simulation = sim;

            this.Reload();
        }

        private void selectedScenarioNumericUpDown_ValueChanged(object sender, EventArgs e)
        {
            int index = (int)this.selectedScenarioNumericUpDown.Value;
            this.exportButton.Enabled = index > 0;
            this.SelectScenario(index);
        }

        private void SelectScenario(int index)
        {
            foreach (Control control in this.containerPanel.Controls)
            {
                PercentilPanel panel = control as PercentilPanel;
                if (panel == null) continue;

                panel.SelectCurve(index);
            }
        }

        private void exportButton_Click(object sender, EventArgs e)
        {
            if (this.simulation == null) return;
            int index = (int)this.selectedScenarioNumericUpDown.Value;
            if (index <= 0 || index > this.simulation.Scenarios.Count) return;

            ScenarioNamePrompt prompt = new ScenarioNamePrompt();
            if (prompt.ShowDialog(this.ParentForm) != DialogResult.OK)
            {
                return;
            }

            Scenario[] simulationScenarios = new Scenario[this.simulation.Scenarios.Count];
            this.simulation.Scenarios.CopyTo(simulationScenarios, 0);

            Scenario selectedScenario = simulationScenarios[index - 1];

            LoadForm.Instance.DoWork(new WorkParameters(
                                         bw => ExportScenario(selectedScenario, prompt.ScenarioName), null));
        }

        private static void ExportScenario(Scenario oldScenario, string scenarioName)
        {
            using (ISession session = SessionManager.OpenSession())
            {
                Scenario scenario = new Scenario();
                scenario.Name = scenarioName;
                scenario.Generated = false;
                scenario.Description = Labels.MultiScenarioResult_ExportScenario_export_of_a_generated_scenario;
                scenario.Selected = true;
                scenario.VariableValues = new List<VariableValue>(oldScenario.VariableValues.Count);

                foreach (VariableValue oldValue in oldScenario.VariableValues)
                {
                    VariableValue value = new VariableValue();
                    value.Scenario = scenario;
                    value.Variable = oldValue.Variable;

                    scenario.VariableValues.Add(value);
                    oldValue.Variable.VariableValues.Add(value);

                    if (oldValue.ValueIsArray())
                    {
                        string vectorName = CreateVector(session, oldValue.GetArrayValue(),
                                                         scenarioName + "_" + value.Variable.Name);
                        value.Value = vectorName;
                    }
                    else
                    {
                        value.Value = oldValue.Value;
                    }
                }

                scenario.Create(session);
                session.Flush();
            }
        }

        private static string CreateVector(ISession session, double[] values, string name)
        {
            Vector vector = new Vector();
            vector.Frequency = VectorFrequency.Monthly;
            vector.Group = VectorGroup.Workspace;
            vector.ImportantMarketData = false;
            vector.Name = name;
            vector.VectorPoints = new List<VectorPoint>();

            DateTime valuationDate = Param.GetValuationDate(session) ?? DateTime.Now;

            for (int i = 0; i < values.Length; i++)
            {
                VectorPoint vectorPoint = new VectorPoint();
                vectorPoint.Vector = vector;
                vectorPoint.Date = valuationDate.AddMonths(i);
                vectorPoint.Value = values[i];

                vector.VectorPoints.Add(vectorPoint);
            }

            vector.Create(session);
            session.Flush();

            return name;
        }

        public void DisposeSimultation()
        {
            this.simulation = null;
            GC.Collect();
        }
    }
}