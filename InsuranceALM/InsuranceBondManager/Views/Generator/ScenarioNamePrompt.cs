﻿using System;
using InsuranceBondManager.Core;
using InsuranceBondManager.Domain;


namespace InsuranceBondManager.Views.Generator
{
    public partial class ScenarioNamePrompt : SessionForm
    {
        public ScenarioNamePrompt()
        {
            InitializeComponent();

            this.okButton.Click += this.OkButtonClick;
        }

        void OkButtonClick(object sender, EventArgs e)
        {
            this.SaveChangesInSession();
        }

        public string ScenarioName
        {
            get { return scenarioTextBox.Text; }
        }

        private void ScenarioTextBoxTextChanged(object sender, EventArgs e)
        {
            if(scenarioTextBox.Text.Length == 0)
            {
                okButton.Enabled = false;
                return;
            }
            Scenario findByName = Scenario.FindByName(this.Session, this.scenarioTextBox.Text);

            okButton.Enabled = findByName == null;
        }
    }
}