using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using ALMSCommon;
using InsuranceBondManager.Domain;
using ZedGraph;
using System.Diagnostics;

namespace InsuranceBondManager.Views.Generator
{
    public delegate void SelectedScenarioHandler(object sender, EventArgs<int> args);
    public delegate void ZoomHandler(object sender, EventArgs<Scale> args);

    public partial class PercentilPanel : UserControl
    {
        private const int BottomPadding = 5;

        private readonly double[][] data;

        private static readonly int[] Percents = new[] { 0, 1, 5, 10, 33, 50, 67, 90, 95, 100 };
        private static readonly string[] PercentLabels = new[] { "1", "5", "10", "33", "MEAN", "67", "90", "95", "99", "100" };
        private static readonly int FirstYear = DateTime.Today.Year;
        private int[] xValues;

        private readonly Color middleColor = Color.Gray;
        private readonly Color outilineColor = Color.Black;
        private readonly Color selectionColor = Color.Green;
        private readonly Color[] colors = new Color[10];
        //{
        //    Color.White,
        //    Color.FromArgb(0, 0, 0), 
        //    Color.FromArgb(63, 63, 63), 
        //    Color.FromArgb(128, 128, 128),
        //    Color.FromArgb(192, 192, 192), 
        //    Color.FromArgb(255, 255, 255),
        //    Color.FromArgb(255, 192, 192), 
        //    Color.FromArgb(255, 128, 128),
        //    Color.FromArgb(255, 63, 63),
        //    Color.FromArgb(192, 0, 0),
        //};

        private readonly Color[] textColors = new[]
                                               {
                                                   Color.White,
                                                   Color.White,
                                                   Color.White,
                                                   Color.Black,
                                                   Color.Black,
                                                   Color.Black,
                                                   Color.Black,
                                                   Color.Black,
                                                   Color.White,
                                                   Color.White,
                                               };

        private readonly double[][] percentils = new double[Percents.Length][];
        private readonly double[][] relativePercentils = new double[Percents.Length][];

        private LineItem selectedCurve = null;

        private int selectedIndex;
        private readonly int count;
        private bool resizeing;

        public event SelectedScenarioHandler SelectedScenarioChanged = delegate { };
        public event ZoomHandler ZoomChanged = delegate { };

        public PercentilPanel(string title, double[][] data)
        {
            for (int i = 0; i < 6; i++)
                this.colors[i] = Color.FromArgb(255, 155 + i * 20, 155 + i * 20);
            for (int i = 6; i < 10; i++)
                this.colors[i] = Color.FromArgb(255 - i * 20, 255 - i * 20, 255);

            this.data = data;
            this.InitializeComponent();

            this.titleLabel.Text = title;

            this.count = data[0] == null ? 0 : data[0].Length;
            if (count <= 0) return;

            this.InitPercentils();            

            this.InitGrid();
            this.UpdatePercentilesGraph();
        }

        private void InitPercentils()
        {
            var sortedValues = new List<double[]>();

            //MTU : do not show first point (OB)
            this.xValues = new int[this.count - 1];
            for (int i = 1; i < this.count; i++)
            {
                this.xValues[i - 1] = i;
            }

            for (int i = 1; i < this.count; i++)
            {
                double[] column = new double[data.Length];
                for (int j = 0; j < data.Length; j++)
                {
                    column[j] = data[j][i];
                }
                Array.Sort(column);
                sortedValues.Add(column);
            }

            for (int j = 0; j < Percents.Length; j++)
            {
                var percentile = new List<double>();
                for (int i = 1; i < this.count; i++)
                {//MTU : do not show first point (OB)
                    percentile.Add(Percentile.Calculate(sortedValues[i - 1], Percents[j]));
                }
                this.percentils[j] = percentile.ToArray();
            }
        }

        private void InitRelativePercentils()
        {
            for (int i = 0; i < this.percentils.Length; i++)
            {
                this.relativePercentils[i] = new double[this.percentils[i].Length];
                for (int j = 0; j < this.percentils[i].Length; j++)
                {
                    this.relativePercentils[i][j] = i == 0
                                                         ? this.percentils[i][j]
                                                         : this.percentils[i][j] - this.percentils[i - 1][j];
                }
            }
        }

        private void InitGrid()
        {
            this.CreateGridColumns();

            for (int j = 1; j < Percents.Length; j++)
            {
                var line = new List<object> { PercentLabels[j - 1] };
                for (int i = 0; i < this.percentils[j].Length; i++)
                {
                    line.Add(this.percentils[j][i]);
                }
                int idx = this.percentileGrid.Rows.Add(line.ToArray());
                this.percentileGrid[0, idx].Style.BackColor = this.colors[j];
                this.percentileGrid[0, idx].Style.ForeColor = this.textColors[j];
            }
        }

        private void CreateGridColumns()
        {
            DataGridViewColumn[] columns = new DataGridViewColumn[this.count];

            DataGridViewCellStyle numericStyle = new DataGridViewCellStyle();
            numericStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            numericStyle.Format = "N3";

            columns[0] = new DataGridViewTextBoxColumn();
            columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            columns[0].Frozen = true;
            columns[0].HeaderText = "";
            columns[0].Name = "labelColumn";
            columns[0].ReadOnly = true;
            columns[0].Width = 40;
            columns[0].FillWeight = 1;
            for (int i = 0; i < this.count - 1; i++)
            {
                columns[i + 1] = new DataGridViewTextBoxColumn();
                columns[i + 1].DefaultCellStyle = numericStyle;
                columns[i + 1].HeaderText = IndexToLabel(this.xValues[i]);
                columns[i + 1].Name = "year" + i + "Column";
                columns[i + 1].ReadOnly = true;
                columns[i + 1].Width = 80;
                columns[i + 1].FillWeight = 1;
            }

            ((ISupportInitialize)this.percentileGrid).BeginInit();
            this.percentileGrid.Columns.Clear();
            this.percentileGrid.Columns.AddRange(columns);
            ((ISupportInitialize)this.percentileGrid).EndInit();
        }

        private void BCloseClick(object sender, EventArgs e)
        {
            this.Parent.Controls.Remove(this);
        }

        private void UpdatePercentilesGraph()
        {
            this.zedGraphControl.IsAntiAlias = true;

            GraphPane graphPane = this.zedGraphControl.GraphPane;

            graphPane.Title.IsVisible = false;
            graphPane.Legend.IsVisible = false;

            graphPane.XAxis.Title.IsVisible = false;
            graphPane.XAxis.Type = AxisType.Text;
            graphPane.XAxis.Scale.TextLabels = this.xValues.Select(IndexToLabel).ToArray();

            graphPane.YAxis.Title.Text = "";
            graphPane.YAxis.Title.IsVisible = false;

            double maxY;
            double minY;
            GetArrayMinMax(this.percentils, out minY, out maxY);
            graphPane.YAxis.Scale.Min = minY;
            graphPane.YAxis.Scale.Max = maxY;

            graphPane.LineType = LineType.Stack;

            graphPane.CurveList.Clear();

            this.InitRelativePercentils();

            double[] selectedScenarioYValues = this.DrawSelectedScenarioCurve();

            if (selectedScenarioYValues != null)
            {
                for (int i = 0; i < selectedScenarioYValues.Length; i++)
                {
                    this.relativePercentils[0][i] -= selectedScenarioYValues[i];
                }
            }

            //draw percentils
            for (int i = 0; i < this.relativePercentils.Length; i++)
            {
                LineItem percentilCurve = graphPane.AddCurve(
                    PercentLabels[i],
                    null,
                    this.relativePercentils[i],
                    this.colors[i], SymbolType.Circle);

                percentilCurve.Symbol.Size = percentilCurve.Line.Width = 3;

                if (i == 0)
                {
                    percentilCurve.Symbol.Fill = new Fill(Color.Black);
                    percentilCurve.Line.Color =
                    percentilCurve.Symbol.Border.Color = Color.Black;
                }
                else
                {
                    percentilCurve.Symbol.Border.Color = percentilCurve.Line.Color = this.colors[i];
                    percentilCurve.Symbol.Fill = percentilCurve.Line.Fill = new Fill(this.colors[i]);
                }
            }

            this.zedGraphControl.AxisChange();
            this.zedGraphControl.Invalidate();
        }

        private static string IndexToLabel(int x)
        {
            return x == 0 ? "OB" : string.Format("{0:00}/{1:0000}", ((x - 1) % 12 + 1), (FirstYear + (x - 1) / 12));
        }

        private static void GetArrayMinMax(double[][] array, out double minY, out double maxY)
        {
            minY = double.MaxValue;
            maxY = double.MinValue;
            foreach (double[] t in array)
            {
                foreach (double percentil in t)
                {
                    minY = Math.Min(minY, percentil);
                    maxY = Math.Max(maxY, percentil);
                }
            }
        }

        public void SelectCurve(int index)
        {
            if(this.count<=0) return;

            this.selectedIndex = index;

            this.UpdatePercentilesGraph();
        }

        private double[] DrawSelectedScenarioCurve()
        {
            GraphPane graphPane = this.zedGraphControl.GraphPane;

            if (this.selectedIndex != 0 && this.selectedIndex <= this.data.Length)
            {
                double[] yValues = new double[data[selectedIndex - 1].Length - 1];
                //MTU : do not shown index 0 (OB)
                Array.Copy(data[selectedIndex - 1], 1, yValues, 0, data[selectedIndex - 1].Length - 1);

                this.selectedCurve = graphPane.AddCurve("", null, yValues, this.selectionColor);
                this.selectedCurve.Line.Width = 3;
                this.selectedCurve.Symbol.IsVisible = false;

                return yValues;
            }

            return null;
        }

        protected override void OnMouseDown(MouseEventArgs e)
        {
            base.OnMouseDown(e);

            if (this.InsideResizeZone(e.Location))
            {
                this.resizeing = true;
                this.Capture = true;
            }
        }

        protected override void OnMouseLeave(EventArgs e)
        {
            base.OnMouseLeave(e);

            this.Cursor = Cursors.Default;
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);

            if (this.resizeing)
            {
                this.Height = e.Location.Y;
            }
            else
            {
                this.Cursor = this.InsideResizeZone(e.Location) ? Cursors.SizeNS : Cursors.Default;
            }
        }

        protected override void OnMouseUp(MouseEventArgs e)
        {
            base.OnMouseUp(e);

            if (this.resizeing)
            {
                this.resizeing = false;
                this.Capture = false;
            }
        }

        private bool InsideResizeZone(Point location)
        {
            return location.Y >= this.Height - BottomPadding && location.Y <= this.Height;
        }

        private bool ZedGraphControlMouseDownEvent(ZedGraphControl sender, MouseEventArgs e)
        {
            if (ModifierKeys != Keys.Alt) return false;

            if (this.count <= 0) return false;

            PointF mousePt = new PointF(e.X, e.Y);
            GraphPane pane = this.zedGraphControl.GraphPane;

            double x;
            double y;
            pane.ReverseTransform(mousePt, out x, out y);

            int clickIndex = (int)Math.Round(x);
            double clickValue = y;

            var deltas = this.data.Select((v, i) => new { Index = i, Delta = Math.Abs(v[clickIndex] - clickValue) });
            double min = deltas.Min(d => d.Delta);

            int scenarioIndex = deltas.First(d => d.Delta == min).Index;
            this.SelectedScenarioChanged(this, new EventArgs<int>(scenarioIndex + 1));

            return true;
        }

        public void ApplyZoom(Scale scale)
        {
            zedGraphControl.GraphPane.XAxis.Scale.Min = scale.Min;
            zedGraphControl.GraphPane.XAxis.Scale.Max = scale.Max;

            this.zedGraphControl.AxisChange();
            this.zedGraphControl.Invalidate();
        }

        private void ZedGraphControlZoomEvent(ZedGraphControl sender, ZoomState oldState, ZoomState newState)
        {
            this.ZoomChanged(this, new EventArgs<Scale>(zedGraphControl.GraphPane.XAxis.Scale));
        }
    }
}