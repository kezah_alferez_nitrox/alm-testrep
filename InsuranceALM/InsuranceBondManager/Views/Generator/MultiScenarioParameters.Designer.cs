﻿using ALMSCommon.Controls;
using ALMSCommon.Controls.TreeGridView;
using InsuranceBondManager.Controls;
using InsuranceBondManager.Resources.Language;

namespace InsuranceBondManager.Views.Generator
{
    partial class MultiScenarioParameters
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.reloadButton = new System.Windows.Forms.Button();
            this.scenariosToGenerateNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.scenariosComboBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.saveAndCloseButton = new System.Windows.Forms.Button();
            this.generateButton = new System.Windows.Forms.Button();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.volatilitiesDataGridView = new ALMSCommon.Controls.DataGridViewEx();
            this.correlationsDataGridView = new ALMSCommon.Controls.DataGridViewEx();
            this.correlationsLabel = new System.Windows.Forms.Label();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scenariosToGenerateNumericUpDown)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.volatilitiesDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.correlationsDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.reloadButton);
            this.panel1.Controls.Add(this.scenariosToGenerateNumericUpDown);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.scenariosComboBox);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(964, 48);
            this.panel1.TabIndex = 0;
            // 
            // reloadButton
            // 
            this.reloadButton.Location = new System.Drawing.Point(293, 12);
            this.reloadButton.Name = "reloadButton";
            this.reloadButton.Size = new System.Drawing.Size(75, 23);
            this.reloadButton.TabIndex = 4;
            this.reloadButton.Text = global::InsuranceBondManager.Resources.Language.Labels.MultiScenarioParameters_InitializeComponent_Reload;
            this.reloadButton.UseVisualStyleBackColor = true;
            this.reloadButton.Click += new System.EventHandler(this.ReloadButtonClick);
            // 
            // scenariosToGenerateNumericUpDown
            // 
            this.scenariosToGenerateNumericUpDown.Location = new System.Drawing.Point(851, 14);
            this.scenariosToGenerateNumericUpDown.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.scenariosToGenerateNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.scenariosToGenerateNumericUpDown.Name = "scenariosToGenerateNumericUpDown";
            this.scenariosToGenerateNumericUpDown.Size = new System.Drawing.Size(101, 20);
            this.scenariosToGenerateNumericUpDown.TabIndex = 3;
            this.scenariosToGenerateNumericUpDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.scenariosToGenerateNumericUpDown.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(731, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(114, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Scenarios to generate ";
            // 
            // scenariosComboBox
            // 
            this.scenariosComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.scenariosComboBox.FormattingEnabled = true;
            this.scenariosComboBox.Location = new System.Drawing.Point(115, 13);
            this.scenariosComboBox.Name = "scenariosComboBox";
            this.scenariosComboBox.Size = new System.Drawing.Size(171, 21);
            this.scenariosComboBox.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Based on scenario";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.saveAndCloseButton);
            this.panel2.Controls.Add(this.generateButton);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 487);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(964, 68);
            this.panel2.TabIndex = 1;
            // 
            // saveAndCloseButton
            // 
            this.saveAndCloseButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.saveAndCloseButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.saveAndCloseButton.Image = global::InsuranceBondManager.Properties.Resources.saveHS;
            this.saveAndCloseButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.saveAndCloseButton.Location = new System.Drawing.Point(630, 13);
            this.saveAndCloseButton.Name = "saveAndCloseButton";
            this.saveAndCloseButton.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.saveAndCloseButton.Size = new System.Drawing.Size(120, 43);
            this.saveAndCloseButton.TabIndex = 3;
            this.saveAndCloseButton.Text = "Save";
            this.saveAndCloseButton.UseVisualStyleBackColor = true;
            this.saveAndCloseButton.Click += new System.EventHandler(this.SaveAndCloseButtonClick);
            // 
            // generateButton
            // 
            this.generateButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.generateButton.Image = global::InsuranceBondManager.Properties.Resources.percentile;
            this.generateButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.generateButton.Location = new System.Drawing.Point(792, 13);
            this.generateButton.Name = "generateButton";
            this.generateButton.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.generateButton.Size = new System.Drawing.Size(160, 43);
            this.generateButton.TabIndex = 0;
            this.generateButton.Text = global::InsuranceBondManager.Resources.Language.Labels.MultiScenarioParameters_InitializeComponent_Launch_Simulation;
            this.generateButton.UseVisualStyleBackColor = true;
            this.generateButton.Click += new System.EventHandler(this.GenerateButtonClick);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 48);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.volatilitiesDataGridView);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.correlationsDataGridView);
            this.splitContainer1.Panel2.Controls.Add(this.correlationsLabel);
            this.splitContainer1.Size = new System.Drawing.Size(964, 439);
            this.splitContainer1.SplitterDistance = 408;
            this.splitContainer1.TabIndex = 2;
            // 
            // volatilitiesDataGridView
            // 
            this.volatilitiesDataGridView.AllowUserToAddRows = false;
            this.volatilitiesDataGridView.AllowUserToDeleteRows = false;
            this.volatilitiesDataGridView.AllowUserToResizeRows = false;
            this.volatilitiesDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.volatilitiesDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.volatilitiesDataGridView.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.volatilitiesDataGridView.Location = new System.Drawing.Point(0, 0);
            this.volatilitiesDataGridView.Name = "volatilitiesDataGridView";
            this.volatilitiesDataGridView.RowHeadersVisible = false;
            this.volatilitiesDataGridView.Size = new System.Drawing.Size(408, 439);
            this.volatilitiesDataGridView.TabIndex = 0;
            // 
            // correlationsDataGridView
            // 
            this.correlationsDataGridView.AllowUserToAddRows = false;
            this.correlationsDataGridView.AllowUserToDeleteRows = false;
            this.correlationsDataGridView.AllowUserToResizeRows = false;
            this.correlationsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.correlationsDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.correlationsDataGridView.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.correlationsDataGridView.Location = new System.Drawing.Point(0, 24);
            this.correlationsDataGridView.Name = "correlationsDataGridView";
            this.correlationsDataGridView.RowHeadersVisible = false;
            this.correlationsDataGridView.Size = new System.Drawing.Size(552, 415);
            this.correlationsDataGridView.TabIndex = 1;
            // 
            // correlationsLabel
            // 
            this.correlationsLabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.correlationsLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.correlationsLabel.Location = new System.Drawing.Point(0, 0);
            this.correlationsLabel.Name = "correlationsLabel";
            this.correlationsLabel.Padding = new System.Windows.Forms.Padding(5);
            this.correlationsLabel.Size = new System.Drawing.Size(552, 24);
            this.correlationsLabel.TabIndex = 0;
            this.correlationsLabel.Text = "Correlations";
            this.correlationsLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Volatility";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle1.Format = "N5";
            this.dataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewTextBoxColumn1.FillWeight = 70F;
            this.dataGridViewTextBoxColumn1.HeaderText = global::InsuranceBondManager.Resources.Language.Labels.MultiScenarioParameters_InitializeComponent_Volatility_factor;
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn2.DataPropertyName = "VariableName";
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dataGridViewTextBoxColumn2.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridViewTextBoxColumn2.HeaderText = global::InsuranceBondManager.Resources.Language.Labels.MultiScenarioParameters_InitializeComponent_Variable;
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // MultiScenarioParameters
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "MultiScenarioParameters";
            this.Size = new System.Drawing.Size(964, 555);
            this.Load += new System.EventHandler(this.MultiScenarioGeneratorLoad);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scenariosToGenerateNumericUpDown)).EndInit();
            this.panel2.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.volatilitiesDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.correlationsDataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.NumericUpDown scenariosToGenerateNumericUpDown;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox scenariosComboBox;
        private System.Windows.Forms.Label label1;
        private DataGridViewEx volatilitiesDataGridView;
        private DataGridViewEx correlationsDataGridView;
        private System.Windows.Forms.Label correlationsLabel;
        private System.Windows.Forms.Button generateButton;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.Button reloadButton;
        private System.Windows.Forms.Button saveAndCloseButton;
    }
}