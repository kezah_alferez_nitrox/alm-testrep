﻿using InsuranceBondManager.Controls;
using InsuranceBondManager.Resources.Language;

namespace InsuranceBondManager.Views.Generator
{
    partial class MultiScenarioResult
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.treeView = new InsuranceBondManager.Controls.TreeViewEx();
            this.containerPanel = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.includeResultsCheckBox = new System.Windows.Forms.CheckBox();
            this.exportButton = new System.Windows.Forms.Button();
            this.scenarioCountLabel = new System.Windows.Forms.Label();
            this.selectedScenarioNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.selectedScenarioNumericUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.treeView);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.containerPanel);
            this.splitContainer1.Panel2.Controls.Add(this.panel1);
            this.splitContainer1.Size = new System.Drawing.Size(881, 463);
            this.splitContainer1.SplitterDistance = 181;
            this.splitContainer1.TabIndex = 0;
            // 
            // treeView
            // 
            this.treeView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeView.Location = new System.Drawing.Point(0, 0);
            this.treeView.Name = "treeView";
            this.treeView.Size = new System.Drawing.Size(181, 463);
            this.treeView.TabIndex = 1;
            this.treeView.DoubleClick += new System.EventHandler(this.treeView_DoubleClick);
            // 
            // containerPanel
            // 
            this.containerPanel.AutoScroll = true;
            this.containerPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.containerPanel.Location = new System.Drawing.Point(0, 41);
            this.containerPanel.Name = "containerPanel";
            this.containerPanel.Size = new System.Drawing.Size(696, 422);
            this.containerPanel.TabIndex = 1;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(696, 41);
            this.panel1.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.includeResultsCheckBox);
            this.groupBox1.Controls.Add(this.exportButton);
            this.groupBox1.Controls.Add(this.scenarioCountLabel);
            this.groupBox1.Controls.Add(this.selectedScenarioNumericUpDown);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(696, 41);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // includeResultsCheckBox
            // 
            this.includeResultsCheckBox.AutoSize = true;
            this.includeResultsCheckBox.Location = new System.Drawing.Point(443, 16);
            this.includeResultsCheckBox.Name = "includeResultsCheckBox";
            this.includeResultsCheckBox.Size = new System.Drawing.Size(232, 17);
            this.includeResultsCheckBox.TabIndex = 10;
            this.includeResultsCheckBox.Text = Labels.MultiScenarioResult_InitializeComponent_Include_results_into_workspace_when_saving;
            this.includeResultsCheckBox.UseVisualStyleBackColor = true;
            // 
            // exportButton
            // 
            this.exportButton.Enabled = false;
            this.exportButton.Location = new System.Drawing.Point(255, 14);
            this.exportButton.Name = "exportButton";
            this.exportButton.Size = new System.Drawing.Size(157, 23);
            this.exportButton.TabIndex = 9;
            this.exportButton.Text = Labels.MultiScenarioResult_InitializeComponent_Export_Selected_Scenario___;
            this.exportButton.UseVisualStyleBackColor = true;
            this.exportButton.Click += new System.EventHandler(this.exportButton_Click);
            // 
            // scenarioCountLabel
            // 
            this.scenarioCountLabel.AutoSize = true;
            this.scenarioCountLabel.Location = new System.Drawing.Point(174, 16);
            this.scenarioCountLabel.Name = "scenarioCountLabel";
            this.scenarioCountLabel.Size = new System.Drawing.Size(59, 13);
            this.scenarioCountLabel.TabIndex = 8;
            this.scenarioCountLabel.Text = Labels.MultiScenarioResult_InitializeComponent_Scenario__;
            // 
            // selectedScenarioNumericUpDown
            // 
            this.selectedScenarioNumericUpDown.Location = new System.Drawing.Point(71, 14);
            this.selectedScenarioNumericUpDown.Name = "selectedScenarioNumericUpDown";
            this.selectedScenarioNumericUpDown.Size = new System.Drawing.Size(97, 20);
            this.selectedScenarioNumericUpDown.TabIndex = 6;
            this.selectedScenarioNumericUpDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.selectedScenarioNumericUpDown.ValueChanged += new System.EventHandler(this.selectedScenarioNumericUpDown_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = Labels.MultiScenarioResult_InitializeComponent_Scenario__;
            // 
            // MultiScenarioResult
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainer1);
            this.Name = "MultiScenarioResult";
            this.Size = new System.Drawing.Size(881, 463);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.selectedScenarioNumericUpDown)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label scenarioCountLabel;
        private System.Windows.Forms.NumericUpDown selectedScenarioNumericUpDown;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel containerPanel;
        private System.Windows.Forms.Button exportButton;
        private System.Windows.Forms.CheckBox includeResultsCheckBox;
        private TreeViewEx treeView;
    }
}
