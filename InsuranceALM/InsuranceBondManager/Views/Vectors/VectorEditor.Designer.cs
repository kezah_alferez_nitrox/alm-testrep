﻿using ALMSCommon.Controls;
using ALMSCommon.Controls.TreeGridView;
using InsuranceBondManager.Resources.Language;

namespace InsuranceBondManager.Views.Vectors
{
    partial class VectorEditor
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.okButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.zedGraphControl = new ZedGraph.ZedGraphControl();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.originComboBox = new System.Windows.Forms.ComboBox();
            this.importantCheckBox = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.typeComboBox = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.frequencyComboBox = new System.Windows.Forms.ComboBox();
            this.nameTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.valuesDataGridView = new ALMSCommon.Controls.DataGridViewEx();
            this.periodColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.valueColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VectorEditorcontextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.CopyValuetoAllCellsBelowtoolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CopyValuetoAllCellsBelowwithIncrementtoolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pasteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.valuesDataGridView)).BeginInit();
            this.VectorEditorcontextMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.okButton);
            this.panel1.Controls.Add(this.cancelButton);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 421);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1047, 57);
            this.panel1.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(4, 4);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(214, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "* Use left mouse button to drag graph points";
            // 
            // okButton
            // 
            this.okButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okButton.Enabled = false;
            this.okButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.okButton.Location = new System.Drawing.Point(793, 13);
            this.okButton.Name = "okButton";
            this.okButton.Padding = new System.Windows.Forms.Padding(5);
            this.okButton.Size = new System.Drawing.Size(116, 32);
            this.okButton.TabIndex = 9;
            this.okButton.Text = global::InsuranceBondManager.Resources.Language.Labels.VectorEditor_InitializeComponent_OK;
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Click += new System.EventHandler(this.OkButtonClick);
            // 
            // cancelButton
            // 
            this.cancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cancelButton.Location = new System.Drawing.Point(919, 13);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Padding = new System.Windows.Forms.Padding(5);
            this.cancelButton.Size = new System.Drawing.Size(116, 32);
            this.cancelButton.TabIndex = 8;
            this.cancelButton.Text = global::InsuranceBondManager.Resources.Language.Labels.VectorEditor_InitializeComponent_Cancel;
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.CancelButtonClick);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.zedGraphControl);
            this.splitContainer1.Panel1.Controls.Add(this.groupBox1);
            this.splitContainer1.Panel1MinSize = 268;
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.valuesDataGridView);
            this.splitContainer1.Size = new System.Drawing.Size(1047, 421);
            this.splitContainer1.SplitterDistance = 750;
            this.splitContainer1.TabIndex = 1;
            // 
            // zedGraphControl
            // 
            this.zedGraphControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.zedGraphControl.EditButtons = System.Windows.Forms.MouseButtons.Left;
            this.zedGraphControl.EditModifierKeys = System.Windows.Forms.Keys.None;
            this.zedGraphControl.IsEnableVEdit = true;
            this.zedGraphControl.Location = new System.Drawing.Point(0, 75);
            this.zedGraphControl.Name = "zedGraphControl";
            this.zedGraphControl.ScrollGrace = 0D;
            this.zedGraphControl.ScrollMaxX = 0D;
            this.zedGraphControl.ScrollMaxY = 0D;
            this.zedGraphControl.ScrollMaxY2 = 0D;
            this.zedGraphControl.ScrollMinX = 0D;
            this.zedGraphControl.ScrollMinY = 0D;
            this.zedGraphControl.ScrollMinY2 = 0D;
            this.zedGraphControl.Size = new System.Drawing.Size(750, 346);
            this.zedGraphControl.TabIndex = 1;
            this.zedGraphControl.ZoomButtons = System.Windows.Forms.MouseButtons.None;
            this.zedGraphControl.PointEditEvent += new ZedGraph.ZedGraphControl.PointEditHandler(this.ZedGraphControlPointEditEvent);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.originComboBox);
            this.groupBox1.Controls.Add(this.importantCheckBox);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.typeComboBox);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.frequencyComboBox);
            this.groupBox1.Controls.Add(this.nameTextBox);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(750, 75);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Details";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(531, 47);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(72, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Set origin on :";
            this.label5.Visible = false;
            // 
            // originComboBox
            // 
            this.originComboBox.Location = new System.Drawing.Point(0, 0);
            this.originComboBox.Name = "originComboBox";
            this.originComboBox.Size = new System.Drawing.Size(121, 21);
            this.originComboBox.TabIndex = 10;
            this.originComboBox.Visible = false;
            // 
            // importantCheckBox
            // 
            this.importantCheckBox.AutoSize = true;
            this.importantCheckBox.Location = new System.Drawing.Point(399, 46);
            this.importantCheckBox.Name = "importantCheckBox";
            this.importantCheckBox.Size = new System.Drawing.Size(132, 17);
            this.importantCheckBox.TabIndex = 7;
            this.importantCheckBox.Text = global::InsuranceBondManager.Resources.Language.Labels.VectorEditor_InitializeComponent_Important_Market_Data;
            this.importantCheckBox.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(229, 47);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Type :";
            // 
            // typeComboBox
            // 
            this.typeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.typeComboBox.FormattingEnabled = true;
            this.typeComboBox.Location = new System.Drawing.Point(272, 44);
            this.typeComboBox.Name = "typeComboBox";
            this.typeComboBox.Size = new System.Drawing.Size(121, 21);
            this.typeComboBox.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Frequency :";
            // 
            // frequencyComboBox
            // 
            this.frequencyComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.frequencyComboBox.FormattingEnabled = true;
            this.frequencyComboBox.Location = new System.Drawing.Point(82, 44);
            this.frequencyComboBox.Name = "frequencyComboBox";
            this.frequencyComboBox.Size = new System.Drawing.Size(136, 21);
            this.frequencyComboBox.TabIndex = 2;
            this.frequencyComboBox.SelectedIndexChanged += new System.EventHandler(this.FrequencyComboBoxSelectedIndexChanged);
            // 
            // nameTextBox
            // 
            this.nameTextBox.Location = new System.Drawing.Point(82, 17);
            this.nameTextBox.Name = "nameTextBox";
            this.nameTextBox.Size = new System.Drawing.Size(662, 20);
            this.nameTextBox.TabIndex = 1;
            this.nameTextBox.TextChanged += new System.EventHandler(this.NameTextBoxTextChanged);
            this.nameTextBox.Leave += new System.EventHandler(this.NameTextBoxLeave);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name :";
            // 
            // valuesDataGridView
            // 
            this.valuesDataGridView.AllowUserToAddRows = false;
            this.valuesDataGridView.AllowUserToDeleteRows = false;
            this.valuesDataGridView.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.valuesDataGridView.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
            this.valuesDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.valuesDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.periodColumn,
            this.valueColumn});
            this.valuesDataGridView.ContextMenuStrip = this.VectorEditorcontextMenuStrip;
            this.valuesDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.valuesDataGridView.Location = new System.Drawing.Point(0, 0);
            this.valuesDataGridView.Name = "valuesDataGridView";
            this.valuesDataGridView.RowHeadersWidth = 23;
            this.valuesDataGridView.Size = new System.Drawing.Size(293, 421);
            this.valuesDataGridView.TabIndex = 0;
            this.valuesDataGridView.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.ValuesDataGridViewCellValueChanged);
            this.valuesDataGridView.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.ValuesDataGridViewDataError);
            this.valuesDataGridView.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ValuesDataGridViewMouseDown);
            // 
            // periodColumn
            // 
            this.periodColumn.DataPropertyName = "Date";
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle1.Format = "dd/MM/yyyy";
            dataGridViewCellStyle1.NullValue = null;
            this.periodColumn.DefaultCellStyle = dataGridViewCellStyle1;
            this.periodColumn.HeaderText = global::InsuranceBondManager.Resources.Language.Labels.VectorEditor_InitializeComponent_Period;
            this.periodColumn.Name = "periodColumn";
            this.periodColumn.ReadOnly = true;
            this.periodColumn.Width = 80;
            // 
            // valueColumn
            // 
            this.valueColumn.DataPropertyName = "Value";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle2.Format = "N5";
            this.valueColumn.DefaultCellStyle = dataGridViewCellStyle2;
            this.valueColumn.HeaderText = global::InsuranceBondManager.Resources.Language.Labels.VectorEditor_InitializeComponent_Value;
            this.valueColumn.Name = "valueColumn";
            // 
            // VectorEditorcontextMenuStrip
            // 
            this.VectorEditorcontextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CopyValuetoAllCellsBelowtoolStripMenuItem,
            this.CopyValuetoAllCellsBelowwithIncrementtoolStripMenuItem,
            this.pasteToolStripMenuItem});
            this.VectorEditorcontextMenuStrip.Name = "VectorEditorcontextMenuStrip";
            this.VectorEditorcontextMenuStrip.Size = new System.Drawing.Size(312, 70);
            // 
            // CopyValuetoAllCellsBelowtoolStripMenuItem
            // 
            this.CopyValuetoAllCellsBelowtoolStripMenuItem.Name = "CopyValuetoAllCellsBelowtoolStripMenuItem";
            this.CopyValuetoAllCellsBelowtoolStripMenuItem.Size = new System.Drawing.Size(311, 22);
            this.CopyValuetoAllCellsBelowtoolStripMenuItem.Text = global::InsuranceBondManager.Resources.Language.Labels.VectorEditor_InitializeComponent_Copy_Value_to_All_Cells_Below;
            this.CopyValuetoAllCellsBelowtoolStripMenuItem.Click += new System.EventHandler(this.CopyValuetoAllCellsBelowtoolStripMenuItemClick);
            // 
            // CopyValuetoAllCellsBelowwithIncrementtoolStripMenuItem
            // 
            this.CopyValuetoAllCellsBelowwithIncrementtoolStripMenuItem.Name = "CopyValuetoAllCellsBelowwithIncrementtoolStripMenuItem";
            this.CopyValuetoAllCellsBelowwithIncrementtoolStripMenuItem.Size = new System.Drawing.Size(311, 22);
            this.CopyValuetoAllCellsBelowwithIncrementtoolStripMenuItem.Text = global::InsuranceBondManager.Resources.Language.Labels.VectorEditor_InitializeComponent_Copy_Value_to_All_Cells_Below_with_Increment;
            this.CopyValuetoAllCellsBelowwithIncrementtoolStripMenuItem.Click += new System.EventHandler(this.CopyValuetoAllCellsBelowwithIncrementtoolStripMenuItemClick);
            // 
            // pasteToolStripMenuItem
            // 
            this.pasteToolStripMenuItem.Name = "pasteToolStripMenuItem";
            this.pasteToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V)));
            this.pasteToolStripMenuItem.Size = new System.Drawing.Size(311, 22);
            this.pasteToolStripMenuItem.Text = global::InsuranceBondManager.Resources.Language.Labels.VectorEditor_InitializeComponent_Paste;
            this.pasteToolStripMenuItem.Click += new System.EventHandler(this.PasteToolStripMenuItemClick);
            // 
            // VectorEditor
            // 
            this.AcceptButton = this.okButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(1047, 478);
            this.ControlBox = false;
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
            this.Name = "VectorEditor";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Vector Editor";
            this.Activated += new System.EventHandler(this.VectorEditorActivated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.VectorEditorFormClosing);
            this.Load += new System.EventHandler(this.VectorEditorLoad);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.valuesDataGridView)).EndInit();
            this.VectorEditorcontextMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.GroupBox groupBox1;
        private ZedGraph.ZedGraphControl zedGraphControl;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox frequencyComboBox;
        private System.Windows.Forms.TextBox nameTextBox;
        private System.Windows.Forms.Label label1;
        private DataGridViewEx valuesDataGridView;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.DataGridViewTextBoxColumn periodColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn valueColumn;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox typeComboBox;
        private System.Windows.Forms.ContextMenuStrip VectorEditorcontextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem CopyValuetoAllCellsBelowtoolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem CopyValuetoAllCellsBelowwithIncrementtoolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pasteToolStripMenuItem;
        private System.Windows.Forms.CheckBox importantCheckBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox originComboBox;
    }
}