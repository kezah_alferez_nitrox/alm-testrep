using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using ALMSCommon;
using InsuranceBondManager.Calculations;
using InsuranceBondManager.Core;
using InsuranceBondManager.Domain;
using InsuranceBondManager.Properties;
using InsuranceBondManager.Resources.Language;
using Eu.AlterSystems.ASNetLib.Core;
using NHibernate.Linq;

namespace InsuranceBondManager.Views.Vectors
{
    public partial class VectorManager : SessionForm
    {
        private TrueSortableBindingList<Vector> vectorList;
        private System.Windows.Forms.ToolTip ToolTip1;
        public VectorManager()
        {
            this.InitializeComponent();

            this.valuesDataGridView.AutoGenerateColumns = false;
            this.vectorGrid.AutoGenerateColumns = false;
            this.columnVectorName.ValueType = typeof(string);
            this.columnVectorDate.ValueType = typeof(DateTime);
            this.vectorGrid.Sorted += this.VectorGridSorted;

            this.groupComboBox.DisplayMember = "Value";
            this.groupComboBox.ValueMember = "Key";
            ToolTip1 = new System.Windows.Forms.ToolTip();
            ToolTip1.SetToolTip(this.vectorFilter, "Filter vectors starting 3 letters. Show all by deleting filter value.");
        }

        void VectorGridSorted(object sender, EventArgs e)
        {
            this.OnSelectedVectorChange();
        }

        public void ReorderList()
        {
            if (Settings.Default.VectorOrderingColumn >= 0 && Settings.Default.VectorOrderingColumn < this.vectorGrid.Columns.Count)
                this.vectorGrid.Sort(this.vectorGrid.Columns[Settings.Default.VectorOrderingColumn],
                    Settings.Default.VectorOrderingAscending ? ListSortDirection.Ascending : ListSortDirection.Descending);

            this.VectorsListSelectedIndexChanged(null, null);
        }

        private void VectorManagerLoad(object sender, EventArgs e)
        {
            this.groupComboBox.DataSource = EnumHelper.ToList(typeof(VectorGroup));
        }

        private void ModifyButtonClick(object sender, EventArgs e)
        {
            this.ModifySelectedVector();
        }

        protected Vector GetSelectedVector()
        {
            if (this.vectorGrid.SelectedRows.Count != 1) return null;
            Vector vector = this.vectorGrid.SelectedRows[0].DataBoundItem as Vector;
            return vector;
        }

        private void ModifySelectedVector()
        {
            Vector vector = this.GetSelectedVector();

            if (vector == null) return;

            VectorEditor vectorEditor = new VectorEditor(vector.Id);
            Tools.ShowDialog(vectorEditor);
            this.RefreshVectors(vector);
        }

        private void NewButtonClick(object sender, EventArgs e)
        {
            Vector vector = null;
            VectorEditor vectorEditor = new VectorEditor();
            if (Tools.ShowDialog(vectorEditor) == DialogResult.OK)
            {
                vector = vectorEditor.GetVector();
                if (vector != null)
                {
                    vector = Vector.Find(this.Session, vector.Id);
                }
            }
            this.RefreshVectors(vector);
        }

        private void DeleteButtonClick(object sender, EventArgs e)
        {
            int countSelectedVectors = this.vectorGrid.SelectedRows.Count;

            if (countSelectedVectors <= 1 ||
                MessageBox.Show(Labels.VectorManager_deleteButton_Click_Are_you_sure_you_want_to_delete__ + this.vectorGrid.SelectedRows.Count + Labels.VectorManager_deleteButton_Click___vectors_, "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                object[] items = new object[this.vectorGrid.SelectedRows.Count];
                int i = 0;
                foreach (DataGridViewRow row in this.vectorGrid.SelectedRows)
                {
                    object item = row.DataBoundItem;
                    items[i] = item;
                    i++;
                }
                foreach (object item in items) //; .SelectedIndices)
                {
                    Vector vector = item as Vector;
                    if (vector == null) continue;

                    string varname = this.IsVectorUnique(vector.Name);
                    if (varname == string.Empty)
                    {
                        if (items.Length == 1)
                        {
                            if (MessageBox.Show(Labels.VectorManager_deleteButton_Click_Are_you_sure_you_want_to_delete_vector_ + vector.Name + " ?", Labels.VectorManager_deleteButton_Click_Delete, MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) == DialogResult.Yes)
                            {
                                this.vectorList.Remove(vector);
                                vector.Delete(this.Session);
                            }
                        }
                        else
                        {
                            this.vectorList.Remove(vector);
                            vector.Delete(this.Session);
                            MainForm.ModifiedScenarioTemplate = true;
                        }
                        MainForm.ModifiedValues = true;
                    }
                    else
                        MessageBox.Show(Labels.VectorManager_deleteButton_Click_Cannot_delete_vector_ + vector.Name + Labels.VectorManager_deleteButton_Click__because_it_is_in_use_by_variable_ + varname, Labels.VectorManager_deleteButton_Click_Error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private string IsVectorUnique(string vectorName)
        {
            string isVu = string.Empty;
            VariableValue[] values = VariableValue.FindAll(this.Session);
            foreach (VariableValue variableValue in values)
            {
                if (variableValue.Value != null)
                {
                    foreach (string operand in Tools.GetOperandsFromExpression(variableValue.Value))
                    {
                        if (vectorName == operand)
                        {
                            isVu = variableValue.Variable.Name;
                            break;
                        }

                    }
                    if (isVu != string.Empty)
                        break;
                }
            }
            return isVu;
        }

        private bool IsDuplicateVectorName(string vectorName)
        {
            Vector existingVector = Vector.FindByName(this.Session, vectorName);
            if (existingVector != null)
            {
                return true;
            }

            return false;
        }

        private void CopyButtonClick(object sender, EventArgs e)
        {
            Vector vector = this.GetSelectedVector();
            if (vector == null) return;

            Vector copy = vector.Copy();
            copy.CreationDate = DateTime.Today;
            int count = 1;
            string copyname = copy.Name;
            copy.Name = copyname + string.Format(Labels.VectorManager_copyButton_Click__Copy__0__, count);

            while (this.IsDuplicateVectorName(copy.Name))
            {
                count++;
                copy.Name = copyname + string.Format(Labels.VectorManager_copyButton_Click__Copy__0__, count);
            }

            this.vectorList.Add(copy);

            while (this.vectorGrid.SelectedRows.Count > 0) this.vectorGrid.SelectedRows[0].Selected = false;
            if (this.vectorGrid.Rows.Count > 0) this.vectorGrid.Rows[this.vectorGrid.Rows.Count - 1].Selected = true;
            copy.Create(this.Session);
            this.Session.Flush();

            MainForm.ModifiedScenarioTemplate = true;
            MainForm.ModifiedValues = true;
        }

        private void GroupComboBoxSelectedIndexChanged(object sender, EventArgs e)
        {
            this.TMI_MoveToWorkSpace.Visible = true;
            this.TMI_MarketDataVectors.Visible = true;
            this.TMI_MarketVolVectors.Visible = true;
            this.RefreshVectors();
            this.UpdateButtons();
            if (this.groupComboBox.SelectedIndex == 0)
                this.TMI_MoveToWorkSpace.Visible = false;
            if (this.groupComboBox.SelectedIndex == 1)
                this.TMI_MarketDataVectors.Visible = false;
            if (this.groupComboBox.SelectedIndex == 2)
                this.TMI_MarketVolVectors.Visible = false;
        }

        private void UpdateButtons()
        {
            this.newButton.Enabled = (VectorGroup)this.groupComboBox.SelectedValue == VectorGroup.Workspace;

            this.modifyButton.Enabled = this.deleteButton.Enabled = this.copyButton.Enabled = (this.vectorGrid.SelectedRows.Count > 0);
            this.modifyButton.Enabled = this.copyButton.Enabled = (this.vectorGrid.SelectedRows.Count == 1);
        }

        private void RefreshVectors(Vector vectorToSelect = null)
        {
            Cursor = Cursors.WaitCursor;
            this.DisposeSession();


            Vector[] vectors = this.Session.Query<Vector>().Where(v => v.Group == (VectorGroup)this.groupComboBox.SelectedValue).Where(v => v.Name.ToLower().Contains(vectorFilter.Text.ToLower())).ToArray();
            this.vectorList = new TrueSortableBindingList<Vector>(new List<Vector>(vectors));
            this.vectorGrid.DataSource = this.vectorList;

            if (this.vectorList.Count == 0)
            {
                this.valuesDataGridView.DataSource = null;
            }

            this.ReorderList();

            int selectedIndex = -1;
            if (vectorToSelect != null)
            {
                for (int index = 0; index < this.vectorGrid.Rows.Count; index++)
                {
                    DataGridViewRow row = this.vectorGrid.Rows[index];
                    row.Selected = row.DataBoundItem is Vector && ((Vector)row.DataBoundItem).Id == vectorToSelect.Id;
                    if (row.Selected)
                    {
                        selectedIndex = index;
                    }
                }
            }

            if (selectedIndex >= 0)
            {
                int displayedRowCount = this.vectorGrid.DisplayedRowCount(true);
                if (selectedIndex >= displayedRowCount + this.vectorGrid.FirstDisplayedScrollingRowIndex)
                {
                    this.vectorGrid.FirstDisplayedScrollingRowIndex = selectedIndex;
                }
            }
            Cursor = Cursors.Arrow;
        }

        private void VectorsListSelectedIndexChanged(object sender, EventArgs e)
        {
            this.OnSelectedVectorChange();
        }

        private void OnSelectedVectorChange()
        {
            this.UpdateButtons();

            Vector vector = this.GetSelectedVector();

            if (vector == null)
            {
                this.valuesDataGridView.DataSource = null;
                return;
            }

            //BindingList<VectorPoint> vectorPointList = new BindingList<VectorPoint>(new List<VectorPoint>(vector.VectorPoints));
            //this.valuesDataGridView.DataSource = vectorPointList;
            this.valuesDataGridView.Rows.Clear();//.DataSource = vectorPointList;

            for (int i = 0; i < vector.VectorPoints.Count; i++)
            {
                if (i == 0)
                    this.valuesDataGridView.Rows.Add(Labels.ResultModel_InitColumns_Opening_Balance, vector.VectorPoints[i].Value);
                else
                    this.valuesDataGridView.Rows.Add(vector.VectorPoints[i].Date, vector.VectorPoints[i].Value);
            }
        }

        private void VectorListBoxDoubleClick(object sender, EventArgs e)
        {
            this.ModifySelectedVector();
        }

        private void TmiMarketDataVectorsClick(object sender, EventArgs e)
        {
            Vector vector = this.GetSelectedVector();
            if (vector == null) return;

            vector.Group = VectorGroup.MarketData;
            this.RefreshVectors();
        }

        private void TmiMarketVolVectorsClick(object sender, EventArgs e)
        {
            Vector vector = this.GetSelectedVector();
            if (vector == null) return;

            vector.Group = VectorGroup.MarketVol;
            this.RefreshVectors();
        }

        private void TmiMoveToWorkSpaceClick(object sender, EventArgs e)
        {
            Vector vector = this.GetSelectedVector();
            if (vector == null) return;

            vector.Group = VectorGroup.Workspace;
            this.RefreshVectors();
        }

        private void ChangeColumnSort(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.ColumnIndex < 0) return;

            DataGridViewColumn column = this.vectorGrid.Columns[e.ColumnIndex];
            Settings.Default.VectorOrderingColumn = column.Index;
            Settings.Default.VectorOrderingAscending = this.vectorGrid.SortOrder != SortOrder.Descending;
        }

        private void CloseButtonClick(object sender, EventArgs e)
        {
            //PersistenceSession.Flush();
            this.SaveChangesInSession();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void vectorFilter_KeyUp(object sender, KeyEventArgs e)
        {
            if (vectorFilter.Text.Length == 0 || vectorFilter.Text.Length > 2)
                RefreshVectors();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            vectorFilter.Text = "";
            RefreshVectors();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            Vector vector = this.GetSelectedVector();
            if (vector == null) return;

            string stringStep = "1";
            int step = 0;
            if (Util.InputBox("Step in months", "Months:", ref stringStep) == DialogResult.OK)
            {
                int.TryParse(stringStep, out step);
            }
            else
            {
                this.Cursor = Cursors.Arrow;
                return;
            }

            string vectorFixedName = vector.Name;
            if (Util.InputBox("Vector start name (the software will digits after for each one)", "Vector start name:", ref vectorFixedName) != DialogResult.OK)
            {
                this.Cursor = Cursors.Arrow;
                return;
            }

            int i = 1;
            int offset = 1;
            int maxDuration = Param.GetMaxDuration(this.Session);
            while (i<maxDuration)
            {
                Vector copy = new Vector();
                copy.CreationDate = DateTime.Today;
                copy.Name = vectorFixedName + offset.ToString();
                copy.VectorPoints = new List<VectorPoint>(maxDuration);
                for (int j = 0; j < i; j++)
                {
                    VectorPoint vectorPoint = new VectorPoint();
                    vectorPoint.Date = vector.VectorPoints[j].Date;
                    vectorPoint.Value = 0;
                    vectorPoint.Vector = copy;
                    copy.VectorPoints.Add(vectorPoint);
                }
                for (int j = i; j < maxDuration; j++)
                {
                    VectorPoint vectorPoint = new VectorPoint();
                    vectorPoint.Date = vector.VectorPoints[j].Date;
                    vectorPoint.Value = vector.VectorPoints[j - i].Value;
                    copy.VectorPoints.Add(vectorPoint);
                }
                this.vectorList.Add(copy);
                copy.Create(this.Session);
                offset++;
                i += step;
            }
            
            this.Session.Flush();


            MainForm.ModifiedScenarioTemplate = true;
            MainForm.ModifiedValues = true;
            this.Cursor = Cursors.Arrow;
        }

    }
}