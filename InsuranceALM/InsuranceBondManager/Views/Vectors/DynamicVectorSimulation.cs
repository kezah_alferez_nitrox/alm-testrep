﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using InsuranceBondManager.Core;
using InsuranceBondManager.Domain;
using InsuranceBondManager.Resources.Language;
using ZedGraph;

namespace InsuranceBondManager.Views.Vectors
{
    public partial class DynamicVectorSimulation : SessionForm
    {
        private BindingList<VectorPoint> vectorPointList = new BindingList<VectorPoint>();
        private LineItem graphCurve;

        public DynamicVectorSimulation()
        {
            this.InitializeComponent();

            this.Load += this.DynamicVectorSimulation_Load;
        }

        private void DynamicVectorSimulation_Load(object sender, EventArgs e)
        {
            this.InitVector();

            this.LoadCategories();
            this.LoadChart();
            this.LoadGrid();
        }

        private void InitVector()
        {
            DateTime startDate = new DateTime(2010, 1, 1);
            for (int i = 0; i < 120; i++)
            {
                DateTime date = new DateTime(startDate.Ticks);
                date = date.AddMonths(i);

                double log = Math.Log(i+1);

                VectorPoint vectorPoint = new VectorPoint();
                vectorPoint.Date = date;
                vectorPoint.Value = (double) log;

                this.vectorPointList.Add(vectorPoint);
            }
        }

        private void LoadGrid()
        {
            this.valuesDataGridView.AutoGenerateColumns = false;
            valuesDataGridView.DataSource = vectorPointList;
        }

        private void LoadChart()
        {
            this.zedGraphControl.GraphPane.Title.IsVisible = false;
            this.zedGraphControl.GraphPane.Legend.IsVisible = false;
            this.zedGraphControl.GraphPane.XAxis.Type = AxisType.Date;
            this.zedGraphControl.GraphPane.XAxis.Title.IsVisible = false;
            this.zedGraphControl.GraphPane.YAxis.Title.IsVisible = false;
            this.zedGraphControl.GraphPane.YAxis.MajorGrid.IsVisible = true;

            InitializeGraph();
        }



        private void InitializeGraph()
        {
            GraphPane zedPane = this.zedGraphControl.GraphPane;

            zedPane.CurveList.Clear();

            PointPairList list = new PointPairList();
            foreach (VectorPoint vectorPoint in this.vectorPointList)
            {
                double x = new XDate(vectorPoint.Date);
                double y = (double)vectorPoint.Value;
                list.Add(x, y);
            }

            this.graphCurve = zedPane.AddCurve(Labels.DynamicVectorSimulation_InitializeGraph_Values, list, Color.DarkGreen);
            this.graphCurve.Symbol.Type = SymbolType.Circle;
            this.graphCurve.Symbol.Fill = new Fill(Color.White);
            this.graphCurve.Symbol.Size = 8;

            this.zedGraphControl.AxisChange();
            this.zedGraphControl.Invalidate();
        }

        private void LoadCategories()
        {
            Category root = Category.FindPortfolio(this.Session);

            TreeNode node = new TreeNode(root.Label);
            AddChildNodes(node, root);
            this.portfolioTreeView.Nodes.Add(node);

            this.portfolioTreeView.ExpandAll();
        }

        private static void AddChildNodes(TreeNode parentNode, Category parent)
        {
            foreach (Category category in parent.ChildrenOrdered)
            {
                TreeNode childNode = new TreeNode(category.Label);

                childNode.Tag = category;
                parentNode.Nodes.Add(childNode);
                AddChildNodes(childNode, category);
            }
        }
    }
}