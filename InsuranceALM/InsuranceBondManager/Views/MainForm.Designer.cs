﻿using InsuranceBondManager.Resources.Language;

namespace InsuranceBondManager.Views
{
    partial class MainForm
    {
        /// <summary>L
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.mainTabControl = new System.Windows.Forms.TabControl();
            this.balanceSheetTabPage = new System.Windows.Forms.TabPage();
            this.balance = new InsuranceBondManager.Views.BalanceSheet.Balance.Balance();
            this.parametersTabPage = new System.Windows.Forms.TabPage();
            this.parameters = new InsuranceBondManager.Views.BalanceSheet.Parameters.Parameters();
            this.resultTabPage = new System.Windows.Forms.TabPage();
            this.result = new InsuranceBondManager.Views.BalanceSheet.Result.Result();
            this.multiScenarioParameters = new System.Windows.Forms.TabPage();
            this.multiScenarioParameters1 = new InsuranceBondManager.Views.Generator.MultiScenarioParameters();
            this.mainToolStrip = new System.Windows.Forms.ToolStrip();
            this.newToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.openToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.saveToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.copyToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.pasteToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.vectorManagerToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.computeResultsStripButton = new System.Windows.Forms.ToolStripButton();
            this.multiScenarioToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.fontNameToolStripComboBox = new System.Windows.Forms.ToolStripComboBox();
            this.fontSizeToolStripComboBox = new System.Windows.Forms.ToolStripComboBox();
            this.searchToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.searchToolStripTextBox = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openWorkspacetoolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openLastToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveWorkspaceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveWorkSpaceAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsTemplateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clearAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.applyScenarioTemplate = new System.Windows.Forms.ToolStripMenuItem();
            this.saveScenarioTemplateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pasteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.selectAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.undoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.redoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vectorManagerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dynamicVectorsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.variableDefinitionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.otherItemsAssumptionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.taxAssumptionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.solvencyAdjustmentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gapAnalysisToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dividendToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dataInterfacesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.currenciesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.parametersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.recalculateALMIDsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.recalculateBookValuesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.redatevectorsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.recalculateAllBookValuesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sendLogsToMaintenanceTeamToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showInsuranceALMHelpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.betaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.betaSaveResultAsCSVToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.betaSaveResultFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.saveScenarioFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.openScenarioFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.workspaceOpenFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.workspaceSaveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.contextMenuStripNewPortfolio = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.picLogo = new System.Windows.Forms.PictureBox();
            this.mainTabControl.SuspendLayout();
            this.balanceSheetTabPage.SuspendLayout();
            this.parametersTabPage.SuspendLayout();
            this.resultTabPage.SuspendLayout();
            this.multiScenarioParameters.SuspendLayout();
            this.mainToolStrip.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // mainTabControl
            // 
            this.mainTabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mainTabControl.Controls.Add(this.balanceSheetTabPage);
            this.mainTabControl.Controls.Add(this.parametersTabPage);
            this.mainTabControl.Controls.Add(this.resultTabPage);
            this.mainTabControl.Controls.Add(this.multiScenarioParameters);
            this.mainTabControl.Location = new System.Drawing.Point(0, 52);
            this.mainTabControl.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.mainTabControl.Name = "mainTabControl";
            this.mainTabControl.SelectedIndex = 0;
            this.mainTabControl.Size = new System.Drawing.Size(1259, 399);
            this.mainTabControl.TabIndex = 1;
            this.mainTabControl.SelectedIndexChanged += new System.EventHandler(this.mainTabControl_SelectedIndexChanged);
            // 
            // balanceSheetTabPage
            // 
            this.balanceSheetTabPage.Controls.Add(this.balance);
            this.balanceSheetTabPage.Location = new System.Drawing.Point(4, 22);
            this.balanceSheetTabPage.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.balanceSheetTabPage.Name = "balanceSheetTabPage";
            this.balanceSheetTabPage.Padding = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.balanceSheetTabPage.Size = new System.Drawing.Size(1251, 373);
            this.balanceSheetTabPage.TabIndex = 0;
            this.balanceSheetTabPage.Text = global::InsuranceBondManager.Resources.Language.Labels.MainForm_InitializeComponent_Balance_Sheet;
            this.balanceSheetTabPage.UseVisualStyleBackColor = true;
            // 
            // balance
            // 
            this.balance.Dock = System.Windows.Forms.DockStyle.Fill;
            this.balance.Location = new System.Drawing.Point(2, 3);
            this.balance.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.balance.Name = "balance";
            this.balance.Size = new System.Drawing.Size(1247, 367);
            this.balance.TabIndex = 0;
            // 
            // parametersTabPage
            // 
            this.parametersTabPage.Controls.Add(this.parameters);
            this.parametersTabPage.Location = new System.Drawing.Point(4, 22);
            this.parametersTabPage.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.parametersTabPage.Name = "parametersTabPage";
            this.parametersTabPage.Padding = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.parametersTabPage.Size = new System.Drawing.Size(1251, 373);
            this.parametersTabPage.TabIndex = 1;
            this.parametersTabPage.Text = global::InsuranceBondManager.Resources.Language.Labels.MainForm_InitializeComponent_Parameters;
            this.parametersTabPage.UseVisualStyleBackColor = true;
            // 
            // parameters
            // 
            this.parameters.Dock = System.Windows.Forms.DockStyle.Fill;
            this.parameters.Location = new System.Drawing.Point(2, 3);
            this.parameters.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.parameters.Name = "parameters";
            this.parameters.Size = new System.Drawing.Size(1247, 367);
            this.parameters.TabIndex = 0;
            this.parameters.Load += new System.EventHandler(this.parameters_Load);
            // 
            // resultTabPage
            // 
            this.resultTabPage.Controls.Add(this.result);
            this.resultTabPage.Location = new System.Drawing.Point(4, 22);
            this.resultTabPage.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.resultTabPage.Name = "resultTabPage";
            this.resultTabPage.Padding = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.resultTabPage.Size = new System.Drawing.Size(1251, 373);
            this.resultTabPage.TabIndex = 2;
            this.resultTabPage.Text = global::InsuranceBondManager.Resources.Language.Labels.MainForm_InitializeComponent_Result;
            this.resultTabPage.UseVisualStyleBackColor = true;
            // 
            // result
            // 
            this.result.Dock = System.Windows.Forms.DockStyle.Fill;
            this.result.Location = new System.Drawing.Point(2, 3);
            this.result.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.result.Name = "result";
            this.result.Size = new System.Drawing.Size(1247, 367);
            this.result.TabIndex = 0;
            // 
            // multiScenarioParameters
            // 
            this.multiScenarioParameters.Controls.Add(this.multiScenarioParameters1);
            this.multiScenarioParameters.Location = new System.Drawing.Point(4, 22);
            this.multiScenarioParameters.Name = "multiScenarioParameters";
            this.multiScenarioParameters.Padding = new System.Windows.Forms.Padding(3);
            this.multiScenarioParameters.Size = new System.Drawing.Size(1251, 373);
            this.multiScenarioParameters.TabIndex = 3;
            this.multiScenarioParameters.Text = "Random Scenario Generator";
            this.multiScenarioParameters.UseVisualStyleBackColor = true;
            // 
            // multiScenarioParameters1
            // 
            this.multiScenarioParameters1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.multiScenarioParameters1.Location = new System.Drawing.Point(3, 3);
            this.multiScenarioParameters1.Name = "multiScenarioParameters1";
            this.multiScenarioParameters1.Size = new System.Drawing.Size(1245, 367);
            this.multiScenarioParameters1.TabIndex = 0;
            this.multiScenarioParameters1.Load += new System.EventHandler(this.multiScenarioParameters1_Load);
            // 
            // mainToolStrip
            // 
            this.mainToolStrip.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mainToolStrip.AutoSize = false;
            this.mainToolStrip.Dock = System.Windows.Forms.DockStyle.None;
            this.mainToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripButton,
            this.openToolStripButton,
            this.saveToolStripButton,
            this.toolStripSeparator1,
            this.toolStripButton4,
            this.copyToolStripButton,
            this.pasteToolStripButton,
            this.toolStripSeparator2,
            this.vectorManagerToolStripButton,
            this.computeResultsStripButton,
            this.multiScenarioToolStripButton,
            this.toolStripSeparator3,
            this.fontNameToolStripComboBox,
            this.fontSizeToolStripComboBox,
            this.searchToolStripButton,
            this.searchToolStripTextBox,
            this.toolStripLabel1});
            this.mainToolStrip.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.mainToolStrip.Location = new System.Drawing.Point(0, 24);
            this.mainToolStrip.Name = "mainToolStrip";
            this.mainToolStrip.Padding = new System.Windows.Forms.Padding(0, 2, 1, 0);
            this.mainToolStrip.Size = new System.Drawing.Size(1006, 25);
            this.mainToolStrip.TabIndex = 3;
            this.mainToolStrip.Text = "toolStrip1";
            this.mainToolStrip.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.mainToolStrip_ItemClicked);
            // 
            // newToolStripButton
            // 
            this.newToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.newToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("newToolStripButton.Image")));
            this.newToolStripButton.ImageTransparentColor = System.Drawing.Color.Black;
            this.newToolStripButton.Name = "newToolStripButton";
            this.newToolStripButton.Size = new System.Drawing.Size(23, 20);
            this.newToolStripButton.Text = global::InsuranceBondManager.Resources.Language.Labels.MainForm_InitializeComponent_New;
            this.newToolStripButton.Click += new System.EventHandler(this.NewPortfolioMenuClick);
            // 
            // openToolStripButton
            // 
            this.openToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.openToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("openToolStripButton.Image")));
            this.openToolStripButton.ImageTransparentColor = System.Drawing.Color.Black;
            this.openToolStripButton.Name = "openToolStripButton";
            this.openToolStripButton.Size = new System.Drawing.Size(23, 20);
            this.openToolStripButton.Text = "Open";
            this.openToolStripButton.Click += new System.EventHandler(this.openToolStripButton_Click);
            // 
            // saveToolStripButton
            // 
            this.saveToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.saveToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("saveToolStripButton.Image")));
            this.saveToolStripButton.ImageTransparentColor = System.Drawing.Color.Black;
            this.saveToolStripButton.Name = "saveToolStripButton";
            this.saveToolStripButton.Size = new System.Drawing.Size(23, 20);
            this.saveToolStripButton.Text = global::InsuranceBondManager.Resources.Language.Labels.MainForm_InitializeComponent_Save;
            this.saveToolStripButton.Click += new System.EventHandler(this.saveToolStripButton_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 23);
            // 
            // toolStripButton4
            // 
            this.toolStripButton4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton4.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton4.Image")));
            this.toolStripButton4.ImageTransparentColor = System.Drawing.Color.Black;
            this.toolStripButton4.Name = "toolStripButton4";
            this.toolStripButton4.Size = new System.Drawing.Size(23, 20);
            this.toolStripButton4.Text = global::InsuranceBondManager.Resources.Language.Labels.MainForm_InitializeComponent_Cut;
            // 
            // copyToolStripButton
            // 
            this.copyToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.copyToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("copyToolStripButton.Image")));
            this.copyToolStripButton.ImageTransparentColor = System.Drawing.Color.Black;
            this.copyToolStripButton.Name = "copyToolStripButton";
            this.copyToolStripButton.Size = new System.Drawing.Size(23, 20);
            this.copyToolStripButton.Text = global::InsuranceBondManager.Resources.Language.Labels.MainForm_InitializeComponent_Copy;
            this.copyToolStripButton.Click += new System.EventHandler(this.copyToolStripButton_Click);
            // 
            // pasteToolStripButton
            // 
            this.pasteToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.pasteToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("pasteToolStripButton.Image")));
            this.pasteToolStripButton.ImageTransparentColor = System.Drawing.Color.Black;
            this.pasteToolStripButton.Name = "pasteToolStripButton";
            this.pasteToolStripButton.Size = new System.Drawing.Size(23, 20);
            this.pasteToolStripButton.Text = global::InsuranceBondManager.Resources.Language.Labels.MainForm_InitializeComponent_Paste;
            this.pasteToolStripButton.Click += new System.EventHandler(this.pasteToolStripButton_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 23);
            // 
            // vectorManagerToolStripButton
            // 
            this.vectorManagerToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.vectorManagerToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("vectorManagerToolStripButton.Image")));
            this.vectorManagerToolStripButton.ImageTransparentColor = System.Drawing.Color.Black;
            this.vectorManagerToolStripButton.Name = "vectorManagerToolStripButton";
            this.vectorManagerToolStripButton.Size = new System.Drawing.Size(23, 20);
            this.vectorManagerToolStripButton.Text = global::InsuranceBondManager.Resources.Language.Labels.MainForm_InitializeComponent_Vector_Manager;
            this.vectorManagerToolStripButton.Click += new System.EventHandler(this.vectorManagerToolStripButton_Click);
            // 
            // computeResultsStripButton
            // 
            this.computeResultsStripButton.Image = global::InsuranceBondManager.Properties.Resources._48px_Gnome_accessories_calculator_svg;
            this.computeResultsStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.computeResultsStripButton.Name = "computeResultsStripButton";
            this.computeResultsStripButton.Size = new System.Drawing.Size(76, 20);
            this.computeResultsStripButton.Text = "&Calculate";
            this.computeResultsStripButton.Click += new System.EventHandler(this.computeResultsStripButton_Click);
            // 
            // multiScenarioToolStripButton
            // 
            this.multiScenarioToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("multiScenarioToolStripButton.Image")));
            this.multiScenarioToolStripButton.ImageTransparentColor = System.Drawing.Color.White;
            this.multiScenarioToolStripButton.Name = "multiScenarioToolStripButton";
            this.multiScenarioToolStripButton.Size = new System.Drawing.Size(164, 20);
            this.multiScenarioToolStripButton.Text = global::InsuranceBondManager.Resources.Language.Labels.MainForm_InitializeComponent_Multi_scenario_Simulation;
            this.multiScenarioToolStripButton.Click += new System.EventHandler(this.MultiScenarioToolStripButtonClick);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 23);
            // 
            // fontNameToolStripComboBox
            // 
            this.fontNameToolStripComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.fontNameToolStripComboBox.Name = "fontNameToolStripComboBox";
            this.fontNameToolStripComboBox.Size = new System.Drawing.Size(144, 23);
            this.fontNameToolStripComboBox.SelectedIndexChanged += new System.EventHandler(this.fontNameToolStripComboBox_SelectedIndexChanged);
            // 
            // fontSizeToolStripComboBox
            // 
            this.fontSizeToolStripComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.fontSizeToolStripComboBox.Items.AddRange(new object[] {
            "5",
            "5.5",
            "6",
            "6.5",
            "7",
            "7.5",
            "8",
            "8.5",
            "9",
            "10",
            "11",
            "12"});
            this.fontSizeToolStripComboBox.Name = "fontSizeToolStripComboBox";
            this.fontSizeToolStripComboBox.Size = new System.Drawing.Size(89, 23);
            this.fontSizeToolStripComboBox.SelectedIndexChanged += new System.EventHandler(this.fontSizeToolStripComboBox_SelectedIndexChanged);
            // 
            // searchToolStripButton
            // 
            this.searchToolStripButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.searchToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.searchToolStripButton.Image = global::InsuranceBondManager.Properties.Resources.find;
            this.searchToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.searchToolStripButton.Name = "searchToolStripButton";
            this.searchToolStripButton.Size = new System.Drawing.Size(23, 20);
            this.searchToolStripButton.Text = "Search Product";
            this.searchToolStripButton.Click += new System.EventHandler(this.SearchToolStripButtonClick);
            // 
            // searchToolStripTextBox
            // 
            this.searchToolStripTextBox.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.searchToolStripTextBox.BackColor = System.Drawing.SystemColors.HotTrack;
            this.searchToolStripTextBox.ForeColor = System.Drawing.SystemColors.Info;
            this.searchToolStripTextBox.Name = "searchToolStripTextBox";
            this.searchToolStripTextBox.Size = new System.Drawing.Size(100, 23);
            this.searchToolStripTextBox.TextBoxTextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.searchToolStripTextBox.ToolTipText = "Product description or ID";
            this.searchToolStripTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.SearchToolStripTextBoxKeyPress);
            this.searchToolStripTextBox.TextChanged += new System.EventHandler(this.SearchToolStripTextBoxTextChanged);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(121, 20);
            this.toolStripLabel1.Text = "Quick Product Search";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.menuStrip1.AutoSize = false;
            this.menuStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem,
            this.toolsToolStripMenuItem,
            this.optionsToolStripMenuItem,
            this.helpToolStripMenuItem,
            this.betaToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1055, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "balanceMenuStrip";
            this.menuStrip1.VisibleChanged += new System.EventHandler(this.MenuVisibleChanged);
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openWorkspacetoolStripMenuItem,
            this.openLastToolStripMenuItem,
            this.saveWorkspaceToolStripMenuItem,
            this.saveWorkSpaceAsToolStripMenuItem,
            this.saveAsTemplateToolStripMenuItem,
            this.clearAllToolStripMenuItem,
            this.toolStripSeparator7,
            this.newToolStripMenuItem,
            this.openToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.saveAsToolStripMenuItem,
            this.toolStripSeparator4,
            this.applyScenarioTemplate,
            this.saveScenarioTemplateToolStripMenuItem,
            this.toolStripSeparator5,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = global::InsuranceBondManager.Resources.Language.Labels.MainForm_InitializeComponent__File;
            // 
            // openWorkspacetoolStripMenuItem
            // 
            this.openWorkspacetoolStripMenuItem.Name = "openWorkspacetoolStripMenuItem";
            this.openWorkspacetoolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.openWorkspacetoolStripMenuItem.Size = new System.Drawing.Size(258, 22);
            this.openWorkspacetoolStripMenuItem.Text = global::InsuranceBondManager.Resources.Language.Labels.MainForm_InitializeComponent_Open_Workspace____;
            this.openWorkspacetoolStripMenuItem.Click += new System.EventHandler(this.openWorkspacetoolStripMenuItem_Click);
            // 
            // openLastToolStripMenuItem
            // 
            this.openLastToolStripMenuItem.Name = "openLastToolStripMenuItem";
            this.openLastToolStripMenuItem.Size = new System.Drawing.Size(258, 22);
            this.openLastToolStripMenuItem.Text = global::InsuranceBondManager.Resources.Language.Labels.MainForm_InitializeComponent_Open_Last;
            // 
            // saveWorkspaceToolStripMenuItem
            // 
            this.saveWorkspaceToolStripMenuItem.Name = "saveWorkspaceToolStripMenuItem";
            this.saveWorkspaceToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.saveWorkspaceToolStripMenuItem.Size = new System.Drawing.Size(258, 22);
            this.saveWorkspaceToolStripMenuItem.Text = global::InsuranceBondManager.Resources.Language.Labels.MainForm_InitializeComponent_Save_Workspace;
            this.saveWorkspaceToolStripMenuItem.Click += new System.EventHandler(this.saveWorkspaceToolStripMenuItem_Click);
            // 
            // saveWorkSpaceAsToolStripMenuItem
            // 
            this.saveWorkSpaceAsToolStripMenuItem.Name = "saveWorkSpaceAsToolStripMenuItem";
            this.saveWorkSpaceAsToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.S)));
            this.saveWorkSpaceAsToolStripMenuItem.Size = new System.Drawing.Size(258, 22);
            this.saveWorkSpaceAsToolStripMenuItem.Text = global::InsuranceBondManager.Resources.Language.Labels.MainForm_InitializeComponent_Save_WorkSpace_as____;
            this.saveWorkSpaceAsToolStripMenuItem.Click += new System.EventHandler(this.saveAsToolStripButton_Click);
            // 
            // saveAsTemplateToolStripMenuItem
            // 
            this.saveAsTemplateToolStripMenuItem.Name = "saveAsTemplateToolStripMenuItem";
            this.saveAsTemplateToolStripMenuItem.Size = new System.Drawing.Size(258, 22);
            this.saveAsTemplateToolStripMenuItem.Text = "Save as Template";
            this.saveAsTemplateToolStripMenuItem.Click += new System.EventHandler(this.SaveAsTemplate);
            // 
            // clearAllToolStripMenuItem
            // 
            this.clearAllToolStripMenuItem.Name = "clearAllToolStripMenuItem";
            this.clearAllToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.clearAllToolStripMenuItem.Size = new System.Drawing.Size(258, 22);
            this.clearAllToolStripMenuItem.Text = global::InsuranceBondManager.Resources.Language.Labels.MainForm_InitializeComponent_Clear_All;
            this.clearAllToolStripMenuItem.Click += new System.EventHandler(this.clearALLStripMenuItem_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(255, 6);
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.Size = new System.Drawing.Size(258, 22);
            this.newToolStripMenuItem.Text = global::InsuranceBondManager.Resources.Language.Labels.MainForm_InitializeComponent_New_Portfolio;
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(258, 22);
            this.openToolStripMenuItem.Text = global::InsuranceBondManager.Resources.Language.Labels.MainForm_InitializeComponent_Import_Portfolio;
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(258, 22);
            this.saveToolStripMenuItem.Text = global::InsuranceBondManager.Resources.Language.Labels.MainForm_InitializeComponent_Save;
            this.saveToolStripMenuItem.Visible = false;
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(258, 22);
            this.saveAsToolStripMenuItem.Text = global::InsuranceBondManager.Resources.Language.Labels.MainForm_InitializeComponent_Export_portfolio;
            this.saveAsToolStripMenuItem.Click += new System.EventHandler(this.saveAsToolStripMenuItem_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(255, 6);
            // 
            // applyScenarioTemplate
            // 
            this.applyScenarioTemplate.Name = "applyScenarioTemplate";
            this.applyScenarioTemplate.Size = new System.Drawing.Size(258, 22);
            this.applyScenarioTemplate.Text = global::InsuranceBondManager.Resources.Language.Labels.MainForm_InitializeComponent_Import_Scenario;
            this.applyScenarioTemplate.Click += new System.EventHandler(this.applyScenarioTemplate_Click);
            // 
            // saveScenarioTemplateToolStripMenuItem
            // 
            this.saveScenarioTemplateToolStripMenuItem.Name = "saveScenarioTemplateToolStripMenuItem";
            this.saveScenarioTemplateToolStripMenuItem.Size = new System.Drawing.Size(258, 22);
            this.saveScenarioTemplateToolStripMenuItem.Text = global::InsuranceBondManager.Resources.Language.Labels.MainForm_InitializeComponent_Export_Scenario;
            this.saveScenarioTemplateToolStripMenuItem.Click += new System.EventHandler(this.saveScenarioTemplateToolStripMenuItem_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(255, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q)));
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(258, 22);
            this.exitToolStripMenuItem.Text = global::InsuranceBondManager.Resources.Language.Labels.MainForm_InitializeComponent_E_xit;
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cutToolStripMenuItem,
            this.copyToolStripMenuItem,
            this.pasteToolStripMenuItem,
            this.toolStripSeparator6,
            this.selectAllToolStripMenuItem,
            this.toolStripSeparator8,
            this.undoToolStripMenuItem,
            this.redoToolStripMenuItem});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.editToolStripMenuItem.Text = global::InsuranceBondManager.Resources.Language.Labels.MainForm_InitializeComponent__Edit;
            this.editToolStripMenuItem.DropDownOpening += new System.EventHandler(this.editToolStripMenuItem_DropDownOpening);
            // 
            // cutToolStripMenuItem
            // 
            this.cutToolStripMenuItem.Name = "cutToolStripMenuItem";
            this.cutToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X)));
            this.cutToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.cutToolStripMenuItem.Text = global::InsuranceBondManager.Resources.Language.Labels.MainForm_InitializeComponent_Cut;
            // 
            // copyToolStripMenuItem
            // 
            this.copyToolStripMenuItem.Name = "copyToolStripMenuItem";
            this.copyToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this.copyToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.copyToolStripMenuItem.Text = global::InsuranceBondManager.Resources.Language.Labels.MainForm_InitializeComponent_Copy;
            this.copyToolStripMenuItem.Click += new System.EventHandler(this.copyToolStripMenuItem_Click);
            // 
            // pasteToolStripMenuItem
            // 
            this.pasteToolStripMenuItem.Name = "pasteToolStripMenuItem";
            this.pasteToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V)));
            this.pasteToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.pasteToolStripMenuItem.Text = global::InsuranceBondManager.Resources.Language.Labels.MainForm_InitializeComponent_Paste;
            this.pasteToolStripMenuItem.Click += new System.EventHandler(this.pasteToolStripMenuItem_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(161, 6);
            // 
            // selectAllToolStripMenuItem
            // 
            this.selectAllToolStripMenuItem.Enabled = false;
            this.selectAllToolStripMenuItem.Name = "selectAllToolStripMenuItem";
            this.selectAllToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A)));
            this.selectAllToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.selectAllToolStripMenuItem.Text = global::InsuranceBondManager.Resources.Language.Labels.MainForm_InitializeComponent_Select_All;
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(161, 6);
            // 
            // undoToolStripMenuItem
            // 
            this.undoToolStripMenuItem.Name = "undoToolStripMenuItem";
            this.undoToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Z)));
            this.undoToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.undoToolStripMenuItem.Text = "Undo";
            this.undoToolStripMenuItem.Click += new System.EventHandler(this.undoToolStripMenuItem_Click);
            // 
            // redoToolStripMenuItem
            // 
            this.redoToolStripMenuItem.Name = "redoToolStripMenuItem";
            this.redoToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Y)));
            this.redoToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.redoToolStripMenuItem.Text = "Redo";
            this.redoToolStripMenuItem.Click += new System.EventHandler(this.redoToolStripMenuItem_Click);
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.vectorManagerToolStripMenuItem,
            this.dynamicVectorsToolStripMenuItem,
            this.variableDefinitionToolStripMenuItem,
            this.otherItemsAssumptionsToolStripMenuItem,
            this.taxAssumptionsToolStripMenuItem,
            this.solvencyAdjustmentToolStripMenuItem,
            this.gapAnalysisToolStripMenuItem,
            this.dividendToolStripMenuItem,
            this.dataInterfacesToolStripMenuItem});
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.toolsToolStripMenuItem.Text = global::InsuranceBondManager.Resources.Language.Labels.MainForm_InitializeComponent__Tools;
            // 
            // vectorManagerToolStripMenuItem
            // 
            this.vectorManagerToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("vectorManagerToolStripMenuItem.Image")));
            this.vectorManagerToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Black;
            this.vectorManagerToolStripMenuItem.Name = "vectorManagerToolStripMenuItem";
            this.vectorManagerToolStripMenuItem.Size = new System.Drawing.Size(208, 22);
            this.vectorManagerToolStripMenuItem.Text = global::InsuranceBondManager.Resources.Language.Labels.MainForm_InitializeComponent_Static_Vectors___;
            this.vectorManagerToolStripMenuItem.Click += new System.EventHandler(this.vectorManagerToolStripMenuItem_Click);
            // 
            // dynamicVectorsToolStripMenuItem
            // 
            this.dynamicVectorsToolStripMenuItem.Name = "dynamicVectorsToolStripMenuItem";
            this.dynamicVectorsToolStripMenuItem.Size = new System.Drawing.Size(208, 22);
            this.dynamicVectorsToolStripMenuItem.Text = global::InsuranceBondManager.Resources.Language.Labels.MainForm_InitializeComponent_Dynamic_Vectors___;
            this.dynamicVectorsToolStripMenuItem.Click += new System.EventHandler(this.dynamicVectorsToolStripMenuItem_Click);
            // 
            // variableDefinitionToolStripMenuItem
            // 
            this.variableDefinitionToolStripMenuItem.Name = "variableDefinitionToolStripMenuItem";
            this.variableDefinitionToolStripMenuItem.Size = new System.Drawing.Size(208, 22);
            this.variableDefinitionToolStripMenuItem.Text = global::InsuranceBondManager.Resources.Language.Labels.MainForm_InitializeComponent_Variable_Definition____;
            this.variableDefinitionToolStripMenuItem.Click += new System.EventHandler(this.variableDefinitionToolStripMenuItem_Click);
            // 
            // otherItemsAssumptionsToolStripMenuItem
            // 
            this.otherItemsAssumptionsToolStripMenuItem.Name = "otherItemsAssumptionsToolStripMenuItem";
            this.otherItemsAssumptionsToolStripMenuItem.Size = new System.Drawing.Size(208, 22);
            this.otherItemsAssumptionsToolStripMenuItem.Text = global::InsuranceBondManager.Resources.Language.Labels.MainForm_InitializeComponent_Other_Items_Assumptions;
            this.otherItemsAssumptionsToolStripMenuItem.Click += new System.EventHandler(this.otherItemsAssumptionsToolStripMenuItem_Click);
            // 
            // taxAssumptionsToolStripMenuItem
            // 
            this.taxAssumptionsToolStripMenuItem.Name = "taxAssumptionsToolStripMenuItem";
            this.taxAssumptionsToolStripMenuItem.Size = new System.Drawing.Size(208, 22);
            this.taxAssumptionsToolStripMenuItem.Text = global::InsuranceBondManager.Resources.Language.Labels.MainForm_InitializeComponent_Tax_Assumptions;
            this.taxAssumptionsToolStripMenuItem.Click += new System.EventHandler(this.taxAssumptionsToolStripMenuItem_Click);
            // 
            // solvencyAdjustmentToolStripMenuItem
            // 
            this.solvencyAdjustmentToolStripMenuItem.Name = "solvencyAdjustmentToolStripMenuItem";
            this.solvencyAdjustmentToolStripMenuItem.Size = new System.Drawing.Size(208, 22);
            this.solvencyAdjustmentToolStripMenuItem.Text = global::InsuranceBondManager.Resources.Language.Labels.MainForm_InitializeComponent__Solvency_Adjustment;
            this.solvencyAdjustmentToolStripMenuItem.Click += new System.EventHandler(this.solvencyAdjustmentToolStripMenuItem_Click);
            // 
            // gapAnalysisToolStripMenuItem
            // 
            this.gapAnalysisToolStripMenuItem.Name = "gapAnalysisToolStripMenuItem";
            this.gapAnalysisToolStripMenuItem.Size = new System.Drawing.Size(208, 22);
            this.gapAnalysisToolStripMenuItem.Text = "Gap Analysis";
            this.gapAnalysisToolStripMenuItem.Click += new System.EventHandler(this.gapAnalysisToolStripMenuItem_Click);
            // 
            // dividendToolStripMenuItem
            // 
            this.dividendToolStripMenuItem.Name = "dividendToolStripMenuItem";
            this.dividendToolStripMenuItem.Size = new System.Drawing.Size(208, 22);
            this.dividendToolStripMenuItem.Text = global::InsuranceBondManager.Resources.Language.Labels.MainForm_InitializeComponent_Dividend;
            this.dividendToolStripMenuItem.Click += new System.EventHandler(this.dividendToolStripMenuItem_Click);
            // 
            // dataInterfacesToolStripMenuItem
            // 
            this.dataInterfacesToolStripMenuItem.Name = "dataInterfacesToolStripMenuItem";
            this.dataInterfacesToolStripMenuItem.Size = new System.Drawing.Size(208, 22);
            this.dataInterfacesToolStripMenuItem.Text = "External Data Interfaces";
            this.dataInterfacesToolStripMenuItem.Click += new System.EventHandler(this.dataInterfacesToolStripMenuItem_Click);
            // 
            // optionsToolStripMenuItem
            // 
            this.optionsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.currenciesToolStripMenuItem,
            this.parametersToolStripMenuItem,
            this.toolStripMenuItem1,
            this.recalculateALMIDsToolStripMenuItem,
            this.recalculateBookValuesToolStripMenuItem,
            this.redatevectorsToolStripMenuItem,
            this.recalculateAllBookValuesToolStripMenuItem,
            this.sendLogsToMaintenanceTeamToolStripMenuItem});
            this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
            this.optionsToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.optionsToolStripMenuItem.Text = global::InsuranceBondManager.Resources.Language.Labels.MainForm_InitializeComponent__Options;
            // 
            // currenciesToolStripMenuItem
            // 
            this.currenciesToolStripMenuItem.Name = "currenciesToolStripMenuItem";
            this.currenciesToolStripMenuItem.Size = new System.Drawing.Size(250, 22);
            this.currenciesToolStripMenuItem.Text = global::InsuranceBondManager.Resources.Language.Labels.MainForm_InitializeComponent__Currencies;
            this.currenciesToolStripMenuItem.Click += new System.EventHandler(this.currenciesToolStripMenuItem_Click);
            // 
            // parametersToolStripMenuItem
            // 
            this.parametersToolStripMenuItem.Name = "parametersToolStripMenuItem";
            this.parametersToolStripMenuItem.Size = new System.Drawing.Size(250, 22);
            this.parametersToolStripMenuItem.Text = global::InsuranceBondManager.Resources.Language.Labels.MainForm_InitializeComponent__Parameters;
            this.parametersToolStripMenuItem.Click += new System.EventHandler(this.parametersToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(247, 6);
            this.toolStripMenuItem1.Visible = false;
            // 
            // recalculateALMIDsToolStripMenuItem
            // 
            this.recalculateALMIDsToolStripMenuItem.Name = "recalculateALMIDsToolStripMenuItem";
            this.recalculateALMIDsToolStripMenuItem.Size = new System.Drawing.Size(250, 22);
            this.recalculateALMIDsToolStripMenuItem.Text = "&Recalculate Product IDs";
            this.recalculateALMIDsToolStripMenuItem.Visible = false;
            this.recalculateALMIDsToolStripMenuItem.Click += new System.EventHandler(this.recalculateALMIDsToolStripMenuItem_Click);
            // 
            // recalculateBookValuesToolStripMenuItem
            // 
            this.recalculateBookValuesToolStripMenuItem.Name = "recalculateBookValuesToolStripMenuItem";
            this.recalculateBookValuesToolStripMenuItem.Size = new System.Drawing.Size(250, 22);
            this.recalculateBookValuesToolStripMenuItem.Text = "Recalculate &Book Values";
            this.recalculateBookValuesToolStripMenuItem.Visible = false;
            this.recalculateBookValuesToolStripMenuItem.Click += new System.EventHandler(this.recalculateBookValuesToolStripMenuItem_Click);
            // 
            // redatevectorsToolStripMenuItem
            // 
            this.redatevectorsToolStripMenuItem.Name = "redatevectorsToolStripMenuItem";
            this.redatevectorsToolStripMenuItem.Size = new System.Drawing.Size(250, 22);
            this.redatevectorsToolStripMenuItem.Text = "Redate &vectors";
            this.redatevectorsToolStripMenuItem.Click += new System.EventHandler(this.redatevectorsToolStripMenuItem_Click);
            // 
            // recalculateAllBookValuesToolStripMenuItem
            // 
            this.recalculateAllBookValuesToolStripMenuItem.Name = "recalculateAllBookValuesToolStripMenuItem";
            this.recalculateAllBookValuesToolStripMenuItem.Size = new System.Drawing.Size(250, 22);
            this.recalculateAllBookValuesToolStripMenuItem.Text = "Recalculate All Book Values";
            this.recalculateAllBookValuesToolStripMenuItem.Click += new System.EventHandler(this.recalculateAllBookValuesToolStripMenuItem_Click);
            // 
            // sendLogsToMaintenanceTeamToolStripMenuItem
            // 
            this.sendLogsToMaintenanceTeamToolStripMenuItem.Name = "sendLogsToMaintenanceTeamToolStripMenuItem";
            this.sendLogsToMaintenanceTeamToolStripMenuItem.Size = new System.Drawing.Size(250, 22);
            this.sendLogsToMaintenanceTeamToolStripMenuItem.Text = "Send Logs To Maintenance Team";
            this.sendLogsToMaintenanceTeamToolStripMenuItem.Click += new System.EventHandler(this.sendLogsToMaintenanceTeamToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showInsuranceALMHelpToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = global::InsuranceBondManager.Resources.Language.Labels.MainForm_InitializeComponent__Help;
            // 
            // showInsuranceALMHelpToolStripMenuItem
            // 
            this.showInsuranceALMHelpToolStripMenuItem.Name = "showInsuranceALMHelpToolStripMenuItem";
            this.showInsuranceALMHelpToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.showInsuranceALMHelpToolStripMenuItem.Text = "Show Insurance ALM Help";
            this.showInsuranceALMHelpToolStripMenuItem.Click += new System.EventHandler(this.showInsuranceALMHelpToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.aboutToolStripMenuItem.Text = global::InsuranceBondManager.Resources.Language.Labels.MainForm_InitializeComponent_About;
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // betaToolStripMenuItem
            // 
            this.betaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.betaSaveResultAsCSVToolStripMenuItem});
            this.betaToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.betaToolStripMenuItem.ForeColor = System.Drawing.Color.DarkBlue;
            this.betaToolStripMenuItem.Name = "betaToolStripMenuItem";
            this.betaToolStripMenuItem.Size = new System.Drawing.Size(47, 20);
            this.betaToolStripMenuItem.Text = "&BETA";
            this.betaToolStripMenuItem.Visible = false;
            // 
            // betaSaveResultAsCSVToolStripMenuItem
            // 
            this.betaSaveResultAsCSVToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.betaSaveResultAsCSVToolStripMenuItem.Name = "betaSaveResultAsCSVToolStripMenuItem";
            this.betaSaveResultAsCSVToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.betaSaveResultAsCSVToolStripMenuItem.Text = "Save Result As CSV";
            this.betaSaveResultAsCSVToolStripMenuItem.Click +=new System.EventHandler(betaSaveResultAsCSVToolStripMenuItem_Click);
            // 
            // betaSaveResultFileDialog
            // 
            this.betaSaveResultFileDialog.DefaultExt = "csv";
            this.betaSaveResultFileDialog.Filter = "CSV files (*.csv)|*.csv|All files (*.*)|*.*";
            this.betaSaveResultFileDialog.FilterIndex = 1;
            this.betaSaveResultFileDialog.RestoreDirectory = true;
            // 
            // openFileDialog
            // 
            this.openFileDialog.DefaultExt = "alm";
            this.openFileDialog.Filter = "ALM File|*.alm|All files|*.*";
            // 
            // saveFileDialog
            // 
            this.saveFileDialog.DefaultExt = "alm";
            this.saveFileDialog.FileName = "New ALM";
            this.saveFileDialog.Filter = "ALM Files|*.alm|All files|*.*";
            // 
            // saveScenarioFileDialog
            // 
            this.saveScenarioFileDialog.DefaultExt = "sct";
            this.saveScenarioFileDialog.FileName = "New SCT";
            this.saveScenarioFileDialog.Filter = "SCT Files|*.sct|All files|*.*";
            // 
            // openScenarioFileDialog
            // 
            this.openScenarioFileDialog.DefaultExt = "sct";
            this.openScenarioFileDialog.Filter = "SCT Files|*.sct|All files|*.*";
            // 
            // workspaceOpenFileDialog
            // 
            this.workspaceOpenFileDialog.DefaultExt = "alw";
            this.workspaceOpenFileDialog.Filter = "Workspace Files|*.alw|All files|*.*";
            // 
            // workspaceSaveFileDialog
            // 
            this.workspaceSaveFileDialog.DefaultExt = "alw";
            this.workspaceSaveFileDialog.FileName = "New Workspace";
            this.workspaceSaveFileDialog.Filter = "Workspace Files|*.alw|All files|*.*";
            // 
            // contextMenuStripNewPortfolio
            // 
            this.contextMenuStripNewPortfolio.Name = "contextMenuStripNewPortfolio";
            this.contextMenuStripNewPortfolio.Size = new System.Drawing.Size(61, 4);
            // 
            // picLogo
            // 
            this.picLogo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.picLogo.BackColor = System.Drawing.Color.Transparent;
            this.picLogo.Location = new System.Drawing.Point(1043, 0);
            this.picLogo.Name = "picLogo";
            this.picLogo.Size = new System.Drawing.Size(216, 72);
            this.picLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picLogo.TabIndex = 4;
            this.picLogo.TabStop = false;
            this.picLogo.Click += new System.EventHandler(this.PicLogoClick);
            // 
            // MainForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.ClientSize = new System.Drawing.Size(1259, 451);
            this.Controls.Add(this.picLogo);
            this.Controls.Add(this.mainTabControl);
            this.Controls.Add(this.mainToolStrip);
            this.Controls.Add(this.menuStrip1);
            this.KeyPreview = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.Name = "MainForm";
            this.Text = "ALM Solutions";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.BalanceSheetFormClosing);
            this.Load += new System.EventHandler(this.BalanceSheet_Load);
            this.HelpRequested += new System.Windows.Forms.HelpEventHandler(this.BalanceSheet_HelpRequested);
            this.mainTabControl.ResumeLayout(false);
            this.balanceSheetTabPage.ResumeLayout(false);
            this.parametersTabPage.ResumeLayout(false);
            this.resultTabPage.ResumeLayout(false);
            this.multiScenarioParameters.ResumeLayout(false);
            this.mainToolStrip.ResumeLayout(false);
            this.mainToolStrip.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl mainTabControl;
        private System.Windows.Forms.TabPage parametersTabPage;
        private System.Windows.Forms.TabPage resultTabPage;
        private InsuranceBondManager.Views.BalanceSheet.Result.Result result;
        private InsuranceBondManager.Views.BalanceSheet.Parameters.Parameters parameters;
        private System.Windows.Forms.TabPage balanceSheetTabPage;
        public InsuranceBondManager.Views.BalanceSheet.Balance.Balance balance;
        private System.Windows.Forms.ToolStrip mainToolStrip;
        private System.Windows.Forms.ToolStripButton newToolStripButton;
        private System.Windows.Forms.ToolStripButton openToolStripButton;
        private System.Windows.Forms.ToolStripButton saveToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton toolStripButton4;
        private System.Windows.Forms.ToolStripButton copyToolStripButton;
        private System.Windows.Forms.ToolStripButton pasteToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton vectorManagerToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.ToolStripComboBox fontNameToolStripComboBox;
        private System.Windows.Forms.ToolStripComboBox fontSizeToolStripComboBox;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem applyScenarioTemplate;
        private System.Windows.Forms.ToolStripMenuItem saveScenarioTemplateToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pasteToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripMenuItem selectAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vectorManagerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem variableDefinitionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem otherItemsAssumptionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem taxAssumptionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dividendToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem optionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem currenciesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem parametersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog saveScenarioFileDialog;
        private System.Windows.Forms.OpenFileDialog openScenarioFileDialog;
        private System.Windows.Forms.ToolStripButton computeResultsStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripMenuItem openWorkspacetoolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveWorkspaceToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog workspaceOpenFileDialog;
        private System.Windows.Forms.SaveFileDialog workspaceSaveFileDialog;
        private System.Windows.Forms.ToolStripMenuItem clearAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openLastToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showInsuranceALMHelpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dynamicVectorsToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton multiScenarioToolStripButton;
        private System.Windows.Forms.ToolStripMenuItem solvencyAdjustmentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveWorkSpaceAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem recalculateALMIDsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem recalculateBookValuesToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripNewPortfolio;
        private System.Windows.Forms.ToolStripMenuItem saveAsTemplateToolStripMenuItem;
        private System.Windows.Forms.PictureBox picLogo;
        private System.Windows.Forms.ToolStripTextBox searchToolStripTextBox;
        private System.Windows.Forms.ToolStripButton searchToolStripButton;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripMenuItem dataInterfacesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem redatevectorsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem recalculateAllBookValuesToolStripMenuItem;
        private System.Windows.Forms.TabPage multiScenarioParameters;
        private Generator.MultiScenarioParameters multiScenarioParameters1;
        private System.Windows.Forms.ToolStripMenuItem gapAnalysisToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sendLogsToMaintenanceTeamToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripMenuItem undoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem redoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem betaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem betaSaveResultAsCSVToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog betaSaveResultFileDialog;
        
    }
}