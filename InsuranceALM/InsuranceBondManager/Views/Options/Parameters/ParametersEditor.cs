﻿using System;
using ALMSCommon;
using InsuranceBondManager.BusinessLogic;
using InsuranceBondManager.Core;
using InsuranceBondManager.Domain;
using log4net.Core;

namespace InsuranceBondManager.Views.Options.Parameters
{
    public partial class ParametersEditor : SessionForm
    {
        public ParametersEditor()
        {
            InitializeComponent();
            LoadParameters();
        }

        private void LoadParameters()
        {
            maxDurationNumericUpDown.Value = Param.GetMaxDuration(this.Session);
            RecoveryLagInMonths.Value = Param.GetRecoveryLagInMonths(this.Session);
            dividendComboBox.SelectedIndex = Param.GetDividendPaymentMonth(this.Session) - 1;
            autoBookPriceCheckBox.Checked = Param.GetAutoCalculateBookPrice(this.Session);
            optimiseForComboBox.SelectedIndex = Param.GetComputeOptimisationType(this.Session);
            perProductResultDetailsCheckBox.Checked = Param.GetPerProductResultDetails(this.Session);
            OCIManagementComboBox.SelectedIndex = Param.GetOCIManagementSelectedIndex(this.Session);
            confirmBeforeDelete.Checked = Param.GetConfirmBeforeDelete(this.Session);
            importDeletesMissingProducts.Checked = Param.GetImportDeletesMissingProducts(this.Session);
            linkSwaps2by2DuringImport.Checked = Param.GetLinkSwaps2by2(this.Session);
            computeAnalysisCategoriesCheckBox.Checked = Param.GetComputeAnalysisCategories(this.Session);
            debugLogLevelCheckBox.Checked = Param.GetDebugLogLevel(this.Session);
            originModelCheckbox.Checked = Param.GetOriginBasedOnStartdate(this.Session);
            dateValidationMsgCheckbox.Checked = Param.GetDateValidateMsgPopUp(this.Session);
        }
        
        private void SaveParameters()
        {
            Param.SetMaxDuration(this.Session, (int)this.maxDurationNumericUpDown.Value);
            Param.SetRecoveryLagInMonths(this.Session, (int)RecoveryLagInMonths.Value);
            Param.SetDividendPaymentMonth(this.Session, dividendComboBox.SelectedIndex + 1);
            Param.SetAutoCalculateBookPrice(this.Session, autoBookPriceCheckBox.Checked);
            Param.SetComputeOptimisationType(this.Session, optimiseForComboBox.SelectedIndex);
            Param.SetPerProductResultDetails(this.Session, perProductResultDetailsCheckBox.Checked);
            Param.SetOCIManagementSelectedIndex(this.Session, OCIManagementComboBox.SelectedIndex);
            Param.SetConfirmBeforeDelete(this.Session, confirmBeforeDelete.Checked);
            Param.SetImportDeletesMissingProducts(this.Session, importDeletesMissingProducts.Checked);
            Param.SetLinkSwaps2by2(this.Session,linkSwaps2by2DuringImport.Checked);
            Param.SetComputeAnalysisCategories(this.Session,computeAnalysisCategoriesCheckBox.Checked);
            Param.SetDebugLogLevel(this.Session, debugLogLevelCheckBox.Checked);
            Param.SetOriginBasedOnStartdate(this.Session,originModelCheckbox.Checked);
            Param.SetDateValidateMsgPopUp(this.Session, dateValidationMsgCheckbox.Checked);

            if (Param.GetDebugLogLevel(this.Session))
                Logger.SetLoggerLevel(Level.Info);
            else
                Logger.SetLoggerLevel(Level.Error);

            MainForm.ModifiedValues = true;
        }

        private void ButtonOkClick(object sender, EventArgs e)
        {
            SaveParameters();
            this.SaveChangesInSession();
            BalanceBL.EnsureSpecialCategoriesAndBondsExist();

            this.Close();
        }

        private void BtnCancelClick(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BrowseLogo(object sender, EventArgs e)
        {
            Param.SetLogo(this.Session, Tools.browseForLogo());
        }

        private void button1_Click(object sender, EventArgs e)
        {
            

        }

        private void DebugLogLevelCheckBox_CheckedChanged(object sender, EventArgs e)
        {

        }
    }
}
