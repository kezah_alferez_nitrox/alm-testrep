using ALMSCommon.Controls;
using ALMSCommon.Controls.TreeGridView;
using InsuranceBondManager.Resources.Language;

namespace InsuranceBondManager.Views.Options.FxRates
{
    partial class FxRatesEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.bottomPanel = new System.Windows.Forms.Panel();
            this.addButton = new System.Windows.Forms.Button();
            this.closeButton = new System.Windows.Forms.Button();
            this.fxRatesGroupBox = new System.Windows.Forms.GroupBox();
            this.currenciesGrid = new ALMSCommon.Controls.DataGridViewEx();
            this.gridContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.button1 = new System.Windows.Forms.Button();
            this.bottomPanel.SuspendLayout();
            this.fxRatesGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.currenciesGrid)).BeginInit();
            this.gridContextMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // bottomPanel
            // 
            this.bottomPanel.Controls.Add(this.button1);
            this.bottomPanel.Controls.Add(this.addButton);
            this.bottomPanel.Controls.Add(this.closeButton);
            this.bottomPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bottomPanel.Location = new System.Drawing.Point(0, 182);
            this.bottomPanel.Name = "bottomPanel";
            this.bottomPanel.Size = new System.Drawing.Size(452, 57);
            this.bottomPanel.TabIndex = 1;
            // 
            // addButton
            // 
            this.addButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.addButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.addButton.Location = new System.Drawing.Point(3, 13);
            this.addButton.Name = "addButton";
            this.addButton.Padding = new System.Windows.Forms.Padding(5);
            this.addButton.Size = new System.Drawing.Size(116, 32);
            this.addButton.TabIndex = 10;
            this.addButton.Text = global::InsuranceBondManager.Resources.Language.Labels.FxRatesEditor_InitializeComponent_Add_new_currency;
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.AddButtonClick);
            // 
            // closeButton
            // 
            this.closeButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.closeButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.closeButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.closeButton.Location = new System.Drawing.Point(324, 13);
            this.closeButton.Name = "closeButton";
            this.closeButton.Padding = new System.Windows.Forms.Padding(5);
            this.closeButton.Size = new System.Drawing.Size(116, 32);
            this.closeButton.TabIndex = 9;
            this.closeButton.Text = global::InsuranceBondManager.Resources.Language.Labels.FxRatesEditor_InitializeComponent_Close;
            this.closeButton.UseVisualStyleBackColor = true;
            this.closeButton.Click += new System.EventHandler(this.CloseButtonClick);
            // 
            // fxRatesGroupBox
            // 
            this.fxRatesGroupBox.Controls.Add(this.currenciesGrid);
            this.fxRatesGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fxRatesGroupBox.Location = new System.Drawing.Point(0, 0);
            this.fxRatesGroupBox.Name = "fxRatesGroupBox";
            this.fxRatesGroupBox.Size = new System.Drawing.Size(452, 182);
            this.fxRatesGroupBox.TabIndex = 2;
            this.fxRatesGroupBox.TabStop = false;
            this.fxRatesGroupBox.Text = "Variables";
            // 
            // currenciesGrid
            // 
            this.currenciesGrid.AllowUserToAddRows = false;
            this.currenciesGrid.AllowUserToDeleteRows = false;
            this.currenciesGrid.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.currenciesGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.currenciesGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.currenciesGrid.Location = new System.Drawing.Point(3, 16);
            this.currenciesGrid.Name = "currenciesGrid";
            this.currenciesGrid.Size = new System.Drawing.Size(446, 163);
            this.currenciesGrid.TabIndex = 1;
            this.currenciesGrid.RowHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.CurrenciesGridRowHeaderMouseClick);
            // 
            // gridContextMenuStrip
            // 
            this.gridContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deleteToolStripMenuItem});
            this.gridContextMenuStrip.Name = "gridContextMenuStrip";
            this.gridContextMenuStrip.Size = new System.Drawing.Size(159, 26);
            // 
            // deleteToolStripMenuItem
            // 
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Delete)));
            this.deleteToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.deleteToolStripMenuItem.Text = global::InsuranceBondManager.Resources.Language.Labels.FxRatesEditor_InitializeComponent__Delete;
            this.deleteToolStripMenuItem.Click += new System.EventHandler(this.DeleteToolStripMenuItemClick);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(146, 13);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(161, 31);
            this.button1.TabIndex = 11;
            this.button1.Text = "Delete selected currencies";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // FxRatesEditor
            // 
            this.AcceptButton = this.closeButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.closeButton;
            this.ClientSize = new System.Drawing.Size(452, 239);
            this.ControlBox = false;
            this.Controls.Add(this.fxRatesGroupBox);
            this.Controls.Add(this.bottomPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "FxRatesEditor";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Currencies";
            this.Load += new System.EventHandler(this.CurrenciesLoad);
            this.bottomPanel.ResumeLayout(false);
            this.fxRatesGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.currenciesGrid)).EndInit();
            this.gridContextMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel bottomPanel;
        private System.Windows.Forms.Button closeButton;
        private System.Windows.Forms.GroupBox fxRatesGroupBox;
        private System.Windows.Forms.Button addButton;
        private DataGridViewEx currenciesGrid;
        private System.Windows.Forms.ContextMenuStrip gridContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
        private System.Windows.Forms.Button button1;
    }
}