using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Threading;
using ALMS.Products;
using ALMSCommon;
using ALMSCommon.Forms;
using ALMSCommon.Threading;
using InsuranceALM.Infrastructure.Data;
using InsuranceBondManager.Core;
using InsuranceBondManager.Domain;
using InsuranceBondManager.Properties;
using InsuranceBondManager.Resources.Language;

using Iesi.Collections.Generic;
using NHibernate;

namespace InsuranceBondManager.Calculations
{
    public class Simulation
    {
        private readonly bool isMonteCarlo;
        private readonly ExpressionParser parser = new ExpressionParser();

        private readonly IDictionary<Scenario, ScenarioData> scenarioDatas = new Dictionary<Scenario, ScenarioData>();

        private readonly IDictionary<string, Vector> allVectors = new Dictionary<string, Vector>(30,
                                                                                                 StringComparer.
                                                                                                     CurrentCultureIgnoreCase);

        private readonly IDictionary<string, Variable> allVariables = new Dictionary<string, Variable>(30,
                                                                                                       StringComparer.
                                                                                                           CurrentCultureIgnoreCase);
        private readonly IDictionary<string, DynamicVariable> allDynamicVariables = new Dictionary<string, DynamicVariable>(30,
                                                                                                       StringComparer.
                                                                                                           CurrentCultureIgnoreCase);

        private readonly ThreadSafeDictionary<string, CashflowProduct> allCashFlowProduct =
                                        new ThreadSafeDictionary<string, CashflowProduct>();

        private readonly Iesi.Collections.Generic.ISet<Vector> usedVectors = new HashedSet<Vector>();
        private readonly Iesi.Collections.Generic.ISet<Variable> usedVariables = new HashedSet<Variable>();
        private readonly Iesi.Collections.Generic.ISet<DynamicVariable> usedDynamicVariables = new HashedSet<DynamicVariable>();
        private readonly Iesi.Collections.Generic.ISet<Currency> usedCurrencies = new HashedSet<Currency>();

        private OtherItemAssum[] otherItemAssumptions;
        private Dividend[] dividends;
        private Category[] categories;
        private FXRate[] fxRates;
        private List<Scenario> selectedScenarios;
        private Currency valuationCurrency;
        private DateTime valuationDate;

        private readonly List<ValidationMessage> validationErrors = new List<ValidationMessage>();
        private readonly List<ValidationMessage> validationWarnings = new List<ValidationMessage>();

        private double standardTax;

        private Product[] products;
        private FundamentalVector[] fundamentalVectors;
        private DynamicVariable[] dynamicVariables;

        private readonly object locker = new object();
        int threadCount;

        public bool IsSuccesfull { get; private set; }

        private int currentScenarioIndex = -1;
        private Dictionary<string, DynamicVariableEvaluator> dynamicVariableEvaluators;
        private bool perProductResultDetails;
        private int resultSteps;

        private SolvencyAdjustment[] solvencyAdjustments;
        private readonly bool isComputationDaily = AlmConfiguration.IsComputationDaily();

        public List<ValidationMessage> ValidationErrors
        {
            get { return this.validationErrors; }
        }

        public List<ValidationMessage> ValidationWarnings
        {
            get { return this.validationWarnings; }
        }

        public Simulation()
            : this(false)
        {
        }

        public Simulation(bool isMonteCarlo)
        {
            this.isMonteCarlo = isMonteCarlo;
        }

        private void Initialize(ISession session)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo(Constants.CULTURE_NAME);

            this.IsSuccesfull = false;

            this.scenarioDatas.Clear();
            this.allVectors.Clear();
            this.allVariables.Clear();
            this.allDynamicVariables.Clear();
            this.usedVectors.Clear();
            this.usedVariables.Clear();
            this.usedDynamicVariables.Clear();
            this.usedCurrencies.Clear();
            this.validationErrors.Clear();
            this.validationWarnings.Clear();

            Vector[] vectors = Vector.FindAll(session);
            foreach (Vector vector in vectors)
            {
                NHibernateUtil.Initialize(vector.VectorPoints);
                this.allVectors.Add(vector.Name, vector);
            }

            Variable[] variables = Variable.FindAll(session);
            foreach (Variable variable in variables)
            {
                NHibernateUtil.Initialize(variable.VariableValues);
                this.allVariables.Add(variable.Name, variable);
            }

            this.fundamentalVectors =
                FundamentalVector.FindAll(session).OrderBy(fv => fv.Unit).ThenBy(fv => fv.Delta).ToArray();

            this.otherItemAssumptions = OtherItemAssum.FindAll(session);
            this.solvencyAdjustments = SolvencyAdjustment.FindAll(session);
            this.dividends = Dividend.FindAll(session);
            this.categories = Category.FindAll(session);

            foreach (Category category in this.categories)
            {
                InitializeCategory(category);
                    if (category.ALMID == 0)
                    {
                        category.ALMID = Category.GetNewALMID(session);
                        session.Flush();
                    }
            }

            this.fxRates = FXRate.FindAll(session);
            this.valuationCurrency = Param.GetValuationCurrency(session);
            this.valuationDate = Param.GetValuationDate(session) ?? DateTime.Today;

            // Pour appeler le get avant la sÚparation en threads
            int temp = Param.GetDividendPaymentMonth(session);

            this.products = Product.FindAllNotExcluded(session);

            this.selectedScenarios = new List<Scenario>(!this.isMonteCarlo
                                                            ? Scenario.FindSelected(session)
                                                            : Scenario.FindGenerated(session));

            foreach (Scenario scenario in this.selectedScenarios)
            {
                NHibernateUtil.Initialize(scenario.VariableValues);
            }

            this.dynamicVariables = DynamicVariable.FindAll(session);

            foreach (DynamicVariable variable in dynamicVariables)
            {
                this.allDynamicVariables.Add(variable.Name, variable);
            }

            this.perProductResultDetails = Param.GetPerProductResultDetails(session);

            this.resultSteps = Param.ResultMonths(session);
            if (this.isComputationDaily)
            {
                this.resultSteps = (int) ((this.valuationDate.AddDays(-1).AddMonths(this.resultSteps).AddDays(1) - this.valuationDate).TotalDays + 2);
            }
        }

        private static void InitializeCategory(Category category)
        {
            NHibernateUtil.Initialize(category.Children);

            foreach (Category child in category.Children)
            {
                InitializeCategory(child);
            }
        }

        private Dictionary<int, CategoryData> CreateCategoryDatas(Category[] categories)
        {
            Dictionary<int, CategoryData> datas = new Dictionary<int, CategoryData>();

            foreach (Category category in categories)
            {
                //datas[category.ALMID]=new CategoryData(resultSteps));
                datas.Add(category.ALMID, new CategoryData(resultSteps));
            }
            return datas;
        }

        private IList<ProductData> CreateProductDatas(IList<IFundamentalVector> fundamentalVectorDatas, ICouponExpressionEvaluator valorisationEvaluator, IFormulaEvaluator standardEvaluator, IDiscountCurveCollection discountCurves,
                                                      bool isFirstScenario, Product[] limitToProducts, bool isDaily,bool originStartDateForModel)
        {
            IList<ProductData> datas = new List<ProductData>();
            foreach (Product product in limitToProducts)
            {
                bool shouldupdateBookPrice = isFirstScenario && !this.isMonteCarlo;
                DateTime startDate = product.StartDate.GetValueOrDefault(this.valuationDate).AddDays(-1);

                int startStep = product.ComputeStartStep(valuationDate, isDaily);

                ProductData productData = product.Attrib == ProductAttrib.Roll
                                              ? new RollProductData(product, startDate, this.valuationDate, startStep,
                                                                    fundamentalVectorDatas, this.parser,
                                                                    valorisationEvaluator, standardEvaluator, discountCurves, this.allCashFlowProduct,
                                                                    shouldupdateBookPrice, this.resultSteps, isDaily, originStartDateForModel)
                                              : new ProductData(product, startDate, this.valuationDate, startStep,
                                                                fundamentalVectorDatas, this.parser,
                                                                    valorisationEvaluator, standardEvaluator, discountCurves, this.allCashFlowProduct,
                                                                shouldupdateBookPrice, this.resultSteps, isDaily, originStartDateForModel);
                productData.Currency = productData.Product.Currency ?? this.valuationCurrency;

                double factor = (this.isComputationDaily ? 1.0 : 30.0);

                if ((product.Attrib == ProductAttrib.InFuture) ||
                 (product.Attrib == ProductAttrib.InBalance && (int)Math.Round((startDate - this.valuationDate).TotalDays / factor) > 0))
                {
                    productData.ImpactTreasuryStep = (int)Math.Round((startDate - this.valuationDate).TotalDays / factor) + 1;
                }

                datas.Add(productData);
            }

            foreach (ProductData productData in datas)
            {
                Product product = productData.Product;
                if (product.ProductType == ProductType.Swaption)
                {
                    productData.LinkedProductDatas = datas.Where(pd => product.LinkedProducts.Any(lp => lp == pd.Product)).ToList();
                }
            }

            return datas;
        }


        private IList<IFundamentalVector> CreateFundamentalVectorDatas(int steps)
        {
            IList<IFundamentalVector> datas = new List<IFundamentalVector>();
            foreach (FundamentalVector fundamentalVector in this.fundamentalVectors)
            {
                FundamentalVectorData fundamentalVectorData = new FundamentalVectorData(fundamentalVector, this.parser, steps);
                datas.Add(fundamentalVectorData);
            }

            return datas;
        }

        public void Run(BackgroundWorker backgroundWorker, int stopAfterResultSteps = -1)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();

            using (new DebugDisabler())
            {
                using (ISession session = SessionManager.OpenSession())
                {
                    this.threadCount = Param.GetComputeOptimisationType(session) == 0 ?
                        Math.Min(Settings.Default.MaxSimulationThreads, Environment.ProcessorCount) : 1;

                    this.Initialize(session);

                    this.standardTax = Param.GetStandardTax(session);
                    SimulationParams parameters = new SimulationParams()
                        {
                            DividendPaymentMonth = Param.GetDividendPaymentMonth(session),
                            MaxDuration = Param.GetMaxDuration(session),
                            ResultSteps = resultSteps,
                            StopAfterResultMonths = stopAfterResultSteps,
                            RollEnabled = Param.GetRollEnabled(session),
                            ValuationCurrency = Param.GetValuationCurrency(session),
                            IsComputationDaily = AlmConfiguration.IsTreasuryManagementModule(),
                            ComputeGap = Param.GetComputeGap(session),
                            CashflowPresentation = Param.GetGapCashflowPresentation(session),
                            GapFixedRate = Param.GetGapFixedRate(session),
                            GapType = Param.GetGapType(session),
                            OriginForModel = Param.GetOriginBasedOnStartdate(session),
                        };
                   
                    if (!this.InitialValidation(session, parameters.ResultSteps, parameters.IsComputationDaily,parameters.OriginForModel))
                    {
                        return;
                    }


                    //MTU : stop old paralelisation causing fake results
                    //if (threadCount == 1)
                    //{
                    this.ThreadRun(new object[] { backgroundWorker, parameters });
                    //}
                    //else
                    //{
                    //    IList<Thread> threads = new List<Thread>();

                    //    for (int i = 0; i < threadCount; i++)
                    //    {
                    //        Thread thread = new Thread(this.ThreadRun);
                    //        thread.CurrentCulture = Thread.CurrentThread.CurrentCulture;
                    //        threads.Add(thread);
                    //        thread.Start(new object[] { backgroundWorker, parameters });
                    //    }

                    //    for (int i = 0; i < threadCount; i++)
                    //    {
                    //        threads[i].Join();
                    //    }
                    //}

                    this.IsSuccesfull = this.validationErrors.Count == 0 &&
                                        !backgroundWorker.CancellationPending;
                    session.Flush();
                }
            }

            Debug.WriteLine(stopwatch.ElapsedMilliseconds);

            GC.Collect();
            GC.WaitForPendingFinalizers();
        }

        private void ThreadRun(object arg)
        {
            object[] args = (object[])arg;
            BackgroundWorker backgroundWorker = (BackgroundWorker)args[0];
            while (true)
            {
                int scenarioIndex;
                lock (this.locker)
                {
                    this.currentScenarioIndex++;
                    scenarioIndex = this.currentScenarioIndex;
                }

                if (scenarioIndex >= this.selectedScenarios.Count || backgroundWorker.CancellationPending)
                {
                    break;
                }

                try
                {
                    LoadForm.Instance.SetTitle(string.Format(Labels.Simulation_ThreadRun_Running_scenario__0__of__1_, (scenarioIndex + 1), this.selectedScenarios.Count));
                    this.RunScenario(scenarioIndex, backgroundWorker, (SimulationParams)args[1]);
                    int finishedCount;
                    lock (this.locker)
                    {
                        finishedCount = this.scenarioDatas.Count;
                    }
                    backgroundWorker.ReportProgress(100 * finishedCount / this.selectedScenarios.Count);
                }
                catch (Exception ex)
                {
                    AggregateException aggregateException = ex as AggregateException;
                    if (aggregateException != null)
                    {
                        foreach (var innerException in aggregateException.InnerExceptions)
                        {
                            this.AddException(scenarioIndex, innerException);
                        }
                    }
                    else
                    {
                        this.AddException(scenarioIndex, ex);
                    }

                    Debug.Fail(ex.ToString());
                }
            }
        }

        private void AddException(int scenarioIndex, Exception ex)
        {
            SimulationException simulationException = ex as SimulationException;
            if (simulationException != null)
                this.AddValidationMessage(simulationException.ValidationMessage);
            else
                this.AddValidationMessage(new ValidationMessage(Labels.Simulation_ThreadRun_Scenario_ + scenarioIndex + ": " + ex));
        }

        private void RunScenario(int i, BackgroundWorker backgroundWorker, SimulationParams parameters)
        {
            Util.setDefaultCultureInfoToThread();
            Scenario scenario = this.selectedScenarios[i];

            IList<IFundamentalVector> fundamentalVectorDatas = this.CreateFundamentalVectorDatas(parameters.ResultSteps);

            Dictionary<string, object>[] vectorValues = new Dictionary<string, object>[this.resultSteps];
            Dictionary<string, double>[] variableValues = new Dictionary<string, double>[this.resultSteps];
            Dictionary<string, object>[] vectorAndVariableValues = new Dictionary<string, object>[this.resultSteps];
            Dictionary<string, double[]> namedVariableValues = new Dictionary<string, double[]>(32, StringComparer.CurrentCultureIgnoreCase);
            Dictionary<Currency, double[]> fxRateValues = new Dictionary<Currency, double[]>();

            Util.ValidateVariables(this.selectedScenarios, this.usedVariables, this.allVectors,
                                   this.usedVectors, this.validationErrors);
            
            if (this.validationErrors.Count > 0)
            {
                return;
            }

            Util.ComputeVectorValues(this.valuationDate, vectorValues, this.usedVectors, parameters.ResultSteps, parameters.IsComputationDaily);
            Util.ComputeVariableValues(scenario, this.parser, vectorValues, variableValues,
                                       this.usedVariables, this.validationErrors, parameters);

            if (this.validationErrors.Count > 0)
            {
                return;
            }

            Util.InitVariableAndVectorValues(vectorAndVariableValues, vectorValues, variableValues, dynamicVariables, namedVariableValues, parameters);

            this.ValuateFundamentalVectors(fundamentalVectorDatas, vectorAndVariableValues);
            if (this.validationErrors.Count > 0)
            {
                return;
            }

            this.ComputeFxRates(vectorAndVariableValues, fxRateValues);
            if (this.validationErrors.Count > 0)
            {
                return;
            }

            FormulaEvaluator standardEvaluator = new FormulaEvaluator(this.parser, this.valuationDate, vectorAndVariableValues);
            CouponExpressionEvaluator valorisationEvaluator = new CouponExpressionEvaluator(this.valuationDate, namedVariableValues, fundamentalVectorDatas, parameters.IsComputationDaily);

            DiscountCurveCollection discountCurves = ComputeDiscountCurves(fundamentalVectorDatas, vectorAndVariableValues, valorisationEvaluator, standardEvaluator);
            IList<ProductData> productDatas = this.CreateProductDatas(fundamentalVectorDatas, valorisationEvaluator, standardEvaluator, discountCurves, i == 0, this.products, parameters.IsComputationDaily,parameters.OriginForModel);
            Dictionary<int, CategoryData> categoryData = this.CreateCategoryDatas(this.categories);

            ScenarioData scenarioData = new ScenarioData(scenario, vectorAndVariableValues,
                                                         this.dynamicVariableEvaluators,
                                                         namedVariableValues,
                                                         fxRateValues, productDatas, categoryData,
                                                         this.categories, this.otherItemAssumptions,
                                                         this.solvencyAdjustments, this.dividends,
                                                         this.standardTax, discountCurves, this.valuationDate,
                                                         parameters,
                                                         true);
                                                           //this.selectedScenarios.Count == 1);

            Util.EvaluateDynamicVariables(0, productDatas.Distinct(new AlmIdProductDataComparer()).ToDictionary(
                    pd => pd.Product.ALMIDDisplayed, pd => pd), categoryData, scenarioData.dynamicVariableEvaluators, null, scenarioData.namedVariableValues, scenarioData.vectorAndVariableValues);

            this.ValuateProducts(productDatas, vectorAndVariableValues, namedVariableValues);

            if (this.validationErrors.Count > 0)
            {
                return;
            }

            this.ValuateOtherItemAssumptions(vectorAndVariableValues);
            if (this.validationErrors.Count > 0)
            {
                return;
            }

            this.ValuateSolvencyAdjustments(vectorAndVariableValues);
            if (this.validationErrors.Count > 0)
            {
                return;
            }

            scenarioData = new ScenarioData(scenario, vectorAndVariableValues,
                                            this.dynamicVariableEvaluators,
                                            namedVariableValues,
                                            fxRateValues, productDatas, categoryData,
                                            this.categories, this.otherItemAssumptions,
                                            this.solvencyAdjustments, this.dividends,
                                            this.standardTax, discountCurves, this.valuationDate, parameters,
                                            true);
                                                           //this.selectedScenarios.Count == 1);

            scenarioData.Compute(backgroundWorker);
            if (backgroundWorker.CancellationPending)
            {
                this.IsSuccesfull = false;
            }

            scenarioData.Clear(this.isMonteCarlo || !perProductResultDetails);

            lock (this.locker)
            {
                this.scenarioDatas.Add(scenario, scenarioData);
            }
        }

        private bool InitialValidation(ISession session,int steps, bool isDaily,bool originForModel)
        {
            this.EnsureSPecialProductsAttributes(session);
            
            IList<IFundamentalVector> fundamentalVectorDatas = this.CreateFundamentalVectorDatas(steps);

            IList<ProductData> productDatas = this.CreateProductDatas(fundamentalVectorDatas, null, null, null, false, this.products, isDaily, originForModel);
            
            this.ValidateProducts(productDatas);
            this.ValidateOia();
            this.ValidateDividends();
            this.ValidateFxRates();
            this.ValidateFundamentalVectors(fundamentalVectorDatas);
            this.ValidateSolvency();
            this.InitializeDynamicVariables();

            return this.validationErrors.Count == 0;
        }

        private void ValidateProducts(IEnumerable<ProductData> productDatas)
        {
            foreach (ProductData productData in productDatas)
            {
                if (productData.Product.Attrib == ProductAttrib.CashAdj)
                {
                    continue;
                }
                
                productData.ValidateBond(this.allVectors, this.allVariables, this.dynamicVariables);
                this.usedVariables.AddAll(productData.UsedVariables);
                this.usedVectors.AddAll(productData.UsedVectors);
                if (productData.Product.Currency != null)
                {
                    this.usedCurrencies.Add(productData.Product.Currency);
                }
                this.validationErrors.AddRange(productData.ValidationErrors);
            }
        }

        private void EnsureSPecialProductsAttributes(ISession session)
        {
            IList<Product> allProducts = Product.FindAll(session);
            foreach (Product prod in allProducts)
            {
                //for all non-Standard or Roll extend durationMonths to max durationMonths && force monthly 
                if (prod.Category.Type != CategoryType.Normal && prod.Category.Type != CategoryType.Roll)
                {
                    if (prod.Category.Type == CategoryType.Treasury ||
                        prod.Category.Type == CategoryType.IBNR ||
                        prod.Category.Type == CategoryType.PositiveDerivativesValuation ||
                        prod.Category.Type == CategoryType.NegativeDerivativesValuation ||
                        prod.Category.Type == CategoryType.ModelImpairments ||
                        prod.Category.Type == CategoryType.CashAdj ||
                        prod.Category.Type == CategoryType.Receivable ||
                        prod.Category.Type == CategoryType.Payable ||
                        prod.Category.Type == CategoryType.OtherComprehensiveIncome ||
                        prod.Category.Type == CategoryType.Reserves)
                    {
                        prod.Duration = Util.InfiniteDuration;
                        prod.AmortizationType=ProductAmortizationType.NO_MATURITY;
                        }
                    else
                        prod.Duration = resultSteps;

                    prod.Frequency = ProductFrequency.Monthly;

                    if (prod.Category.Type == CategoryType.Treasury)
                        prod.LiquidityRunOff = 1;
                }
            }
            session.Flush();
        }

        private void InitializeDynamicVariables()
        {
            string[] allVariableAndVectorNames = this.allVariables.Select(v => v.Key).Concat(allVectors.Select(v => v.Key)).ToArray();

            this.dynamicVariableEvaluators = Util.InitializeDynamicVariables(this.dynamicVariables,
                                                            allVariableAndVectorNames,
                                                            this.products.Select(p => p.ALMIDDisplayed).ToArray(),
                                                            this.categories.Select(c => c.ALMID).ToArray(),
                                                            this.validationErrors);

            foreach (KeyValuePair<string, DynamicVariableEvaluator> evaluator in dynamicVariableEvaluators)
            {
                string[] argNames = evaluator.Value.ArgNames;

                Variable[] variables = this.allVariables.Where(v => argNames.Contains(v.Key)).Select(v => v.Value).ToArray();
                this.usedVariables.AddAll(variables);

                Vector[] vectors = this.allVectors.Where(v => argNames.Contains(v.Key)).Select(v => v.Value).ToArray();
                this.usedVectors.AddAll(vectors);
            }
        }

        private void ValuateProducts(IEnumerable<ProductData> productDatas,
                                     IDictionary<string, object>[] vectorAndVariableValues,
                                     IDictionary<string, double[]> namedVariableValues)
        {
            foreach (ProductData productData in productDatas)
            {
                productData.ValuateProduct(vectorAndVariableValues, namedVariableValues);
                this.validationErrors.AddRange(productData.ValidationErrors);
                this.validationWarnings.AddRange(productData.ValidationWarnings);
            }
        }

        private void ValuateFundamentalVectors(IList<IFundamentalVector> fundamentalVectorDatas,
                                               IDictionary<string, object>[] vectorAndVariableValues)
        {
            foreach (FundamentalVectorData fundamentalVectorData in fundamentalVectorDatas)
            {
                fundamentalVectorData.Valuate(vectorAndVariableValues);
                this.validationErrors.AddRange(fundamentalVectorData.ValidationErrors);
            }
        }

        private void ValuateOtherItemAssumptions(IDictionary<string, object>[] vectorAndVariableValues)
        {
            foreach (OtherItemAssum oia in this.otherItemAssumptions)
            {
                oia.Values = new double[this.resultSteps];

                //MTU : As OIA is now computed each step for integrating DynVar into its formulas, just compute here step=0
                //for (int i = 0; i < this.resultSteps; i++)
                //{
                //    oia.Values[i] = this.GetComputedValue(oia.Vect, vectorAndVariableValues[i]);
                //}
                oia.Values[0] = this.GetComputedValue(oia.Vect, vectorAndVariableValues[0]);
            }
        }

        


        private void ValuateSolvencyAdjustments(IDictionary<string, object>[] vectorAndVariableValues)
        {
            foreach (SolvencyAdjustment adjustment in this.solvencyAdjustments)
            {
                adjustment.Values = new double[this.resultSteps];

                for (int i = 0; i < this.resultSteps; i++)
                {
                    adjustment.Values[i] = this.GetComputedValue(adjustment.Vector, vectorAndVariableValues[i]);
                }
            }
        }

        private double GetComputedValue(string value, IDictionary<string, object> variables)
        {
            double tempDecimal;
            if (!double.TryParse(value, out tempDecimal))
            {
                tempDecimal = this.ComputeExpression(value, variables);
            }
            return tempDecimal;
        }

        private double ComputeExpression(string expression, IDictionary<string, object> variables)
        {
            double expressionValue = 0;
            try
            {
                expressionValue = this.parser.Evaluate(expression, variables);
            }
            catch (Exception ex)
            {
                Log.Warn(ex);
                this.AddValidationMessage(
                    new ValidationMessage(string.Format(Labels.Simulation_ComputeExpression__0__in_expression___1__, ex.Message, expression)));
            }

            return expressionValue;
        }

        private void ComputeFxRates(IDictionary<string, object>[] vectorAndVariableValues,
                                    IDictionary<Currency, double[]> fxRateValues)
        {
            fxRateValues.Clear();

            fxRateValues[this.valuationCurrency] = new double[this.resultSteps];
            for (int i = 0; i < this.resultSteps; i++)
            {
                fxRateValues[this.valuationCurrency][i] = 1;
            }

            foreach (FXRate rate in this.fxRates)
            {
                if (this.IsRateNeeded(rate))
                {
                    fxRateValues[rate.From] = new double[this.resultSteps];

                    for (int i = 0; i < this.resultSteps; i++)
                    {
                        fxRateValues[rate.From][i] = this.GetComputedValue(rate.Value, vectorAndVariableValues[i]);
                    }
                }
            }
        }

        private void ComputeGapFixedRate(IDictionary<string, object>[] vectorAndVariableValues,
                                   IDictionary<Currency, double[]> fxRateValues)
        {
            fxRateValues.Clear();

            fxRateValues[this.valuationCurrency] = new double[this.resultSteps];
            for (int i = 0; i < this.resultSteps; i++)
            {
                fxRateValues[this.valuationCurrency][i] = 1;
            }

            foreach (FXRate rate in this.fxRates)
            {
                if (this.IsRateNeeded(rate))
                {
                    fxRateValues[rate.From] = new double[this.resultSteps];

                    for (int i = 0; i < this.resultSteps; i++)
                    {
                        fxRateValues[rate.From][i] = this.GetComputedValue(rate.Value, vectorAndVariableValues[i]);
                    }
                }
            }
        }

        private void ValidateSolvency()
        {
            foreach (SolvencyAdjustment adjustment in solvencyAdjustments)
            {
                if (Tools.IsDecimalString(adjustment.Vector))
                {
                    continue;
                }

                if (string.IsNullOrEmpty(adjustment.Vector))
                {
                    this.AddValidationMessage(new ValidationMessage(adjustment));
                }
                else
                {
                    string[] operands = Tools.GetOperandsFromExpression(adjustment.Vector);
                    foreach (string operand in operands)
                    {
                        if (Tools.IsDecimalString(operand))
                        {
                            continue;
                        }
                        if (this.allVectors.ContainsKey(operand))
                        {
                            this.usedVectors.Add(this.allVectors[operand]);
                            continue;
                        }
                        if (this.allVariables.ContainsKey(operand))
                        {
                            this.usedVariables.Add(this.allVariables[operand]);
                            continue;
                        }

                        this.AddValidationMessage(new ValidationMessage(adjustment));
                    }
                }
            }
        }

        private void ValidateOia()
        {
            foreach (OtherItemAssum assum in this.otherItemAssumptions)
            {
                if (Tools.IsDecimalString(assum.Vect))
                {
                    continue;
                }

                if (string.IsNullOrEmpty(assum.Vect))
                {
                    this.AddValidationMessage(new ValidationMessage(assum));
                }
                else
                {
                    string[] operands = Tools.GetOperandsFromExpression(assum.Vect);
                    foreach (string operand in operands)
                    {
                        if (Tools.IsDecimalString(operand))
                        {
                            continue;
                        }
                        if (this.allVectors.ContainsKey(operand))
                        {
                            this.usedVectors.Add(this.allVectors[operand]);
                            continue;
                        }
                        if (this.allVariables.ContainsKey(operand))
                        {
                            this.usedVariables.Add(this.allVariables[operand]);
                            continue;
                        }
                        if (this.allDynamicVariables.ContainsKey(operand))
                        {
                            this.usedDynamicVariables.Add(this.allDynamicVariables[operand]);
                            continue;
                        }
                        this.AddValidationMessage(new ValidationMessage(assum));
                    }
                }
            }
        }

        private bool IsFormulaValid(string formula)
        {
            if (Tools.IsDecimalString(formula))
            {
                return true;
            }

            string[] operands = Tools.GetOperandsFromExpression(formula);
            foreach (string operand in operands)
            {
                if (Tools.IsDecimalString(operand))
                {
                    continue;
                }
                if (this.allVectors.ContainsKey(operand))
                {
                    this.usedVectors.Add(this.allVectors[operand]);
                    continue;
                }
                if (this.allVariables.ContainsKey(operand))
                {
                    this.usedVariables.Add(this.allVariables[operand]);
                    continue;
                }

                return false;
            }

            return true;
        }

        private void ValidateFundamentalVectors(IList<IFundamentalVector> fundamentalVectorDatas)
        {
            foreach (FundamentalVectorData fundamentalVectorData in fundamentalVectorDatas)
            {
                fundamentalVectorData.Validate(this.allVectors, this.allVariables, this.usedVectors, this.usedVariables);
                this.validationErrors.AddRange(fundamentalVectorData.ValidationErrors);
            }
        }

        private void ValidateDividends()
        {
            foreach (Dividend dividend in this.dividends)
            {
                double temp;
                if (!double.TryParse(dividend.Amount, out temp))
                {
                    if (this.allVectors.ContainsKey(dividend.Amount))
                    {
                        this.usedVectors.Add(this.allVectors[dividend.Amount]);
                    }
                    else
                    {
                        this.AddValidationMessage(new ValidationMessage(dividend));
                    }
                }
            }
        }

        private void ValidateFxRates()
        {
            foreach (FXRate rate in this.fxRates)
            {
                if (this.IsRateNeeded(rate) && !this.IsFormulaValid(rate.Value))
                {
                    this.AddValidationMessage(new ValidationMessage(rate));
                }
            }

            foreach (Currency usedCurrency in this.usedCurrencies)
            {
                if (usedCurrency.Id == this.valuationCurrency.Id)
                {
                    continue;
                }

                if (!this.ExistsFxRateFrom(usedCurrency))
                {
                    this.AddValidationMessage(
                        new ValidationMessage(Labels.Simulation_ValidateFxRates_No_exchange_rate_for_ + usedCurrency.Symbol + "/" +
                                              this.valuationCurrency.Symbol));
                }
            }
        }

        private void AddValidationMessage(ValidationMessage message)
        {
            lock (this.validationErrors)
            {
                this.validationErrors.Add(message);
            }
        }

        private bool ExistsFxRateFrom(Currency currency)
        {
            foreach (FXRate rate in this.fxRates)
            {
                if (rate.From == currency)
                {
                    return true;
                }
            }
            return false;
        }

        private bool IsRateNeeded(FXRate rate)
        {
            return this.usedCurrencies.Contains(rate.From) && rate.To == this.valuationCurrency && rate.To != rate.From;
        }

        public ScenarioData this[Scenario scenario]
        {
            get { return this.scenarioDatas.ContainsKey(scenario) ? this.scenarioDatas[scenario] : null; }
        }

        public int Count
        {
            get { return this.scenarioDatas.Count; }
        }

        public ICollection<Scenario> Scenarios
        {
            get { return this.scenarioDatas.Keys; }
        }

        public DiscountCurveCollection ComputeDiscountCurves(IList<IFundamentalVector> fundamentalVectorDatas,
                                 IDictionary<string, object>[] vectorAndVariableValues, CouponExpressionEvaluator valorisationEvaluator, FormulaEvaluator standardEvaluator)
        {
            DiscountCurve[] discountCurves = new DiscountCurve[this.resultSteps];
            for (int step = 0; step < discountCurves.Length; step++)
            {
                DateTime date = isComputationDaily ? this.valuationDate.AddDays(step).AddDays(-1) : this.valuationDate.AddMonths(step).AddDays(-1);

                DiscountCurve discountCurve = new DiscountCurve(date,
                                                                valorisationEvaluator,
                                                                standardEvaluator,
                                                                fundamentalVectorDatas,
                                                                allCashFlowProduct);
                discountCurve.ComputeZc(vectorAndVariableValues[step]);
                discountCurves[step] = discountCurve;
            }
            return new DiscountCurveCollection(this.valuationDate, discountCurves);
            /*foreach (ProductData productData in productDatas)
            {
                productData.SetDiscountCurves(discountCurveCollection);
            }*/
        }
        //public XmlSchema GetSchema()
        //{
        //    return null;
        //}

        //public void ReadXml(XmlReader reader)
        //{
        //    reader.Read(); // Skip <Simulation>
        //    DictionarySerializer<Scenario, ScenarioData> serializer = new DictionarySerializer<Scenario, ScenarioData>();
        //    this.scenarioData = serializer.Deserialize(reader);
        //}

        //public void WriteXml(XmlWriter writer)
        //{
        //    DictionarySerializer<Scenario, ScenarioData> serializer = new DictionarySerializer<Scenario, ScenarioData>();
        //    serializer.Serialize(this.scenarioData, writer);
        //}
    }
}

