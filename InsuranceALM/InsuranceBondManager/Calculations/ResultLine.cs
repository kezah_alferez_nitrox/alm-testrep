﻿using System;
using System.Collections.Generic;
using InsuranceBondManager.Core;

namespace InsuranceBondManager.Calculations
{
    public class ResultLine
    {
        public ResultLine(string id, string label, FormattingType type)
        {
            this.Id = id;
            this.Label = label;
            this.Type = type;
            this.Children = new List<ResultLine>();
            this.Formatting = "N2";
        }

        public string Id { get; set; }
        public string Label { get; set; }
        public double[] Values { get; set; }
        public string Formatting { get; set; }
        public FormattingType Type { get; set; }
        public IList<ResultLine> Children { get; set; }
        public Func<ScenarioData,int, double> Function { get; set; }

        public double this[int t]
        {
            get { return Values[t]; }
        }

        public void Compute(ScenarioData scenarioData, int t)
        {
            if (this.Function == null) return;

            this.Values[t] = this.Function(scenarioData, t);
        }
    }
}