﻿using System;
using System.Collections.Generic;
using System.Linq;
using InsuranceALM.Infrastructure.Data;
using InsuranceBondManager.Core;
using InsuranceBondManager.Domain;
using InsuranceBondManager.Resources.Language;
using NHibernate;
using NHibernate.Linq;

namespace InsuranceBondManager.Calculations
{
    public class ResultData
    {
        public ResultData()
        {
            this.Lines = new List<ResultLine>();
            this.LineDictionary = new Dictionary<string, ResultLine>();
        }

        public IList<ResultLine> Lines { get; set; }

        public IDictionary<string, ResultLine> LineDictionary { get; set; }

        public void Initialize(Category[] categories, OtherItemAssum[] otherItemAssum, Dividend[] dividends)
        {
            this.Lines.Clear();

            this.CreateAllCategoriesLines(categories, ResultSection.BookValue);
            this.CreateAllCategoriesLines(categories, ResultSection.Income);
            this.CreateNetInterestIncome();
            this.CreateGainLosesOnFinancialInstrument();
            this.CreateOtherItemAssuptions(otherItemAssum);
            this.CreateNetIncomeBeforeImpairmentCharge();
            this.CreateImpairementData(categories);
            this.CreateImpairments();
            this.CreateIncomePreTax();
            this.CreateIncomeTaxBenefit();
            this.CreateNetIncome();
            this.CreateDividends(dividends);
            this.CreateRoe();
            this.CreateCostIcomeRatio();
            this.CreatePeriodicROA();
            this.CreateAllCategoriesLines(categories, ResultSection.Yield);
            this.CreateSolvencyRows();
            //this.CreateLcrRows();
        }

        private void InitializeLineDictionary(IEnumerable<ResultLine> lines)
        {
            foreach (ResultLine line in lines)
            {
                this.InitializeLineDictionary(line.Children);
                this.LineDictionary[line.Id] = line;
            }
        }

        public void InitializeValues(int count)
        {
            this.LineDictionary.Clear();
            this.InitializeLineDictionary(this.Lines);

            foreach (ResultLine line in this.LineDictionary.Values)
            {
                line.Values = new double[count];
            }
        }

        private void CreateLcrRows()
        {
            ResultLine liquidAssetsL1Line = new ResultLine("LCR - L1 Liquid assets",
                                                           Labels.ResultModel_CreateLcrRows_L1_Liquid_assets,
                                                           FormattingType.Header);
            liquidAssetsL1Line.Function = (sd, t) => sd.Lcr.LiquidAssetsL1[t];
            ResultLine liquidAssetsL2Line = new ResultLine("LCR -  L2 Liquid assets",
                                                           Labels.ResultModel_CreateLcrRows_L2_Liquid_assets,
                                                           FormattingType.Header);
            //liquidAssetsL2Line.Function = (sd, t) => sd.Lcr.LiquidAssetsL2[t];
            ResultLine liquidAssetsL2ALine = new ResultLine("LCR -  L2 Liquid assets",
                                                           Labels.ResultModel_CreateLcrRows_L2_Liquid_assets,
                                                           FormattingType.Header);
            ResultLine liquidAssetsL2BLine = new ResultLine("LCR -  L2 Liquid assets",
                                                           Labels.ResultModel_CreateLcrRows_L2_Liquid_assets,
                                                           FormattingType.Header);
            liquidAssetsL2Line.Children.Add(liquidAssetsL2ALine);
            liquidAssetsL2Line.Children.Add(liquidAssetsL2BLine);
            ResultLine liquidAssetsLine = new ResultLine("LCR -  Liquid assets",
                                                         Labels.ResultModel_CreateLcrRows_Liquid_assets,
                                                         FormattingType.Header);
            liquidAssetsLine.Function = (sd, t) => sd.Lcr.LiquidAssets[t];
            ResultLine netCashOutflowsInLine = new ResultLine("LCR -  Cash Inflows",
                                                              Labels.ResultModel_CreateLcrRows_Cash_Inflows,
                                                              FormattingType.Header);
            netCashOutflowsInLine.Function = (sd, t) => sd.Lcr.NetCashOutflowsIn[t];
            ResultLine netCashOutflowsOutLine = new ResultLine("LCR -  Cash Outflows",
                                                               Labels.ResultModel_CreateLcrRows_Cash_Outflows,
                                                               FormattingType.Header);
            netCashOutflowsOutLine.Function = (sd, t) => sd.Lcr.NetCashOutflowsOut[t];
            ResultLine netCashOutflowsLine = new ResultLine("LCR -  Net Cash Outflows",
                                                            Labels.ResultModel_CreateLcrRows_Net_Cash_Outflows,
                                                            FormattingType.Header);
            netCashOutflowsLine.Function = (sd, t) => sd.Lcr.NetCashOutflows[t];

            ResultLine liquidityCoverageRatioLine = new ResultLine("LCR -  Liquidity Coverage Ratio",
                               Labels.ResultModel_CreateLcrRows_Liquidity_Coverage_Ratio_, FormattingType.Header);
            netCashOutflowsLine.Function = (sd, t) => sd.Lcr.LiquidAssets[t] / sd.Lcr.NetCashOutflows[t];

            this.Lines.Add(liquidAssetsLine);
            liquidAssetsLine.Children.Add(liquidAssetsL1Line);
            liquidAssetsLine.Children.Add(liquidAssetsL2Line);
            this.Lines.Add(netCashOutflowsLine);
            netCashOutflowsLine.Children.Add(netCashOutflowsOutLine);
            netCashOutflowsLine.Children.Add(netCashOutflowsInLine);
            this.Lines.Add(liquidityCoverageRatioLine);
        }

        private void CreateSolvencyRows()
        {
            ResultLine line = new ResultLine("Solvency - RWA", Labels.ResultModel_CreateSolvencyRows_RWA,
                                             FormattingType.Header);
            line.Function = (sd, t) => sd.Solvency.Rwa[t];
            this.Lines.Add(line);

            line = new ResultLine("Solvency - Core Equity", Labels.ResultModel_CreateSolvencyRows_Core_Equity,
                                  FormattingType.Header);
            line.Function = (sd, t) => sd.Solvency.CoreEquity[t];
            this.Lines.Add(line);

            line = new ResultLine("Solvency - Tiers 1", Labels.ResultModel_CreateSolvencyRows_Tiers_1,
                                  FormattingType.Header);
            line.Function = (sd, t) => sd.Solvency.Tiers1[t];
            this.Lines.Add(line);

            line = new ResultLine("Solvency - Regulatory Capital",
                                  Labels.ResultModel_CreateSolvencyRows_Regulatory_Capital, FormattingType.Header);
            line.Function = (sd, t) => sd.Solvency.RegulatoryCapital[t];
            this.Lines.Add(line);

            line = new ResultLine("Solvency - Ratio on Core Equity",
                                  Labels.ResultModel_CreateSolvencyRows_Solvency_Ratio_on_Core_Equity,
                                  FormattingType.Header);
            line.Function = (sd, t) => sd.Solvency.SolvencyRatioOnCoreEquity[t];
            this.Lines.Add(line);

            line = new ResultLine("Solvency - Ratio Tiers 1",
                                  Labels.ResultModel_CreateSolvencyRows_Solvency_Ratio_Tiers_1, FormattingType.Header);
            line.Function = (sd, t) => sd.Solvency.SolvencyRatioTiers1[t];
            this.Lines.Add(line);

            line = new ResultLine("Solvency - Ratio", Labels.ResultModel_CreateSolvencyRows_Solvency_Ratio,
                                  FormattingType.Header);
            line.Function = (sd, t) => sd.Solvency.SolvencyRatio[t];
            this.Lines.Add(line);
        }

        private void CreatePeriodicROA()
        {
            ResultLine line = new ResultLine("Periodic ROA", Labels.ResultModel_CreatePeriodicROA_Periodic_ROA,
                                             FormattingType.Header);
            line.Function = (sd, t) => sd.GetNetIncome()[t] / sd.GetTotalAssets()[t] * 12;
            this.Lines.Add(line);
        }

        private void CreateCostIcomeRatio()
        {
            ResultLine line = new ResultLine("Cost Income Ratio",
                                             Labels.ResultModel_CreateCostIcomeRation_Cost_Income_Ratio,
                                             FormattingType.Header);
            line.Function = ComputeCostIcomeRatio;
            this.Lines.Add(line);
        }

        private double ComputeCostIcomeRatio(ScenarioData sd, int t)
        {
            double gainLosesOnFinancialInstrument = sd.GetGainLosesOnFinancialInstrument()[t];
            double netInterestIncome = sd.GetNetInterestIncome()[t];
            IDictionary<OtherItemAssum, double[]> oia = sd.GetOtherItemAssumptionsNotImpairments();

            double cost = oia.Where(pair => pair.Key.SubtotalGroup == OtherItemAssum.CostGroup).
                Sum(c => (double?)c.Value[t]).GetValueOrDefault();
            double commisions = oia.Where(pair => pair.Key.SubtotalGroup == OtherItemAssum.CommisionsAndOthersGroup).
                Sum(c => (double?)c.Value[t]).GetValueOrDefault();

            double sum = commisions + gainLosesOnFinancialInstrument + netInterestIncome;

            return Util.IsNotZero(sum) ? Math.Abs(cost / sum) : 0;
        }

        private void CreateDividends(IEnumerable<Dividend> dividends)
        {
            ResultLine line = new ResultLine("Dividends", Labels.ResultModel_CreateDividends_Dividends,
                                             FormattingType.Header);
            foreach (Dividend dividend in dividends)
            {
                ResultLine dividendLine = new ResultLine("Dividends - " + dividend.Name, dividend.Name,
                                                         FormattingType.Normal);
                Dividend closureDividend = dividend;
                dividendLine.Function = (sd, t) => sd.GetDividendAmounts()[closureDividend][t];
                line.Children.Add(dividendLine);
            }
            line.Function = (sd, t) => line.Children.Sum(c => (double?)c.Values[t]).GetValueOrDefault();
            this.Lines.Add(line);
        }

        private void CreateNetIncome()
        {
            ResultLine line = new ResultLine("Net Income", Labels.ResultModel_CreateNetIncome_Net_Income,
                                             FormattingType.Header);
            line.Function = (sd, t) => sd.GetNetIncome()[t];
            this.Lines.Add(line);
        }

        private void CreateIncomeTaxBenefit()
        {
            ResultLine line = new ResultLine("Income tax", Labels.ResultModel_CreateIncomeTaxBenefit_Income_tax_benefit____expense_,
                                             FormattingType.Header);
            line.Function = (sd, t) => sd.GetTaxes()[t];
            this.Lines.Add(line);
        }

        private void CreateIncomePreTax()
        {
            ResultLine line = new ResultLine("Income Pre Tax", Labels.ResultModel_CreateIncomePreTax_Income_Pre_Tax,
                                             FormattingType.Header);
            line.Function = (sd, t) => sd.GetIncomePreTax()[t];
            this.Lines.Add(line);
        }

        private void CreateRoe()
        {
            ResultLine line = new ResultLine("ROE", Labels.ResultModel_CreateRoe_ROE, FormattingType.Header);
            line.Function = (sd, t) => sd.GetNetIncome()[t] / sd.GetTotalEquity()[t];
            this.Lines.Add(line);
        }

        private void CreateImpairments()
        {
            ResultLine line = new ResultLine("Impairment Charge", Labels.ResultModel_CreateImpairments_Impairment_Charge,
                                             FormattingType.Header);
            ResultLine modelImpairementsline = new ResultLine("Model impairements", "Model impairements",
                                             FormattingType.Normal);

            modelImpairementsline.Function = (sd, t) => sd.GetImpairment()[t];
            line.Children.Add(modelImpairementsline);
            line.Function = (sd, t) => line.Children.Sum(c => (double?)c.Values[t]).GetValueOrDefault();
            this.Lines.Add(line);
        }

        private void CreateImpairementData(IEnumerable<Category> categories)
        {
            foreach (Category category in categories)
            {
                if (category.IsVisibleCheckedModel == true && AnyProductsAreModel(category))
                {
                    ResultLine line = new ResultLine("Impairment Charge - " + category.Label, category.Label,
                                                     FormattingType.Normal);
                    Category closureCategory = category;
                    line.Function = (sd, t) => sd.GetImpermentData(closureCategory)[t];
                    this.Lines.Add(line);
                }
            }
        }

        private static bool AnyProductsAreModel(Category category)
        {
            using (ISession session = SessionManager.OpenSession())  // todo CDU
            {
               return session.Query<Product>().Any(p => p.Category == category && p.IsModel);
            }
        }

        private void CreateNetIncomeBeforeImpairmentCharge()
        {
            ResultLine line =
                new ResultLine("Income before impairment charge",
                               Labels.ResultModel_CreateNetIncomeBeforeImpairmentCharge_Income_before_impairment_charge,
                               FormattingType.Header);
            line.Function = (sd, t) => sd.GetInterestIncomeBeforeImpairment()[t];
            this.Lines.Add(line);
        }

        private void CreateOtherItemAssuptions(OtherItemAssum[] oias)
        {
            string[] subTotalGroupNames = oias.Select(o => o.SubtotalGroup).Distinct().ToArray();
            bool costExists = subTotalGroupNames.Any(n => n == OtherItemAssum.CostGroup);
            bool commisionExists = subTotalGroupNames.Any(n => n == OtherItemAssum.CommisionsAndOthersGroup);

            IDictionary<string, int> subTotalGroups = new Dictionary<string, int>();
            foreach (string groupName in subTotalGroupNames)
            {
                if (string.IsNullOrEmpty(groupName)) continue;
                int position = 0;
                OtherItemAssum[] itemsInGroup = oias.Where(o => o.SubtotalGroup == groupName).ToArray();
                if (itemsInGroup != null && itemsInGroup.Length > 0)
                {
                    position = itemsInGroup.Min(oia => oia.Position);
                }
                subTotalGroups.Add(groupName, position);
            }

            subTotalGroupNames = subTotalGroupNames.Where(stg => !stg.Equals(OtherItemAssum.ImpairmentsRisk)).
                OrderBy(s => (!string.IsNullOrEmpty(s) && subTotalGroups.ContainsKey(s)) ? subTotalGroups[s] : int.MaxValue)
                .ToArray();

            foreach (string subTotalGroup in subTotalGroupNames)
            {
                if (subTotalGroup == OtherItemAssum.CostGroup)
                {
                    this.CreateNetBankingIncome();
                }

                OtherItemAssum[] groupOias = oias.Where(o => o.SubtotalGroup == subTotalGroup).
                    OrderBy(o => o.Position).ToArray();

                foreach (OtherItemAssum oia in groupOias)
                {
                    ResultLine line = new ResultLine("Other Item Assumptions - " + oia.Descrip, oia.Descrip,
                                                     FormattingType.Normal);

                    OtherItemAssum clojureOia = oia;
                    line.Function = (sd, t) => sd.GetOtherItemAssumptionsNotImpairments()[clojureOia][t];

                    if (oia.IsVisibleChecked == true)
                    {
                        this.Lines.Add(line);
                    }
                }

                if (subTotalGroup != null)
                {
                    ResultLine line = new ResultLine("Other Item Assumptions - " + subTotalGroup, subTotalGroup,
                                                     FormattingType.Header);
                    line.Function = (sd, t) => groupOias.Sum(o => (double?)sd.GetOtherItemAssumptionsNotImpairments()[o][t]).GetValueOrDefault();
                    this.Lines.Add(line);
                }
                if (subTotalGroup == OtherItemAssum.CommisionsAndOthersGroup && !costExists)
                {
                    this.CreateNetBankingIncome();
                }
            }
            if (!costExists && !commisionExists)
            {
                this.CreateNetBankingIncome();
            }
        }

        private void CreateNetBankingIncome()
        {
            ResultLine line = new ResultLine("Net Banking Income",
                                             Labels.ResultModel_CreateNetBankingIncome_Net_Banking_Income,
                                             FormattingType.Header);
            line.Function = (sd, t) => sd.GetNetBankingIncome()[t];
            this.Lines.Add(line);
        }

        private void CreateGainLosesOnFinancialInstrument()
        {
            ResultLine gainLosesLine =
                new ResultLine("Gain Losses on Financial Instrument",
                               Labels.
                                   ResultModel_CreateGainLosesOnFinancialInstrument_Gain_Losses_on_Financial_Instrument,
                               FormattingType.Header);

            ResultLine dividendsLine = new ResultLine("Dividends on Equity", "Dividends on Equity", FormattingType.Header);
            dividendsLine.Function = (sd, t) => sd.GetDividendsOnEquity()[t];

            ResultLine FXPnLLine = new ResultLine("FX P&L", "FX P&L", FormattingType.Header);
            FXPnLLine.Function = (sd, t) => sd.GetFX_PnL()[t];
                        
            ResultLine otherGainLosesLine = new ResultLine("Other Gain & Losses", "Other Gain & Losses",
                                                           FormattingType.Header);
            otherGainLosesLine.Function = (sd, t) => sd.GetGainLosesOnFinancialInstrument()[t];

            gainLosesLine.Children.Add(dividendsLine);
            gainLosesLine.Children.Add(FXPnLLine);
            gainLosesLine.Children.Add(otherGainLosesLine);
            
            gainLosesLine.Function = (sd, t) => sd.GetDividendsOnEquity()[t] + sd.GetGainLosesOnFinancialInstrument()[t];

            this.Lines.Add(gainLosesLine);
        }

        private void CreateNetInterestIncome()
        {
            ResultLine line = new ResultLine("Net Interest Income",
                                             Labels.ResultModel_CreateNetInterestIncome_Net_Interest_Income,
                                             FormattingType.Header);
            line.Function = (sd, t) => sd.GetNetInterestIncome()[t];
            this.Lines.Add(line);
        }

        private void CreateAllCategoriesLines(IEnumerable<Category> categories, ResultSection section)
        {
            if (categories == null) return;

            Category root = categories.FirstOrDefault(c => c.BalanceType == BalanceType.BalanceSheet);
            if (root == null) return;

            Category assetCategory = root.Children.Single(c => c.BalanceType == BalanceType.Asset);
            Category liabilityCategory = root.Children.Single(c => c.BalanceType == BalanceType.Liability);
            Category equityCategory = root.Children.Single(c => c.BalanceType == BalanceType.Equity);

            ResultLine assetLine = this.CreateCategoryLines(assetCategory, section, 1);
            ResultLine liabilityLine = this.CreateCategoryLines(liabilityCategory, section, 2);
            ResultLine equityLine = this.CreateCategoryLines(equityCategory, section, 2);

            string label = GetSectionLabel(section, null) + " - Liabilities & Equities";
            ResultLine liabilityEquityLine = new ResultLine(label, label, FormattingType.Header);
            liabilityEquityLine.Children.Add(liabilityLine);
            liabilityEquityLine.Children.Add(equityLine);

            this.Lines.Add(assetLine);
            this.Lines.Add(liabilityEquityLine);

            // liabilityEquityLine.Function = (sd, t) => liabilityLine.Function(sd, t) + equityLine.Function(sd, t);
        }

        private static string GetSectionLabel(ResultSection section, Category category)
        {
            string type;
            switch (section)
            {
                case ResultSection.BookValue:
                    type = Labels.ResultModel_GetSectionLabel_Book_Values;
                    break;
                case ResultSection.Income:
                    if (category != null && category.BalanceType == BalanceType.Asset)
                        type = Labels.ResultModel_GetSectionLabel_Interest_Income;
                    else
                        type = Labels.ResultModel_GetSectionLabel_Interest_Expenses;
                    break;
                case ResultSection.Yield:
                    type = Labels.ResultModel_GetSectionLabel_Yield;
                    break;
                default:
                    throw new ArgumentOutOfRangeException("section");
            }
            return type;
        }

        private ResultLine CreateCategoryLines(Category category, ResultSection section, int level)
        {
            string label = level <= 1 ? GetSectionLabel(section, category) + " " + category.Label : category.Label;
            string id = section + " - " + category.FullPath();
            FormattingType type = level <= 1 ? FormattingType.Header : FormattingType.Normal;
            ResultLine line = new ResultLine(id, label, type);

            if (category.Children.Count > 0)
            {
                foreach (Category child in category.ChildrenOrdered)
                {
                    line.Children.Add(this.CreateCategoryLines(child, section, level + 1));
                }
                Category[] leafs = this.GetLeafs(category).ToArray();
                switch (section)
                {
                    case ResultSection.BookValue:
                        line.Function = (sd, t) => leafs.Sum(c => (double?)sd.CategoryBookValue[c.Id][t]).GetValueOrDefault();
                        break;
                    case ResultSection.Income:
                        line.Function = (sd, t) => leafs.Sum(c => (double?)sd.CategoryIncome[c.Id][t]).GetValueOrDefault();
                        break;
                    case ResultSection.Yield:
                        line.Function = (sd, t) =>
                            getYieldValue(t, sd, leafs);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException("section");
                }
            }
            else
            {
                switch (section)
                {
                    case ResultSection.BookValue:
                        line.Function = (sd, t) => sd.CategoryBookValue[category.Id][t];
                        break;
                    case ResultSection.Income:
                        line.Function = (sd, t) => sd.CategoryIncome[category.Id][t];
                        break;
                    case ResultSection.Yield:
                        line.Function = (sd, t) => getYieldValue(category, t, sd);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException("section");
                }
            }

            return line;
        }

        private static double getYieldValue(Category category, int t, ScenarioData sd)
        {
            if (t>0)
            {
                return sd.CategoryIncome[category.Id][t] / sd.CategoryBookValue[category.Id][t-1] * 12;
            }
            else
            {
                return 0;
            }
        }

        private static double getYieldValue(int t, ScenarioData sd, Category[] leafs)
        {
            if (t>0)
            {
                return leafs.Sum(c => (double?)sd.CategoryIncome[c.Id][t]).GetValueOrDefault() /
                       leafs.Sum(c => (double?)sd.CategoryBookValue[c.Id][t-1]).GetValueOrDefault() * 12;
            }
            else
            {
                return 0;
            }
        }

        private IEnumerable<Category> GetLeafs(Category category)
        {
            foreach (Category child in category.ChildrenOrdered)
            {
                if (!child.Children.Any()) yield return child;
                foreach (Category childLeaf in GetLeafs(child))
                {
                    yield return childLeaf;
                }
            }
        }

        public void Compute(ScenarioData scenarioData, int t)
        {
            foreach (ResultLine line in LineDictionary.Values)
            {
                line.Compute(scenarioData, t);
            }
        }
    }
}