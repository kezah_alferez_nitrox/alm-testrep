using System;
using System.Collections.Generic;
using System.Linq;
using ALMS.Products;
using ALMSCommon.Threading;
using InsuranceBondManager.Domain;

namespace InsuranceBondManager.Calculations
{
    public class RollProductData : ProductData
    {
        private readonly int resultSteps;
        private readonly IList<ProductData> subRollDatas = new List<ProductData>();

        public RollProductData(Product product, DateTime startDate, DateTime valuationDate, int startStep, IList<IFundamentalVector> fundamentalVectors, ExpressionParser parser, ICouponExpressionEvaluator valorisationEvaluator, IFormulaEvaluator standardEvaluator, IDiscountCurveCollection discountCurves, ThreadSafeDictionary<string, CashflowProduct> allCashFlowProduct, bool updateBookPrice, int resultSteps, bool isDaily, bool originStartDateForModel)
            : base(product, startDate, valuationDate, startStep, fundamentalVectors, parser, valorisationEvaluator, standardEvaluator, discountCurves,
            allCashFlowProduct, updateBookPrice, resultSteps, isDaily,originStartDateForModel)
        {
            this.resultSteps = resultSteps;
        }

        public IList<ProductData> SubRollDatas
        {
            get { return this.subRollDatas; }
        }

        public override void ValuateProduct(IDictionary<string, object>[] variables, IDictionary<string, double[]> namedVariables)
        {
            base.ValuateProduct(variables, namedVariables);

            subRollDatas.Clear();
        }

        public override double GetBookValueInValuationCurrency(int step)
        {
            return this.subRollDatas.Sum(rd => rd.GetBookValueInValuationCurrency(step));
        }

        public override double GetBookValue(int step)
        {
            return this.subRollDatas.Sum(rd => rd.GetBookValue(step));
        }

        public override double GetAccruedInterest(int step)
        {
            return this.subRollDatas.Sum(rd => rd.GetAccruedInterest(step)); 
        }

        public override double GetDirtyBookValue(int step)
        {
            return this.subRollDatas.Sum(rd => rd.GetDirtyBookValue(step)); 
        }

        public override void Run(int step)
        {
            foreach (ProductData subRollData in this.subRollDatas)
            {
                subRollData.Run(step);
                this.CumulateSubRoll(step, subRollData);
            }
        }

        private void CumulateSubRoll(int step, ProductData subRollData)
        {
            this.CurrFace[step] += subRollData.CurrFace[step];
            this.CouponIncome[step] += subRollData.CouponIncome[step];
            this.GainAndLosesIncome[step] += subRollData.GainAndLosesIncome[step];
            this.InterestIncome[step] += subRollData.InterestIncome[step];
            this.PrincipalIncome[step] += subRollData.PrincipalIncome[step];
            this.LossImpairment[step] += subRollData.LossImpairment[step];
            this.IRCValue[step] += subRollData.IRCValue[step];

            if (this.ModelData != null)
            {
                this.ModelData.LossesImpairment[step] += subRollData.LossImpairment[step];
                this.ModelData.Recovery[step] += subRollData.Recovery[step];
            }
        }

        public void AddSubRoll(int step, double bookValue,IDiscountCurveCollection discountCurves)
        {
            DateTime startDate = PricerUtil.StepToDate(this.ValuationDate, step, IsDaily).AddDays(-1);
            ProductData subRoll = new ProductData(this.Product, startDate, this.ValuationDate,
                                                  step,
                                                  this.fundamentalVectors, this.parser,
                                                  this.ValorisationEvaluator, this.StandardEvaluator, discountCurves, this.allCashFlowProduct, false, this.resultSteps, this.IsDaily,this.OriginStartDateForModel);

            CopyFormulaValuesToSubRoll(this, subRoll);
            subRoll.Run(step);
            ///MTU Marc's bullshit ? :) subRoll.CurrFace[step] = bookValue / subRoll.GetBookDirtyPrice(step);
            if (subRoll.GetBookPrice(step) == 0)
                subRoll.BookPrice[step] = 1;
            subRoll.CurrFace[step] = bookValue/subRoll.GetBookPrice(step);
            this.CumulateSubRoll(step, subRoll);

            this.subRollDatas.Add(subRoll);
        }

        private static void CopyFormulaValuesToSubRoll(ProductData roll, ProductData subRoll)
        {
            int length = roll.CouponValues.Length;

            subRoll.Currency = roll.Currency;

            Array.Copy(roll.CouponValues, subRoll.CouponValues, length);
            Array.Copy(roll.FxRate, subRoll.FxRate, length);
            Array.Copy(roll.IrcPercentageValues, subRoll.IrcPercentageValues, length);

            if (roll.Product.IsModelAndNotDerivative)
            {
                Array.Copy(roll.CdrValues, subRoll.CdrValues, length);
                Array.Copy(roll.CprValues, subRoll.CprValues, length);
                Array.Copy(roll.LgdValues, subRoll.LgdValues, length);                
            }            
        }

        public override void Clear()
        {
            base.Clear();

            foreach (ProductData subRollData in subRollDatas)
            {
                subRollData.Clear();
            }
        }
    }
}