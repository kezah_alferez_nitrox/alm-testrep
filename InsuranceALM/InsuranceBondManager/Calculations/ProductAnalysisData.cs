using System;
using System.Collections.Generic;
using System.Linq;
using ALMS.Products;
using ALMSCommon.Threading;
using InsuranceBondManager.Domain;
using Iesi.Collections.Generic;
using NHibernate;

namespace InsuranceBondManager.Calculations
{
    public class ProductAnalysisData
    {
        private  ProductData productData;
        private readonly ExpressionParser parser;
        private readonly ISession session;
        private readonly SimulationParams parameters;
        private readonly Product product;

        private readonly IDictionary<string, Vector> allVectors = new Dictionary<string, Vector>(30, StringComparer.CurrentCultureIgnoreCase);
        private readonly IDictionary<string, Variable> allVariables = new Dictionary<string, Variable>(30, StringComparer.CurrentCultureIgnoreCase);

        private readonly IDictionary<string, object>[] vectorValues;
        private readonly IDictionary<string, double>[] variableValues;
        private readonly IDictionary<string, object>[] variableAndVectorValues;
        private readonly ThreadSafeDictionary<string, CashflowProduct> allCashFlowProduct =
                                   new ThreadSafeDictionary<string, CashflowProduct>();
        private readonly IDictionary<string, double[]> namedVariables = new Dictionary<string, double[]>(32, StringComparer.CurrentCultureIgnoreCase);
   
        private readonly List<ValidationMessage> validationErrors = new List<ValidationMessage>();
        private List<Scenario> selectedScenarios;
        private DateTime valuationDate;
        private readonly IList<IFundamentalVector> fundamentalVectorDatas = new List<IFundamentalVector>();
        private int resultMonths;
        

        public ProductAnalysisData(ISession session, SimulationParams parameters, Product product)
        {
            parser = new ExpressionParser();

            resultMonths = Param.ResultMonths(session);
            
            vectorValues = new IDictionary<string, object>[resultMonths];
            variableValues = new IDictionary<string, double>[resultMonths];
            variableAndVectorValues = new IDictionary<string, object>[resultMonths];

            this.session = session;
            this.parameters = parameters;
            this.product = product;

            Initialise();
        }

        private void Initialise()
        {
            Vector[] vectors = Vector.FindAll(this.session);
            foreach (Vector vector in vectors)
            {
                allVectors.Add(vector.Name, vector);
            }

            Variable[] variables = Variable.FindAll(this.session);
            foreach (Variable variable in variables)
            {
                allVariables.Add(variable.Name, variable);
            }

            selectedScenarios = new List<Scenario>(Scenario.FindSelected(this.session));

            valuationDate = Param.GetValuationDate(this.session) ?? DateTime.Today;

            FundamentalVector[] fundamentalVectors = FundamentalVector.FindAll(this.session).OrderBy(fv => fv.Unit).ThenBy(fv => fv.Delta).ToArray();
            foreach (FundamentalVector fundamentalVector in fundamentalVectors)
            {
                FundamentalVectorData fundamentalVectorData = new FundamentalVectorData(fundamentalVector, this.parser, parameters.ResultSteps);
                fundamentalVectorDatas.Add(fundamentalVectorData);
            }
        }

        public List<ValidationMessage> RunSimulation(Scenario scenario)
        {
            return this.RunSimulationInternal(scenario, false, resultMonths);
        }

        public List<ValidationMessage> ComputeBookPrice(Scenario scenario)
        {
            return this.RunSimulationInternal(scenario, true, 1);
        }
            
        private List<ValidationMessage> RunSimulationInternal(Scenario scenario, bool updateBookPrice, int steps)
        {
            CouponExpressionEvaluator valorisationEvaluator = new CouponExpressionEvaluator(this.valuationDate, this.namedVariables, this.fundamentalVectorDatas, parameters.IsComputationDaily);
            FormulaEvaluator standardEvaluator = new FormulaEvaluator(this.parser, this.valuationDate, this.variableAndVectorValues);

            DateTime startDate = product.StartDate.GetValueOrDefault(this.valuationDate).AddDays(-1);
            List<Product> allProds = new List<Product>(this.product.LinkedProducts);
            allProds.Add(product);
            List<ProductData> allProdsData = new List<ProductData>();
            foreach (Product prod in allProds)
            {
                ProductData newProdData = new ProductData(prod, startDate, this.valuationDate, 0,
                                                   this.fundamentalVectorDatas, this.parser, valorisationEvaluator,
                                                   standardEvaluator, null, this.allCashFlowProduct, updateBookPrice,
                                                   this.resultMonths, parameters.IsComputationDaily,parameters.OriginForModel);
                newProdData.ValidateBond(this.allVectors, this.allVariables, new DynamicVariable[0]);
                if (newProdData.ValidationErrors.Count > 0) return newProdData.ValidationErrors;

                ValidateFundamentalVectors(newProdData.UsedVectors, newProdData.UsedVariables);

                Util.ValidateVariables(selectedScenarios, newProdData.UsedVariables, allVectors,
                                       newProdData.UsedVectors, validationErrors);
                if (validationErrors.Count > 0) return validationErrors;

                Util.ComputeVectorValues(valuationDate, vectorValues, newProdData.UsedVectors,
                                         this.parameters.ResultSteps, this.parameters.IsComputationDaily);
                Util.ComputeVariableValues(scenario, parser, vectorValues, variableValues,
                                           newProdData.UsedVariables, validationErrors, this.parameters);
                if (validationErrors.Count > 0) return validationErrors;

                Util.InitVariableAndVectorValues(this.variableAndVectorValues, this.vectorValues, this.variableValues,
                                                 null, this.namedVariables, this.parameters);

                DiscountCurveCollection discountCurveCollection = this.GetDiscoutCurveCollection(valorisationEvaluator,
                                                                                                 standardEvaluator);
                newProdData.SetDiscoutCurves(discountCurveCollection);

                newProdData.ValuateProduct(this.variableAndVectorValues, namedVariables);
                if (newProdData.ValidationErrors.Count > 0) return newProdData.ValidationErrors;
                
                if (prod == product) 
                    productData = newProdData;
                else
                    allProdsData.Add(newProdData);
            }

            productData.LinkedProductDatas = allProdsData;

            productData.StartStep =  productData.Product.ComputeStartStep(valuationDate, parameters.IsComputationDaily);

            for (int i = 0; i < steps; i++)
            {
                foreach (ProductData data in allProdsData.Where(d=>d.Product.ProductType==ProductType.Swap))
                {
                    data.Run(i);
                }
                this.productData.Run(i);
            }

            return this.productData.ValidationErrors;
        }

        private DiscountCurveCollection GetDiscoutCurveCollection(CouponExpressionEvaluator valorisationEvaluator, FormulaEvaluator standardEvaluator)
        {
            DiscountCurve[] discountCurves = new DiscountCurve[Param.ResultMonths(this.session)];
            for (int i = 0; i < discountCurves.Length; i++)
            {
                DiscountCurve discountCurve = new DiscountCurve(this.valuationDate.AddDays(-1), valorisationEvaluator, standardEvaluator, this.fundamentalVectorDatas, this.allCashFlowProduct);
                discountCurve.ComputeZc(this.variableAndVectorValues[i]);
                discountCurves[i] = discountCurve;
            }
            return new DiscountCurveCollection(this.valuationDate, discountCurves);
        }

        private void ValidateFundamentalVectors(Iesi.Collections.Generic.ISet<Vector> usedVectors, Iesi.Collections.Generic.ISet<Variable> usedVariables)
        {
            foreach (FundamentalVectorData vector in fundamentalVectorDatas)
            {
                vector.Validate(allVectors, allVariables, usedVectors, usedVariables);
            }
        }

        public ModelData ModelData
        {
            get { return this.productData.ModelData; }
        }

        public ProductData ProductData
        {
            get { return this.productData; }
        }
    }
}