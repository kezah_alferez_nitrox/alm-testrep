﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InsuranceBondManager.Calculations
{
    public class CategoryData
    {
        public CategoryData(int resultMonths)
        {
            CouponIncome = new double[resultMonths];
            BookValue = new double[resultMonths];
            PrincipalIncome    = new double[resultMonths];
            InterestIncome     = new double[resultMonths];
            GainAndLosesIncome = new double[resultMonths];
            LossImpairment = new double[resultMonths];
        }

        public double[] BookValue { get; set; }
        public double[] CouponIncome      { get; set; }
        public double[] PrincipalIncome   { get; set; }
        public double[] InterestIncome    { get; set; }
        public double[] GainAndLosesIncome{ get; set; }
        public double[] LossImpairment    { get; set; }
    }
}
