using System;
using InsuranceBondManager.Domain;
using InsuranceBondManager.Resources.Language;

namespace InsuranceBondManager.Calculations
{
    public class ValidationMessage
    {
        private readonly SolvencyAdjustment adjustment;
        private readonly string customMessage;
        private readonly FXRate rate;
        private readonly Product product;
        private readonly VariableValue variableValue;
        private readonly Dividend dividend;
        private readonly OtherItemAssum otherItemAssum;
        private readonly string property;
        private readonly Exception exception;
        private readonly string value;
        private readonly ValidationMessageType type;
        private readonly FundamentalVector fundamentalVector;

        public ValidationMessage(Product target, string property, string value)
        {
            this.product = target;
            this.property = property;
            this.value = value;
            type = ValidationMessageType.Bond;
        }

        public ValidationMessage(Product target, string property, Exception exception)
        {
            this.product = target;
            this.property = property;
            this.exception = exception;
            type = ValidationMessageType.Bond;
        }

        public ValidationMessage(FundamentalVector target, string property, string value)
        {
            this.fundamentalVector = target;
            this.property = property;
            this.value = value;
            type = ValidationMessageType.FundamentalVector;
        }

        public ValidationMessage(VariableValue target, string property)
        {
            this.variableValue = target;
            this.property = property;
            type = ValidationMessageType.VariableValue;
        }

        public ValidationMessage(Dividend target)
        {
            this.dividend = target;
            type = ValidationMessageType.Dividend;
        }

        public ValidationMessage(OtherItemAssum target)
        {
            this.otherItemAssum = target;
            type = ValidationMessageType.OtherItemAssumption;
        }

        public ValidationMessage(FXRate rate)
        {
            this.rate = rate;
            type = ValidationMessageType.FxRate;
        }

        public ValidationMessage(string message)
        {
            customMessage = message;
            type = ValidationMessageType.Custom;
        }

        public ValidationMessage(SolvencyAdjustment adjustment)
        {
            this.adjustment = adjustment;
            type = ValidationMessageType.SolvencyAdjustment;
        }

        public ValidationMessage(Product target, string message)
        {
            product = target;
            customMessage = message;
            type = ValidationMessageType.Bond;
        }

        public ValidationMessageType Type
        {
            get { return type; }
        }

        public string Property
        {
            get { return property; }
        }

        public Product Product
        {
            get { return this.product; }
        }

        public VariableValue VariableValue
        {
            get { return variableValue; }
        }

        public Dividend Dividend
        {
            get { return dividend; }
        }

        public OtherItemAssum OtherItemAssum
        {
            get { return otherItemAssum; }
        }

        public string Message
        {
            get
            {
                switch (type)
                {
                    case ValidationMessageType.Bond:
                        if (this.exception != null)
                        {
                            return "Product " + this.product.ALMIDDisplayed + ": " + this.exception.Message;
                        }

                        if (customMessage != null)
                        {
                            return "Product " + this.product.ALMIDDisplayed + ": " + customMessage;
                        }

                        return string.Format(Labels.ValidationMessage_Message_, property, value,
                                         this.product.ALMIDDisplayed);

                    case ValidationMessageType.VariableValue:
                        return string.Format(Labels.ValidationMessage_Message_VariableValue_,
                                             property, variableValue.Variable.Name, variableValue.Value,
                                             variableValue.Scenario.Name);

                    case ValidationMessageType.Dividend:
                        return string.Format(Labels.ValidationMessage_Message_Unknown_vector___0___used_by_Dividend___1__,
                                             dividend.Amount, dividend.Name);
                    case ValidationMessageType.OtherItemAssumption:
                        return string.Format(Labels.ValidationMessage_Message_Invalid___0___used_by_Other_Item_Assumption___1__,
                                             otherItemAssum.Vect, otherItemAssum.Descrip);
                    case ValidationMessageType.SolvencyAdjustment:
                        return string.Format(Labels.ValidationMessage_Message_Invalid___0___used_by_Solvency_Adjustment___1__,
                                             adjustment.Vector, adjustment.Description);
                    case ValidationMessageType.FxRate:
                        return string.Format(Labels.ValidationMessage_Message_Invalid_value_for_exchage_rate___0___1__,
                            rate.From == null ? "null" : rate.From.Symbol,
                            rate.To == null ? "null" : rate.To.Symbol);
                    case ValidationMessageType.FundamentalVector:
                        return string.Format(Labels.ValidationMessage_Message_Invalid__0__for_fundamental_vector__1_, this.property,
                                             this.fundamentalVector);
                    case ValidationMessageType.Custom:
                        return customMessage;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }
    }

    public enum ValidationMessageType
    {
        Bond,
        VariableValue,
        Dividend,
        OtherItemAssumption,
        FxRate,
        Custom,
        SolvencyAdjustment,
        FundamentalVector
    }
}