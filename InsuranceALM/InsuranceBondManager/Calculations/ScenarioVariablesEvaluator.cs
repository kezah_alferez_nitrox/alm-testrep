using System;
using System.Collections.Generic;
using ALMS.Products;
using InsuranceBondManager.Domain;
using Iesi.Collections.Generic;
using NHibernate;

namespace InsuranceBondManager.Calculations
{
    public class ScenarioVariablesEvaluator : IFormulaEvaluator
    {
        private readonly Scenario scenario;

        private readonly ExpressionParser parser = new ExpressionParser();

        private readonly IDictionary<string, Vector> allVectors = new Dictionary<string, Vector>(30, StringComparer.CurrentCultureIgnoreCase);
        private readonly IDictionary<string, Variable> allVariables = new Dictionary<string, Variable>(30, StringComparer.CurrentCultureIgnoreCase);

        private readonly Iesi.Collections.Generic.ISet<Vector> usedVectors = new HashedSet<Vector>();

        private readonly IDictionary<string, object>[] vectorValues;

        private readonly IDictionary<string, double>[] variableValues;

        private readonly IDictionary<string, object>[] allValues;

        private DateTime valuationDate;

        private readonly List<ValidationMessage> validationErrors = new List<ValidationMessage>();

        private int resultMonths;
        private ISession session;
        private readonly SimulationParams parameters;

        public ScenarioVariablesEvaluator(Scenario scenario, ISession session, SimulationParams parameters)
        {
            this.scenario = scenario;

            this.resultMonths = Param.ResultMonths(session);
            vectorValues=new IDictionary<string, object>[resultMonths];
            variableValues = new IDictionary<string, double>[resultMonths];
            allValues = new IDictionary<string, object>[resultMonths];
            this.session = session;
            this.parameters = parameters;
        }

        public List<ValidationMessage> ValidationErrors
        {
            get
            {
                return validationErrors;
            }
        }

        public IDictionary<Variable, double[]> Values
        {
            get
            {
                IDictionary<Variable, double[]> dictionary = new Dictionary<Variable, double[]>();

                foreach (Variable variable in allVariables.Values)
                {
                    dictionary.Add(variable, new double[this.resultMonths]);
                }

                for (int i = 0; i < this.resultMonths; i++)
                {
                    foreach (KeyValuePair<string, double> pair in variableValues[i])
                    {
                        dictionary[allVariables[pair.Key]][i] = (double)pair.Value;
                    }
                }

                return dictionary;
            }
        }

        private void Initialize()
        {
            allVectors.Clear();
            allVariables.Clear();
            usedVectors.Clear();
            validationErrors.Clear();

            Vector[] vectors = Vector.FindAll(this.session);
            foreach (Vector vector in vectors)
            {
                allVectors.Add(vector.Name, vector);
            }

            Variable[] variables = Variable.FindAll(this.session);
            foreach (Variable variable in variables)
            {
                allVariables.Add(variable.Name, variable);
            }

            valuationDate = Param.GetValuationDate(this.session) ?? DateTime.Today;
        }

        public void Run()
        {
            Initialize();

            if (validationErrors.Count > 0) return;
            
            Util.ValidateVariables(new[] { scenario }, allVariables.Values, allVectors, usedVectors, validationErrors);
            if (validationErrors.Count > 0) return;

            Util.ComputeVectorValues(valuationDate, vectorValues, usedVectors, parameters.ResultSteps, false);
            Util.ComputeVariableValues(scenario, parser, vectorValues, variableValues, allVariables.Values, validationErrors, parameters);
            if (validationErrors.Count > 0) return;

            BuildValues();
        }

        private void BuildValues()
        {
            for (int i = 0; i < this.resultMonths; i++)
            {
                allValues[i] = new Dictionary<string, object>(32, StringComparer.InvariantCultureIgnoreCase);

                foreach (KeyValuePair<string, object> pair in vectorValues[i])
                {
                    allValues[i].Add(pair.Key.ToLowerInvariant(), pair.Value);    
                }

                foreach (KeyValuePair<string, double> pair in variableValues[i])
                {
                    allValues[i].Add(pair.Key.ToLowerInvariant(), pair.Value);
                }
            }
        }

        public double Evaluate(string formula, DateTime date)
        {
            int month = ConvertDateToStep(date, this.valuationDate, this.resultMonths, parameters.IsComputationDaily);

            return Evaluate(formula, month);
        }

        public static int ConvertDateToStep(DateTime date, DateTime valuationDate, int resultMonths, bool isDaily)
        {
            int month = PricerUtil.DateDiffInSteps(date,valuationDate, isDaily);

            if (month < 0) month = 0;
            if (month >= resultMonths) month = resultMonths - 1;
            return month;
        }

        public double Evaluate(string formula, int month)
        {
            return (double)this.parser.Evaluate(formula, this.allValues[month]);
        }
    }
}