﻿using System;

namespace InsuranceBondManager.ExternalData
{
    public interface IExternalDataInterface
    {
        string Name { get; }
        void Import(object sender, EventArgs eventArgs);
    }
}
