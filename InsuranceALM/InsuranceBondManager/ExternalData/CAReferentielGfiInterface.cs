﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using ALMS.Products;
using InsuranceALM.Infrastructure.Data;
using InsuranceBondManager.Calculations;
using InsuranceBondManager.Domain;
using InsuranceBondManager.Views;
using InsuranceBondManager.Views.BalanceSheet.Parameters;
using Eu.AlterSystems.ASNetLib.Core;
using NHibernate;

namespace InsuranceBondManager.ExternalData
{
    public class CAReferentielGfiInterface : IExternalDataInterface
    {
        private static string avance = "AVANCE_";
        public const string FIELD_SEPARATOR = "\t";
        public const string LINE_SEPARATOR = "\n";
        private static Currency EUR = null;
        public string Name { get { return "Interface Crédit Agricole REFERENTIEL GFI"; } }
        private static DateTime valuationDate;
        private static Category balanceSheet;
        private static Category portfolio;
        private static Category liabilities;
        private static Category depotsSansCouts;
        private static Category autresEpargne;
        private static Category creditsCategory;
        private static double LIVRET_A = 0.0175;

        private static IDictionary<string, Tuple<string, double, bool>> epargneCategories = new Dictionary<string, Tuple<string, double, bool>>();
        private static IDictionary<string, ProductAmortizationType> creditsAmortTrancod = new Dictionary<string, ProductAmortizationType>();
        private static IDictionary<string, Tuple<string, ProductFrequency?>> indicesTranscod = new Dictionary<string, Tuple<string, ProductFrequency?>>();

        private static IDictionary<string, Tuple<string, ProductFrequency>> marketDataTranscod = new Dictionary<string, Tuple<string, ProductFrequency>>();
        
        public void Import(object sender, EventArgs eventArgs)
        {
            valuationDate = MainForm.ValuationDate;
            if (MessageBox.Show("Are you sure you want to import data for a Valuation Date = " + valuationDate.ToShortDateString() + " ?\nClick Yes to continue, No to stop the import.", Constants.APPLICATION_NAME,
                                   MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }

            string[] sqls = new string[7];
            int i = 0;
            sqls[i++] = SQL_CREDITS;
            sqls[i++] = SQL_CREDITS_G2;
            sqls[i++] = SQL_DAT;
            sqls[i++] = SQL_EPARGNE;
            sqls[i++] = SQL_PRETS_EMPRUNTS;
            sqls[i++] = SQL_SWAPS;
            sqls[i++] = SQL_TCN;

            for (int j = 0; j < 7; j++)
            {
                sqls[j] = replaceDate(sqls[j], valuationDate);
            }



            SqlConnection myConnection = new SqlConnection("server=(local);" +
                                       "Trusted_Connection=yes;Integrated Security=yes;" +
                                       "database=REFERENTIEL_GFI; " +
                                       "connection timeout=30");
            try
            {
                myConnection.Open();
            }
            catch (Exception e)
            {
                MessageBox.Show("Error openning the connection to database : " + e.Message);
            }
            int count = 0;
            using (ISession session = SessionManager.OpenSession())
            {
                portfolio = Category.FindPortfolio(session);
                balanceSheet = Category.FindBalanceSheet(session);
                liabilities = balanceSheet.Children.First(c => c.BalanceType == BalanceType.Liability);
                EnsureAndClearCategoriesEPARGNE(session);
                EnsureAndClearCategoriesDAT(session);
                EnsureAndClearCategoriesCredits(session);
                Currency[] allCurrencies = Currency.FindAll(session);
                EUR = allCurrencies.First(c => c.Symbol == "€");

                try
                {
                    SqlDataReader myReader;
                    SqlCommand myCommand = null;
                    int maxId = Product.GetMaxAlmIdForCategory(session, creditsCategory);
                    for (int j = 0; j < 7; j++)
                    {
                        myReader = executeQuery(sqls[j], myCommand, myConnection);

                        while (myReader.Read())
                        {
                            string ccase ="";
                            switch (j)
                            {
                                case 0:
                                    ccase = "CREDITS";
                                    break;
                                case 1:
                                    ccase = "G2";
                                    break;
                                case 2:
                                    ccase = "DAT";
                                    break;
                                case 3:
                                    ccase = "EPARGNE";
                                    break;
                                case 4:
                                    ccase = "PRETS_EMPRUNTS";
                                    break;
                                case 5:
                                    ccase = "SWAPS";
                                    break;
                                case 6:
                                    ccase = "TCN";
                                    break;
                            }

                            Debug.WriteLine("Found records in "+ccase);

                            count++;
                            if (j == 0)
                            {//Crédits
                                traiterCREDITS(myReader, session,maxId+count+1);
                            }

                            if (j == 2)
                            {//DAT
                                traiterDAT(myReader, session);
                            }
                            //if (j == 3)
                            //{//Epargne
                            //    traiterEPARGNE(myReader, session);
                            //}
                        }
                        myReader.Close();
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }
                session.Flush();
            }
            MainForm.Instance.balance.RefreshTreeView();
            MessageBox.Show(count + " products imported.");


        }

        private static void EnsureAndClearCategoriesDAT(ISession session)
        {
            Category category = null;
            //Category
            Category collecteClientele = EnsureCategoryExists(session, liabilities, "Collecte clientèle");
            depotsSansCouts = EnsureCategoryExists(session, collecteClientele, "Dépôts sans coût (PME et Particuliers)");
            category = EnsureCategoryExists(session, depotsSansCouts, "DAT CT (DI <= 1 an)");
            category.Bonds.Clear();
            category = EnsureCategoryExists(session, depotsSansCouts, "DAT MLT (DI > 1 an)");
            category.Bonds.Clear();
            category = EnsureCategoryExists(session, depotsSansCouts, "DAT CT (DI <= 1 an)");
            category.Bonds.Clear();
        }
        private static void EnsureAndClearCategoriesEPARGNE(ISession session)
        {
            epargneCategories.Clear();
            epargneCategories.Add("BGP TITRE ORDINAIRE", new Tuple<string, double, bool>("", 0, false));
            epargneCategories.Add("CA LEASING LOCATION OPERATION", new Tuple<string, double, bool>("", 0, false));
            epargneCategories.Add("CARRE JAUNE ( CEL )", new Tuple<string, double, bool>("Balance Sheet/Liabilities/Collecte clientèle/Dépôts sans coût (PME et Particuliers)/Epargne logement PEL-CEL/CEL", 0, false));
            epargneCategories.Add("CARRE MAUVE ( PEL )", new Tuple<string, double, bool>("Balance Sheet/Liabilities/Collecte clientèle/Dépôts sans coût (PME et Particuliers)/Epargne logement PEL-CEL/PEL", 0, false));
            epargneCategories.Add("COMPTE CLIC MONETIQUE", new Tuple<string, double, bool>("", 0, false));
            epargneCategories.Add("COMPTE DPA", new Tuple<string, double, bool>("Balance Sheet/Liabilities/Collecte clientèle/Dépôts sans coût (PME et Particuliers)/Epargne centralisée hors émissions réseau et épargne logement (livret et autres)/DAT Evolution 5 et DPA", 0, true));
            epargneCategories.Add("COMPTE DPA FISCAL", new Tuple<string, double, bool>("Balance Sheet/Liabilities/Collecte clientèle/Dépôts sans coût (PME et Particuliers)/Epargne centralisée hors émissions réseau et épargne logement (livret et autres)/DAT Evolution 5 et DPA", 0, true));
            epargneCategories.Add("COMPTE EPARGNE AGRI", new Tuple<string, double, bool>("Balance Sheet/Liabilities/Collecte clientèle/Dépôts sans coût (PME et Particuliers)/Epargne centralisée hors émissions réseau et épargne logement (livret et autres)/DAT Evolution 5 et DPA", 0, true));
            epargneCategories.Add("COMPTE SUPPORT CAPITAL", new Tuple<string, double, bool>("", 0, false));
            epargneCategories.Add("COMPTE SUR LIVRET ( CSL )", new Tuple<string, double, bool>("Balance Sheet/Liabilities/Collecte clientèle/Dépôts sans coût (PME et Particuliers)/Epargne centralisée hors émissions réseau et épargne logement (livret et autres)/CSL", 0, true));
            epargneCategories.Add("COMPTE TITRE ORDINAIRE", new Tuple<string, double, bool>("", 0, false));
            epargneCategories.Add("COMPTE TITRES PLUS", new Tuple<string, double, bool>("", 0, false));
            epargneCategories.Add("CONVENTION INDOSUEZ", new Tuple<string, double, bool>("", 0, false));
            epargneCategories.Add("CSL ASSOCIATIONS", new Tuple<string, double, bool>("Balance Sheet/Liabilities/Collecte clientèle/Dépôts sans coût (PME et Particuliers)/Epargne centralisée hors émissions réseau et épargne logement (livret et autres)/CSL", 0, true));
            epargneCategories.Add("CSL BIS", new Tuple<string, double, bool>("Balance Sheet/Liabilities/Collecte clientèle/Dépôts sans coût (PME et Particuliers)/Epargne centralisée hors émissions réseau et épargne logement (livret et autres)/CSL", 0, true));
            epargneCategories.Add("CSL EXCEDENT PRO", new Tuple<string, double, bool>("Balance Sheet/Liabilities/Collecte clientèle/Dépôts sans coût (PME et Particuliers)/Epargne centralisée hors émissions réseau et épargne logement (livret et autres)/CSL", 0, true));
            epargneCategories.Add("CSL HABITAT SOCIAL", new Tuple<string, double, bool>("Balance Sheet/Liabilities/Collecte clientèle/Dépôts sans coût (PME et Particuliers)/Epargne centralisée hors émissions réseau et épargne logement (livret et autres)/CSL", 0, true));
            epargneCategories.Add("CTO ASSISTE MONTPENSIER", new Tuple<string, double, bool>("", 0, false));
            epargneCategories.Add("CTO LIBRE MONTPENSIER", new Tuple<string, double, bool>("", 0, false));
            epargneCategories.Add("DAT EVOLUTION5 CREDIT AGRICOLE", new Tuple<string, double, bool>("Balance Sheet/Liabilities/Collecte clientèle/Dépôts sans coût (PME et Particuliers)/Epargne centralisée hors émissions réseau et épargne logement (livret et autres)/DAT Evolution 5 et DPA", 0, true));
            epargneCategories.Add("EUROFACTOR EXP FULL CLASSIQUE", new Tuple<string, double, bool>("", 0, false));
            epargneCategories.Add("EUROFACTOR EXP PREFERENCE", new Tuple<string, double, bool>("", 0, false));
            epargneCategories.Add("EUROFACTOR EXP RDB MANDAT GESTIO", new Tuple<string, double, bool>("", 0, false));
            epargneCategories.Add("EUROFACTOR EXP REVERSE FACTORING", new Tuple<string, double, bool>("", 0, false));
            epargneCategories.Add("FLORIAGRI", new Tuple<string, double, bool>("", 0, false));
            epargneCategories.Add("GARANTIE DECES", new Tuple<string, double, bool>("", 0, false));
            epargneCategories.Add("INVESTSTORE INTEGRAL", new Tuple<string, double, bool>("", 0, false));
            epargneCategories.Add("LIVRET A", new Tuple<string, double, bool>("Balance Sheet/Liabilities/Collecte clientèle/Dépôts sans coût (PME et Particuliers)/Epargne centralisée hors émissions réseau et épargne logement (livret et autres)/Livret A-LDD", 0.4272, true));
            epargneCategories.Add("LIVRET A ASSOCIATION", new Tuple<string, double, bool>("Balance Sheet/Liabilities/Collecte clientèle/Dépôts sans coût (PME et Particuliers)/Epargne centralisée hors émissions réseau et épargne logement (livret et autres)/Livret A-LDD", 0.4272, true));
            epargneCategories.Add("LIVRET A HLM", new Tuple<string, double, bool>("Balance Sheet/Liabilities/Collecte clientèle/Dépôts sans coût (PME et Particuliers)/Epargne centralisée hors émissions réseau et épargne logement (livret et autres)/Livret A-LDD", 0.4272, true));
            epargneCategories.Add("LIVRET CODEBIS", new Tuple<string, double, bool>("Balance Sheet/Liabilities/Collecte clientèle/Dépôts sans coût (PME et Particuliers)/Epargne centralisée hors émissions réseau et épargne logement (livret et autres)/CODEBIS", 0, true));
            epargneCategories.Add("LIVRET D' EPARGNE POPULAIRE", new Tuple<string, double, bool>("Balance Sheet/Liabilities/Collecte clientèle/Dépôts sans coût (PME et Particuliers)/Epargne centralisée hors émissions réseau et épargne logement (livret et autres)/TIWI-LEP-LVJ", 0.7, true));
            epargneCategories.Add("LIVRET DE DEVELOPPEMENT DURABLE", new Tuple<string, double, bool>("Balance Sheet/Liabilities/Collecte clientèle/Dépôts sans coût (PME et Particuliers)/Epargne centralisée hors émissions réseau et épargne logement (livret et autres)/Livret A-LDD", 0.4272, true));
            epargneCategories.Add("LIVRET JEUNE MOZAIC", new Tuple<string, double, bool>("Balance Sheet/Liabilities/Collecte clientèle/Dépôts sans coût (PME et Particuliers)/Epargne centralisée hors émissions réseau et épargne logement (livret et autres)/TIWI-LEP-LVJ", 0, true));
            epargneCategories.Add("LIVRET TANDEM", new Tuple<string, double, bool>("Balance Sheet/Liabilities/Collecte clientèle/Dépôts sans coût (PME et Particuliers)/Epargne centralisée hors émissions réseau et épargne logement (livret et autres)/TIWI-LEP-LVJ", 0, true));
            epargneCategories.Add("LIVRET TIWI", new Tuple<string, double, bool>("Balance Sheet/Liabilities/Collecte clientèle/Dépôts sans coût (PME et Particuliers)/Epargne centralisée hors émissions réseau et épargne logement (livret et autres)/TIWI-LEP-LVJ", 0, true));
            epargneCategories.Add("ORCHESTRAL", new Tuple<string, double, bool>("Balance Sheet/Liabilities/Collecte clientèle/Dépôts sans coût (PME et Particuliers)/Epargne centralisée hors émissions réseau et épargne logement (livret et autres)/PEP", 0, true));
            epargneCategories.Add("PIERRISSIME", new Tuple<string, double, bool>("", 0, false));
            epargneCategories.Add("PLAN D'EPARGNE POPULAIRE", new Tuple<string, double, bool>("Balance Sheet/Liabilities/Collecte clientèle/Dépôts sans coût (PME et Particuliers)/Epargne centralisée hors émissions réseau et épargne logement (livret et autres)/PEP", 0, true));
            epargneCategories.Add("SERVICE D'AIDE A LA PERSONNE", new Tuple<string, double, bool>("", 0, false));
            epargneCategories.Add("VARIUS 5 TERME DROP", new Tuple<string, double, bool>("Balance Sheet/Liabilities/Collecte clientèle/Dépôts sans coût (PME et Particuliers)/Epargne centralisée hors émissions réseau et épargne logement (livret et autres)/Divers CAM", 0, false));
            epargneCategories.Add("VARIUS 7 MENSUEL DROP", new Tuple<string, double, bool>("Balance Sheet/Liabilities/Collecte clientèle/Dépôts sans coût (PME et Particuliers)/Epargne centralisée hors émissions réseau et épargne logement (livret et autres)/Divers CAM", 0, false));
            epargneCategories.Add("COMPTE SUR LIVRET 2006 2", new Tuple<string, double, bool>("Balance Sheet/Liabilities/Collecte clientèle/Dépôts sans coût (PME et Particuliers)/Epargne centralisée hors émissions réseau et épargne logement (livret et autres)/CSL", 0, false));
            epargneCategories.Add("CSL SOCIETAIRES", new Tuple<string, double, bool>("Balance Sheet/Liabilities/Collecte clientèle/Dépôts sans coût (PME et Particuliers)/Epargne centralisée hors émissions réseau et épargne logement (livret et autres)/CSL", 0, true));

            string[] avancesCategories = (string[])epargneCategories.Keys.ToArray().Clone();

            //=SUBSTITUTE(B2;"Balance Sheet/Liabilities/Collecte clientèle/Dépôts sans coût (PME et Particuliers)";"Balance Sheet/Liabilities/DEA/-Avances miroir/Avances miroir")
            //Tuple<string, string> categ;
            foreach (string avancesCategory in avancesCategories)
            {
                //categ=GetLastCategoryAndPath(epargneCategories[avancesCategory].Item1);
                string categName = (string) epargneCategories[avancesCategory].Item1;
                string categReplaced = categName.Replace("Balance Sheet/Liabilities/Collecte clientèle/Dépôts sans coût (PME et Particuliers)", "Balance Sheet/Liabilities/DEA/-Avances miroir/Avances miroir");
                epargneCategories.Add(avance + avancesCategory, new Tuple<string, double, bool>(categReplaced,
                                                                                                epargneCategories[avancesCategory].Item2,
                                                                                                epargneCategories[avancesCategory].Item3));
            }
            Category categ;
            foreach (Tuple<string, double, bool> epargneCategory in epargneCategories.Values)
            {
                if (epargneCategory.Item1 != "")
                {
                    categ = EnsureCategoryExistsRecursively(session, portfolio, epargneCategory.Item1);
                    categ.Bonds.Clear();
                }
            }
            autresEpargne = EnsureCategoryExists(session, liabilities, "Collecte clientèle/AUTRES");
            if (autresEpargne.Children!=null) autresEpargne.Children.Clear();
            if (autresEpargne.Bonds!=null) autresEpargne.Bonds.Clear();
        }
        private static void EnsureAndClearCategoriesCredits(ISession session)
        {
            creditsCategory = EnsureCategoryExistsRecursively(session, portfolio, "Balance Sheet/Asset/Actifs clientèle/Crédits clientèle");
            creditsCategory.Bonds.Clear();
            creditsCategory.Children.Clear();

            creditsAmortTrancod.Clear();
            creditsAmortTrancod.Add("In Fine", ProductAmortizationType.BULLET);
            creditsAmortTrancod.Add("K+I Constant", ProductAmortizationType.CST_PAY);
            creditsAmortTrancod.Add("Linéaire", ProductAmortizationType.LINEAR_AMORT);
            creditsAmortTrancod.Add("AMORT.CST", ProductAmortizationType.LINEAR_AMORT);
            creditsAmortTrancod.Add("IN-FINE", ProductAmortizationType.BULLET);
            creditsAmortTrancod.Add("VERST.CST", ProductAmortizationType.CST_PAY);
            creditsAmortTrancod.Add("N/A", ProductAmortizationType.LINEAR_AMORT);
            creditsAmortTrancod.Add("NA", ProductAmortizationType.LINEAR_AMORT);
            creditsAmortTrancod.Add("Progressif", ProductAmortizationType.CST_PAY);

            indicesTranscod.Clear();
            indicesTranscod.Add("ATL C REVISABLE", new Tuple<string, ProductFrequency?>("TV", null));
            indicesTranscod.Add("ATL GPE CA REVISABLE", new Tuple<string, ProductFrequency?>("TV", null));
            indicesTranscod.Add("ATL I REVISABLE", new Tuple<string, ProductFrequency?>("TV", null));
            indicesTranscod.Add("ATL MIGRATION 11,90", new Tuple<string, ProductFrequency?>("TV", null));
            indicesTranscod.Add("ATL MIGRATION 13,90", new Tuple<string, ProductFrequency?>("TV", null));
            indicesTranscod.Add("ATL MIGRATION 15,50", new Tuple<string, ProductFrequency?>("TV", null));
            indicesTranscod.Add("ATL MIGRATION 5,90", new Tuple<string, ProductFrequency?>("TV", null));
            indicesTranscod.Add("ATL MIGRATION 9,90", new Tuple<string, ProductFrequency?>("TV", null));
            indicesTranscod.Add("ATL SAL CADIF REVISA", new Tuple<string, ProductFrequency?>("TV", null));
            indicesTranscod.Add("ATL SELECT", new Tuple<string, ProductFrequency?>("TV", null));
            indicesTranscod.Add("ATL STANDARD 1", new Tuple<string, ProductFrequency?>("TV", null));
            indicesTranscod.Add("ATL STANDARD 2", new Tuple<string, ProductFrequency?>("TV", null));
            indicesTranscod.Add("ATL STANDARD 3", new Tuple<string, ProductFrequency?>("TV", null));
            indicesTranscod.Add("ATL STANDARD 4", new Tuple<string, ProductFrequency?>("TV", null));
            indicesTranscod.Add("ATL STANDARD TX REVISABLE", new Tuple<string, ProductFrequency?>("TV", null));
            indicesTranscod.Add("ATL STD BASCULE REV 3BIS", new Tuple<string, ProductFrequency?>("TV", null));
            indicesTranscod.Add("ATL STD REVISABLE", new Tuple<string, ProductFrequency?>("TV", null));
            indicesTranscod.Add("ATOUT LIBRE AUTO", new Tuple<string, ProductFrequency?>("TV", null));
            indicesTranscod.Add("ATOUT LIBRE C", new Tuple<string, ProductFrequency?>("TV", null));
            indicesTranscod.Add("ATOUT LIBRE ETUDIANT", new Tuple<string, ProductFrequency?>("TV", null));
            indicesTranscod.Add("ATOUT LIBRE FIDELITE", new Tuple<string, ProductFrequency?>("TV", null));
            indicesTranscod.Add("ATOUT LIBRE FIDELITE 02.2001", new Tuple<string, ProductFrequency?>("TV", null));
            indicesTranscod.Add("ATOUT LIBRE HABITAT", new Tuple<string, ProductFrequency?>("TV", null));
            indicesTranscod.Add("ATOUT LIBRE I", new Tuple<string, ProductFrequency?>("TV", null));
            indicesTranscod.Add("ATOUT LIBRE PRIVILEGE", new Tuple<string, ProductFrequency?>("TV", null));
            indicesTranscod.Add("ATOUT LIBRE QUOTIDIEN", new Tuple<string, ProductFrequency?>("TV", null));
            indicesTranscod.Add("ATOUT LIBRE SALARIES GROUPE CA", new Tuple<string, ProductFrequency?>("TV", null));
            indicesTranscod.Add("ATOUT LIBRE SELECT", new Tuple<string, ProductFrequency?>("TV", null));
            indicesTranscod.Add("ATOUT LIBRE SERENITE", new Tuple<string, ProductFrequency?>("TV", null));
            indicesTranscod.Add("ATOUT LIBRE STANDARD", new Tuple<string, ProductFrequency?>("TV", null));
            indicesTranscod.Add("CONSO TAUX MINORE", new Tuple<string, ProductFrequency?>("TV", null));
            indicesTranscod.Add("COURT TERME AGRICOLE", new Tuple<string, ProductFrequency?>("TV", null));
            indicesTranscod.Add("EURIBOR 1 MOIS", new Tuple<string, ProductFrequency?>("E1M", null));
            indicesTranscod.Add("EURIBOR 12 MOIS", new Tuple<string, ProductFrequency?>("E1Y", null));
            indicesTranscod.Add("EURIBOR 12 MOIS MOYENNE", new Tuple<string, ProductFrequency?>("E1Y", null));
            indicesTranscod.Add("EURIBOR 3 MOIS", new Tuple<string, ProductFrequency?>("E3M", null));
            indicesTranscod.Add("EURIBOR 3 MOIS MOYENNE", new Tuple<string, ProductFrequency?>("E3M", null));
            indicesTranscod.Add("EURIBOR 6 MOIS", new Tuple<string, ProductFrequency?>("E6M", null));
            indicesTranscod.Add("FIXE 0%", new Tuple<string, ProductFrequency?>("TV", null));
            //indicesTranscod.Add("LIVRET A", new Tuple<string, ProductFrequency?>("Livret_A", null));
            indicesTranscod.Add("MODULO 5", new Tuple<string, ProductFrequency?>("TV", null));
            indicesTranscod.Add("MOYEN TERME AGRICOLE", new Tuple<string, ProductFrequency?>("TV", null));
            indicesTranscod.Add("PBE 01/90-07/91", new Tuple<string, ProductFrequency?>("TV", null));
            indicesTranscod.Add("PBE 05/94-99/99", new Tuple<string, ProductFrequency?>("TV", null));
            indicesTranscod.Add("PBE 12 A 84 MOIS", new Tuple<string, ProductFrequency?>("TV", null));
            indicesTranscod.Add("PBE 85 A 180 MOIS", new Tuple<string, ProductFrequency?>("TV", null));
            indicesTranscod.Add("PBE AVANT 1986", new Tuple<string, ProductFrequency?>("TV", null));
            indicesTranscod.Add("PBE MINORE  08/98 - 99/99", new Tuple<string, ProductFrequency?>("TV", null));
            indicesTranscod.Add("PRE ATT ATL REVISABLE", new Tuple<string, ProductFrequency?>("TV", null));
            indicesTranscod.Add("PRE ATT ATOUT STANDARD", new Tuple<string, ProductFrequency?>("TV", null));
            indicesTranscod.Add("REVOLVING PARTICULIERS", new Tuple<string, ProductFrequency?>("TV", null));
            indicesTranscod.Add("SCORE ATL ASSIMILES", new Tuple<string, ProductFrequency?>("TV", null));
            indicesTranscod.Add("SCORE ATL PRIVILEGE", new Tuple<string, ProductFrequency?>("TV", null));
            indicesTranscod.Add("SCORE ATL SALARIE CASA", new Tuple<string, ProductFrequency?>("TV", null));
            indicesTranscod.Add("SCORE ATL SALARIE CR", new Tuple<string, ProductFrequency?>("TV", null));
            indicesTranscod.Add("SCORE ATL SELECT", new Tuple<string, ProductFrequency?>("TV", null));
            indicesTranscod.Add("TAG 1 MOIS", new Tuple<string, ProductFrequency?>("E3M", null));
            indicesTranscod.Add("TAG 12 MOIS", new Tuple<string, ProductFrequency?>("E3M", null));
            indicesTranscod.Add("TAG 3 MOIS", new Tuple<string, ProductFrequency?>("E3M", null));
            indicesTranscod.Add("TAG 6 MOIS", new Tuple<string, ProductFrequency?>("E3M", null));
            indicesTranscod.Add("TAM  REVISE CHAQUE MOIS", new Tuple<string, ProductFrequency?>("E3M", null));
            indicesTranscod.Add("TAUX CREDIPRO", new Tuple<string, ProductFrequency?>("TV", null));
            indicesTranscod.Add("TAUX DE BASE CADIF", new Tuple<string, ProductFrequency?>("TV", null));
            indicesTranscod.Add("TAUX DE BASE DES BANQUES", new Tuple<string, ProductFrequency?>("TV", null));
            indicesTranscod.Add("TAUX FIXE 5 %", new Tuple<string, ProductFrequency?>("TV", null));
            indicesTranscod.Add("TEC 10", new Tuple<string, ProductFrequency?>("TEC10", null));
            indicesTranscod.Add("TME", new Tuple<string, ProductFrequency?>("TME", null));
            indicesTranscod.Add("TRBO+BONS", new Tuple<string, ProductFrequency?>("TV", null));
            indicesTranscod.Add("taux_moyenne ('CODEVIB')", new Tuple<string, ProductFrequency?>("CODEVI", ProductFrequency.Monthly));
            indicesTranscod.Add("taux_revisable ('CODEVIB')", new Tuple<string, ProductFrequency?>("CODEVI", ProductFrequency.Monthly));
            indicesTranscod.Add("taux_revisable ('E1R')", new Tuple<string, ProductFrequency?>("E1M", ProductFrequency.Monthly));
            indicesTranscod.Add("taux_revisable ('E3R')", new Tuple<string, ProductFrequency?>("E3M", ProductFrequency.Quarterly));
            indicesTranscod.Add("taux_revisable ('E6R')", new Tuple<string, ProductFrequency?>("E6M", ProductFrequency.SemiAnnual));
            indicesTranscod.Add("CMS", new Tuple<string, ProductFrequency?>("CMS5Y", ProductFrequency.Monthly));
            indicesTranscod.Add("882OAT", new Tuple<string, ProductFrequency?>("IPC", ProductFrequency.Monthly));
            indicesTranscod.Add("88200BMTN", new Tuple<string, ProductFrequency?>("E3M", ProductFrequency.Quarterly));
            indicesTranscod.Add("88200BMTNS", new Tuple<string, ProductFrequency?>("E3M", ProductFrequency.Quarterly));
            indicesTranscod.Add("E12R", new Tuple<string, ProductFrequency?>("E1Y", ProductFrequency.Annual));
            indicesTranscod.Add("E1R", new Tuple<string, ProductFrequency?>("E1M", ProductFrequency.Monthly));
            indicesTranscod.Add("E3R", new Tuple<string, ProductFrequency?>("E3M", ProductFrequency.Quarterly));
            indicesTranscod.Add("E6R", new Tuple<string, ProductFrequency?>("E6M", ProductFrequency.SemiAnnual));
            indicesTranscod.Add("IPC", new Tuple<string, ProductFrequency?>("IPC", ProductFrequency.Monthly));
            indicesTranscod.Add("LIVRETA1", new Tuple<string, ProductFrequency?>("Livret_A", ProductFrequency.SemiAnnual));
            indicesTranscod.Add("LIVRETAD", new Tuple<string, ProductFrequency?>("Livret_A", ProductFrequency.SemiAnnual));
            indicesTranscod.Add("OIS", new Tuple<string, ProductFrequency?>("EONIA", ProductFrequency.AtMaturity));
            indicesTranscod.Add("TAG", new Tuple<string, ProductFrequency?>("E3M", ProductFrequency.Monthly));
            indicesTranscod.Add("EUR1M", new Tuple<string, ProductFrequency?>("E1M", ProductFrequency.Monthly));
            indicesTranscod.Add("EUR3M", new Tuple<string, ProductFrequency?>("E3M", ProductFrequency.Quarterly));
            indicesTranscod.Add("EUR6M", new Tuple<string, ProductFrequency?>("E6M", ProductFrequency.SemiAnnual));
            indicesTranscod.Add("EUR12M", new Tuple<string, ProductFrequency?>("E1Y", ProductFrequency.Annual));
            indicesTranscod.Add("LIVRET A", new Tuple<string, ProductFrequency?>("Livret_A", ProductFrequency.SemiAnnual));
            indicesTranscod.Add("taux_OIS ('EONIA','EUR')", new Tuple<string, ProductFrequency?>("EONIA", ProductFrequency.AtMaturity));
            indicesTranscod.Add("(TRBO+B5)/2", new Tuple<string, ProductFrequency?>("TV", null));
            indicesTranscod.Add("EONIA (OU TEMPE)", new Tuple<string, ProductFrequency?>("EONIA", ProductFrequency.AtMaturity));
            indicesTranscod.Add("EURIBOR 1 MOIS JOUR", new Tuple<string, ProductFrequency?>("E1M", ProductFrequency.Monthly));
            indicesTranscod.Add("EURIBOR 12 MOIS JOUR", new Tuple<string, ProductFrequency?>("E1Y", ProductFrequency.Annual));
            indicesTranscod.Add("EURIBOR 12M MOYEN MENSUEL", new Tuple<string, ProductFrequency?>("E1Y", ProductFrequency.Annual));
            indicesTranscod.Add("EURIBOR 1M MOYEN MENSUEL", new Tuple<string, ProductFrequency?>("E1M", ProductFrequency.Monthly));
            indicesTranscod.Add("EURIBOR 2 MOIS JOUR", new Tuple<string, ProductFrequency?>("E1M", ProductFrequency.Monthly));
            indicesTranscod.Add("EURIBOR 3 MOIS JOUR", new Tuple<string, ProductFrequency?>("E3M", ProductFrequency.Quarterly));
            indicesTranscod.Add("EURIBOR 3M MOYEN MENSUEL", new Tuple<string, ProductFrequency?>("E3M", ProductFrequency.Quarterly));
            indicesTranscod.Add("EURIBOR 6 MOIS JOURS", new Tuple<string, ProductFrequency?>("E6M", ProductFrequency.SemiAnnual));
            indicesTranscod.Add("EURIBOR 6M MOYEN MENSUEL", new Tuple<string, ProductFrequency?>("E6M", ProductFrequency.SemiAnnual));
            indicesTranscod.Add("LDD", new Tuple<string, ProductFrequency?>("Livret_A", ProductFrequency.SemiAnnual));
            indicesTranscod.Add("T E C 10 MOYEN", new Tuple<string, ProductFrequency?>("TEC10", null));
            indicesTranscod.Add("T.A.G. TAUX ANNUEL GLISSANT", new Tuple<string, ProductFrequency?>("E1M", ProductFrequency.Monthly));
            indicesTranscod.Add("TAUX VARIABLE LIBRE", new Tuple<string, ProductFrequency?>("TV", null));
            indicesTranscod.Add("TEC 5 MENSUEL MOYENNE", new Tuple<string, ProductFrequency?>("TEC10", null));
            indicesTranscod.Add("TMM (OU T4M)", new Tuple<string, ProductFrequency?>("EONIA", ProductFrequency.AtMaturity));
            indicesTranscod.Add("TRCAM", new Tuple<string, ProductFrequency?>("TV", null));
            indicesTranscod.Add("TAM", new Tuple<string, ProductFrequency?>("E1M", ProductFrequency.Monthly));

            marketDataTranscod.Clear();
            marketDataTranscod.Add("U", new Tuple<string, ProductFrequency>("E1Y", ProductFrequency.AtMaturity));
            marketDataTranscod.Add("A", new Tuple<string, ProductFrequency>("E1Y", ProductFrequency.Annual));
            marketDataTranscod.Add("S", new Tuple<string, ProductFrequency>("E6M", ProductFrequency.SemiAnnual));
            marketDataTranscod.Add("T", new Tuple<string, ProductFrequency>("E3M", ProductFrequency.Quarterly));
            marketDataTranscod.Add("M", new Tuple<string, ProductFrequency>("E1M", ProductFrequency.Monthly));
            marketDataTranscod.Add("NA", new Tuple<string, ProductFrequency>("E1M", ProductFrequency.Monthly));

        }


        private static SqlDataReader executeQuery(string sqlQuery, SqlCommand myCommand, SqlConnection myConnection)
        {
            SqlDataReader myReader = null;

            myCommand = new SqlCommand(sqlQuery,
                                                  myConnection);
            myReader = myCommand.ExecuteReader();
            return myReader;
        }
        private static void traiterEPARGNE(SqlDataReader ligne, ISession session)
        {
            Product np = new Product();
            Product np2 = new Product();

            Category category;
            Tuple<string, double, bool> epargneCategoryTuple;
            if (epargneCategories.ContainsKey(ligne["PRODUIT"].ToString()))
            {
                epargneCategoryTuple = epargneCategories[ligne["PRODUIT"].ToString()];
            }
            else
            {
                epargneCategoryTuple = new Tuple<string, double, bool>(autresEpargne.FullPath() + "/" + ligne["PRODUIT"].ToString(), 0, false);
            }
            np.Category = EnsureCategoryExists(session, portfolio, epargneCategoryTuple.Item1);
            np2.Category = EnsureCategoryExists(session, portfolio, avance + epargneCategoryTuple.Item1);

            np.ProductType = ProductType.BondFr;
            np2.ProductType = np.ProductType;

            np.Description = ligne["PRODUIT"].ToString();
            np2.Description = np.Description;

            np.Exclude = false;
            np2.Exclude = false;
            np.IsModel = false;
            np2.IsModel = false;

            np.AmortizationType = ProductAmortizationType.BULLET;
            np2.AmortizationType = ProductAmortizationType.BULLET;

            np.Amortization = "0";
            np2.Amortization = "0";
            np.Complement = 0;
            np2.Complement = 0;
            np.InvestmentRule = ProductInvestmentRule.Growth;
            np2.InvestmentRule = np.InvestmentRule;
            //=IF($B2="Balance Sheet/Liabilities/Collecte clientèle/Dépôts sans coût (PME et Particuliers)/Epargne logement PEL/CEL";"growth_epargne_logement";"growth_epargne_centralisee_hors_epargne_logement")
            if (epargneCategoryTuple.Item1 ==
                "Balance Sheet/Liabilities/Collecte clientèle/Dépôts sans coût (PME et Particuliers)/Epargne logement PEL/CEL")
            {
                np.InvestmentParameter = "growth_epargne_logement";
            }
            else
            {
                np.InvestmentParameter = "growth_epargne_centralisee_hors_epargne_logement";
            }
            np2.InvestmentParameter = np.InvestmentParameter;
            np.Accounting = ProductAccounting.LAndR;
            np2.Accounting = np.Accounting;
            double origFace = (double.Parse(ligne["ENCOURS"].ToString()) / 1000 * (1 - epargneCategoryTuple.Item2));
            np.OrigFace = origFace.ToString();
            np2.OrigFace = (-1 * origFace / 2).ToString();

            np.BookPrice = "100";
            np2.BookPrice = np.BookPrice;

            np.ActuarialPrice = "100";
            np2.ActuarialPrice = np.ActuarialPrice;

            np.RollSpec = Product.NoRoll;
            np2.RollSpec = np.RollSpec;

            np.RollRatio = "1";
            np2.RollRatio = np.RollRatio;
            
            double taux =0;
            if (ligne["TAUX"].ToString()!="")
                taux= (double.Parse(ligne["TAUX"].ToString()) / 100);
            if (!epargneCategoryTuple.Item3)
            {
                np.Coupon = taux.ToString();
            }
            else
            {
                if (taux > LIVRET_A)
                {
                    np.Coupon = "Livret_A+" + Math.Abs(taux - LIVRET_A);
                }
                else
                {
                    np.Coupon = "Livret_A-" + Math.Abs(taux - LIVRET_A);
                }
            }
            np2.Coupon = np.Coupon;

            np.CouponType = CouponType.Fixed;
            np2.CouponType = np.CouponType;
            //=IF(DAT!$E8="000000";999;YEARFRAC(DAT!$C$3;VALUE("01/"&RIGHT(DAT!$E8;2)&"/"&LEFT(DAT!$E8;4)))*12)

            if (ligne["DUREE"].ToString() == "999")
            {
                np.Duration = Util.InfiniteDuration;
            }
            else
            {
                if (ligne["DUREE"].ToString() != "")
                    np.Duration = (int) double.Parse(ligne["DUREE"].ToString().Replace(".", ","));
                        //YearFrac(valuationDate, DateTime.Parse("01/" + ligne["ECHEANCE"].ToString().Substring(4, 2) + "/" + ligne["ECHEANCE"].ToString().Substring(0, 4))) * 12;
                else
                    np.Duration = 0;
            }
            np2.Duration = np.Duration;
            //=IF(Z2<12;"ACT360";"ACT365")

            np.CouponBasis = CouponBasis.A30360;
            np2.CouponBasis = np.CouponBasis;

            np.CouponCalcType = CouponCalcType.InAdvance;
            np2.CouponCalcType = np.CouponCalcType;

            np.Frequency = ProductFrequency.Monthly;
            np2.Frequency = np.Frequency;

            np.Tax = ProductTax.Standard;
            np2.Tax = np.Tax;

            np.Attrib = ProductAttrib.InBalance;
            np2.Attrib = np.Attrib;

            np.Currency = EUR;
            np2.Currency = EUR;
            np.RWA = 0;
            np2.RWA = 0;
            np.LiquidityEligibility = LiquidityEligibility.NotEligible;
            np2.LiquidityEligibility = LiquidityEligibility.NotEligible;
            np.LiquidityHaircut = 0;
            np2.LiquidityHaircut = 0;
            np.LiquidityRunOff = 0.05;
            np2.LiquidityRunOff = 0.05;
            np.Category.Bonds.Add(np);
            np2.Category.Bonds.Add(np2);
            np.Create(session);
            np2.Create(session);


        }

        private static void traiterDAT(SqlDataReader ligne, ISession session)
        {
            Product np = new Product();

            Category category;

            if (ligne["VALEUR"].ToString() == "000000" || ligne["ECHEANCE"].ToString() == "000000")
            {//=IF(OR(DAT!$D8="000000";DAT!$E8="000000");"Balance Sheet/Liabilities/Collecte clientèle/Dépôts sans coût (PME et Particuliers)/DAT CT (DI <= 1 an)"

                category = EnsureCategoryExists(session, depotsSansCouts, "DAT CT (DI <= 1 an)");
            }
            else
            {//IF(YEARFRAC(VALUE("01/"&RIGHT(DAT!$D8;2)&"/"&LEFT(DAT!$D8;4));VALUE("01/"&RIGHT(DAT!$E8;2)&"/"&LEFT(DAT!$E8;4)))*12>12;"Balance Sheet/Liabilities/Collecte clientèle/Dépôts avec coût (PME et Particuliers)/DAT MLT (DI > 1 an)";"Balance Sheet/Liabilities/Collecte clientèle/Dépôts sans coût (PME et Particuliers)/DAT CT (DI <= 1 an)"))

                DateTime d1 = DateTime.Parse("01/" + ligne["VALEUR"].ToString().Substring(4, 2) + "/" + ligne["VALEUR"].ToString().Substring(0, 4));
                DateTime d2 = DateTime.Parse("01/" + ligne["ECHEANCE"].ToString().Substring(4, 2) + "/" + ligne["ECHEANCE"].ToString().Substring(0, 4));

                if (YearFrac(d1, d2) * 12 > 12)
                {
                    category = EnsureCategoryExists(session, depotsSansCouts, "DAT MLT (DI > 1 an)");
                }
                else
                {
                    category = EnsureCategoryExists(session, depotsSansCouts, "DAT CT (DI <= 1 an)");
                }
            }
            np.Category = category;
            np.ProductType = ProductType.BondFr;
            np.Description = ligne["LIBELLE"].ToString();
            np.Exclude = false;
            np.IsModel = true;
            np.AmortizationType = ProductAmortizationType.BULLET;
            np.Amortization = "0";
            np.Complement = 0;
            np.InvestmentRule = ProductInvestmentRule.Constant;
            np.Accounting = ProductAccounting.LAndR;
            np.OrigFace = ((int)(double.Parse(ligne["ENCOURS"].ToString()) / 1000)).ToString();
            np.BookPrice = "100";
            np.ActuarialPrice = "100";
            np.RollSpec = "R00010";
            np.RollRatio = "growth_DAT";
            np.Coupon = (double.Parse(ligne["TAUX"].ToString()) / 100).ToString();
            np.CouponType = CouponType.Fixed;
            //=IF(DAT!$E8="000000";999;YEARFRAC(DAT!$C$3;VALUE("01/"&RIGHT(DAT!$E8;2)&"/"&LEFT(DAT!$E8;4)))*12)

            if (ligne["ECHEANCE"].ToString() == "000000")
            {
                np.Duration = Util.InfiniteDuration;
            }
            else
            {
                try
                {
                    np.Duration = YearFrac(valuationDate, DateTime.Parse("01/" + ligne["ECHEANCE"].ToString().Substring(4, 2) + "/" + ligne["ECHEANCE"].ToString().Substring(0, 4))) * 12;
                }
                catch (Exception)
                {
                    np.Duration = 0;

                }
                
            }
            //=IF(Z2<12;"ACT360";"ACT365")
            if (np.Duration < 12)
            {
                np.CouponBasis = CouponBasis.ACT360;
            }
            else
            {
                np.CouponBasis = CouponBasis.ACT365;
            }
            np.CouponCalcType = CouponCalcType.InAdvance;
            np.Frequency = ProductFrequency.AtMaturity;
            np.Tax = ProductTax.Standard;
            np.Attrib = ProductAttrib.InBalance;
            np.CPR = "RA_DAT";
            np.Currency = EUR;
            np.RWA = 0;
            np.LiquidityEligibility = LiquidityEligibility.NotEligible;
            np.LiquidityHaircut = 0;
            np.LiquidityRunOff = 1;
            category.Bonds.Add(np);
            np.Create(session);

        }

        private static void traiterCREDITS(SqlDataReader ligne, ISession session, int id)
        {
            Product np = new Product();

            np.ALMID = id + 1;

            np.Category = creditsCategory;
            if (ligne["Nature Taux"]=="VAR")
                np.ProductType = ProductType.BondVr;
            else
                np.ProductType = ProductType.BondFr;

            string indice = ligne["Indice"].ToString();
            np.Description = ligne["Nature Taux"].ToString() + " - " + indice;

            np.Exclude = false;
            np.IsModel = true;

            np.AmortizationType = ligne["Amortissement"].ToString()==""?ProductAmortizationType.BULLET: creditsAmortTrancod[ligne["Amortissement"].ToString()];

            np.Amortization = "0";
            np.Complement = 0;
            np.InvestmentRule = ProductInvestmentRule.Constant;
            np.Accounting = ProductAccounting.LAndR;


            np.OrigFace = ((int)(double.Parse(ligne["CRD"].ToString()))).ToString();
            np.BookPrice = "100";
            np.ActuarialPrice = "100";
            np.RollSpec = "R00002";
            np.RollRatio = "growth_conso";

            /*
 * IF('Crédits Hors G2'!E10<>"VAR";
 *      'Crédits Hors G2'!J10/100;
 * ELSE
 *      "max("&'Crédits Hors G2'!O10/100&",
 *          "&IF('Crédits Hors G2'!N10>0;
 *                  "min("&'Crédits Hors G2'!N10/100&","&
 *                          IF(ISERROR(VLOOKUP('Crédits Hors G2'!G10;tInd;2;0));
 *                                  VLOOKUP('Crédits Hors G2'!F10;tFreq;3;0);
 *                          ELSE
 *                                  IF(VLOOKUP('Crédits Hors G2'!G10;tInd;2;0)="TV";
 *                                          VLOOKUP('Crédits Hors G2'!F10;tFreq;3;0);
 *                                  ELSE
 *                                          VLOOKUP('Crédits Hors G2'!G10;tInd;2;0)
 *                                  )
 *                         )
 *                         &IF('Crédits Hors G2'!K10>=0;
                                                   "+";
                            ELSE
                                                   "-"
 *                          )
                            &ABS('Crédits Hors G2'!K10/100)&"))";
 *          ELSE
 *                          IF(ISERROR(VLOOKUP('Crédits Hors G2'!G10;tInd;2;0));
 *                                  VLOOKUP('Crédits Hors G2'!F10;tFreq;3;0);
 *                          ELSE
 *                                  IF(VLOOKUP('Crédits Hors G2'!G10;tInd;2;0)="TV";
 *                                          VLOOKUP('Crédits Hors G2'!F10;tFreq;3;0);
 *                                          ELSE
 *                                          VLOOKUP('Crédits Hors G2'!G10;tInd;2;0)
 *                                   )
 *                          )
 *                          &IF('Crédits Hors G2'!K10>=0;
 *                                           "+";
 *                                           ELSE
 *                                           "-"
 *                          )
 *                          &ABS('Crédits Hors G2'!K10/100)&")"))
 * */
            string frequence = ligne["Fréquence"].ToString();
            string floorString = ZeroIfVoid(ligne["Floor"].ToString());
            int floor = ((int)(double.Parse(floorString)));
            double margeSurIndice = double.Parse(ZeroIfVoid(ligne["Marge sur Indice"].ToString())) / 100;
            if (ligne["Nature Taux"].ToString() != "VAR")
                np.Coupon = (double.Parse(ZeroIfVoid(ligne["Taux Client"].ToString())) / 100).ToString();
            else
            {
                np.Coupon = "max(" + ((int) (double.Parse(floorString)/1000)).ToString()+",";
                
                if (floor>0)
                {
                    np.Coupon += "min(" + (floor/100).ToString() + ",";
                    
                    if (!indicesTranscod.ContainsKey(indice))
                    {
                        np.Coupon += marketDataTranscod[frequence].Item1;
                    }
                    else
                    {
                        if (indicesTranscod[indice].Item1 == "TV")
                            np.Coupon += marketDataTranscod[frequence].Item1;
                        else
                            np.Coupon += indicesTranscod[indice].Item1;
                    }
                    
                    if (margeSurIndice >= 0)
                        np.Coupon += "+";
                    else
                        np.Coupon += "-";
                    np.Coupon += Math.Abs(margeSurIndice).ToString()+"))";

                }
                else
                {
                    if (!indicesTranscod.ContainsKey(indice))
                    {
                        np.Coupon += marketDataTranscod[frequence].Item1;
                    }
                    else
                    {
                        if (indicesTranscod[indice].Item1 == "TV")
                            np.Coupon += marketDataTranscod[frequence].Item1;
                        else
                            np.Coupon += indicesTranscod[indice].Item1;
                    }

                    if (margeSurIndice >= 0)
                        np.Coupon += "+";
                    else
                        np.Coupon += "-";
                    np.Coupon += Math.Abs(margeSurIndice).ToString() + ")";
                }
            }
            if (ligne["Option"].ToString() == "SANS")
            {
                if (ligne["Nature Taux"].ToString() != "VAR")
                    np.CouponType = CouponType.Fixed;
                else
                    np.CouponType = CouponType.Floating;
            }
            else
            {
                np.CouponType = CouponType.Variable;
            }

            np.Duration = (int)double.Parse(ligne["DRAC"].ToString());
            np.CouponBasis = CouponBasis.A30360;
            
            np.CouponCalcType = CouponCalcType.InAdvance;

            np.Frequency = marketDataTranscod[frequence].Item2;
            np.Tax = ProductTax.Standard;
            np.Attrib = ProductAttrib.InBalance;
            np.CPR = "coefRA*RA_conso";
            np.CDR = "0,01";
            np.LGD = "0,7";
            np.Basel2 = "0,75";
            np.Currency = EUR;
            np.RWA = 0;
            string tci = ligne["TCI"].ToString();
            if (tci != "")
                np.IRC = tci;
            np.LiquidityEligibility = LiquidityEligibility.NotEligible;
            np.LiquidityHaircut = 0;
            np.LiquidityRunOff = 0.5;
            np.ComputeBookValue(100);
            creditsCategory.Bonds.Add(np);
            np.Create(session);

        }

        private static string ZeroIfVoid(string input)
        {
            return input == "" ? "0" : input;
        }
        private static Tuple<string,string> GetLastCategoryAndPath(string categoryName,string lastPrefix="")
        {
            if (categoryName == "")
                return null;
            int pos = categoryName.IndexOf('/');
            string endOfCategoryName = "";
            if (pos == -1)
            {
                pos = categoryName.Length;
            }
            else
            {
                endOfCategoryName = categoryName.Substring(pos + 1, categoryName.Length - pos - 1);
            }

            string firstSubCategoryLevel = categoryName.Substring(0, pos);
            

            if (endOfCategoryName != "")
            {
                return GetLastCategoryAndPath(endOfCategoryName,lastPrefix+"/"+firstSubCategoryLevel);
            }
            else
            {
                return new Tuple<string, string>(categoryName,lastPrefix);
            }

        }

        private static Category EnsureCategoryExistsRecursively(ISession session, Category parent, string categoryName)
        {
            if (categoryName == "")
                return null;
            int pos = categoryName.IndexOf('/');
            string endOfCategoryName = "";
            if (pos == -1)
            {
                pos = categoryName.Length;

            }
            else
            {
                endOfCategoryName = categoryName.Substring(pos + 1, categoryName.Length - pos - 1);
            }

            string firstSubCategoryLevel = categoryName.Substring(0, pos);
            Category subCateg = EnsureCategoryExists(session, parent, firstSubCategoryLevel);

            if (endOfCategoryName != "")
            {
                return EnsureCategoryExistsRecursively(session, subCateg, endOfCategoryName);
            }
            else
            {
                return subCateg;
            }

        }


        private static Category EnsureCategoryExists(ISession session, Category parent, string categoryName)
        {
            Debug.WriteLine("Creating " + categoryName+" in "+parent.FullPath());
            Category child = parent.Children.FirstOrDefault(c => c.Label == categoryName);
            if (child == null)
            {
                child = new Category();
                child.Label = categoryName;
                child.ReadOnly = true;
                child.Type = CategoryType.Normal;
                child.Parent = parent;
                child.BalanceType = parent.BalanceType;
                child.isVisible = parent.isVisible;
                child.ALMID = Category.GetNewALMID(session)+1;
                parent.Children.Add(child);
                child.Create(session);
            }
            return child;
        }

        public static double YearFrac(DateTime startDate, DateTime endDate)
        {
            if (endDate < startDate)
            {
                throw new ArgumentOutOfRangeException("endDate cannot be less than startDate");
            }

            int endDay = endDate.Day;
            int startDay = startDate.Day;

            switch (startDay)
            {
                case 31:
                    {
                        startDay = 30;
                        if (endDay == 31)
                        {
                            endDay = 30;
                        }
                    } break;

                case 30:
                    {
                        if (endDay == 31)
                        {
                            endDay = 30;
                        }
                    } break;

                case 29:
                    {
                        if (startDate.Month == 2)
                        {
                            startDay = 30;
                            if ((endDate.Month == 2) && (endDate.Day == 28 + (DateTime.IsLeapYear(endDate.Year) ? 1 : 0)))
                            {
                                endDay = 30;
                            }
                        }
                    } break;

                case 28:
                    {
                        if ((startDate.Month == 2) && (!DateTime.IsLeapYear(startDate.Year)))
                        {
                            startDay = 30;
                            if ((endDate.Month == 2) && (endDate.Day == 28 + (DateTime.IsLeapYear(endDate.Year) ? 1 : 0)))
                            {
                                endDay = 30;
                            }
                        }
                    } break;
            }

            return ((endDate.Year - startDate.Year) * 360 + (endDate.Month - startDate.Month) * 30 + (endDay - startDay)) / 360.0;
        }


        private const string SQL_CREDITS = @"SELECT
REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','') AS 'Segment 1',
CASE WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='PRO ENTREPRISES' AND CLE LIKE '%PI%' THEN 'PROMO IMMO' WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='PRO ENTREPRISES' AND CLE LIKE '%AG%' THEN 'AGRI'  WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='PRO ENTREPRISES' AND CLE LIKE '%PE%' THEN 'PROS'  WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='PRO ENTREPRISES' AND CLE LIKE '%CP%' AND CLE <> 'MIGRESCPTE' THEN 'COLL PUB' WHEN (REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='PRO ENTREPRISES' AND CLE NOT LIKE '%AG%' AND CLE NOT LIKE '%PE%' AND CLE NOT LIKE '%CP%') OR CLE ='MIGRESCPTE' THEN 'AUTRES' WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='CONSO' THEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU3)), 'TOTAL ','') WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='HABITAT' AND CLE LIKE '%G2%' THEN 'G2' WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='HABITAT' AND CLE NOT LIKE '%G2%' AND CLE LIKE '%PAS%' THEN 'PRETS ACCESSION SOCIALE HORS G2'  WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='HABITAT' AND CLE NOT LIKE '%G2%' AND CLE LIKE '%PC%' THEN 'PRETS CONVENTIONNES HORS G2'  WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='HABITAT' AND CLE LIKE '%EPL%' THEN 'PEL'  WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='HABITAT' AND CLE LIKE '%PII%' THEN 'PRETS INVESTISSEURS IMMOBILIERS' WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='HABITAT' AND CLE LIKE '%OPT%' THEN 'OPTIPLAN'  WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='HABITAT' AND CLE LIKE '%HBPL%' THEN 'PRETS LOCATIF INTERMEDIAIRE/PRETS LOCATIF SOCIAL'  WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='HABITAT' AND (CLE LIKE '%PAR%' OR CLE LIKE '%PTZ%') THEN 'PRETS TAUX ZERO'  WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='HABITAT' AND CLE NOT LIKE '%G2%' AND CLE LIKE '%PTH%' THEN 'PRETS TOUT HABITAT HORS G2'  WHEN CLE IN ('HBBDF') THEN 'PLAN BDF'  WHEN CLE IN ('HBRELAIS') THEN 'PRETS RELAIS' END AS 'Segment 2',
CASE WHEN CDOPTU=1 THEN 'CAP' WHEN CDOPTU=3 THEN 'COLLAR' ELSE 'SANS' END AS 'Option',
LIBELLE AS 'Amortissement',
CASE WHEN CQTXPR=0 THEN 'N/A' WHEN CQTXPR=1 THEN 'FIXE' ELSE 'VAR' END AS 'Nature Taux',
CASE WHEN CFPEPR=0 THEN 'U' WHEN CFPEPR=5 THEN 'M' WHEN CFPEPR=6 THEN 'T' WHEN CFPEPR=7 THEN 'S' WHEN CFPEPR=8 THEN 'A' ELSE 'N/A' END AS 'Fréquence',
CASE WHEN LTRIM(RTRIM(CTINDXL))='NE S APPLIQUE PAS' THEN 'N/A'  ELSE LTRIM(RTRIM(CTINDXL)) END AS 'Indice',
CAST(SUBSTRING(DA1REA,1,4) AS NUMERIC) AS 'Année',
CASE WHEN SUM(MKDURE*QQRPR3)>0 AND SUM(MKDURE*QQRPR3*(DATEDIFF(MONTH, CONVERT(VARCHAR,$YYYYMMDD$, 112), CONVERT(VARCHAR,DHDNPR,112))))> 0 THEN SUM(MKDURE*QQRPR3*(DATEDIFF(MONTH, CONVERT(VARCHAR,$YYYYMMDD$, 112), CONVERT(VARCHAR,DHDNPR,112))))/SUM(MKDURE*QQRPR3) ELSE 0 END AS 'DRAC',
CASE WHEN SUM(MKDURE*QQRPR3)>0 THEN SUM(MKDURE*QQRPR3*TXCLIENT)/SUM(MKDURE*QQRPR3) ELSE 0 END AS 'Taux Client',
CASE WHEN SUM(MKDURE*QQRPR3)>0 THEN SUM(MKDURE*QQRPR3*MRGIND)/SUM(MKDURE*QQRPR3) ELSE 0 END AS 'Marge sur Indice',
CASE WHEN SUM(MKDURE*QQRPR3)>0 THEN SUM(MKDURE*QQRPR3*TCI)/SUM(MKDURE*QQRPR3) ELSE 0 END AS 'TCI',
CAST(SUM(MKDURE*QQRPR3/100/1000) as varchar) AS 'CRD',
CASE WHEN SUM(MKDURE*QQRPR3)>0 THEN SUM(MKDURE*QQRPR3*TXCAPE)/SUM(MKDURE*QQRPR3) ELSE 0 END AS 'Cap',
CASE WHEN SUM(MKDURE*QQRPR3)>0 THEN SUM(MKDURE*QQRPR3*TXFLOR)/SUM(MKDURE*QQRPR3) ELSE 0 END AS 'Floor'

FROM T_GFI_CREDIT_STOCK 
    LEFT JOIN T_GFI_CREDIT_CONTRATS ON T_GFI_CREDIT_STOCK.IDELCO = T_GFI_CREDIT_CONTRATS.IDELCO 
    LEFT JOIN T_GFI_CREDIT_STRUCTURE ON T_GFI_CREDIT_CONTRATS.CLE_STRUCTURE COLLATE French_CI_AS = T_GFI_CREDIT_STRUCTURE.CLE 
    LEFT JOIN T_GFI_REFERENTIEL_CDLAPR ON CAST(T_GFI_CREDIT_STOCK.CDLAPR AS NUMERIC)= CAST(T_GFI_REFERENTIEL_CDLAPR.CDLAPR AS NUMERIC) 
    LEFT JOIN T_GFI_REFERENTIEL_CTINDX ON T_GFI_CREDIT_STOCK.CTINDX = T_GFI_REFERENTIEL_CTINDX.CTINDX
WHERE 
PERIODE = $YYYYMM$ AND
DA1REA > 19000000 AND
DA1REA < 21000000 AND
DHDNPR > 19000000 AND
DHDNPR < 21000000 AND
LEFT(LTRIM(RTRIM(NCPRRE)),6)<>'131260' 
--AND CLE NOT LIKE '%G2%' 

GROUP BY
REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ',''),
CASE WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='PRO ENTREPRISES' AND CLE LIKE '%PI%' THEN 'PROMO IMMO' WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='PRO ENTREPRISES' AND CLE LIKE '%AG%' THEN 'AGRI'  WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='PRO ENTREPRISES' AND CLE LIKE '%PE%' THEN 'PROS'  WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='PRO ENTREPRISES' AND CLE LIKE '%CP%' AND CLE <> 'MIGRESCPTE' THEN 'COLL PUB' WHEN (REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='PRO ENTREPRISES' AND CLE NOT LIKE '%AG%' AND CLE NOT LIKE '%PE%' AND CLE NOT LIKE '%CP%') OR CLE ='MIGRESCPTE' THEN 'AUTRES' WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='CONSO' THEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU3)), 'TOTAL ','') WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='HABITAT' AND CLE LIKE '%G2%' THEN 'G2' WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='HABITAT' AND CLE NOT LIKE '%G2%' AND CLE LIKE '%PAS%' THEN 'PRETS ACCESSION SOCIALE HORS G2'  WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='HABITAT' AND CLE NOT LIKE '%G2%' AND CLE LIKE '%PC%' THEN 'PRETS CONVENTIONNES HORS G2'  WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='HABITAT' AND CLE LIKE '%EPL%' THEN 'PEL'  WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='HABITAT' AND CLE LIKE '%PII%' THEN 'PRETS INVESTISSEURS IMMOBILIERS' WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='HABITAT' AND CLE LIKE '%OPT%' THEN 'OPTIPLAN'  WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='HABITAT' AND CLE LIKE '%HBPL%' THEN 'PRETS LOCATIF INTERMEDIAIRE/PRETS LOCATIF SOCIAL'  WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='HABITAT' AND (CLE LIKE '%PAR%' OR CLE LIKE '%PTZ%') THEN 'PRETS TAUX ZERO'  WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='HABITAT' AND CLE NOT LIKE '%G2%' AND CLE LIKE '%PTH%' THEN 'PRETS TOUT HABITAT HORS G2'  WHEN CLE IN ('HBBDF') THEN 'PLAN BDF'  WHEN CLE IN ('HBRELAIS') THEN 'PRETS RELAIS' END,
CASE WHEN CDOPTU=1 THEN 'CAP' WHEN CDOPTU=3 THEN 'COLLAR' ELSE 'SANS' END,
LIBELLE,
CASE WHEN CQTXPR=0 THEN 'N/A' WHEN CQTXPR=1 THEN 'FIXE' ELSE 'VAR' END,
CASE WHEN CFPEPR=0 THEN 'U' WHEN CFPEPR=5 THEN 'M' WHEN CFPEPR=6 THEN 'T' WHEN CFPEPR=7 THEN 'S' WHEN CFPEPR=8 THEN 'A' ELSE 'N/A' END,
CASE WHEN LTRIM(RTRIM(CTINDXL))='NE S APPLIQUE PAS' THEN 'N/A'  ELSE LTRIM(RTRIM(CTINDXL)) END,
CAST(SUBSTRING(DA1REA,1,4) AS NUMERIC)";

        private const string SQL_CREDITS_HORS_G2 = @"--Crédits Hors G2
SELECT
REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','') AS 'Segment 1',
CASE WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='PRO ENTREPRISES' AND CLE LIKE '%PI%' THEN 'PROMO IMMO' WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='PRO ENTREPRISES' AND CLE LIKE '%AG%' THEN 'AGRI'  WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='PRO ENTREPRISES' AND CLE LIKE '%PE%' THEN 'PROS'  WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='PRO ENTREPRISES' AND CLE LIKE '%CP%' AND CLE <> 'MIGRESCPTE' THEN 'COLL PUB' WHEN (REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='PRO ENTREPRISES' AND CLE NOT LIKE '%AG%' AND CLE NOT LIKE '%PE%' AND CLE NOT LIKE '%CP%') OR CLE ='MIGRESCPTE' THEN 'AUTRES' WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='CONSO' THEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU3)), 'TOTAL ','') WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='HABITAT' AND CLE LIKE '%G2%' THEN 'G2' WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='HABITAT' AND CLE NOT LIKE '%G2%' AND CLE LIKE '%PAS%' THEN 'PRETS ACCESSION SOCIALE HORS G2'  WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='HABITAT' AND CLE NOT LIKE '%G2%' AND CLE LIKE '%PC%' THEN 'PRETS CONVENTIONNES HORS G2'  WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='HABITAT' AND CLE LIKE '%EPL%' THEN 'PEL'  WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='HABITAT' AND CLE LIKE '%PII%' THEN 'PRETS INVESTISSEURS IMMOBILIERS' WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='HABITAT' AND CLE LIKE '%OPT%' THEN 'OPTIPLAN'  WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='HABITAT' AND CLE LIKE '%HBPL%' THEN 'PRETS LOCATIF INTERMEDIAIRE/PRETS LOCATIF SOCIAL'  WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='HABITAT' AND (CLE LIKE '%PAR%' OR CLE LIKE '%PTZ%') THEN 'PRETS TAUX ZERO'  WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='HABITAT' AND CLE NOT LIKE '%G2%' AND CLE LIKE '%PTH%' THEN 'PRETS TOUT HABITAT HORS G2'  WHEN CLE IN ('HBBDF') THEN 'PLAN BDF'  WHEN CLE IN ('HBRELAIS') THEN 'PRETS RELAIS' END AS 'Segment 2',
CASE WHEN CDOPTU=1 THEN 'CAP' WHEN CDOPTU=3 THEN 'COLLAR' ELSE 'SANS' END AS 'Option',
LIBELLE AS 'Amortissement',
CASE WHEN CQTXPR=0 THEN 'N/A' WHEN CQTXPR=1 THEN 'FIXE' ELSE 'VAR' END AS 'Nature Taux',
CASE WHEN CFPEPR=0 THEN 'U' WHEN CFPEPR=5 THEN 'M' WHEN CFPEPR=6 THEN 'T' WHEN CFPEPR=7 THEN 'S' WHEN CFPEPR=8 THEN 'A' ELSE 'N/A' END AS 'Fréquence',
CASE WHEN LTRIM(RTRIM(CTINDXL))='NE S APPLIQUE PAS' THEN 'N/A'  ELSE LTRIM(RTRIM(CTINDXL)) END AS 'Indice',
CAST(SUBSTRING(DA1REA,1,4) AS NUMERIC) AS 'Année',
CASE WHEN SUM(MKDURE*QQRPR3)>0 AND SUM(MKDURE*QQRPR3*(DATEDIFF(MONTH, CONVERT(VARCHAR,$YYYYMMDD$, 112), CONVERT(VARCHAR,DHDNPR,112))))> 0 THEN SUM(MKDURE*QQRPR3*(DATEDIFF(MONTH, CONVERT(VARCHAR,$YYYYMMDD$, 112), CONVERT(VARCHAR,DHDNPR,112))))/SUM(MKDURE*QQRPR3) ELSE 0 END AS 'DRAC',
CASE WHEN SUM(MKDURE*QQRPR3)>0 THEN SUM(MKDURE*QQRPR3*TXCLIENT)/SUM(MKDURE*QQRPR3) ELSE 0 END AS 'Taux Client',
CASE WHEN SUM(MKDURE*QQRPR3)>0 THEN SUM(MKDURE*QQRPR3*MRGIND)/SUM(MKDURE*QQRPR3) ELSE 0 END AS 'Marge sur Indice',
CASE WHEN SUM(MKDURE*QQRPR3)>0 THEN SUM(MKDURE*QQRPR3*TCI)/SUM(MKDURE*QQRPR3) ELSE 0 END AS 'TCI',
SUM(MKDURE*QQRPR3/100) AS 'CRD',
CASE WHEN SUM(MKDURE*QQRPR3)>0 THEN SUM(MKDURE*QQRPR3*TXCAPE)/SUM(MKDURE*QQRPR3) ELSE 0 END AS 'Cap',
CASE WHEN SUM(MKDURE*QQRPR3)>0 THEN SUM(MKDURE*QQRPR3*TXFLOR)/SUM(MKDURE*QQRPR3) ELSE 0 END AS 'Floor'

FROM T_GFI_CREDIT_STOCK 
    JOIN T_GFI_CREDIT_CONTRATS ON T_GFI_CREDIT_STOCK.IDELCO = T_GFI_CREDIT_CONTRATS.IDELCO 
    JOIN T_GFI_CREDIT_STRUCTURE ON T_GFI_CREDIT_CONTRATS.CLE_STRUCTURE COLLATE French_CI_AS = T_GFI_CREDIT_STRUCTURE.CLE 
    JOIN T_GFI_REFERENTIEL_CDLAPR ON CAST(T_GFI_CREDIT_STOCK.CDLAPR AS NUMERIC)= CAST(T_GFI_REFERENTIEL_CDLAPR.CDLAPR AS NUMERIC) 
    JOIN T_GFI_REFERENTIEL_CTINDX ON T_GFI_CREDIT_STOCK.CTINDX = T_GFI_REFERENTIEL_CTINDX.CTINDX
WHERE 
PERIODE = $YYYYMM$ AND
DA1REA > 19000000 AND
DA1REA < 21000000 AND
DHDNPR > 19000000 AND
DHDNPR < 21000000 AND
LEFT(LTRIM(RTRIM(NCPRRE)),6)<>'131260' AND
CLE NOT LIKE '%G2%' 

GROUP BY
REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ',''),
CASE WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='PRO ENTREPRISES' AND CLE LIKE '%PI%' THEN 'PROMO IMMO' WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='PRO ENTREPRISES' AND CLE LIKE '%AG%' THEN 'AGRI'  WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='PRO ENTREPRISES' AND CLE LIKE '%PE%' THEN 'PROS'  WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='PRO ENTREPRISES' AND CLE LIKE '%CP%' AND CLE <> 'MIGRESCPTE' THEN 'COLL PUB' WHEN (REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='PRO ENTREPRISES' AND CLE NOT LIKE '%AG%' AND CLE NOT LIKE '%PE%' AND CLE NOT LIKE '%CP%') OR CLE ='MIGRESCPTE' THEN 'AUTRES' WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='CONSO' THEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU3)), 'TOTAL ','') WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='HABITAT' AND CLE LIKE '%G2%' THEN 'G2' WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='HABITAT' AND CLE NOT LIKE '%G2%' AND CLE LIKE '%PAS%' THEN 'PRETS ACCESSION SOCIALE HORS G2'  WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='HABITAT' AND CLE NOT LIKE '%G2%' AND CLE LIKE '%PC%' THEN 'PRETS CONVENTIONNES HORS G2'  WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='HABITAT' AND CLE LIKE '%EPL%' THEN 'PEL'  WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='HABITAT' AND CLE LIKE '%PII%' THEN 'PRETS INVESTISSEURS IMMOBILIERS' WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='HABITAT' AND CLE LIKE '%OPT%' THEN 'OPTIPLAN'  WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='HABITAT' AND CLE LIKE '%HBPL%' THEN 'PRETS LOCATIF INTERMEDIAIRE/PRETS LOCATIF SOCIAL'  WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='HABITAT' AND (CLE LIKE '%PAR%' OR CLE LIKE '%PTZ%') THEN 'PRETS TAUX ZERO'  WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='HABITAT' AND CLE NOT LIKE '%G2%' AND CLE LIKE '%PTH%' THEN 'PRETS TOUT HABITAT HORS G2'  WHEN CLE IN ('HBBDF') THEN 'PLAN BDF'  WHEN CLE IN ('HBRELAIS') THEN 'PRETS RELAIS' END,
CASE WHEN CDOPTU=1 THEN 'CAP' WHEN CDOPTU=3 THEN 'COLLAR' ELSE 'SANS' END,
LIBELLE,
CASE WHEN CQTXPR=0 THEN 'N/A' WHEN CQTXPR=1 THEN 'FIXE' ELSE 'VAR' END,
CASE WHEN CFPEPR=0 THEN 'U' WHEN CFPEPR=5 THEN 'M' WHEN CFPEPR=6 THEN 'T' WHEN CFPEPR=7 THEN 'S' WHEN CFPEPR=8 THEN 'A' ELSE 'N/A' END,
CASE WHEN LTRIM(RTRIM(CTINDXL))='NE S APPLIQUE PAS' THEN 'N/A'  ELSE LTRIM(RTRIM(CTINDXL)) END,
CAST(SUBSTRING(DA1REA,1,4) AS NUMERIC)";
        private const string SQL_CREDITS_G2 = @"--Crédits G2
SELECT
REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','') AS 'Segment 1',
CASE WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='PRO ENTREPRISES' AND CLE LIKE '%PI%' THEN 'PROMO IMMO' WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='PRO ENTREPRISES' AND CLE LIKE '%AG%' THEN 'AGRI'  WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='PRO ENTREPRISES' AND CLE LIKE '%PE%' THEN 'PROS'  WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='PRO ENTREPRISES' AND CLE LIKE '%CP%' AND CLE <> 'MIGRESCPTE' THEN 'COLL PUB' WHEN (REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='PRO ENTREPRISES' AND CLE NOT LIKE '%AG%' AND CLE NOT LIKE '%PE%' AND CLE NOT LIKE '%CP%') OR CLE ='MIGRESCPTE' THEN 'AUTRES' WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='CONSO' THEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU3)), 'TOTAL ','') WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='HABITAT' AND CLE LIKE '%G2%' THEN 'G2' WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='HABITAT' AND CLE NOT LIKE '%G2%' AND CLE LIKE '%PAS%' THEN 'PRETS ACCESSION SOCIALE HORS G2'  WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='HABITAT' AND CLE NOT LIKE '%G2%' AND CLE LIKE '%PC%' THEN 'PRETS CONVENTIONNES HORS G2'  WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='HABITAT' AND CLE LIKE '%EPL%' THEN 'PEL'  WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='HABITAT' AND CLE LIKE '%PII%' THEN 'PRETS INVESTISSEURS IMMOBILIERS' WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='HABITAT' AND CLE LIKE '%OPT%' THEN 'OPTIPLAN'  WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='HABITAT' AND CLE LIKE '%HBPL%' THEN 'PRETS LOCATIF INTERMEDIAIRE/PRETS LOCATIF SOCIAL'  WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='HABITAT' AND (CLE LIKE '%PAR%' OR CLE LIKE '%PTZ%') THEN 'PRETS TAUX ZERO'  WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='HABITAT' AND CLE NOT LIKE '%G2%' AND CLE LIKE '%PTH%' THEN 'PRETS TOUT HABITAT HORS G2'  WHEN CLE IN ('HBBDF') THEN 'PLAN BDF'  WHEN CLE IN ('HBRELAIS') THEN 'PRETS RELAIS' END AS 'Segment 2',
CASE WHEN CDOPTU=1 THEN 'CAP' WHEN CDOPTU=3 THEN 'COLLAR' ELSE 'SANS' END AS 'Option',
LIBELLE AS 'Amortissement',
CASE WHEN CQTXPR=0 THEN 'N/A' WHEN CQTXPR=1 THEN 'FIXE' ELSE 'VAR' END AS 'Nature Taux',
CASE WHEN CFPEPR=0 THEN 'U' WHEN CFPEPR=5 THEN 'M' WHEN CFPEPR=6 THEN 'T' WHEN CFPEPR=7 THEN 'S' WHEN CFPEPR=8 THEN 'A' ELSE 'N/A' END AS 'Fréquence',
CASE WHEN LTRIM(RTRIM(CTINDXL))='NE S APPLIQUE PAS' THEN 'N/A'  ELSE LTRIM(RTRIM(CTINDXL)) END AS 'Indice',
CAST(SUBSTRING(DA1REA,1,4) AS NUMERIC) AS 'Année',
CASE WHEN SUM(MKDURE*QQRPR3)<> 0 AND SUM(MKDURE*QQRPR3*DATEDIFF(MONTH, CONVERT(VARCHAR,$YYYYMMDD$, 112),CONVERT(VARCHAR,DATEADD(MONTH, PDNEUT, CONVERT(VARCHAR, DHZEPR,112)),112))) >0 AND SUM(MKDURE*QQRPR3*DATEDIFF(MONTH,CONVERT(VARCHAR,DATEADD(MONTH, PDNEUT, CONVERT(VARCHAR, DHZEPR,112)),112), CONVERT(VARCHAR, DHDNPR,112))) >0 THEN SUM(MKDURE*QQRPR3*DATEDIFF(MONTH, CONVERT(VARCHAR,$YYYYMMDD$, 112),CONVERT(VARCHAR,DATEADD(MONTH, PDNEUT, CONVERT(VARCHAR, DHZEPR,112)),112)))/SUM(MKDURE*QQRPR3) ELSE 0 END AS 'DRAC TF',
CASE WHEN SUM(MKDURE*QQRPR3)>0 THEN SUM(MKDURE*QQRPR3*TXCLIENT)/SUM(MKDURE*QQRPR3) ELSE 0 END AS 'Taux Client',
CASE WHEN SUM(MKDURE*QQRPR3)>0 THEN SUM(MKDURE*QQRPR3*MRGIND)/SUM(MKDURE*QQRPR3) ELSE 0 END AS 'Marge sur Indice',
CASE WHEN SUM(MKDURE*QQRPR3)>0 THEN SUM(MKDURE*QQRPR3*TCI)/SUM(MKDURE*QQRPR3) ELSE 0 END AS 'TCI',
SUM(MKDURE*QQRPR3/100) AS 'CRD',
CASE WHEN SUM(MKDURE*QQRPR3)<> 0 AND SUM(MKDURE*QQRPR3*DATEDIFF(MONTH, CONVERT(VARCHAR,$YYYYMMDD$, 112),CONVERT(VARCHAR,DHDNPR,112))) > 0 AND SUM(MKDURE*QQRPR3*DATEDIFF(MONTH,CONVERT(VARCHAR,DATEADD(MONTH, PDNEUT, CONVERT(VARCHAR, DHZEPR,112)),112),CONVERT(VARCHAR,DHDNPR,112))) > 0 THEN SUM(MKDURE*QQRPR3*DATEDIFF(MONTH, CONVERT(VARCHAR,$YYYYMMDD$, 112),CONVERT(VARCHAR,DHDNPR,112)))/SUM(MKDURE*QQRPR3) ELSE 0 END AS 'DRAC',
CASE WHEN SUM(MKDURE*QQRPR3)>0 THEN SUM(MKDURE*QQRPR3*TXCAPE)/SUM(MKDURE*QQRPR3) ELSE 0 END AS 'Cap',
CASE WHEN SUM(MKDURE*QQRPR3)>0 THEN SUM(MKDURE*QQRPR3*TXFLOR)/SUM(MKDURE*QQRPR3) ELSE 0 END AS 'Floor'

FROM T_GFI_CREDIT_STOCK JOIN T_GFI_CREDIT_CONTRATS ON T_GFI_CREDIT_STOCK.IDELCO = T_GFI_CREDIT_CONTRATS.IDELCO JOIN T_GFI_CREDIT_STRUCTURE ON T_GFI_CREDIT_CONTRATS.CLE_STRUCTURE COLLATE French_CI_AS = T_GFI_CREDIT_STRUCTURE.CLE JOIN T_GFI_REFERENTIEL_CDLAPR ON CAST(T_GFI_CREDIT_STOCK.CDLAPR AS NUMERIC)= CAST(T_GFI_REFERENTIEL_CDLAPR.CDLAPR AS NUMERIC) JOIN T_GFI_REFERENTIEL_CTINDX ON T_GFI_CREDIT_STOCK.CTINDX = T_GFI_REFERENTIEL_CTINDX.CTINDX
WHERE 
PERIODE = $YYYYMM$ AND
DA1REA > 19000000 AND
DA1REA < 21000000 AND
DHDNPR > 19000000 AND
DHDNPR < 21000000 AND
DHZEPR > 19000000 AND
DHZEPR < 21000000 AND
LEFT(LTRIM(RTRIM(NCPRRE)),6)<>'131260' AND
CLE LIKE '%G2%' 

GROUP BY
REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ',''),
CASE WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='PRO ENTREPRISES' AND CLE LIKE '%PI%' THEN 'PROMO IMMO' WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='PRO ENTREPRISES' AND CLE LIKE '%AG%' THEN 'AGRI'  WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='PRO ENTREPRISES' AND CLE LIKE '%PE%' THEN 'PROS'  WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='PRO ENTREPRISES' AND CLE LIKE '%CP%' AND CLE <> 'MIGRESCPTE' THEN 'COLL PUB' WHEN (REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='PRO ENTREPRISES' AND CLE NOT LIKE '%AG%' AND CLE NOT LIKE '%PE%' AND CLE NOT LIKE '%CP%') OR CLE ='MIGRESCPTE' THEN 'AUTRES' WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='CONSO' THEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU3)), 'TOTAL ','') WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='HABITAT' AND CLE LIKE '%G2%' THEN 'G2' WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='HABITAT' AND CLE NOT LIKE '%G2%' AND CLE LIKE '%PAS%' THEN 'PRETS ACCESSION SOCIALE HORS G2'  WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='HABITAT' AND CLE NOT LIKE '%G2%' AND CLE LIKE '%PC%' THEN 'PRETS CONVENTIONNES HORS G2'  WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='HABITAT' AND CLE LIKE '%EPL%' THEN 'PEL'  WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='HABITAT' AND CLE LIKE '%PII%' THEN 'PRETS INVESTISSEURS IMMOBILIERS' WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='HABITAT' AND CLE LIKE '%OPT%' THEN 'OPTIPLAN'  WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='HABITAT' AND CLE LIKE '%HBPL%' THEN 'PRETS LOCATIF INTERMEDIAIRE/PRETS LOCATIF SOCIAL'  WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='HABITAT' AND (CLE LIKE '%PAR%' OR CLE LIKE '%PTZ%') THEN 'PRETS TAUX ZERO'  WHEN REPLACE(RTRIM(LTRIM(LIB_NIVEAU2)), 'TOTAL ','')='HABITAT' AND CLE NOT LIKE '%G2%' AND CLE LIKE '%PTH%' THEN 'PRETS TOUT HABITAT HORS G2'  WHEN CLE IN ('HBBDF') THEN 'PLAN BDF'  WHEN CLE IN ('HBRELAIS') THEN 'PRETS RELAIS' END,
CASE WHEN CDOPTU=1 THEN 'CAP' WHEN CDOPTU=3 THEN 'COLLAR' ELSE 'SANS' END,
LIBELLE,
CASE WHEN CQTXPR=0 THEN 'N/A' WHEN CQTXPR=1 THEN 'FIXE' ELSE 'VAR' END,
CASE WHEN CFPEPR=0 THEN 'U' WHEN CFPEPR=5 THEN 'M' WHEN CFPEPR=6 THEN 'T' WHEN CFPEPR=7 THEN 'S' WHEN CFPEPR=8 THEN 'A' ELSE 'N/A' END,
CASE WHEN LTRIM(RTRIM(CTINDXL))='NE S APPLIQUE PAS' THEN 'N/A'  ELSE LTRIM(RTRIM(CTINDXL)) END,
CAST(SUBSTRING(DA1REA,1,4) AS NUMERIC)";

        private const string SQL_TCN = @"--TCN
SELECT
CAST(T_GFI_KTPB.NUEOPR AS NUMERIC) AS 'NUMERO',
CASE WHEN T_GFI_KTPB.IDTITR LIKE 'CDN%' THEN 'CDN' WHEN T_GFI_KTPB.IDTITR LIKE 'BMTN%' THEN 'BMTN' ELSE 'OTHER' END AS 'CATEG1',
CASE WHEN T_GFI_KTP8.CDAECO < 400 THEN 'EFI' ELSE 'NON EFI' END AS 'CATEG2',
CASE WHEN T_GFI_KTPB.CDINTM ='' THEN 'DDE' ELSE 'DFI' END AS 'CATEG3',
LTRIM(RTRIM(T_GFI_KTP8.LIDESC)) AS 'LIBELLE',
T_GFI_KTPB.DATOPV AS 'OPER',
T_GFI_KTPB.DVAL4 AS 'VALEUR',
T_GFI_KTPB.DHPRE AS 'ECHEANCE',
T_GFI_KTP3.MTNML1 AS 'NOMINAL',
T_GFI_KTPB.TXVA01 AS 'TX',
CASE ISNUMERIC(T_GFI_KTPB.CLEXT2) WHEN 1 THEN CAST(T_GFI_KTPB.CLEXT2 AS FLOAT) ELSE NULL END AS 'TXREF',
LTRIM(RTRIM(T_GFI_KTP6.LMVALE)) AS 'FORMULE INDICE',
LTRIM(RTRIM(T_GFI_KTPB.CDTAK1)) AS 'INDICE'

FROM T_GFI_KTP3 INNER JOIN ((T_GFI_KTP6 RIGHT JOIN T_GFI_KTPB ON T_GFI_KTP6.CCOD10 = T_GFI_KTPB.CDTAK1) INNER JOIN T_GFI_KTP8 ON T_GFI_KTPB.CDCTT = T_GFI_KTP8.CDTIER) ON T_GFI_KTP3.NUOFIN = T_GFI_KTPB.NUOPRT

WHERE 
T_GFI_KTPB.CDPRTF LIKE '882000EMITCN%' AND
T_GFI_KTPB.CDST4 NOT LIKE 'A' AND
T_GFI_KTP3.DDAB2='$DD/MM/YYYY$' AND
T_GFI_KTP3.CDST4 LIKE 'I' AND
(T_GFI_KTPB.IDTITR LIKE 'CDN%' OR T_GFI_KTPB.IDTITR LIKE 'BMTN%')";

        private const string SQL_PRETS_EMPRUNTS = @"--Prêts/Emprunts
SELECT
CAST(T_GFI_KTPB.NUEOPR AS NUMERIC) AS 'NUMERO',
CASE WHEN LTRIM(RTRIM(T_GFI_KTP8.LIDESC))='CREDIT LOGEMENT' THEN 'CREDIL' ELSE LTRIM(RTRIM(T_GFI_KTPB.IDTITR)) END AS 'CATEG1',
CASE WHEN (CONVERT(Datetime, T_GFI_KTPB.DHPRE, 112)-CONVERT(Datetime, T_GFI_KTPB.DVAL4, 112)) > 370 THEN 'LT' ELSE 'CT' END AS 'CATEG2',
LTRIM(RTRIM(T_GFI_KTP8.LIDESC)) AS 'LIBELLE',
T_GFI_KTPB.DVAL4 AS 'VALEUR',
T_GFI_KTPB.DHPRE AS 'ECHEANCE',
T_GFI_KTP3.MTNML1 AS 'NOMINAL',
T_GFI_KTPB.TXVA01 AS 'TX',
LTRIM(RTRIM(T_GFI_KTP6.LMVALE)) AS 'FORMULE INDICE'

FROM T_GFI_KTP3 INNER JOIN ((T_GFI_KTP6 RIGHT JOIN T_GFI_KTPB ON T_GFI_KTP6.CCOD10 = T_GFI_KTPB.CDTAK1) INNER JOIN T_GFI_KTP8 ON T_GFI_KTPB.CDCTT = T_GFI_KTP8.CDTIER) ON T_GFI_KTP3.NUOFIN = T_GFI_KTPB.NUOPRT

WHERE 
T_GFI_KTPB.CDPRTF LIKE '882000TRESOR' AND
T_GFI_KTPB.CDST4 NOT LIKE 'A' AND
T_GFI_KTP3.DDAB2<='$DD/MM/YYYY$' AND
T_GFI_KTP3.DFAB2>'$DD/MM/YYYY$' AND
(T_GFI_KTP3.CDST4 LIKE 'V' OR T_GFI_KTP3.CDST4 LIKE 'M')";

        private const string SQL_SWAPS = @"--Swaps
SELECT
CAST(T_GFI_KTPB.NUEOPR AS NUMERIC) AS 'NUMERO',
LTRIM(RTRIM(T_GFI_KTP8.LIDESC)) AS 'LIBELLE',
T_GFI_KTPB.DVAL4 AS 'VALEUR',
T_GFI_KTPB.DHPRE AS 'ECHEANCE',
CASE WHEN T_GFI_KTPB.CDTAK1=' ' THEN 'TF' ELSE LTRIM(RTRIM(T_GFI_KTPB.CDTAK1)) END AS 'JBE PRÊT',
T_GFI_KTPB.TXVA01 AS 'MARGE_P',
CASE WHEN T_GFI_KTPB.CDTAK2=' ' THEN 'TF' ELSE LTRIM(RTRIM(T_GFI_KTPB.CDTAK2)) END AS 'JBE EMP',
T_GFI_KTPB.TXVA02 AS 'MARGE_E',
T_GFI_KTP3.TXVALE AS 'TX',
T_GFI_KTP3.MTNML1 AS 'NOMINAL',
RTRIM(LTRIM(T_GFI_KTPB.IDTITR)) AS 'CATEG COUVERTURE'

FROM T_GFI_KTP3 INNER JOIN ((T_GFI_KTP6 RIGHT JOIN T_GFI_KTPB ON T_GFI_KTP6.CCOD10 = T_GFI_KTPB.CDTAK1) INNER JOIN T_GFI_KTP8 ON T_GFI_KTPB.CDCTT = T_GFI_KTP8.CDTIER) ON T_GFI_KTP3.NUOFIN = T_GFI_KTPB.NUOPRT

WHERE 
(T_GFI_KTPB.CDPRTF LIKE '882000SW%' OR T_GFI_KTPB.CDPRTF LIKE '882000EQ%' OR T_GFI_KTPB.CDPRTF LIKE '882000CDS%')  AND
T_GFI_KTPB.CDST4 NOT LIKE 'A'  AND
T_GFI_KTP3.DDAB2='$DD/MM/YYYY$' AND
T_GFI_KTP3.CDST4 LIKE 'I'  AND
T_GFI_KTPB.IDTITR NOT IN ('MICH_CLICC','MICH_CLICS')  AND
T_GFI_KTPB.CDPRTF <> '882000SWAPPO_TRA'  AND
LTRIM(RTRIM(T_GFI_KTP3.IDPOST)) NOT LIKE 'ASOULTE'
";
        private const string SQL_DAT = @"--DAT
SELECT
CASE WHEN LTRIM(RTRIM(LIBELLE_PRODUIT)) IS NULL THEN 'LIBELLE MANQUANT' ELSE LTRIM(RTRIM(LIBELLE_PRODUIT)) END AS 'LIBELLE',
SUBSTRING(CLE_STRUCTURE,4,1) AS 'TYPE',
SUM(MTRESTANT) AS 'ENCOURS',
SUBSTRING(DDSOU,1,6) AS 'VALEUR',
SUBSTRING(DHBON,1,6) AS 'ECHEANCE',
SUM(TXADB*MTRESTANT)/SUM(MTRESTANT)  AS 'TAUX'

FROM T_GFI_DAT_STOCK
WHERE 
PERIODE = $YYYYMM$ AND
CLE_STRUCTURE IS NOT NULL

GROUP BY
CASE WHEN LTRIM(RTRIM(LIBELLE_PRODUIT)) IS NULL THEN 'LIBELLE MANQUANT' ELSE LTRIM(RTRIM(LIBELLE_PRODUIT)) END,
SUBSTRING(CLE_STRUCTURE,4,1),
SUBSTRING(DDSOU,1,6),
SUBSTRING(DHBON,1,6)
";
        private const string SQL_EPARGNE = @"--Epargne
SELECT
LTRIM(RTRIM(LNPROD)) AS 'PRODUIT',
CAST(SUBSTRING(DDFON,1,4) AS NUMERIC)  AS 'DEBUT',
CASE WHEN LTRIM(RTRIM(LNPROD))='DAT EVOLUTION5 CREDIT AGRICOLE' THEN 60 ELSE SUM(COTPS*MSMIN)/SUM(MSMIN) END AS 'DUREE',
SUM(MSMIN) AS 'ENCOURS',
TXINR AS 'TAUX'

FROM T_GFI_EPARGNE_BILANCIELLE JOIN T_GFI_REFERENTIEL_PROD_EPARGNE ON IDCDP=IDCDPR
WHERE 
PERIODE = $YYYYMM$ AND
LTRIM(RTRIM(LNPROD)) NOT IN ('COMPTE SUPPORT CAPITAL', 'COMPTE TITRE ORDINAIRE', 'COMPTE TITRES PLUS', 'CONVENTION INDOSUEZ', 'CTO ASSISTE MONTPENSIER', 'CTO LIBRE MONTPENSIER', 'EUROFACTOR EXP FULL CLASSIQUE', 'EUROFACTOR EXP PREFERENCE', 'EUROFACTOR EXP RDB MANDAT GESTIO', 'EUROFACTOR EXP REVERSE FACTORING', 'FLORIAGRI', 'GARANTIE DECES', 'INVESTSTORE INTEGRAL', 'LIVRET TANDEM', 'PIERRISSIME', 'SERVICE D''''AIDE A LA PERSONNE', 'BGP TITRE ORDINAIRE')

GROUP BY
LTRIM(RTRIM(LNPROD)),
CAST(SUBSTRING(DDFON,1,4) AS NUMERIC),
TXINR";


        private string replaceDate(string sql, DateTime date)
        {
            string yyyymmdd = date.ToString("yyyyMMdd");
            string yyyymm = date.ToString("yyyyMM");
            string dd_mm_yyyy = date.ToString("dd/MM/yyyy");
            return sql.Replace("$YYYYMMDD$", yyyymmdd)
                               .Replace("$YYYYMM$", yyyymm)
                               .Replace("$DD/MM/YYYY$", dd_mm_yyyy);
        }
    }
}
