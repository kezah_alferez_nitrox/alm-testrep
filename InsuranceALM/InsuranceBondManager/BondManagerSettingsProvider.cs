﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace InsuranceBondManager
{
    public class InsuranceBondManagerSettingsProvider : SettingsProvider
    {
        public override string ApplicationName
        {
            get { return "InsuranceBondManagerSettingsProvider"; }
            set { }
        }

        public override void Initialize(string name, NameValueCollection config)
        {
            base.Initialize(this.ApplicationName, config);
        }

        private string SavingPath
        {
            get
            {
                return Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) +
                    Path.DirectorySeparatorChar + "BondManager" + Path.DirectorySeparatorChar + "user_insurance.config";
            }
        }

        public override SettingsPropertyValueCollection GetPropertyValues(SettingsContext context, SettingsPropertyCollection collection)
        {
            SettingsPropertyValueCollection values = new SettingsPropertyValueCollection();

            try
            {
                foreach (SettingsProperty property in collection)
                {
                    SettingsPropertyValue value = new SettingsPropertyValue(property);
                    value.IsDirty = false;
                    values.Add(value);
                }

                if (!File.Exists(this.SavingPath)) return values;

                using (XmlTextReader tr = new XmlTextReader(this.SavingPath))
                {
                    tr.ReadStartElement("InsuranceBondManager");
                    foreach (SettingsPropertyValue value in values)
                    {
                        tr.ReadStartElement(value.Name);
                        if (!tr.HasValue) continue;
                        value.SerializedValue = tr.ReadContentAsObject();
                        tr.ReadEndElement();
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Exception(ex.Message, ex);
            }

            return values;
        }

        public override void SetPropertyValues(SettingsContext context, SettingsPropertyValueCollection collection)
        {
            try
            {
                if (!File.Exists(this.SavingPath)) Directory.CreateDirectory(Path.GetDirectoryName(this.SavingPath));

                using (XmlTextWriter tw = new XmlTextWriter(this.SavingPath, Encoding.Unicode))
                {
                    tw.WriteStartDocument();
                    tw.WriteStartElement("InsuranceBondManager");
                    foreach (SettingsPropertyValue propertyValue in collection)
                    {
                        tw.WriteStartElement(propertyValue.Name);
                        if (propertyValue.SerializedValue != null) tw.WriteValue(propertyValue.SerializedValue);
                        tw.WriteEndElement();
                    }
                    tw.WriteEndElement();
                    tw.WriteEndDocument();
                }
            }
            catch (Exception ex)
            {
                Log.Exception(ex.Message, ex);
            }

        }
    }
}
