using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using InsuranceBondManager.Domain;
using NHibernate;
using NHibernate.Linq;

namespace InsuranceBondManager.BusinessLogic
{
    public class DomainTools
    {
        public static string[] GetAllVectorNames(ISession session)
        {
            return session.Query<Vector>().Select(v=>v.Name).ToArray();
        }

        public static string[] GetAllVariableNames(ISession session)
        {
            return session.Query<Variable>().Select(v => v.Name).ToArray();
        }

        public static string[] GetAllVariablesAndVectorNames(ISession session)
        {
            return GetAllVectorNames(session).Union(GetAllVariableNames(session)).ToArray();
        }
    }
}
