using System;
using System.Linq;
using ALMS.Products;
using InsuranceALM.Infrastructure.Data;
using InsuranceBondManager.Calculations;
using InsuranceBondManager.Domain;
using System.Collections.Generic;
using NHibernate;
using NHibernate.Linq;

namespace InsuranceBondManager.BusinessLogic
{
    public class CurrencyBL
    {
        private const double IdentityFxRate = 1;

        public static double Convert(double value, Currency from, Currency to)
        {
            return FxRate(@from, to) * value;
        }

        public static double FxRate(Currency from, Currency to)
        {
            if (@from == null || to == null || @from.Id == to.Id) return IdentityFxRate;
            return FxRatesCache[@from][to];
        }

        public static void ResetValuationDateFxRates()
        {
            fxRatesCache.Clear();
        }

        private static readonly Dictionary<Currency, Dictionary<Currency, double>> fxRatesCache = new Dictionary<Currency, Dictionary<Currency, double>>();
        private static Dictionary<Currency, Dictionary<Currency, double>> FxRatesCache
        {
            get
            {
                if (fxRatesCache.Count == 0)
                {
                    using (ISession session = SessionManager.OpenSession())
                    {
                        Currency[] currencies = EntityBase<Currency, int>.FindAll(session);
                        foreach (Currency fromCurrency in currencies)
                        {
                            fxRatesCache.Add(fromCurrency, new Dictionary<Currency, double>());
                            //fxRatesCache[fromCurrency] = new Dictionary<Currency, double>();
                            foreach (Currency toCurrency in currencies)
                            {
                                fxRatesCache[fromCurrency][toCurrency] = IdentityFxRate;
                            }
                        }

                        Scenario[] selected = Scenario.FindSelected(session);
                        Scenario firstScenario = selected.Length > 0 ? selected[0] : null;

                        DateTime valuationDate = Param.GetValuationDate(session) ?? DateTime.Today;
                        FXRate[] allFXRates = EntityBase<FXRate, int>.FindAll(session);

                        foreach (Currency fromCurrency in currencies)
                        {
                            foreach (Currency toCurrency in currencies)
                            {
                                if (fromCurrency == toCurrency) continue;

                                foreach (FXRate rate in allFXRates)
                                {
                                    if (rate.From == fromCurrency && rate.To == toCurrency && rate.Value != null)
                                    {
                                        double fxRate = ComputeFxRate(session, rate.Value, firstScenario, valuationDate);

                                        fxRatesCache[fromCurrency][toCurrency] = fxRate;
                                        fxRatesCache[toCurrency][fromCurrency] = IdentityFxRate / fxRate;
                                    }
                                }
                            }
                        }
                        session.Flush();
                    }
                }
                return fxRatesCache;
            }
        }

        private static double ComputeFxRate(ISession session, string variableName, Scenario scenario, DateTime date)
        {
            double fxRate;

            if (Double.TryParse(variableName, out fxRate)) return fxRate;

            Variable variable = Variable.FindByName(session, variableName);
            if (variable == null) return IdentityFxRate;

            VariableValue variableValue = VariableValue.Find(session, variable, scenario);
            if (variableValue == null || String.IsNullOrEmpty(variableValue.Value)) return IdentityFxRate;

            fxRate = ComputeVariableValue(session, variableValue.Value, date);
            return fxRate;
        }

        private static double ComputeVariableValue(ISession session, string expression, DateTime date)
        {
            double decimalValue;
            if (Double.TryParse(expression, out decimalValue)) return decimalValue;

            IDictionary<string, object> operands = ComputeOperands(session, expression, date);
            ExpressionParser parser = new ExpressionParser();
            try
            {
                decimalValue = (double)parser.Evaluate(expression, operands);
            }
            catch (Exception ex)
            {
                Log.Exception("ComputeVariableValue", ex);
                decimalValue = IdentityFxRate;
            }

            return decimalValue;
        }

        private static IDictionary<string, object> ComputeOperands(ISession session, string value, DateTime date)
        {
            string[] operands = Tools.GetOperandsFromExpression(value);
            IDictionary<string, object> vectorValues = new SortedDictionary<string, object>(StringComparer.CurrentCultureIgnoreCase);

            foreach (string operand in operands)
            {
                double unused;
                if (Double.TryParse(operand, out unused)) continue;

                Vector vector = Vector.FindByName(session, operand);
                if (vector == null) continue;

                VectorPoint point = vector.GetCurrentValue(session, date);
                if (point == null)
                {
                    if (vector.VectorPoints.Count > 0)
                    {
                        point = vector.VectorPoints[0];
                    }
                    else
                    {
                        continue;
                    }
                }

                vectorValues[operand.ToLowerInvariant()] = point.Value;
            }

            return vectorValues;
        }

        public static double ConvertToValuationCurrency(double value, Currency currency, Currency valuationCurrency)
        {
            return Convert(value, currency, valuationCurrency);
        }

        public static void SetSpecialBondsToValuationCurrency()
        {
            using (ISession session = SessionManager.OpenSession())
            {
                Currency valueationCurrency = Param.GetValuationCurrency(session);

                SetBondsCurrency(session, Product.FindByAttrib(session, ProductAttrib.CashAdj), valueationCurrency);
                SetBondsCurrency(session, Product.FindByAttrib(session, ProductAttrib.Roll), valueationCurrency);
                SetBondsCurrency(session, Product.FindByAttrib(session, ProductAttrib.ModelImpairments), valueationCurrency);
                SetBondsCurrency(session, Product.FindByAttrib(session, ProductAttrib.IBNR), valueationCurrency);
                SetBondsCurrency(session, Product.FindByAttrib(session, ProductAttrib.Treasury), valueationCurrency);

                session.Flush();
            }
        }

        private static void SetBondsCurrency(ISession session, Product[] products, Currency currency)
        {
            foreach (Product bond in products)
            {
                bond.Currency = currency;
                bond.Update(session);
            }
        }

        public static void RemoveUnusedCurrencies()
        {
            using (ISession session = SessionManager.OpenSession())
            {
                FXRate[] allFxRates = FXRate.FindAll(session);
                Currency[] allCurrencies = Currency.FindAllNotEmpty(session);

                allCurrencies = allCurrencies.Where(c => FxRateHasValue(c, allFxRates)).ToArray();

                Currency[] usedCurrencies = session.Query<Product>().
                    Where(p => p.Attrib != ProductAttrib.Treasury && p.Attrib != ProductAttrib.Payable && p.Attrib != ProductAttrib.Receivable).
                    Select(p => p.Currency).
                    Distinct().
                    ToArray();

                usedCurrencies = session.Query<FundamentalVector>().
                    Select(p => p.Currency).
                    Distinct().
                    ToArray().
                    Union(usedCurrencies).
                    ToArray();

                Currency valuationCurrency = Param.GetValuationCurrency(session);
                usedCurrencies = usedCurrencies.
                    Union(new[] { valuationCurrency }).
                    Where(c => c != null).
                    Distinct().
                    ToArray();

                Currency[] toRemove = allCurrencies.
                    Where(c => !usedCurrencies.Contains(c)).
                    ToArray();

                foreach (Currency currency in toRemove)
                {
                    DeleteCurrency(session, currency);
                }

                session.Flush();
            }
        }

        private static bool FxRateHasValue(Currency currency, FXRate[] allFxRates)
        {
            FXRate from = allFxRates.FirstOrDefault(fx => fx.From == currency);
            FXRate to = allFxRates.FirstOrDefault(fx => fx.To == currency);

            return (from == null || string.IsNullOrEmpty(from.Value)) && 
                (to == null || string.IsNullOrEmpty(to.Value));
        }

        public static void DeleteCurrency(ISession session, Currency currency)
        {
            foreach (FXRate fxRate in session.Query<FXRate>().
                Where(fx => fx.To == currency || fx.From == currency))
            {
                fxRate.Delete(session);
            }

            foreach (Product product in session.Query<Product>().
                Where(p => (p.Category.Type == CategoryType.Treasury ||
                            p.Category.Type == CategoryType.Receivable ||
                            p.Category.Type == CategoryType.Payable)
                           && p.Currency == currency))
            {
                product.Delete(session);
            }

            currency.Delete(session);
        }
    }
}