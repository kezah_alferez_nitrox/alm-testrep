﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using InsuranceBondManager.Calculations;
using InsuranceBondManager.Domain;
using InsuranceBondManager.Views;

namespace InsuranceBondManager.Core.BetaTools
{
    public class BetaResultCSV
    {
        public static bool SaveBetaResultAsCSV = false;
        public static string BetaResultFileNameCSV = null;

        private readonly ResultModel resultModel;
        private readonly ScenarioData scenarioData;
        private readonly ReportAnalysisType analysisType;

        private StreamWriter writer;

        private enum BetaProductData
        {
            CurrFace,
            BookPrice,
            ActuarialPrice,
            BookValue,
            Growth,
            Receivable,
            MarginAccruedInterest,
            LiveFace,
            RemainPrincipal,
            CouponIncome,
            GainAndLosesIncome,
            PrincipalIncome,
            InterestIncome,
            FxPNLIncome,
            Default,
            LossImpairment,
            Recovery,
            Spread,
            RealCouponRate,
            RealIRCRate,
            GapNoMaturityRate,
            LaggedRecovery,
            ApplicationCharge,
            ApplicationFee,
            PrepayementFee,
            InvestmentParameter,
            ReserveCashFlows,
            IRCValue,
            IRCPercentageValues,
            CPRValues,
            CDRValues,
            LGDValues,
            CouponValues,
            RollRatioValues
        }

        public BetaResultCSV(ResultModel resultModel, ReportAnalysisType analysisType, Simulation simulation, Scenario scenario)
        {
            this.resultModel = resultModel;
            this.analysisType = analysisType;
            this.scenarioData = simulation[scenario];
        }

        //Save Result As CSV
        public bool BetaOutputResultToCSV()
        {
            try
            {
                writer = new StreamWriter(BetaResultFileNameCSV);

                //Category Header
                writer.Write("Result_Category,CategoryChild_1,CategoryChild_2,CategoryChild_3,");
                WriteDateInterval();

                //Category Data Values
                IList<ResultRowModel> gridData = resultModel.GetRows(analysisType);
                foreach (var p in gridData)
                {
                    if (!WriteCategoryValues(p, p.Description.ToString() + ",,,,")) continue;

                    foreach (var pChild1 in p.Children)
                    {
                        if (!WriteCategoryValues(pChild1, p.Description.ToString() + "," +
                                                          pChild1.Description.ToString() + ",,,")) continue;

                        foreach (var pChild2 in pChild1.Children)
                        {
                            if (!WriteCategoryValues(pChild2, p.Description.ToString() + "," +
                                                              pChild1.Description.ToString() + "," +
                                                              pChild2.Description.ToString() + ",,")) continue;

                            foreach (var pChild3 in pChild2.Children)
                            {
                                if (!WriteCategoryValues(pChild3, p.Description.ToString() + "," +
                                                                  pChild1.Description.ToString() + "," +
                                                                  pChild2.Description.ToString() + "," +
                                                                  pChild3.Description.ToString() + ",")) continue;
                            }
                        }
                    }
                }
                
                //Product Header
                writer.Write("\n\nProduct,Type,Attribute,ProductData,");
                WriteDateInterval();

                //Product Data Values
                foreach (ProductData p in scenarioData.ProductDatas)
                {
                    foreach (BetaProductData bpd in Enum.GetValues(typeof(BetaProductData)))
                    {
                        writer.Write("\n" + p.ToString() + "," + 
                                            p.Product.ProductType.ToString() + "," +
                                            p.Product.Attrib.ToString() + "," +
                                            bpd.ToString() + ",");
                        WriteProductValues(bpd, p);
                    }
                }
            }
            catch (Exception Ex)
            {
                SaveBetaResultAsCSV = false;
                BetaResultFileNameCSV = null;
                MessageBox.Show(Ex.Message, "Save Result As CSV", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            writer.Close();
            SaveBetaResultAsCSV = false;
            BetaResultFileNameCSV = null;

            return true;
        }

        private void WriteDateInterval()
        {
            for (int i = 0; i < resultModel.ValueColumnsCount; i++)
                writer.Write(resultModel.DateIntervals[i].ToString(i == 0) + ",");
        }

        private bool WriteCategoryValues(ResultRowModel p, string category)
        {
            if (p.Type == FormattingType.PeriodHeader || p.RowValues == null || p.Length == 0) return false;

            writer.Write("\n" + category);

            for (int i = 2; i < p.RowValues.Count(); i++)
                writer.Write(p.RowValues[i] == null ? ",," : p.RowValues[i].ToString() + ",");

            return true;
        }

        private void WriteProductValues(BetaProductData bpd, ProductData p)
        {
            for (int i = 0; i < scenarioData.ResultMonths; i++)
            {
                switch (bpd)
                {
                    case BetaProductData.CurrFace:
                        writer.Write(p.CurrFace == null ? ",," : p.CurrFace[i].ToString() + ",");
                        break;

                    case BetaProductData.Receivable:
                        writer.Write(p.GetAccruedInterest(i).ToString() + ",");
                        break;

                    case BetaProductData.Growth:
                        writer.Write(p.Growth == null ? ",," : p.Growth[i].ToString() + ",");
                        break;

                    case BetaProductData.LiveFace:
                        writer.Write(p.LiveFace == null ? ",," : p.LiveFace[i].ToString() + ",");
                        break;

                    case BetaProductData.RemainPrincipal:
                        writer.Write(p.RemainPrincipal == null ? ",," : p.RemainPrincipal[i].ToString() + ",");
                        break;

                    case BetaProductData.BookPrice:
                        writer.Write(p.BookPrice == null ? ",," : p.BookPrice[i].ToString() + ",");
                        break;

                    case BetaProductData.ActuarialPrice:
                        writer.Write(p.ActuarialPrice == null ? ",," : p.ActuarialPrice[i].ToString() + ",");
                        break;

                    case BetaProductData.MarginAccruedInterest:
                        writer.Write(p.MarginAccruedInterest == null ? ",," : p.MarginAccruedInterest[i].ToString() + ",");
                        break;

                    case BetaProductData.CouponIncome:
                        writer.Write(p.CouponIncome == null ? ",," : p.CouponIncome[i].ToString() + ",");
                        break;

                    case BetaProductData.IRCValue:
                        writer.Write(p.IRCValue == null ? ",," : p.IRCValue[i].ToString() + ",");
                        break;

                    case BetaProductData.PrincipalIncome:
                        writer.Write(p.PrincipalIncome == null ? ",," : p.PrincipalIncome[i].ToString() + ",");
                        break;

                    case BetaProductData.LossImpairment:
                        writer.Write(p.LossImpairment == null ? ",," : p.LossImpairment[i].ToString() + ",");
                        break;

                    case BetaProductData.Recovery:
                        writer.Write(p.Recovery == null ? ",," : p.Recovery[i].ToString() + ",");
                        break;

                    case BetaProductData.InterestIncome:
                        writer.Write(p.InterestIncome == null ? ",," : p.InterestIncome[i].ToString() + ",");
                        break;

                    case BetaProductData.ApplicationFee:
                        writer.Write(p.ApplicationFee == null ? ",," : p.ApplicationFee[i].ToString() + ",");
                        break;

                    case BetaProductData.ApplicationCharge:
                        writer.Write(p.ApplicationCharge == null ? ",," : p.ApplicationCharge[i].ToString() + ",");
                        break;

                    case BetaProductData.PrepayementFee:
                        writer.Write(p.PrepayementFee == null ? ",," : p.PrepayementFee[i].ToString() + ",");
                        break;

                    case BetaProductData.GainAndLosesIncome:
                        writer.Write(p.GainAndLosesIncome == null ? ",," : p.GainAndLosesIncome[i].ToString() + ",");
                        break;

                    case BetaProductData.FxPNLIncome:
                        writer.Write(p.FxPnlIncome == null ? ",," : p.FxPnlIncome[i].ToString() + ",");
                        break;

                    case BetaProductData.InvestmentParameter:
                        writer.Write(p.InvestmentParameter == null ? ",," : p.InvestmentParameter[i].ToString() + ",");
                        break;

                    case BetaProductData.CouponValues:
                        writer.Write(p.CouponValues == null ? ",," : p.CouponValues[i].ToString() + ",");
                        break;

                    case BetaProductData.RollRatioValues:
                        writer.Write(p.RollRatioValues == null ? ",," : p.RollRatioValues[i].ToString() + ",");
                        break;

                    case BetaProductData.CPRValues:
                        writer.Write(p.CprValues == null ? ",," : p.CprValues[i].ToString() + ",");
                        break;

                    case BetaProductData.CDRValues:
                        writer.Write(p.CdrValues == null ? ",," : p.CdrValues[i].ToString() + ",");
                        break;

                    case BetaProductData.LGDValues:
                        writer.Write(p.LgdValues == null ? ",," : p.LgdValues[i].ToString() + ",");
                        break;

                    case BetaProductData.ReserveCashFlows:
                        writer.Write(p.ReserveCashflows == null ? ",," : p.ReserveCashflows[i].ToString() + ",");
                        break;

                    case BetaProductData.BookValue:
                        writer.Write(p.GetBookValue(i).ToString() + ",");
                        break;

                    case BetaProductData.RealCouponRate:
                        writer.Write(p.RealCouponRate == null ? ",," : p.RealCouponRate[i].ToString() + ",");
                        break;

                    case BetaProductData.RealIRCRate:
                        writer.Write(p.RealIRCRate == null ? ",," : p.RealIRCRate[i].ToString() + ",");
                        break;

                    case BetaProductData.GapNoMaturityRate:
                        writer.Write(p.GapNoMaturityRate == null ? ",," : p.GapNoMaturityRate[i].ToString() + ",");
                        break;

                    case BetaProductData.LaggedRecovery:
                        writer.Write(p.LaggedRecovery == null ? ",," : p.LaggedRecovery[i].ToString() + ",");
                        break;

                    case BetaProductData.IRCPercentageValues:
                        writer.Write(p.IrcPercentageValues == null ? ",," : p.IrcPercentageValues[i].ToString() + ",");
                        break;

                    case BetaProductData.Default:
                        writer.Write(p.Default == null ? ",," : p.Default[i].ToString() + ",");
                        break;

                    case BetaProductData.Spread:
                        writer.Write(p.Spread == null ? ",," : p.Spread[i].ToString() + ",");
                        break;
                }
            }
        }
    }
}
