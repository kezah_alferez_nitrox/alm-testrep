using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Text;
using System.Xml.Serialization;
using InsuranceALM.Infrastructure.Data;
using InsuranceBondManager.Domain;
using NHibernate;


namespace InsuranceBondManager.Core.IO
{
    public class ScenarioXmlSerializer : IScenarioSerializer
    {
        private const string GzippedXmlFileHeader = "GXA";
        private const byte FileVersion = 1;
        private const int GzippedXmlSignatureLength = 6;
        private const int GzippedXmlFileHeaderLength = 7;

        public void Save(string filename)
        {
            ScenarioTemplate stl = new ScenarioTemplate();

            using (FileStream stream = File.Open(filename, FileMode.Create))
            {
                byte[] bytes = Encoding.Unicode.GetBytes(GzippedXmlFileHeader);
                stream.Write(bytes, 0, bytes.Length);
                stream.WriteByte(FileVersion);
                using (GZipStream gZipStream = new GZipStream(stream, CompressionMode.Compress))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof (ScenarioTemplate));
                    serializer.Serialize(gZipStream, stl);
                }
            }
        }

        public void Load(string filename)
        {
                ScenarioTemplate newscenariotemplate = LoadScenarioTemplate(filename);

                newscenariotemplate.isLoad = true;
                if (newscenariotemplate.Scenarios == null) newscenariotemplate.Scenarios = new Scenario[0];
                if (newscenariotemplate.VariableValues == null)
                    newscenariotemplate.VariableValues = new VariableValue[0];
                if (newscenariotemplate.Variables == null) newscenariotemplate.Variables = new Variable[0];
                if (newscenariotemplate.Vectors == null) newscenariotemplate.Vectors = new Vector[0];
                if (newscenariotemplate.VectorPoints == null) newscenariotemplate.VectorPoints = new VectorPoint[0];
                if (newscenariotemplate.Currencies == null) newscenariotemplate.Currencies = new Currency[0];
                if (newscenariotemplate.FxRates == null) newscenariotemplate.FxRates = new FXRate[0];
                if (newscenariotemplate.Dividends == null) newscenariotemplate.Dividends = new Dividend[0];
                if (newscenariotemplate.FundamentalVectors == null)
                    newscenariotemplate.FundamentalVectors = new FundamentalVector[0];
                if (newscenariotemplate.DynamicVariables == null)
                    newscenariotemplate.DynamicVariables = new DynamicVariable[0];
                Persister persister = new Persister();

                persister.DeleteScenarioData();

                IDictionary<int, Currency> currencyMap = new Dictionary<int, Currency>();

                IDictionary<int, Variable> variableMap = persister.PersistVariables(newscenariotemplate.Scenarios,
                                                                                    newscenariotemplate.Variables,
                                                                                    newscenariotemplate.VariableValues);
                persister.PersistVectors(newscenariotemplate.Vectors, newscenariotemplate.VectorPoints);
                persister.PersistCurrencies(newscenariotemplate.Currencies, currencyMap);
                persister.PersistFxRates(newscenariotemplate.FxRates, currencyMap);
                persister.PersistValuationCurrency(newscenariotemplate.ValuationCurrency, currencyMap);
                persister.PersistValuationDate(newscenariotemplate.ValuationDate);
                persister.PersistDividends(newscenariotemplate.Dividends);
                persister.PersistFundamentalVectors(newscenariotemplate.FundamentalVectors, variableMap, currencyMap);
                persister.PersistDynamicVariables(newscenariotemplate.DynamicVariables);
        }

        private static ScenarioTemplate LoadScenarioTemplate(string filename)
        {
            ScenarioFileType scenarioFileType = GetScenarioFileType(filename);
            if (scenarioFileType == ScenarioFileType.Unknown)
            {
                throw new IOException("Unknown ALM file format: " + filename);
            }

            ScenarioTemplate newscenariotemplate;

            if (scenarioFileType == ScenarioFileType.Xml)
            {
                using (FileStream stream = File.Open(filename, FileMode.Open))
                {
                    newscenariotemplate = DeserializeScenarioTemplate(stream);
                }
            }
            else
            {
                using (FileStream stream = File.Open(filename, FileMode.Open))
                {
                    stream.Seek(GzippedXmlFileHeaderLength, SeekOrigin.Begin);

                    using (GZipStream gZipStream = new GZipStream(stream, CompressionMode.Decompress))
                    {
                        newscenariotemplate = DeserializeScenarioTemplate(gZipStream);
                    }
                }
            }
            return newscenariotemplate;
        }

        private static ScenarioTemplate DeserializeScenarioTemplate(Stream stream)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(ScenarioTemplate));
            ScenarioTemplate newscenariotemplate = (ScenarioTemplate)serializer.Deserialize(stream);
            return newscenariotemplate;
        }

        private static ScenarioFileType GetScenarioFileType(string filename)
        {
            using (FileStream stream = File.OpenRead(filename))
            {
                byte[] buffer = new byte[GzippedXmlSignatureLength];
                if (stream.Read(buffer, 0, buffer.Length) != buffer.Length)
                {
                    return ScenarioFileType.Unknown;
                }
                string signature = Encoding.Unicode.GetString(buffer, 0, GzippedXmlSignatureLength);
                if (signature == GzippedXmlFileHeader)
                {
                    return ScenarioFileType.GzippedXml;
                }
                else
                {
                    return ScenarioFileType.Xml;
                }
            }
        }

        enum ScenarioFileType
        {
            Unknown,
            Xml,
            GzippedXml
        }
    }
}