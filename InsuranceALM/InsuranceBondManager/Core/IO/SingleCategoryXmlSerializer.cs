﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using BondManager.Domain;

namespace BondManager.Core.IO
{
    public class SingleCategoryXmlSerializer : IXmlSerializable
    {
        public Category Category { get; set; }
        public List<int> CategoryIds { get; set; }
        public List<int> ProductIds { get; set; }

        public SingleCategoryXmlSerializer()
        {
            this.CategoryIds = new List<int>();
            this.ProductIds = new List<int>();
        }

        public void Serialize(string fileName)
        {
            using (Stream stream = File.Create(fileName))
            {
                XmlWriter writer = new XmlTextWriter(stream, Encoding.Default);
                
            }
        }

        public XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(XmlReader reader)
        {
            throw new NotImplementedException();
        }

        public void WriteXml(XmlWriter writer)
        {
            writer.WriteComment("Serialized category " + Category.Label);
            writer.WriteStartElement("Category");
            
            // Propriétés de category
            writer.WriteElementString("Balance", ((int)Category.BalanceType).ToString());
            writer.WriteElementString("Code", Category.Code);
            writer.WriteElementString("IsNew", Category.IsNew.ToString());
            if (Category.IsVisibleChecked.HasValue) writer.WriteElementString("IsVisibleChecked", Category.IsVisibleChecked.Value.ToString());
            if (Category.IsVisibleCheckedModel.HasValue) writer.WriteElementString("", Category.IsVisibleCheckedModel.Value.ToString());
            writer.WriteElementString("Label", Category.Label);
            writer.WriteElementString("Position", Category.Position.ToString());
            writer.WriteElementString("ReadOnly", Category.ReadOnly.ToString());
            writer.WriteElementString("Type", ((int)Category.Type).ToString());
            if (Category.isVisible.HasValue) writer.WriteElementString("Category", Category.isVisible.Value.ToString());

            // Bonds
            writer.WriteStartElement("Bonds");
            foreach (Product product in Category.Bonds)
            {
                ProductIds.Add(product.Id);
                writer.WriteElementString("Bond", product.Id.ToString());

                //writer.WriteStartElement("Bond");
                //this.WriteBasePropertiesAsElements(writer, product);
                //writer.WriteEndElement();
            }
            writer.WriteEndElement();

            // Categories
            writer.WriteStartAttribute("Categories");
            foreach (Category child in Category.Children)
            {
                CategoryIds.Add(child.Id);
                writer.WriteElementString("Category", child.Id.ToString());
                this.CategoryIds.Add(child.Id);

                //writer.WriteStartElement("Category");
                //SingleCategoryXmlSerializer serializer = new SingleCategoryXmlSerializer();
                //serializer.Category = child;
                //serializer.WriteXml(writer);
                //writer.WriteEndElement();
            }
            writer.WriteEndElement();

            writer.WriteEndElement();
        }

        protected void WriteBasePropertiesAsElements(XmlWriter writer, object value)
        {
            PropertyInfo[] properties = value.GetType().GetProperties();
            foreach (PropertyInfo property in properties)
            {
                object pValue = property.GetValue(value, new object[0]);
                if (pValue == null) continue;
                if (pValue is int || pValue is float || pValue is double || pValue is decimal || pValue is string || pValue is bool || pValue is long || pValue is Enum)
                    writer.WriteElementString(property.Name, pValue.ToString());
            }
        }
    }
}
