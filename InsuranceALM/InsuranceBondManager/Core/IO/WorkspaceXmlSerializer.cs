using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using ALMS.Products;
using ALMSCommon;
using InsuranceALM.Infrastructure.Data;
using InsuranceBondManager.BusinessLogic;
using InsuranceBondManager.Domain;

using System.ComponentModel;
using Iesi.Collections.Generic;
using NHibernate;
using NHibernate.Linq;

namespace InsuranceBondManager.Core.IO
{
    public class WorkspaceXmlSerializer
    {
        private const string GzippedXmlFileHeader = "GXW";
        private const byte FileVersion = 7;
        private const int GzippedXmlSignatureLength = 6;
        private const int GzippedXmlFileHeaderLength = 7;

        private static bool haveToVerifyCRC = true;

        public string Filename { get; set; }

        public GeneratorResultNode[] GeneratorResultNodes { get; private set; }
        public int[] ScenarioSetupColumnsOrder { get; private set; }

        public WorkspaceXmlSerializer()
            : this(null, null)
        {
        }

        public WorkspaceXmlSerializer(GeneratorResultNode[] generatorResultNodes, int[] scenarioSetupColumnsOrder)
        {
            this.ScenarioSetupColumnsOrder = scenarioSetupColumnsOrder;
            this.GeneratorResultNodes = generatorResultNodes;
        }


        public void Save(BackgroundWorker backgroundWorker)
        {
            WorkspaceTemplate workspaceTemplate = new WorkspaceTemplate();
            backgroundWorker.ReportProgress(3);
            workspaceTemplate.LoadFromDatabase(backgroundWorker);
            workspaceTemplate.GeneratorResultNodes = this.GeneratorResultNodes;
            workspaceTemplate.ScenarioSetupColumnsOrder = this.ScenarioSetupColumnsOrder;

            foreach (Product bond in workspaceTemplate.Products)
            {
                bond.OtherSwapLegId = bond.OtherSwapLeg == null ? -1 : bond.OtherSwapLeg.Id;
            }

            workspaceTemplate.ComputeCrc();

            using (FileStream stream = File.Open(Filename, FileMode.Create))
            {
                byte[] bytes = Encoding.Unicode.GetBytes(GzippedXmlFileHeader);
                stream.Write(bytes, 0, bytes.Length);
                stream.WriteByte(FileVersion);
                backgroundWorker.ReportProgress(90);
                using (GZipStream gZipStream = new GZipStream(stream, CompressionMode.Compress))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(WorkspaceTemplate));
                    serializer.Serialize(gZipStream, workspaceTemplate);
                    backgroundWorker.ReportProgress(99);
                }
            }
        }

        public void Load(BackgroundWorker backgroundWorker)
        {
            Stopwatch sw = Stopwatch.StartNew();

            Param.ClearCaches();
            BalanceBL.ClearCache();

            WorkspaceTemplate workspaceTemplate = LoadWorkspaceTemplate(Filename);

            if (workspaceTemplate.Categories == null) workspaceTemplate.Categories = new Category[0];
            if (workspaceTemplate.Products == null) workspaceTemplate.Products = new Product[0];
            if (workspaceTemplate.Scenarios == null) workspaceTemplate.Scenarios = new Scenario[0];
            if (workspaceTemplate.VariableValues == null) workspaceTemplate.VariableValues = new VariableValue[0];
            if (workspaceTemplate.Variables == null) workspaceTemplate.Variables = new Variable[0];
            if (workspaceTemplate.Vectors == null) workspaceTemplate.Vectors = new Vector[0];
            if (workspaceTemplate.VectorPoints == null) workspaceTemplate.VectorPoints = new VectorPoint[0];
            if (workspaceTemplate.FundamentalVectors == null) workspaceTemplate.FundamentalVectors = new FundamentalVector[0];
            if (workspaceTemplate.DynamicVariables == null) workspaceTemplate.DynamicVariables = new DynamicVariable[0];
            if (workspaceTemplate.Currencies == null) workspaceTemplate.Currencies = new Currency[0];
            if (workspaceTemplate.FxRates == null) workspaceTemplate.FxRates = new FXRate[0];
            if (workspaceTemplate.Parameters == null) workspaceTemplate.Parameters = new Param[0];
            if (workspaceTemplate.Dividends == null) workspaceTemplate.Dividends = new Dividend[0];
            if (workspaceTemplate.OIAs == null) workspaceTemplate.OIAs = new OtherItemAssum[0];
            if (workspaceTemplate.SolvencyAdjustments == null) workspaceTemplate.SolvencyAdjustments = new SolvencyAdjustment[0];
            if (workspaceTemplate.VariableCorrelation == null) workspaceTemplate.VariableCorrelation = new VariableCorrelation[0];
            if (workspaceTemplate.VolatilityConfig == null) workspaceTemplate.VolatilityConfig = new VolatilityConfig[0];

            backgroundWorker.ReportProgress(5);

            SessionManager.DeleteDataBase();
            SessionManager.EnsureDataBaseExists();

            Persister persister = new Persister();

            Debug.WriteLine("After delete " + sw.ElapsedMilliseconds);

            backgroundWorker.ReportProgress(25);

            IDictionary<int, Currency> currencyMap = new Dictionary<int, Currency>();

            persister.PersistCurrencies(workspaceTemplate.Currencies, currencyMap, backgroundWorker);

            Debug.WriteLine("After PersistCurrencies " + sw.ElapsedMilliseconds);

            persister.PersistVectors(workspaceTemplate.Vectors, workspaceTemplate.VectorPoints, backgroundWorker);
            Debug.WriteLine("After PersistVectors " + sw.ElapsedMilliseconds);

            IDictionary<int, Variable> variableMap = persister.PersistVariables(workspaceTemplate.Scenarios, workspaceTemplate.Variables, workspaceTemplate.VariableValues,
                                                                 backgroundWorker);
            Debug.WriteLine("After PersistVariables " + sw.ElapsedMilliseconds);

            persister.PersistCategoriesAndBonds(workspaceTemplate.Categories, workspaceTemplate.Products, currencyMap,
                                            backgroundWorker);
            Debug.WriteLine("After PersistCategoriesAndBonds " + sw.ElapsedMilliseconds);

            persister.PersistFundamentalVectors(workspaceTemplate.FundamentalVectors, variableMap, currencyMap);
            Debug.WriteLine("After PersistFundamentalVectors " + sw.ElapsedMilliseconds);

            persister.PersistDynamicVariables(workspaceTemplate.DynamicVariables);

            backgroundWorker.ReportProgress(90);
            Debug.WriteLine("After PersistDynamicVariables " + sw.ElapsedMilliseconds);

            persister.PersistFxRates(workspaceTemplate.FxRates, currencyMap);

            backgroundWorker.ReportProgress(94);
            Debug.WriteLine("After PersistFxRates " + sw.ElapsedMilliseconds);

            persister.PersistParameters(workspaceTemplate.Parameters, currencyMap);

            backgroundWorker.ReportProgress(96);
            Debug.WriteLine("After PersistParameters " + sw.ElapsedMilliseconds);

            persister.PersistOtherItemsAssum(workspaceTemplate.OIAs);
            Debug.WriteLine("After PersistOtherItemsAssum " + sw.ElapsedMilliseconds);

            persister.PersistSolvencyAdjustments(workspaceTemplate.SolvencyAdjustments);
            Debug.WriteLine("After PersistSolvencyAdjustments " + sw.ElapsedMilliseconds);

            persister.PersistVolatilityConfig(workspaceTemplate.VolatilityConfig, variableMap);
            Debug.WriteLine("After PersistVolatilityConfig " + sw.ElapsedMilliseconds);

            persister.PersistVariableCorrelation(workspaceTemplate.VariableCorrelation, variableMap);

            backgroundWorker.ReportProgress(98);
            Debug.WriteLine("After PersistVariableCorrelation " + sw.ElapsedMilliseconds);

            persister.PersistDividends(workspaceTemplate.Dividends);
            Debug.WriteLine("After PersistDividends " + sw.ElapsedMilliseconds);

            FixWorkspace();

            backgroundWorker.ReportProgress(100);
            Debug.WriteLine("After CreationDate " + sw.ElapsedMilliseconds);

            this.GeneratorResultNodes = workspaceTemplate.GeneratorResultNodes;
            this.ScenarioSetupColumnsOrder = workspaceTemplate.ScenarioSetupColumnsOrder;

            if (haveToVerifyCRC && !workspaceTemplate.TestCrc())
            {
                //throw new Exception("Workspace verification failed: data may be corrupted");
            }

            haveToVerifyCRC = true;

        }

        private static void FixWorkspace()
        {
            // Stopwatch sw = new Stopwatch();



            using (ISession session = SessionManager.OpenSession())
            {
                if (Scenario.FindDefault(session) == null)
                {
                    Scenario newDefault = Scenario.FindSelected(session).FirstOrDefault();
                    if (newDefault != null) newDefault.IsDefault = true;
                }

                DateTime now = DateTime.Now;
                foreach (Vector vector in Vector.FindAll(session))
                    if (vector.CreationDate == null) vector.CreationDate = now;
                foreach (Variable variable in Variable.FindAll(session))
                    if (variable.CreationDate == null) variable.CreationDate = now;

                //fix products non-swaptions with LinkedProducts
                //MTU : abandon� car trop de temps (3 min pour 4500 bonds). faite avant le delete de bonds, apparement seul endroit ou ceci pose un pb
                //sw.Reset();
                //sw.Start();
                //IEnumerable<Product> nonSwaptionsWithLinkedProducts = Product.FindNonSwaptionsWithLinkedProducts(session);
                //foreach (Product prod in nonSwaptionsWithLinkedProducts)
                //{
                //    prod.LinkedProducts.Clear();
                //    prod.Save(session);
                //}
                //sw.Stop();
                //Console.WriteLine(sw.ElapsedMilliseconds);

                //move products linked to a non terminal category to a (root) sub-category
                Product[] allProducts = Product.FindAll(session);
                Category newCateg;
                //Hashtable times = new Hashtable(allProducts.Length);
                foreach (Product prod in allProducts)
                {
                    if (double.IsInfinity(prod.BookPriceValue) ||
                        double.IsNegativeInfinity(prod.BookPriceValue) ||
                        double.IsNaN(prod.BookPriceValue))
                    {
                        prod.BookPrice = "100";
                        prod.BookPriceValue = 1;
                    }

                    if (prod.CouponType == CouponType.Variable)
                    {
                        prod.CouponType = CouponType.Floating;
                    }
                    //sw.Reset();
                    //sw.Start();
                    if (prod.Category.Children.Count > 0)
                    {
                        newCateg = prod.Category.Children.FirstOrDefault(d => d.Label == "(miscellaneous)");
                        if (newCateg == null)
                        {
                            newCateg = new Category
                                           {
                                               Parent = prod.Category,
                                               Label = "(miscellaneous)",
                                               Bonds = new List<Product>(),
                                               Children = new List<Category>(),
                                               BalanceType = prod.Category.BalanceType,
                                               isVisible = prod.Category.isVisible,
                                               IsVisibleChecked = prod.Category.IsVisibleChecked,
                                               IsVisibleCheckedModel = prod.Category.IsVisibleCheckedModel,
                                               Type = prod.Category.Type,
                                               Position = prod.Category.Children.Count,
                                               ReadOnly = false
                                           };
                            session.Save(newCateg);
                            prod.Category.Children.Add(newCateg);
                        }
                        //prod.Category.Bonds.Remove(prod);
                        prod.Category = newCateg;
                        newCateg.Bonds.Add(prod);

                        prod.Category.Save(session);
                        prod.Save(session);
                    }
                    //sw.Stop();
                    //times.Add(prod.ToString(),sw.ElapsedMilliseconds);
                }


                session.Flush();
                

            }

            BalanceBL.EnsureRollCategoryHasDefaultSubCategory();
        }

        private static WorkspaceTemplate LoadWorkspaceTemplate(string filename)
        {
            WorkspaceTemplate workspaceTemplate;
            bool versionChanged = false;
            using (FileStream stream = File.Open(filename, FileMode.Open))
            {
                // return DeserializeXmlScenarioTemplate(stream);

                stream.Seek(GzippedXmlSignatureLength, SeekOrigin.Begin);
                int version = stream.ReadByte();

                if (version > FileVersion)
                {
                    throw new Exception("Invalid file format. Please update ." + Constants.APPLICATION_NAME);
                }

                using (GZipStream gZipStream = new GZipStream(stream, CompressionMode.Decompress))
                {
                    workspaceTemplate = DeserializeXmlScenarioTemplate(gZipStream);
                    switch (version)
                    {
                        case 1:
                            versionChanged = true;
                            MigrateToV2(workspaceTemplate);
                            goto case 2;
                        case 2:
                            versionChanged = true;
                            MigrateFromV2ToV3(workspaceTemplate);
                            goto case 3;
                        case 3:
                            versionChanged = true;
                            MigrateFromV3ToV4(workspaceTemplate);
                            goto case 4;
                        case 4:
                            versionChanged = true;
                            MigrateFromV4ToV5(workspaceTemplate);
                            goto case 5;
                        case 5:
                            versionChanged = true;
                            MigrateFromV5ToV6(workspaceTemplate);
                            break;
                        case 6:
                            versionChanged = true;
                            MigrateFromV6ToV7(workspaceTemplate);
                            break;
                    }
                }
            }

            haveToVerifyCRC = !versionChanged;

            if (versionChanged)
            {

                //todo save
            }
            return workspaceTemplate;
        }

        private static void MigrateToV2(WorkspaceTemplate ws)
        {
            ws.Categories = IOTools.migrateCategoriesToV2(ws.Categories);
        }


        private static void MigrateFromV3ToV4(WorkspaceTemplate ws)
        {
            DateTime valuationDate = GetValuationDateFromWorkspace(ws);

            ws.VectorPoints = Tools.RedateVectorsFromWorkspace(ws.VectorPoints, valuationDate);
        }

        private static void MigrateFromV4ToV5(WorkspaceTemplate ws)
        {
            int i = 1;
            foreach (Category category in ws.Categories)
            {
                category.ALMID = i++;
            }
        }

        private static void MigrateFromV5ToV6(WorkspaceTemplate ws)
        {
            foreach (Product product in ws.Products)
            {
                product.ComplementString = product.Complement.ToString();
            }
        }

        private static void MigrateFromV6ToV7(WorkspaceTemplate ws)
        {
            foreach (Product product in ws.Products)
            {
                product.FirstRate = product.FirstCoupon;
            }
        }

        //private static void MigrateFromV5ToV6(WorkspaceTemplate ws)
        //{

        //}

        private static DateTime GetValuationDateFromWorkspace(WorkspaceTemplate ws)
        {
            DateTime valuationDate = DateTime.Now;
            foreach (Param param in ws.Parameters)
            {
                if (param.Id == Param.VALUATION_DATE)
                {
                    valuationDate = Tools.ResetDateToFirstOfMonth(DateTime.Parse(param.Val));
                    break;
                }
            }
            return valuationDate;
        }

        private static void MigrateFromV2ToV3(WorkspaceTemplate ws)
        {
            //MTU : en stand by

            ////MTU add new value at valuationdate-1=val((val date)
            //DateTime valuationDate = DateTime.Now;
            //foreach (Param param in ws.Parameters)
            //{
            //    if (param.Id == Param.VALUATION_DATE)
            //    {
            //        valuationDate = Tools.ResetDateToFirstOfMonth(DateTime.Parse(param.Val));
            //        break;
            //    }
            //}

            //VectorPoint vp;
            //List<VectorPoint> newVps=new List<VectorPoint>();
            //foreach (VectorPoint vectorPoint in ws.VectorPoints)
            //{//MTU : set the Opening Balance Vector Value
            //    if (/*vectorPoint.Vector.Frequency == VectorFrequency.Monthly &&*/
            //        vectorPoint.Date.Month == valuationDate.Month && vectorPoint.Date.Year == valuationDate.Year)
            //    {
            //        vp = new VectorPoint();
            //        vp.Date = valuationDate.AddMonths(-1);
            //        vp.Value = vectorPoint.Value;
            //        vp.Vector = vectorPoint.Vector;
            //        newVps.Add(vp);
            //    }
            //}
            //VectorPoint[] newAll = new VectorPoint[ws.VectorPoints.Length+newVps.Count];
            //Array.Copy(ws.VectorPoints,newAll,ws.VectorPoints.Length);
            //Array.Copy(newVps.ToArray(), 0, newAll, ws.VectorPoints.Length, newVps.Count);
            //ws.VectorPoints = newAll;
        }


        private static WorkspaceTemplate DeserializeXmlScenarioTemplate(Stream stream)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(WorkspaceTemplate));
            serializer.UnknownElement += SerializerUnknownElement;
            serializer.UnknownNode += SerializerUnknownNode;
            serializer.UnknownAttribute += SerializerUnknownAttribute;
            serializer.UnreferencedObject += SerializerUnreferencedObject;
            WorkspaceTemplate workspaceTemplate = (WorkspaceTemplate)serializer.Deserialize(stream);
            return workspaceTemplate;
        }

        static void SerializerUnreferencedObject(object sender, UnreferencedObjectEventArgs e)
        {
            Debug.WriteLine(e.UnreferencedId);
        }

        static void SerializerUnknownAttribute(object sender, XmlAttributeEventArgs e)
        {
            Debug.WriteLine(e.Attr.Value);
        }

        static void SerializerUnknownNode(object sender, XmlNodeEventArgs e)
        {
            Debug.WriteLine(e.Text);
        }

        static void SerializerUnknownElement(object sender, XmlElementEventArgs e)
        {
            Debug.WriteLine(e.Element.OuterXml);
        }
    }
}