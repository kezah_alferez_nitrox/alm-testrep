using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using ALMS.Products;
using InsuranceALM.Infrastructure.Data;
using InsuranceBondManager.Domain;
using System.ComponentModel;
using NHibernate;

namespace InsuranceBondManager.Core.IO
{
    [XmlRoot("Workspace")]
    public class WorkspaceTemplate
    {
        private Category[] categories;
        private Product[] products;

        private Variable[] variables;
        private VariableValue[] variableValues;
        private Scenario[] scenarios;
        private Vector[] vectors;
        private VectorPoint[] vectorPoints;
        private FundamentalVector[] fundamentalVectors;
        private DynamicVariable[] dynamicVariables;

        private Currency[] currencies;
        private FXRate[] fxRates;
        private Param[] parameters;

        private OtherItemAssum[] oias;
        private Dividend[] dividends;

        public void LoadFromDatabase(BackgroundWorker backgroundWorker)
        {
            using (ISession session = SessionManager.OpenSession())
            {
                categories = Category.FindAll(session);
                backgroundWorker.ReportProgress(5);
                this.products = Product.FindAll(session);
                this.PrepareProducts();

                backgroundWorker.ReportProgress(10);
                variables = Variable.FindAll(session);
                backgroundWorker.ReportProgress(11);
                variableValues = VariableValue.FindAll(session);
                backgroundWorker.ReportProgress(13);
                scenarios = Scenario.FindAll(session);
                backgroundWorker.ReportProgress(14);
                vectors = Vector.FindAll(session); // Vector.FindByGroup(VectorGroup.Workspace);
                backgroundWorker.ReportProgress(15);
                fundamentalVectors = FundamentalVector.FindAll(session);
                backgroundWorker.ReportProgress(16);
                dynamicVariables = DynamicVariable.FindAll(session);
                backgroundWorker.ReportProgress(17);
                vectorPoints = VectorPoint.FindAll(session);
                backgroundWorker.ReportProgress(20);
                // VectorPoint.FindByVectorGroup(VectorGroup.Workspace);
                currencies = Currency.FindAll(session);
                backgroundWorker.ReportProgress(21);
                fxRates = FXRate.FindAll(session);
                backgroundWorker.ReportProgress(22);
                parameters = Param.FindAll(session);
                backgroundWorker.ReportProgress(23);
                oias = OtherItemAssum.FindAll(session);
                SolvencyAdjustments = SolvencyAdjustment.FindAll(session);
                VariableCorrelation = Domain.VariableCorrelation.FindAll(session);
                VolatilityConfig = Domain.VolatilityConfig.FindAll(session);
                backgroundWorker.ReportProgress(24);
                dividends = Dividend.FindAll(session);
                backgroundWorker.ReportProgress(25);
            }
        }

        private void PrepareProducts()
        {
            foreach (Product product in this.products)
            {
                if (product.ProductType == ProductType.Swaption || product.ProductType == ProductType.Swap)
                {
                    product.LinkedProductsIds = product.LinkedProducts.Select(p => p.Id).ToArray();
                }else
                {
                    product.LinkedProductsIds = new int[0];
                }
            }
        }

        private uint GetCrc()
        {
            return GetCrc(categories) ^
                   GetCrc(products) ^
                   GetCrc(variables) ^
                   GetCrc(variableValues) ^
                   GetCrc(scenarios) ^
                   GetCrc(vectors) ^
                   GetCrc(vectorPoints) ^
                   GetCrc(fundamentalVectors) ^
                   GetCrc(dynamicVariables) ^
                   GetCrc(currencies) ^
                   GetCrc(fxRates) ^
                   GetCrc(parameters) ^
                   GetCrc(oias) ^
                   GetCrc(dividends);
        }

        private uint GetCrc(IEnumerable<object> objects)
        {
            if (objects == null) return 0;

            return objects.Aggregate<object, uint>(0, (o1, o2) => o1 ^ EntityUtil.ComputeChecksum(o2));
        }

        public void ComputeCrc()
        {
            this.Crc = GetCrc();
        }

        public bool TestCrc()
        {
            if (this.Crc == 0) return true;

            return this.Crc == GetCrc();
        }

        [XmlElement]
        public Category[] Categories
        {
            get { return categories; }
            set { categories = value; }
        }

        [XmlElement("Bonds")]
        public Product[] Products
        {
            get { return this.products; }
            set { this.products = value; }
        }

        [XmlElement]
        public Variable[] Variables
        {
            get { return variables; }
            set { variables = value; }
        }

        [XmlElement]
        public VariableValue[] VariableValues
        {
            get { return variableValues; }
            set { variableValues = value; }
        }

        [XmlElement]
        public Scenario[] Scenarios
        {
            get { return scenarios; }
            set { scenarios = value; }
        }

        [XmlElement]
        public Vector[] Vectors
        {
            get { return vectors; }
            set { vectors = value; }
        }

        [XmlElement]
        public VectorPoint[] VectorPoints
        {
            get { return vectorPoints; }
            set { vectorPoints = value; }
        }

        [XmlElement]
        public FundamentalVector[] FundamentalVectors
        {
            get { return fundamentalVectors; }
            set { fundamentalVectors = value; }
        }

        [XmlElement]
        public DynamicVariable[] DynamicVariables
        {
            get { return this.dynamicVariables; }
            set { this.dynamicVariables = value; }
        }

        [XmlElement]
        public Currency[] Currencies
        {
            get { return currencies; }
            set { currencies = value; }
        }

        [XmlElement]
        public FXRate[] FxRates
        {
            get { return fxRates; }
            set { fxRates = value; }
        }

        [XmlElement]
        public Param[] Parameters
        {
            get { return parameters; }
            set { parameters = value; }
        }

        [XmlElement]
        public OtherItemAssum[] OIAs
        {
            get { return oias; }
            set { oias = value; }
        }

        [XmlElement]
        public Dividend[] Dividends
        {
            get { return dividends; }
            set { dividends = value; }
        }

        [XmlElement]
        public GeneratorResultNode[] GeneratorResultNodes { get; set; }

        [XmlElement]
        public int[] ScenarioSetupColumnsOrder { get; set; }

        [XmlElement]
        public SolvencyAdjustment[] SolvencyAdjustments { get; set; }

        [XmlElement]
        public VariableCorrelation[] VariableCorrelation { get; set; }

        [XmlElement]
        public VolatilityConfig[] VolatilityConfig { get; set; }

        [XmlAttributeAttribute]
        public uint Crc { get; set; }
    }
}