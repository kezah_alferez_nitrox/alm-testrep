namespace InsuranceBondManager.Core.IO
{
    internal interface IBalanceSerializer {
        void Save(string fileName);
        void Load(string fileName);
    }
}