using System.Xml.Serialization;
using InsuranceALM.Infrastructure.Data;
using InsuranceBondManager.Domain;
using NHibernate;

namespace InsuranceBondManager.Core.IO
{
    [XmlRoot("SaveOpenFile")]
    public class BalanceTemplate
    {
        public bool isLoad = false;

        private Category[] categories;
        private Product[] products;
        private Currency[] currencies;
        private OtherItemAssum[] otherItemAssums;

        [XmlElement]
        public Category[] Categories
        {
            get
            {
                using (ISession session = SessionManager.OpenSession())
                {
                    if (isLoad)
                        return categories;
                    else
                        return Category.FindAll(session);
                }
            }

            set { categories = value; }

        }

        [XmlElement("Bonds")]
        public Product[] Products
        {
            get
            {
                using (ISession session = SessionManager.OpenSession())
                {
                    if (isLoad)
                        return this.products;
                    else
                        return Product.FindAll(session);
                }
            }
            set { this.products = value; }
        }

        [XmlElement]
        public Currency[] Currencies
        {
            get
            {
                using (ISession session = SessionManager.OpenSession())
                {
                    if (isLoad)
                        return currencies;
                    else
                        return Currency.FindAll(session);
                }
            }
            set { currencies = value; }
        }

        [XmlElement]
        public OtherItemAssum[] OthierItemAssums
        {
            get
            {
                using (ISession session = SessionManager.OpenSession())
                {
                    if (isLoad) return otherItemAssums;
                    else return OtherItemAssum.FindAll(session);
                }
            }
            set { otherItemAssums = value; }
        }
    }
}