﻿using System;
using InsuranceBondManager.Domain;
using NHibernate;

namespace InsuranceBondManager.Core.IO
{
    public class AlmIdGenerator
    {
        private int? assetAlmId;
        private int? equityAlmId;
        private int? liabilityAlmId;
        private int? rollAlmId;
        private int? offBalanceSheetAlmId;

        public void SetAlmId(ISession session, Product product)
        {
            int almId = 0;
            if (product.Category.Type == CategoryType.Roll)
            {
                this.rollAlmId = this.rollAlmId ??
                                 Product.GetMaxAlmIdForCategory(session, new Category {Type = CategoryType.Roll});
                this.rollAlmId++;
                almId = (int) this.rollAlmId;
            }
            else
            {
                switch (product.Category.BalanceType)
                {
                    case BalanceType.Asset:
                        this.assetAlmId = this.assetAlmId ??
                                          Product.GetMaxAlmIdForCategory(session, new Category {BalanceType = BalanceType.Asset});
                        this.assetAlmId++;
                        almId = this.assetAlmId.GetValueOrDefault();
                        break;
                    case BalanceType.Liability:
                        this.liabilityAlmId = this.liabilityAlmId ??
                                              Product.GetMaxAlmIdForCategory(session, new Category
                                                                                 {BalanceType = BalanceType.Liability});
                        this.liabilityAlmId++;
                        almId = this.liabilityAlmId.GetValueOrDefault();
                        break;
                    case BalanceType.Equity:
                        this.equityAlmId = this.equityAlmId ??
                                           Product.GetMaxAlmIdForCategory(session, new Category
                                                                              {BalanceType = BalanceType.Equity});
                        this.equityAlmId++;
                        almId = this.equityAlmId.GetValueOrDefault();
                        break;
                    case BalanceType.Derivatives:
                        this.offBalanceSheetAlmId = this.offBalanceSheetAlmId ??
                                           Product.GetMaxAlmIdForCategory(session, new Category { BalanceType = BalanceType.Derivatives });
                        this.offBalanceSheetAlmId++;
                        almId = this.offBalanceSheetAlmId.GetValueOrDefault();
                        break;
                    default : throw new ArgumentException("There is no such BalanceType as "+product.Category.BalanceType + " for "+product.Description + " in " + product.Category.Label);
                }
            }

            if (almId > 0)
            {
                product.ALMID = almId;
            }
        }
    }
}