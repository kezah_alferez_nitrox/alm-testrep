using System;
using System.ComponentModel;
using InsuranceBondManager.Domain;

namespace InsuranceBondManager.Core.Import
{
    public interface IMarketDataImporter
    {
        bool CanImportFrom(string fileName);
        Vector[] ImportFrom(string fileName, VectorGroup vectorGroup, DateTime startDate, DataDirection dataDirection, BackgroundWorker backgroundWorker);
    }
}