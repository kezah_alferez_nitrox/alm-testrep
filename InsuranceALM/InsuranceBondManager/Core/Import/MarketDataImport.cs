using System;
using System.ComponentModel;
using System.Globalization;
using System.Threading;
using InsuranceALM.Infrastructure.Data;
using InsuranceBondManager.BusinessLogic;
using InsuranceBondManager.Domain;
using Eu.AlterSystems.ASNetLib.Core;
using NHibernate;


namespace InsuranceBondManager.Core.Import
{
    public class MarketDataImport
    {
        private readonly string fileName;
        private readonly DateTime startDate;
        private readonly VectorGroup vectorGroup;
        private readonly DataDirection dataDirection;

        public int ImportedVectorCount { get; set; }

        public MarketDataImport(string fileName, DateTime startDate,VectorGroup vectorGroup, DataDirection dataDirection)
        {
            this.fileName = fileName;
            this.startDate = startDate;
            this.vectorGroup = vectorGroup;
            this.dataDirection = dataDirection;
            this.ImportedVectorCount = 0;
        }

        public void Import(BackgroundWorker backgroundWorker)
        {
            using (ISession session = SessionManager.OpenSession())
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo(Constants.CULTURE_NAME);

                IMarketDataImporter importer = new ExcelMarketDataImporter(session);

                Vector[] vectors = importer.ImportFrom(fileName, vectorGroup, startDate, dataDirection, backgroundWorker);

                DateTime now = DateTime.Now;

                int count = vectors.Length;
                for (int i = 0; i < count; i++)
                {
                    backgroundWorker.ReportProgress(50 + 49 * (i + 1) / count);
                    Vector vector = vectors[i];
                    vector.CreationDate = now;
                    vector.Save(session);

                    if (i % 50 == 0 || i == count - 1)
                    {
                        session.Flush();
                        session.Clear();
                    }
                }

                session.Close();

                ImportedVectorCount = vectors.Length;
                CurrencyBL.ResetValuationDateFxRates();
            }
        }
    }

    public enum DataDirection
    {
        [EnumDescription("Rows")]
        Horizontal,
        [EnumDescription("Columns")]
        Vertical,
    }
}