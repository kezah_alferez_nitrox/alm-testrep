﻿using System;
using System.Collections.Generic;
using System.Text;
using InsuranceBondManager.Calculations;

namespace InsuranceBondManager.Core
{
    public class MultiResultModel
    {
        public IDictionary<Calculations.ReportType, ResultModel> ResultModels { get; set; }

        public MultiResultModel()
        {
            ResultModels = new Dictionary<Calculations.ReportType, ResultModel>();
        }
    }
}
