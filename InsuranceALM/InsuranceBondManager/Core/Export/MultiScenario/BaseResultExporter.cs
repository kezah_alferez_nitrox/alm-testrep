using System;
using System.Collections.Generic;
using InsuranceBondManager.Calculations;
using InsuranceBondManager.Domain;
using InsuranceBondManager.Resources.Language;
using Excel = Microsoft.Office.Interop.Excel;

namespace InsuranceBondManager.Core.Export.MultiScenario
{
    public abstract class BaseResultExporter
    {
        private readonly Excel.Worksheet worksheet;
        private readonly IDictionary<Scenario, ResultModel> models;
        private readonly int columnCount;
        private readonly int rowCount;
        private readonly ReportAggregationType aggregationType;

        protected BaseResultExporter(Excel.Worksheet worksheet, IDictionary<Scenario, ResultModel> simulation, int columns, int rows, ReportAggregationType aggregationType)
        {
            this.worksheet = worksheet;
            this.models = simulation;
            this.columnCount = columns;
            this.rowCount = rows;
            this.aggregationType = aggregationType;
        }

        public void Export()
        {
            if (this.models.Count == 0) return;

            this.PrepareCells();

            ExcelUtil.InsertRows(worksheet, 3, rowCount);

            int scenarioNo = 0;
            foreach (Scenario scenario in this.models.Keys)
            {
                ExcelUtil.SetCellValue(worksheet, 1, 2 + scenarioNo * columnCount, Labels.BaseResultExporter_Export_SCENARIO_ + scenario.Name + " (" + aggregationType.ToString() + ")");

                ResultModel result = this.models[scenario];
                IList<ResultRowModel> resultRows = result.GetRows(ReportAnalysisType.Standard);

                resultRows = FlattenTree(resultRows);

                int skippedRows = 0;
                for (int rowNo = 0; rowNo < resultRows.Count; rowNo++)
                {
                    ResultRowModel row = resultRows[rowNo];

                    if (rowNo == 0)
                    {
                        for (int colNo = 0; colNo < columnCount; colNo++)
                        {
                            ExcelUtil.SetCellValue(this.worksheet, 2,
                                2 + scenarioNo * columnCount + colNo, row[colNo]);
                        }
                        continue;
                    }

                    if (this.SkipRow(row))
                    {
                        skippedRows++;
                        continue;
                    }

                    if (scenarioNo == 0)
                    {
                        ExcelUtil.SetCellValue(this.worksheet, 2 + rowNo - skippedRows, 1, row.Description);
                        if (row.Type == FormattingType.Header || row.Type == FormattingType.MasterHeader)
                        {
                            ExcelUtil.MakeRowBold(this.worksheet, 2 + rowNo - skippedRows);
                        }
                    }

                    for (int colNo = 0; colNo < columnCount; colNo++)
                    {
                        ExcelUtil.SetCellValue(this.worksheet, 2 + rowNo - skippedRows,
                            2 + scenarioNo * columnCount + colNo, row[colNo]);
                    }
                }

                scenarioNo++;
            }
        }

        private IList<ResultRowModel> FlattenTree(IEnumerable<ResultRowModel> input, int indent = 0)
        {
            List<ResultRowModel> output = new List<ResultRowModel>();

            foreach (ResultRowModel row in input)
            {
                row.Description = new string(' ', indent * 2) + row.Description;
                output.Add(row);
                output.AddRange(this.FlattenTree(row.Children, indent + 1));
            }
   
            return output;
        }

        protected abstract bool SkipRow(ResultRowModel row);

        private void PrepareCells()
        {
            ExcelUtil.InsertColumns(this.worksheet, 3, this.columnCount * this.models.Count - 2);

            ExcelUtil.Merge(this.worksheet, 2, columnCount);
            for (int i = 1; i < this.models.Count; i++)
            {
                ExcelUtil.Merge(this.worksheet, 2 + i * this.columnCount, columnCount);
                ExcelUtil.CopyColumns(this.worksheet, 2, columnCount, 2 + i * this.columnCount);
            }
        }
    }
}