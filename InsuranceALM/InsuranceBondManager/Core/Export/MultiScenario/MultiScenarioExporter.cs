using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using InsuranceALM.Infrastructure.Data;
using InsuranceBondManager.Calculations;
using InsuranceBondManager.Domain;
using NHibernate;
using Excel=Microsoft.Office.Interop.Excel;


namespace InsuranceBondManager.Core.Export.MultiScenario
{
    public class MultiScenarioExporter
    {
        private readonly Simulation simulation;
        private readonly Calculations.ReportType reportType;
        private readonly ReportAggregationType aggregationType;
        private readonly DateTime valuationDate;
        private int resultColumns = 0;
        private int resultRows = 0;

        private readonly IDictionary<Scenario, ResultModel> models = new Dictionary<Scenario, ResultModel>();

        public MultiScenarioExporter(Simulation simulation, Calculations.ReportType reportType, ReportAggregationType aggregationType, DateTime valuationDate)
        {
            this.simulation = simulation;
            this.reportType = reportType;
            this.aggregationType = aggregationType;
            this.valuationDate = valuationDate;
        }

        public void Export()
        {
            Cursor.Current = Cursors.WaitCursor;
            string templatePath = Path.GetDirectoryName(Application.ExecutablePath) + "\\Excel\\Template\\MultiScenarioTemplate.xls ";

            string outFolder = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) +
                               "\\ALMS\\Excel\\Reports";

            if (!Directory.Exists(outFolder)) Directory.CreateDirectory(outFolder);

            string outputPath = outFolder + "\\MultiScenario_" + DateTime.Now.ToString("dd_MM_yyyy_hh-mm-ss") + ".xls";
            if (File.Exists(outputPath)) File.Delete(outputPath);

            Excel.Application app = null;
            Excel.Workbook workBook = null;

            try
            {
                app = new Excel.Application();
                app.Visible = false;

                workBook = app.Workbooks.Open(templatePath,
                                              0,
                                              true,
                                              5,
                                              "",
                                              "",
                                              true,
                                              Excel.XlPlatform.xlWindows,
                                              "\t",
                                              false,
                                              false,
                                              0,
                                              true,
                                              1,
                                              0);

                this.ExportToWorkbook(workBook);

                workBook.Close(true, outputPath, true);
            }
            catch (Exception ex)
            {
                Log.Exception("MultiScenario export ", ex);
                throw;
            }
            finally
            {
                if (null != workBook)
                {
                    Marshal.ReleaseComObject(workBook);
                }
                if (null != app)
                {
                    app.Quit();
                    Marshal.ReleaseComObject(app);
                }
            }

            GC.Collect();
            GC.WaitForPendingFinalizers();

            System.Diagnostics.Process.Start(outputPath);

            Cursor.Current = Cursors.Default;
        }

        private void ExportToWorkbook(Excel.Workbook workbook)
        {
            //using (new SessionScope(FlushAction.Never))
            using (ISession session = SessionManager.OpenSession())
            {
                this.PrepareData(session);

                SyntheseExporter syntheseExporter = new SyntheseExporter((Excel.Worksheet) workbook.Sheets[1],
                                                                         this.models, this.resultColumns);
                syntheseExporter.Export();

                HypotesesBaseExporter hypotesesBaseExporter =
                    new HypotesesBaseExporter(session, (Excel.Worksheet) workbook.Sheets[2], this.models, this.aggregationType);
                hypotesesBaseExporter.Export();

                BalanceSheetExporter balanceSheetExporter =
                    new BalanceSheetExporter((Excel.Worksheet) workbook.Sheets[3], this.models, this.resultColumns,
                                             this.resultRows, this.aggregationType);
                balanceSheetExporter.Export();

                ProfitAndLossExporter profitAndLossExporter =
                    new ProfitAndLossExporter((Excel.Worksheet) workbook.Sheets[4], this.models, this.resultColumns,
                                              this.resultRows, this.aggregationType);
                profitAndLossExporter.Export();
            }
        }

        private void PrepareData(ISession session)
        {
            Category[] categories = Category.FindAll(session);
            foreach (Scenario scenario in this.simulation.Scenarios)
            {
                ScenarioData data = this.simulation[scenario];
                ResultModel resultModel = new ResultModel(data, categories, this.reportType, this.aggregationType, this.valuationDate,ReportAnalysisType.Standard);
                this.models.Add(scenario, resultModel);

                this.resultRows = resultModel.GetRows(ReportAnalysisType.Standard).Count;
                this.resultColumns = Math.Max(this.resultColumns, resultModel.DateIntervals.Count);
            }            
        }
    }
}