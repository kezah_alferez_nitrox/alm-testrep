using System;
using System.Collections.Generic;
using System.Linq;
using InsuranceBondManager.Calculations;
using InsuranceBondManager.Domain;
using InsuranceBondManager.Resources.Language;
using Excel = Microsoft.Office.Interop.Excel;

namespace InsuranceBondManager.Core.Export.MultiScenario
{
    public class SyntheseExporter
    {
        private const int BalanceSheetInitialRow = 7;
        private const int EquityInitialRow = 11;
        private const int NetIncomeInitialRow = 3;

        private readonly Excel.Worksheet worksheet;
        private readonly IDictionary<Scenario, ResultModel> models;
        private readonly int columnCount;

        public SyntheseExporter(Excel.Worksheet worksheet, IDictionary<Scenario, ResultModel> simulation, int columns)
        {
            this.worksheet = worksheet;
            this.models = simulation;
            this.columnCount = columns;
        }

        public void Export()
        {
            if (this.models.Count == 0) return;

            ExcelUtil.InsertRows(this.worksheet, EquityInitialRow, this.models.Count - 1);
            ExcelUtil.InsertRows(this.worksheet, BalanceSheetInitialRow, this.models.Count - 1);
            ExcelUtil.InsertRows(this.worksheet, NetIncomeInitialRow, this.models.Count - 1);

            ExcelUtil.InsertColumns(this.worksheet, 3, this.columnCount - 2);

            int i = 0;
            foreach (Scenario scenario in this.models.Keys)
            {
                ResultModel result = this.models[scenario];
                IList<ResultRowModel> resultRows = result.GetRows(ReportAnalysisType.Standard);

                int firstRow = NetIncomeInitialRow;
                ExcelUtil.SetCellValue(this.worksheet, firstRow + i, 1, scenario.Name);
                this.FillRow(firstRow, i, resultRows, SyntheseType.NetIncome);

                firstRow = BalanceSheetInitialRow + this.models.Count - 1;
                ExcelUtil.SetCellValue(this.worksheet, firstRow + i, 1, scenario.Name);
                this.FillRow(firstRow, i, resultRows, SyntheseType.TotalBalanceSheet);

                firstRow = EquityInitialRow + this.models.Count * 2 - 2;
                ExcelUtil.SetCellValue(this.worksheet, firstRow + i, 1, scenario.Name);
                this.FillRow(firstRow, i, resultRows, SyntheseType.TotalEquity);

                i++;
            }
        }

        private void FillRow(int firstRow, int i, IList<ResultRowModel> resultRows, SyntheseType syntheseType)
        {
            for (int j = 0; j < this.columnCount; j++)
            {
                ExcelUtil.SetCellValue(this.worksheet, firstRow - 1, 2 + j, resultRows[0][j]);
                ResultRowModel row = FindRow(resultRows, syntheseType);
                if (row != null)
                {
                    ExcelUtil.SetCellValue(this.worksheet, firstRow + i, 2 + j, row[j]);
                }
            }
        }

        private static ResultRowModel FindRow(IList<ResultRowModel> rows, SyntheseType syntheseType)
        {
            ResultRowModel found = rows.FirstOrDefault(row => row.SyntheseType == syntheseType);
            if (found != null) return found;

            foreach (ResultRowModel r in rows)
            {
                found = FindRow(r.Children, syntheseType);
                if (found != null) break;
            }

            return found;
        }
    }
}