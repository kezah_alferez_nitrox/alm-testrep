using System;
using System.Collections.Generic;
using InsuranceBondManager.Calculations;
using Microsoft.Office.Interop.Excel;
using Scenario=InsuranceBondManager.Domain.Scenario;

namespace InsuranceBondManager.Core.Export.MultiScenario
{
    public class ProfitAndLossExporter : BaseResultExporter
    {
        public ProfitAndLossExporter(Worksheet worksheet, IDictionary<Scenario, ResultModel> simulation, int columns, int rows, ReportAggregationType aggregationType)
            : base(worksheet, simulation, columns, rows, aggregationType)
        {
        }

        protected override bool SkipRow(ResultRowModel row)
        {
            return (row.ResultSection.GetValueOrDefault() == ResultSection.BookValue  ||
                row.ResultSection.GetValueOrDefault() == ResultSection.Yield);
        }
    }
}