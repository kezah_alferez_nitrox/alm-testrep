using NHibernate;

namespace InsuranceBondManager.Core
{
    public interface ISessionHolder
    {
        ISession Session { get; }
        void SaveChangesInSession();
    }
}