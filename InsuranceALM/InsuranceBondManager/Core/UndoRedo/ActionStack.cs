﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using InsuranceBondManager.Domain;

namespace InsuranceBondManager.Core.UndoRedo
{
    public class ActionStack
    {


        public IList<BalanceSheetOperation> Stack { get; set; }
        public int CurrentPosition { get; set; }

        private static Lazy<ActionStack> instance = new Lazy<ActionStack>(true);

        public static ActionStack GetInstance()
        {
            return instance.Value;
        }

        public ActionStack()
        {
            Stack = new List<BalanceSheetOperation>();
            CurrentPosition = -1;
        }

        public void Do(BalanceSheetOperation bso)
        {
            //if called with same arguments then do not add to stack
            //if (CurrentPosition > -1)
            //{
            //    List<Product> diff1 = Stack[CurrentPosition].UndoProducts.Except(bso.UndoProducts).ToList();
            //    List<Product> diff2 = bso.UndoProducts.Except(Stack[CurrentPosition].UndoProducts).ToList();

            //    bool same = (diff1.Count == 0 && diff2.Count == 0);
            //    if (same) return;
            //}

            RemoveAfterCurrentPosition();
            Stack.Add(bso);
            CurrentPosition = Stack.Count - 1;
        }

        private void RemoveAfterCurrentPosition()
        {
            int count = Stack.Count;
            for (int i = CurrentPosition + 1; i < count; i++)
            {
                Stack.RemoveAt(Stack.Count - 1);
            }
        }

        public IBalanceSheetOperationPosition Undo()
        {
            if (!CanUndo()) return null;
            BalanceSheetOperation bso = Stack[CurrentPosition];
            bso.Undo();
            CurrentPosition--;
            return bso;
        }
        public IBalanceSheetOperationPosition Redo()
        {
            if (!CanRedo()) return null;
            CurrentPosition++;
            BalanceSheetOperation bso = Stack[CurrentPosition];
            bso.Redo();
            
            return bso;

        }

        public bool CanUndo()
        {
            return CurrentPosition > -1;
        }
        public bool CanRedo()
        {
            return CurrentPosition < Stack.Count - 1;
        }

    }

}
