﻿using System.Collections.Generic;
using System.Diagnostics;
using InsuranceBondManager.Domain;
using InsuranceALM.Infrastructure.Data;
using NHibernate;

namespace InsuranceBondManager.Core.UndoRedo
{
    //public enum OperationType
    //{
    //    AddLines,
    //    RemoveLines,
    //    ChangeLines,
    //    ChangeCategory,
    //    RemoveCategory,
    //    AddCategory
    //}

    public interface IBalanceSheetOperationPosition
    {
        int Page { get; set; }
        int Row { get; set; }
    }

    public abstract class BalanceSheetOperation : IBalanceSheetOperationPosition
    {
        public Category Category { get; set; }
        
        public int Page { get; set; }
        public int Row { get; set; }
        public List<Product> UndoProducts { get; set; }
        public List<Product> RedoProducts { get; set; }
        public BalanceSheetOperation(Category category,
        
                                    int page,
                                    int row,
                                    List<Product> undoProducts,
                                    List<Product> redoProducts)
        {
            Category = category;
        
            Page = page;
            Row = row;
            UndoProducts = undoProducts;
            RedoProducts = redoProducts;
        }

        public abstract void Undo();
        public abstract void Redo();
    }

    public class AddLinesBalanceSheetOperation : BalanceSheetOperation
    {
        public AddLinesBalanceSheetOperation(Category category, int page, int row, List<Product> undoProducts,
                                    List<Product> redoProducts)
            : base(category, page, row, undoProducts,redoProducts)
        {
        }

        public override void Undo()
        {
            using (IStatelessSession session = SessionManager.OpenStatelessSession())
            {
                foreach (Product product in UndoProducts)
                {
                    session.Delete(product);
                }
            }
        }

        public override void Redo()
        {
            using (ISession session = SessionManager.OpenSession())
            {
                foreach (Product product in RedoProducts)
                {
                    Debug.WriteLine("id before=" + product.Id);
                    session.SaveOrUpdateCopy(product, product.Id);
                    Debug.WriteLine("id after=" + product.Id);
                }
                session.Flush();
            }
            
        }
    }

    public class RemoveLinesBalanceSheetOperation : BalanceSheetOperation
    {
        public RemoveLinesBalanceSheetOperation(Category category, int page, int row, List<Product> undoProducts,
                                    List<Product> redoProducts)
            : base(category, page, row, undoProducts, redoProducts)
        {
        }

        public override void Undo()
        {
            using (ISession session = SessionManager.OpenSession())
            {
                foreach (Product product in UndoProducts)
                {
                    Debug.WriteLine("id before="+product.Id);
                    session.SaveOrUpdateCopy(product, product.Id);
                    Debug.WriteLine("id after=" + product.Id);
                }
                session.Flush();
            }
        }

        public override void Redo()
        {
            using (IStatelessSession session = SessionManager.OpenStatelessSession())
            {
                foreach (Product product in RedoProducts)
                {
                    Debug.WriteLine("id before=" + product.Id);
                    session.Delete(product);
                    
                }
            }

        }
    }

    public class ChangeLinesBalanceSheetOperation : BalanceSheetOperation
    {
        public ChangeLinesBalanceSheetOperation(Category category, int page, int row, List<Product> undoProducts,
                                    List<Product> redoProducts)
            : base(category, page, row, undoProducts, redoProducts)
        {
        }

        public override void Undo()
        {
            using (ISession session = SessionManager.OpenSession())
            {
                foreach (Product product in UndoProducts)
                {
                    session.Merge(product);
                }
                session.Flush();
            }
        }

        public override void Redo()
        {
            using (ISession session = SessionManager.OpenSession())
            {
                foreach (Product product in RedoProducts)
                {
                    session.Merge(product);
                }
                session.Flush();
            }
        }
    }

}
