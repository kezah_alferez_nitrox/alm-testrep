﻿using System;
using System.Diagnostics;
using System.Windows.Forms;
using InsuranceALM.Infrastructure.Data;
using NHibernate;

namespace InsuranceBondManager.Core
{
    public class SessionUserControl : UserControl, ISessionHolder
    {
        private ISession session;

        public SessionUserControl()
        {
            this.Disposed += this.SessionUserControlDisposed;
        }

        protected void ClearSession()
        {
            if (this.session != null)
            {
                this.session.Dispose();
                this.session = null;
            }
        }

        ISession ISessionHolder.Session
        {
            get { return Session; }
        }

        void ISessionHolder.SaveChangesInSession()
        {
            this.SaveChangesInSession();
        }

        protected ISession Session
        {
            get
            {
                if (this.session == null)
                {
                    this.session = SessionManager.OpenSession();
                    // Debug.WriteLine("*** Core SessionUserControl OpenSession " + this.session.GetHashCode() + " ***");
                }
                return this.session;
            }
        }

        public void SaveChangesInSession()
        {
            if (this.session == null) return;

            this.session.Flush();
        }

        private void SessionUserControlDisposed(object sender, EventArgs e)
        {
            ClearSession();
        }
    }
}
