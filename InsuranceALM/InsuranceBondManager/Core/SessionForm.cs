﻿using System;
using System.Windows.Forms;
using ALMSCommon;
using ALMSCommon.Forms;
using InsuranceALM.Infrastructure.Data;
using InsuranceBondManager.Calculations;
using InsuranceBondManager.Domain;
using NHibernate;

namespace InsuranceBondManager.Core
{
    public class SessionForm : DpiForm, ISessionHolder
    {
        private ISession session;

        public SessionForm()
        {
            this.FormClosed += this.SessionFormClosed;
        }

        ISession ISessionHolder.Session
        {
            get { return Session; }
        }

        void ISessionHolder.SaveChangesInSession()
        {
            this.SaveChangesInSession();
        }

        protected ISession Session
        {
            get
            {
                if (this.session == null)
                {
                    this.session = SessionManager.OpenSession();
                }
                return this.session;
            }
        }

        protected void DisposeSession()
        {
            if (session == null) return;

            session.Dispose();
            session = null;
        }

        public void SaveChangesInSession()
        {
            this.Cursor = Cursors.WaitCursor;
            try
            {
                if (this.session == null) throw new InvalidOperationException("Session in null");

                this.session.Flush();

                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Dialogs.Error("Error", "An error occured, your changes may have not been saved.");
                Log.Error(ex.ToString());
                this.Cursor = Cursors.Default;
                this.Close();
            }
        }

        private void SessionFormClosed(object sender, FormClosedEventArgs e)
        {
            if (this.session != null)
            {
                this.session.Dispose();
                this.session = null;
            }
        }

        protected SimulationParams GetSimulationParameters()
        {
            return Util.GetSimulationParams(this.Session);
        }
    }
}
