﻿using System;
using System.Collections.Generic;
using ALMS.Products;
using ALMSCommon;
using InsuranceBondManager.Calculations;

namespace InsuranceBondManager.Core
{
    public class ResultAggregator
    {
        private readonly ReportAggregationType aggregationType;
        private readonly IList<DateInterval> dateIntervals;
        private readonly int valuationMonth;
        private bool isComputationDaily;

        public ResultAggregator(ReportType reportType, ReportAggregationType aggregationType, DateTime valuationDate,
                                int resultMonths)
        {
            this.aggregationType = aggregationType;
            this.isComputationDaily = AlmConfiguration.IsComputationDaily();
            this.valuationMonth = PricerUtil.DateToInt(valuationDate, isComputationDaily);
            this.dateIntervals = GetDateIntervals(valuationDate, reportType, resultMonths, isComputationDaily);
        }

        public IList<DateInterval> DateIntervals
        {
            get { return this.dateIntervals; }
        }

        private static IList<DateInterval> GetDateIntervals(DateTime valuationDate, ReportType reportType, int resultSteps, bool isComputationDaily)
        {
            IList<DateInterval> result = new List<DateInterval>();

            DateTime firstDate;
            DateTime lastDate;

            if (isComputationDaily)
            {
                firstDate = valuationDate;
                lastDate = firstDate.AddDays(resultSteps - 2);
            }
            else
            {
                firstDate = Util.LastDayOfMonth(valuationDate);
                lastDate = Util.LastDayOfMonth(firstDate.AddMonths(resultSteps - 2));
            }

            DateTime date = firstDate;

            result.Add(new DateInterval(firstDate, firstDate, isComputationDaily));

            int increment;
            do
            {
                switch (reportType)
                {
                    case ReportType.Annually:
                        increment = 12;
                        break;
                    case ReportType.AnnuallyAsCalendarYear:
                        increment = 12 - (date.Month - 1) % 12;
                        break;
                    case ReportType.Quarterly:
                        increment = 3;
                        break;
                    case ReportType.QuarterlyAsCalendarYear:
                        increment = 3 - (date.Month - 1) % 3;
                        break;
                    case ReportType.Monthly:
                        increment = 1;
                        break;
                    case ReportType.Daily:
                        increment = -1;
                        break;

                    default:
                        throw new ArgumentOutOfRangeException();
                }

                if (increment != -1) //month based computations
                {
                    if (isComputationDaily)
                    {
                        DateTime nextDate = date.AddMonths(increment);
                        if (nextDate > lastDate)
                        {
                            nextDate = lastDate;
                        }
                        result.Add(new DateInterval(date, nextDate.AddDays(-1), isComputationDaily));
                        date = nextDate;
                    }
                    else
                    {
                        if (Util.LastDayOfMonth(date.AddMonths(increment)) >= lastDate)
                        {
                            increment = PricerUtil.DateDiffInSteps(lastDate, date, isComputationDaily);
                        }

                        result.Add(new DateInterval(date, Util.LastDayOfMonth(date.AddMonths(increment - 1)), isComputationDaily));
                        date = Util.LastDayOfMonth(date.AddMonths(increment));
                    }
                }
                else
                {
                    //daily
                    result.Add(new DateInterval(date, date, isComputationDaily));
                    date = date.AddDays(1);
                }
            } while (date < lastDate && increment != 0);

            return result;
        }


        public double DateIntervalValue(double[] values, int step, int offset = 0)
        {
            double value = 0;

            switch (this.aggregationType)
            {
                case ReportAggregationType.Final:
                    value = step == 0
                                ? values[0]
                                : values[this.DateIntervals[step].EndUnit - this.valuationMonth + 1 - offset];
                    break;
                case ReportAggregationType.Average:
                    value = this.DateIntervalAverage(values, step, offset);
                    break;
                case ReportAggregationType.Sum:
                    value = this.DateIntervalSum(values, step, offset);
                    break;

            }

            return value;
        }

        public T DateIntervalValueGeneric<T>(T[] values, int step)
        {
            T value = default(T);

            switch (this.aggregationType)
            {
                case ReportAggregationType.Final:
                    value=GetValue(values, step);
                    break;
            }

            return value;
        }

        public T GetValue<T>(T[] values, int step)
        {
            return step == 0 ? values[0] : values[this.DateIntervals[step].EndUnit - this.valuationMonth + 1];
        }

        private double DateIntervalAverage(double[] values, int step, int offset = 0)
        {
            if (step == 0) return values[0];


            DateInterval interval = this.DateIntervals[step];

            double sum = 0;
            for (int i = interval.StartUnit; i <= interval.EndUnit; i++)
            {
                sum += values[i - this.valuationMonth + 1 - offset];
            }

            return sum / (interval.EndUnit - interval.StartUnit + 1);
        }


        public double DateIntervalSum(double[] values, int step, int offset = 0)
        {
            if (step == 0) return 0;

            double sum = 0;
            DateInterval interval = this.DateIntervals[step];
            for (int i = interval.StartUnit; i <= interval.EndUnit; i++)
            {
                sum += values[i - this.valuationMonth + 1 - offset];
            }

            return sum;
        }
    }
}