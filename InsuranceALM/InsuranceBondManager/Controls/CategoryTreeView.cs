using System;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Windows.Forms;
using InsuranceALM.Infrastructure.Data;
using InsuranceBondManager.Domain;
using InsuranceBondManager.Views;
using NHibernate;

namespace InsuranceBondManager.Controls
{
    public class CategoryTreeView : TreeView
    {
        private readonly Pen linePen;
        private readonly Pen rectanglePen;
        public TreeNode FoundNode;

        private bool dragDropIsOrdering;
        private bool ignoreNodeCheckedChanged;
        private TreeNode lastDraggedOverNode;

        private Font boldFont;

        private ISession session;

        public CategoryTreeView()
        {
            this.AllowDrop = true;
            this.CanOrganize = true;

            this.linePen = new Pen(Color.Black);

            this.rectanglePen = new Pen(Color.Black);
            this.rectanglePen.DashStyle = DashStyle.Dash;
            this.AfterCheck += this.CategoryTreeViewAfterCheck;
            
            this.boldFont = new Font(this.Font, FontStyle.Bold);


        }

        public bool CanOrganize { get; set; }

        private void SetModifiedValues()
        {
            MainForm.ModifiedValues = true;
        }

        void CategoryTreeViewAfterCheck(object sender, TreeViewEventArgs e)
        {
            if (this.ignoreNodeCheckedChanged) return;

            this.ignoreNodeCheckedChanged = true;

            if (e.Node.Checked)
                this.EnsureParentIsVisible(e.Node);
            else
                this.ApplyNodeCheckedToChild(e.Node);

            this.ignoreNodeCheckedChanged = false;
        }

        private void ApplyNodeCheckedToChild(TreeNode treeNode)
        {
            foreach (TreeNode node in treeNode.Nodes)
            {
                node.Checked = treeNode.Checked;
                this.ApplyNodeCheckedToChild(node);
            }
        }

        private void EnsureParentIsVisible(TreeNode node)
        {
            while (node.Parent != null)
            {
                node = node.Parent;
                if (!node.Checked)
                    node.Checked = true;
            }
        }

        public void LoadNodes()
        {
            this.Nodes.Clear();

            this.NewSession();

            Category root = Category.FindPortfolio(this.session);
            if (root == null) return;
            
            TreeNode node = new TreeNode(root.Label+"      ");
            node.Tag = root;
            node.NodeFont = boldFont;

            this.AddChildNodes(node, root, 0);
            node.NodeFont = this.boldFont;
            this.Nodes.Add(node);

            node.Expand();

            foreach (TreeNode subnode in node.Nodes)
            {
                subnode.Expand();

                foreach (TreeNode subsubnode in subnode.Nodes)
                {
                    subsubnode.Expand();
                }
            }
        }

        private void NewSession()
        {
            if (this.session != null)
                this.session.Dispose();
            this.session = SessionManager.OpenSession();
        }


        public void UpdateVisibles(TreeNode RootNode)
        {
            //foreach (Category cat in Category.FindAll())
            //{
            //    if (cat.isVisible == true && cat.Parent != null)
            //    {
            //        GoThrough(RootNode, cat.Id);
            //    }
            //}
            foreach (Category cat in Category.FindAll(this.session))
            {
                this.FoundNode = null;
                this.SearchNode(RootNode, cat.Id);
                if (this.FoundNode != null && this.FoundNode.Parent != null)
                {
                    cat.isVisible = this.FoundNode.Checked;
                    //if (_foundnode.Parent.IsExpanded)
                    //{
                    //    cat.isVisible = true;
                    //}
                    //else
                    //{
                    //    cat.isVisible = false;
                    //}
                }
            }
            this.session.Flush();
        }

        public void LoadVisibleNodesForConfig()
        {            
            this.Nodes.Clear();

            this.ignoreNodeCheckedChanged = true;

            this.NewSession();
            Category root = Category.FindBalanceSheet(this.session);

            TreeNode RootNode = new TreeNode(root.Label);
            RootNode.NodeFont = this.boldFont;
            RootNode.Tag = root;
            
            this.AddChildNodes(RootNode, root, 0);
            this.Nodes.Add(RootNode);
            RootNode.Expand();

            TreeNode node1 = RootNode;


            foreach (Category cat in Category.FindAll(this.session))
            {
                if (cat.isVisible == true && cat.Parent != null)
                {
                    this.GoThrough(node1, cat.Id);
                }
            }

            this.ignoreNodeCheckedChanged = false;
            this.session.Flush();
        }


        private void GoThrough(TreeNode node, int id)
        {
            foreach (TreeNode _node in node.Nodes)
            {
                if (_node.Tag is Category && (_node.Tag as Category).Id == id)
                {
                    _node.Parent.Expand();
                    _node.Checked = true;
                }
                this.GoThrough(_node, id);
            }

        }

        private void SearchNode(TreeNode node, int id)
        {
            Category category = node.Tag as Category;
            if (category != null && category.Id == id)
            {
                this.FoundNode = node;
            }
            else
            {
                if (node.FirstNode != null)
                    this.SearchNode(node.FirstNode, id);
                if (node.NextNode != null)
                    this.SearchNode(node.NextNode, id);
            }
        }

        private void AddChildNodes(TreeNode parentNode, Category parent, int level)
        {
            foreach (Category category in parent.ChildrenOrdered)
            {
                TreeNode childNode = new TreeNode(category.Label + "      ");
                if (level >= 2)
                {
                    childNode.Tag = category;
                }
                else
                {
                    childNode.NodeFont = this.boldFont;
                    childNode.Tag = category;
                }
                    
                if (level == 0)
                {
                    childNode.ForeColor = Color.Blue;
                }
                parentNode.Nodes.Add(childNode);
                this.AddChildNodes(childNode, category, level+1);
            }
        }

        public void RenameNode()
        {
            if (null != this.SelectedNode)
            {
                this.LabelEdit = true;
                this.SelectedNode.BeginEdit();
            }
        }

        public void AddNode()
        {
            if (null == this.SelectedNode) return;

            Category parent = (Category)this.SelectedNode.Tag;
            if (parent == null) return;

            Category category = new Category();
            category.Parent = parent;
            category.IsVisibleChecked = true;
            category.isVisible = true;
            category.ALMID = Category.GetNewALMID(session);
            TreeNode childNode = new TreeNode(string.Empty);
            childNode.Tag = category;
            this.SelectedNode.Nodes.Add(childNode);
            this.SelectedNode = childNode;
            this.LabelEdit = true;
            childNode.BeginEdit();
        }

        public void DeleteNode()
        {
            if (null == this.SelectedNode) return;
            if (this.SelectedNode.Nodes.Count > 0) return;

            Category category = (Category)this.SelectedNode.Tag;
            if(category == null) return;

            category.Refresh(session);

            category.Parent.Children.Remove(category);
            category.Delete(this.session);

            session.Flush();

            this.Nodes.Remove(this.SelectedNode);
        }

        protected override void OnAfterLabelEdit(NodeLabelEditEventArgs e)
        {
            if (!string.IsNullOrEmpty(e.Label))
            {
                Category category = (Category)e.Node.Tag;

                if (category.IsNew)
                {
                    CategoryType categoryType = category.Parent.Type == CategoryType.RollParent ? CategoryType.Roll : category.Parent.Type;

                    category.Label = e.Label;
                    category.BalanceType = category.Parent.BalanceType;                    
                    category.Type = categoryType;
                    category.Create(this.session);

                    category.Parent.Children.Add(category);
                }
                else
                {
                    e.Node.Tag = category;
                    category.Label = e.Label;
                    category.Save(this.session);
                }

                session.Flush();

                e.Node.Text = e.Label;
                e.Node.EndEdit(false);

                this.SetModifiedValues();

                this.SelectedNode = e.Node;
                this.OnAfterSelect(new TreeViewEventArgs(e.Node));
            }
            else
            {
                e.CancelEdit = true;
                Category category = (Category)e.Node.Tag;

                if (category.IsNew)
                {
                    TreeNode parent = e.Node.Parent;
                    e.Node.EndEdit(true);
                    this.SelectedNode = parent;
                    parent.Nodes.Remove(e.Node);
                }
            }
            this.LabelEdit = false;
            base.OnAfterLabelEdit(e);
        }

        protected override void OnMouseClick(MouseEventArgs e)
        {
            base.OnMouseClick(e);

            if (e.Button != MouseButtons.Right) return;

            TreeNode node = this.GetNodeAt(e.Location);
            if (null != node)
            {
                this.SelectedNode = node;
            }
        }

        protected override void OnItemDrag(ItemDragEventArgs e)
        {
            if (!this.CanOrganize) return;

            this.DoDragDrop(e.Item, DragDropEffects.Move);
        }

        protected override void OnDragOver(DragEventArgs e)
        {
            e.Effect = DragDropEffects.None;
            if (!e.Data.GetDataPresent(typeof(TreeNode)))
            {
                this.SelectedNode = null;
                return;
            }

            TreeNode srcNode = (TreeNode)e.Data.GetData(typeof(TreeNode));
            if (srcNode == null) return;

            Point pt = this.PointToClient(new Point(e.X, e.Y));
            TreeNode destinationNode = this.GetNodeAt(pt);
            if (destinationNode == null) return;

            Category srcCategory = srcNode.Tag as Category;
            Category destCategory = destinationNode.Tag as Category;
            if (srcCategory == null || destCategory == null)
            {
                return;
            }

            if (this.lastDraggedOverNode != null && this.lastDraggedOverNode != destinationNode)
            {
                this.InvalidateNode(this.lastDraggedOverNode);
            }
            this.lastDraggedOverNode = destinationNode;

            Rectangle bounds = destinationNode.Bounds;
            bool overBottom = pt.Y + 4 >= bounds.Bottom;

            if (overBottom)
            {
                if (!this.dragDropIsOrdering)
                {
                    this.InvalidateNode(destinationNode);
                }
                using (Graphics g = Graphics.FromHwnd(this.Handle))
                {
                    g.DrawLine(this.linePen, bounds.Left, bounds.Bottom, bounds.Right, bounds.Bottom);
                }
            }
            else
            {
                if (this.dragDropIsOrdering)
                {
                    this.InvalidateNode(destinationNode);
                }
                using (Graphics g = Graphics.FromHwnd(this.Handle))
                {
                    g.DrawRectangle(this.rectanglePen, bounds.Left, bounds.Top, bounds.Width, bounds.Height);
                }
            }

            this.dragDropIsOrdering = overBottom;

            bool alowed = CanMoveCategory(srcCategory, destCategory, this.dragDropIsOrdering);

            e.Effect = alowed ? DragDropEffects.Move : DragDropEffects.None;
        }

        private bool CanMoveCategory(Category srcCategory, Category destCategory, bool isOrdering)
        {

            //todo : globally review these rules
            if (isOrdering)
            {
                if (srcCategory == destCategory) return false;

                if (srcCategory.Parent == destCategory.Parent) return true;
                destCategory = destCategory.Parent;
            }

            if (destCategory == null) return false;

            if (srcCategory == destCategory) return false;

            if (srcCategory.Type != destCategory.Type) return false;

            if (IsParent(srcCategory, destCategory)) return false;

            if (destCategory.Parent == null) return false;

            if (srcCategory.Parent == null) return false;

            if (srcCategory.Parent == destCategory) return false;

            if (srcCategory.Parent.BalanceType == BalanceType.Portfolio) return false;

            if (srcCategory.Parent.Parent == null) return false;

            if (srcCategory.Parent.Parent.BalanceType == BalanceType.Portfolio) return false;

            if (destCategory.Parent.BalanceType == BalanceType.Portfolio) return false;

            if (destCategory.ProductCount(session)>0) return false;

            return true;
        }

        private static bool IsParent(Category parent, Category child)
        {
            while (child.Parent != null)
            {
                if (parent == child.Parent) return true;
                child = child.Parent;
            }

            return false;
        }

        private void InvalidateNode(TreeNode node)
        {
            Rectangle rectangle = node.Bounds;
            rectangle.Inflate(1, 1);
            this.Invalidate(rectangle);
        }

        protected override void OnDragDrop(DragEventArgs e)
        {
            Point pt = this.PointToClient(new Point(e.X, e.Y));
            TreeNode destinationNode = this.GetNodeAt(pt);

            if (this.lastDraggedOverNode != null)
            {
                this.InvalidateNode(this.lastDraggedOverNode);
                this.lastDraggedOverNode = null;
            }

            // Ensure that the list item index is contained in the data.
            if (e.Data.GetDataPresent(typeof(TreeNode)))
            {
                TreeNode srcNode = (TreeNode)e.Data.GetData(typeof(TreeNode));
                if (srcNode == destinationNode) return;
                if (e.Effect == DragDropEffects.Move)
                {
                    Category srcCategory = srcNode.Tag as Category;
                    Category destCategory = destinationNode.Tag as Category;
                    if (srcCategory == null || destCategory == null)
                    {
                        return;
                    }

                    if (this.dragDropIsOrdering)
                    {
                        if (destinationNode.Parent == null) return;

                        srcNode.Remove();
                        destinationNode.Parent.Nodes.Insert(destinationNode.Index + 1, srcNode);

                        if (destCategory.Parent == srcCategory.Parent)
                        {
                            destCategory.Parent.Children.Remove(srcCategory);
                            destCategory.Parent.Children.Insert(
                                destCategory.Parent.Children.IndexOf(destCategory) + 1, srcCategory);
                        }
                        else
                        {
                            srcCategory.Parent = destCategory.Parent;
                            destCategory.Parent.Children.Insert(
                                destCategory.Parent.Children.IndexOf(destCategory) + 1, srcCategory);
                            destCategory.Save(this.session);
                        }

                        for (int i = 0; i < destinationNode.Parent.Nodes.Count; i++)
                        {
                            TreeNode node = destinationNode.Parent.Nodes[i];
                            Category category = node.Tag as Category;
                            if (category == null) continue;

                            category.Position = node.Index;
                        }
                    }
                    else
                    {
                        srcNode.Remove();
                        destinationNode.Nodes.Add(srcNode);
                        destinationNode.Expand();

                        srcCategory.Parent = destCategory;

                        destCategory.Children.Add(srcCategory);
                        this.UpdateBalanceType(srcCategory, destCategory.BalanceType);
                        destCategory.Save(this.session);
                    }

                    this.session.Flush();
                }

                this.SelectedNode = srcNode;
            }
        }

        private void UpdateBalanceType(Category category, BalanceType balanceType)
        {
            category.BalanceType = balanceType;
            foreach (Category child in category.Children)
            {
                this.UpdateBalanceType(child, balanceType);
            }
        }
    }
}